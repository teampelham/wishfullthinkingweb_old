<?php
include ('../wfs/database_connection.php');

function parseTokenHeader() {
    $headers = array();
    foreach($_SERVER as $key => $value) {
        if (substr($key, 0, 15) <> 'HTTP_X_WF_TOKEN') {
            continue;
        }
        $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
        return $value;
    }
    return NULL;
}

header('Content-Type: text/xml');

$authHeader = parseTokenHeader();
$error = array(); //Declare An Array to store any error message

if(empty($authHeader) || $authHeader == "undefined") {
    $error[] = 'Invalid Token ';
}

if (empty($error)) //send to Database if there's no error '
{ 
    $query_verify = "SELECT * FROM `Users` WHERE AuthenticationToken = '$authHeader'";
    $result_verify = mysqli_query($dbc, $query_verify);
    
    if (!$result_verify) { //if the Query Failed ,similar to if($result_verify_email==false)
        $error[] = ' Database Error Occurred ';
    } else if (mysqli_num_rows($result_verify) == 0) { 
        $error[] = 'Invalid Token';
    }
    
    if(empty($error))
    {
        $item = $result_verify->fetch_object();
        
        echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        // Flush the buffered output.
        echo '<user email="' . $item->Email . '" username="' . $item->Username . '" title="' . $item->Title . '" authenticationToken="' . $item->AuthenticationToken . '" restricted="0" home="0" guest="0" secure="1" homeSize="1" certificateVersion="2">' . "\n";
        echo '    <entitlements all="0"></entitlements>' . "\n";
        echo "    <username>" . $name . "</username>\n";
        echo "    <email>" . $item->Email . "</email>\n";
        echo "    <authentication-token>" . $item->AuthenticationToken . "</authentication-token>\n";
        echo "</user>\n";
    }
    mysqli_close($dbc); //Close the DB Connection
}

if (!empty($error)) //send to Database if there's no error '
{ //If the "error" array contains error msg , display them
    http_response_code(422);
   //header("HTTP/1.1 401 DEAD WRONG");
    echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
    echo "<errors>\n";
    
    foreach ($error as $key => $values) {
        echo "    <error>".$values."</error>\n";
    }
    
    echo "</errors>\n";
}
?>