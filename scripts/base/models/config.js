define([
    "underscore", 
    "backbone"
], function(_, bb) {
    "use strict";
    var Config = bb.Model.extend({
        defaults: _.extend({        // WHAT ARE THESE FOR???
            env: "development",     // TODO: Switch this once we're live, record where this is used too
            bundled: false,
            hosted: true,
            flash: true,
            colorizeLogs: true,
            defaultTarget: "_self",
            serverUpdate: false,
            serverSavePrimaryOnly: false,
            requestAllConnections: false,
            supportsHttpsMediaServer: false,
            allowMixedContent: false
        }, window.WFWEB)
    });
    
    return new Config();
});