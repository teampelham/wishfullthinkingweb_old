define([
    "underscore",
    "base/adapters/settingsAdapter", 
    "base/models/baseModel"
], function(_, settingsAdapter, baseModel) {
    "use strict";
    
    var annoucementModel = baseModel.extend({
        whitelistedAttrs: ["promo", "timestamp", "link", "thumb", "title", "description"],
        
        initialize: function() {
            this.listenTo(settingsAdapter.getInstance(), "change:announcementsLastReadTimestamp", this.onTimestampChange);
            this.onTimestampChange();
        },
        
        parse: function (announcementObj) {
            return _.pick(announcementObj, this.whitelistedAttrs);
        },
        
        onTimestampChange: function () {
            this.set("isUnread", this.get("timestamp") > settingsAdapter.get("announcementsLastReadTimestamp"));
        }
    });
    
    return annoucementModel;
});