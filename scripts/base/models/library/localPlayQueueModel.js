define([
	"underscore", 
	"jquery", 
	"backbone", 
	"base/models/baseModel", 
	"base/utils/types/mediaTypes", 
	"base/models/library/playQueueItemCollection", 
	"base/models/library/playQueueItemModel"
], function(e, t, i, n, s, a, r) {
    "use strict";
    var o = function(e) {
        return e instanceof i.Collection ? e : e.get("children")
    }, l = function(e) {
            var t = e.get("type"),
                i = "channelDirectory" === t,
                n = i || t && "directory" === s.getMediaTypeByString(t).element || e.get("isFolder"),
                a = void 0 === e.length;
            return a && !n
        }, c = function(t) {
            var i = this.get("nextPlayQueueItemID");
            this.set("nextPlayQueueItemID", ++i);
            var n = e.extend({}, t.attributes, {
                playQueueItemID: i
            }),
                s = new r(n, {
                    server: t.server,
                    parent: t.parent
                });
            return s
        }, d = n.extend({
            defaults: {
                selectedItem: null,
                mediaType: null,
                collection: null,
                owned: !0,
                lastAddedItem: null,
                hasUserAdditions: !1,
                nextPlayQueueItemID: 0
            },
            url: "",
            initialize: function(e) {
                this.set("mediaType", e.mediaType), this.set("collection", new a([], {
                    type: e.mediaType,
                    server: e.server
                }))
            },
            fetch: function() {},
            queueContent: function(i, n, s) {
                s || (s = {});
                var a = t.Deferred().resolve().promise();
                return this.isItemQueueable(i) && (i.get("children") && e.result(i.get("children"), "url") && (a = a.then(function() {
                    return i.get("children").populate()
                })), a = a.done(e.bind(function() {
                    this.set("lastAddedItem", i), this.get("collection").length && this.set("hasUserAdditions", !0);
                    var t = this.get("collection").indexOf(n); - 1 === t && (t = 0);
                    var a = s.next ? {
                        at: t + 1
                    } : {}, r = o(i);
                    if (r) {
                        var d = r.filter(l);
                        s.shuffle && (d = e(d).shuffle());
                        var u = e.map(d, e.bind(c, this));
                        this.get("collection").add(u, a)
                    } else this.get("collection").add(c.apply(this, [i]), a);
                    this.get("collection").trigger("reset", this.get("collection"))
                }, this))), a
            },
            addContent: function(t, i) {
                var n = this.queueContent(t, this.get("selectedItem"), i);
                return n.done(e.bind(function() {
                    var e = null;
                    i && i.firstItem ? e = this.get("collection").find(function(e) {
                        return e.get("key") === i.firstItem.get("key")
                    }) : this.get("collection").length && (e = this.get("collection").at(0)), this.set("selectedItem", e)
                }, this)), n
            },
            removeItem: function(e) {
                return e.get("playQueueItemID") === this.get("selectedItem").get("playQueueItemID") ? t.Deferred().resolve().promise() : (this.set("hasUserAdditions", !0), this.get("collection").remove(e), this.get("collection").trigger("reset", this.get("collection")), t.Deferred().resolve().promise())
            },
            moveItem: function(e, i) {
                if (e === this.get("selectedItem").get("playQueueItemID")) return t.Deferred().resolve().promise();
                var n = this.get("collection"),
                    s = n.findWhere({
                        playQueueItemID: e
                    });
                n.remove(s);
                var a = i || this.get("selectedItem").get("playQueueItemID"),
                    r = n.findWhere({
                        playQueueItemID: a
                    }),
                    o = n.indexOf(r) + 1;
                return this.set("hasUserAdditions", !0), n.add(s, {
                    at: o
                }), n.trigger("reset", n), t.Deferred().resolve().promise()
            },
            isItemQueueable: function(t) {
                if (t.get("currentSubset") && "photo" !== t.viewGroup) return !1;
                if ("photoAlbum" === t.get("type") && !t.isLibraryItem()) return !1;
                var i = ["track", "episode", "movie", "photo", "clip", "userPlaylistItem", "photoAlbum", "album", "season"],
                    n = e(i).contains(t.get("type")),
                    s = o(t),
                    a = s && s.any(l);
                return n || a
            },
            yieldOwnership: function() {
                var i = this.get("lastAddedItem"),
                    n = e.result(i.get("children") || i, "url");
                return t.Deferred().resolve(n).promise()
            },
            takeOwnership: function() {
                return t.Deferred().resolve().promise()
            },
            release: function() {
                this.get("collection").release()
            },
            supports: function(e) {
                return !(e && (e.unwatched || e.remoteRefresh))
            }
        });
    return d
})