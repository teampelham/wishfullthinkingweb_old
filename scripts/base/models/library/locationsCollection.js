define([
	"underscore", 
	"base/models/baseCollection", 
	"base/models/library/locationsModel"
], function(_, baseCollection, locationsModel) {
    "use strict";
	
    var locationCollection = baseCollection.extend({
        model: locationsModel,
        url:  "device/getlocationlist",
        
        initialize: function(initial, initObj) {
            initObj = initObj || {};
            
            this.server = initObj.server;    
        },
        
        parse: function(locationObj) {
            var locations = this.convert(locationObj);
            if (!_.isArray(locations))
                locations = [locations];
            
            return locations;
        },
        
        toJSON: function() {
            var locations = [];
            this.each(function(location) {
               locations.push(location.toJSON()); 
            });
            return locations;
        }
    });
	
    return locationCollection;
})