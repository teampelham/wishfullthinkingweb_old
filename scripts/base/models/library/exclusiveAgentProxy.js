define([
	"underscore", 
	"backbone.obscura"
], function(e, t) {
    "use strict";
    var i = t.extend({
        constructor: function(i, n, s, a) {
            this.exclusiveAgents = n, this.agentIdentifier = s, t.call(this, i, a), this.filterBy(e.bind(this.filterExclusiveAgent, this))
        },
        filterExclusiveAgent: function(e) {
            var t = e.get("identifier"),
                i = this.exclusiveAgents;
            if (!i) return !0;
            var n = this.agentIdentifier,
                s = -1 !== i.indexOf(n),
                a = -1 !== i.indexOf(t),
                r = s ? t == n : !a;
            return r
        }
    });
    return i
});