define([
	"underscore", 
	"jquery", 
	"base/utils/parser", 
	"base/models/library/agentModel"
], function(_, $, parser, agentModel) {
    "use strict";
    
	var insteonAgentModel = agentModel.extend({
		serverMethods: {
			beginRegister: "begindeviceregister",
			cancelRegister: "canceldeviceregister"	
		},
		initialize: function(initObj) {
			initObj = initObj || {};
			
			this.server = initObj.server;
		},
		
		url: function() {
			var connection = this.server.get("activeConnection") || {};
			
			return connection.uri + '/insteon/';
		},
		
		beginRegisterMode: function() {
			var url = this.url();
			
			// TODO: Use xhrTracker here?
			var promise = $.post(url + this.serverMethods.beginRegister);
			
			return promise;
		},
		
		cancelRegisterMode: function() {
			var url = this.url();
			
			// TODO: Use xhrTracker here?
			var promise = $.post(url + this.serverMethods.cancelRegister);
			
			return promise;
		}
	});
	
	return insteonAgentModel;
});