define([
	"require", 
	"underscore", 
	"jquery", 
	"base/utils/t", 
	"base/utils/parser", 
	"base/utils/mediaUtil", 
	"base/utils/types/streamTypes", 
	"base/utils/formatters/stringUtil", 
	"base/models/baseModel", 
    "base/utils/types/automationProtocolTypes"
], function(require, _, $, t, parser, mediaUtil, streamTypes, stringUtil, baseModel, automationProtocolTypes) {
    "use strict";
    
	var automatedDeviceModel = baseModel.extend({
        idAttribute: "ratingKey",
        defaults: {
            title: "",
            titleSort: "",
            tagline: "",
            rating: 0,
            summary: "",
            year: "",
            studio: "",
            contentRating: "",
            originallyAvailableAt: "",
            originalTitle: "",
            thumb: "",
            parentThumb: "",
            grandparentThumb: "",
            banner: "",
            art: "",
            parentArt: "",
            grandparentArt: "",
            primaryExtraKey: ""
        },
        omittedSaveAttrs: ["updatedAt", "thumb", "banner", "art", "Media", "Location"],
        tagAttrs: ["Collection", "Director", "Writer", "Genre", "Mood", "Country", "Label", "Similar"],
        subcollections: ["Media", "Genre", "Collection", "Field", "Writer", "Director", "Role", "Country", "Location", "Label"],
        metadataElementNames: ["Directory", "Video", "Track", "Photo", "Hub"],
        whitelistedSaveAttrs: ["automatedDeviceID", "title", "location", "address", "hardware", "dimmable", "type", "deviceType"],
        
		url: function() {
            return this.get("key");
        },
        
        getUrl: function() {
            return "/device/setdevicestate?deviceID=" + this.get("automatedDeviceID");
        },
        
        saveUrl: function() {
            return "/device/updatedevice";
        },
        
        isNew: function() {
            return !this.has("automatedDeviceID");
        },
        
        setNewOnLevel: function(e) {
            if (_.isString(e))
                e = this.parse(e);
                
            if(!_.isUndefined(e.OnLevel) && this.get("type") === "light") {
                this.set("OnLevel", e.OnLevel);  
                this.trigger("change:onLevel", e.OnLevel);
            }
        },
		
        urlRoot: function() {
            return this.get("key");
        },
		
        firstItemUrl: function() {
            return this.url();
        },
		
        singleMetadataModel: function() {
            return this;
        },
		
        cacheKey: function(key) {
            if (key.identifier && !this.isLibraryItem(key)) 
                key = this.normalizeKey(key);
                
            var childKey = this.normalizeChildrenKey(key.key);
            return this.server && this.server.getScopedKey(childKey.key);
        },
		
        preserveCachedAttributes: function(e) {
            return /\/(children|library\/sections\/\d+\/\w+)(\?.*)?$/.test(e);
        },
		
        initialize: function() {
            this.listenTo(this, "socket:timeline", function(e) {
                e.state === c.PROCESSED ? (this.fetch({
                    data: {
                        includeExtras: "1",
                        includeRelated: "1",
                        includeRelatedCount: "5",
                        includeConcerts: "1",
                        includeOnDeck: "1"
                    }
                }), this.refreshChildren()) : e.state === c.DELETED && this.trigger("destroy", this, this.collection)
            })
        },
		
        convert: function(metaObj) {
            if (_.isString(metaObj)) 
                metaObj = parser.replaceXMLElementNames(metaObj, this.metadataElementNames, "Metadata");
                
            var meta = _.isObject(metaObj) ? metaObj : parser.parseXML(metaObj);
            
            if (!meta.ratingKey && !meta.key && !meta.id) {
                var medadata = meta.Metadata || meta;
                medadata.identifier = meta.identifier; 
                medadata.mediaTagPrefix = meta.mediaTagPrefix; 
                medadata.mediaTagVersion = meta.mediaTagVersion; 
                medadata.allowSync = meta.allowSync; 
                medadata.librarySectionID = meta.librarySectionID; 
                medadata.librarySectionUUID = meta.librarySectionUUID; 
                meta = medadata;
            }
            
            parser.parseBoolean(meta, ["allowSync", "selected"]); 
            parser.parseInteger(meta, ["index", "automatedDeviceID", "parentIndex", "absoluteIndex", "duration", "viewOffset", "viewCount", "leafCount", "viewedLeafCount", "extraType", "lastViewedAt", "addedAt", "updatedAt"]); 
            parser.parseFloat(meta, ["rating", "userRating"]); 
            parser.parseArray(meta, this.subcollections);
            var medias = meta.Media;
            
            _.each(medias, function(media) {
                parser.parseBoolean(media, ["indirect", "optimizedForStreaming", "premium"]); 
                parser.parseInteger(media, ["id", "duration", "width", "height", "bitrate", "audioChannels"]); 
                parser.parseArray(media, "Part");
                var mediaParts = media.Part;
                
                _.each(mediaParts, function(mediaPart) {
                    parser.parseBoolean(mediaPart, "optimizedForStreaming"); 
                    parser.parseInteger(mediaPart, ["id", "duration", "size"]); 
                    parser.parseEncodedString(mediaPart, "file"); 
                    parser.parseArray(mediaPart, "Stream"); 
                    mediaPart.exists = "0" !== mediaPart.exists; 
                    mediaPart.accessible = "0" !== mediaPart.accessible;
                    var streams = mediaPart.Stream;
                    _.each(streams, function(stream) {
                        parser.parseBoolean(stream, "selected"); 
                        parser.parseInteger(stream, ["id", "index", "streamType", "duration", "width", "height", "bitrate", "bitDepth", "samplingRate", "channels"]);
                    })
                })
            });
            
            return meta;
        },
		
        parse: function(metaObj) {
            var metadata = this.convert(metaObj);
            
            if (!metadata.key) 
                return metadata;
                
            if (!this.isLibraryItem(metadata)) 
                metadata = this.normalize(metadata); 
            
            if ("photo" === metadata.type && /\/children$/.test(metadata.key)) 
                metadata.type = "photoAlbum"; 
            
            if ("photoalbum" === metadata.type) 
                metadata.type = "photoAlbum"; 
            
            metadata.isFolder = /\/folder\?parent\=/.test(metadata.key); 
            metadata.isExtra = !! metadata.extraType;
            var metadataCollection = require("base/models/library/metadataCollection");
            //var hubCollection = require("base/models/library/hubCollection");
            
            this.parseChildren(metadata, metadataCollection); 
            //this.parseExtras(metadata, metadataCollection); 
            //this.parseRelated(metadata, hubCollection); 
            //this.parseOverlays(metadata); 
            //this.parsePopularLeaves(metadata, metadataCollection); 
            //this.parseConcerts(metadata, h); 
            this.parseOnDeck(metadata); 
            
            if (null != metadata.viewedLeafCount)
                metadata.unviewedLeafCount = metadata.leafCount - metadata.viewedLeafCount;
            
            var medias = metadata.Media;
            var subTitles = streamTypes.getStreamTypeByString("subtitles");
            
            _.each(medias, function(media) {
                var part = _.first(media.Part);
                
                if (part && part.Stream) {
                    var stream = part.Stream;
                    var subTitleStream = mediaUtil.getStreams(media, {
                        streamType: subTitles.id,
                        selected: true
                    });
                    var streamObj = {
                        id: 0,
                        streamType: subTitles.id,
                        language: t("None")
                    };
                    
                    if (!subTitleStream.length) 
                        streamObj.selected = true;
                         
                    stream.unshift(streamObj);
                }
            }); 
            
            _.defaults(metadata, this.defaults);
            return metadata;
        },
		
        parseChildren: function(meta, MetadataCollection) {
            var normalChild = this.normalizeChildrenKey(meta.key);
            
            if (normalChild.hasChildren) {
                meta.key = normalChild.key;
                if (!this.get("children")) {
                    var prevKey = normalChild.previousKey;
                    if ("album" === meta.type) {
                        prevKey += prevKey.indexOf("?") > -1 ? "&" : "?"; 
                        prevKey += "includeRelated=1";
                    }
                    var metaCollection = new MetadataCollection([], {
                        server: this.server,
                        section: this.section,
                        parent: this,
                        url: prevKey
                    });
                    this.set("children", metaCollection);
                }
            }
        },
		
        parseRelated: function(item, HubCollection) {
            if (!item.Related) 
                return void this.unset("related");
                
            var relatedItems, isMeta; 
            var related = this.get("related");
            
            if (item.Related.Metadata) {
                relatedItems = item.Related; 
                isMeta = true;
            }
            else {
                relatedItems = []; 
                isMeta = false;
            }
            
            if (related) 
                related.reset(relatedItems, {
                    parse: isMeta
                }); 
            else {
                related = new HubCollection(relatedItems, {
                    server: this.server,
                    section: this.section,
                    parent: this,
                    populated: true,
                    parse: isMeta
                }); 
                this.set("related", related);
            } 
            delete item.Related;
        },
        
        parseOnDeck: function(item) {
            if (!item.OnDeck) 
                return void this.unset("onDeck");
                
            var onDeck = this.get("onDeck");
            var onDeckMeta = item.OnDeck.Metadata;
            
            if (onDeck) 
                onDeck.set(onDeckMeta, {
                    parse: true
                }); 
            else {
                onDeck = new metadataModel(onDeckMeta, {
                    server: this.server,
                    section: this.section,
                    parse: true
                }); 
                this.set("onDeck", onDeck);
            }
            delete item.OnDeck;
        },
        
        normalizeChildrenKey: function(childKey) {
            var keyRegex = /(.*)\/children$/.exec(childKey);
            var prevKey = childKey;
            
            if (keyRegex) 
                childKey = keyRegex[1]; 
            
            return {
                key: childKey,
                previousKey: prevKey,
                hasChildren: !! keyRegex
            };
        },
        
        normalizeKey: function(key) {
            var keySplit = key.key.split("/");
            if (1 === keySplit.length && key.containerKey) {
                key.hasInvalidKey = true; 
                key.originalKey = key.key; 
                key.key = key.containerKey + "/" + key.key;
            } 
            
            if (!key.ratingKey) 
                key.ratingKey = key.originalKey || key.key;
            
            return key;
        },
        
        getSaveParams: function() {
            var saveParams = baseModel.prototype.getSaveParams.apply(this, arguments);
            saveParams = _.pick(saveParams, this.whitelistedSaveAttrs);
            
            if ("type" in saveParams) {
                var protocolType = automationProtocolTypes.getAutomationTypeByID(saveParams.type) || automationProtocolTypes.getSectionTypeByString(saveParams.type);
                if (protocolType)
                    saveParams.AutomationProtocolID = protocolType.typeID;
                delete saveParams.type;
            }
            
            if ("active" in saveParams) {
                saveParams.Active = !!saveParams.active;
                delete saveParams.active;
            }
            else {
                saveParams.Active = true;
            }
            
            if ("deviceType" in saveParams) {
                saveParams.DeviceTypeID = saveParams.deviceType;
                delete saveParams.deviceType;
            }
            
            if ("automatedDeviceID" in saveParams) {
                saveParams.DeviceID = saveParams.automatedDeviceID;
                delete saveParams.automatedDeviceID;
            }
            
            if ("title" in saveParams) {
                saveParams.DeviceName = saveParams.title;
                delete saveParams.title;
            }
            
            if (_.isObject(saveParams.type))
                saveParams.type = saveParams.type.typeString;
            
            if ("location" in saveParams) {
                saveParams.LocationID = saveParams.location;
                delete saveParams.location;
            }
            
            if ("address" in saveParams) {
                saveParams.DeviceAddress = saveParams.address;
                delete saveParams.address;
            }
            
            if ("hardware" in saveParams) {
                saveParams.HardwareID = saveParams.hardware;
                delete saveParams.hardware;
            }
            
            if ("dimmable" in saveParams) {
                saveParams.Dimmable = !!saveParams.dimmable;
                delete saveParams.dimmable;
            }
            
            return { deviceData: JSON.stringify(saveParams) };
        },
        
        getLocation: function() {
            console.warn('parseRelated');
            //var e = t.first(this.get("Location"));
            //return e ? e.path : this.getFilepath()
        }
    });
    
    return automatedDeviceModel;
});