define([
    "underscore", 
    "base/utils/parser", 
    "base/mixins/models/paginationMixin", 
    "base/models/baseCollection", 
    "base/models/library/metadataModel"
], function(_, parser, paginationMixin, baseCollection, metadataModel) {
    "use strict";
    
    var metadataCollection = baseCollection.extend({
        mixins: paginationMixin,
        model: metadataModel,
        metadataElementNames: ["Directory", "Video", "Track", "Photo", "Hub"],
        collectionProperties: ["key", "totalSize", "viewGroup", "allowSync", "title", "grandparentTitle", "art"],
        
        url: function() {
            var itemType = this.section || this.parent;
            return itemType ? _.result(itemType, "url") : "";
        },
        
        initialize: function(e, initOptions) {
            if (!initOptions) 
                initOptions = {}; 
            this.allowAllLeaves = initOptions.allowAllLeaves; 
            this.populatedRange = {
                start: 0,
                end: 0
            };
        },
        
        addItem: function(item) {
            this.add(item);
            this.totalSize += 1;  
        },
        
        parse: function(metaObj) {
            var metas = this.convert(this.mergeElements(metaObj));
            
            if (null == metas.totalSize) 
                metas.totalSize = metas.size;
                 
            parser.parseInteger(metas, "totalSize"); 
            parser.parseBoolean(metas, "allowSync"); 
            parser.parseArray(metas, "Metadata"); 
            
            _.each(this.collectionProperties, function(prop) {
                var propValue = this[prop];
                var metaValue = metas[prop];
                var sectionSubsetId = this.section ? this.section.get("currentSubset").id : null;
                var nonGeneric = sectionSubsetId && "all" !== sectionSubsetId && "folder" !== sectionSubsetId;
                
                if (nonGeneric && "totalSize" === prop && metaValue > 200) 
                    metaValue = 200; 
                    
                if ("totalSize" === prop && this.totalSizeAdjustment) 
                    metaValue += this.totalSizeAdjustment;
                     
                if (void 0 !== metaValue && propValue !== metaValue) {
                    this[prop] = metaValue; 
                    this.trigger("change:" + prop, this, metaValue);
                }
            }, this);
            
            var metaData = metas.Metadata;
            var url = _.result(this, "url");
            
            _.each(metaData, function(meta) {
                meta.containerKey = url; 
                
                if (metas.identifier) 
                    meta.identifier = metas.identifier; 
                    
                if (metas.mediaTagPrefix) 
                    meta.mediaTagPrefix = metas.mediaTagPrefix; 
                    
                if (metas.mediaTagVersion) 
                    meta.mediaTagVersion = metas.mediaTagVersion;
            }); 
            
            if (this.allowAllLeaves) 
                return metaData; 
            
            return _.reject(metaData, function(meta) {
                var isAllLeaves = /\/allLeaves$/.test(meta.key);
                
                if (isAllLeaves && this.totalSize) {
                    this.totalSize -= 1; 
                    if (!this.totalSizeAdjustment) 
                        this.totalSizeAdjustment = 0; 
                    this.totalSizeAdjustment -= 1;
                }
                
                return isAllLeaves;
            }, this);
        },
        
        mergeElements: function(elements) {
            if (_.isString(elements)) 
                elements = parser.replaceXMLElementNames(elements, this.metadataElementNames, "Metadata");
            
            else if (_.isObject(elements) && !elements.Metadata) {
                var picked = _.compact(_.pick(elements, this.metadataElementNames));
                elements.Metadata = _.flatten(picked, true);
            }
            
            return elements;
        },
        
        remove: function() {
            baseCollection.prototype.remove.apply(this, arguments); 
            paginationMixin.remove.apply(this, arguments);
        },
        
        clear: function() {
            this.totalSize = null; 
            this.trigger("change:totalSize", this, null); 
            delete this.totalSizeAdjustment; 
            this.populatedRange = {
                start: 0,
                end: 0
            }; 
            baseCollection.prototype.clear.apply(this, arguments);
        },
        
        splitByType: function() {
            var split = {};
            
            this.each(function(meta) {
                var metaType = meta.get("type");
                split.hasOwnProperty(metaType) ? split[metaType].push(meta) : split[metaType] = [meta];
            });
            
            return split;
        },
        
        filterByType: function(filterType) {
            var items = this.filter(function(item) {
                return item.get("type") === filterType;
            });
            
            return this.clone(items);
        }
    });
    
    return metadataCollection;
});