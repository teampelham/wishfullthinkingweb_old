define([
	"underscore", 
	"jquery", 
	"base/models/baseModel", 
	"base/utils/dispatcher", 
	"base/utils/contentUtil", 
	"base/utils/parser", 
	"base/models/library/playQueueItemCollection"
], function(e, t, i, n, s, a, r) {
    "use strict";
    var o = {
        audio: "Track",
        photo: "Photo",
        video: "Video"
    }, l = i.extend({
            idAttribute: "playQueueID",
            defaults: {
                selectedItem: null,
                mediaType: null,
                collection: null,
                server: null,
                owned: !0,
                allowedElement: null,
                hasUserAdditions: !1,
                playQueueID: null,
                playQueueVersion: 0,
                playQueueSelectedItemID: null,
                message: null
            },
            url: function() {
                return "/playQueues/" + this.get("playQueueID")
            },
            initialize: function(e, t) {
                this.set("server", t.server), this.set("allowedElement", o[this.get("mediaType")]), this.set("collection", new r([], {
                    server: t.server
                }))
            },
            _fetch: function() {
                var t = e.result(this, "url");
                return this._ajax("GET", t)
            },
            convert: function(t) {
                var i = e.isObject(t) ? t : a.parseXML(t);
                return a.parseInteger(i, "playQueueVersion"), a.parseArray(i, this.get("allowedElement")), i
            },
            parse: function(t) {
                t = a.replaceXMLElementNames(t, ["Directory"], "Metadata");
                var i = this.convert(t),
                    n = ["playQueueSelectedItemID", "playQueueSelectedItemOffset", "message"];
                i.playQueueVersion > this.get("playQueueVersion") && n.push("playQueueID", "playQueueVersion"), this.set(e.pick(i, n));
                var s = i[this.get("allowedElement")],
                    r = e(s).pluck("playQueueItemID").indexOf(i.playQueueSelectedItemID),
                    o = parseInt(i.playQueueSelectedItemOffset, 10),
                    l = o - r;
                return e.each(s, function(e, t) {
                    e.queueIndex = l + t, i.identifier && (e.identifier = i.identifier), i.mediaTagPrefix && (e.mediaTagPrefix = i.mediaTagPrefix), i.mediaTagVersion && (e.mediaTagVersion = i.mediaTagVersion)
                }), s.message = i.message, s.totalSize = parseInt(i.playQueueTotalCount, 10), s.availableRange = {
                    start: l,
                    end: l + s.length
                }, s
            },
            _ajax: function(i, s) {
                "audio" === this.get("mediaType") && (s += -1 === s.indexOf("?") ? "?" : "&", s += "includeRelated=1");
                var a = e.extend(this.get("server").getXHROptions(s), {
                    type: i
                }),
                    r = t.ajax(a),
                    o = e.bind(function(e) {
                        var t = this.parse(e);
                        t.message && n.trigger("show:alert", {
                            type: "error",
                            message: t.message
                        }), this.get("collection").reset(t, {
                            parse: !0
                        }), this.set("selectedItem", this.getCurrentItem())
                    }, this);
                return r.done(o, this)
            },
            getCurrentItem: function() {
                var e = this.get("playQueueSelectedItemID");
                return this.get("collection").findWhere({
                    playQueueItemID: e
                }) || null
            },
            queueContent: function(i, n) {
                n || (n = {});
                var a = {
                    type: this.get("mediaType")
                };
                if (i.get("playlistType")) a.playlistID = i.get("ratingKey");
                else {
                    var r = e.pick(n, "unwatched", "items", "excludeSeedItem");
                    a.uri = s.uri(i, r)
                }
                return n.firstItem && (a.key = n.firstItem.get("key")), n.extrasPrefixCount && (a.extrasPrefixCount = n.extrasPrefixCount), a.shuffle = n.shuffle ? 1 : 0, n.continuous && "episode" === i.get("type") && (a.continuous = 1), this.get("playQueueID") ? (a.next = n.next ? 1 : 0, this.set("hasUserAdditions", !0), this._addContentPromise = this._ajax("PUT", this.url() + "?" + t.param(a, !0))) : this._addContentPromise = this._ajax("POST", "/playQueues?" + t.param(a, !0)), this._addContentPromise
            },
            addContent: function(t, i) {
                var n = this.queueContent(t, i);
                return n.done(i && i.firstItem ? e.bind(function() {
                    var e = i.firstItem.get("key"),
                        t = this.get("collection").find(function(t) {
                            return t.get("key") === e
                        });
                    this.set("selectedItem", t)
                }, this) : e.bind(function() {
                    this.set("selectedItem", this.get("collection").at(0))
                }, this))
            },
            removeItem: function(e) {
                if (e.get("playQueueItemID") === this.get("selectedItem").get("playQueueItemID")) return t.Deferred().resolve().promise();
                this.set("hasUserAdditions", !0);
                var i = this.url() + "/items/" + e.get("playQueueItemID");
                return this._ajax("DELETE", i)
            },
            moveItem: function(e, i) {
                if (e === this.get("selectedItem").get("playQueueItemID")) return t.Deferred().resolve().promise();
                var n = i || this.get("selectedItem").get("playQueueItemID");
                this.set("hasUserAdditions", !0);
                var s = {
                    after: n
                }, a = this.url() + "/items/" + e + "/move?" + t.param(s);
                return this._ajax("PUT", a)
            },
            isItemQueueable: function(t) {
                var i = e(t).result("url"),
                    n = /\/(library|hubs|services\/gracenote)\//.test(i) || t.get("playlistType"),
                    s = -1 !== i.indexOf("/sync/"),
                    a = -1 !== i.indexOf("/folder"),
                    r = t.get("type") && ("photoAlbum" === t.get("type") || "photo" === t.get("type")),
                    o = "photo" === t.viewGroup || r,
                    l = t.server.supports("photoPlayQueues"),
                    c = !1;
                if (t.get("currentSubset")) {
                    var d = t.get("currentSubset").id;
                    c = "folder" === d || "recentlyViewedShows" === d
                }
                return !(!n || s || a || !l && o || c)
            },
            yieldOwnership: function() {
                this.set("owned", !1);
                var i = this._addContentPromise || t.Deferred().resolve().promise();
                return i.then(e.bind(function() {
                    return this.get("playQueueID") && this.get("collection").length > 0 ? e.result(this, "url") + "?own=1&window=200" : null
                }, this))
            },
            takeOwnership: function() {
                if (this.get("owned")) return t.Deferred().resolve().promise();
                this.set("owned", !0);
                var i = e.result(this, "url") + "?own=1";
                return this._ajax("GET", i)
            },
            release: function() {
                this.get("collection").release()
            },
            supports: function() {
                return !0
            }
        });
    return l
})