define([
	"underscore", 
	"backbone.obscura"
], function(e, t) {
    "use strict";
    var i = t.extend({
        constructor: function(i, n, s, a) {
            this.exclusiveScanners = n, this.scannerName = s, t.call(this, i, a), this.filterBy(e.bind(this.filterExclusiveScanner, this))
        },
        filterExclusiveScanner: function(e) {
            var t = e.get("name"),
                i = (this.sectionType, this.exclusiveScanners);
            if (!i) return !0;
            var n = this.scannerName,
                s = -1 !== i.indexOf(n),
                a = -1 !== i.indexOf(t),
                r = s ? t == n : !a;
            return r
        }
    });
    return i
});