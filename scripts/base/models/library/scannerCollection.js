define([
	"underscore", 
	"base/models/baseCollection", 
	"base/models/library/scannerModel"
], function(e, t, i) {
    "use strict";
    var n = t.extend({
        model: i,
        url: function() {
            return "system/scanners/" + this.sectionType.typeID
        },
        initialize: function(e, t) {
            this.sectionType = t.sectionType
        },
        parse: function(e) {
            return this.convert(e, "Scanner")
        },
        hasExclusiveScanner: function(t) {
            var i = this.map(function(e) {
                return e.get("name")
            });
            return !!e.intersection(i, t).length
        }
    });
    return n
});