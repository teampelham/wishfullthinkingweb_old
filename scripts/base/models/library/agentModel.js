define([
	"underscore", 
	"jquery", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, $, parser, baseModel) {
    "use strict";
    
	var agentModel = baseModel.extend({
        idAttribute: "identifier",
        whitelistedAttrs: ["identifier", "name", "enabled", "hasPrefs", "language"],
        
		convert: function(agentObj) {
            var agent = _.isObject(agentObj) ? agentObj : parser.parseXML(agentObj);
            parser.parseBoolean(agent, ["enabled", "hasPrefs"]); 
			parser.parseArray(agent, ["Language"]);
			return agent;
        },
        
		parse: function(agentObj) {
            var agent = this.convert(agentObj);
            agent.language = _.pluck(agent.Language, "code"); 
			agent = _.pick(agent, this.whitelistedAttrs);
        },
        
		fetchAttribution: function() {
            var xhrOptions = this.server.getXHROptions("/system/agents/attribution?identifier=" + this.id);
            var xhr = $.ajax(xhrOptions);
            xhr.done(_.bind(function(result) {
                this.set("attribution", result);
            }, this));
			
			return xhr;
        }
    });
	
    return agentModel;
});