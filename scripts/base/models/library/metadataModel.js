define([
	"require", 
	"underscore", 
	"jquery", 
	"base/utils/t", 
	"base/utils/parser", 
	"base/utils/mediaUtil", 
	//"base/utils/types/mediaTypes", 
	"base/utils/types/streamTypes", 
	"base/utils/formatters/stringUtil", 
	//"base/utils/constants/timelineEvents", 
	"base/models/baseModel", 
    "base/utils/types/automationProtocolTypes"
	//"base/models/library/OverlayCollection", 
	//"base/models/library/ConcertCollection"
], function(require, _, $, t, parser, mediaUtil, streamTypes, stringUtil, baseModel, automationProtocolTypes) {
    "use strict";
    
	var metadataModel = baseModel.extend({
        idAttribute: "ratingKey",
        defaults: {
            title: "",
            titleSort: "",
            tagline: "",
            rating: 0,
            summary: "",
            year: "",
            studio: "",
            contentRating: "",
            originallyAvailableAt: "",
            originalTitle: "",
            thumb: "",
            parentThumb: "",
            grandparentThumb: "",
            banner: "",
            art: "",
            parentArt: "",
            grandparentArt: "",
            primaryExtraKey: ""
        },
        omittedSaveAttrs: ["updatedAt", "thumb", "banner", "art", "Media", "Location"],
        tagAttrs: ["Collection", "Director", "Writer", "Genre", "Mood", "Country", "Label", "Similar"],
        subcollections: ["Media", "Genre", "Collection", "Field", "Writer", "Director", "Role", "Country", "Location", "Label"],
        metadataElementNames: ["Directory", "Video", "Track", "Photo", "Hub"],
        whitelistedSaveAttrs: ["automatedDeviceID", "title", "location", "address", "hardware", "dimmable", "protocolType", "deviceType"],
        
		url: function() {
            return this.get("key");
        },
        
        getUrl: function() {
            return "/device/setdevicestate?deviceID=" + this.get("automatedDeviceID");
        },
        
        saveUrl: function() {
            return "/device/updatedevice";
        },
        
        isNew: function() {
            return !this.has("automatedDeviceID");
        },
        
        setNewOnLevel: function(e) {
            if (_.isString(e))
                e = this.parse(e);
                
            if(!_.isUndefined(e.OnLevel) && this.get("type") === "light") {
                this.set("OnLevel", e.OnLevel);  
                this.trigger("change:onLevel", e.OnLevel);
            }
        },
		
        urlRoot: function() {
            return this.get("key");
        },
		
        firstItemUrl: function() {
            return this.url();
        },
		
        singleMetadataModel: function() {
            return this;
        },
		
        cacheKey: function(key) {
            if (key.identifier && !this.isLibraryItem(key)) 
                key = this.normalizeKey(key);
                
            var childKey = this.normalizeChildrenKey(key.key);
            return this.server && this.server.getScopedKey(childKey.key);
        },
		
        preserveCachedAttributes: function(e) {
            return /\/(children|library\/sections\/\d+\/\w+)(\?.*)?$/.test(e);
        },
		
        initialize: function() {
            this.listenTo(this, "socket:timeline", function(e) {
                e.state === c.PROCESSED ? (this.fetch({
                    data: {
                        includeExtras: "1",
                        includeRelated: "1",
                        includeRelatedCount: "5",
                        includeConcerts: "1",
                        includeOnDeck: "1"
                    }
                }), this.refreshChildren()) : e.state === c.DELETED && this.trigger("destroy", this, this.collection)
            })
        },
		
        convert: function(metaObj) {
            if (_.isString(metaObj)) 
                metaObj = parser.replaceXMLElementNames(metaObj, this.metadataElementNames, "Metadata");
                
            var meta = _.isObject(metaObj) ? metaObj : parser.parseXML(metaObj);
            
            if (!meta.ratingKey && !meta.key && !meta.id) {
                var medadata = meta.Metadata || meta;
                medadata.identifier = meta.identifier; 
                medadata.mediaTagPrefix = meta.mediaTagPrefix; 
                medadata.mediaTagVersion = meta.mediaTagVersion; 
                medadata.allowSync = meta.allowSync; 
                medadata.librarySectionID = meta.librarySectionID; 
                medadata.librarySectionUUID = meta.librarySectionUUID; 
                meta = medadata;
            }
            
            parser.parseBoolean(meta, ["allowSync", "selected"]); 
            parser.parseInteger(meta, ["index", "automatedDeviceID", "parentIndex", "absoluteIndex", "duration", "viewOffset", "viewCount", "leafCount", "viewedLeafCount", "extraType", "lastViewedAt", "addedAt", "updatedAt"]); 
            parser.parseFloat(meta, ["rating", "userRating"]); 
            parser.parseArray(meta, this.subcollections);
            var medias = meta.Media;
            
            _.each(medias, function(media) {
                parser.parseBoolean(media, ["indirect", "optimizedForStreaming", "premium"]); 
                parser.parseInteger(media, ["id", "duration", "width", "height", "bitrate", "audioChannels"]); 
                parser.parseArray(media, "Part");
                var mediaParts = media.Part;
                
                _.each(mediaParts, function(mediaPart) {
                    parser.parseBoolean(mediaPart, "optimizedForStreaming"); 
                    parser.parseInteger(mediaPart, ["id", "duration", "size"]); 
                    parser.parseEncodedString(mediaPart, "file"); 
                    parser.parseArray(mediaPart, "Stream"); 
                    mediaPart.exists = "0" !== mediaPart.exists; 
                    mediaPart.accessible = "0" !== mediaPart.accessible;
                    var streams = mediaPart.Stream;
                    _.each(streams, function(stream) {
                        parser.parseBoolean(stream, "selected"); 
                        parser.parseInteger(stream, ["id", "index", "streamType", "duration", "width", "height", "bitrate", "bitDepth", "samplingRate", "channels"]);
                    })
                })
            });
            
            return meta;
        },
		
        parse: function(metaObj) {
            var metadata = this.convert(metaObj);
            
            if (!metadata.key) 
                return metadata;
                
            if (!this.isLibraryItem(metadata)) 
                metadata = this.normalize(metadata); 
            
            if ("photo" === metadata.type && /\/children$/.test(metadata.key)) 
                metadata.type = "photoAlbum"; 
            
            if ("photoalbum" === metadata.type) 
                metadata.type = "photoAlbum"; 
            
            metadata.isFolder = /\/folder\?parent\=/.test(metadata.key); 
            metadata.isExtra = !! metadata.extraType;
            var metadataCollection = require("base/models/library/metadataCollection");
            //var hubCollection = require("base/models/library/hubCollection");
            
            this.parseChildren(metadata, metadataCollection); 
            //this.parseExtras(metadata, metadataCollection); 
            //this.parseRelated(metadata, hubCollection); 
            //this.parseOverlays(metadata); 
            //this.parsePopularLeaves(metadata, metadataCollection); 
            //this.parseConcerts(metadata, h); 
            this.parseOnDeck(metadata); 
            
            if (null != metadata.viewedLeafCount)
                metadata.unviewedLeafCount = metadata.leafCount - metadata.viewedLeafCount;
            
            var medias = metadata.Media;
            var subTitles = streamTypes.getStreamTypeByString("subtitles");
            
            _.each(medias, function(media) {
                var part = _.first(media.Part);
                
                if (part && part.Stream) {
                    var stream = part.Stream;
                    var subTitleStream = mediaUtil.getStreams(media, {
                        streamType: subTitles.id,
                        selected: true
                    });
                    var streamObj = {
                        id: 0,
                        streamType: subTitles.id,
                        language: t("None")
                    };
                    
                    if (!subTitleStream.length) 
                        streamObj.selected = true;
                         
                    stream.unshift(streamObj);
                }
            }); 
            
            _.defaults(metadata, this.defaults);
            return metadata;
        },
		
        parseChildren: function(meta, MetadataCollection) {
            var normalChild = this.normalizeChildrenKey(meta.key);
            
            if (normalChild.hasChildren) {
                meta.key = normalChild.key;
                if (!this.get("children")) {
                    var prevKey = normalChild.previousKey;
                    if ("album" === meta.type) {
                        prevKey += prevKey.indexOf("?") > -1 ? "&" : "?"; 
                        prevKey += "includeRelated=1";
                    }
                    var metaCollection = new MetadataCollection([], {
                        server: this.server,
                        section: this.section,
                        parent: this,
                        url: prevKey
                    });
                    this.set("children", metaCollection);
                }
            }
        },
		
        parsePopularLeaves: function(e, t) {
            console.warn('parseLeaves');
            /*if (!e.PopularLeaves) return void this.unset("popularLeaves");
            var i = this.get("popularLeaves"),
                n = [],
                s = !1;
            e.PopularLeaves.size > 0 && (n = e.PopularLeaves, s = !0), i ? i.set(n, {
                parse: s
            }) : (i = new t(n, {
                server: this.server,
                section: this.section,
                parent: this,
                url: e.PopularLeaves.key,
                populated: !0,
                parse: s
            }), this.set("popularLeaves", i)), delete e.PopularLeaves*/
        },
        
        parseExtras: function(e, t) {
            console.warn('parseExtras');
            /*if (!e.Extras) return void this.unset("extras");
            var i, n, s = this.get("extras");
            e.Extras.size > 0 ? (i = e.Extras, n = !0) : (i = [], n = !1), s ? s.set(i, {
                parse: n
            }) : (s = new t(i, {
                server: this.server,
                section: this.section,
                parent: this,
                url: e.key + "/extras",
                populated: !0,
                parse: n
            }), this.set("extras", s)), delete e.Extras*/
        },
        
        parseRelated: function(item, HubCollection) {
            if (!item.Related) 
                return void this.unset("related");
                
            var relatedItems, isMeta; 
            var related = this.get("related");
            
            if (item.Related.Metadata) {
                relatedItems = item.Related; 
                isMeta = true;
            }
            else {
                relatedItems = []; 
                isMeta = false;
            }
            
            if (related) 
                related.reset(relatedItems, {
                    parse: isMeta
                }); 
            else {
                related = new HubCollection(relatedItems, {
                    server: this.server,
                    section: this.section,
                    parent: this,
                    populated: true,
                    parse: isMeta
                }); 
                this.set("related", related);
            } 
            delete item.Related;
        },
        
        parseOverlays: function(e) {
            console.warn('parseRelated');
            /*if (!e.Overlay) return void this.unset("overlays");
            var i = this.get("overlays"),
                n = t.isArray(e.Overlay) ? e.Overlay : [e.Overlay],
                s = !0;
            i ? i.reset(n, {
                parse: s
            }) : (i = new u(n, {
                server: this.server,
                section: this.section,
                parent: this,
                populated: !0,
                parse: s
            }), this.set("overlays", i)), delete e.Overlay*/
        },
        
        parseConcerts: function(e) {
            console.warn('parseRelated');
            /*if (!e.Concert) return void this.unset("concerts");
            var i = this.get("concerts"),
                n = t.isArray(e.Concert) ? e.Concert : [e.Concert],
                s = !0;
            i ? i.reset(n, {
                parse: s
            }) : (i = new h(n, {
                server: this.server,
                section: this.section,
                parent: this,
                populated: !0,
                parse: s
            }), this.set("concerts", i)), delete e.Concert*/
        },
        
        parseOnDeck: function(item) {
            if (!item.OnDeck) 
                return void this.unset("onDeck");
                
            var onDeck = this.get("onDeck");
            var onDeckMeta = item.OnDeck.Metadata;
            
            if (onDeck) 
                onDeck.set(onDeckMeta, {
                    parse: true
                }); 
            else {
                onDeck = new metadataModel(onDeckMeta, {
                    server: this.server,
                    section: this.section,
                    parse: true
                }); 
                this.set("onDeck", onDeck);
            }
            delete item.OnDeck;
        },
        
        normalize: function(e) {
            console.warn('parseRelated');
            //return e = this.normalizeKey(e), e = this.normalizeType(e), e = this.normalizeMetadata(e)
        },
        
        normalizeChildrenKey: function(childKey) {
            var keyRegex = /(.*)\/children$/.exec(childKey);
            var prevKey = childKey;
            
            if (keyRegex) 
                childKey = keyRegex[1]; 
            
            return {
                key: childKey,
                previousKey: prevKey,
                hasChildren: !! keyRegex
            };
        },
        
        normalizeKey: function(key) {
            var keySplit = key.key.split("/");
            if (1 === keySplit.length && key.containerKey) {
                key.hasInvalidKey = true; 
                key.originalKey = key.key; 
                key.key = key.containerKey + "/" + key.key;
            } 
            
            if (!key.ratingKey) 
                key.ratingKey = key.originalKey || key.key;
            
            return key;
        },
        
        normalizeType: function(e) {
            console.warn('parseRelated');
            //return e.mediaType && !e.type && (e.type = e.mediaType, delete e.mediaType), e.elementName && !e.type && (e.type = e.elementName.toLowerCase()), /image/i.test(e.type) ? e.type = "photo" : /(movie|episode|video)/i.test(e.type) && (e.type = "clip"), r.getMediaTypeByString(e.type) || //(e.type = "channelDirectory"), e
        },
        
        normalizeMetadata: function(e) {
            console.warn('parseRelated');
            //return e.label ? (e.title = e.label, delete e.label) : e.artist && (e.track ? (e.title = e.track, e.originalTitle = e.artist, e.parentTitle = e.album, e.grandparentTitle = e.albumArtist) : e.album ? (e.title = e.album, e.parentTitle = e.artist) : e.title = e.artist, delete e.track, //delete e.album, delete e.albumArtist, delete e.artist), e.totalTime && (e.duration = e.totalTime, delete e.totalTime), e
        },
        
        refreshChildren: function() {
            console.warn('parseRelated');
            /*var e = this.get("children");
            if (e && e.populatePromise) {
                var t = e.fetch();
                t.always(function() {
                    e.invoke("refreshChildren")
                })
            }*/
        },
        
        resolveIndirect: function(e, n) {
            console.warn('parseRelated');
            /*var s = i.Deferred();
            if (!e || !n) return s.reject(this).promise();
            if (!e.indirect) return s.resolve(this).promise();
            if (n.postURL) return this.resolvePostUrl(e, n);
            var a = n.key;
            this.server.get("cloud") && (a = this.server.resolveUrl(n.key, {
                connection: {
                    scheme: "https",
                    address: "node.plexapp.com",
                    port: 32443
                }
            }));
            var r = i.ajax(this.server.getXHROptions(a));
            return r.done(t.bind(function(t) {
                this.mergeIndirectResponse(t, e, n) ? s.resolve(this) : s.reject(this)
            }, this)), r.fail(t.bind(function() {
                s.reject(this)
            }, this)), s.promise()*/
        },
        
        resolvePostUrl: function(e, n) {
            console.warn('parseRelated');
            /*var s = i.Deferred(),
                a = this.server.get("cloud") || this.server.get("synced"),
                r = a ? this.server.parent.get("primaryServer") : this.server;
            if (!r) return s.reject(this).promise();
            var o = r.getXHROptions("/system/proxy");
            o.headers = o.headers || {}, o.headers["X-Plex-Url"] = n.postURL;
            var l = i.ajax(o);
            return l = l.then(t.bind(function(e, t, s) {
                var a = n.key + "&postURL=" + encodeURIComponent(n.postURL),
                    r = s.getAllResponseHeaders() + "\r\n" + e;
                this.server.get("cloud") && (a = this.server.resolveUrl(a, {
                    connection: {
                        scheme: "https",
                        address: "node.plexapp.com",
                        port: 32443
                    }
                }));
                var o = this.server.getXHROptions(a);
                return o.type = "POST", o.data = r, i.ajax(o)
            }, this)), l.done(t.bind(function(t) {
                this.mergeIndirectResponse(t, e, n) ? s.resolve(this) : s.reject(this)
            }, this)), l.fail(t.bind(function() {
                s.reject(this)
            }, this)), s.promise()*/
        },
        
        mergeIndirectResponse: function(e, i, n) {
            console.warn('parseRelated');
            /*var a = s.parseXML(e);
            if (!(a && a.Video && a.Video.Media && a.Video.Media.Part)) return !1;
            var r = this.parse(a.Video),
                o = t.first(r.Media),
                l = t.first(o.Part);
            if (o.Part.length > 1) {
                var c = t.indexOf(i.Part, n);
                c < o.Part.length && (l = o.Part[c])
            }
            if (!o || !l) return !1;
            if (o.indirect = !1, t.extend(i, t.omit(o, "Part")), t.extend(n, l), a.httpHeaders) {
                for (var d, u = new RegExp("([^=]+)=([^&]+)&?", "g"), h = {}; d = u.exec(a.httpHeaders);) d[1] in h ? (t.isArray(h[d[1]]) || (h[d[1]] = [h[d[1]]]), h[d[1]].push(d[2])) : h[d[1]] = d[2];
                this.set("indirectHttpHeaders", h, {
                    silent: !0
                })
            }
            return !0*/
        },
        
        getSaveParams: function() {
            var saveParams = baseModel.prototype.getSaveParams.apply(this, arguments);
            saveParams = _.pick(saveParams, this.whitelistedSaveAttrs);
            
            if ("protocolType" in saveParams) {
                var protocolType = automationProtocolTypes.getAutomationTypeByID(saveParams.protocolType) || automationProtocolTypes.getSectionTypeByString(saveParams.protocolType);
                if (protocolType)
                    saveParams.AutomationProtocolID = protocolType.typeID;
                delete saveParams.protocolType;
            }
            
            if ("active" in saveParams) {
                saveParams.Active = !!saveParams.active;
                delete saveParams.active;
            }
            else {
                saveParams.Active = true;
            }
            
            if ("deviceType" in saveParams) {
                saveParams.DeviceTypeID = saveParams.deviceType;
                delete saveParams.deviceType;
            }
            
            if ("automatedDeviceID" in saveParams) {
                saveParams.DeviceID = saveParams.automatedDeviceID;
                delete saveParams.automatedDeviceID;
            }
            
            if ("title" in saveParams) {
                saveParams.DeviceName = saveParams.title;
                delete saveParams.title;
            }
            
            if (_.isObject(saveParams.type))
                saveParams.type = saveParams.type.typeString;
            
            if ("location" in saveParams) {
                saveParams.LocationID = saveParams.location;
                delete saveParams.location;
            }
            
            if ("address" in saveParams) {
                saveParams.DeviceAddress = saveParams.address;
                delete saveParams.address;
            }
            
            if ("hardware" in saveParams) {
                saveParams.HardwareID = saveParams.hardware;
                delete saveParams.hardware;
            }
            
            if ("dimmable" in saveParams) {
                saveParams.Dimmable = !!saveParams.dimmable;
                delete saveParams.dimmable;
            }
            
            return { deviceData: JSON.stringify(saveParams) };
        },
        
        /*getSaveParams: function(changes) {
            changes = changes || {};
            _.each(changes.Field, function(field) {
               changes[field.name + ".locked"] = field.locked; 
            });
            
            delete changes.Field;
            _.each(this.tagAttrs, function(tag) {
                if (tag in changes) {
                    if (_.isEmpty(changes[tag]))
                        changes[stringUtil.lowerCaseFirstLetter(tag) + "[]"] = "";
                    else
                        _.each(changes[tag], function(item, idx) {
                            changes[stringUtil.lowerCaseFirstLetter(tag) + "[" + idx + "].tag.tag"] = item.tag;
                        });
                    delete changes[tag];
                }            
            });
            
            return changes;
        },*/
        
        batchAdd: function(e, n, s) {
            console.warn('parseRelated');
            /*var a = t.map(e, function(e) {
                return e.get("ratingKey")
            }),
                r = "/library/metadata/" + a.join(",") + "/" + n + "/add?tag=" + encodeURIComponent(s),
                o = t.extend(this.server.getXHROptions(r), {
                    type: "PUT"
                });
            return i.ajax(o)*/
        },
        
        getDownloadUrl: function() {
            console.warn('parseRelated');
            /*var e = t.first(this.get("Media"));
            if (e) {
                var i = t.first(e.Part);
                if (i && i.key) return this.server.resolveUrl(i.key, {
                    appendToken: !0
                })
            }
            return ""*/
        },
        
        getFilepath: function() {
            console.warn('parseRelated');
            /*var e = t.first(this.get("Media"));
            if (e) {
                var i = t.first(e.Part);
                if (i) return i.file
            }
            return null*/
        },
        
        getLocation: function() {
            console.warn('parseRelated');
            //var e = t.first(this.get("Location"));
            //return e ? e.path : this.getFilepath()
        },
        
        getMediaKey: function() {
            console.warn('parseRelated');
            /*var e = t.first(this.get("Media")),
                i = this.get("key");
            if (e) {
                var n = t.first(e.Part);
                if (n) return n.key
            }
            return i*/
        },
        
        isLibraryItem: function(item) {
            var itemID = item ? item.identifier : this.get("identifier");
            if (itemID) 
                return "com.plexapp.plugins.library" === itemID;
            var itemKey = item ? item.key : this.get("key");
            
            return /^(\/sync\/[A-Za-z0-9-\/]*)?\/library\//.test(itemKey);
        },
        
        isPlexMusicItem: function() {
            console.warn('parseRelated');
            //var e = this.section;
            //return e && "com.plexapp.agents.plexmusic" === e.get("agent")
        }
    });
    
    return metadataModel;
});