define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel", 
	"base/models/library/metadataCollection"
], function(_, parser, baseModel, MetadataCollection) {
    "use strict";
	
    var hubModel = baseModel.extend({
        idAttribute: "hubIdentifier",
        whitelistedAttrs: ["key", "hubIdentifier", "title", "type", "size", "more", "browse"],
        
		url: function() {
            return this.get("key");
        },
        
		convert: function(hubObj) {
            var hub = _.isObject(hubObj) ? hubObj : parser.parseXML(hubObj);
            parser.parseInteger(hub, "size"); 
			parser.parseBoolean(hub, ["more", "browse"]);
			
			return hub;
        },
		
        parse: function(hubObj) {
            var hub = this.convert(hubObj);
            var children = this.get("children");
            
			if (!children) {
				children = new MetadataCollection([], {
					server: this.server,
					section: this.section,
					parent: this,
					url: hub.key
				}); 
				this.set("children", children);
			} 
			
			s.set(hubObj, {
                parse: true,
                silent: true
            }); 
			
			hub = _.pick(hub, this.whitelistedAttrs);
			return hub;
        }
    });
	
    return hubModel;
});