define([
	"underscore", 
	"base/models/baseCollection", 
	"base/models/library/agentModel"
], function(_, baseCollection, agentModel) {
    "use strict";
	
    var agentCollection = baseCollection.extend({
        model: agentModel,
        
		url: function() {
            if (this.primaryAgent) 
				return "web/systemagents/contributors?mediaType=" + this.mediaTypeID + "&primaryAgent=" + this.primaryAgent; 
			
			if (this.relatedAgent) 
				return "web/systemagents/" + this.relatedAgent + "/config/" + this.mediaTypeID; 
			
			if (this.mediaTypeID) 
				return "web/systemagents?mediaType=" + this.mediaTypeID;
				
			return "web/systemagents";
        },
        
		initialize: function(e, initObj) {
            this.primaryAgent = initObj.primaryAgent; 
			this.relatedAgent = initObj.relatedAgent; 
			this.sectionType = initObj.sectionType; 
			this.mediaTypeID = this.sectionType ? this.sectionType.typeID : initObj.mediaTypeID;
        },
        
		parse: function(agentObj) {
            return this.convert(agentObj, "Agent");
        },
        
		hasExclusiveAgent: function(t) {
			console.warn('what');
            /*var i = this.map(function(e) {
                return e.get("identifier")
            });
            return !!e.intersection(i, t).length*/
        }
    });
	
    return agentCollection;
});