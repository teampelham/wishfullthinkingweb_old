define(["base/models/baseModel"], function(e) {
    "use strict";
    var t = e.extend({
        idAttribute: "name"
    });
    return t
});