define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"], function(e, t, i) {
    "use strict";
    var n = i.extend({
        whitelistedAttrs: ["id", "tag", "venue", "city", "country", "url", "at"],
        convert: function(i) {
            var n = e.isObject(i) ? i : t.parseXML(i);
            return n
        },
        parse: function(i) {
            var n = this.convert(i);
            return n = e.pick(n, this.whitelistedAttrs), t.parseInteger(n, ["id", "at"]), n
        }
    });
    return n
})