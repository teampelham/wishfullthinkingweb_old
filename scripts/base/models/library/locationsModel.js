define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel", 
], function(_, parser, baseModel) {
    "use strict";
	
    var locationsModel = baseModel.extend({
        idAttribute: "LocationID",
        whitelistedAttrs: ["key", "title", "childLocations"],
        
		convert: function(locationObj) {
            var location = _.isObject(locationObj) ? locationObj : parser.parseXML(locationObj);
            
			parser.parseInteger(location, "LocationID");
			location.key = location.LocationID;
			location.title = location.Title;
			location.childLocations = location.ChildLocations;
			
			_.each(location.childLocations, function(child) {
				this.convert(child);
			}, this);
			
			return location;
        },
		
        parse: function(locationObj) {
            var location = this.convert(locationObj);
			
			location = _.pick(location, this.whitelistedAttrs);
			return location;
        }
    });
	
    return locationsModel;
});