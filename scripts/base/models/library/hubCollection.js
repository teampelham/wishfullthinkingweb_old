define([
	"underscore", 
	"base/utils/parser", 
	"base/utils/formatters/stringUtil", 
	"base/models/baseCollection", 
	"base/models/library/hubModel"
], function(e, t, i, n, s) {
    "use strict";
    var a = n.extend({
        model: s,
        subcollections: ["Playlist", "Directory", "Video", "Track", "Photo", "Hub"],
        url: function() {
            var t = "/hubs";
            if (this.section && (t += "/sections/" + this.section.id), this.excludes && this.excludes.length > 0) {
                var n = e.map(this.excludes, function(e) {
                    return "exclude" + i.upperCaseFirstLetter(e) + "=1"
                });
                t += "?" + n.join("&")
            }
            return t
        },
        initialize: function(e, t) {
            this.excludes = t && t.excludes
        },
        populate: function() {
            return this.populatePromise && "pending" !== this.populatePromise.state() ? this.fetch() : n.prototype.populate.apply(this, arguments)
        },
        fetch: function(t) {
            t || (t = {});
            var i = e.clone(t);
            if (this.populatePromise && "resolved" === this.populatePromise.state()) {
                i.remove = !1;
                var s = e.result(this, "url");
                s += s.indexOf("?") > -1 ? "&" : "?", s += "onlyTransient=1", i.url = s
            }
            return n.prototype.fetch.call(this, i)
        },
        parse: function(i) {
            return e.isString(i) && (i = t.replaceXMLElementNames(i, this.subcollections, "Metadata")), this.convert(i, "Metadata")
        }
    });
    return a
})