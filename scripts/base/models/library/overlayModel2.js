define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(e, t, i) {
    "use strict";
    var n = i.extend({
        whitelistedAttrs: ["key", "marginTop", "marginRight", "marginBottom", "marginLeft", "width", "height", "alignHorizontal", "alignVertical", "relative", "imageScaling", "marginScaling"],
        convert: function(i) {
            var n = e.isObject(i) ? i : t.parseXML(i);
            return n
        },
        parse: function(i) {
            var n = this.convert(i);
            return n = e.pick(n, this.whitelistedAttrs), t.parseInteger(n, ["marginTop", "marginRight", "marginBottom", "marginLeft", "width", "height"]), n
        }
    });
    return n
})