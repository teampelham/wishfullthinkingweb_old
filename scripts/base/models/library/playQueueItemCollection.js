define([
	"underscore", 
	"base/models/baseCollection", 
	"base/models/library/playQueueItemModel"
], function(e, t, i) {
    "use strict";
    var n = t.extend({
        model: i,
        parse: function(i) {
            return e.extend(this, e.pick(i, ["totalSize", "availableRange"])), t.prototype.parse.apply(this, arguments)
        }
    });
    return n
})