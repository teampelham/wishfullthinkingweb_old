define([
	"underscore", 
	"base/utils/types/mediaTypes", 
	"base/models/library/metadataCollection2"
], function(_, mediaTypes, metadataCollection) {
    "use strict";
	
    var sectionMetadataCollection = metadataCollection.extend({
        pageStart: 0,
        pageSize: 0,
		
        url: function() {
            var folderKey = this.section.get("currentFolderKey");
            if (folderKey) 
				return folderKey;
            
			var sectionUrl = _.result(this.section, "url");
            var subset = this.section.get("currentSubset");
            var subsetId = subset.id;
			
            if (!this.section.get("allowAdvancedFilters")) {
                var filters = this.section.get("currentFilters");
                return filters.length ? filters[0].values[0] : sectionUrl + "/" + subsetId;
            }
			
			if ("recentlyAdded" === subsetId || "newest" === subsetId) 
				subsetId = "all";
            
			var url = sectionUrl + "/" + subsetId;
            var params = this.getUrlParams();
			
            if (params.length) {
				url += url.indexOf("?") > -1 ? "&" : "?"; 
				url += params;
			} 
			
			return url;
        },
        
		initialize: function() {
            metadataCollection.prototype.initialize.apply(this, arguments); 
			if (this.server.get("synced")) {
				this.pageStart = null;
				this.pageSize = null;
			}
        },
        
		clear: function() {
            if (this.server.get("synced")) {
				this.pageStart = null; 
				this.pageSize = null;
			}
			else {
				this.pageStart = 0; 
				this.pageSize = 0;
			} 
			metadataCollection.prototype.clear.apply(this, arguments);
        },
        
		getUrlParams: function() {
            var filterParams = this.getFilterUrlParams({
                sendType: true
            });
            
			if (this.section.get("refreshing") && this.server.supports("timelineState")) 
				filterParams.push("timelineState=1"); 
				
			return filterParams.length ? filterParams.join("&") : "";
        },
        
		getFilterUrlParams: function(initObj) {
			initObj = initObj || {};
            
            var leaves = {}, filterParams = [];
            var mediaTypeId = this.getMediaType().id;
            
            if (initObj.sendType && mediaTypeId && mediaTypeId !== mediaTypes.getMediaTypeByString("photo").id)
                leaves.type = mediaTypeId;
                
            var currentFilters = this.section.get("currentFilters");
            
            _.each(currentFilters, function(filter) {
                if (filter.values.length) 
                    leaves[filter.key] = filter.values.join(",");
            });
            
            var sort = this.getSort();
            if (sort) 
                leaves.sort = sort;
            
            for (var l in leaves) 
                filterParams.push(("unviewedLeafCount" === l) ? l + ">=" + leaves[l] : l + "=" + leaves[l]);
            
            return filterParams;
        },
        
		getMediaType: function() {
            var currentType = this.section.get("currentType");
            var currentSubset = this.section.get("currentSubset");
            
            if (!currentType) 
                currentType = mediaTypes.getMediaTypeByString(this.section.get("type"));
            
            var subsetId = currentSubset.id;
            
            if (/^(onDeck|recentlyAdded|recentlyViewed|newest)$/.test(subsetId)) {
                var typeId = currentType.id;
                if (typeId === mediaTypes.getMediaTypeByString("show").id || typeId === mediaTypes.getMediaTypeByString("season").id) 
                    return mediaTypes.getMediaTypeByString("episode");
                    
                if (typeId === mediaTypes.getMediaTypeByString("artist").id || typeId === mediaTypes.getMediaTypeByString("track").id) 
                    return mediaTypes.getMediaTypeByString("album")
            } 
            else if (/^recentlyViewedShows$/.test(subsetId)) 
                return mediaTypes.getMediaTypeByString("show");
            
            if (this.parent && "season" === this.parent.get("type")) 
                return mediaTypes.getMediaTypeByString("episode"); 
            
            return currentType;
        },
        
		getSort: function() {
            var currentSort = this.section.get("currentSort");
            var currentSubset = this.section.get("currentSubset").id;
            
            if ("recentlyAdded" === currentSubset)
                return "addedAt:desc";
            else if ("newest" === currentSubset) 
                return "originallyAvailableAt:desc";
            else if (currentSort) 
                return currentSort.id + ":" + currentSort.get("direction"); 
                
            return false;
        }
    });
	
    return sectionMetadataCollection;
});