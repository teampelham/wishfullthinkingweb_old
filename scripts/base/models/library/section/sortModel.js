define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, parser, baseModel) {
    "use strict";
    
	var sortEnum = {
        ASCENDING: "asc",
        DESCENDING: "desc"
    };
	
	var sortModel = baseModel.extend({
		idAttribute: "key",
		defaults: {
			selected: false
		},
		whitelistedAttrs: ["key", "title", "isDefault", "defaultDirection"],
		
		convert: function(sortObj) {
			var sort = _.isObject(sortObj) ? sortObj : parser.parseXML(sortObj);
			return sort;
		},
		
		parse: function(sortObj) {
			var sort = this.convert(sortObj);
			if (!sort.defaultDirection) 
				sort.defaultDirection = sort["default"] || sortEnum.ASCENDING; 
			
			sort.isDefault = !! sort["default"]; 
			sort = _.pick(sort, this.whitelistedAttrs); 
			if (!sort.direction) 
				sort.direction = sort.defaultDirection;
			
			return sort;
		},
		
		reverseSortDirection: function() {
			var direction = this.get("direction") || this.get("defaultDirection");
			direction = direction && direction !== sortEnum.ASCENDING ? sortEnum.ASCENDING : sortEnum.DESCENDING; 
			this.set("direction", direction);
		}
	});
	
    sortModel.directions = sortEnum;
	return sortModel;
});