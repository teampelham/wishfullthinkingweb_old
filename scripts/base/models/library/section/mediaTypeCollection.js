define([
	"base/utils/types/mediaTypes", 
	"base/models/baseCollection", 
	"base/models/library/section/mediaTypeModel"
], function(mediaTypes, baseCollection, mediaTypeModel) {
    "use strict";
	
    var mediaTypeCollection = baseCollection.extend({
        model: mediaTypeModel,
        
		initialize: function(e, media) {
            var section = media.section;
            this.updateMediaTypes(section, section.get("type")); 
			this.listenTo(section, "change:type", this.updateMediaTypes);
        },
        
		updateMediaTypes: function(t, i) {
            var relatedTypes = mediaTypes.getRelatedMediaTypes(i);
            this.reset(relatedTypes, {
                parse: true
            });
        }
    });
	
    return mediaTypeCollection;
});