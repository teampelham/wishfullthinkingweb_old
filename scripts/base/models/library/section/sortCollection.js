define([
	"underscore", 
	"jquery", 
	"base/models/baseCollection", 
	"base/models/library/section/sortModel"
], function(_, $, baseCollection, sortModel) {
    "use strict";
	
    var sortCollection = baseCollection.extend({
        model: sortModel,
        
		url: function() {
            var url = _.result(this.section, "url") + "/sort";   
            var typeID = this.typeID || this.section.get("currentType").id;
            var typeObj = typeID ? {
				type: typeID
			} : {};
			
            return url + "?" + $.param(typeObj, true);
        },
        
		parse: function(sort) {
            return this.convert(sort, "Directory")
        }
    });
	
    return sortCollection;
});