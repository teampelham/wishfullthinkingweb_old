define([
	"underscore", 
	"base/utils/parser", 
	"base/utils/types/sectionTypes", 
	"base/utils/types/subsetTypes", 
	"base/models/baseModel", 
	"base/models/library/section/sectionSubsetModel", 
	"base/models/library/section/sectionSubsetCollection", 
	"base/models/library/section/mediaTypeCollection", 
	"base/models/library/section/filterCollection", 
	"base/models/library/section/linearFilterCollection", // c
	"base/models/library/section/sortCollection",        // d
	"base/models/library/section/alphanumericCollection",    // u
	"base/models/library/sectionMetadataCollection"  // h
], function(_, parser, sectionTypes, subsetTypes, baseModel, SectionSubsetModel, SectionSubsetCollection, MediaTypeCollection, FilterCollection, 
    LinearFilterCollection, SortCollection, AlphanumericCollection, SectionMetadataCollection) {
    "use strict";
    var sectionModel = baseModel.extend({
        idAttribute: "key",
        persistInCache: true,
		
        defaults: function() {
            return {
                isSection: true,
                currentType: null,
                currentFilters: [],
                currentSort: null,
                currentFirstCharacter: null,
                currentFolderKey: null
            }
        },
		
        url: function() {
            if (this.isNew()) 
                return _.result(this, "urlRoot") || _.result(this.collection, "url");
                
            var nonNumeric = _.isNaN(parseInt(this.id, 10));
            var url = nonNumeric ? this.id : "/web/sections/" + this.id;
            return url;
        },
        
        whitelistedAttrs: ["key", "uuid", "title", "type", "thumb", "art", "language", "agent", "scanner", "createdAt", "updatedAt", "allowAdvancedFilters", "allowSync", "refreshing", "Location"],
        whitelistedSaveAttrs: ["name", "title", "type", "agent", "scanner", "language", "importFromiTunes", "Location"],
        
		cacheKey: function(item) {
            if (this.server) 
                this.server.getScopedKey(item);
        },
		
        initialize: function() {
            var sectionInit = {
                server: this.server,
                section: this
            }; 
            var Filters = this.get("allowAdvancedFilters") ? FilterCollection : LinearFilterCollection;
            var mediaTypes = new MediaTypeCollection(null, sectionInit);
            var sectionMeta = new SectionMetadataCollection([], sectionInit);
            
            this.set({
                types: mediaTypes,
                items: sectionMeta,
                currentSubset: new SectionSubsetModel(subsetTypes.getSubsetType("all"), sectionInit),
                subsets: new SectionSubsetCollection([], sectionInit),
                filters: new Filters([], sectionInit),
                sorts: new SortCollection([], sectionInit),
                alphanumeric: new AlphanumericCollection([], sectionInit),
                currentType: mediaTypes.at(0)
            }); 
            
            this.server.get("shared") || this.listenTo(sectionMeta, "change:allowSync", function(e, t) {
                this.set("allowSync", t);
            });
        },
		
        convert: function(sectionObj) {
            var section = _.isObject(sectionObj) ? sectionObj : parser.parseXML(sectionObj);
            parser.parseBoolean(section, ["filters", "allowAdvancedFilters", "allowSync", "refreshing"]); 
            parser.parseInteger(section, ["createdAt", "updatedAt"]); 
            parser.parseArray(section, "Location"); 
            return section;
        },
		
        parse: function(sectionObj) {
            var section = this.convert(sectionObj);
            section.allowAdvancedFilters = section.filters; 
            section = _.pick(section, this.whitelistedAttrs);
            return section;
        },
		
        changedAttributes: function() {
            var changedAttrs = baseModel.prototype.changedAttributes.apply(this, arguments);
            if (changedAttrs) 
                changedAttrs = _.pick(changedAttrs, this.whitelistedSaveAttrs);
                
            return changedAttrs;
        },
		
        getSectionType: function() {
            var modelType = this.get("type");
            return sectionTypes.getSectionType(modelType) || sectionTypes.getSectionTypeByString(modelType);
        },
		
        getSaveParams: function() {
            var saveParams = baseModel.prototype.getSaveParams.apply(this, arguments);
            saveParams = _.pick(saveParams, this.whitelistedSaveAttrs);
            if ("type" in saveParams) {
                var sectionType = sectionTypes.getSectionType(saveParams.type) || sectionTypes.getSectionTypeByString(saveParams.type);
                if (sectionType)
                    saveParams.type = sectionType.typeString;
            }
            
            if ("title" in saveParams) {
                saveParams.name = saveParams.title;
                delete saveParams.title;
            }
            
            if (_.isObject(saveParams.type))
                saveParams.type = saveParams.type.typeString;
            
            if ("Location" in saveParams) {
                saveParams.location = _.map(saveParams.Location, function(location) {
                    return location.path;
                });
                delete saveParams.Location;
            }
            
            return saveParams;
        },
		
        getSyncTitle: function(t) {
            console.warn('sync title');
            /*var i = this.get("title"),
                n = [],
                s = this.get("currentSubset"),
                a = this.get("currentFilters");
            return t ? (e.each(t, function(e) {
                n.push(e.get("title"))
            }), n = n.join(", "), i + ": " + n) : "all" !== s.id ? i + ": " + s.get("title") : a.length ? (e.each(a, function(e) {
                n.push(e.titles.join(", "))
            }), i + ": " + n.join(",")) : i*/
        },
		
        setAgentAndScanner: function(e, t, i, n) {
            console.warn('agent!');
            /*var s = e.defaultAgent,
                a = e.defaultScanner;
            if (n) {
                var r = e.exclusiveAgents,
                    o = e.exclusiveScanners,
                    l = t.hasExclusiveAgent(r),
                    c = i.hasExclusiveScanner(o);
                l && (s = r[0]), c && (a = o[0])
            }
            this.set({
                agent: s,
                scanner: a
            })*/
        }
    });
	
    return sectionModel;
});