define([
	"underscore", 
	"base/models/baseCollection", 
	"base/models/library/section/filterOptionModel"
], function(_, baseCollection, filterOptionModel) {
    "use strict";
	
    var filterOptionCollection = baseCollection.extend({
        model: filterOptionModel,
        
		url: function() {
            var url = _.result(this.section, "url");
            var fullUrl = url + "/" + this.parent.id;
            if (this.type) 
				fullUrl += "?type=" + this.type; 
			else if (!this.standalone)
				fullUrl += "?" + this.getUrlParams(); 
				
			return fullUrl;
        },
        
		initialize: function(e, initObj) {
            this.standalone = initObj.standalone;
			this.type = initObj.type;
        },
        
		getUrlParams: function() {
            var paramObj = {}; 
			var paramArray = [];
            var currentType = this.section.get("currentType");
            var currentFilters = this.section.get("currentFilters");
            
			if (currentType) 
				paramObj.type = currentType.id; 
			
			_.each(currentFilters, function(filter) {
                if (filter.key !== this.parent.id && filter.values.length) 
					paramObj[filter.key] = filter.values.join(",");
            }, this);
            
			for (var param in paramObj) 
				paramArray.push("unviewedLeafCount" === param ? param + ">=" + paramObj[param] : param + "=" + paramObj[param]);
            
			return paramArray.join("&");
        },
        
		parse: function(filterObj) {
            return this.convert(filterObj, "Directory");
        },
        
		search: function(criteria) {
            var matches = this.filter(function(filter) {
                return filter.get("title").toLowerCase().indexOf(criteria.toLowerCase()) > -1;
            });
			
            return this.clone(matches);
        }
    });
	
    return filterOptionCollection;
});