define([
	"underscore", 
	"base/models/baseCollection", 
	"base/models/library/section/sectionSubsetModel"
], function(_, baseCollection, sectionSubsetModel) {
    "use strict";
	
    var sectionSubsetCollection = baseCollection.extend({
        model: sectionSubsetModel,
        
		url: function() {
            return _.result(this.section, "url");
        },
        
		parse: function(subsets) {
            return this.convert(subsets, "Directory");
        },
        
		filterPrimaries: function() {
            var primary = this.filter(function(subset) {
                return subset.get("primary")
            });
			
            return this.clone(primary);
        }
    });
	
    return sectionSubsetCollection;
});