define([
	"base/utils/ttlUtil", 
	"base/models/storageModel", 
	"base/models/scopedStorageModel"
], function(ttlUtil, storageModel, scopedStorageModel) {
    "use strict";
	
    var n = 7776e6;
    var SectionStorageModel = storageModel.extend({
		storageKey: "sectionViewState",
		serialize: function(i) {
			return storageModel.prototype.serialize.call(this, ttlUtil.augmentDictionary(i, {
				ttl: n
			}));
		}
	});
	
    var sectionViewStateStorageModel = scopedStorageModel.extend({
        initialize: function(e, initObj) {
            scopedStorageModel.prototype.initialize.apply(this, arguments); 
			this.section = initObj.section; 
			this.storage = new SectionStorageModel();
        },
		
        storageKey: function() {
            return this.section.getScopedKey("attrs");
        },
		
        toJSON: function() {
            var json = scopedStorageModel.prototype.toJSON.apply(this, arguments);
            return ttlUtil.augmentObj(json, n, true);
        }
    });
	
    return sectionViewStateStorageModel;
});