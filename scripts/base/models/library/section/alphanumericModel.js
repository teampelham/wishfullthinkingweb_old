define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, parser, baseModel) {
    "use strict";
	
    var alphanumericModel = baseModel.extend({
        idAttribute: "key",
        whitelistedAttrs: ["key", "title", "size"],
        
		url: function() {
            return "/library/sections/" + this.section.id + "/firstCharacter/" + this.id;
        },
        
		convert: function(filterObj) {
            var filter = _.isObject(filterObj) ? filterObj : parser.parseXML(filterObj);
            parser.parseInteger(filter, "size");
			return filter;
        },
        
		parse: function(filterObj) {
            var filter = this.convert(filterObj);
            return _.pick(filter, this.whitelistedAttrs);
        }
    });
	
    return alphanumericModel;
});