define([
	"underscore", 
	"base/models/baseCollection", 
	"base/models/library/section/alphanumericModel"
], function(_, baseCollection, alphanumericModel) {
    "use strict";
	
    var alphanumericCollecton = baseCollection.extend({
        model: alphanumericModel,
		
        url: function() {
            var url = _.result(this.section, "url") + "/firstCharacter";
            var params = this.getUrlParams();
            var resolvedUrl = params ? url + "?" + params : url;
            return resolvedUrl;
        },
		
        getUrlParams: function() {
            var paramObj = {}; 
			var paramArray = [];
            var currentType = this.section.get("currentType");
			
            if (currentType) 
				paramObj.type = currentType.id;
				
            var currentFilters = this.section.get("currentFilters");
            
			_.each(currentFilters, function(filter) {
                if (filter.values.length) 
					paramObj[filter.key] = filter.values.join(",");
            }, this);
			
            for (var param in paramObj) 
				paramArray.push("unviewedLeafCount" === param ? param + ">=" + paramObj[param] : param + "=" + paramObj[param]);
            
			return paramArray.join("&")
        },
		
        parse: function(alphaFilter) {
            return this.convert(alphaFilter, "Directory");
        }
    });
	
    return alphanumericCollecton;
});