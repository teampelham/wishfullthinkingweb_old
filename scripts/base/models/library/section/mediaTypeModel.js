define([
	"underscore", 
	"base/models/baseModel"
], function(_, baseModel) {
    "use strict";
    
	var mediaTypeModel = baseModel.extend({
        defaults: {
            selected: false
        },
        whitelistedAttrs: ["id", "typeString", "title", "plural", "element"],
        
		parse: function(media) {
            return _.pick(media, this.whitelistedAttrs);
        }
    });
	
    return mediaTypeModel;
});