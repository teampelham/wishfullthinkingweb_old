define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel", 
	"base/models/library/section/filterOptionCollection"
], function(_, parser, baseModel, FilterOptionCollection) {
    "use strict";
    
	var linearFilterModel = baseModel.extend({
        idAttribute: "key",
        whitelistedAttrs: ["secondary", "key", "title"],
        
		initialize: function() {
            this.set("options", new FilterOptionCollection([], {
                server: this.server,
                section: this.section,
                parent: this
            }));
        },
        
		convert: function(filterObj) {
            var filter = _.isObject(filterObj) ? filterObj : parser.parseXML(filterObj);
            parser.parseBoolean(filter, "secondary");
			return filter;
        },
        
		parse: function(filterObj) {
            var filter = this.convert(filterObj);
            filter.title = filter.title.replace("By ", "");
			return _.pick(filter, this.whitelistedAttrs);
        }
    });
	
    return linearFilterModel;
});