define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, parser, baseModel) {
    "use strict";
    
	var sectionSubsetModel = baseModel.extend({
        idAttribute: "key",
        whitelistedAttrs: ["key", "title", "secondary"],
        whitelistedPrimarySubsets: ["all", "recentlyAdded", "recentlyViewed", "recentlyViewedShows", "onDeck", "newest", "folder"],
        
		convert: function(subsetObj) {
            var subset = _.isObject(subsetObj) ? subsetObj : parser.parseXML(subsetObj);
            parser.parseBoolean(subset, "secondary");
			return subset;
        },
        
		parse: function(subsetObj) {
            var subset = this.convert(subsetObj);
            subset = _.pick(subset, this.whitelistedAttrs);
			subset.primary = !subset.secondary && _.contains(this.whitelistedPrimarySubsets, subset.key); 
			
			if ("all" === subset.key)
				subset.title = "All"; 
				
			return subset;
        }
    });
	
    return sectionSubsetModel;
});