define([
	"base/models/baseCollection", 
	"base/models/library/section/sectionModel"
], function(baseCollection, sectionModel) {
    "use strict";
	
    var sectionCollection = baseCollection.extend({
        model: sectionModel,
        url: "/web/sections",
        
		parse: function(sectionObj) {
            return this.convert(sectionObj, "Directory");
        }
    });
	
    return sectionCollection;
});