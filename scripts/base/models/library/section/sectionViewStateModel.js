define([
	"underscore", 
	"base/utils/promiseUtil", 
	"base/utils/types/subsetTypes", 
	"base/utils/types/mediaTypes", 
	"base/models/library/section/sectionSubsetModel", 
	"base/models/baseModel"
], function(_, promiseUtil, subsetTypes, mediaTypes, SectionSubsetModel, baseModel) {
    "use strict";
	
    var sectionViewStateModel = baseModel.extend({
        initialize: function(t, initObj) {
            this.storage = initObj.storage; 
			this.section = initObj.section;
			this.type = initObj.type; 
			this.filters = initObj.filters; 
			this.sortKey = initObj.sortKey; 
			this.sortDirection = initObj.sortDirection;
			this.subset = !this.section.get("currentFilters").length && initObj.subset; 
			this.debouncedStorageSave = _.debounce(_.bind(this.storage.save, this.storage), 300);
        },
		
        _fetch: function(state) {
            state = state || {};
            // this.storage is SectionViewStateStorageModel
			var promise = promiseUtil.alwaysResolve(this.storage.fetch());
			
            promise = promise.done(_.bind(function() {
                if (this.type) 
					this.storage.set("typeID", this.type); 
				
				if (this.sortKey && this.sortDirection) 
					this.storage.set({
						sortKey: this.sortKey,
						sortDirection: this.sortDirection
					}); 
					
				if (this.subset) 
					this.storage.set("subset", this.subset); 
				
				if (this.filters && this.filters.length)
					this.storage.set("filters", this.filters);
            }, this)); 
			
			promise = promise.then(_.bind(function() {
                var fetchSortPromise = this.fetchSort();
                fetchSortPromise.then(_.bind(function(sort) {
                    if (sort)
                        sort.set("direction", this.storage.get("sortDirection") || sort.get("defaultDirection"));
                }, this));
                return fetchSortPromise;
            }, this)); 
			
			promise = promise.done(_.bind(this.storage.save, this.storage));
			
			return promise.then(_.constant(this));
        },
		
        fetchSort: function() {
            var sorts = this.section.get("sorts");
            sorts.typeID = this.storage.get("typeID");
            var sortPromise = sorts.populate();
            
            sortPromise = sortPromise.then(_.bind(function() {
                var sortKey = this.storage.get("sortKey");
                var currentSorts;
                if (sortKey) 
                    currentSorts = sorts.findWhere({
                        key: sortKey
                    }); 
                this.sort = currentSorts || sorts.findWhere({
                    isDefault: true
                });
                return this.sort;
            }, this));
            
            return sortPromise;
        },
		
        getType: function() {
            //this.storage.clear("typeID");       // TODO: WHAT THE FUUUUUUUUUUCK
            var typeID = this.storage.get("typeID") || mediaTypes.getMediaTypeByString(this.section.get("type")).id;
            var types = this.section.get("types");
            return types.get(typeID);
        },
		
        getSubsetModel: function() {
            var subsetType, subset = this.storage.get("subset");
            if (subset) 
                subsetType = subsetTypes.getSubsetType(subset); 
            
            if (!subsetType) {
                this.storage.set("subset", "all"); 
                subsetType = subsetTypes.getSubsetType("all");
            }
            
            var sectionSubset = new SectionSubsetModel(subsetType, {
                server: this.section.server,
                section: this.section
            });
            
            return sectionSubset;
        },
		
        getSort: function() {
            if (this.sort) {
               this.sort.set("selected", true); 
	           return this.sort;
            }
        },
		
        getFilters: function() {
            return this.storage.get("filters") || [];
        },
		
        refreshAttributes: function() {
			console.warn('implement');
            /*var e = this.section.get("currentType");
            e && this.storage.set("typeID", e.get("id"));
            var t = this.section.get("currentSort");
            t && (this.storage.set("sortKey", t.get("key")), this.storage.set("sortDirection", t.get("direction") || t.get("defaultDirection")));
            var i = this.section.get("currentFilters");
            this.storage.set("filters", i)*/
        },
		
        getSectionAttrs: function() {
            return {
                currentType: this.getType(),
                currentSubset: this.getSubsetModel(),
                currentSort: this.getSort(),
                currentFilters: this.getFilters()
            }
        },
		
        addListeners: function() {
            this.listenTo(this.section, {
                "change:currentType": this.onValueChange,
                "change:currentSort": this.onValueChange,
                "change:currentSubset": this.onValueChange,
                "change:currentFilters": this.onValueChange
            }); 
			this.listenTo(this.section.get("sorts"), "change:direction", this.onValueChange);
        },
		
        removeListeners: function() {
            this.stopListening(this.section); 
			this.stopListening(this.section.get("sorts"));
        },
        
		onValueChange: function() {
            this.refreshAttributes(); 
			this.debouncedStorageSave();
        }
    });
	
    return sectionViewStateModel;
});