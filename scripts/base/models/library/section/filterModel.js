define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel", 
	"base/models/library/section/filterOptionCollection"
], function(_, parser, baseModel, FilterOptionCollection) {
    "use strict";
	
    var filterModel = baseModel.extend({
        idAttribute: "filter",
        whitelistedAttrs: ["filter", "filterType", "key", "title"],
        
		initialize: function(e, initObj) {
            if (!initObj) 
				initObj = {}; 
			
			this.set("options", new FilterOptionCollection([], {
                server: this.server,
                section: this.section,
                parent: this,
                standalone: initObj.standalone,
                type: initObj.type
            }));
        },
        
		convert: function(filterObj) {
            var filter = _.isObject(filterObj) ? filterObj : parser.parseXML(filterObj);
            return filter;
        },
        
		parse: function(filterObj) {
            var filter = this.convert(filterObj);
            return _.pick(filter, this.whitelistedAttrs);
        }
    });
	
    return filterModel;
});