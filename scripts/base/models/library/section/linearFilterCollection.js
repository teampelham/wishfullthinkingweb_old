define([
	"underscore", 
	"base/models/baseCollection", 
	"base/models/library/section/linearFilterModel"
], function(_, baseCollection, linearFilterModel) {
    "use strict";
	
    var linearFilterCollection = baseCollection.extend({
        model: linearFilterModel,
        
		url: function() {
            var url = _.result(this.section, "url");
            return url;
        },
		
        parse: function(linearFilterObj) {
            return this.convert(linearFilterObj, "Directory")
        },
        
		filterBySecondary: function() {
            var secondary = this.filter(function(filter) {
                return filter.get("secondary");
            });
            return this.clone(secondary);
        }
    });
	
    return linearFilterCollection;
})