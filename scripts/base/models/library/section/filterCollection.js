define([
	"underscore", 
	"jquery", 
	"base/models/baseCollection", 
	"base/models/library/section/filterModel"
], function(_, $, baseCollection, filterModel) {
    "use strict";
	
    var filterCollection = baseCollection.extend({
        model: filterModel,
        comparator: "filterType",
        
		url: function() {
            var url = _.result(this.section, "url") + "/filters";
            var currentType = this.section.get("currentType");
            var initObj = {
                    includeAdvanced: 1
                };
            if(currentType) 
				initObj.type = currentType.id;
			
			return url + "?" + $.param(initObj, true);
        },
		
        parse: function(filter) {
            return this.convert(filter, "Directory");
        }
    });
    
	return filterCollection;
});