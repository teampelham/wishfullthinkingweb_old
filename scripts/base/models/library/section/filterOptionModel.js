define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, parser, baseModel) {
    "use strict";
	
    var filterOptionModel = baseModel.extend({
        idAttribute: "key",
        whitelistedAttrs: ["key", "type", "title"],
        
		convert: function(filterObj) {
            var filter = _.isObject(filterObj) ? filterObj : parser.parseXML(filterObj);
            return filter;
        },
       
	    parse: function(filterObj) {
            var filter = this.convert(filterObj);
            return _.pick(filter, this.whitelistedAttrs);
        }
    });
	
    return filterOptionModel;
});