define([
	"underscore", 
	"base/utils/baseClass", 
	"base/mixins/eventMixin", 
	"base/utils/types/mediaTypes2", 
	"base/models/library/localPlayQueueModel", 
	"base/models/library/playQueueModel"], function(e, t, i, n, s, a) {
    "use strict";
    var r = t.extend({
        mixins: i,
        initialize: function(e) {
            this.server = e.server, this._queues = {
                audio: null,
                photo: null,
                video: null
            }, this.canCreateOnLocal = s.prototype.isItemQueueable, this.doesLocalSupport = s.prototype.supports
        },
        createFor: function(e, t) {
            t = t || e;
            var i;
            i = t.get("isPlaylist") ? t.get("playlistType") : e.get("element") || n.getChildMediaType(t.get("type")).element;
            var r = null;
            return this.canCreateOnServer(e) ? r = new a({
                mediaType: i
            }, {
                server: this.server
            }) : this.canCreateOnLocal(e) && (r = new s({
                mediaType: i
            }, {
                server: this.server
            })), this._queues[i] = r, this.trigger("change change:playQueue:" + i, r), r
        },
        getForType: function(e) {
            return this._queues[e]
        },
        createForType: function(e, t) {
            var i = null;
            return i = t.onServer ? new a({
                mediaType: e
            }, {
                server: this.server
            }) : new s({
                mediaType: e
            }), this._queues[e] = i, this.trigger("change change:playQueue:" + e, i), i
        },
        getFor: function(e) {
            var t = e.get("type");
            if (!t || "channelDirectory" === t) return null;
            var i;
            i = e.get("isPlaylist") ? e.get("playlistType") : e.get("element") || n.getChildMediaType(t).element;
            var s = this._queues[i];
            return s && s.isItemQueueable(e) ? s : null
        },
        get: function(t) {
            return e.findWhere(this._queues, {
                id: t
            })
        },
        populateQueue: function(e, t) {
            var i = new a({
                playQueueID: e,
                mediaType: t
            }, {
                server: this.server
            });
            return this._queues[t] = i, i.populate()
        },
        remove: function(e) {
            var t = this._queues[e];
            t && (this._queues[e] = null, this.trigger("change remove:playQueue:" + e, t))
        },
        removeAll: function() {
            e.each(e.keys(this._queues), this.remove, this)
        },
        canCreateOnServer: function(e) {
            return this.server.supports("playQueues") && !this.server.get("synced") && a.prototype.isItemQueueable(e)
        },
        doesServerSupport: function(e) {
            return this.server.supports("playQueues") && a.prototype.supports(e)
        },
        canCreateFor: function(e, t) {
            return this.canCreateOnServer(e) && this.doesServerSupport(t) || this.canCreateOnLocal(e) && this.doesLocalSupport(t)
        }
    });
    return r
})