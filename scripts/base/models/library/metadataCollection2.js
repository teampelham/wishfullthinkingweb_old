define([
	"underscore", 
	"base/utils/parser", 
	"base/mixins/models/paginationMixin2", 
	"base/models/baseCollection", 
	"base/models/library/metadataModel2"
], function(e, t, i, n, s) {
    "use strict";
    var a = n.extend({
        mixins: i,
        model: s,
        metadataElementNames: ["Directory", "Video", "Track", "Photo", "Hub"],
        collectionProperties: ["key", "totalSize", "viewGroup", "allowSync", "title", "grandparentTitle", "art"],
        url: function() {
            var t = this.section || this.parent;
            return t ? e.result(t, "url") : ""
        },
        initialize: function(e, t) {
            t || (t = {}), this.allowAllLeaves = t.allowAllLeaves, this.populatedRange = {
                start: 0,
                end: 0
            }
        },
        parse: function(i) {
            var n = this.convert(this.mergeElements(i));
            null == n.totalSize && (n.totalSize = n.size), t.parseInteger(n, "totalSize"), t.parseBoolean(n, "allowSync"), t.parseArray(n, "Metadata"), e.each(this.collectionProperties, function(e) {
                var t = this[e],
                    i = n[e],
                    s = this.section ? this.section.get("currentSubset").id : null,
                    a = s && "all" !== s && "folder" !== s;
                a && "totalSize" === e && i > 200 && (i = 200), "totalSize" === e && this.totalSizeAdjustment && (i += this.totalSizeAdjustment), void 0 !== i && t !== i && (this[e] = i, this.trigger("change:" + e, this, i))
            }, this);
            var s = n.Metadata,
                a = e.result(this, "url");
            return e.each(s, function(e) {
                e.containerKey = a, n.identifier && (e.identifier = n.identifier), n.mediaTagPrefix && (e.mediaTagPrefix = n.mediaTagPrefix), n.mediaTagVersion && (e.mediaTagVersion = n.mediaTagVersion)
            }), this.allowAllLeaves ? s : e.reject(s, function(e) {
                var t = /\/allLeaves$/.test(e.key);
                return t && this.totalSize && (this.totalSize -= 1, this.totalSizeAdjustment || (this.totalSizeAdjustment = 0), this.totalSizeAdjustment -= 1), t
            }, this)
        },
        mergeElements: function(i) {
            if (e.isString(i)) i = t.replaceXMLElementNames(i, this.metadataElementNames, "Metadata");
            else if (e.isObject(i) && !i.Metadata) {
                var n = e.compact(e.pick(i, this.metadataElementNames));
                i.Metadata = e.flatten(n, !0)
            }
            return i
        },
        remove: function() {
            n.prototype.remove.apply(this, arguments), i.remove.apply(this, arguments)
        },
        clear: function() {
            this.totalSize = null, this.trigger("change:totalSize", this, null), delete this.totalSizeAdjustment, this.populatedRange = {
                start: 0,
                end: 0
            }, n.prototype.clear.apply(this, arguments)
        },
        splitByType: function() {
            var e = {};
            return this.each(function(t) {
                var i = t.get("type");
                e.hasOwnProperty(i) ? e[i].push(t) : e[i] = [t]
            }), e
        },
        filterByType: function(e) {
            var t = this.filter(function(t) {
                return t.get("type") === e
            });
            return this.clone(t)
        }
    });
    return a
});