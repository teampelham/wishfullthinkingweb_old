﻿define([
	"require", 
	"underscore", 
	"jquery", 
	"base/utils/t", 
	"base/utils/parser", 
	"base/utils/mediaUtil2", 
	"base/utils/types/mediaTypes2", 
	"base/utils/types/streamTypes2", 
	"base/utils/formatters/stringUtil", 
	"base/utils/constants/timelineEvents", 
	"base/models/baseModel", 
	"base/models/library/overlayCollection2", 
	"base/models/library/concertCollection2"
], function(e, t, i, n, s, a, r, o, l, c, d, u, h) {
    "use strict";
    var p = d.extend({
        idAttribute: "ratingKey",
        defaults: {
            title: "",
            titleSort: "",
            tagline: "",
            rating: 0,
            summary: "",
            year: "",
            studio: "",
            contentRating: "",
            originallyAvailableAt: "",
            originalTitle: "",
            thumb: "",
            parentThumb: "",
            grandparentThumb: "",
            banner: "",
            art: "",
            parentArt: "",
            grandparentArt: "",
            primaryExtraKey: ""
        },
        omittedSaveAttrs: ["updatedAt", "thumb", "banner", "art", "Media", "Location"],
        tagAttrs: ["Collection", "Director", "Writer", "Genre", "Mood", "Country", "Label", "Similar"],
        subcollections: ["Media", "Genre", "Collection", "Field", "Writer", "Director", "Role", "Country", "Location", "Label"],
        metadataElementNames: ["Directory", "Video", "Track", "Photo", "Hub"],
        url: function() {
            return this.get("key")
        },
        urlRoot: function() {
            return this.get("key")
        },
        firstItemUrl: function() {
            return this.url()
        },
        singleMetadataModel: function() {
            return this
        },
        cacheKey: function(e) {
            e.identifier && !this.isLibraryItem(e) && (e = this.normalizeKey(e));
            var t = this.normalizeChildrenKey(e.key);
            return this.server && this.server.getScopedKey(t.key)
        },
        preserveCachedAttributes: function(e) {
            return /\/(children|library\/sections\/\d+\/\w+)(\?.*)?$/.test(e)
        },
        initialize: function() {
            this.listenTo(this, "socket:timeline", function(e) {
                e.state === c.PROCESSED ? (this.fetch({
                    data: {
                        includeExtras: "1",
                        includeRelated: "1",
                        includeRelatedCount: "5",
                        includeConcerts: "1",
                        includeOnDeck: "1"
                    }
                }), this.refreshChildren()) : e.state === c.DELETED && this.trigger("destroy", this, this.collection)
            })
        },
        convert: function(e) {
            t.isString(e) && (e = s.replaceXMLElementNames(e, this.metadataElementNames, "Metadata"));
            var i = t.isObject(e) ? e : s.parseXML(e);
            if (!i.ratingKey && !i.key && !i.id) {
                var n = i.Metadata || i;
                n.identifier = i.identifier, n.mediaTagPrefix = i.mediaTagPrefix, n.mediaTagVersion = i.mediaTagVersion, n.allowSync = i.allowSync, n.librarySectionID = i.librarySectionID, n.librarySectionUUID = i.librarySectionUUID, i = n
            }
            s.parseBoolean(i, ["allowSync", "selected"]), s.parseInteger(i, ["index", "parentIndex", "absoluteIndex", "duration", "viewOffset", "viewCount", "leafCount", "viewedLeafCount", "extraType", "lastViewedAt", "addedAt", "updatedAt"]), s.parseFloat(i, ["rating", "userRating"]), s.parseArray(i, this.subcollections);
            var a = i.Media;
            return t.each(a, function(e) {
                s.parseBoolean(e, ["indirect", "optimizedForStreaming", "premium"]), s.parseInteger(e, ["id", "duration", "width", "height", "bitrate", "audioChannels"]), s.parseArray(e, "Part");
                var i = e.Part;
                t.each(i, function(e) {
                    s.parseBoolean(e, "optimizedForStreaming"), s.parseInteger(e, ["id", "duration", "size"]), s.parseEncodedString(e, "file"), s.parseArray(e, "Stream"), e.exists = "0" !== e.exists, e.accessible = "0" !== e.accessible;
                    var i = e.Stream;
                    t.each(i, function(e) {
                        s.parseBoolean(e, "selected"), s.parseInteger(e, ["id", "index", "streamType", "duration", "width", "height", "bitrate", "bitDepth", "samplingRate", "channels"])
                    })
                })
            }), i
        },
        parse: function(i) {
            var s = this.convert(i);
            if (!s.key) return s;
            this.isLibraryItem(s) || (s = this.normalize(s)), "photo" === s.type && /\/children$/.test(s.key) && (s.type = "photoAlbum"), "photoalbum" === s.type && (s.type = "photoAlbum"), s.isFolder = /\/folder\?parent\=/.test(s.key), s.isExtra = !! s.extraType;
            var r = e("base/models/library/metadataCollection2"),
                l = e("base/models/library/hubCollection");
            this.parseChildren(s, r), this.parseExtras(s, r), this.parseRelated(s, l), this.parseOverlays(s), this.parsePopularLeaves(s, r), this.parseConcerts(s, h), this.parseOnDeck(s), null != s.viewedLeafCount && (s.unviewedLeafCount = s.leafCount - s.viewedLeafCount);
            var c = s.Media,
                d = o.getStreamTypeByString("subtitles");
            return t.each(c, function(e) {
                var i = t.first(e.Part);
                if (i && i.Stream) {
                    var s = i.Stream,
                        r = a.getStreams(e, {
                            streamType: d.id,
                            selected: !0
                        }),
                        o = {
                            id: 0,
                            streamType: d.id,
                            language: n("None")
                        };
                    r.length || (o.selected = !0), s.unshift(o)
                }
            }), t.defaults(s, this.defaults), s
        },
        parseChildren: function(e, t) {
            var i = this.normalizeChildrenKey(e.key);
            if (i.hasChildren && (e.key = i.key, !this.get("children"))) {
                var n = i.previousKey;
                "album" === e.type && (n += n.indexOf("?") > -1 ? "&" : "?", n += "includeRelated=1");
                var s = new t([], {
                    server: this.server,
                    section: this.section,
                    parent: this,
                    url: n
                });
                this.set("children", s)
            }
        },
        parsePopularLeaves: function(e, t) {
            if (!e.PopularLeaves) return void this.unset("popularLeaves");
            var i = this.get("popularLeaves"),
                n = [],
                s = !1;
            e.PopularLeaves.size > 0 && (n = e.PopularLeaves, s = !0), i ? i.set(n, {
                parse: s
            }) : (i = new t(n, {
                server: this.server,
                section: this.section,
                parent: this,
                url: e.PopularLeaves.key,
                populated: !0,
                parse: s
            }), this.set("popularLeaves", i)), delete e.PopularLeaves
        },
        parseExtras: function(e, t) {
            if (!e.Extras) return void this.unset("extras");
            var i, n, s = this.get("extras");
            e.Extras.size > 0 ? (i = e.Extras, n = !0) : (i = [], n = !1), s ? s.set(i, {
                parse: n
            }) : (s = new t(i, {
                server: this.server,
                section: this.section,
                parent: this,
                url: e.key + "/extras",
                populated: !0,
                parse: n
            }), this.set("extras", s)), delete e.Extras
        },
        parseRelated: function(e, t) {
            if (!e.Related) return void this.unset("related");
            var i, n, s = this.get("related");
            e.Related.Metadata ? (i = e.Related, n = !0) : (i = [], n = !1), s ? s.reset(i, {
                parse: n
            }) : (s = new t(i, {
                server: this.server,
                section: this.section,
                parent: this,
                populated: !0,
                parse: n
            }), this.set("related", s)), delete e.Related
        },
        parseOverlays: function(e) {
            if (!e.Overlay) return void this.unset("overlays");
            var i = this.get("overlays"),
                n = t.isArray(e.Overlay) ? e.Overlay : [e.Overlay],
                s = !0;
            i ? i.reset(n, {
                parse: s
            }) : (i = new u(n, {
                server: this.server,
                section: this.section,
                parent: this,
                populated: !0,
                parse: s
            }), this.set("overlays", i)), delete e.Overlay
        },
        parseConcerts: function(e) {
            if (!e.Concert) return void this.unset("concerts");
            var i = this.get("concerts"),
                n = t.isArray(e.Concert) ? e.Concert : [e.Concert],
                s = !0;
            i ? i.reset(n, {
                parse: s
            }) : (i = new h(n, {
                server: this.server,
                section: this.section,
                parent: this,
                populated: !0,
                parse: s
            }), this.set("concerts", i)), delete e.Concert
        },
        parseOnDeck: function(e) {
            if (!e.OnDeck) return void this.unset("onDeck");
            var t = this.get("onDeck"),
                i = !0,
                n = e.OnDeck.Metadata;
            t ? t.set(n, {
                parse: i
            }) : (t = new p(n, {
                server: this.server,
                section: this.section,
                parse: i
            }), this.set("onDeck", t)), delete e.OnDeck
        },
        normalize: function(e) {
            return e = this.normalizeKey(e), e = this.normalizeType(e), e = this.normalizeMetadata(e)
        },
        normalizeChildrenKey: function(e) {
            var t = /(.*)\/children$/.exec(e),
                i = e;
            return t && (e = t[1]), {
                key: e,
                previousKey: i,
                hasChildren: !! t
            }
        },
        normalizeKey: function(e) {
            var t = e.key.split("/");
            return 1 === t.length && e.containerKey && (e.hasInvalidKey = !0, e.originalKey = e.key, e.key = e.containerKey + "/" + e.key), e.ratingKey || (e.ratingKey = e.originalKey || e.key), e
        },
        normalizeType: function(e) {
            return e.mediaType && !e.type && (e.type = e.mediaType, delete e.mediaType), e.elementName && !e.type && (e.type = e.elementName.toLowerCase()), /image/i.test(e.type) ? e.type = "photo" : /(movie|episode|video)/i.test(e.type) && (e.type = "clip"), r.getMediaTypeByString(e.type) || (e.type = "channelDirectory"), e
        },
        normalizeMetadata: function(e) {
            return e.label ? (e.title = e.label, delete e.label) : e.artist && (e.track ? (e.title = e.track, e.originalTitle = e.artist, e.parentTitle = e.album, e.grandparentTitle = e.albumArtist) : e.album ? (e.title = e.album, e.parentTitle = e.artist) : e.title = e.artist, delete e.track, delete e.album, delete e.albumArtist, delete e.artist), e.totalTime && (e.duration = e.totalTime, delete e.totalTime), e
        },
        refreshChildren: function() {
            var e = this.get("children");
            if (e && e.populatePromise) {
                var t = e.fetch();
                t.always(function() {
                    e.invoke("refreshChildren")
                })
            }
        },
        resolveIndirect: function(e, n) {
            var s = i.Deferred();
            if (!e || !n) return s.reject(this).promise();
            if (!e.indirect) return s.resolve(this).promise();
            if (n.postURL) return this.resolvePostUrl(e, n);
            var a = n.key;
            this.server.get("cloud") && (a = this.server.resolveUrl(n.key, {
                connection: {
                    scheme: "https",
                    address: "node.plexapp.com",
                    port: 32443
                }
            }));
            var r = i.ajax(this.server.getXHROptions(a));
            return r.done(t.bind(function(t) {
                this.mergeIndirectResponse(t, e, n) ? s.resolve(this) : s.reject(this)
            }, this)), r.fail(t.bind(function() {
                s.reject(this)
            }, this)), s.promise()
        },
        resolvePostUrl: function(e, n) {
            var s = i.Deferred(),
                a = this.server.get("cloud") || this.server.get("synced"),
                r = a ? this.server.parent.get("primaryServer") : this.server;
            if (!r) return s.reject(this).promise();
            var o = r.getXHROptions("/system/proxy");
            o.headers = o.headers || {}, o.headers["X-Plex-Url"] = n.postURL;
            var l = i.ajax(o);
            return l = l.then(t.bind(function(e, t, s) {
                var a = n.key + "&postURL=" + encodeURIComponent(n.postURL),
                    r = s.getAllResponseHeaders() + "\r\n" + e;
                this.server.get("cloud") && (a = this.server.resolveUrl(a, {
                    connection: {
                        scheme: "https",
                        address: "node.plexapp.com",
                        port: 32443
                    }
                }));
                var o = this.server.getXHROptions(a);
                return o.type = "POST", o.data = r, i.ajax(o)
            }, this)), l.done(t.bind(function(t) {
                this.mergeIndirectResponse(t, e, n) ? s.resolve(this) : s.reject(this)
            }, this)), l.fail(t.bind(function() {
                s.reject(this)
            }, this)), s.promise()
        },
        mergeIndirectResponse: function(e, i, n) {
            var a = s.parseXML(e);
            if (!(a && a.Video && a.Video.Media && a.Video.Media.Part)) return !1;
            var r = this.parse(a.Video),
                o = t.first(r.Media),
                l = t.first(o.Part);
            if (o.Part.length > 1) {
                var c = t.indexOf(i.Part, n);
                c < o.Part.length && (l = o.Part[c])
            }
            if (!o || !l) return !1;
            if (o.indirect = !1, t.extend(i, t.omit(o, "Part")), t.extend(n, l), a.httpHeaders) {
                for (var d, u = new RegExp("([^=]+)=([^&]+)&?", "g"), h = {}; d = u.exec(a.httpHeaders);) d[1] in h ? (t.isArray(h[d[1]]) || (h[d[1]] = [h[d[1]]]), h[d[1]].push(d[2])) : h[d[1]] = d[2];
                this.set("indirectHttpHeaders", h, {
                    silent: !0
                })
            }
            return !0
        },
        getSaveParams: function(e) {
            return t.each(e.Field, function(t) {
                e[t.name + ".locked"] = t.locked
            }), delete e.Field, t.each(this.tagAttrs, function(i) {
                i in e && (t.isEmpty(e[i]) ? e[l.lowerCaseFirstLetter(i) + "[]"] = "" : t.each(e[i], function(t, n) {
                    e[l.lowerCaseFirstLetter(i) + "[" + n + "].tag.tag"] = t.tag
                }), delete e[i])
            }), e
        },
        batchAdd: function(e, n, s) {
            var a = t.map(e, function(e) {
                return e.get("ratingKey")
            }),
                r = "/library/metadata/" + a.join(",") + "/" + n + "/add?tag=" + encodeURIComponent(s),
                o = t.extend(this.server.getXHROptions(r), {
                    type: "PUT"
                });
            return i.ajax(o)
        },
        getDownloadUrl: function() {
            var e = t.first(this.get("Media"));
            if (e) {
                var i = t.first(e.Part);
                if (i && i.key) return this.server.resolveUrl(i.key, {
                    appendToken: !0
                })
            }
            return ""
        },
        getFilepath: function() {
            var e = t.first(this.get("Media"));
            if (e) {
                var i = t.first(e.Part);
                if (i) return i.file
            }
            return null
        },
        getLocation: function() {
            var e = t.first(this.get("Location"));
            return e ? e.path : this.getFilepath()
        },
        getMediaKey: function() {
            var e = t.first(this.get("Media")),
                i = this.get("key");
            if (e) {
                var n = t.first(e.Part);
                if (n) return n.key
            }
            return i
        },
        isLibraryItem: function(e) {
            var t = e ? e.identifier : this.get("identifier");
            if (t) return "com.plexapp.plugins.library" === t;
            var i = e ? e.key : this.get("key");
            return /^(\/sync\/[A-Za-z0-9-\/]*)?\/library\//.test(i)
        },
        isPlexMusicItem: function() {
            var e = this.section;
            return e && "com.plexapp.agents.plexmusic" === e.get("agent")
        }
    });
    return p
})