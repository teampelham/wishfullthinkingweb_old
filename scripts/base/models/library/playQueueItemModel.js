define([
	"underscore", 
	"base/models/library/metadataModel2"
], function(e, t) {
    "use strict";
    var i = t.extend({
        idAttribute: "playQueueItemID",
        propagatedAttributes: ["viewOffset", "viewCount"],
        cacheKey: function(e) {
            return this.server && this.server.getScopedKey(e.playQueueItemID)
        },
        initialize: function() {
            t.prototype.initialize.apply(this, arguments), this.listenTo(this, "change", function() {
                var i = this.changedAttributes(),
                    n = e.intersection(e.keys(i), this.propagatedAttributes);
                e.each(n, function(i) {
                    var n = new t({
                        key: this.get("key")
                    }, {
                        server: this.server,
                        onlyIfCached: !0
                    });
                    e.isEmpty(n) || n.set(i, this.get(i))
                }, this)
            })
        }
    });
    return i
})