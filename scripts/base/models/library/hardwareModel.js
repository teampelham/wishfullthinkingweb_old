define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel", 
], function(_, parser, baseModel) {
    "use strict";
	
    var hardwareModel = baseModel.extend({
        idAttribute: "HardwareID",
        whitelistedAttrs: ["key", "title", "dimmable", "automationProtocolID"],
        
		convert: function(hardwareObj) {
            var hardware = _.isObject(hardwareObj) ? hardwareObj : parser.parseXML(hardwareObj);
			return hardware;
        },
		
        parse: function(hardwareObj) {
            var hardware = this.convert(hardwareObj);
            parser.parseInteger(hardware, "HardwareID");
			hardware.key = hardware.HardwareID;
			hardware.title = hardware.Title;
			hardware.dimmable = hardware.Dimmable;
            hardware.automationProtocolID = hardware.AutomationProtocolID;
			
			hardware = _.pick(hardware, this.whitelistedAttrs);
			return hardware;
        }
    });
	
    return hardwareModel;
});