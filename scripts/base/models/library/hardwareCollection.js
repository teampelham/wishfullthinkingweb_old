define([
	"underscore", 
	"base/models/baseCollection", 
	"base/models/library/hardwareModel"
], function(_, baseCollection, hardwareModel) {
    "use strict";
	
    var hardwareCollection = baseCollection.extend({
        model: hardwareModel,
        url:  "device/gethardwarelist",
        
        initialize: function(initial, initObj) {
            initObj = initObj || {};
            
            this.server = initObj.server;    
        },
        
        parse: function(hardwareObj) {
            var hardwares = this.convert(hardwareObj);
            
            return hardwares;
        },
        
        toJSON: function() {
            var hardwares = [];
            this.each(function(hardware) {
               hardwares.push(hardware.toJSON()); 
            });
            return hardwares;
        }
    });
	
    return hardwareCollection;
});