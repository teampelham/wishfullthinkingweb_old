define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, parser, baseModel) {
    "use strict";
	
    var serverCloudAccountModel = baseModel.extend({
        url: "/myplex/account",
        whitelistedAttrs: ["username", "mappingState", "mappingError", "mappingErrorMessage", "signInState", "publicAddress", "publicPort", "privateAddress", "privatePort", "subscriptionActive", "subscriptionState"],
        
		defaults: {
            signedIn: false,
            remoteAccessState: "",
            mappingState: "",
            mappingError: "",
            mappingErrorMessage: "",
            signInState: "",
            publicAddress: "",
            publicPort: "",
            privateAddress: "",
            privatePort: ""
        },
		
        isNew: function() {
            return !this.get("signedIn");
        },
		
        convert: function(modelObj) {
            var model = e.isObject(modelObj) ? modelObj : t.parseXML(modelObj);
            _.defaults(model, _.result(this, "defaults"));
			return model;
        },
		
        parse: function(modelObj) {
            var model = this.convert(modelObj);
            model = _.pick(model, this.whitelistedAttrs);
			
			if ("ok" === model.signInState) 
				model.signedIn = true; 
			else {
				model.signedIn = false;
				model.username = "";
			} 
			
			if (model.signedIn && "mapped" === model.mappingState && !model.mappingError)
				model.remoteAccessState = "ok";
			else if (!model.signedIn || model.mappingError) 
				model.remoteAccessState = "unreachable";
			
			return model;
        },
		
        isWaiting: function() {
            var remoteAccessState = this.get("remoteAccessState");
            return "connecting" === remoteAccessState || "disconnecting" === remoteAccessState;
        }
    });
	
    return serverCloudAccountModel;
});