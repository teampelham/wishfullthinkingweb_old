define([
	"underscore", 
	"jquery", 
	"base/models/baseCollection", 
	"base/models/server/serverModel"
], function(_, $, baseCollection, serverModel) {
    "use strict";
    
	var serverCollection = baseCollection.extend({
        model: serverModel,
        url: "/servers",
        
        parse: function(e) {
            return this.convert(e, "Server");
        },
        
        comparator: function(server) {
            var t = server.get("friendlyName") || "";
            var i = server.get("shared");
            var n = server.get("synced");
            var s = i ? "1" : "0";
            return s += n ? "1" : "0", s += t.toLowerCase();
        },
        
        toLiteJSON: function() {
            return this.map(function(server) {
                return server.toLiteJSON();
            })
        },
        
        populateServer: function(server) {
            return this.findServer(server).then(function(result1) {
                return result1.populate().then(function(result2) {
                    return result2;
                });
            });
        },
        
        populateAnyServer: function() {
            function onAuthComplete(e, authorized) {
                if (authorized) 
                    deferred.resolve(e);
                else 
                    populateAnyFailCheck();
            }
            
            var deferred = $.Deferred();
            var someAuthorized = this.findWhere({
                authorized: true
            });
                
            if (someAuthorized) 
                return deferred.resolve(someAuthorized).promise();  // resolve the promise with the first authorized server
            
            var populateAnyFailCheck = _.bind(function() {
                var allUnauthorized = this.every(function(server) {
                    return server.get("authorized") === false
                });
                
                if (allUnauthorized)
                    deferred.reject();      // the last server to complete still has all servers unauthorized, so reject the promise
            }, this);
            
            this.on("change:authorized", onAuthComplete);       // start listening for authorized changes
            
            this.populate().always(function() {
                "pending" === deferred.state() && populateAnyFailCheck()
            });
             
            deferred.always(_.bind(function() {
                this.off("change:authorized", onAuthComplete);  // stop listening for this once the call completes
            }, this));
            
            return deferred.promise();
        },
        
        findServer: function(serverID) {
            function serverAdded(e) {
                if (e.id === serverID) 
                    deferred.resolve(e);
            }

            function serverChanged(e, t) {
                if (t === serverID) 
                    deferred.resolve(e);
            }
            
            var deferred = $.Deferred();
            var server = this.get(serverID);
            
            if (server) 
                return deferred.resolve(server).promise(); 
            
            this.on("add", serverAdded); 
            this.on("change:machineIdentifier", serverChanged); 
            
            this.populate().always(function() {
                if ("pending" === deferred.state()) 
                    deferred.reject();
            }); 
            
            deferred.always(_.bind(function() {
                this.off("add", serverAdded); 
                this.off("change:machineIdentifier", serverChanged);
            }, this)); 
            
            return deferred.promise();
        }
    });
	
    return serverCollection;
});