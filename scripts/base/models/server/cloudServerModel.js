define([
	"underscore", 
	"jquery", 
	"base/adapters/loggerAdapter", 
	"base/models/server/serverModel", 
	"base/models/server/resourceCollection", 
	"base/models/library/playQueueManager"
], function(_, $, loggerAdapter, serverModel, ResourceCollection, PlayQueueManager) {
    "use strict";
    
    var cloudServerModel = serverModel.extend({
        baseUrl: "http://app.wishfullthinking.net",
        
		defaults: function() {
            return {
                machineIdentifier: "wishFullCloud",
                friendlyName: "wishFull.net",
                cloud: true,
                downForMaintenance: false,
                /*plexPassUrl: this.baseUrl + "/subscription/about",
                accountUrl: this.baseUrl + "/users/edit",
                cloudSyncUrl: this.baseUrl + "/cloudsync/providers",
                appsUrl: this.baseUrl + "/downloads",
                helpUrl: "http://support.plex.tv/hc/en-us",*/
                resources: new ResourceCollection([], {
                    server: this
                }),
                playQueueManager: new PlayQueueManager({
                    server: this
                })
            }
        },
		
        initialize: function() {
            this.listenTo(this, "change:resources", this.onResourcesChange);
			this.forwardResourcesEvents();
        },
		
        forwardResourcesEvents: function() {
            var resources = this.get("resources");
            if (resources) {
				this.listenTo(resources, "all", function() {
                    var args = _.toArray(arguments);
                    var firstArg = args[0];
                	args[0] = "change:resources:" + firstArg;
					this.trigger.apply(this, args);
            	});
            }
        },
        
        // This testConnection override is essentially just a ping request to the cloud that returns the IP Address
		testConnection: function(serverConnection) {   // n
            var serverName = this.get("friendlyName");   // s
            var url = this.resolveUrl("/wfs/ping.php", {    // a
                connection: serverConnection
            });
            var xhrOptions = _.extend(this.getXHROptions(url), {     // r
                timeout: this.timeout
            });
            var ajaxCall = $.ajax(xhrOptions);              // o
            
            loggerAdapter.log("Testing connection to " + serverName + " at " + url);
            ajaxCall.done(_.bind(function() {
                loggerAdapter.success("Setting active connection for " + serverName + " (" + url + ")");
                this.set({
                    connected: true,
                    available: true,
                    downForMaintenance: false,
                    activeConnection: serverConnection
                });
            }, this));
            
            ajaxCall.fail(_.bind(function(result) {
                loggerAdapter.error(serverName + " is unavailable at " + url + " (Status " + result.status + ")");
                this.set({
                    connected: false,
                    available: false,
                    downForMaintenance: 503 === result.status
                }); 
                this.populatePromise.rejectWith(this, [this, result]);
            }, this));
        },
        
        onResourcesChange: function() {
            console.warn('onresourceschange');
            //var e = this.previous("resources");
            //e && this.stopListening(e), this.forwardResourcesEvents()
        }
    });
    
	cloudServerModel.prototype.cache = serverModel.prototype.cache;
	
	return cloudServerModel;
});