define([
	"underscore", 
	"jquery", 
	"backbone", 
	"base/adapters/loggerAdapter", 
	"base/mixins/serverMixin", 
	"base/models/baseModel", 
	"base/models/config", 
	"base/models/library/section/sectionCollection", 
	"base/models/library/playQueueManager", 
	//"base/models/library/PlaylistsSectionModel", 
	"base/models/server/serverCloudAccountModel", 
	"base/models/server/serverUpdateModel", 
	"base/models/server/serverSettingCollection", 
	"base/models/server/serverAlertCollection", 
	//"base/models/server/TranscodeJobCollection", 
    "base/models/server/sessionCollection"
], function(_, $, bb, loggerAdapter, serverMixin, baseModel, config, SectionCollection, PlayQueueManager, ServerCloudAccountModel, ServerUpdateModel, ServerSettingCollection, ServerAlertCollection, SessionCollection) {
    "use strict";
    
	var serverModel = baseModel.extend({
        mixins: serverMixin,
        persistInCache: true,
        timeout: 1e4,
        
        defaults: function() {
            return _.extend(serverMixin.defaults.apply(this, arguments), {
                cloudAccount: new ServerCloudAccountModel({}, {
                    server: this
                }),
                update: config.get("serverUpdate") ? new ServerUpdateModel({}, {
                    server: this
                }) : null,
                settings: new ServerSettingCollection([], {
                    server: this
                }),
                sessions: new SessionCollection([], {
                    server: this
                }),
                sections: new SectionCollection([], {
                    server: this
                }),
                playQueueManager: new PlayQueueManager({
                    server: this
                }),
                alerts: new ServerAlertCollection([], {
                    server: this
                })
                /*transcodeJobs: new m([], {
                    server: this
                }),

                
                playlists: new c({}, {
                    server: this
                })*/
            });
        },
        
        constructor: function() {
            this.connections = [];
            // Hooks into the populateMixin via the baseModel to set up the initial pending-populate promise object
            this.resetPopulatePromise($.Deferred());
            
            return baseModel.prototype.constructor.apply(this, arguments) || null;
        },
        
        initialize: function(e, initObj) {
            if (initObj && initObj.source)
                this.addSource(initObj.source);
                
            this.listenTo(this, "change:machineIdentifier", this.onMachineIdentifierChange);
        },
        
        fetch: function() {
            return bb.Model.prototype.fetch.apply(this, arguments);
        },
        
        merge: function(cache) {    // t
            console.log('merge');
            /*cache.set("isMerged", true);
            if (cache.get("bundled"))
                this.set("bundled", true);
                
            var sources = cache.get("sources");   // i
            _.each(sources, function(source) {
                this.addSource(source, {
                    silent: true
                });
            }, this);
            _.each(cache.connections, function(connection) {    // t
                logger.log("Merging connection (" + connection.address + ") into " + this.get("friendlyName"));
                this.addConnection(e.pick(connection, "uri", "scheme", "address", "port", "accessToken", "bundled"));
            }, this); 
            
            if (this.collection) {
                this.collection.remove(cache.cid, {
                    silent: true
                }); 
                this.collection._byId[this.id] = this;
                this.collection.trigger("remove", cache, this);
            } 
            this.cache.remove(cache);*/
        },
        
        toLiteJSON: function() {
            return this.pick(["friendlyName", "machineIdentifier", "accessToken", "cloud", "shared", "synced", "sources"]);
        },
        
        addSource: function(source, options) {     // t, i
            if (!options)
                options = {};
            else
                console.warn('i');
            
            var sources = this.get("sources");  // n
            
            if (source && !_.contains(sources, source)) {
                sources.push(source);
                if (!options.silent) 
                    this.trigger("change:sources", this, sources);
            }
        },
        
        removeSource: function(source, i) {  // t, i
            if (!i)
                i = {};
            else
                console.warn('i');
                
            var sources = this.get("sources") || [];    // n
            var index = _.indexOf(sources, source);                // s
            if (index > -1)  
                sources.splice(index, 1);
            
            if (!i.silent)
                this.trigger("change:sources", this, sources);
        },
        
        addConnection: function(connection) {
            var match = _.findWhere(this.connections, {     // n
                uri: connection.uri,
                scheme: connection.scheme,
                address: connection.address,
                port: connection.port
            });
            
            // find or create the new connection
            if (match) {
                // no need to test if connected already
                if (match.available || match.accessToken || !connection.accessToken) 
                    return match;
                match.pending = true; 
                match.accessToken = connection.accessToken;
                connection = match;
            } 
            else {
                connection.pending = true; 
                connection.isLocalAddress = this.isLocalAddress(connection.address);
                connection.isLoopbackAddress = this.isLoopbackAddress(connection.address);
                connection.isSecure = this.isSecure(connection.scheme); 
                this.connections.push(connection);
            }
            
            // server connection may be rejected
            if ("rejected" === this.populatePromise.state()) {
                this.resetPopulatePromise($.Deferred());
                this.set("authorized", null);
            }
            
            // trigger a change, the ServerConnectionManager will test the connection
            this.trigger("change:connections", this, this.connections);
            
            return connection;
        },
        
        hasLocalConnection: function() {
            console.log('hasLocalConnection');
            //var activeConnection = this.get("activeConnection");
            //return activeConnection ? activeConnection.isLocalAddress : null;
        },
        
        hasLoopbackConnection: function() {
            console.log('hasLoopbackConnection');
            //var activeConnection = this.get("activeConnection");
            //return activeConnection ? activeConnection.isLoopbackAddress : null;
        },
        
        hasSecureConnection: function() {
            var activeConnection = this.get("activeConnection");
            return activeConnection ? "https" === _.scheme : null;
        },
        
        canBePrimary: function(version) {
            if(this.get("synced") || this.get("cloud"))             // synced means player, cloud can't be primary either
                return false;
                
            if(this.get("shared") && !this.get("multiuser"))        // shared and multiuser servers aren't primary candidates
                return false;
                
            if (this.get("available") && this.get("authorized"))    // only available, authorized servers meeting the min version req can be primary
                return this.isVersionOrNewer(version);
                
            return false;
        },
        
        onMachineIdentifierChange: function() {
            console.log('onMachineIdentifierChange');
            /*if (this.collection) 
                this.listenToOnce(this.collection, "change:machineIdentifier", this.onMachineIdentifierChanged); 
            else
                this.onMachineIdentifierChanged();*/
        },
        
        onMachineIdentifierChanged: function() {
            console.log('onMachineIdentifiedChanged');
            /*var cacheKey = this.cacheKey(this.attributes); // e
            var cache = this.cache.get(cacheKey);  // t
            if (cache) 
                if (cache.get("connected") || !this.get("connected"))
                    cache.merge(this);
                else {
                    this.merge(cache);
                    this.cache.store(this);
                } 
            else 
                this.cache.store(this, cacheKey);*/
        }
    });
    
    //_.extend(serverModel.prototype, serverMixin);
    return serverModel;
});
