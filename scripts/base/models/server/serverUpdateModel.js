define([
	"underscore", 
	"jquery", 
	"base/utils/dispatcher", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, $, dispatcher, parser, baseModel) {
    "use strict";
    var serverUpdateModel = baseModel.extend({
        url: "/web/updatestatus",
        whitelistedAttrs: ["status", "canInstall", "downloadURL", "state", "progress", "version", "error", "added", "fixed"],
        
		defaults: {
            state: "none",
            progress: 0,
            version: "",
            error: "",
            added: "",
            fixed: "",
            shouldInstall: false
        },
		
        fetch: function() {
            if (this.server.supports("updater")) 
				return baseModel.prototype.fetch.apply(this, arguments);
            return $.Deferred().reject().promise();
        },
		
        convert: function(modelObj) {
            var model = _.isObject(modelObj) ? modelObj : parser.parseXML(modelObj);
            parser.parseBoolean(model, "canInstall");
			parser.parseInteger(model, "status");
			parser.parseInteger(model.Release, "progress");
			return model;
        },
		
        parse: function(modelObj) {
            var model = this.convert(modelObj);
            model = _.extend(model, model.Release);
			model = _.pick(model, this.whitelistedAttrs);
			
			if (0 !== model.status) 
				model.state = "error";
			
			return model;
        }
    });
	
    return serverUpdateModel;
});