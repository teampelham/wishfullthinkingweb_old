define([
	"underscore", 
	"base/models/baseCollection", 
	"base/models/server/browseDirectoryModel"
], function(e, t, i) {
    "use strict";
    var n = t.extend({
        model: i,
        url: function() {
            return this.parent ? this.parent.url() : this.server.resolveUrl("/services/browse")
        },
        initialize: function(t) {
            var i = e.bind(function(e) {
                e.set("parent", this.parent), e.set("children", new n([], {
                    parent: e,
                    server: this.server
                }))
            }, this);
            e.each(t, i), this.on("reset", function() {
                this.each(i)
            }, this), this.on("add", i)
        },
        parse: function(e) {
            return this.convert(e, "Path")
        }
    });
    return n
});