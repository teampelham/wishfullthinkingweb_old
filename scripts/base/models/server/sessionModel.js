define(["base/models/library/metadataModel"], function(metadataModel) {
    "use strict";
    
    var sessionModel = metadataModel.extend({
        idAttribute: "sessionKey",
        
        cacheKey: function(sessionItem) {
            return this.server && this.server.getScopedKey(sessionItem.sessionKey);
        }
    });
    
    return sessionModel;
});