define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, parser, baseModel) {
    "use strict";
	
    var serverSettingModel = baseModel.extend({
        idAttribute: "id",
        url: "/web/prefs",
        
		defaults: {
            type: "string",
            summary: "",
            label: "",
            group: ""
        },
		
        whitelistedAttrs: ["id", "label", "default", "summary", "value", "type", "group", "advanced", "option", "hidden", "enumValues", "values"],
        
		convert: function(settingObj) {
            var setting = _.isObject(settingObj) ? settingObj : parser.parseXML(settingObj);
            if ("bool" === setting.type) 
				parser.parseBoolean(setting, "value"); 
			else if ("int" === setting.type) 
				parser.parseInteger(setting, "value");
				
			parser.parseBoolean(setting, ["advanced", "hidden"]); 
			return setting;
        },
        
		parse: function(settingObj) {
            var setting = this.convert(settingObj);
            setting = _.pick(setting, this.whitelistedAttrs); 
            
            if (setting.enumValues && !_.isArray(setting.enumValues)) {
                setting.enumValues = _.map(setting.enumValues.split("|"), function(val) {
                    var colon = val.indexOf(":");
                    return colon > 0 ? {
                        label: val.slice(colon + 1),
                        value: val.slice(0, colon)
                    } : {
                        label: val,
                        value: val
                    };
                });
            }
             
            if ("enum" === setting.type && setting.values && !_.isArray(setting.values)) {
                var index = 0;
                setting.enumValues = _.map(setting.values.split("|"), function(val) {
                    return {
                        label: val,
                        value: index++
                    };
                });
            }
            return setting;
        },
		
        getSaveParams: function(t) {
			console.warn("getSave");
            /*if (e.has(t, "FriendlyName") && this.server.set("friendlyName", t.FriendlyName), e.has(t, "allowMediaDeletion") && this.server.set("allowMediaDeletion", "1" == t.allowMediaDeletion), e.isEmpty(t)) {
                var i = {};
                return i[this.get("id")] = this.get("value"), i
            }
            return "value" in t && (t[this.get("id")] = t.value, delete t.value), t*/
        }
    });
	
    return serverSettingModel;
});