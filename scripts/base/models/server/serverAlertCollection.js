define([
	"underscore", 
	"base/models/paginatedCollection", 
	"base/models/server/serverAlertModel"
], function(_, paginatedCollection, serverAlertModel) {
    "use strict";
	
    var serverAlertCollection = paginatedCollection.extend({
        model: serverAlertModel,
        url: "/servers",
        pageSize: 20,
        limit: 1e3,
		
        initialize: function() {
            this.startTime = new Date();
			this.listenTo(this.server, "socket:setting", this.onSettingChange); 
			this.listenTo(this.server, "socket:timeline", this.onTimeline); 
			this.listenTo(this.server, "socket:status", this.onStatus); 
			//this.listenTo(this.server, "socket:playing", this.onPlaying); 
			this.listenTo(this.server, "socket:websocket", this.onWebSocket);
        },
		
        add: function() {
            if (this.length + 1 > this.limit) 
				this.remove(this.at(0)); 
				
			paginatedCollection.prototype.add.apply(this, arguments);
        },
		
        onSettingChange: function(setting) {
            this.add(_.extend({
                _type: "setting"
            },setting), {
                parse: true
            });
        },
		
        onTimeline: function(timeline) {
            if (1 !== timeline.state && timeline.sectionID == 0) 
				this.add(_.extend({
                	_type: "timeline"
            	}, timeline), {
                	parse: true
            	});
        },
		
        onStatus: function(status) {
            if ("ProgressNotification" !== status._elementType) 
				this.add(_.extend({
                	_type: "status"
            	}, status), {
                	parse: true
            	});
        },
		
        /*onPlaying: function(t) {
            this.add(e.extend({
                _type: "playing"
            }, t), {
                parse: !0
            })
        },*/
		
        onWebSocket: function(socket) {
            this.add(_.extend({
                _type: "websocket"
            }, socket), {
                parse: true
            });
        }
    });
	
    return serverAlertCollection;
});