define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(e, t, i) {
    "use strict";
    var n = i.extend({
        idAttribute: "key",
        defaults: {
            key: "/services/browse",
            title: "Root",
            path: ""
        },
        whitelistedAttrs: ["key", "title", "path", "home", "network"],
        url: function() {
            return this.server.resolveUrl(this.id)
        },
        convert: function(i) {
            var n = e.isObject(i) ? i : t.parseXML(i);
            return t.parseBoolean(n, ["home", "network"]), n
        }
    });
    return n
});