define(["base/models/baseModel"], function(baseModel) {
    "use strict";
	
    var t = 0;
    var serverAlertModel = baseModel.extend({
        idAttribute: "id",
        
		parse: function(serverAlertObj) {
            var serverAlert = serverAlertObj;
            serverAlert.time = new Date();
			serverAlert.id = "evt" + t++; 
			
			if ("preference" == serverAlert._type) 
				serverAlert.value = serverAlert.value.toString();
				
			return serverAlert;
        }
    });
    return serverAlertModel;
});