define([
	"underscore", 
	"jquery", 
	"base/adapters/storageAdapter", 
	"base/models/storageModel"
], function(_, $, storageAdapter, storageModel) {
    "use strict";
    
	var ServerStorageModel = storageModel.extend({
        storageKey: "connections",
        
        defaults: function() {
            return {
                users: []
            }
        },
        
        initialize: function() {
            this.debouncedSave = _.debounce(this.save, 100);
        },
        
        getCurrentUser: function() {
            var accessToken = storageAdapter.get("myWFAccessToken");
            accessToken = accessToken.then(_.bind(function(result) {
                var users = this.get("users");
                var accessUser = _.findWhere(users, { accessToken: result });
                
                if (!accessUser) {
                    accessUser = { accessToken: result, servers: [] };
                    users.push(accessUser);
                }
                return $.Deferred().resolve(accessUser).promise();
            }, this));
            
            return accessToken;
        },
        
        updateUser: function(newUser) {
            var currentUser = this.getCurrentUser();
            
            currentUser = currentUser.then(_.bind(function(user) {
                user.id = newUser.id; 
                user.title = newUser.get("title"); 
                user.home = newUser.get("home"); 
                user.entitlements = newUser.get("entitlements");
                
                var pin = newUser.get("pin");
                if (pin)
                     user.pin = pin;
                else if (user.pin)
                    delete user.pin; 
                    
                return this.debouncedSave();
            }, this));
            
            return currentUser;
        },
        
        updateLiteServer: function(serverModel, options) {
            if (!options) 
                options = {};
                
            var currentUser = this.getCurrentUser();
            currentUser = currentUser.then(_.bind(function(user) {
                var servers = user.servers;
                var serverMatch = _.findWhere(servers, {
                    machineIdentifier: serverModel.id
                });
                
                if (!serverMatch) {
                    if (!options.add) 
                        return t.Deferred().reject().promise();
                    serverMatch = {
                        connections: []
                    }; 
                    servers.push(serverMatch);
                }
                _.extend(serverMatch, serverModel.toLiteJSON());
                this.debouncedSave(); 
                
                return $.Deferred().resolve(serverMatch).promise();
            }, this));
            
            return currentUser;
        },
        
        updateConnections: function(serverModel, connections) {     // i, n
            var liteServers = this.updateLiteServer(serverModel, {
                add: true
            });
            
            liteServers = liteServers.then(_.bind(function(lite) {
                var connections = _.map(connections, function(connection) {
                    return _.pick(connection, "uri", "scheme", "address", "port", "bundled");
                });
                
                lite.connections = connections; 
                this.debouncedSave(); 
                
                return $.Deferred().resolve(connections).promise();
            }, this));
            
            return liteServers;
        },
        
        removeAllServers: function() {
            console.log('update');
            /*var t = this.getCurrentUser();
            return t.done(e.bind(function(e) {
                e.servers.length = 0, this.debouncedSave()
            }, this)), t*/
        },
        
        removeLiteServer: function(server) {
            var userPromise = this.getCurrentUser();
            
            userPromise.done(_.bind(function(user) {
                var servers = user.servers;
                var idx = _.indexOf(servers, _.findWhere(servers, {
                    machineIdentifier: server.id
                }));
                
                if (idx > -1) {
                    servers.splice(idx, 1); 
                    this.debouncedSave();
                }
            }, this)); 
            
            return userPromise;
        },
        
        pruneLiteServers: function(storedServers, resources) { 
            var currentUser = this.getCurrentUser();
            
            currentUser.done(_.bind(function(user) {
                var resourceServerId = resources.server.id; 
                var servers = user.servers;
                var relevantServers = _.filter(servers, function(server) {
                    return _.contains(server.sources, resourceServerId)
                });
                
                for (var o = relevantServers.length - 1; o > -1; o--) {
                    var relevantServer = relevantServers[o];
                    var relevantId = relevantServer.machineIdentifier;
                    
                    if (!resources.get(relevantId)) {
                        var serverSources = relevantServer.sources;
                        serverSources.splice(_.indexOf(serverSources, resourceServerId), 1);
                        
                        if (!serverSources.length || 1 === serverSources.length && serverSources[0] === relevantId) {
                            var storedServer = storedServers.get(relevantId);
                            if (storedServer) 
                                storedServer.trigger("destroy", storedServer, storedServer.collection); 
                            
                            servers.splice(_.indexOf(servers, relevantServer), 1);
                        }
                        
                        this.debouncedSave();
                    }
                }
            }, this));
            
            return currentUser;
        }
    });
	
    return new ServerStorageModel();
});