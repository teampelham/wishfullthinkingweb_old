define([
    "underscore", 
    "base/models/baseCollection", 
    "base/models/server/serverSettingModel"
], function(_, baseCollection, serverSettingModel) {
    "use strict";
    
    var serverSettingCollection = baseCollection.extend({
        model: serverSettingModel,
        url: "/web/prefs",
        
        initialize: function() {
            this.server.on("socket:settingObj", function(settingObj) {
                var settingId = this.get(settingObj.id);
                if (settingId) {
                    settingObj = settingId.parse(settingObj); 
                    settingId.set("value", settingObj.value); 
                    if ("FriendlyName" === settingObj.id && settingObj.value) 
                        this.server.set("friendlyName", settingObj.value); 
                    else if ("allowMediaDeletion" === settingObj.id) 
                        this.server.set("allowMediaDeletion", settingObj.value);
                }
            }, this);
        },
        
        parse: function(serverSettingObj) {
            var serverSettings = this.convert(serverSettingObj, "Setting");
            return _.filter(serverSettings, function(setting) {
                return setting.id;
            });
        }
        
    });
    return serverSettingCollection;
});