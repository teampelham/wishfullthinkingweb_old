define([
	"base/models/library/metadataCollection", 
	"base/models/server/sessionModel"
], function(metadataCollection, sessionModel) {
    "use strict";
	
    var sessionCollection = metadataCollection.extend({
        model: sessionModel,
        url: "/web/currentsessions"
    });
	
    return sessionCollection;
});