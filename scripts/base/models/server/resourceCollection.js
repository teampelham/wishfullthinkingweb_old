define([
	"jquery", 
	"base/models/config", 
	"base/models/baseCollection", 
	"base/models/server/resourceModel"
], function($, config, baseCollection, resourceModel) {
    "use strict";
    
	var resourceCollection = baseCollection.extend({
        model: resourceModel,
		
        url: function() {
			var url = "/api/resources.php";
            var params = {};
            if (config.get("supportsHttpsMediaServer")) 
                params.includeHttps = 1; 
                
            if (!config.get("requestAllConnections")) 
                params.filterByPublic = 1; 
            
            if (!$.isEmptyObject(params)) 
                url += "?" + _.param(params, true);
                
            return url;
        },
		
        parse: function(resourceObj) {
            return this.convert(resourceObj, "Device");
        }
    });
	
    return resourceCollection;
});