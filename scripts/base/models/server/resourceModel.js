define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, parser, baseModel) {
    "use strict";
	
    var resourceModel = baseModel.extend({
        idAttribute: "clientIdentifier",
        urlRoot: "/api/resources",
		
        whitelistedAttrs: ["clientIdentifier", "name", "sourceTitle", "accessToken", "product", "productVersion", "platform", "platformVersion", "device", "provides", "owned", "synced", "presence", "createdAt", "lastSeenAt", "Connection", "publicAddressMatches"],
        
		convert: function(resourceObj) {
            var resource = _.isObject(resourceObj) ? resourceObj : parser.parseXML(resourceObj);
            if (resource.Device) 
                resource = resource.Device;
                 
            parser.parseBoolean(resource, ["owned", "synced", "presence", "publicAddressMatches"]); 
            parser.parseInteger(resource, ["createdAt", "lastSeenAt"]); 
            parser.parseArray(resource, "Connection");
             
            return resource;
        },
        
        parse: function(resourceObj) {
            var resource = this.convert(resourceObj);
            resource = _.pick(resource, this.whitelistedAttrs); 
            
            if (resource.provides) 
                resource.provides = resource.provides.split(","); 
            
            if (_.contains(resource.provides, "sync-target")) 
                resource.synced = true;
                 
            return resource;
        }
    });
	
    return resourceModel;
});