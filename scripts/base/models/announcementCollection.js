define([
	"base/models/baseCollection", 
	"base/models/announcementModel"
], function(baseCollection, announcementModel) {
    "use strict";
    
	var announcementCollection = baseCollection.extend({
        model: announcementModel,
        url: "http://app.wishfullthinking.net/wfs/announcements.json?cb=" + (new Date).getTime(),
        
        parse: function(announcements) {
            return JSON.parse(announcements);
        }
    });
    
    return announcementCollection;
});