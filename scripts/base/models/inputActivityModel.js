define([
	"underscore", 
	"backbone", 
	"base/adapters/settingsAdapter"
], function(_, bb, settingsAdapter) {
    "use strict";
    var inputActivityModel = bb.Model.extend({
        AWAKE: "awake",
        ASLEEP: "asleep",
        IDLE: "idle",
        NONE: "none",
        
        defaults: {
            state: "none",
            idleTime: void 0,
            sleepTime: void 0
        },
        
        initialize: function(t) {			
            //t || (t = {}), 
			_.bindAll(this, "onIdleTimeout", "onSleepTimeout");
			this._locks = {};
			this.listenTo(this, {
                "change:idleTime": this.notice,
                "change:sleepTime": this.notice
            });
        },
        /*notice: function() {
            {
                var e;
                this.get("enabled")
            }
            this.timer = clearTimeout(this.timer), this.has("idleTime") && !this.isLocked() ? (this.timer = setTimeout(this.onIdleTimeout, this.get("idleTime")), e = this.AWAKE) : e = this.NONE, this.set("state", e)
        },
        isInactive: function() {
            var e = this.get("state");
            return e === this.IDLE || e === this.ASLEEP
        },
        lock: function(e) {
            this._locks[e] = !0, this.notice()
        },
        unlock: function(e) {
            delete this._locks[e], this.notice()
        },
        toggleLock: function(e, t) {
            t ? this.lock(e) : this.unlock(e)
        },
        isLocked: function() {
            return e.keys(this._locks).length > 0
        },*/
        onIdleTimeout: function() {
            console.warn('onTimeout');
            //this.set("state", this.IDLE);
            //this.timer = this.has("sleepTime") ? setTimeout(this.onSleepTimeout, this.get("sleepTime")) : clearTimeout(this.timer);
        },
        
        onSleepTimeout: function() {
            console.warn('onSleep');
            //this.set("state", this.ASLEEP);
            //this.timer = clearTimeout(this.timer);
        }
    });
	
    return inputActivityModel;
});