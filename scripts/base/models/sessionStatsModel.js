define([
	"underscore", 
	"base/models/storageModel"
], function(_, storageModel) {
    "use strict";
	
    var SessionStatsModel = storageModel.extend({
        storageKey: "sessionstats",
		
        incPlaybackCount: function() {
			console.warn('playback');
            this.save("playbackCount", this.get("playbackCount") + 1);
        },
		
        startSession: function() {
			console.warn('start!');
            /*var t = (new Date).getTime();
            this.save({
                playbackCount: 0,
                startTime: t,
                heartbeatTime: t
            }), setInterval(e.bind(function() {
                this.save("heartbeatTime", (new Date).getTime())
            }, this), 1e4)*/
        }
    });
	
    return new SessionStatsModel();
});