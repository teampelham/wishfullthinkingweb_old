define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, parser, baseModel) {
    "use strict";
	
    var homeUserModel = baseModel.extend({
        urlRoot: "/api/home/users.php",
        whitelistedAttrs: ["id", "admin", "protected", "restricted", "guest", "title", "username", "email", "thumb"],
		
        convert: function(homeUserObj) {
            var homeUser = _.isObject(homeUserObj) ? homeUserObj : parser.parseXML(homeUserObj);
            
			if (homeUser.User) 
				homeUser = homeUser.User;
			
			parser.parseInteger(homeUser, "id"); 
			parser.parseBoolean(homeUser, ["admin", "protected", "restricted", "guest"]);
			 
			return homeUser;
        },
		
        parse: function(homeUserObj) {
            var homeUser = this.convert(homeUserObj);
            homeUser = _.pick(homeUser, this.whitelistedAttrs);
			
			return homeUser;
        },
		
        getSaveParams: function(params) {
            if (_.isEmpty(params)) {
                var saveParams = {};
                if (this.get("restricted")) 
					saveParams.title = this.get("title"); 
				else if (this.isNew()) 
					saveParams.invitedEmail = this.get("username");
				return saveParams;
            }
            return baseModel.prototype.getSaveParams.apply(this, arguments);
        }
    });
	
    return homeUserModel;
});