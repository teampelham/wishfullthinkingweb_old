define([
	"underscore", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, parser, baseModel) {
    "use strict";
	
    var homeModel = baseModel.extend({
        url: "/api/home.php",
		whitelistedAttrs: ["id", "guestUserID", "guestEnabled", "name"],
        
		convert: function(modelObj) {
            var model = _.isObject(modelObj) ? modelObj : parser.parseXML(modelObj);
            parser.parseInteger(model, "id");
			parser.parseBoolean(model, ["guestEnabled"]);
			return model;
        },
		
        parse: function(modelObj) {
            var model = this.convert(modelObj);
            model = _.pick(model, this.whitelistedAttrs);
			return model;
        },
		
        getSaveParams: function(model) {
            return _.isEmpty(model) ? {
                guestEnabled: this.get("guestEnabled") ? "1" : "0"
            } : baseModel.prototype.getSaveParams.apply(this, arguments);
        }
    });
	
    return homeModel;
});