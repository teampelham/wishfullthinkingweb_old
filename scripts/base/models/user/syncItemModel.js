define([
	"underscore", 
	"base/utils/parser", 
	"base/utils/syncUtil", 
	"base/models/baseModel"
], function(_, parser, syncUtil, baseModel) {
    "use strict";
    
	var syncItemModel = baseModel.extend({
        whitelistedAttrs: ["id", "version", "rootTitle", "title", "metadataType", "clientIdentifier", "Server", "Status", "MediaSettings", "Policy", "Location"],
        
		sync: function(e, t, header) {
            var versionHeader = syncUtil.addVersionHeader(header, this.device);
            return baseModel.prototype.sync.call(this, e, t, versionHeader);
        },
        
		convert: function(syncObj) {
            var syncItem = _.isObject(syncObj) ? syncObj : parser.parseXML(syncObj);
            parser.parseInteger(syncItem, ["id", "version"]);
			return syncItem;
        },
        
		parse: function(syncObj) {
            var syncItem = this.convert(syncObj);
            syncItem = _.pick(syncItem, this.whitelistedAttrs); 
			
			if (syncItem.Location && syncItem.Location.uri) 	
				syncItem.isDirectory = /library\:\/\/([\w-]+)\/directory\//.test(syncItem.Location.uri); 
			
			return syncItem;
        }
    });
	
    return syncItemModel;
});