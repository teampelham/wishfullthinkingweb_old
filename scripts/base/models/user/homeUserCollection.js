define([
	"base/models/baseCollection", 
	"base/models/user/homeUserModel"
], function(baseCollection, homeUserModel) {
    "use strict";
	
    var homeUserCollection = baseCollection.extend({
        model: homeUserModel,
        url: "/api/home/users.php",
		
        parse: function(collection) {
            return this.convert(collection, "User")
        }
    });
	
    return homeUserCollection;
});