define([
	"underscore", 
	"base/models/baseCollection", 
	"base/models/user/deviceModel"
], function(_, baseCollection, deviceModel) {
    "use strict";
	
    var deviceCollection = baseCollection.extend({
        model: deviceModel,
        url: "/devices.xml",
		
        parse: function(deviceObj) {
            return this.convert(deviceObj, "Device")
        },
        
		filterSyncDevices: function(devices) {
            var hasDevices = !(!devices || !devices.channel);
            var filteredDevices = this.filter(function(device) {
				return device.supports("sync-target") && (!hasDevices || !device.get("isCloudSync"));
			});
            var sortedDevices = _.sortBy(filteredDevices, function(device) {
				return !device.get("isCloudSync");
			});
            return this.clone(sortedDevices);
        },
        
		filterByProducts: function(product) {
            var filtered = this.filter(function(device) {
                return _.contains(product, device.get("product"));
            });
            return this.clone(filtered);
        },
        
		products: function() {
            var products = this.map(function(device) {
                return _.get("product");
            });
			
            return _.uniq(products);
        }
    });
	
    return deviceCollection;
});