define([
	"underscore", 
	"jquery", 
	"base/utils/parser", 
	"base/adapters/loggerAdapter", 	// n
	"base/adapters/storageAdapter", 	// s
	"base/adapters/settingsAdapter", 	// a
	"base/models/baseModel", 			// r
	"base/models/server/serverStorageModel",		// o 
	"base/models/user/deviceCollection", 			// l
	"base/models/user/homeUserCollection", 		// c
	//"base/models/user/friendCollection", 			// d
	"base/models/user/inviteCollection", 			// u
	//"base/models/user/socialNetworkCollection", 	// h
	"base/models/user/homeModel", 					// p
	//"jssha256"										
], function(_, $, parser, loggerAdapter, storageAdapter, settingsAdapter, baseModel, serverStorageModel, DeviceCollection, HomeUserCollection, InviteCollection, HomeModel) {
    "use strict";
    var userModel = baseModel.extend({
        url: "/users/account.php",
        
		defaults: function() {
            var initObj = {
                server: this.server,
                parent: this
            };
            return {
                signedIn: null,
                hasInvalidToken: false,
                subscription: false,
                locked: null,
                devices: new DeviceCollection([], initObj),
                homeUsers: new HomeUserCollection([], initObj),
                invites: new InviteCollection([], initObj),
                /*friends: new d([], t),
                
                sentInvites: new u([], e.extend({
                    subset: "requested"
                }, t)),
                socialNetworks: new h([], t),*/
                homeDetails: new HomeModel({}, initObj)
            }
        },
		// whitelistedAttrs seem to be a way to specify "public" methods for exposing via inheritance (i.e. _.extend)
        whitelistedAttrs: ["id", "pin", "restricted", "home", "maxHomeSize", "username", "email", "thumb", "title", "queueUid", "queueEmail", "accessToken", "guest"],
        initialize: function() {
            this.listenTo(this.server, "change:available", this.onServerAvailableChange);
        },
		
        convert: function(userObj) {
            var user = _.isObject(userObj) ? userObj : parser.parseXML(userObj);
            var subscription = user.subscription;
            var entitlements = user.entitlements;
            
            parser.parseInteger(user, ["id", "maxHomeSize"]);
            parser.parseBoolean(user, ["restricted", "home", "guest"]);
            parser.parseArray(user, "roles");
            
            if (subscription) {
                parser.parseBoolean(subscription, "active"); 
                parser.parseArray(subscription, "feature");
            }
            
            if (entitlements) {
                parser.parseBoolean(entitlements, "all");
                parser.parseArray(entitlements, "entitlement");
            }
            
            return user;
        },
        
        parse: function(userObj) {
            var user = this.convert(userObj);
            var roles = user.roles;
            var subscription = user.subscription;
            var entitlements = user.entitlements;
            var attrs = [];
            
            user.accessToken = user.authenticationToken;
            user = _.pick(user, this.whitelistedAttrs); 
            user.signedIn = true; 
            user.hasInvalidToken = false;
            
            if (subscription) {
                var feature = subscription.feature;
                user.subscription = subscription.active;
                user.plan = subscription.plan;
                attrs.push("subscription: " + user.plan); 
                
                user.features = _.reduce(feature, function(memo, feature) {
                    memo[feature.id] = true;
                    return memo;
                }, {});
                
                attrs.push("features: " + _.keys(user.features).join(", "));
            } 
            else {
                attrs.push("features: none");
                user.features = void 0;
                user.plan = void 0;
                user.subscription = void 0;
            }
            
            if (roles && roles.length) {
                roles = roles[0].role; 
                
                user.roles = _.reduce(roles, function(memo, role) {
                    memo[role.id] = true; 
                    return memo;
                }, {}); 
                
                attrs.push("roles: " + _.keys(user.roles).join(", "));
            }
            else 
                user.roles = void 0;
            
            if (entitlements) { 
                _(entitlements.entitlement).each(function(entitlement) {
                    entitlements[entitlement.id] = true;
                }); 
                user.entitlements = _(entitlements).omit("entitlement");
                attrs.push("entitlements: " + _.keys(user.entitlements).join(", "));
            }
            else
                attrs.push("entitlements: none"); 
            
            loggerAdapter.group(user.title, attrs);
            
            return user;
        },
        
        switchUser: function(xml, remember) {
            var user = this.parse(xml);
            var accessToken = user.accessToken;
            var servers = this.parent.get("servers");
            
            servers.resetPopulatePromise($.Deferred()); 
            if (this.parent.get("primaryServer") === false)
                this.parent.set("primaryServer", null);
            
            var token = storageAdapter.set("myWFAccessToken", accessToken, {
                session: !remember.remember
            });
            
            token.always(_.bind(function() {
                this.server.set("accessToken", accessToken); 
                this.set(user);
                this.resolveServerPromise();
            }, this));
            
            return token;
        },
        
        resolveServerPromise: function() {
            var signedIn = this.get("signedIn");
            var status = signedIn ? "resolved" : "rejected";
            
            this.server.updatePopulatePromise(status); 
            this.server.set("authorized", signedIn);
        },
        
        clearAccountAttributes: function(attrs) {
            if (!attrs) 
                attrs = {}; 
            this.clear();
            this.set(_.extend({}, _.result(this, "defaults"), attrs)); 
            
            if (attrs.hasInvalidToken) {
                this.server.get("resources").reset(); 
                this.clearAccessToken();
            }
        },
        
        clearAccessToken: function() {
            this.server.set("accessToken", null);
            
            var clearPromise = storageAdapter.clear({
                session: true
            });
            
            clearPromise = clearPromise.then(function() {
                if (window.WFWEB && window.WFWEB.myWFAccessToken) 
                    delete window.WFWEB.myWFAccessToken;
                    
                return storageAdapter.remove("myWFAccessToken");
            });
             
            clearPromise = clearPromise.then(function() {
                serverStorageModel.clear(); 
                return serverStorageModel.save(_.result(serverStorageModel, "defaults"));   // reset the server storage to defaults
            });
            
            return clearPromise;
        },
        
        hasPremiumPass: function() {
            var features = this.get("features");
            return !!(features && "pass" in features);
        },
        
        isFullUser: function() {
            return !!this.get("username");
        },
        /*
        hashPIN: function(e, t) {
            return t || (t = this.get("accessToken")), SHA256_hash(e + t)
        },
        
        isEmployee: function() {
            var e = this.get("roles");
            return !!(e && "employee" in e)
        },
        isNinja: function() {
            var e = this.get("roles");
            return this.isEmployee() || !! (e && "ninja" in e)
        },
        
        hasEntitlement: function(e) {
            var t = this.get("entitlements");
            return !(!t || !t.all && !t[e])
        },
        hasSync: function() {
            var e = this.get("features");
            return !!(e && "sync" in e)
        },
        hasHome: function() {
            return !!this.get("pin") || this.get("homeUsers").length > 1
        },*/
        onServerAvailableChange: function(serverManager, connected) {   // t, i
			if (connected == null)
                return;
                
            if (connected === false) {
                this.clearAccountAttributes({       // not signed in once the server goes away
                    signedIn: false
                });
                return void this.resolveServerPromise();
            }
            // connection must be true
            if (this.get("signedIn"))
                this.set("locked", false);      // release the lock on the user
                
            var accessToken = storageAdapter.get("myWFAccessToken", {
                session: true       // store the token in the session 
            });
            
            var successFn = _.bind(function(token) {
                this.server.set("accessToken", token);
                
                var homeDetails = this.get("homeDetails");  
                var promise = this.fetch();
                var homePromise = homeDetails.fetch();
                
                promise.done(_.bind(function() {
                    homePromise.always(_.bind(function() {
                        if (this.get("locked") !== false) {
                                var autoLogin = settingsAdapter.get("autoLogin");
                                var isHome = !! this.get("home");
                                var isUnAuthGuest = !! this.get("guest") && !homeDetails.get("guestEnabled");
                                this.set("locked", isHome && (isUnAuthGuest || !autoLogin));
                        }
                    }, this));
                }, this));
                
                promise.fail(_.bind(function(event, response) {
                    this.clearAccountAttributes({
                        signedIn: false,
                        hasInvalidToken: 401 === response.status || 422 === response.status
                    });
                }, this));
                
                promise.always(_.bind(function() {
                    this.resolveServerPromise();
                }, this));
                
            }, this);
            
            var failFn = _.bind(function() {
                this.clearAccountAttributes({
                    signedIn: false
                }); 
                this.resolveServerPromise();
            }, this);
            
            accessToken = accessToken.then(function(token) {     // success
                return token;
            }, function() {                                     // fail
                return storageAdapter.get("myWFAccessToken");
            });
            
            accessToken.done(successFn).fail(failFn);
            
        }
    });
	
    return userModel;
});