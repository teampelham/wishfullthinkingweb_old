define([
	"underscore", 
	"base/utils/parser",
	"base/models/baseModel", 
	"base/models/user/syncItemCollection"
], function(_, parser, baseModel, SyncItemCollection) {
    "use strict";
	
    var deviceModel = baseModel.extend({
        whitelistedAttrs: ["id", "clientIdentifier", "name", "product", "productVersion", "platform", "platformVersion", "device", "model", "provides", "screenResolution", "screenDensity", "lastSeenAt", "SyncList"],
        
		url: function() {
            return "/devices/" + this.id + ".xml"
        },
        
		initialize: function() {
            this.set("syncItems", new SyncItemCollection([], {
                server: this.server,
                device: this
            }));
        },
        
		convert: function(deviceObj) {
            var device = _.isObject(deviceObj) ? deviceObj : parser.parseXML(deviceObj);
            parser.parseInteger(device, "lastSeenAt");
			return device;
        },
        
		parse: function(deviceObj) {
            var device = this.convert(deviceObj);
            device = _.pick(device, this.whitelistedAttrs); 
			device.isCloudSync = "cloudsync" === device.platform; 
			device.provides = device.provides ? device.provides.split(",") : [];
			return device;
        },
        
		supports: function(fn) {
            var provides = this.get("provides");
            return _.indexOf(provides, fn) > -1;
        }
    });
	
    return deviceModel;
});