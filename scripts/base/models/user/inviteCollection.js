define([
	"backbone.obscura", 
	"base/models/baseCollection", 
	"base/models/user/inviteModel"
], function(Obscura, baseCollection, inviteModel) {
    "use strict";
	
    var inviteCollection = baseCollection.extend({
        model: inviteModel,
        
		url: function() {
            return "/api/invites/" + this.subset;
        },
		
        initialize: function(e, initObj) {
            if (initObj) 
				initObj = {};
			
			this.subset = initObj.subset || "requests";
        },
        
		parse: function(inviteObj) {
            return this.convert(inviteObj, "Invite");
        },
        
		filterByHome: function() {
            var obscure = new Obscura(this);
            
			obscure.filterBy(function(invite) {
                return invite.get("home");
            });
			
			return obscure;
        },
        
		filterByFriends: function() {
            var obscure = new Obscura(this);
            
			obscure.filterBy(function(invite) {
                return !invite.get("home");
            });
			
			return obscure;
        }
    });
	
    return inviteCollection;
});