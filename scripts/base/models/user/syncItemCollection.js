define([
	"underscore", 
	"base/utils/syncUtil", 
	"base/models/baseCollection", 
	"base/models/user/syncItemModel"
], function(_, syncUtil, baseCollection, syncItemModel) {
    "use strict";
	
    var syncItemCollection = baseCollection.extend({
        model: syncItemModel,
        
		url: function() {
            return this.device ? "/devices/" + this.device.get("clientIdentifier") + "/sync_items" : "/servers/" + this.parent.id + "/sync_lists";
        },
        
		sync: function(e, n, header) {
            var versionHeader = syncUtil.addVersionHeader(header, this.device);
            return baseCollection.prototype.sync.call(this, e, n, versionHeader);
        },
        
		parse: function(syncObj) {
            var sync = this.convert(syncObj);
            var syncArrary = [];
            var toClientArray = function(clientID, syncItem) {
				if (syncItem) {
					if (!_.isArray(syncItem)) 
						syncItem = [syncItem]; 
					
					_.each(syncItem, function(prop) {
						prop.clientIdentifier = clientID; 
						syncArrary.push(prop);
					});
				}
			};
			
            if (sync.SyncList && !_.isArray(sync.SyncList)) 
				sync.SyncList = [sync.SyncList]; 
			
			_.each(sync.SyncList, function(syncItem) {
                toClientArray(syncItem.clientIdentifier, syncItem.SyncItems.SyncItem);
            }); 
			
			if (sync.SyncItems)  
				toClientArray(sync.clientIdentifier, sync.SyncItems.SyncItem); 
				
			return syncArrary;
        },
        
		filterByServer: function(serverID) {
            var matches = this.filter(function(device) {
                var server = device.get("Server");
                return server && server.machineIdentifier === serverID;
            });
			
            return this.clone(matches);
        }
    });
	
    return syncItemCollection;
});