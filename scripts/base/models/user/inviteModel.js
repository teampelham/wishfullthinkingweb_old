define([
	"underscore", 
	"jquery", 
	"base/utils/parser", 
	"base/models/baseModel"
], function(_, $, parser, baseModel) {
    "use strict";
	
    var inviteModel = baseModel.extend({
        whitelistedAttrs: ["id", "friend", "server", "home", "friendlyName", "username", "email", "thumb", "createdAt", "Server"],
        urlRoot: "/api/invites/requests",
        
		url: function() {
            var url;
            if (this.isNew()) 
				url = _.result(this, "urlRoot");
            else {
                var keys = this.pick("friend", "server", "home");
                _.each(_.keys(keys), function(e) {
                    keys[e] = keys[e] ? "1" : "0";
                }); 
				url = this.collection ? _.result(this.collection, "url") : _.result(this, "urlRoot"); 
				url = url.replace(/([^\/])$/, "$1/") + encodeURIComponent(this.get("id")); 
				url += "?" + $.param(keys, true);
            }
            return url;
        },
        
		convert: function(inviteObj) {
            var invite = _.isObject(inviteObj) ? inviteObj : parser.parseXML(inviteObj);
            parser.parseInteger(invite, "createdAt"); 
			parser.parseBoolean(invite, ["friend", "server", "home"]); 
			parser.parseArray(invite, "Server");
            
			var server = invite.Server;
            _.each(server, function(serverObj) {
                parser.parseInteger(serverObj, "numLibraries")
            });
			
			return invite;
        },
        
		parse: function(inviteObj) {
            var invite = this.convert(inviteObj);
            return invite = _.pick(invite, this.whitelistedAttrs);
        }
    });
	
    return inviteModel;
});