define([
    "underscore", 
    "backbone", 
    "jquery", 
    "base/utils/parser", 
    "base/utils/cache/modelCache", 
    "base/mixins/models/populateMixin"
], function(_, bb, $, n, ModelCache, populateMixin) {
    "use strict";

    var baseModel = bb.Model.extend({
        mixins: [populateMixin],
        cache: new ModelCache,
        
        constructor: function(key, options) {   // i, n
            key = key || {};
            options = options || {};
            this.server = this.findOption(options, "server");
            this.section = this.findOption(options, "section");
            this.channel = this.findOption(options, "channel");
            this.device = this.findOption(options, "device");
            this.parent = this.findOption(options, "parent");
            
            var skipCache = this.findOption(options, "skipCache");
            if (skipCache)
                return void bb.Model.prototype.constructor.apply(this, arguments);
            
            var itemCacheKey = this.cache.getItemCacheKey(this, key);
            var itemCache = this.cache.get(itemCacheKey);
            
            if (itemCache) {
                if (options.parse) 
                    key = itemCache.parse.call(itemCache, key); 
                
                if (this.preserveCachedAttributes && this.preserveCachedAttributes(options.url)) {
                    return _.each(key, function(item, idx) {
                        if (_.isUndefined(itemCache.get(idx)) && !_.isUndefined(item)) 
                            itemCache.set(idx, item);
                    }); 
                }
                else { 
                    itemCache.set(key); 
                    itemCache.server = this.server || itemCache.server; 
                    itemCache.section = this.section || itemCache.section; 
                    itemCache.channel = this.channel || itemCache.channel; 
                    itemCache.device = this.device || itemCache.device; 
                    itemCache.parent = this.parent || itemCache.parent; 
                    
                    if (options.collection) 
                        itemCache.collection = options.collection;
                    
                    return itemCache;
                }
            }
            
            var onlyIfCached = this.findOption(options, "onlyIfCached");
            if (onlyIfCached)
                return {};
                
            bb.Model.prototype.constructor.apply(this, arguments);
            return void this.cache.store(this, itemCacheKey);
        },
        
        findOption: function(e, t) {
            return e[t] || e.collection && e.collection[t];
        },
        
        clone: function() {
            var e = {
                server: this.server,
                section: this.section,
                channel: this.channel,
                parent: this.parent,
                skipCache: true
            };
            return new this.constructor(this.attributes, e) || null;
        },
        
        convert: function(t) {
            var i = _.isObject(t) ? t : n.parseXML(t);
            return i
        },
        
        parse: function(t) {
            var i = this.convert(t);
            return this.whitelistedAttrs && (i = _.pick(i, this.whitelistedAttrs)), i
        },
        
        save: function(property, value, cb) {   // n, s, a
            var attributes = this.attributes;   // c
            var changes;                    // r
            
            if (property == null || typeof property == "object") {
                changes = property;
                cb = value;
            }
            else {
                changes = {};
                changes[property] = value;
                cb = _.extend({
                    validate: true
                }, cb);
            }
            
            cb = cb || {};
            
            var saveParams = this.getSaveParams(changes, cb);
            _.extend(saveParams, cb.data);
            if (changes && !cb.wait) {
                if (!this.set(changes, cb))
                    return false;
            }
            else if (!this._validate(changes, cb))
                return false;
            
            if (changes && cb.wait)
                this.attributes = _.extend({}, attributes, changes);
            if (_.isEmpty(saveParams) && !cb.force) {
                if (cb.success) 
                    cb.success(this);
                return $.Deferred().resolve(this);
            }
            
            if (cb.parse === 0)
                cb.parse = true;
                
            var self = this;    // u
            var success = cb.success; // h
            
            cb.success = function(result) { // t
                self.attributes = attributes;
                if (result) {
                    var saveResults = self.parse(result, cb);
                    if (cb.wait)
                        saveResults = _.extend(changes || {}, saveResults);
                    
                    if (_.isObject(saveResults) && !self.set(saveResults, cb))
                        return false;
                    
                    self.cache.store(self);
                }
                if (success)
                    success(self, result, cb);
                self.trigger("sync", self, result, cb);
            };
            
            bb.wrapError(this, cb);
            var verb = this.isNew() ? "create" : cb.patch ? "patch" : "update";
            
            if (verb === "patch")
                cb.attrs = changes;
            
            var url = _.result(this, "saveUrl") || _.result(this, "url");   // p
            if (!_.isEmpty(saveParams)) {
                var delimiter = url.indexOf("?") > 0 ? "&" : "?";
                cb.url = url + delimiter + $.param(saveParams, true);
            }
            cb.data = "";
            var syncResult = this.sync(verb, this, cb);
            if (changes && cb.wait)
                this.attributes = attributes;            
            
            return syncResult;
            
            /*var r, o, l, c = this.attributes;
            null == n || "object" == typeof n ? (r = n, a = s) : (r = {})[n] = s, a = e.extend({
                validate: !0
            }, a);
            var d = this.getSaveParams(r, a);
            if (e.extend(d, a.data), r && !a.wait) {
                if (!this.set(r, a)) return !1
            } else if (!this._validate(r, a)) return !1;
            if (r && a.wait && (this.attributes = e.extend({}, c, r)), e.isEmpty(d) && !a.force) return a.success && a.success(this), i.Deferred().resolve(this);
            void 0 === a.parse && (a.parse = !0);
            var u = this,
                h = a.success;
            a.success = function(t) {
                if (u.attributes = c, t) {
                    var i = u.parse(t, a);
                    if (a.wait && (i = e.extend(r || {}, i)), e.isObject(i) && !u.set(i, a)) return !1;
                    u.cache.store(u)
                }
                h && h(u, t, a), u.trigger("sync", u, t, a)
            }, t.wrapError(this, a), o = this.isNew() ? "create" : a.patch ? "patch" : "update", "patch" === o && (a.attrs = r);
            var p = e.result(this, "saveUrl") || e.result(this, "url");
            if (!e.isEmpty(d)) {
                var m = p.indexOf("?") > 0 ? "&" : "?";
                a.url = p + m + i.param(d, !0)
            }
            return a.data = "", l = this.sync(o, this, a), r && a.wait && (this.attributes = c), l*/
        },
        
        getSaveParams: function(e) {
            return this.isNew() ? e || this.toJSON() : this.changedAttributes(e) || {}
        },
        
        getScopedKey: function(t) {
            return null != t && _.isFunction(this.cacheKey) ? this.cacheKey(this.attributes) + "!" + t : t
        },
        
        restoreDefaults: function(t) {
            var i = _.result(this, "defaults");
            if (!i) return this.clear(t);
            var n, s = this.attributes;
            for (n in s) n in i || this.unset(n, t);
            return this.set(i, t), this
        }
    });
    
    baseModel.extend = function() {
        var model = bb.Model.extend.apply(this, arguments);
        model.prototype.cache = new ModelCache();
        return model;
    } 
    
    return baseModel;
});