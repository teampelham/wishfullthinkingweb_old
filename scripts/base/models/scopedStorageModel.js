define([
	"underscore", 
	"jquery", 
	"base/utils/promiseUtil", 
	"base/models/baseModel"
], function(_, $, promiseUtil, baseModel) {
    "use strict";
	
    var scopedStorageModel = baseModel.extend({
        fetch: function() {
            return this.storage.fetch().then(_.bind(function() {
                var storageKey = this.storage.get(_.result(this, "storageKey"));
                return storageKey ? (this.set(storageKey), this) : $.Deferred().reject();
            }, this));
        },
		
        save: function() {
            return promiseUtil.alwaysResolve(this.storage.fetch()).then(_.bind(function() {
                return this.storage.save(_.result(this, "storageKey"), this.toJSON());
            }, this));
        }
    });
	
    return scopedStorageModel;
});