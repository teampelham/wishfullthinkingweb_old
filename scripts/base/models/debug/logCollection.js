
define([
    "underscore", 
    "jquery", 
    "backbone", 
    "base/adapters/loggerAdapter", 
    "base/adapters/settingsAdapter", 
    "base/models/debug/debugModel", 
    "base/models/debug/logModel"
], function(_, $, bb, loggerAdapter, settingsAdapter, debugModel, logModel) {
    "use strict";
    
    /*var o = /(auth_token=|accessToken=|X-Plex-Token=)(\"?\w+\"?)/g,
        l = /(email=)(\"?[^&]+\"?)/g,
        c = /(password=)([^&]+)/,
        d = /(address=|host=)(\"?[^&]+\"?)/g,
        u = function(e) {
            return e = e.replace(o, "$1TOKEN"), e.replace(c, "$1PASSWORD")
        }, h = function(e) {
            return e && e.hasOwnProperty("X-Plex-Token") && (e["X-Plex-Token"] = "TOKEN"), e && e.hasOwnProperty("Authorization") && (e.Authorization = "BASIC AUTHORIZATION"), e
        }, p = function(e) {
            return e ? (e = e.replace(o, '$1"TOKEN"'), e = e.replace(l, '$1"EMAIL"'), e = e.replace(d, '$1"IP ADRRESS"')) : void 0
        };*/
        
    var logCollection = bb.Collection.extend({
        model: logModel,
        
        initialize: function(t, initParams) {    // why is this seeded with an empty array?
            _.bindAll(this, "onAjaxComplete");  // hijack the onAjaxComplete method so we can listen in
            this.parent = initParams.parent; 
            debugModel.parent = initParams.parent; 
            
            if (settingsAdapter.hasInstance())
                this.onSettingsAdapterInstanceChange();
            
            this.listenTo(settingsAdapter, "change:instance", this.onSettingsAdapterInstanceChange); 
            loggerAdapter.on("change:instance", this.onLoggerChange, this); 
            this.updateLogger();
        },
       
        updateLogger: function() {
            // detach the current logger
            if (this.logger) {
                this.logger.off("add", this.onLogAdd, this);
                this.logger = null;
            } 
            
            // attach to the current loggerAdapter
            if (loggerAdapter.hasInstance()) {
                this.logger = loggerAdapter.getInstance(); 
                this.startListening();
            }
        },
        
        startListening: function() {
            // hijack the logger's add method
            if (!this.enabled && this.logger && settingsAdapter.get("debugLevel")) {    // Debug Level?
                this.logger.on("add", this.onLogAdd, this); 
                bb.history.on("route", this.onRoute, this);
                $(document).on("ajaxComplete", this.onAjaxComplete);
                this.enabled = true;
            }
        },
        
        stopListening: function() {
            if (!this.logger) 
                return;    
            
            this.logger.off("add", this.onLogAdd, this);            // remove the event listeners
            bb.history.off("route", this.onRoute, this);            // tell Backbone to ignore routing
            $(document).off("ajaxComplete", this.onAjaxComplete)    // tell jQuery to leave us off the ajaxComplete list
            this.reset();                                           // what's reset do?
            this.enabled = false;
        },
        
        /*add: function() {
            this.length > 200 && this.shift(), i.Collection.prototype.add.apply(this, arguments)
        },
        toJSON: function() {
            var t = i.Collection.prototype.toJSON.apply(this, arguments);
            return t.unshift(e.extend({
                type: "info"
            }, a.toJSON())), t
        },
        toJSONString: function() {
            return JSON.stringify(this.toJSON(), null, 2)
        },*/
        
        onDebugLevelChange: function() {
            if(settingsAdapter.get("debugLevel") === 0)
                this.stopListening();
            else 
                this.startListening()
        },
        
        onLoggerChange: function() {
            this.updateLogger();
        },
        
        onSettingsAdapterInstanceChange: function() {
            if (this.settings) 
                this.stopListening(this.settings);          // ignore the old instance
            
            this.settings = settingsAdapter.getInstance();  // get the new instance
            
            if (this.settings) {
                // Make sure we know to update if the debug settings are changed
                this.listenTo(this.settings, "change:debugLevel", this.onDebugLevelChange);
                this.onDebugLevelChange();
            }
        },
        
        onLogAdd: function(e) {
            console.warn('logged');
            //this.add(e)
        },
        
        onRoute: function(e, t, i) {
            console.warn('routed');
            /*var n = {
                type: "navigation",
                route: t,
                args: i,
                fragment: window.location.hash
            };
            this.add(n)*/
        },
        
        onAjaxComplete: function(t, i, n) {
            console.warn('onajaxcomplete');
            /*var a = {
                type: "ajax",
                url: u(n.url),
                status: i.status
            };
            e.isEmpty(n.headers) || (a.headers = h(n.headers)), e.isEmpty(n.data) || (a.params = n.data), 200 === i.status ? 2 === s.get("debugLevel") ? a.response = p(i.responseText) : a.responseLength = i.responseText ? i.responseText.length : 0 : (a.statusText = i.statusText, a.response = p(i.responseText)), this.add(a)*/
        }
    });
        
    return logCollection;
})