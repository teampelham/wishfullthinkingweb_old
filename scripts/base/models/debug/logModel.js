define(["backbone"], function(bb) {
    "use strict";
    
	var logModel = bb.Model.extend({
        initialize: function() {
            this.set("time", new Date);
        }
    });
	
    return logModel;
});