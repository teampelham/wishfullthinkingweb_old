define([
	"underscore", 
	"backbone", 
	"base/utils/browser"
], function(_, bb, browser) {
    "use strict";
	
    var DebugModel = bb.Model.extend({
        initialize: function() {
            this.set("host", window.location.host); 
			this.set("userAgent", window.navigator.userAgent); 
			this.set("browser", browser);
        },
        /*toJSON: function() {
            var i = this.parent.get("primaryServer"),
                n = this.parent.get("user"),
                s = n.get("features");
            return this.set("version", this.parent.get("version")), i && (this.set("primaryServerUrl", i.url()), this.set("primaryServerAuthToken", !! i.get("accessToken")), this.set("primaryServerVersion", i.get("versionFull"))), this.set("username", n.get("username")), this.set("cloudUrl", n.server.get("connections")), this.set("cloudAccessToken", !! n.server.get("accessToken")), s && this.set("userFeatures", e.keys(s).join(", ")), t.Model.prototype.toJSON.apply(this, arguments)
        }*/
    });
	
    return new DebugModel();
});