define([
	"underscore", 
	"backbone", 
	"base/adapters/storageAdapter"
], function(_, bb, storageAdapter) {
    "use strict";

    function coalesce(defaults, attributes) {
        return defaults ? _.reduce(attributes, function(val, idx, attrs) {
            if (defaults[attrs] !== idx) 
                val[attrs] = idx; 
                return val;
        }, {}) : attributes;
    }
    
    var storageModel = bb.Model.extend({
        save: function(key, value, promise) {
            var itemMap; 
            var attributes = this.attributes;
            
            // in this scenario, key is an object map to save
            if (key === null || typeof key == "object") {
                itemMap = key; 
                promise = value || {}; 
            }
            // otherwise key and item line up
            else {
                (itemMap = {})[key] = value; 
                promise = _.extend({
                    validate: true
                }, promise); 
            }
            
            if (itemMap && !promise.wait) {
                if (!this.set(itemMap, promise)) 
                    return false;
            } 
            else if (!this._validate(itemMap, promise)) 
                return false;
            
            if (itemMap && promise.wait) 
                this.attributes = _.extend({}, attributes, itemMap); 
            
            if (promise.parse === void 0) 
                promise.parse = true;
            
            var self = this;
            var origSuccess = promise.success;
            
            promise.success = function() {
                self.attributes = attributes; 
                if (origSuccess) 
                    origSuccess(self, attributes, promise); 
                self.trigger("sync", self, attributes, promise);
            }; 
            
            bb.wrapError(this, promise);
            
            var defaultAttributes = this.serialize(coalesce(_.result(this, "defaults"), this.attributes));
            var attributeStoragePromise = storageAdapter.set(this._getStorageKey(), defaultAttributes);
            
            attributeStoragePromise.done(promise.success).fail(promise.error);
            
            if (itemMap && promise.wait) 
                this.attributes = attributes; 
            
            return attributeStoragePromise;
        },
        
        populate: function() {
            // there's no intention to populate this model, set up a new promise from fetch
            if (!this.populatePromise)
                this.populatePromise = this.fetch.apply(this, arguments);
                 
            return this.populatePromise;
        },
        
        fetch: function(options) {
            // options forwarded from populate, create a deep copy to leave the original intact
            options = options ? _.clone(options) : {};
            
            if (void 0 === options.parse)   // who would do such a thing?
                options.parse = true;
            
            var self = this;
            var success = options.success;  // capture the original success callback
            
            options.success = function(result) {        // create a new success callback
                if (self.set(self.parse(result, options), options)) {
                    if (success)                        // if there was an original callback, call it now 
                        success(self, result, options);
                    
                    return void self.trigger("sync", self, result, options);
                }
                return false;
            };
            
            bb.wrapError(this, options);    // custom error hook so that we can track and fire events
            
            // now comes time to fetch, get the superclass storage key and retrieve this instance from storage,
            // providing overridden callbacks 
            var store = storageAdapter.get(this._getStorageKey());
            store.done(options.success).fail(options.error);
            this.populatePromise = store;
            
            return store;
        },
        
        destroy: function(n) {
            console.warn('destroy');
            /*n = n ? e.clone(n) : {};
            var s = this,
                a = n.success,
                r = function() {
                    s.trigger("destroy", s, s.collection, n)
                };
            n.success = function(e) {
                n.wait && r(), a && a(s, e, n), s.trigger("sync", s, e, n)
            }, t.wrapError(this, n);
            var o = i.remove(this._getStorageKey());
            return o.done(n.success).fail(n.error), n.wait || r(), o*/
        },
        
        serialize: function(item) {
            return JSON.stringify(item);
        },
        
        parse: function(item) {
            return JSON.parse(item);
        },
        
        sync: function() {
            throw new Error("storage model doesn`t allow syncing to server")
        },
        
        _getStorageKey: function() {
            var storageKey = _.result(this, "storageKey");
            
            if (!storageKey) 
                throw new Error("storageKey is required");
            
            return storageKey;
        }
    });
	
    return storageModel;
});