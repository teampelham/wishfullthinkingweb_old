// AppModel holds other models and keeps track of basic application-level information
define([
	"backbone", 
	"base/models/announcementCollection", 
	"base/models/debug/logCollection", 
	"base/models/server/serverCollection", 
	"base/models/server/cloudServerModel", 
	//"base/models/player/PlexPlayerCollection", 
	"base/models/user/userModel", 
	"base/models/inputActivityModel"
], function(bb, AnnouncementCollection, LogCollection, ServerCollection, CloudServerModel, UserModel, InputActivityModel) {
    "use strict";
    var AppModel = bb.Model.extend({
        defaults: function() {
            return {
                version: "0.0.1",			// Figure out what these defaults are used for
                //updateState: "none",
                authorized: false,
                multiserver: false,
                multiplayer: false,
                //isCastApiAvailable: !1,
                //isCastDeviceAvailable: !1,
                history: [],
                forwardHistory: []
            }
        },
		
        initialize: function() {
            var cloudServer = new CloudServerModel({
                machineIdentifier: "myWishFull"
            }, {
                parent: this
            });

			// backbone.Model method, takes a key/value set or an object map
            this.set({
                announcements: new AnnouncementCollection(),
                inputActivity: new InputActivityModel(),
                logs: new LogCollection([], {
                    parent: this
                }),
                /*players: new PlexPlayerCollection([], {
                    parent: this
                }),*/
                servers: new ServerCollection([cloudServer], {
                    parent: this
                }),
                user: new UserModel({}, {
                    server: cloudServer,
                    parent: this
                })
            });
        }
    });
	
    return new AppModel();
});