define(["base/models/baseCollection"], function(baseCollection) {
    "use strict";
	
    var paginatedCollection = baseCollection.extend({
        pageSize: 10,
        pages: function() {
            return Math.max(Math.ceil(this.length / this.pageSize), 1);
        },
		
        page: function(page, option) {
            option = option || {};
            var start, end = 0;
            start = option.reverse ? this.length - page * this.pageSize : page * this.pageSize - this.pageSize; 
			
			if (0 > start) { 
				end = start; 
				start = 0;
			} 
			return this.models.slice(start, start + this.pageSize + end);
        }
    });
	
    return paginatedCollection;
});