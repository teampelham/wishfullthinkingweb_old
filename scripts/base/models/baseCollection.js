define([
	"underscore", 
	"backbone", 
	"jquery", 
	"base/utils/parser", 
	"base/utils/cache/collectionCache", 
	"base/mixins/models/populateMixin"
], function(_, bb, $, parser, CollectionCache, populateMixin) {
    "use strict";
	
    var baseCollection = bb.Collection.extend({
        mixins: [populateMixin],
        cache: new CollectionCache(),
        
        constructor: function(e, init) {
            if (!init) 
                init = {};
            // Try to init some basic common properties
            this.server = init.server; 
            this.section = init.section;
            this.channel = init.channel; 
            this.device = init.device; 
            this.parent = init.parent; 
            this.allowInCache = init.allowInCache;
            this.persistInCache = init.persistInCache;
            this.skipCache = init.skipCache;
            
            if (init.url) 
                this.url = init.url;
                
            if (init.path)
                this.path = init.path;
            
            if (null != init.pageStart || null != init.pageSize) {
                this.pageStart = init.pageStart || 0; 
                this.pageSize = init.pageSize || 0;
            } 
            
            if (init.populated) 
                this.resetPopulatePromise($.Deferred().resolve().promise()); 
            
            if (!init.allowInCache) 
                return void bb.Collection.prototype.constructor.apply(this, arguments);
                
            this.initialize.apply(this, arguments);
            var cacheKey = this.cache.getItemCacheKey(this);
            var cached = this.cache.get(cacheKey);
            
            if (cached) 
                return cached;
            else if (init.onlyIfCached) 
                return {}; 
                
            bb.Collection.prototype.constructor.apply(this, arguments);
            return void this.cache.store(this, cacheKey);
        },
        
        
        sync: function() {
            if (this.allowInCache) 
                this.cache.store(this); 
                
            return bb.sync.apply(this, arguments);
        },
        
        syncUrl: function() {
            var url = "";
            if (this.url) 
                url = _.result(this, "url");
                
            if (this.path) {
                var path = _.result(this, "path");
                url += ("/" !== url.substr(url.length - 1)) ? "/" : ""; 
                url += ("/" === path[0]) ? path.substr(1) : path;
            }
            
            if (null != this.pageStart || null != this.pageSize) {
                var pageStart = this.pageStart || 0;
                var pageSize = this.pageSize || 0;
                var headers = {};
                headers["Container_Start"] = pageStart; 
                headers["Container_Size"] = pageSize;
                var hasParams = url.indexOf("?");
                var urlLength = url.length;
                url += (hasParams > -1) ? (urlLength - 1 > hasParams) ? "&" : "" : "?"; 
                url += $.param(headers, true);
            }
            
            return url;
        },
        
        convert: function(obj, returnProperty) {
            var newObj = _.isObject(obj) ? obj : parser.parseXML(obj);
            if (!returnProperty)
                return newObj;
            
            var rv = newObj[returnProperty];
            if (rv) { 
                if (!_.isArray(rv)) 
                    rv = [rv];
                return rv;
            } 
            return [];
        },
        
        clone: function(collection) {
            var initObj = {
                server: this.server,
                section: this.section,
                parent: this.parent,
                url: this.url
            };
            if (!collection) 
                collection = this.models; 
            
            return new this.constructor(collection, initObj);
        },
        
        cloneDeep: function(e) {
            console.warn('cloneDeep');
            /*return e || (e = this.map(function(e) {
                return e.clone()
            })), this.clone(e)*/
        },
        
        clear: function(items) {
            items = _.extend({
                silent: true
            }, items); 
            
            this.abortPendingPopulate(); 
            this.resetPopulatePromise(null); 
            this.reset([], items);
        }
    });
    
	baseCollection.extend = function() {
        var collection = bb.Collection.extend.apply(this, arguments);
        collection.prototype.cache = new CollectionCache();
        return collection;
    };
	
	return baseCollection;
});