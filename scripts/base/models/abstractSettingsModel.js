define([
	"base/utils/languageUtil", 
	"base/models/storageModel"
], function(languageUtil, storageModel) {
    "use strict";
    
	var header;
	var languageOption = window.WFWEB_OPTIONS && window.WFWEB_OPTIONS.acceptLanguage;	// Where/why should I set this?
    var navigator = window.navigator;
	
    if (languageOption) 
		header = languageUtil.parseHeader(languageOption);
	else if (navigator.languages) 
		header = navigator.languages; 
	else if (navigator.language) 
		header = [navigator.language]; 
	else if (navigator.userLanguage) 
		header = [navigator.userLanguage];
    
	var language = languageUtil.closestLanguage(header) || "en";
    var abstractSettingsModel = storageModel.extend({
        defaults: {
            language: language,
            autoLogin: false,
            extrasPrefixCount: 0,
            debugLevel: 0
        }
    });
	
    return abstractSettingsModel;
});