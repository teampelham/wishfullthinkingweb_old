define([
	"underscore", 
	"base/utils/t"
], function(_, t) {
    "use strict";
	
    var metadataUtil = {
        /*formatBitrate: function(e, t) {
            var i = ["bps", "kbps", "Mbps", "Gbps", "Tbps"],
                n = parseInt(Math.floor(Math.log(e) / Math.log(1e3)), 10);
            return (e / Math.pow(1e3, n)).toFixed(t) + " " + i[n + 1]
        },
        formatVideoQuality: function(e) {
            var n = e.bitrate,
                s = e.videoResolution;
            if (!n && !s) return t("Unknown");
            var a = [];
            if (n) {
                var r = 1e3 > n || n % 1e3 === 0 ? 0 : 1;
                a.push(this.formatBitrate(n, r))
            }
            if (s && (!n || n >= 1e3)) {
                var o = 0; //i.getResolutionFromSize(s);
                a.push(this.formatResolution(o))
            }
            return a.join(" ")
        },
        formatBytes: function(e) {
            var t = ["B", "KB", "MB", "GB", "TB"],
                i = parseInt(Math.floor(Math.log(e) / Math.log(1024)), 10);
            return (e / Math.pow(1024, i)).toFixed(2) + " " + t[i]
        },
        formatVideoLevel: function(e) {
            return e = parseInt(e, 10), (e / 10).toFixed(1)
        },
        formatResolution: function(t) {
            if (!t) return "";
            if ("8k" === t) return "8K";
            if ("4k" === t) return "4K";
            var i = parseInt(t, 10);
            return e.isNaN(i) ? t.toUpperCase() : t + "p"
        },
        formatAudioCodec: function(e) {
            return e ? "dca" === e ? "DTS" : e.toUpperCase() : ""
        },
        formatAudioChannels: function(e) {
            return e = parseInt(e, 10), e ? 2 > e ? t("Mono") : 3 > e ? t("Stereo") : (e - 1).toString() + ".1" : ""
        },
        formatStreamLanguage: function(e, i) {
            if (!i) return t("None");
            var n = i.language || t("Unknown");
            return "subtitles" === e ? i.codec && (n += " (" + i.codec.toUpperCase(), i.forced && (n += " " + t("Forced")), n += ")") : "audio" === e && i.codec && (n += " (" + this.formatAudioCodec(i.codec), i.channels && (n += " " + this.formatAudioChannels(i.channels)), n += ")"), n
        }*/
    };
	
    return metadataUtil;
});