define([
	"underscore", 
	"moment", 
	"base/utils/t"
], function(e, t, i) {
    "use strict";
	
    var dateTimeUtil = {
		
        formatHoursMinutes: function(time) {
            var duration = t.duration(time);
            var totalHours = duration.hours() + 24 * duration.days();
            var minutes = duration.minutes();
            var seconds = duration.seconds();
            
            if (totalHours && minutes) 
                i("{1} hr {2} min", totalHours, minutes);
            else if (totalHours) 
                i("{1} hr", totalHours);
            else if (minutes) 
                i("{1} min", minutes);
            else if (seconds > 0) 
                i("{1} sec", seconds); 
            
            return "";
        },
		
        formatMinutes: function(e) {
			console.warn("dateTime");
            /*var n = t.duration(e),
                s = 24 * n.days() * 60,
                a = 60 * n.hours(),
                r = n.minutes() + a + s,
                o = n.seconds();
            return r ? i("{1} min", r) : o > 0 ? i("{1} sec", o) : ""*/
        },
		
        formatDaysHours: function(e) {
			console.warn("dateTime");
            /*var n = t.duration(e),
                s = n.days(),
                a = n.hours(),
                r = i(s > 1 ? "days" : "day"),
                o = i(a > 1 || 0 >= a ? "hours" : "hour");
            return s && a ? s + " " + r + ", " + a + " " + o : s ? s + " " + r : a + " " + o*/
        },
		
        formatDays: function(e) {
			console.warn("dateTime");
            /*var n = t.duration(e),
                s = 365 * n.years(),
                a = 30 * n.months(),
                r = s + a + n.days(),
                o = i(r > 1 || 0 >= r ? "days" : "day");
            return r + " " + o*/
        },
		
        formatBroadDuration: function(e) {
			console.warn("dateTime");
            //return 54e5 >= e ? this.formatMinutes(e) : 864e5 >= e ? this.formatHoursMinutes(e) : 6048e5 >= e ? this.formatDaysHours(e) : this.formatDays(e)
        },
		
        formatDate: function(date, format) {
            if(!format) 
                format = "MMMM D, YYYY";
            return date ? t(date).format(format) : "";
        },
		
        formatTimestamp: function(e, i) {
			console.warn("dateTime");
            //return i || (i = "MMMM D, YYYY"), e ? t.unix(e).format(i) : ""
        },
		
        formatDuration: function(i) {
			console.warn("dateTime");
            /*if (0 >= i || !e.isNumber(i)) return "0:00";
            var n = t.duration(i),
                s = n.seconds(),
                a = n.minutes(),
                r = n.hours() + 24 * n.days(),
                o = "";
            return o = 10 > s ? ":0" + s : ":" + s, o = 10 > a && r > 0 ? "0" + a + o : a + o, r > 0 && (o = r + ":" + o), o*/
        },
		
        formatMinutesRemaining: function(e) {
			console.warn("dateTime");
			/*
            if (!e) return "";
            if (60 > e) return 1 === e ? i("{1} second left", e) : i("{1} seconds left", e);
            var t = Math.ceil(e / 60);
            return 1 === t ? i("{1} minute left", t) : i("{1} minutes left", t)*/
        },
		
        formatTimeAgo: function(e) {
			console.warn("dateTime");
            //return e ? t.unix(e).fromNow() : ""
        }
    };
	
    return dateTimeUtil;
});