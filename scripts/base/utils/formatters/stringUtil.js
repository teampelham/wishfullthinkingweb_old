define([
	"underscore", 
	"base/utils/t"
], function(_, t) {
    "use strict";
    var ascii = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var stringUtil = {
        
        encode64: function(inputStr) {
            var inChar1, inChar2, shift1, shift2, shift3;
            var outputStr = "";
            var inChar3 = "";
            var shift4 = "";
            var idx = 0;
            do {
                inChar1 = inputStr.charCodeAt(idx++); 
                inChar2 = inputStr.charCodeAt(idx++); 
                inChar3 = inputStr.charCodeAt(idx++);
                 
                shift1 = inChar1 >> 2; 
                shift2 = (3 & inChar1) << 4 | inChar2 >> 4; 
                shift3 = (15 & inChar2) << 2 | inChar3 >> 6; 
                shift4 = 63 & inChar3;
                
                if (isNaN(inChar2)) 
                    shift3 = shift4 = 64;
                else if (isNaN(inChar3))
                    shift4 = 64; 
                
                outputStr = outputStr + ascii.charAt(shift1) + ascii.charAt(shift2) + ascii.charAt(shift3) + ascii.charAt(shift4);
                // reset vars
                inChar1 = inChar2 = inChar3 = ""; 
                shift1 = shift2 = shift3 = shift4 = "";
            } while (idx < inputStr.length);
            
            return outputStr;
        },
        pluralize: function(e, t, i) {
            return null == i && (i = e + "s"), 1 === t ? e : i;
        },
        /*decode64: function(e) {
            var t, n, s, a, r, o = "",
                l = "",
                c = "",
                d = 0,
                u = /[^A-Za-z0-9\+\/\=]/g;
            if (u.exec(e)) throw new Error('There were invalid base64 characters in the input text.\nValid base64 characters are A-Z, a-z, 0-9, "+", "/", and "="\nExpect errors in decoding.');
            e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            do s = i.indexOf(e.charAt(d++)), a = i.indexOf(e.charAt(d++)), r = i.indexOf(e.charAt(d++)), c = i.indexOf(e.charAt(d++)), t = s << 2 | a >> 4, n = (15 & a) << 4 | r >> 2, l = (3 & r) << 6 | c, o += String.fromCharCode(t), 64 != r && (o += String.fromCharCode(n)), 64 != c && (o += String.fromCharCode(l)), t = n = l = "", s = a = r = c = ""; while (d < e.length);
            return unescape(o)
        },*/
        
        toUtf8: function(inStr) {
            var outStr = "";
            var i = 0;
            for (var n = 0; n < inStr.length; n++) {
                var charCode = inStr.charCodeAt(n);
                if (!(128 > charCode)) {
                    if (2048 > charCode) { 
                        if (n > i) 
                            outStr += inStr.substring(i, n); 
                        i = n + 1; 
                        outStr += String.fromCharCode(192 | charCode >>> 6); 
                        outStr += String.fromCharCode(128 | 63 & charCode);
                    }
                    else {  
                        if (n > i) 
                            outStr += inStr.substring(i, n); 
                        i = n + 1; 
                        outStr += String.fromCharCode(224 | charCode >>> 12);
                        outStr += String.fromCharCode(128 | charCode >>> 6 & 63); 
                        outStr += String.fromCharCode(128 | 63 & charCode);
                    }
                }
            }
            
            if (i > 0 && i < inStr.length) 
                outStr += inStr.substring(i);
            
            return i === 0 ? inStr : outStr;
        },
        /*
        truncate: function(e, t) {
            if (!e) return "";
            if (e.length > t) {
                for (var i = e.substring(0, t - 3);
                    /\S/.test(i.charAt(i.length - 1)) === !1 && i.length > 1;) i = i.slice(0, -1);
                return i + "..."
            }
            return e
        },
        truncateMiddle: function(e, t, i) {
            if (!e) return "";
            if (e.length > t) {
                i = i || Math.floor((t - 3) / 2);
                var n = t - 3 - i;
                return e.substr(0, i) + "..." + e.substr(e.length - n)
            }
            return e
        },
        
        formatLabel: function(e) {
            for (var t = "", i = 0, n = e.length; n > i; i++) {
                var s = e.charAt(i),
                    a = s.toUpperCase();
                t += 0 === i ? a : s === a ? " " + a : s
            }
            return t
        },
        formatBoolean: function(e) {
            return t(e && 0 !== parseInt(e, 10) ? "Yes" : "No")
        },
        upperCase: function(e) {
            return (e || "").toUpperCase()
        },
        upperCaseFirstLetter: function(e) {
            return e.charAt(0).toUpperCase() + e.slice(1)
        },
        lowerCaseFirstLetter: function(e) {
            return e.charAt(0).toLowerCase() + e.slice(1)
        },
        titleCase: function(e) {
            //return e.replace(
			/\w\S*///g, 	remove two of the slashes
			/*function(e) {
                return e.charAt(0).toUpperCase() + e.substr(1).toLowerCase()
            })
        },
        getLastSegment: function(t) {
            return e.first(/[^\/\\]+$/.exec(t || "") || [])
        },*/
        random: function() {
            return Math.random().toString(36).substr(2);
        },
		/*
        shortFilename: function(e, t) {
            return e ? n.truncateMiddle(n.getLastSegment(e), t) : void 0
        },*/
        
        escapeRegExp: function(reg) {
            return reg.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
        },
        
        stableJSON: function(json) {
            return JSON.stringify(_.reduce(_.keys(json).sort(), function(memo, item) {
                memo[item] = json[item];
                return memo;
            }, {}));
        }
    };
		
    return stringUtil;
});