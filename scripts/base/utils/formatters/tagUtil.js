define([
    "underscore", 
    "base/utils/formatters/stringUtil", 
    "base/utils/types/mediaTypes"
], function(e, t, i) {
    "use strict";

    var n = {
        truncateTagList: function(i) {
            var n = i.tags,
                s = i.separator || ", ",
                a = "";
            if (n && n.length) {
                var r = i.maxTags || n.length;
                r > n.length && (r = n.length);
                for (var o = 0; r > o; o++) {
                    var l = n[o],
                        c = e.escape(t.truncate(l.tag, i.maxLen));
                    "undefined" != typeof l.tag && "" !== l.tag && (a += c, r > o + 1 && (a += s))
                }
            }
            return a
        },
        truncateLinkedTagList: function(n) {
            var s = n.model.section,
                a = i.getMediaTypeByString(n.model.get("type")),
                r = n.tags,
                o = s && s.get("allowAdvancedFilters"),
                l = "",
                c = function(i) {
                    var r = e.escape(t.truncate(i.tag, n.maxLen));
                    if (!o || !i.id) return r;
                    var l = ["#!/server/" + encodeURIComponent(s.server.id), "/section/" + encodeURIComponent(s.id), "/filter", "/" + encodeURIComponent(a.id), "/" + encodeURIComponent(n.key), "/" + encodeURIComponent(i.id), "/" + encodeURIComponent(i.tag)];
                    return '<a class="pivot-link" href="' + l.join("") + '">' + r + "</a>"
                };
            if (r && r.length) {
                var d = n.maxTags || r.length;
                d > r.length && (d = r.length);
                for (var u = 0; d > u; u++) {
                    var h = r[u];
                    "undefined" != typeof h.tag && "" !== h.tag && (l += c(h), d > u + 1 && (l += ", "))
                }
            }
            return l
        }
    };
    return n
});