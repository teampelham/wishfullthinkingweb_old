define(["underscore"], function(e) {
    "use strict";
    var t = {
        leadingZero: function(t) {
            if (t = parseInt(t, 10), e.isNaN(t)) return "";
            var i = 10 > t ? "0" : "";
            return i + t
        },
        bytesToSize: function(t, i) {
            if (t = parseInt(t, 10), i = i || 1, e.isNaN(t)) return "";
            if (0 === t) return "0 Bytes";
            var n = ["Bytes", "KB", "MB", "GB", "TB"],
                s = parseInt(Math.floor(Math.log(t) / Math.log(1024)), 10);
            return (t / Math.pow(1024, s)).toFixed(i) + " " + n[s]
        }
    };
    return t
});