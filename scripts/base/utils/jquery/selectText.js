define(["jquery-lib"], function($) {
    "use strict";
    
	$.fn.selectText = function() {
        var range; 
		var element = this[0];
		
        if (document.body.createTextRange) { 
			range = document.body.createTextRange(), 
			range.moveToElementText(element), 
			range.select();
		}
        else if (window.getSelection) {
            var selection = window.getSelection();
            range = document.createRange();
			range.selectNodeContents(element); 
			selection.removeAllRanges();
			selection.addRange(range);
        }
    };
});