define(["jquery-lib"], function($) {
    "use strict";
    
	$.fn.isDisabled = function() {
        return this.hasClass("disabled") || this.hasClass("active") || this.hasClass("hidden");
    };
});