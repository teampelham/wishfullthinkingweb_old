define(["jquery-lib"], function($) {
    "use strict";
	
    $.fn.triggerReflow = function() {
        var element = this[0];
        if (element)
			element.offsetHeight;
		return this;
    };
});