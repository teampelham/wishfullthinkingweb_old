define(["underscore", "jquery"], function(e, t) {
    "use strict";

    function i(i) {
        return i = e.isArray(i) ? i : e.toArray(arguments), t.when.apply(t, e.map(i, s))
    }

    function n(i) {
        if (!i.length) return t.Deferred().reject().promise();
        var n = t.Deferred(),
            s = i.length;
        return e.each(i, function(e) {
            e.done(function() {
                s--, n.resolveWith(this, arguments)
            }).fail(function() {
                s--, 0 === s && n.reject()
            })
        }), n.promise()
    }

    function s(e) {
        var i = t.Deferred();
        return e.always(function() {
            i.resolve.apply(i, arguments)
        }), i.promise()
    }
    var a = {
        whenEach: i,
        whenFirstSucceed: n,
        alwaysResolve: s
    };
    return a
});