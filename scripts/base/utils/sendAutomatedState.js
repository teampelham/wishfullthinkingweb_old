define([
    "underscore", 
    "jquery", 
], function(_, $) {
    "use strict";

    function sendAutomatedState(autoDeviceModel, isOnState) {
        var defer = $.Deferred(); 
        var autoDeviceServer = autoDeviceModel.server;
        var connection = autoDeviceServer.get("activeConnection");
        
        var url = connection.uri + autoDeviceModel.getUrl();
        url += "&level=" + (isOnState ? "100" : "0");      // TODO: check url string for ?
        
        var xhr = new XMLHttpRequest;
            
        xhr.open("GET", url, true); 
        this.xhr = xhr;
            
        xhr.onreadystatechange = _.bind(function() {
            if (4 === xhr.readyState) {
                xhr.onreadystatechange = null; 
                this.xhr = null; 
                
                if (200 === xhr.status) {
                    var response = xhr.response;
                    
                    defer.resolve(response);        // TODO: Convert to JSON? or something else?
                } 
                else 
                    defer.reject();
            }
        }, this); 
        xhr.send();
            
        return defer.promise();
    }
    
    return sendAutomatedState;
});