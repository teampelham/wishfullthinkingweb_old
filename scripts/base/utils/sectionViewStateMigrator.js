define([
	"underscore", 
	"jquery", 
	"base/adapters/settingsAdapter", 
	"base/adapters/storageAdapter"
], function(_, $, settingsAdapter, storageAdapter) {
    "use strict";

    function migrateStuff() {
        var instance = settingsAdapter.getInstance();
        var sectionViewState = storageAdapter.get("sectionViewState");
        var defer = $.Deferred();
        
        sectionViewState.then(function(result) {
            try {
                defer.resolve(JSON.parse(result));
            } catch (t) {
                defer.resolve({});
            }
        }).fail(function() {
            defer.resolve({});
        });
        
        defer.promise().then(function(result) {
            _.each(instance.attributes, function(attr, prop) {
                var subset = prop.match(/(^.+)!subset$/);
                if (subset) {
                    var notAttr = subset[1] + "!attr";
                    
                    if (!result[notAttr])
                        result[notAttr] = {
                            subset: attr
                        };
                    
                    instance.unset(prop);
                }
            });
            return $.when(storageAdapter.set("sectionViewState", JSON.stringify(result)), instance.save());
        });
    }
    
    var sectionViewStateMigrator = {
        migrate: migrateStuff
    };
    
    return sectionViewStateMigrator;
});