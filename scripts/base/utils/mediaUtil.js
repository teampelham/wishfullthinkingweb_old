define([
	"underscore", 
	"jquery", 
	"moment", 
	"base/utils/t", 
	//"base/utils/formatters/numberUtil", 
	//"base/utils/types/streamTypes", 
	"base/adapters/loggerAdapter"
], function(_, $, moment, t, loggerAdapter) {
    "use strict";
    var mediaUtil = {
        capabilities: "protocols=http-live-streaming,http-mp4-streaming,http-streaming-video,http-streaming-video-720p,http-mp4-video,http-mp4-video-720p;videoDecoders=h264{profile:high&resolution:1080&level:51};audioDecoders=mp3,aac{bitrate:160000}",
        serverTimelineDelay: 1e4,
        pingDelay: 3e4,
        playbackDelay: 1e3,
        /*timeline: function(i, n) {
            var s = i.item,
                a = i.media,
                o = i.part,
                l = s.get("ratingKey"),
                c = s.get("type");
            if (!l) return t.Deferred().resolve().promise();
            var d = {
                ratingKey: l,
                key: s.get("key"),
                state: i.state
            };
            if (i.continuing && (d.continuing = "1"), s.get("playQueueItemID") && (d.playQueueItemID = s.get("playQueueItemID")), !("photo" === c || a && o)) return t.Deferred().resolve().promise();
            if ("photo" !== c) {
                var u = s.get("viewOffset") || 0,
                    h = a.duration;
                if (e.isNumber(i.position) && (u = i.position, u = this.getAbsoluteOffset(a, o, u)), h || e.isNumber(i.duration) && (h = i.duration, a.duration = h), !h) return t.Deferred().resolve().promise();
                u > h && (u = h), d.time = u, d.duration = h, "track" !== c && s.isLibraryItem() && this.setViewOffset(s, u, h)
            }
            var p = "timeline: " + d.state;
            null != d.time && (p += ", " + d.time + "/" + d.duration), r.log(p);
            var m = "/:/timeline?" + t.param(d, !0);
            return n = e.extend(s.server.getXHROptions(m), n), t.ajax(n)
        },
        setViewOffset: function(e, t, i) {
            1 !== e.get("extraType") && (6e4 > t || (t > .9 * i ? (e.set("viewOffset", 0), e.set("viewCount", 1)) : e.set("viewOffset", t)))
        },
        ping: function(e, i) {
            if (!i) return t.Deferred().resolve().promise();
            r.log("ping transcoder");
            var n;
            return n = e.supports("universalTranscoder") ? "/video/:/transcode/universal/ping?session=" + i : "/video/:/transcode/segmented/ping?session=" + i, t.ajax(e.getXHROptions(n))
        },
        stop: function(i, n, s) {
            if (!n) return t.Deferred().resolve().promise();
            r.log("stop transcoder");
            var a;
            return a = i.supports("universalTranscoder") ? "/video/:/transcode/universal/stop?session=" + n : "/video/:/transcode/segmented/stop?session=" + n, s = e.extend(i.getXHROptions(a), s), t.ajax(s)
        },
        getMediaIndex: function(t, i) {
            var n = t.get("Media"),
                s = e.indexOf(n, e.findWhere(n, {
                    id: i
                }));
            return s
        },
        getPartIndex: function(t, i) {
            var n = t.Part,
                s = e.indexOf(n, e.findWhere(n, {
                    key: i
                }));
            return s
        },
        getPartCount: function(e) {
            return e ? e.Part.length : 0
        },
        getStreams: function(t, i) {
            var n = [];
            if (t) {
                var s = e.first(t.Part);
                s && s.Stream && n.push.apply(n, e.where(s.Stream, i))
            }
            return n
        },
        getSelectedVideo: function(t) {
            var i = a.getStreamTypeByString("video"),
                n = this.getStreams(t, {
                    streamType: i.id
                });
            return e.first(n)
        },
        getSelectedAudio: function(t) {
            var i = a.getStreamTypeByString("audio"),
                n = this.getStreams(t, {
                    streamType: i.id,
                    selected: !0
                });
            return e.first(n)
        },
        getSelectedSubtitles: function(t) {
            var i = a.getStreamTypeByString("subtitles"),
                n = this.getStreams(t, {
                    streamType: i.id,
                    selected: !0
                }),
                s = e.first(n);
            return s && s.id > 0 ? s : void 0
        },
        selectStream: function(t, i, n) {
            if (t) {
                var s = this.getStreams(t, {
                    streamType: i.id
                });
                e.each(s, function(e) {
                    e.selected = e.id === n
                })
            }
        },
        getMissingParts: function(t) {
            var i = [];
            return e.isArray(t) || (t = [t]), e.each(t, function(t) {
                var n = t.Part;
                n && e.each(n, function(e) {
                    (e.exists === !1 || e.accessible === !1) && i.push(e)
                })
            }), i
        },
        getRelativeOffset: function(e, t, i) {
            var n = i;
            if (e.Part.length > 1)
                for (var s = 0, a = e.Part.length; a > s; s++) {
                    var r = e.Part[s];
                    if (!r.duration) break;
                    if (t.key === r.key) break;
                    n -= r.duration
                }
            return n
        },
        getAbsoluteOffset: function(e, t, i) {
            var n = i;
            if (e.Part.length > 1)
                for (var s = 0, a = e.Part.length; a > s; s++) {
                    var r = e.Part[s];
                    if (!r.duration) break;
                    if (t.key === r.key) break;
                    n += r.duration
                }
            return n
        },
        getPartByAbsoluteOffset: function(t, i) {
            var n;
            if (t.Part.length > 1)
                for (var s = 0, a = 0, r = t.Part.length; r > a && (n = t.Part[a], n.duration) && (s += n.duration, !(s > i)); a++);
            else n = e.first(t.Part);
            return n
        },
        getScreenshotIndex: function(t, i) {
            if (t.indexes) {
                var n = t.indexes.split(",");
                if (n.length) return e.indexOf(n, i) > -1 ? i : n[0]
            }
        },
        getScreenshotUrl: function(e, t, i) {
            var n = this.getPartByAbsoluteOffset(e, t);
            if (i = this.getScreenshotIndex(n, i)) {
                var s = this.getRelativeOffset(e, n, t);
                return n.key.replace(/[^\/]*$/, "") + "indexes/" + i + "/" + s
            }
        },
        getSeasonIndex: function(e) {
            var t = e.seasonPrefix || "S{1}",
                i = e.seasonIndex;
            return i > 1e3 ? i : (e.noLeadingZero || (i = s.leadingZero(i)), n(t, i))
        },
        getSeasonEpisodeIndex: function(e) {
            var t = e.seasonPrefix || "S{1}",
                a = e.episodePrefix || "E{1}",
                r = e.seasonIndex,
                o = e.episodeIndex;
            return o > 0 && 1e3 > r ? (e.noLeadingZero || (r = s.leadingZero(r), o = s.leadingZero(o)), n(t, r) + n(a, o)) : e.date ? i(e.date).format("M/D/YY") : ""
        },
        getEpisodeIndex: function(e) {
            var t = e.episodePrefix || "E{1}",
                a = e.episodeIndex || 0;
            return a > 0 ? (e.noLeadingZero || (a = s.leadingZero(a)), n(t, a)) : e.date ? i(e.date).format("M/D/YY") : ""
        },
        isMediaAnalysisRequired: function(t) {
            var i = t ? e.first(t.get("Media")) : null;
            return !(!i || i.container)
        }*/
    };
	
    return mediaUtil;
});