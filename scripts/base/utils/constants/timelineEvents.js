define("plex/base/utils/constants/timelineEvents", [], function() {
    "use strict";
    var timelineEvents = {
        CREATED: 0,
        PROGRESS: 1,
        MATCHING: 2,
        DOWNLOADING_METADATA: 3,
        LOADING_METADATA: 4,
        PROCESSED: 5,
        ANALYZING: 6,
        DELETED: 9
    };
	
    return timelineEvents;
});