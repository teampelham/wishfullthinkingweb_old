define([], function() {
    "use strict";
    
	var fallbackOptions = {
        NEVER: "never",
        SAME_NETWORK: "samenetwork",
        ALWAYS: "always"
    };
    
	return fallbackOptions.settings = [{
        value: fallbackOptions.NEVER,
        title: "Never"
    }, 
	{
        value: fallbackOptions.SAME_NETWORK,
        title: "On same network as server"
    }, 
	{
        value: fallbackOptions.ALWAYS,
        title: "Always"
    }];
	return fallbackOptions;
});