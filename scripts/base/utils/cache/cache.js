define([
	"underscore", 
	"base/utils/baseClass", 
	"base/utils/dispatcher", 
	"base/mixins/eventMixin",
    "base/adapters/loggerAdapter"
], function(_, baseClass, dispatcher, eventMixin, loggerAdapter) {
    "use strict";
    
    var cache = baseClass.extend({
        mixins: eventMixin,
        maxCycles: 3,
        maxSize: 50,
        isListening: false,
        
        initialize: function() {
            this._cache = {};
            this.size = 0;
            this.cycles = [];
            this.cycle();
        },
        
        startListening: function() {
            if (this.isListening)
                return;
                
            this.isListening = true;
            this.listenTo(dispatcher,  {
                "cycle:cache": this.cycle,
                "clearWhere:cache": this.clearWhere,
                "clearAll:cache": this.clearAll,
                "clearCycles:cache": this.clearCycles
            });
        },
        
        get: function(cacheKey) {
            var item = this._cache[cacheKey];
            if (item && item._cacheCycle)
                this.setItemCycle(item, cacheKey, _.first(this.cycles));
            return item;
        },
        
        store: function(item, cacheKey) {   // t, i
            if (!cacheKey)
                cacheKey = this.getItemCacheKey(item);
            
            this.remove(item);
            if (!cacheKey) 
                return;
            
            this.startListening();
            item._cacheKey = cacheKey,
            this._cache[cacheKey] = item;
            this.size++;
            if (!item.persistInCache)
                this.setItemCycle(item, cacheKey, _.first(this.cycles));
        },
        
        getItemCacheKey: function(t) {
            console.log('getItemCacheKey');
            //return e.result(t, "cacheKey")
        },
        
        setItemCycle: function(item, cacheKey, cycle) {
            var itemCacheCycle = item._cacheCycle;
            
            if (itemCacheCycle && itemCacheCycle.hasOwnProperty(cacheKey)) 
                delete itemCacheCycle[cacheKey]; 
            
            item._cacheCycle = cycle; 
            cycle[cacheKey] = item;
        },
        
        cycle: function() {
            if (this.size > this.maxSize) {
                loggerAdapter.warn('Cache overflow, size: ' + this.size);
                _.each(this.cycles.slice(1), this.removeCycle, this);
            }
            
            this.cycles.unshift({});
            
            if (this.cycles.length > Math.max(this.maxCycles, 1))
                this.removeCycle(_.last(this.cycles));
        },
        
        remove: function(values) { // e
            if (values && values._cacheKey) {
                var cacheKey = values._cacheKey; // t
                if (this._cache.hasOwnProperty(cacheKey)) {
                    delete this._cache[cacheKey];
                    this.size--;
                }
                if (values._cacheCycle) {
                    if (values._cacheCycle.hasOwnProperty(cacheKey)) 
                        delete values._cacheCycle[cacheKey]; 
                        
                    delete values._cacheCycle; 
                }
                
                delete values._cacheKey
            }
        },
        
        removeCycle: function(cycle) {
            _.each(_.values(cycle), this.remove, this);
            var idx = _.indexOf(this.cycles, cycle);
            this.cycles.splice(idx, 1);
        },
        
        clearWhere: function(predicate) {
            var cacheItems = _.where(_.values(this._cache), predicate);
            _.each(cacheItems, this.remove, this);
        },
        
        clearAll: function() {
            console.log('clearAll');
            //e.each(e.values(this._cache), this.remove, this), this.cycles.length = 0, this.cycle(), this.stopListening(), this.isListening = !1
        },
        
        clearCycles: function() {
            console.log('clearCycles');
            //for (var e = this.cycles; e.length;) this.removeCycle(e[e.length - 1]);
            //this.cycle()
        }
        
    });
    
    return cache;
});