define([
	"underscore", 
	"base/utils/cache/cache"
], function(_, cache) {
    "use strict";
    
    var modelCache = cache.extend({
        maxCycles: 5,
        maxSize: 100,
        
        getItemCacheKey: function(cacheItem, scope) {
            
            if (_.isFunction(cacheItem.cacheKey)) {
                if (!scope)
                    scope = cacheItem.toJSON();
                    
                return cacheItem.cacheKey(scope);
            }
            return void 0;
        }
    });
    
    return modelCache;
});