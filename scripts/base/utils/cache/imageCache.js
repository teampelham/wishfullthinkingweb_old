define([
	"underscore", 
	"base/utils/cache/cache"
], function(_, cache) {
    "use strict";
	
    var imageCache = cache.extend({
        maxCycles: 3,
        maxSize: 50,
        imageParamsRegex: /&?(width|height|minSize)=([^&]+)/g,
        _infoMemo: {},
        
        store: function(item, cacheKey) {
            cacheKey = cacheKey || this.getItemCacheKey(item);
            
            if (cacheKey) {
                var url = item.url;
                var info = this.getImageInfo(url);
                var imageSize = {
                    url: url,
                    blobUrl: item.blobUrl,
                    size: info.size
                }; 
                var cachedItem = this.get(cacheKey);
                
                if (cachedItem) {
                    if (!cachedItem.images.hasOwnProperty(url)) {
                        var sizes = cachedItem.sizes;
                        cachedItem.images[url] = imageSize;
                        var sortSize = _.sortedIndex(sizes, imageSize, "size");
                        sizes.splice(sortSize, 0, imageSize);
                    }
                } 
                else 
                    cachedItem = {
                        images: {},
                        sizes: [imageSize]
                    }; 
                cachedItem.images[url] = imageSize; 
                this.startListening(); 
                cachedItem._cacheKey = cacheKey; 
                this._cache[cacheKey] = cachedItem; 
                this.size++; 
                
                if (!item.persistInCache) 
                    this.setItemCycle(cachedItem, cacheKey, _.first(this.cycles));
            }
        },
        
        getItemCacheKey: function(item) {
            var info = this.getImageInfo(item.url);
            return info.key;
        },
        
        getImageInfo: function(img) {
            if (this._infoMemo.hasOwnProperty(img)) 
                return this._infoMemo[img];
            
            var emptySize = {
                width: 0,
                height: 0,
                minSize: 0
            };
            var key = img.replace(this.imageParamsRegex, function(e, i, n) {
                return emptySize[i] = parseInt(n, 10), ""
            });
            var info = {
                key: key,
                size: emptySize.width * emptySize.height + emptySize.minSize
            };
            
            this._infoMemo[img] = info;
            return info;
        },
        
        removeImageInfo: function(e) {
            console.log('removeImageInfo');
            //this._infoMemo.hasOwnProperty(e) && delete this._infoMemo[e]
        },
        
        getBlobUrl: function(img) {
            var info = this.getImageInfo(img);
            var item = this.get(info.key);
            
            if (item) {
                var image = item.images[img];
                if (image) 
                    return image.blobUrl;
            }
        },
        
        getPlaceholder: function(img) {
            var info = this.getImageInfo(img);
            var item = this.get(info.key);
                
            if (item) {
                if (item.images[img]) 
                    return;
                var size = img.last(item.sizes);
                return size.blobUrl;
            }
        },
        
        remove: function(n) {
            console.log('remove');
            //i.prototype.remove.apply(this, arguments), e.each(n.sizes, function(e) {
            //    this.removeImageInfo(e.url), t.hasObjectUrl && window.URL.revokeObjectURL(e.blobUrl)
            //}, this)
        }
    });
    
    return imageCache;
});