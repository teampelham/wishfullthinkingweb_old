define([
	"underscore", 
	"base/utils/cache/cache"], function(e, t) {
    "use strict";
    var i = t.extend({
        maxCycles: 5,
        maxSize: 100,
        getItemCacheKey: function(t, i) {
            return e.isFunction(t.cacheKey) ? (i || (i = t.toJSON()), t.cacheKey(i)) : void 0
        }
    });
    return i
})