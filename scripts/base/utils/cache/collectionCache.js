define([
	"underscore", 
	"base/utils/cache/cache"
], function(_, cache) {
    "use strict";
	
    var collectionCache = cache.extend({
        maxCycles: 3,
        maxSize: 20,
        
        get: function() {
			console.warn('get');
            var e = cache.prototype.get.apply(this, arguments);
            if (e) 
                e.every(function(e) {
                    return e.cache && e.cache.isListening ? (e.cache.store(e), true) : false;
                });
            return e;
        },
        
        getItemCacheKey: function(t) {
			console.warn('getKey');
            var i = _.result(t, "url");
            return t.server ? t.server.getScopedKey(i) : i;
        }
    });
    
    return collectionCache;
});