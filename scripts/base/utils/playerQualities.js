define([
	"underscore", 
	"base/utils/mediaUtil2", 
	"base/adapters/loggerAdapter"], function(e, t, i) {
    "use strict";
    var n = {
        ORIGINAL: -1,
        bitrates: [64, 96, 208, 320, 720, 1500, 2e3, 3e3, 4e3, 8e3, 1e4, 12e3, 2e4],
        videoResolutions: ["220x180", "220x128", "284x160", "420x240", "576x320", "720x480", "1280x720", "1280x720", "1280x720", "1920x1080", "1920x1080", "1920x1080", "1920x1080"],
        videoQualities: [10, 20, 30, 30, 40, 60, 60, 75, 100, 60, 75, 90, 100],
        getQualityByIndex: function(e) {
            return 0 > e ? {
                index: -1
            } : {
                index: e,
                bitrate: this.bitrates[e],
                videoResolution: this.videoResolutions[e],
                videoQuality: this.videoQualities[e]
            }
        },
        getQualityAttribute: function(e) {
            return e.getFilepath() ? e.server.hasLocalConnection() ? "localQuality" : "remoteQuality" : "channelQuality"
        },
        getCurrentQuality: function(t, n) {
            var s = this.getQualityAttribute(t),
                a = n.get(s);
            return a >= this.bitrates.length && (i.warn("Quality index " + a + " is out of bounds"), a = n.defaults[s], n.save(s, a)), e.extend(this.getQualityByIndex(a), {
                attribute: s
            })
        },
        getAvailableQualities: function(e) {
            e || (e = {});
            for (var t = e.lowestQualityIndex, i = e.highestQualityIndex, n = e.comparator, s = [], a = 0, r = this.bitrates.length; r > a; a++)
                if (!(t && t > a)) {
                    if (i && a > i) return s;
                    if (n && n.call(this, a)) return s;
                    s.push(this.getQualityByIndex(a))
                }
            return s
        },
        getAvailableQualitiesFor: function(e, i, n) {
            if (!e || !i) return [];
            var s = i.get("transcoderVideo"),
                a = i.get("transcoderVideoRemuxOnly"),
                r = t.getSelectedVideo(e),
                o = !(!r || !r.bitrate),
                l = !! e.videoResolution;
            if (!s || a || !o && !l) return [];
            var c = o ? this.getAvailableQualitiesBelowBitrate(r.bitrate, n) : this.getAvailableQualitiesBelowResolution(e.videoResolution, n);
            return c
        },
        getAvailableQualitiesBelowBitrate: function(e, t) {
            return t || (t = {}), t.comparator = function(t) {
                var i = this.bitrates[t];
                return i >= e
            }, this.getAvailableQualities(t)
        },
        getAvailableQualitiesBelowResolution: function(e, t) {
            t || (t = {});
            var i = this.getHeightFromResolution(e);
            return t.comparator = function(e) {
                var t = this.videoResolutions[e],
                    n = this.getHeightFromResolution(this.getResolutionFromSize(t));
                return n >= i
            }, this.getAvailableQualities(t)
        },
        getResolutionFromSize: function(e) {
            var t = e.split("x"),
                i = parseInt(t[1], 10);
            return i > 2560 ? "8k" : i > 1100 ? "4k" : i > 720 ? "1080" : i > 576 ? "720" : i > 480 ? "576" : i > 360 ? "480" : "sd"
        },
        getHeightFromResolution: function(e) {
            switch (e) {
                case "8k":
                    return 5120;
                case "4k":
                    return 2560;
                case "1080":
                    return 1080;
                case "720":
                    return 720;
                case "576":
                    return 576;
                case "480":
                    return 480;
                case "sd":
                    return 360;
                default:
                    return 360
            }
        },
        getNearestQualityIndex: function(e) {
            if (!e) return -1;
            var t = this.bitrates.indexOf(e);
            if (0 > t) {
                t = 0;
                for (var i = 0; i < this.bitrates.length; i++) {
                    var n = this.bitrates[i];
                    if (n > e) break;
                    t = i
                }
            }
            return t
        }
    };
    return n
});