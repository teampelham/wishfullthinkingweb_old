define([
	"underscore", 
	"jquery", 
	"base/utils/baseClass", 
	"base/utils/capabilities", 
	"base/utils/xhrTracker", 
	"base/utils/parser", 
	"base/mixins/eventMixin", 
	"base/models/appModel"
], function(_, $, baseClass, capabilities, xhrTracker, parser, eventMixin, appModel) {
    "use strict";
    
    var cloudServerEventConnection = baseClass.extend({
        mixins: eventMixin,
        
        url: function() {
            var user = appModel.get("user");
            var relativeUrl = "/sub/" + this.requestType + "/" + user.id + "/" + appModel.get("clientID");
            var absoluteUrl = this.server.resolveUrl(relativeUrl, {
                connection: {
                    // TODO: Where is requestType set? Need to set up subdomains for this?
                    scheme: "websockets" === this.requestType ? "wss" : "https",
                    address: "websockets" === this.requestType ? "pubsubload.wishfullthinking.net" : "pubsubpoll.wishfullthinking.net"
                },
                appendToken: true
            });
            
            return absoluteUrl;
        },
        
        initialize: function(initObj) {
            _.bindAll(this, "onSocketOpen", "onSocketClose", "onSocketError", "onSocketMessage", "onPollSuccess", "onPollError");
			this.server = initObj.server;
        },
        
        open: function() {
            if (this.socket || this.pollRequest)
                this.close();
            if (capabilities.hasWebSocket) {
                this._deferred = $.Deferred();
                this.requestType = "websockets";
                /*this.socket = new WebSocket(_.result(this, "url"));
                this.socket.onopen = this.onSocketOpen;
                this.socket.onclose = this.onSocketClose;
                this.socket.onerror = this.onSocketError;
                this.socket.onmessage = this.onSocketMessage;*/
                return this._deferred.promise();
            }
            return this.poll();
        },
        
        close: function() {
            console.warn('do this');
            /*
            this.pollRequest && (s.abort(this.pollRequest.rid), this.pollRequest = null), this._deferred && "pending" === this._deferred.state() && this._deferred.reject(), this.socket && (this.socket.onopen = null, this.socket.onclose = null, this.socket.onerror = null, this.socket.onmessage = null, this.socket.close(), this.socket = null)*/
        },
        
        poll: function() {
            console.warn('do this');
            /*this.requestType = "longpolling";
            var i = e.result(this, "url"),
                n = this.server.getXHROptions(i);
            return this.pollRequest = t.ajax(n).done(this.onPollSuccess).fail(this.onPollError), this.pollRequest.promise()*/
        },
        
        handleResponse: function(t) {
            console.warn('do this');
            /*if (t) {
                var i = a.parseXML(t);
                if (e.isObject(i)) {
                    var n = this[i.command];
                    e.isFunction(n) && n.call(this, i)
                }
            }*/
        },
        
        relayStarted: function(e) {
            console.warn('do this');
            /*var t = o.get("servers"),
                i = t.get(e.clientIdentifier);
            i && i.addConnection({
                address: e.host,
                port: e.port,
                accessToken: i.get("accessToken"),
                relay: !0
            })*/
        },
        
        onSocketOpen: function(e) {
            console.warn('socket opened!');
            this.trigger("open", this.server, e); 
            this._deferred.resolve(this.server, e);
        },
        
        onSocketClose: function(e) {
            console.warn('socket closed!');
            //this.close(), this.trigger("close", this.server, e), this._deferred.reject(this.server, e)
        },
        
        onSocketError: function(e) {
            console.warn('socket error!');
            //this.close(), this.trigger("error", this.server, e), this._deferred.reject(this.server, e)
        },
        
        onSocketMessage: function(e) {
            console.warn('socket messaged!');
            //this.handleResponse(e.data)
        },
        
        onPollSuccess: function(e) {
            console.warn('something polled!');
            //this.trigger("open", this.server, e), this.handleResponse(e), this.poll()
        },
        onPollError: function(e) {
            console.warn('something errored!');
            //this.pollRequest.aborting || this.trigger("error", this.server, e)
        }
    });
    
	return cloudServerEventConnection;
});