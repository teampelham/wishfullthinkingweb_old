define([
	"underscore", 
	"jquery", 
	"base/utils/baseClass", 
	"base/utils/capabilities", 
	"base/utils/xhrTracker", 
	"base/utils/parser", 
	"base/mixins/eventMixin", 
	"base/models/appModel"
], function(_, $, baseClass, capabilities, xhrTracker, parser, eventMixin, appModel) {
    "use strict";
    
    var automatedDeviceEventConnection = baseClass.extend({
        mixins: eventMixin,
        
        url: function() {
            var connection = this.server.get("activeConnection");
            var connectionProps = _.pick(connection, "address", "port");
            
            connectionProps.scheme = "https" === connection.scheme ? "wss" : "ws"; 
            //if (connection.uri) 
            //    connectionProps.uri = connection.uri.replace(/^http/, "ws");
            
            var url = this.server.resolveUrl("/ws", {
                connection: connectionProps,
                appendToken: true
            });
            
            return url;
        },
        
        initialize: function(initObj) {
            _.bindAll(this, "onSocketOpen", "onSocketClose", "onSocketError", "onSocketMessage", "onPollSuccess", "onPollError");
            
			this.server = initObj.server;
        },
        
        open: function() {
            if (this.socket || this.pollRequest)
                this.close();
            
            if (capabilities.hasWebSocket) {
                this._deferred = $.Deferred();
                this.requestType = "websockets";
                this.socket = new WebSocket(_.result(this, "url"));
                this.socket.onopen = this.onSocketOpen;
                this.socket.onclose = this.onSocketClose;
                this.socket.onerror = this.onSocketError;
                this.socket.onmessage = this.onSocketMessage;
                return this._deferred.promise();
            }
            return this.poll();
        },
        
        close: function() {
            if (this.pollRequest) {
                xhrTracker.abort(this.pollRequest.rid); 
                this.pollRequest = null;
            }
            
            if (this._deferred && "pending" === this._deferred.state()) 
                this._deferred.reject(); 
                
            if (this.socket) {
                this.socket.onopen = null; 
                this.socket.onclose = null; 
                this.socket.onerror = null; 
                this.socket.onmessage = null; 
                this.socket.close(); 
                this.socket = null;
            }
        },
        
        poll: function() {
            console.warn('do this');
            /*this.requestType = "longpolling";
            var i = e.result(this, "url"),
                n = this.server.getXHROptions(i);
            return this.pollRequest = t.ajax(n).done(this.onPollSuccess).fail(this.onPollError), this.pollRequest.promise()*/
        },
        
        handleResponse: function(data) {
            if (data) {
                var dataObj = parser.parseXML(data);
                if (_.isObject(dataObj)) 
                    this.trigger("message", this.server, dataObj);
                else
                    this.trigger("message", this.server, data);
            }
        },
        
        send: function(msg, sender) {
            if(_.isObject(msg) && sender) {
                msg.sender = sender;
                msg = JSON.stringify(msg);
            }
            
            if (this.socket && this.socket.readyState === 1) {
                this.socket.send(msg);
            }  
        },
        
        relayStarted: function(e) {
            console.warn('do this');
            /*var t = o.get("servers"),
                i = t.get(e.clientIdentifier);
            i && i.addConnection({
                address: e.host,
                port: e.port,
                accessToken: i.get("accessToken"),
                relay: !0
            })*/
        },
        
        onSocketOpen: function(e) {
            this.trigger("open", this.server, e); 
            this._deferred.resolve(this.server, e);
        },
        
        onSocketClose: function(e) {
            this.close();
            this.trigger("close", this.server, e); 
            this._deferred.reject(this.server, e);
        },
        
        onSocketError: function(e) {
            this.close(); 
            this.trigger("error", this.server, e); 
            this._deferred.reject(this.server, e);
        },
        
        onSocketMessage: function(e) {
            this.handleResponse(e.data);
        },
        
        onPollSuccess: function(e) {
            console.warn('something polled!');
            //this.trigger("open", this.server, e), this.handleResponse(e), this.poll()
        },
        onPollError: function(e) {
            console.warn('something errored!');
            //this.pollRequest.aborting || this.trigger("error", this.server, e)
        }
    });
    
	return automatedDeviceEventConnection;
});