define([
	"underscore", 
	"jquery", 
	"base/utils/t", 
	"base/utils/baseClass", 
	"base/utils/dispatcher", 
	//"base/utils/playerUtil", 
	//"base/utils/syncUtil", 
	"base/mixins/eventMixin", 
	"base/models/appModel", 
	"base/models/library/section/sectionModel", 
	"base/models/library/metadataModel", 
	//"base/models/library/syncMetadataModel", 
	"base/models/server/sessionModel"
], function(_, $, t, baseClass, dispatcher, eventMixin, appModel, sectionModel, metadataModel, sessionModel) {
    "use strict";
    
    var mediaServerEventConnection = baseClass.extend({
        mixins: eventMixin,
        
        url: function() {
            var connection = this.server.get("activeConnection");
            var connectionProps = _.pick(connection, "address", "port");
            
            connectionProps.scheme = "https" === connection.scheme ? "wss" : "ws"; 
            if (connection.uri) 
                connectionProps.uri = connection.uri.replace(/^http/, "ws");
            
            var url = this.server.resolveUrl("/:/websockets/notifications", {
                connection: connectionProps,
                appendToken: true
            });
            
            return url;
        },
        
        initialize: function(initObj) {
            _.bindAll(this, "onOpen", "onClose", "onError", "onMessage");
            this.server = initObj.server;
        },
        
        open: function() {
            if (this.socket) 
                this.close(); 
            
            this._deferred = $.Deferred(); 
            this.socket = new WebSocket(_.result(this, "url")); 
            this.socket.onopen = this.onOpen; 
            this.socket.onclose = this.onClose; 
            this.socket.onerror = this.onError; 
            this.socket.onmessage = this.onMessage;
            
            return this._deferred.promise();
        },
        
        close: function() {
            if (this._deferred && "pending" === this._deferred.state()) 
                this._deferred.reject(); 
            
            if (this.socket) {
                this.socket.onopen = null; 
                this.socket.onclose = null; 
                this.socket.onerror = null; 
                this.socket.onmessage = null; 
                this.socket.close(); 
                this.socket = null;
            }
        },
        
        testConnection: function() {
            if (this.server.get("connected")) {
                var update = this.server.get("update");
                if (update && "installing" === update.get("state")) 
                    this.server.set("connected", null);
                 
                this.server.trigger("error:connection", this.server);
            }
        },
        
        preference: function(pref) {
            pref.key = pref.id; 
            pref.server = this.server; 
            this.server.trigger("socket:setting", pref);
        },
        
        timeline: function(t) {
            t.server = this.server;
            var i = {
                server: this.server,
                onlyIfCached: !0
            }, n = new c({
                    key: t.sectionID
                }, i);
            if (!e.isEmpty(n)) {
                i.section = n, t.section = n;
                var a = "/library/metadata/" + t.itemID,
                    r = new d({
                        key: a
                    }, i);
                if (e.isEmpty(r) || (t.item = r, r.set({
                    metadataState: t.metadataState,
                    mediaState: t.mediaState
                }), r.trigger("socket:timeline", t)), t.rootItemID) {
                    var o = "/library/metadata/" + t.rootItemID,
                        l = new d({
                            key: o
                        }, i);
                    e.isEmpty(l) || (l.fetch(), l.refreshChildren())
                }
                if (t.parentItemID) {
                    var u = "/library/metadata/" + t.parentItemID,
                        h = new d({
                            key: u
                        }, i);
                    e.isEmpty(h) || (h.fetch(), h.refreshChildren())
                }
                n.trigger("socket:timeline", t)
            }
            t.server.trigger("socket:timeline", t), s.trigger("timeline", t)
        },
        
        status: function(statusItem) {
            var status = statusItem.title + (statusItem.description ? ": " + statusItem.description : "");
            dispatcher.trigger("show:status", this.server, status); 
            this.server.trigger("socket:status", statusItem);
            
            if ("LIBRARY_UPDATE" === statusItem.notificationName) 
                this.server.get("sections").fetch();
        },
        
        progress: function(statusItem) {
            dispatcher.trigger("show:status", this.server, statusItem.message); 
            this.server.trigger("socket:status", statusItem);
        },
        
        playing: function(t) {
            console.warn('playing!');
            /*if (t.sessionKey && !this.server.get("shared")) {
                var n = {
                    server: this.server,
                    onlyIfCached: !0
                };
                var r = this.server.get("sessions");
                var o = new h({
                    sessionKey: t.sessionKey
                }, n);
                
                if (e.isEmpty(o) || o.get("key") !== t.key) {
                    var c = r.fetch();
                    c.done(e.bind(function() {
                        if (o = r.get(t.sessionKey)) {
                            var n = l.get("user");
                            var a = o.get("Player").title;
                            var c = o.get("User").title;
                            var d = o.get("grandparentTitle");
                            var u = o.get("title");
                            
                            if (d) 
                                u = d + " - " + u;
                                
                            var h = i("{1} started playing {2} on {3}.", c, u, a);
                            
                            if (c && c !== n.get("username")) 
                                s.trigger("show:status", this.server, h); 
                            
                            this.server.trigger("socket:playing", e.extend(t, {
                                username: c || i("Unknown"),
                                title: u,
                                player: a
                            }));
                        }
                    }, this));
                } 
                else if (t.state === a.states.STOPPED) { 
                    o.trigger("destroy", o, o.collection); 
                    o.cache.remove(o);
                }
                else {
                    var d = o.get("Player");
                    d && (d.state = t.state, o.trigger("change:Player", o, d)), o.set("viewOffset", t.viewOffset)
                }
            }*/
        },
        
        syncState: function(state) {
            console.warn('wtf');
            /*var initObj = {
                server: this.server,
                onlyIfCached: true
            };
             
            var n = new u({
                key: state.key,
                syncItemId: state.syncItemId
            }, initObj);
            
            if (!_.isEmpty(n)) {
                var s = n.get("Media");
                _.each(s, function(i) {
                    var s = i.Part;
                    _.each(s, function(e) {
                        e.transcodeState = state.syncFileState; 
                        e.transcodeStateContext = state.syncFileStateContext; 
                        e.syncState = state.syncState; 
                        e.syncStateContext = state.syncStateContext;
                        var i = r.getSyncStateFromPart(e);
                        
                        if (r.waitingRegex.test(i)) 
                            e.key = "placeholder"; 
                        
                        n.trigger("change:Media", n, n.get("Media"));
                    })
                })
            }*/
        },
        
        "update.statechange": function(state) {
            var update = this.server.get("update");
            var updateAttrs = _.pick(state, update.whitelistedAttrs);
            
            _.defaults(updateAttrs, _.result(update, "defaults")); 
            if ("downloading" === update.get("state") && "available" === updateAttrs.state) 
                updateAttrs.state = "downloading"; 
            update.set(updateAttrs);
        },
        
        reachability: function() {
            var cloudAccount = this.server.get("cloudAccount");
            cloudAccount.fetch();
        },
        
        account: function(cloud) {
            this.server.set("myPremiumSubscription", cloud.hasPremiumPass);
        },
        
        onOpen: function(e) {
            this.server.trigger("socket:websocket", {
                opened: true
            }); 
            this.trigger("open", this.server, e); 
            this._deferred.resolve(this.server, e);
        },
        
        onClose: function(e) {
            this.close(), this.server.trigger("socket:websocket", {
                closed: true
            }); 
            this.trigger("close", this.server, e); 
            this._deferred.reject(this.server, e); 
            this.testConnection();
        },
        
        onError: function(e) {
            this.close(), this.server.trigger("socket:websocket", {
                error: true
            }); 
            this.trigger("error", this.server, e); 
            this._deferred.reject(this.server, e); 
            this.testConnection();
        },
        
        onMessage: function(msg) {
            var result;
            
            try {
                result = JSON.parse(msg.data)
            } 
            catch (err) {
                return;
            }
            
            var type = result.type;
            var callback = this[type];
                
            e.each(result._children, function(child) {
                this.trigger("message", child);
                 
                if (_.isFunction(callback)) 
                    callback.call(this, child);
            }, this);
        }
    });
    
    return mediaServerEventConnection;
});