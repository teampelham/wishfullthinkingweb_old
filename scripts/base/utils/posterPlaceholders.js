define([], function() {
    "use strict";
	
    var standard = "placeholder-standard";
    var square = "placeholder-square";
    var wide = "placeholder-widescreen";
    var folder = "placeholder-folder";
    var friend = "placeholder-friend";
    var light = "placeholder-light";
    
    var posterPlaceholders = {
            movie: standard,
            show: standard,
            season: standard,
            episode: wide,
            artist: square,
            album: square,
            track: square,
            photo: square,
            photoAlbum: square,
            clip: wide,
            userPlaylistItem: wide,
            playlist: square,
            playlistFolder: square,
            channel: square,
            channelDirectory: square,
            folder: folder,
            friend: friend,
            device: square,
            poster: standard,
            art: wide,
            banner: wide,
            light: light
        };
    return posterPlaceholders;
});