define([
	"underscore", 
	"base/utils/baseClass", 
	"base/utils/capabilities", 
	"base/mixins/eventMixin", 
	"base/models/config"
], function(_, baseClass, capabilities, eventMixin, config) {
    "use strict";

    var consoleLogger = baseClass.extend({
        mixins: eventMixin,
        
        log: function(message, options) {
            var color;
            var logFn;
            var type = "log";
            
            if (options) {
                type = options.type || type; 
                color = options.color;
                logFn = options.logFn;
            }
            
            this.trigger("add", {
                type: type,
                message: message
            });
            
            if (capabilities.hasConsole) {
                logFn = logFn in console ? console[logFn] : console.log;
                if (color && config.get("colorizeLogs")) {
                    message = "%c" + message;
                    color = "color: " + color + ";";
                    logFn.call(console, message, color);
                }
                else
                    logFn.call(console, message);
            }
        },
        
        success: function(message) {
            this.log(message, {
                type: "success",
                color: "green"
            });
        },
        
        info: function(message) {
            this.log(message, {
                type: "info",
                logFn: "info",
                color: "blue"
            });
        },
        
        debug: function(message) {
            this.log(message, {
                type: "debug",
                logFn: "trace",
                color: "#d500ff"
            });
        },
        
        warn: function(message) {
            this.log(message, {
                type: "warning",
                logFn: "warn",
                color: "#ff6600"
            });
        },
        
        error: function(message) {
            this.log(message, {
                type: "error",
                logFn: "error",
                color: "red"
            });
        },
        
        group: function(groupName, messages, nestLevel) {
            if (console.group)
                console.group(groupName);
            
            var levelFn = nestLevel && nestLevel.level; 
            var logFn = _.bind(this[levelFn] || this.log, this);
            
            for (var r = 0, o = messages.length; o > r; r++) 
                logFn(messages[r]);
                
            if (console.groupEnd)
                console.groupEnd();
        }
    });
    
    return consoleLogger;
});