define([
	"underscore", 
	"base/utils/baseClass"
], function(e, t) {
    "use strict";
    var i = t.extend({
        types: [{
            id: 1,
            typeString: "video",
            key: "videoStreamID",
            title: "Video"
        }, {
            id: 2,
            typeString: "audio",
            key: "audioStreamID",
            title: "Audio"
        }, {
            id: 3,
            typeString: "subtitles",
            key: "subtitleStreamID",
            title: "Subtitles"
        }],
        initialize: function() {
            this.byID = e.reduce(this.types, function(e, t) {
                return e[t.id] = t, e
            }, {}), this.byType = e.reduce(this.types, function(e, t) {
                return e[t.typeString] = t, e
            }, {})
        },
        getStreamType: function(e) {
            return this.byID[e]
        },
        getStreamTypeByString: function(e) {
            return this.byType[e]
        }
    });
    return new i
})