define(["underscore", "base/utils/baseClass2"], function(e, t) {
    "use strict";
    var i = t.extend({
        types: [{
            key: "all",
            title: "All"
        }, {
            key: "unwatched",
            title: "Unwatched"
        }, {
            key: "newest",
            title: "Recently Released"
        }, {
            key: "recentlyAdded",
            title: "Recently Added"
        }, {
            key: "recentlyViewed",
            title: "Recently Viewed"
        }, {
            key: "recentlyViewedShows",
            title: "Recently Viewed Shows"
        }, {
            key: "onDeck",
            title: "On Deck"
        }, {
            key: "folder",
            title: "By Folder"
        }],
        initialize: function() {
            this.byKey = e.reduce(this.types, function(e, t) {
                return e[t.key] = t, e
            }, {})
        },
        getSubsetType: function(e) {
            return this.byKey[e]
        }
    });
    return new i
})