define([
	"underscore", 
	"base/utils/baseClass", 
	"base/utils/formatters/stringUtil"
], function(_, baseClass, stringUtil) {
    "use strict";
	
    var MediaTypes = baseClass.extend({
        types: [{
            id: 1,
            typeString: "movie",
            title: "Movie",
            plural: stringUtil.pluralize("Movie"),
            element: "video"
        }, {
            id: 2,
            typeString: "show",
            title: "Show",
            plural: stringUtil.pluralize("TV Show"),
            element: "directory",
            child: 3,
            grandchild: 4
        }, {
            id: 3,
            typeString: "season",
            title: "Season",
            plural: stringUtil.pluralize("Season"),
            element: "directory",
            parent: 2,
            child: 4
        }, {
            id: 4,
            typeString: "episode",
            title: "Episode",
            plural: stringUtil.pluralize("Episode"),
            element: "video",
            grandparent: 2,
            parent: 3
        }, {
            id: 5,
            typeString: "trailer",
            title: "Trailer",
            plural: stringUtil.pluralize("Trailer"),
            element: "video"
        }, {
            id: 6,
            typeString: "comic",
            title: "Comic",
            plural: stringUtil.pluralize("Comic"),
            element: "photo"
        }, {
            id: 7,
            typeString: "person",
            title: "Person",
            plural: stringUtil.pluralize("Person", null, "People"),
            element: "directory"
        }, {
            id: 8,
            typeString: "artist",
            title: "Artist",
            plural: stringUtil.pluralize("Artist"),
            element: "directory",
            child: 9,
            grandchild: 10
        }, {
            id: 9,
            typeString: "album",
            title: "Album",
            plural: stringUtil.pluralize("Album"),
            element: "directory",
            parent: 8,
            child: 10
        }, {
            id: 10,
            typeString: "track",
            title: "Track",
            plural: stringUtil.pluralize("Track"),
            element: "audio",
            grandparent: 8,
            parent: 9
        }, {
            id: 11,
            typeString: "picture",
            title: "Picture",
            plural: stringUtil.pluralize("Picture"),
            element: "photo"
        }, {
            id: 12,
            typeString: "clip",
            title: "Clip",
            plural: stringUtil.pluralize("Clip"),
            element: "video"
        }, {
            id: 13,
            typeString: "photo",
            title: "Photo",
            plural: stringUtil.pluralize("Photo"),
            element: "photo",
            parent: 14
        }, {
            id: 14,
            typeString: "photoAlbum",
            title: "Photo Album",
            plural: stringUtil.pluralize("Photo Album"),
            element: "directory",
            child: 13
        }, {
            id: 15,
            typeString: "playlist",
            title: "Playlist",
            plural: stringUtil.pluralize("Playlist"),
            element: "directory"
        }, {
            id: 16,
            typeString: "playlistFolder",
            title: "Playlist",
            plural: stringUtil.pluralize("Playlist"),
            element: "directory",
            child: 15
        }, 
        {
            id: 22,
            typeString: "light",
            title: "Light",
            plural: stringUtil.pluralize("Light"),
            element: "directory",
            child: 3,
            grandchild: 4
        },
        {
            id: 1001,
            typeString: "userPlaylistItem",
            title: "Clip",
            plural: stringUtil.pluralize("Clip"),
            element: "video"
        }],
		
        initialize: function() {
            this.byID = _.reduce(this.types, function(memo, media) {
                memo[media.id] = media;
				return memo;
            }, {}); 
			
			this.byType = _.reduce(this.types, function(memo, media) {
                memo[media.typeString] = media;
				return memo;
            }, {})
        },
		
        getMediaType: function(id) {
            return this.byID[id];
        },
		
        getMediaTypeByString: function(typeStr) {
            return this.byType[typeStr];
        },
		
        getRelatedMediaTypes: function(typeStr) {
            var media = this.byType[typeStr];
            
			if (!media) 
				return [];
            
			var relations = ["grandparent", "parent", "child", "grandchild"];
            var smallerSet = _.compact(_.map(relations, _.bind(function(relation) {
				return media.hasOwnProperty(relation) ? this.byID[media[relation]] : void 0;
			}, this)));
            
			smallerSet.unshift(media); 
			return smallerSet;
        },
		
        getChildMediaType: function(typeStr) {
            var media = this.byType[typeStr];
            if (media) {
                var child = media.grandchild || media.child;
                return child ? this.byID[child] : media;
            }
        },
		
        typeHasChildMedia: function(typeStr) {
            var childType = this.getChildMediaType(typeStr);
            return childType ? childType.typeString !== typeStr : false;
        }
    });
	
    return new MediaTypes();
});