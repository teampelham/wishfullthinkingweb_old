define([
	"underscore", 
	"base/utils/baseClass"
], function(_, baseClass) {
    "use strict";
    
	var SubsetTypes = baseClass.extend({
        types: [{
            key: "all",
            title: "All"
        }, {
            key: "unwatched",
            title: "Unwatched"
        }, {
            key: "newest",
            title: "Recently Released"
        }, {
            key: "recentlyAdded",
            title: "Recently Added"
        }, {
            key: "recentlyViewed",
            title: "Recently Viewed"
        }, {
            key: "recentlyViewedShows",
            title: "Recently Viewed Shows"
        }, {
            key: "onDeck",
            title: "On Deck"
        }, {
            key: "folder",
            title: "By Folder"
        }],
		
        initialize: function() {
            this.byKey = _.reduce(this.types, function(memo, item) {
                memo[item.key] = item;
				return memo;
            }, {});
        },
	
        getSubsetType: function(subset) {
            return this.byKey[subset];
        }
    });
	
    return new SubsetTypes();
});