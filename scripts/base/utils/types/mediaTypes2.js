define([
	"underscore", 
	"base/utils/baseClass2", 
	"base/utils/formatters/stringUtil2"
], function(e, t, i) {
    "use strict";
    var n = t.extend({
        types: [{
            id: 1,
            typeString: "movie",
            title: "Movie",
            plural: i.pluralize("Movie"),
            element: "video"
        }, {
            id: 2,
            typeString: "show",
            title: "Show",
            plural: i.pluralize("TV Show"),
            element: "directory",
            child: 3,
            grandchild: 4
        }, {
            id: 3,
            typeString: "season",
            title: "Season",
            plural: i.pluralize("Season"),
            element: "directory",
            parent: 2,
            child: 4
        }, {
            id: 4,
            typeString: "episode",
            title: "Episode",
            plural: i.pluralize("Episode"),
            element: "video",
            grandparent: 2,
            parent: 3
        }, {
            id: 5,
            typeString: "trailer",
            title: "Trailer",
            plural: i.pluralize("Trailer"),
            element: "video"
        }, {
            id: 6,
            typeString: "comic",
            title: "Comic",
            plural: i.pluralize("Comic"),
            element: "photo"
        }, {
            id: 7,
            typeString: "person",
            title: "Person",
            plural: i.pluralize("Person", null, "People"),
            element: "directory"
        }, {
            id: 8,
            typeString: "artist",
            title: "Artist",
            plural: i.pluralize("Artist"),
            element: "directory",
            child: 9,
            grandchild: 10
        }, {
            id: 9,
            typeString: "album",
            title: "Album",
            plural: i.pluralize("Album"),
            element: "directory",
            parent: 8,
            child: 10
        }, {
            id: 10,
            typeString: "track",
            title: "Track",
            plural: i.pluralize("Track"),
            element: "audio",
            grandparent: 8,
            parent: 9
        }, {
            id: 11,
            typeString: "picture",
            title: "Picture",
            plural: i.pluralize("Picture"),
            element: "photo"
        }, {
            id: 12,
            typeString: "clip",
            title: "Clip",
            plural: i.pluralize("Clip"),
            element: "video"
        }, {
            id: 13,
            typeString: "photo",
            title: "Photo",
            plural: i.pluralize("Photo"),
            element: "photo",
            parent: 14
        }, {
            id: 14,
            typeString: "photoAlbum",
            title: "Photo Album",
            plural: i.pluralize("Photo Album"),
            element: "directory",
            child: 13
        }, {
            id: 15,
            typeString: "playlist",
            title: "Playlist",
            plural: i.pluralize("Playlist"),
            element: "directory"
        }, {
            id: 16,
            typeString: "playlistFolder",
            title: "Playlist",
            plural: i.pluralize("Playlist"),
            element: "directory",
            child: 15
        }, {
            id: 1001,
            typeString: "userPlaylistItem",
            title: "Clip",
            plural: i.pluralize("Clip"),
            element: "video"
        }],
        initialize: function() {
            this.byID = e.reduce(this.types, function(e, t) {
                return e[t.id] = t, e
            }, {}), this.byType = e.reduce(this.types, function(e, t) {
                return e[t.typeString] = t, e
            }, {})
        },
        getMediaType: function(e) {
            return this.byID[e]
        },
        getMediaTypeByString: function(e) {
            return this.byType[e]
        },
        getRelatedMediaTypes: function(t) {
            var i = this.byType[t];
            if (!i) return [];
            var n = ["grandparent", "parent", "child", "grandchild"],
                s = e.compact(e.map(n, e.bind(function(e) {
                    return i.hasOwnProperty(e) ? this.byID[i[e]] : void 0
                }, this)));
            return s.unshift(i), s
        },
        getChildMediaType: function(e) {
            var t = this.byType[e];
            if (t) {
                var i = t.grandchild || t.child;
                return i ? this.byID[i] : t
            }
        },
        typeHasChildMedia: function(e) {
            var t = this.getChildMediaType(e);
            return t ? t.typeString !== e : !1
        }
    });
    return new n
});