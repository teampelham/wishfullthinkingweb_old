define([
	"underscore", 
	"base/utils/baseClass"
], function(_, baseClass) {
    "use strict";
	
    var mediaTypes = [{
        key: "devices",
        typeID: "1",
        typeString: "device",
        title: "Automated Devices",
        defaultAgent: "com.plexapp.agents.imdb",
        defaultLanguage: "en",
        defaultScanner: "Plex Movie Scanner",
        sectionTypeIcon: "film"
    }, {
        key: "schedules",
        typeID: "2",
        typeString: "schedule",
        title: "Scehdules",
        defaultAgent: "com.plexapp.agents.imdb",
        defaultLanguage: "en",
        defaultScanner: "Plex Movie Scanner",
        sectionTypeIcon: "display"    
    
        /*key: "movies",
        typeID: "1",
        typeString: "movie",
        title: "Movies",
        defaultAgent: "com.plexapp.agents.imdb",
        defaultLanguage: "en",
        defaultScanner: "Plex Movie Scanner",
        sectionTypeIcon: "film"
    }, {
        key: "tv",
        typeID: "2",
        typeString: "show",
        title: "TV Shows",
        defaultAgent: "com.plexapp.agents.thetvdb",
        defaultLanguage: "en",
        defaultScanner: "Plex Series Scanner",
        sectionTypeIcon: "display"
    }, {
        key: "music",
        typeID: "8",
        typeString: "artist",
        title: "Music",
        defaultAgent: "com.plexapp.agents.lastfm",
        defaultLanguage: "en",
        defaultScanner: "Plex Music Scanner",
        sectionTypeIcon: "music",
        exclusiveAgents: ["com.plexapp.agents.plexmusic"],
        exclusiveScanners: ["Plex Premium Music Scanner"]
    }, {
        key: "photos",
        typeID: "13",
        typeString: "photo",
        title: "Photos",
        defaultAgent: "com.plexapp.agents.none",
        defaultLanguage: "xn",
        defaultScanner: "Plex Photo Scanner",
        sectionTypeIcon: "picture"
    }, {
        key: "videos",
        typeID: "1",
        typeString: "movie",
        title: "Home Videos",
        defaultAgent: "com.plexapp.agents.none",
        defaultLanguage: "xn",
        defaultScanner: "Plex Video Files Scanner",
        sectionTypeIcon: "facetime-video"*/
    }];
	var SectionTypes = baseClass.extend({
		
		initialize: function() {
			this.byKey = _.reduce(mediaTypes, function(memo, value) {
				memo[value.key] = value;
				return memo;
			}, {}); 
			
			this.byType = _.reduceRight(mediaTypes, function(memo, item) {
				memo[item.typeString] = item;
				return memo;
			}, {});
		},
		
		getSectionTypes: function() {
			return mediaTypes;
		},
		
		getSectionType: function(sectionTypeKey) {
			return this.byKey[sectionTypeKey];
		},
		
		getSectionTypeByString: function(sectionTypeStr) {
			return this.byType[sectionTypeStr];
		}
	});
	
    return new SectionTypes();
})