define([
	"underscore", 
	"base/utils/baseClass"
], function(_, baseClass) {
    "use strict";
	
    var deviceTypes = [{
        key: "light",
        typeID: "1",
        typeString: "light",
        title: "Lights/Lamps/Bulbs",
        defaultAgent: "com.plexapp.agents.imdb",
        defaultLanguage: "en",
        defaultScanner: "Plex Movie Scanner",
        sectionTypeIcon: "film"
    }, {
        key: "appliance",
        typeID: "2",
        typeString: "appliance",
        title: "Appliances",
        defaultAgent: "com.plexapp.agents.imdb",
        defaultLanguage: "en",
        defaultScanner: "Plex Movie Scanner",
        sectionTypeIcon: "display"    
	}, {
        key: "hvac",
        typeID: "3",
        typeString: "hvac",
        title: "Heating and Cooling",
        defaultAgent: "com.plexapp.agents.imdb",
        defaultLanguage: "en",
        defaultScanner: "Plex Movie Scanner",
        sectionTypeIcon: "display"
	}, {
        key: "sensor",
        typeID: "4",
        typeString: "sensor",
        title: "Sensors",
        defaultAgent: "com.plexapp.agents.imdb",
        defaultLanguage: "en",
        defaultScanner: "Plex Movie Scanner",
        sectionTypeIcon: "display"
	}, {
        key: "door",
        typeID: "5",
        typeString: "door",
        title: "Doors and Closures",
        defaultAgent: "com.plexapp.agents.imdb",
        defaultLanguage: "en",
        defaultScanner: "Plex Movie Scanner",
        sectionTypeIcon: "display"  
  	}, {
        key: "lock",
        typeID: "6",
        typeString: "lock",
        title: "Lockables",
        defaultAgent: "com.plexapp.agents.imdb",
        defaultLanguage: "en",
        defaultScanner: "Plex Movie Scanner",
        sectionTypeIcon: "display"  
	}, {
        key: "other",
        typeID: "7",
        typeString: "other",
        title: "Others",
        defaultAgent: "com.plexapp.agents.imdb",
        defaultLanguage: "en",
        defaultScanner: "Plex Movie Scanner",
        sectionTypeIcon: "display"        
    }];
	var AutomatedDeviceTypes = baseClass.extend({
		
		initialize: function() {
			this.byKey = _.reduce(deviceTypes, function(memo, value) {
				memo[value.key] = value;
				return memo;
			}, {}); 
			
			this.byType = _.reduceRight(deviceTypes, function(memo, item) {
				memo[item.typeID] = item;
				return memo;
			}, {});
		},
		
		getDeviceTypes: function() {
			return deviceTypes;
		},
		
		getDeviceType: function(deviceTypeKey) {
			return this.byKey[deviceTypeKey];
		},
		
		getDeviceTypeByID: function(typeID) {
			return this.byType[typeID];
		}
	});
	
    return new AutomatedDeviceTypes();
})