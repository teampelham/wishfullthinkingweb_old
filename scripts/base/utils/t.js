define(["base/adapters/localizerAdapter"], function(localizer) {
   "use strict";
   
   return function() {
       return localizer.t.apply(localizer, arguments);
   }
});