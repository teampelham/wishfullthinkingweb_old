define(["underscore"], function(e) {
    "use strict";

    function t() {
        e.isFunction(this.initialize) && this.initialize.apply(this, arguments)
    }
    var i = function(t, i) {
        var n, s = this;
        n = t && e.has(t, "constructor") ? t.constructor : function() {
            return s.apply(this, arguments) || null
        }, e.extend(n, s, i);
        var a = function() {
            this.constructor = n
        };
        if (a.prototype = s.prototype, n.prototype = new a, t && t.hasOwnProperty("mixins")) {
            var r = t.mixins;
            delete t.mixins, e.isArray(r) || (r = [r]), e.each(r, function(t) {
                e.extend(n.prototype, t)
            })
        }
        return t && e.extend(n.prototype, t), n.__super__ = s.prototype, n
    };
    return t.extend = i, t
});