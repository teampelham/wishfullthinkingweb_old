define([
	"underscore", 
	"jquery"
], function(_, $) {
    "use strict";

    function whenEach(callback) {
        callback = _.isArray(callback) ? callback : _.toArray(arguments); 
		return $.when.apply($, _.map(callback, alwaysResolve));
    }

    function firstSuccess(callback) {
        if (!callback.length) 
			return $.Deferred().reject().promise();
        
		var deferred = $.Deferred();
        var callbackLength = callback.length;
        
		_.each(callback, function(cb) {
            cb.done(function() {
                callbackLength--; 
				deferred.resolveWith(this, arguments);
            }).fail(function() {
                callbackLength--; 
				if (0 === callbackLength) 
					deferred.reject();
            });
        }); 
		
		return deferred.promise();
    }

    function alwaysResolve(callback) {
        var deferred = $.Deferred();
        
		callback.always(function() {
            deferred.resolve.apply(deferred, arguments);
        }); 
		
		return deferred.promise();
    }
	
    var promiseUtil = {
        whenEach: whenEach,
        whenFirstSucceed: firstSuccess,
        alwaysResolve: alwaysResolve
    };
	
    return promiseUtil;
});