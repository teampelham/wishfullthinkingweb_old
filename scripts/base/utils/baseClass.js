define(["underscore"], function(_) {
    "use strict";

    function baseClass() {
        _.isFunction(this.initialize) && this.initialize.apply(this, arguments)
    }
	
    var extend = function(superClass, context) {
		var newObj;
		var self = this;
		
		if (superClass && _.has(superClass, "constructor"))
			newObj = superClass.constructor;
		else 
			newObj = function() { return self.apply(this, arguments) || null; };
		
		_.extend(newObj, self, context);
		
		var BaseConstructor = function() {
			this.constructor = newObj;
		}
		
		BaseConstructor.prototype = self.prototype;
		newObj.prototype = new BaseConstructor();
		
		if (superClass && superClass.hasOwnProperty("mixins")) {
			var mixins = superClass.mixins;
			delete superClass.mixins;
			
			if (!_.isArray(mixins)) 
				mixins = [mixins];
				
			_.each(mixins, function(mixin) {
				_.extend(newObj.prototype, mixin);
			});
		}
		
		if (superClass)
			_.extend(newObj.prototype, superClass);
		
		newObj.__super__ = self.prototype;
		return newObj;
	};
    
	baseClass.extend = extend; 
	
	return baseClass;
});