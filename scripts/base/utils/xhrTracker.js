define([
	"underscore", 
	"jquery", 
	"base/utils/baseClass"
], function(_, $, baseClass) {
    "use strict";
	
    var XhrTracker = baseClass.extend({
        
		initialize: function() {
            _.bindAll(this, "onAjaxSend");
			this.requests = []; 
			this.byId = {};
        },
		
        startListening: function() {
            $(document).on("ajaxSend", this.onAjaxSend);
        },
		
        stopListening: function() {
            $(document).off("ajaxSend", this.onAjaxSend);
        },
        
        abortAll: function(scope) {
            for (var t = this.requests.length - 1; t > -1; t--) {
                var request = this.requests[t];
                if (null == scope || scope === request.settings.scope)
                    this.abort(request);
            }
        },
        
        abort: function(requestObj) {
            var request = _.isString(requestObj) ? this.byId[requestObj] : requestObj;
            
            if (requestObj) {
                request.xhr.aborting = true; 
                request.xhr.abort(); 
                this.remove(request);
            }
        },
        
        store: function(xhr, ajaxOptions) {
            var id = _.uniqueId("r");
            xhr.rid = id;
            
            var key = {
                xhr: xhr,
                settings: ajaxOptions
            };
            
            this.requests.push(key);
            this.byId[id] = key;
            return key;
        },
        
        
        remove: function(xhrKey) {
            var idx = _.indexOf(this.requests, xhrKey);
            if (idx > -1)
                this.requests.splice(idx, 1);
                
            if (this.byId.hasOwnProperty(xhrKey.xhr.rid))
                delete this.byId[xhrKey.xhr.rid];
        },
		
        onAjaxSend: function(event, xhr, ajaxOptions) {
            if (ajaxOptions.skipCorsWithRedirect) 
				return;
			// Add all ajax requests to a collection	
            var key = this.store(xhr, ajaxOptions);
            xhr.always(_.bind(function() {
                if (!xhr.aborting)
					this.remove(key);      // Ditch the request when it's completed, unless it was aborted
            }, this));
        }
    });
	
    return new XhrTracker();
});