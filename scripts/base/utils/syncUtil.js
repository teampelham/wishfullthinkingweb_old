define([], function() {
    "use strict";
	
    var syncUtil = {
        waitingRegex: /^(transcoded|downloaded|directplay|processed)$/,
        processingRegex: /^(pending|transcoding|downloading)$/,
        failedRegex: /^(failed|error)$/,
        
		addVersionHeader: function(request, list) {
            var syncList = list && list.get("SyncList");
            if (syncList && syncList.version) {
				request = request || {}; 
				request.headers = request.headers || {}; 
				request.headers["X-Plex-Sync-Version"] = syncList.version;
			}
			return request
        },
        
		getSyncStateFromPart: function(part) {
            return part.transcodeState || part.syncState;
        },
        
		getSyncStateContextFromPart: function(part) {
            return part.transcodeStateContext || part.syncStateContext;
        }
    };
	
    return syncUtil;
});