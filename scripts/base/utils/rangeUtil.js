define(["underscore"], function(e) {
    "use strict";

    function t(t) {
        var i = e.clone(t);
        return "undefined" != typeof i.last ? (i.end = i.last + 1, i.size = i.end - i.start) : "undefined" != typeof i.size ? (i.end = i.start + i.size, i.last = i.end - 1) : "undefined" != typeof i.end && (i.last = i.end - 1, i.size = i.end - i.start), i
    }

    function i(i, n) {
        return e.filter(e.map(i, t), function(e) {
            return n ? e.size >= 0 : e.size > 0
        })
    }
    var n = {
        difference: function(e, n, s) {
            e = t(e), n = t(n);
            var a = [{
                start: e.start,
                last: Math.min(e.last, n.start - 1)
            }, {
                start: Math.max(e.start, n.end),
                last: e.last
            }];
            return i(a, s)
        },
        union: function(e, n, s) {
            if (e = t(e), n = t(n), e.start > n.start) {
                var a = e;
                e = n, n = a
            }
            var r = [e];
            return n.start <= e.end ? r[0].last = Math.max(n.last, e.last) : r.push(n), i(r, s)
        },
        degap: function(e, i) {
            var n = t(e);
            return i = t(i), i.size ? (n.start > i.end && (n = {
                start: i.end,
                end: n.end
            }), n.end < i.start && (n = {
                start: n.start,
                end: i.start
            }), t(n)) : n
        },
        each: function(e, i, n) {
            e = t(e);
            for (var s = Math.min(e.start, e.end), a = Math.max(e.start, e.end); a >= s; s++) i.call(n, s)
        },
        roundByPage: function(e, i) {
            return e = t(e), t({
                start: Math.floor(e.start / i) * i,
                end: Math.ceil(e.end / i) * i
            })
        }
    };
    return n;
});