define([
    "underscore", 
    "base/utils/baseClass"
], function(e, t) {
    "use strict";

    function i(t, i) {
        var n = e.invert(t);
        return e.reduce(t, function(e, t) {
            return e[t] = i[n[t]], e
        }, {})
    }
    var n = t.extend({
        types: {
            VIDEO: "video",
            MUSIC: "music",
            PHOTO: "photo"
        },
        elements: {
            VIDEO: "video",
            MUSIC: "audio",
            PHOTO: "photo"
        },
        streams: {
            VIDEO: "video",
            MUSIC: "audio",
            PHOTO: "photo"
        },
        states: {
            STOPPED: "stopped",
            PAUSED: "paused",
            PLAYING: "playing",
            BUFFERING: "buffering",
            ERROR: "error"
        },
        castStates: {
            ERROR: "IDLE",
            STOPPED: "IDLE",
            PAUSED: "PAUSED",
            PLAYING: "PLAYING",
            BUFFERING: "BUFFERING"
        },
        initialize: function() {
            this.typeByElement = i(this.elements, this.types), this.elementByType = i(this.types, this.elements), this.typeByStream = i(this.streams, this.types), this.streamByType = i(this.types, this.streams), this.statebyCastState = i(this.castStates, this.states), this.castStateByState = i(this.states, this.castStates)
        },
        getTypeByElement: function(e) {
            return this.typeByElement[e]
        },
        getElementByType: function(e) {
            return this.elementByType[e]
        },
        getTypeByStream: function(e) {
            return this.typeByStream[e]
        },
        getStreamByType: function(e) {
            return this.streamByType[e]
        },
        getStateByCastState: function(e) {
            return this.statebyCastState[e]
        },
        getCastStateByState: function(e) {
            return this.castStateByState[e]
        }
    });
    return new n
});