define(["underscore"], function(_) {
    "use strict";
    var ttlUtil = {
        augmentDictionary: function(items, ttlItem) {
            ttlItem = ttlItem || {}; 
			
			return _.reduce(items, function(memo, item, context) {
                var time = (new Date).getTime();
                var ttl = _.isFunction(ttlItem.ttl) ? ttlItem.ttl.call(ttlItem, context, item) : ttlItem.ttl;
                var refresh = _.isFunction(ttlItem.refresh) ? ttlItem.refresh.call(ttlItem, context, item) : ttlItem.refresh;
                var augmented = ttlUtil.augmentObj(item, ttl, refresh, time);
                
				if (augmented) 
					memo[context] = augmented;
					
				return memo;
            }, {});
        },
		
        augmentObj: function(item, ttl, refresh, time) {
            var itemTtl = item._ttl;
            time = time || (new Date).getTime();
			
			if (itemTtl) {
                if (refresh) 
					itemTtl.expireAt = time + ttl;
                else if (itemTtl.expireAt <= time) 
					return null;
            } 
			else 
				itemTtl = {
					expireAt: time + ttl
				};
            
			item._ttl = itemTtl;
			return item;
        }
    };
	
    return ttlUtil;
});