define(["base/utils/browser"], function(browser) {
    "use strict";
    
    var capabilities = {
        hasConsole: !! (window.console && window.console.log && window.console.log.call),
        hasWebSocket: !! window.WebSocket,
        hasEventSource: !! window.EventSource,
        hasReplaceState: !(!window.history || !window.history.replaceState),
        hasFileUpload: !! (window.File && window.FileList && window.FileReader),
        hasMediaSource: !(!window.MediaSource && !window.WebKitMediaSource),
        
        hasObjectUrl: function() {
            var isIE11 = "ie" === browser.id && 11 === parseInt(browser.version, 10);
            return (isIE11 || browser.presto) ? false : !! (window.URL && window.URL.createObjectURL && window.URL.revokeObjectURL);
        }(),
        
        hasBlob: function() {
            try {
                return "fireos" === browser.platformID ? false : !! new Blob();
            } catch (t) {
                return false;
            }
        }(),
        
        hasFullscreen: function() {
            var div = document.createElement("div");
            return !!(div.requestFullScreen || div.webkitRequestFullScreen || div.mozRequestFullScreen || div.msRequestFullscreen);
        }(),
        
        hasCanvas: function() {
            var canvas = document.createElement("canvas");
            return !(!canvas.getContext || !canvas.getContext("2d"))
        }(),
        
        hasTouchScreen: !! ("ontouchstart" in window),
        
        hasPageVisibility: function() {
            return document.hidden === true || document.hidden === false;
        }(),
        
        hasWebKitPageVisibility: function() {
            return document.webkitHidden === true || document.webkitHidden === false;
        }(),
        
        isChromeApp: !(!window.chrome || !window.chrome.storage)
    };
    
    return capabilities;
});