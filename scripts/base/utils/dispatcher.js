define([
	"base/utils/baseClass", 
	//"base/mixins/eventMixin"
], function(baseClass) {
    "use strict";
    
	var Dispatcher = baseClass.extend({
        //mixins: eventMixin
    });
	
    return new Dispatcher();
});