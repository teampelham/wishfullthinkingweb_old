define([
	"underscore", 
	"jquery", 
	"backbone", 
	"base/utils/types/mediaTypes", 
	"base/models/library/hubModel"
], function(_, $, bb, mediaTypes, hubModel) {
    "use strict";
	
    var contentUtil = {
        _resolveURI: function(t) {
			console.warn('implement');
            /*var i = t.parts,
                n = e.map(i, function(e) {
                    return e ? encodeURIComponent(e) : ""
                });
            return t.prefix + "://" + n.join("/")*/
        },
		
        _getSectionInfo: function(t) {
			console.warn('implement');
            /*var i = t.get("librarySectionUUID"),
                n = t.get("librarySectionID");
            if (!i || !n) {
                var s = t.server.getSectionForItem(t);
                e.isEmpty(s) || (i = s.get("uuid"), n = s.id)
            }
            return {
                sectionUUID: i,
                sectionID: n
            }*/
        },
		
        uri: function(t, i) {
			console.warn('implement');
            /*var n = i && i.items;
            if (n && n.length) {
                if (n.length > 1) return this.getBatchLibraryMetadataURI(n, i);
                t = e.first(n)
            }
            return t.get("isSection") ? this.getSectionURI(t, i) : this.getLibraryMetadataURI(t, i)*/
        },
		
        getSectionURI: function(t) {
			console.warn('implement');
            /*var i = e.result(t, "url"),
                s = t.get("items").getFilterUrlParams().slice(0),
                a = t.get("currentType"),
                r = t.get("currentSubset"),
                o = s.indexOf("unwatchedLeaves=1");
            if (o > -1) {
                var l = n.getChildMediaType(t.get("type")).typeString;
                s.splice(o, 1, l + ".unwatched=1")
            }
            s.push("sourceType=" + a.id);
            var c = i + "/" + r.id + "?" + s.join("&");
            return this._resolveURI({
                prefix: "library",
                parts: [t.get("uuid"), "directory", c]
            })*/
        },
		
        getBatchLibraryMetadataURI: function(t) {
			console.warn('implement');
            /*var i = this._getSectionInfo(e.first(t)),
                n = e.map(t, function(e) {
                    return e.get("ratingKey")
                }),
                s = "/library/metadata/" + n.join(",");
            return this._resolveURI({
                prefix: "library",
                parts: [i.sectionUUID, "directory", s]
            })*/
        },
		
        getLibraryMetadataURI: function(t, n) {
			console.warn('implement');
            /*n || (n = {});
            var a;
            e.isArray(t) && (a = t, t = e.first(t));
            var r, o = this._getSectionInfo(t),
                l = e.result(t, "url");
            if (n.excludeSeedItem && (l += l.indexOf("?") > -1 ? "&" : "?", l += "excludeSeedItem=1"), r = t instanceof i.Collection || t instanceof s ? "directory" : "item", a) {
                var c = e.map(a, function(e) {
                    return e.get("ratingKey")
                });
                r = "directory", l = "/library/metadata/" + c.join(",")
            }
            return this._resolveURI({
                prefix: "library",
                parts: [o.sectionUUID, r, l]
            })*/
        }
    };
	
    return contentUtil;
});