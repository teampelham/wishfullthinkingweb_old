define([
	"underscore", 
	"jquery", 
	"base/utils/baseClass", 
	"base/adapters/loggerAdapter"
], function(_, $, baseClass, loggerAdapter) {
    "use strict";
    
	var baseWorker = baseClass.extend({
        initialize: function() {
            this.jobs = []
        },
        
        minDelay: 2e3,
        maxJobAge: 0,
        
        schedule: function() {
            throw new Error("`schedule` must be implemented by a subclass");
        },
        process: function() {
            throw new Error("`process` must be implemented by a subclass");
        },
        timeout: function() {
            throw new Error("`timeout` must be implemented by a subclass");
        },
        
        add: function(jobData) {
            var job = _.find(this.jobs, function(job) {
                return job.data === jobData;
            });
            
            if (job) 
                return job.promise;
            
            var now = +new Date;
            var newJob = {
                data: jobData,
                addedAt: now
            };
            this.jobs.push(newJob); 
            this.rescheduleJobInQueue(newJob); 
            this.scheduleNextTick();
        },
        
        remove: function(jobData) {
            this.jobs = _.filter(this.jobs, function(job) {
                return job.data !== jobData;
            }), 
            this.scheduleNextTick();
        },
        
        rescheduleJobInQueue: function(job) {
            var age = this.jobAge(job);
            var schedule = this.schedule(job.data, age);
            
            this.logRescheduledJob(job, {
                jobName: "job",
                delay: schedule
            });
            
            var now = +new Date();
            job.processAt = now + schedule;
            this.jobs = _.sortBy(this.jobs, function(j) {
                return j.processAt;
            });
        },
        
        logRescheduledJob: function(e, job) {
            var delay = Math.round(job.delay / 1e3);
            var msg = "Retrying " + job.jobName;
            
            if(job.subjectName)
                msg += " for " + job.subjectName;
            
            msg += " in " + delay + " seconds";
            loggerAdapter.log(msg);
        },
        
        scheduleNextTick: function() {
            var nextJob = this.jobs[0];
            
            if (this._timeout) 
                clearTimeout(this._timeout); 
            
            if (nextJob) 
                this._timeout = setTimeout(_.bind(this.processJob, this, nextJob), this.jobDelay(nextJob));
        },
        
        jobDelay: function(nextJob) {
            var date = +new Date;
            return Math.max(nextJob.processAt - date, 1, this.minDelay);
        },
        
        jobAge: function(job) {
            var now = +new Date();
            return now - job.addedAt;
        },
        
        processJob: function(job) {
            var data = job.data;
            var age = this.jobAge(job);
            
            if (this.maxJobAge && age >= this.maxJobAge) {
                this.removeJob(job);
                this.scheduleNextTick();
                return void this.timeout(data, age);
            }
            
            var process = this.process(data, age);
            var processComplete = process ? process.then ? process : $.when(process) : $.Deferred().reject();
            
            processComplete.then(_.bind(this.onProcessSuccessful, this, job), _.bind(this.onProcessFailure, this, job));
        },
        
        removeJob: function(job) {
            this.jobs.splice(_.indexOf(this.jobs, job), 1);
        },
        
        onProcessSuccessful: function(job) {
            this.removeJob(job); 
            this.scheduleNextTick();
        },
        
        onProcessFailure: function(job) {
            this.rescheduleJobInQueue(job); 
            this.scheduleNextTick();
        }
    });
    
    return baseWorker;
});