define(["base/utils/workers/baseWorker"], function(baseWorker) {
    "use strict";
    
	var serverEventRetryWorker = baseWorker.extend({
        minDelay: 3e3,
        maxDelay: 6e5,
        
		initialize: function(serverEventManager) {
            baseWorker.prototype.initialize.apply(this, arguments); 
			this.serverEventManager = serverEventManager;
        },
        /*
		process: function(e) {
            return this.serverEventManager.connect({
                server: e
            })
        },
        
		schedule: function(e, t) {
            return Math.max(Math.min(Math.floor(2 * t), this.maxDelay), this.minDelay)
        },
        
		logRescheduledJob: function(t, i) {
            e.prototype.logRescheduledJob.call(this, t, {
                jobName: "server events connection",
                subjectName: t.data.get("friendlyName"),
                delay: i.delay
            })
        }*/
    });
	
    return serverEventRetryWorker;
});