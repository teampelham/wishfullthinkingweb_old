define(["base/utils/workers/baseWorker"], function(baseWorker) {
    "use strict";
	
    var baseServerRetryWorker = baseWorker.extend({
        initialize: function(serverConnectionManager) {
            baseWorker.prototype.initialize.apply(this, arguments); 
			this.serverConnectionManager = serverConnectionManager;
        },
        
		process: function(connections) {
            return this.serverConnectionManager.testAllConnections(connections);
        },
        
		logRescheduledJob: function(t, i) {
            baseWorker.prototype.logRescheduledJob.call(this, t, {
                jobName: "connection tests",
                subjectName: t.data.get("friendlyName"),
                delay: i.delay
            });
        }
    });
	
    return baseServerRetryWorker;
});