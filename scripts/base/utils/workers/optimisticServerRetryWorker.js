define(["base/utils/workers/baseServerRetryWorker"], function(baseServerRetryWorker) {
    "use strict";
    
	var optimisticServerRetryWorker = baseServerRetryWorker.extend({
        minDelay: 500,
        maxJobAge: 12e4,
        
		schedule: function() {
            return 2e3;
        },
		
        timeout: function(e) {
            var t = e.get("update");
            t.set("state", "error");
        }
    });
	
    return optimisticServerRetryWorker;
});