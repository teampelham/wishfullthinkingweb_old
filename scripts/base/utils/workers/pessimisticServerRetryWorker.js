define(["base/utils/workers/baseServerRetryWorker"], function(baseServerRetryWorker) {
    "use strict";
    
	var pessimisticServerRetryWorker = baseServerRetryWorker.extend({
        minDelay: 3e3,
        maxDelay: 6e5,
		
        schedule: function(e, t) {
            return Math.max(Math.min(Math.floor(1.15 * t), this.maxDelay), this.minDelay);
        }
    });
	
    return pessimisticServerRetryWorker;
})