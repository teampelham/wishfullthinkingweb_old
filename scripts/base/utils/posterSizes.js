define([], function() {
    "use strict";
    
	var posterSizes = {
        sizes: {
            movie: "portrait",
            show: "portrait",
            season: "portrait",
            episode: "landscape",
            artist: "square",
            album: "square",
            track: "square",
            photo: "square",
            photoAlbum: "square",
            clip: "landscape",
            playlist: "square",
            playlistFolder: "square",
            userPlaylistItem: "landscape",
            channel: "square",
            channelDirectory: "square",
            folder: "portrait",
            friend: "square",
            poster: "portrait",
            art: "landscape",
            banner: "banner",
            light: "square",
            kinect: "square"
        },
        ratios: {
            portrait: 1 / 1.5,
            square: 1,
            landscape: 16 / 9,
            banner: 5.2
        },
		
        getRatioByType: function(posterType) {
            return this.ratios[this.sizes[posterType]];
        },
        
		getPosterWidth: function(posterType, width) {
            return width * this.getRatioByType(posterType)
        },
        
		getPosterHeight: function(posterType, height) {
            return height / this.getRatioByType(posterType)
        }
    };
	
    return posterSizes;
});