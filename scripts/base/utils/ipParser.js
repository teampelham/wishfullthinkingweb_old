define(["underscore"], function(e) {
    "use strict";
	
    var ipParser = {
        ipv4Regex: /^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/,
        
        ipv6ValidityRegex: /(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))/i,
        
        ipv6ParsingRegex: /[a-f0-9]{1,4}/gi,
        
        isValidAddress: function(url) {
            return this.isValidV4(url) || this.isValidV6(url);
        },
        
        isLoopbackAddress: function(url) {
            return this.isLoopbackV4(url) || this.isLoopbackV6(url);
        },
        
        isPrivateAddress: function(url) {
            return this.isPrivateV4(url) || this.isPrivateV6(url);
        },
        
        parseV4: function(url) {
            var ipv4Match = this.ipv4Regex.exec(url);
            if (null !== ipv4Match) 
                return {
                    octets: _.map(_.rest(ipv4Match), function(item) {
                        return parseInt(item, 10);
                    })
                };
        },
        
        isValidV4: function(url) {
            var ipv4 = this.parseV4(url);
            
            return !(!ipv4 || !_.all(ipv4.octets, function(octet) {
                return octet >= 0 && 255 >= octet;
            }));
        },
        
        isLoopbackV4: function(url) {
            var ipv4 = this.parseV4(url);
            
            return !(!ipv4 || 127 !== ipv4.octets[0] && !_.all(ipv4.octets, function(octet) {
                return 0 === octet;
            }));
        },
        
        isPrivateV4: function(url) {
            var ipv4 = this.parseV4(url);
            if (ipv4) {
                if (10 === ipv4.octets[0])
                    return true;
                if (172 === ipv4.octets[0] && ipv4.octets[1] >= 16 && ipv4.octets[1] <= 31) 
                    return true;
                if (192 === ipv4.octets[0] && 168 === ipv4.octets[1]) 
                    return true;
            }
            return false;
        },
        
        parseV6: function(url) {
            if (this.ipv6ValidityRegex.test(url)) {
                var urlParts = url.split("::");
                var ipv6Match = urlParts[0].match(this.ipv6ParsingRegex);
                var partsContainer = {
                    parts: []
                };
                
                if (null !== ipv6Match) {
                    if (ipv6Match.length > 8) 
                        return;
                    partsContainer.parts = _.map(ipv6Match, function(v6Part) {
                        return parseInt(v6Part, 16);
                    });
                }
                
                if (urlParts.length > 1) {
                    var ipv6Match2 = urlParts[1].match(this.ipv6ParsingRegex);
                    var ipv6Parts2 = _.map(ipv6Match2, function(e) {
                        return parseInt(e, 16);
                    });
                    var missingParts = 8 - partsContainer.parts.length - ipv6Parts2.length;
                    
                    if (0 > missingParts || 8 == missingParts) 
                        return;
                    
                    _.times(missingParts, function() {
                        partsContainer.parts.push(0);
                    }); 
                    partsContainer.parts = partsContainer.parts.concat(ipv6Parts2);
                }
                return partsContainer;
            }
        },
        
        isValidV6: function(url) {
            var ipv6 = this.parseV6(url);
            return !(!ipv6 || !_.all(ipv6.parts, function(ipv6Part) {
                return e >= ipv6Part && 65535 >= ipv6Part;
            }));
        },
        
        isLoopbackV6: function(url) {
            var ipv6 = this.parseV6(url);
            if (ipv6) {
                var allZeroes = _.all(_.take(ipv6.parts, 7), function(part) {
                    return 0 === part;
                });
                
                return allZeroes && 1 === _.last(ipv6.parts);
            }
            return false;
        },
        
        isPrivateV6: function(url) {
            var ipv6 = this.parseV6(url);
            return !!(ipv6 && ipv6.parts[0] >= 64512 && ipv6.parts[0] <= 65023);
        }
    };
    
    return ipParser;
});