define(["underscore", "jquery", "base/utils/mediaUtil", "base/adapters/loggerAdapter"], function(e, t, i, n) {
    "use strict";
    var s = {
        extensions: {
            dash: ".mpd",
            hls: ".m3u8",
            http: ""
        },
        logParams: function(t, i) {
            var s = [];
            e.each(e.keys(i), function(e) {
                s.push(e + ": " + i[e])
            }), n.group("Transcoder: " + t + " options", s)
        },
        serializeParams: function(i) {
            var n, s = {}, a = {}, r = /^X-Plex/;
            return e.each(i, function(e, t) {
                r.test(t) ? a[t] = e : s[t] = e
            }), s = t.param(s, !0), a = t.param(a, !0), n = s && a ? s + "&" + a : s || a
        },
        image: function(t, i, n) {
            if (!i) return "";
            var s = n.fallbackServer,
                a = !1;
            if (s)
                if (t.get("cloud")) a = !0;
                else if (t.get("synced")) {
                var r = !t.get("activeConnection").isLocalAddress,
                    o = s.get("activeConnection").isLocalAddress;
                a = r || o
            }
            var l = a ? s : t,
                c = l.id === t.id;
            t.get("cloud") && (i = s ? s.resolveUrl(i, {
                appendToken: !0
            }) : t.resolveUrl(i, {
                appendToken: !0,
                connection: {
                    scheme: "https",
                    address: "node.plexapp.com",
                    port: "32443"
                }
            }));
            var d = {
                url: t.resolveUrl(i, {
                    useLoopback: c,
                    appendToken: !0
                }),
                width: parseInt(n.width, 10),
                height: parseInt(n.height, 10)
            };
            n.minSize && (d.minSize = 1), n.format && (d.format = n.format), n.filters && e.each(n.filters, function(e, t) {
                void 0 !== e && (d[t] = e)
            });
            var u = "/photo/:/transcode?" + this.serializeParams(d, l);
            return l.resolveUrl(u, {
                appendToken: !0
            })
        },
        music: function(t, i, n) {
            var s = e.first(i.get("Media"));
            if (s) {
                var a = e.first(s.Part);
                if (a) {
                    var r, o;
                    return t.supports("universalMusicTranscoder") ? (r = e.extend({
                        path: t.resolveUrl(i.get("key"), {
                            useLoopback: !0
                        }),
                        mediaIndex: 0,
                        partIndex: 0
                    }, n), o = "/music/:/transcode/universal/start.mp3") : (r = {
                        identifier: i.get("identifier"),
                        url: t.resolveUrl(a.key, {
                            useLoopback: !0
                        }),
                        format: "mp3",
                        audioCodec: "libmp3lame",
                        audioBitrate: 320,
                        audioSamples: 44100
                    }, o = "/music/:/transcode/generic.mp3"), this.logParams("Music", r), r = e.extend(r, t.getHeaders()), t.resolveUrl(o + "?" + this.serializeParams(r, t))
                }
            }
        },
        video: function(t, n, s, a, r) {
            r || (r = {});
            var o = t.id === n.server.id,
                l = n.server.get("cloud") ? t : n.server,
                c = e.extend({
                    path: l.resolveUrl(n.get("key"), {
                        useLoopback: o,
                        appendToken: !o
                    }),
                    mediaIndex: i.getMediaIndex(n, s.id),
                    partIndex: i.getPartIndex(s, a.key),
                    protocol: "hls",
                    offset: 0,
                    fastSeek: 1,
                    directPlay: 0,
                    directStream: 1,
                    subtitleSize: 100,
                    audioBoost: 100
                }, r);
            c.path = c.path.replace(/^http[s]?:\/\/node.plexapp.com:[\d]+\//, t.url()), this.logParams("Video", c);
            var d = n.get("indirectHttpHeaders");
            d && (c.httpCookies = d.Cookie, c.userAgent = d["User-Agent"]), c = e.extend(c, t.getHeaders());
            var u = this.extensions[c.protocol],
                h = "/video/:/transcode/universal/start" + u;
            return t.resolveUrl(h + "?" + this.serializeParams(c, t))
        }
    };
    return s
});