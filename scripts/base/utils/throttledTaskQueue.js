define([
	"underscore", 
	"base/adapters/performanceAdapter"
], function(_, performanceAdapter) {
    "use strict";

    function runTasks(timeout, batchSize) {
        timer = setTimeout(function() {
            timer = null; 
			removeExpired(); 
			_.times(batchSize, runTask); 
			
			if (taskQueue.length) 
				runTasks(timeout, batchSize);
        }, timeout);
    }

    function removeExpired() {
        var dateNow = (new Date).getTime();
        taskQueue = _.filter(taskQueue, function(task) {
            return task.expire > dateNow;
        });
    }

    function runTask() {
        for (var e = 0; e < taskQueue.length; e++) {
            var task = taskQueue[e];
            var failed = false;
            
			try {
                failed = task.fn.apply(task.thisArg);
            } catch (err) {
                console.error(err); 
				failed = true;
            }
			
            if (failed) {
                taskQueue.splice(e, 1);
                break;
            }
        }
    }
	
    var throttledTaskQueue = {
        defaultWait: 100,
        defaultBatchSize: 1,
        timeout: 3e4,
        
		enqueue: function(fn, thisArg, cancelToken, runNext) {
            var task = {
                fn: fn,
                thisArg: thisArg,
                cancelToken: cancelToken,
                expire: (new Date).getTime() + this.timeout
            };
			
			runNext ? taskQueue.unshift(task) : taskQueue.push(task);
            if (!timer) {
                var throttle = performanceAdapter.prop("taskThrottle") || {};
				var wait = throttle.wait || this.defaultWait;
                var batchSize = throttle.batchSize || this.defaultBatchSize;
                
				runTasks(wait, batchSize);
            }
        },
		
        cancel: function(token) {
            taskQueue = _.reject(taskQueue, function(task) {
                return task.cancelToken === token;
            });
        }
    };
	
	var taskQueue = [];
    var timer = null;
	
    return throttledTaskQueue;
});