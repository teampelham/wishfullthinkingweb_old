define(["underscore", "x2js", "base/adapters/loggerAdapter"], function(_, X2JS, logger) {
    "use strict";

    function foreach(callback, items) {
        _.isArray(items) ? _.each(items, callback) : callback(items);
    }

    function s(t) {
        return e.reduce(t, function(e, t) {
            var n = t.split("="),
                s = n[0],
                a = n[1];
            try {
                s = decodeURIComponent(s), a = void 0 === a ? null : decodeURIComponent(a), e.hasOwnProperty(s) ? Array.isArray(e[s]) ? e[s].push(a) : e[s] = [e[s], a] : e[s] = a
            } catch (r) {
                i.warn("Unable to decode query string at " + s)
            }
            return e
        }, {})
    }
    var x2jsObj = new X2JS(false, "", true);
    var parser = {
        parseXML: function(str) {
            var xml = x2jsObj.parseXmlString(str);
            if (xml) {
                if (xml.getElementsByTagName("parsererror").length) {
                    var theyMightBeJSON = this.isJSONString(str);
                    if(!!theyMightBeJSON)       // Screw XML, I want to pass in a JSON string!
                        return theyMightBeJSON;
                    logger.warn("parseXML: Errors found while parsing XML document")
                }
                return x2jsObj.xml2json(xml);
            }
            return null;
        },
        
        isJSONString: function(str) {
            try {
                var couldItBe = JSON.parse(str);
                if(_.isObject(couldItBe))
                    return couldItBe;
                else if(_.isString(couldItBe))
                    return this.isJSONString(couldItBe);
            }
            catch(err) { }
            
            return false;
        },
        
        replaceXMLElementNames: function(t, i, n) {
            _.isArray(i) || (i = [i]);
            var s = i.join("|"),
                a = new RegExp("<(" + s + ")", "g"),
                r = new RegExp("</(" + s + ")>", "g");
            return t = t.replace(a, "<" + n + ' elementName="$1"'), t = t.replace(r, "</" + n + ">")
        },
        /*
        parseEncodedString: function(e, t) {
            function i(t) {
                if (e.hasOwnProperty(t)) {
                    var i = e[t];
                    if (null == i) e[t] = "";
                    else try {
                        e[t] = decodeURIComponent(i)
                    } catch (n) {}
                }
            }
            e && n(i, t)
        },*/
        parseInteger: function(parseObj, intProp) {
            function intParse(intProp) {
                if (parseObj.hasOwnProperty(intProp)) {
                    var toInt = parseObj[intProp];
                    parseObj[intProp] = (null == toInt || "" === toInt) ? 0 : parseInt(toInt, 10);
                }
            }
            if (parseObj) 
                foreach(intParse, intProp);
        },
        
        parseFloat: function(parseObj, floatProp) {
            function floatParse(floatProp) {
                if (parseObj.hasOwnProperty(floatProp)) {
                    var toFloat = parseObj[floatProp];
                    parseObj[floatProp] = null === toFloat || "" === toFloat ? 0 : parseFloat(toFloat);
                }
            }
            if (parseObj)
                foreach(floatParse, floatProp);
        },
        
        parseBoolean: function(parseObj, boolProp) {
            function boolParse(boolProp) {
                if (parseObj.hasOwnProperty(boolProp)) {
                    var toBool = parseObj[boolProp];
                    parseObj[boolProp] = toBool === true || "true" == toBool || "1" == toBool;
                }
            }
            if (parseObj) 
                foreach(boolParse, boolProp);
        },
        
        parseArray: function(collection, parseItem) {
            function parseCallback(i) {
                if (collection.hasOwnProperty(parseItem)) {
                    var item = collection[parseItem];
                    if (!_.isArray(item)) 
                        collection[parseItem] = [item];
                } 
                else 
                    collection[parseItem] = [];
            }
            
            if (collection) 
                foreach(parseCallback, parseItem);
        },
        /*
        parseQueryString: function(t, i, a) {
            function r(i) {
                if (t.hasOwnProperty(i)) {
                    var n = t[i];
                    if (!e.isObject(n)) return e.isString(n) && (n = n.trim().replace(/^(\?|#)/, "")) ? void(t[i] = s(n.split(a))) : void(t[i] = {})
                }
            }
            t && (a || (a = "&"), n(r, i))
        }*/
    };
    return parser;
});