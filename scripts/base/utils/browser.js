define(["underscore", "base/utils/baseClass"], function(_, baseClass) {
    "use strict";
    var i = /version\/(\d+(\.\d+)?)/i,
        n = /(?:chrome|crios|gfibertv)[\/-](\d+(\.\d+)?)/i,
        s = /firefox[ \/](\d+(\.\d+)?)/i,
        a = /(msie |rv:)(\d+(\.\d+)?)/i,
        r = /(?:applewebkit|safari)\/(\d+(\.\d+)?)/i,
        browser = baseClass.extend({
            browsers: [{
                id: "konvergo",
                name: "Konvergo",
                webkit: true,
                regex: /Konvergo/,
                versionRegex: /Konvergo\S* (\d+(\.\d+)+)/i
            }, {
                id: "wiiu",
                name: "Wii U",
                msie: true,
                regex: /wiiu/i,
                versionRegex: r
            }, {
                id: "ps3",
                name: "Unknown Browser",
                regex: /\(PLAYSTATION 3 \d\./
            }, {
                id: "ps3",
                name: "Unknown Browser",
                regex: /\(PlayStation 4 \d\./
            }, {
                id: "ps3",
                name: "Mystery 3",
                webkit: true,
                regex: /\(PlayStation 3\)/,
                versionRegex: r
            }, {
                id: "ps4",
                name: "Mystery 4",
                webkit: true,
                regex: /\(PlayStation 4\)/,
                versionRegex: r
            }, {
                id: "ie",
                name: "Internet Explorer",
                msie: true,
                regex: /(msie|trident)/i,
                versionRegex: a,
                versionMatchIndex: 2
            }, {
                id: "opera",
                name: "Opera",
                presto: true,
                regex: /opera/i,
                versionRegex: i
            }, {
                id: "opera",
                name: "Opera",
                webkit: true,
                regex: /OPR\//,
                versionRegex: /OPR\/\s?(\d+(\.\d+)?)/
            }, {
                id: "chrome",
                name: "Chrome",
                webkit: true,
                regex: /(chrome|crios|gfiber)/i,
                versionRegex: n
            }, {
                id: "firefox",
                name: "Firefox",
                gecko: true,
                regex: /firefox/i,
                versionRegex: s
            }, {
                id: "safari",
                name: "Safari",
                webkit: true,
                regex: /os x.+safari/i,
                versionRegex: i
            }, {
                id: "safari",
                name: "Safari",
                webkit: true,
                regex: /applewebkit.+version/i,
                versionRegex: /applewebkit.+version\/(\d+(\.\d+)?)/i
            }, {
                id: "safari",
                name: "Safari",
                webkit: true,
                regex: /applewebkit/i,
                versionRegex: /(?:applewebkit|safari)\/(\d+(\.\d+)?)/i
            }],
            platforms: [{
                id: "webos",
                name: "webOS",
                tv: true,
                predicate: function(e) {
                    var t = /web[o0]s/i.test(e) && /large screen/i.test(e);
                    return this.version(e) ? t : false
                },
                version: function() {
                    try {
                        return JSON.parse(window.PalmSystem.deviceInfo).platformVersion
                    } catch (e) {}
                }
            }, {
                id: "weboshybrid",
                name: "webOS",
                tv: true,
                predicate: function(e) {
                    var t = /web[o0]s/i.test(e) && /large screen/i.test(e);
                    return !(!t || !window.NetCastGetMouseOnOff)
                }
            }, {
                id: "netcast",
                name: "NetCast",
                tv: true,
                regex: /netcast/i
            }, {
                id: "fireos",
                name: "Fire TV Stick",
                tv: true,
                regex: /AFTM/,
                version: function(e) {
                    var t = /cordova-amazon-fireos\/([\d\.]+)/.exec(e);
                    return t && t[1] || void 0
                }
            }, {
                id: "fireos",
                name: "Fire TV",
                tv: true,
                regex: /AFT./,
                version: function(e) {
                    var t = /cordova-amazon-fireos\/([\d\.]+)/.exec(e);
                    return t && t[1] || void 0
                }
            }, {
                id: "blackberry",
                name: "Blackberry",
                mobile: true,
                regex: /BlackBerry|RIM|BB10/
            }, {
                id: "ps3",
                name: "PlayStation 3",
                tv: true,
                regex: /P(lay)?S(tation )?3/,
                version: function() {
                    return window.WM_devSettings ? window.WM_devSettings.version : void 0
                }
            }, {
                id: "ps4",
                name: "PlayStation 4",
                tv: true,
                regex: /P(lay)?S(tation )?4/,
                version: function() {
                    return window.WM_devSettings ? window.WM_devSettings.version : void 0
                }
            }, {
                id: "wiiu",
                name: "Wii U",
                tv: true,
                regex: /Nintendo WiiU/
            }, {
                id: "smarthub",
                name: "SmartHub",
                tv: true,
                regex: /smarthub/i
            }, {
                id: "sta",
                name: "Smart TV Alliance",
                tv: true,
                version: function(t) {
                    var i, n;
                    if (/SmartTvA/.test(t)) n = /SmartTvA\/(\d+\.\d+)/.exec(t), i = n && n[1] || i;
                    else {
                        var s = [/Linux i686.+Chromium\/18/, /nettv\/4.2/i, /toshiba-dtv/i, /viera/i],
                            a = [/nettv\/4/i],
                            r = [/nettv/i],
                            o = function(e) {
                                return e.test(t)
                            };
                        _.some(s, o) ? i = "2.0" : _.some(a, o) ? i = "1.0" : _.some(r, o) && (i = "0.0")
                    }
                    return i
                },
                predicate: function(e) {
                    return void 0 !== this.version(e)
                }
            }, {
                id: "tivo",
                name: "TiVo",
                tv: true,
                version: function(e) {
                    var t = /TiVo_(TCD[^,]+)/.exec(e);
                    return t && t[1] || void 0
                },
                predicate: function(e) {
                    return /TiVo/.test(e)
                }
            }, {
                id: "viziomtk",
                name: "Vizio MTK",
                tv: true,
                predicate: function(e) {
                    return /opera/i.test(e) && "undefined" != typeof iMTK_JSP
                }
            }, {
                id: "viziosigma",
                name: "Vizio Sigma",
                tv: true,
                predicate: function(e) {
                    return /opera/i.test(e) && /vizio/i.test(e)
                },
                version: function(e) {
                    var t = /\/([^\s]+) \(Vizio/.exec(e);
                    return t && t[1] || void 0
                }
            }, {
                id: "operatvstore",
                name: "Opera TV Store",
                tv: true,
                predicate: function(e) {
                    var t = /Opera TV Store/.test(e),
                        i = /TV Store/.test(e) && /OMI\//.test(e);
                    return t || i
                }
            }, {
                id: "operatv",
                name: "Opera TV",
                tv: true,
                regex: /opera\/\d+\.\d+ \(linux (mips|arm)/i
            }, {
                id: "googlefiber",
                name: "Google Fiber",
                tv: true,
                regex: /gfibertv/i
            }, {
                id: "ios",
                name: "iOS",
                mobile: true,
                regex: /(iphone|ipad|ipod)/i
            }, {
                id: "android",
                name: "Android",
                mobile: true,
                regex: /android/i
            }, {
                id: "windowsphone",
                name: "Windows Phone",
                mobile: true,
                regex: /windows phone/i
            }, {
                id: "webos",
                name: "webOS",
                mobile: true,
                predicate: function(e) {
                    return /web[o0]s/i.test(e) && !/large screen/i.test(e)
                }
            }, {
                id: "linuxrpi",
                name: "Linux Raspberry Pi",
                tv: true,
                regex: /Konvergo-linux-rpi/i,
                version: function(e) {
                    var t = /\(([^\)]+)\)$/.exec(e);
                    return t && t[1] || void 0
                }
            }, {
                id: "linux",
                name: "Linux",
                context: "platform",
                regex: /linux/i
            }, {
                id: "osx",
                name: "OSX",
                context: "platform",
                regex: /mac/i
            }, {
                id: "windows",
                name: "Windows",
                context: "platform",
                regex: /win/i
            }],
            id: "unknown",
            name: "Unknown Browser",
            platform: "Unknown Platform",
            version: "0",
            platformVersion: "0",
            platformModifierKey: "ctrl",
            webkit: false,
            gecko: false,
            msie: false,
            presto: false,
            
            constructor: function(browser) {
                var userAgent = "";
                var platform = "";
                
                if (browser) {
                    userAgent = browser.userAgent; 
                    platform = browser.platform;
                }
                else if (null !== navigator) {
                    userAgent = window.navigator.userAgent; 
                    platform = window.navigator.platform;
                }
                
                this.detect({
                    userAgent: userAgent,
                    platform: platform
                });
            },
            
            detect: function(browserInfo) {
                var version;
                var browserMatch = this.runTests(this.browsers, browserInfo);
                var platformMatch = this.runTests(this.platforms, browserInfo);
                
                if (browserMatch) {
                    this.id = browserMatch.id;
                    this.name = browserMatch.name;
                    this.version = this.getVersion(browserInfo.userAgent, browserMatch.versionRegex, browserMatch.versionMatchIndex || 1); 
                    this.webkit = !! browserMatch.webkit;
                    this.gecko = !! browserMatch.gecko;
                    this.msie = !! browserMatch.msie;
                    this.presto = !! browserMatch.presto;
                }
                if (platformMatch) {
                    version = _.isFunction(platformMatch.version) ? platformMatch.version(browserInfo.userAgent) : platformMatch.version;
                    this.platform = platformMatch.name;
                    this.platformID = platformMatch.id;
                    this.platformVersion = version || this.platformVersion;
                    this.mobile = !! platformMatch.mobile;
                    this.tv = !! platformMatch.tv;
                    if ("OSX" === platformMatch.name || "iOS" === platformMatch.name) 
                        this.platformModifierKey = "command";
                }
            },
            
            runTests: function(potentials, browser) {
                var test;
                var continueTests;
                var current;

                for (var a = 0; continueTests !== true && (current = potentials[a++]);) {
                    test = browser[current.context || "userAgent"];
                    if (current.regex)
                        continueTests = current.regex.test(test);
                    else if (current.predicate)
                        continueTests = current.predicate(test);
                }
                return current;
            },
            
            getVersion: function(userAgent, versionRegex, versionMatchIndex) {
                var versionMatch = userAgent.match(versionRegex);
                return versionMatch && versionMatch.length > versionMatchIndex && versionMatch[versionMatchIndex] || "0";
            },
            
            toJSON: function() {
                var json = {};
                for (var i in this)
                    if (_.has(this, i) && this[i]) 
                        json[i] = this[i];
                return json;
            }
        });
        
    return new browser();
});