define([
	"underscore", 
	"base/utils/baseClass"
], function(_, baseClass) {
    "use strict";
	
    var viewPool = baseClass.extend({
        initialize: function() {
            this.pool = [];
        },
        
        lockOrCreate: function(views, callback, context) {
            _.each(views, function(view) {
                if (!this.find(view)) {
                    var missingViews = _.find(this.pool, function(poolView) {
                        return !_.contains(views, poolView.id);
                    }, this);
                    
                    if (!missingViews) {
                        missingViews = {}; 
                        this.pool.push(missingViews); 
                        missingViews.view = callback.call(context, missingViews);
                    }
                    missingViews.id = view;
                }
            }, this);
        },
        
        each: function(fn, arg) {
            _.each(this.pool, fn, arg);
        },
        
        filter: function(fn, arg) {
            this.pool = _.filter(this.pool, fn, arg);
        },
        
        find: function(id) {
            return _.findWhere(this.pool, {
                id: id
            });
        },
        
        findByModel: function(model) {
            return _.find(this.pool, function(poolView) {
                return poolView.view.model === model;
            });
        },
        
        close: function() {
            _.each(this.pool, function(poolView) {
                poolView.view.close();
            }); 
            this.pool = [];
        }
    });
	
    return viewPool;
});