define([
	"underscore", 
	"jquery", 
	"base/utils/rangeUtil"
], function(_, $, rangeUtil) {
    "use strict";
	
    var paginationMixin = {
        fetchRange: function(range) {
            range.remove = false; 
			range.scope = "page"; 
			if (range.prepend) 
				range.at = 0; 
			
			this.pageStart = range.start; 
			this.pageSize = range.size; 
			return this.fetch(range).promise();
        },
		
        populateRange: function(n) {
            var s = $.Deferred();
            if (this.hasPendingPopulate()) return s.reject().promise();
            this.populatedRange || (this.populatedRange = {
                start: 0,
                end: 0
            });
            var a = rangeUtil.difference(n, this.populatedRange)[0];
            if (!a) return s.resolve().promise();
            if (a = rangeUtil.degap(a, this.populatedRange), n.reloadThreshold && a.size > n.reloadThreshold) return this.reset([], {
                silent: !0
            }), this.populatedRange = {
                start: 0,
                end: 0
            }, this.populateRange(_.omit(n, "reloadThreshold"));
            var r = _.extend(n, a);
            r.prepend = a.start < this.populatedRange.start;
            var o = this.fetchRange(r);
            return o.done(_.bind(function(e) {
                this.populatedRange = {
                    start: rangeUtil.union(this.populatedRange, a)[0].start,
                    size: e.length
                }, s.resolve(e)
            }, this)), o.fail(s.reject), s.promise()
            /*var deferred = $.Deferred();
            
			if (this.hasPendingPopulate()) 
				return deferred.reject().promise();
            
			if (!this.populatedRange) 
                this.populatedRange = {
                    start: 0,
                    end: 0
                };
            
            var rangeDifference = rangeUtil.difference(range, this.populatedRange)[0];
            if (!rangeDifference) 
				return deferred.resolve().promise();
			
            rangeDifference = rangeUtil.degap(rangeDifference, this.populatedRange);
            
            if (range.reloadThreshold && rangeDifference.size > range.reloadThreshold) 
				return this.reset([], {
                	silent: true
            	});
				
			this.populatedRange = {
                start: 0,
                end: 0
            };
            
			this.populateRange(_.omit(range, "reloadThreshold"));
            var extendedRange = _.extend(range, rangeDifference);
            extendedRange.prepend = rangeDifference.start < this.populatedRange.start;
            var fetchPromise = this.fetchRange(extendedRange);
            
			fetchPromise.done(_.bind(function(newRange) {
                this.populatedRange = {
                    start: rangeUtil.union(this.populatedRange, rangeDifference)[0].start,
                    size: newRange.length
                }; 
				deferred.resolve(newRange);
            }, this)); 
            
			fetchPromise.fail(deferred.reject);
            
			return deferred.promise();*/
        },
		
        remove: function(range) {
            var count = _.isArray(range) ? range.length : 1;
            
			if (this.populatedRange && this.populatedRange.end) 
				this.populatedRange.end -= count;
        }
    };
	
    return paginationMixin;
});