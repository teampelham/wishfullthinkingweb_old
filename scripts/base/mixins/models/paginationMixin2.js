define([
	"underscore", 
	"jquery", 
	"base/utils/rangeUtil"
], function(e, t, i) {
    "use strict";
    var n = {
        fetchRange: function(e) {
            return e.remove = !1, e.scope = "page", e.prepend && (e.at = 0), this.pageStart = e.start, this.pageSize = e.size, this.fetch(e).promise()
        },
        populateRange: function(n) {
            var s = t.Deferred();
            if (this.hasPendingPopulate()) return s.reject().promise();
            this.populatedRange || (this.populatedRange = {
                start: 0,
                end: 0
            });
            var a = i.difference(n, this.populatedRange)[0];
            if (!a) return s.resolve().promise();
            if (a = i.degap(a, this.populatedRange), n.reloadThreshold && a.size > n.reloadThreshold) return this.reset([], {
                silent: !0
            }), this.populatedRange = {
                start: 0,
                end: 0
            }, this.populateRange(e.omit(n, "reloadThreshold"));
            var r = e.extend(n, a);
            r.prepend = a.start < this.populatedRange.start;
            var o = this.fetchRange(r);
            return o.done(e.bind(function(e) {
                this.populatedRange = {
                    start: i.union(this.populatedRange, a)[0].start,
                    size: e.length
                }, s.resolve(e)
            }, this)), o.fail(s.reject), s.promise()
        },
        remove: function(t) {
            var i = e.isArray(t) ? t.length : 1;
            this.populatedRange && this.populatedRange.end && (this.populatedRange.end -= i)
        }
    };
    return n
});