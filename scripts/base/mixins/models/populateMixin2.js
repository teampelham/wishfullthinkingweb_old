define([
	"underscore", "jquery", "backbone", "base/utils/xhrTracker"], function(e, t, i, n) {
    "use strict";
    var s = {
        hasPendingPopulate: function() {
            return !!this.populatePromise && "pending" === this.populatePromise.state()
        },
        populate: function() {
            return this.populatePromise || this.fetch.apply(this, arguments), this.populatePromise
        },
        fetch: function() {
            var n, s = t.Deferred();
            return this.hasPendingPopulate() ? this.populatePromise : (n = this._fetch ? this._fetch.apply(this, arguments) : this instanceof i.Collection ? i.Collection.prototype.fetch.apply(this, arguments) : i.Model.prototype.fetch.apply(this, arguments), this.populatePromise = s.promise(), this.populatePromise.rid = n.rid, n.done(e.bind(function(e) {
                s.resolveWith(this, [this, e]), this.resetPopulatePromise(t.Deferred().resolveWith(this, [this]).promise())
            }, this)), n.fail(e.bind(function(e) {
                s.rejectWith(this, [this, e]), this.resetPopulatePromise(t.Deferred().rejectWith(this, [this]).promise())
            }, this)), this.populatePromise)
        },
        abortPendingPopulate: function() {
            return this.hasPendingPopulate() && n.abort(this.populatePromise.rid), this.populatePromise
        },
        resetPopulatePromise: function(e) {
            return this.hasPendingPopulate() || (this.populatePromise = e), this.populatePromise
        },
        updatePopulatePromise: function(e) {
            var i = this.populatePromise && this.populatePromise.state();
            return i === e ? this.populatePromise : (this.resetPopulatePromise(t.Deferred()), "resolved" === e ? this.populatePromise.resolveWith(this, [this]) : "rejected" === e && this.populatePromise.rejectWith(this, [this]), this.populatePromise)
        }
    };
    return s
});