define([
	"underscore", 
	"jquery", 
	"backbone",
    "base/utils/xhrTracker"
], function(_, $, bb, xhrTracker) {
    "use strict";
    var populateMixin = {
        
        hasPendingPopulate: function() {
            // the populatePromise is a jQuery Deferred object, initially blank
            return !!this.populatePromise && "pending" === this.populatePromise.state();
        },
        
        populate: function() {
            if (!this.populatePromise)
                this.fetch.apply(this, arguments);
            return this.populatePromise;
        },
        
        fetch: function() {
            var xhr;          // n
            var defer = $.Deferred();   // s
            
            if (this.hasPendingPopulate()) 
                return this.populatePromise;
            
            if (this._fetch)
                xhr = this._fetch.apply(this, arguments);
            else if (this instanceof bb.Collection)
                xhr = bb.Collection.prototype.fetch.apply(this, arguments);
            else
                xhr = bb.Model.prototype.fetch.apply(this, arguments);
            
            this.populatePromise = defer.promise();
            this.populatePromise.rid = xhr.rid;
            
            xhr.done(_.bind(function(result) {
                defer.resolveWith(this, [this, result]);
                this.resetPopulatePromise($.Deferred().resolveWith(this, [this]).promise());
            }, this));
            
            xhr.fail(_.bind(function(result) {
                defer.rejectWith(this, [this, result]);
                this.resetPopulatePromise($.Deferred().rejectWith(this, [this]).promise());
            }, this));
            
            return this.populatePromise;
        },
        
        abortPendingPopulate: function() {

            if (this.hasPendingPopulate())
                xhrTracker.abort(this.populatePromise.rid);
                
            return this.populatePromise;
        },
        
        resetPopulatePromise: function(deferred) {
            if (!this.hasPendingPopulate())         // Updates the deferred promise if it's done or rejected
                this.populatePromise = deferred;
            
            return this.populatePromise;
        },
        
        updatePopulatePromise: function(state) {
            // determines whether the populate promise should resolve or reject
            var promiseState = this.populatePromise && this.populatePromise.state();
            
            if (promiseState === state)
                return this.populatePromise;
            
            this.resetPopulatePromise($.Deferred());
            
            if (state === "resolved")
                this.populatePromise.resolveWith(this, [this]);
            else if (state === "rejected")
                this.populatePromise.rejectWith(this, [this]);
                
            return this.populatePromise;
        }
    };
    
    return populateMixin;
});