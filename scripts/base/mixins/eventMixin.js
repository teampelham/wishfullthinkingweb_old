define(["underscore"], function(_) {
    "use strict";
    
	var spaceRegex = /\s+/; 
    
    var isEventMap = function(mixin, method, event, extraArgs) {
        if (!event) 
            return true;
            
        if ("object" == typeof event) {
            for (var ev in event) 
                mixin[method].apply(mixin, [ev, event[ev]].concat(extraArgs));
                
            return false;
        }
        
        if (spaceRegex.test(event)) {
			var events = event.split(spaceRegex);
            for (var o = 0, length = events.length; length > o; o++) 
                mixin[method].apply(mixin, [events[o]].concat(extraArgs));
                
            return false;
        }
        
        return true;
    };
	
	var executeCallbacks = function(events, args) {
        var event;
        // each event can pass in a context to execute in, making the callback execute in its local context
        for (var i = 0; i < events.length; i++) {
             event = events[i];
             event.callback.apply(event.ctx, args);
        }
    };
	
	var eventMixin = {
        // on is a way of subscribing to an event and registering a callback with optional parameters
        on: function(eventName, callback, args) {     // e, t, n
            // if the eventName turns out to be an object, its a map with the callbacks added
			if (!isEventMap(this, "on", eventName, [callback, args]) || !callback)
                return this;    // if its not a map and there's no callback, then send back the mixin
            
            if (!this._events)
                this._events = {};    
            
            var event = this._events[eventName] || (this._events[eventName] = []);       // initialize new events to an empty array
            
            event.push({
                callback: callback,
                context: args,
                ctx: args || this
            });
            
            return this;
        },
        
        once: function(t, n, s) {
            console.warn('once!');
            if (!isEventMap(this, "once", t, [n, s]) || !n) return this;
            var a = this,
                r = _.once(function() {
                    a.off(t, r), n.apply(this, arguments)
                });
            return r._callback = n, this.on(t, r, s)
        },
        
        off: function(event, arg1, s) {
            var a, subEvent, eventObj;
            
            if (!this._events || !isEventMap(this, "off", event, [arg1, s])) 
                return this;
                
            if (!event && !arg1 && !s) {
                this._events = void 0;
                return this;
            }
                
            var eventArray = event ? [event] : _.keys(this._events); 
            for (var i = 0; eventArray.length > i; i++) {
                event = eventArray[i]; 
                
                eventObj = this._events[event];
                if (eventObj) {
                    this._events[event] = a = [];
                    
                    if (arg1 || s) {
                        for (var j = 0; eventObj.length > j; j++) {
                            subEvent = eventObj[j]; 
                            if (arg1 && arg1 !== subEvent.callback && arg1 !== subEvent.callback._callback || s && s !== subEvent.context) 
                                a.push(subEvent);
                        }
                    }
                    
                    if (!a.length)
                        delete this._events[event];
                }
            }
            return this;
        },
        
        trigger: function(eventName) {
            if (!this._events) 
				return this;
			
            var xtraArgs = [].slice.call(arguments, 1);
			
            if (!isEventMap(this, "trigger", eventName, xtraArgs)) 
				return this;
				
            var event = this._events[eventName];
            var allEvents = this._events.all;
			
            if (event) 
				executeCallbacks(event, xtraArgs);
			
			if (allEvents) 
				executeCallbacks(allEvents, arguments);
			
			return this;
        },
        
        stopListening: function(target, callback, context) {
            var listeners = this._listeningTo;
            
            if (!listeners)     // if not listening to anything, job done! 
                return this;
                
            var stateless = !callback && !context;
            if (!context && typeof callback == "object") 
                context = this; 
            
            if (target) 
                (listeners = {})[target._listenId] = target;
                
            for (var listener in listeners) {
                target = listeners[listener]; 
                target.off(callback, context, this);
                 
                if (stateless || _.isEmpty(target._events)) 
                    delete this._listeningTo[listener];
            }
            
            return this;
        },
        
        hasListener: function(event) {
            var listeners = this._events && this._events[event];
            return _.isArray(listeners) && listeners.length > 0;
        }
    };
	
    var listenFns = {
        listenTo: "on",
        listenToOnce: "once"
    };
    
	
    _.each(listenFns, function(fn, idx) {
        eventMixin[idx] = function(target, events, handler) {
            var listeningTo = this._listeningTo || (this._listeningTo = {});
            
            var listenId = target._listenId || (target._listenId = _.uniqueId("l"));
            
            listeningTo[listenId] = target;
            if(!(handler && typeof events))
                handler = this;
            target[fn](events, handler, this);
            return this;
        }
    });
	
	eventMixin.bind = eventMixin.on;
	eventMixin.unbind = eventMixin.off;
	
	return eventMixin;
});