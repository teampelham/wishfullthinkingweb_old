define([
    "underscore", 
    "base/adapters/dimensionsAdapter", 
    "base/adapters/settingsAdapter", 
    //"base/models/library/section/SectionModel", 
    "base/utils/parser", 
    "base/utils/ipParser"
], function(_, dimensionsAdapter, settingsAdapter, parser, ipParser) {
    "use strict";

    function r(e) {
        console.log('ar');
        /*if (e.section) return e.section;
        if (e.get("isSection")) return e;
        var t = e.get("librarySectionID");
        if (t) {
            var i = new n({
                key: e.get("librarySectionID")
            }, {
                server: e.server,
                onlyIfCached: !0
            });
            return i
        }
        return e.parent ? r(e.parent) : void 0*/
    }
    
    var serverMixin = {
        idAttribute: "machineIdentifier",
        
        defaults: function() {
            return {
                friendlyName: "Unknown",
                cloud: false,
                bundled: false,
                relayed: false,
                home: false,
                shared: false,
                synced: false,
                isCloudSync: false,
                protocolCapabilities: [],
                sources: []
            };
        },
        
        whitelistedAttrs: ["machineIdentifier", "name", "friendlyName", "home", "sourceTitle", "accessToken", "superToken", "version", "createdAt", "updatedAt", "protocol", "protocolCapabilities", "platform", "platformVersion", "startState", "serverClass", "synced", "sync", "multiuser", "allowMediaDeletion", "transcoderActiveVideoSessions", "transcoderAudio", "transcoderVideo", "transcoderVideoRemuxOnly", "flashInstalled", "silverlightInstalled", "soundflowerInstalled", "myPlex", "myPlexMappingState", "myPlexSigninState", "myPlexSubscription", "myPlexUsername", "product", "presence", "publicAddressMatches"],
        features: {
            updater: "0.0.1"
            /*splitApart: "0.9.7.0",
            merge: "0.9.7.0",
            search: "0.9.7.0",
            advancedFilters: "0.9.7.0",
            cleanBundles: "0.9.7.0",
            timelineState: "0.9.7.2",
            firstRunPref: "0.9.7.3",
            artworkUpload: "0.9.7.9",
            sync: "0.9.7.10",
            universalTranscoder: "0.9.7.17",
            universalMusicTranscoder: "0.9.8.3",
            syncNotifications: "0.9.7.23",
            sharedSettings: "0.9.7.23",
            specificMediaDeletion: "0.9.7.23",
            dash: "0.9.8.6",
            sharedSync: "0.9.9.3",
            chunkedHTTP: "0.9.9.3",
            streamingMKV: "0.9.9.16",
            hubFilters: "0.9.9.2",
            sectionSettings: "0.9.9.6",
            playQueues: "0.9.9.6",
            cancelAllUpdates: "0.9.9.10",
            hubFilterSigns: "0.9.9.12",
            imageFilters: "0.9.9.13",
            playlists: "0.9.9.13",
            playItemsBatchAdd: "0.9.9.16",
            updater: "0.9.11.1",
            labels: "0.9.11.2",
            publicNowPlaying: "0.9.11.3",
            photoPlayQueues: "0.9.11.12",
            reachability: "0.9.11.14",
            sharedSyncStatus: "0.9.11.14",
            softSubtitles: "0.9.12.0",
            transientToken: "0.9.12.0",
            premiumMusic: "0.9.12.0",
            batchEdit: "0.9.12.0",
            compositeDelete: "0.9.12.0",
            audioAsVideo: "0.9.12.0",
            editSimilarArtists: "0.9.12.2",
            companionProxy: "0.9.12.5",
            shortenTranscodeURL: "0.9.12.5"*/
        },
        
        cacheKey: function(serverScope) {
            return serverScope.machineIdentifier;
        },
        
        url: function(connection) {      // e
            if (!connection) 
                connection = this.get("activeConnection");
                
            var url = connection.uri;      
            if (!url) {
                var scheme = connection.scheme || "http";    // base url was not provided by the connection, need to contruct it
                var address = connection.address;                              // n
                var port = connection.port;                         // s
                url = scheme + "://" + address; 
                if(port)
                    url += ":" + port;
            }
            
            if (!/\/$/.test(url))
                url += "/";         // make sure the url ends with a slash
            return url;
        },
        
        useHostedHttps: "https:" === window.location.protocol,
        
        convert: function(serverObj) {            
            var server = _.isObject(serverObj) ? serverObj : parser.parseXML(serverObj);
            parser.parseInteger(server, ["createdAt", "updatedAt", "transcoderActiveVideoSessions"]);
            parser.parseBoolean(server, ["home", "synced", "sync", "multiuser", "allowMediaDeletion", "flashInstalled", "myWF", "myWFSubscription"]);
            
            if (server && "undefined" == typeof server.startState) 
                server.startState = null;
            
            return server;
        },
        
        parse: function(serverObj) {
            var server = this.convert(serverObj);
            
            if (!server || !server.machineIdentifier) 
                return null;
                
            var scheme = server.scheme;
            var address = server.address;
            var port = parseInt(server.port, 10);
            var localAddresses = server.localAddresses;
            server = _.pick(server, this.whitelistedAttrs);
            
            if (!this.get("shared") || server.sourceTitle || server.home) {
                if (server.sourceTitle && !server.home)
                    server.friendlyName = server.sourceTitle;
                else if (!server.friendlyName) 
                    server.friendlyName = server.name;
            }
            else 
                delete server.friendlyName;
            
            if (server.sourceTitle) 
                server.shared = true; 
            
            server.fullVersion = server.version;
            var version = /(\d+\.){2,3}\d+/.exec(server.version);
            
            if (version) 
                server.version = version[0];
            
            if (server.protocolCapabilities) 
                server.protocolCapabilities = server.protocolCapabilities.split(",");
            
            if ("secondary" === server.serverClass) 
                server.synced = true;
            
            if ("cloudsync" === server.platform) 
                server.isCloudSync = true;
                
            if (server.accessToken) 
                this.set("accessToken", server.accessToken);
            
            if (address && port) 
                this.addConnection({
                    scheme: scheme,
                    address: address,
                    port: port,
                    accessToken: server.accessToken
                }); 
            
            if (localAddresses) {
                localAddresses = localAddresses.split(",");
            
                _.each(localAddresses, function(address) {
                    this.addConnection({
                        address: address,
                        port: 32400,
                        accessToken: server.accessToken
                    });
                }, this);
            } 
            
            return server;
        },
        
        // attempts to turn a relative url into an absolute
        resolveUrl: function(relativePath, connectionObj) { 
            if (!connectionObj)
                connectionObj = {};
                
            if (/^\w+:\/\//.test(relativePath))  // test for ://, return if this is already an absolute
                return relativePath;

            var accessToken = this.get("accessToken");        // i
            var url = connectionObj.useLoopback ? "http://127.0.0.1:8655/" : this.url(connectionObj.connection);       // determine the host path
            if (relativePath)
                url += ("/" === relativePath[0]) ? relativePath.substr(1) : relativePath;       // append the relative path to the host
            
            if(connectionObj.appendToken && accessToken) {      // If this connection requires the Token, append it to the url
                url += url.indexOf("?") > 0 ? "&" : "?"; 
                url += "X-WF-Token=" + accessToken;
            }
            return url;
        },
        
        getXHROptions: function(url) {    // e
            var options = {};
            options.url = this.resolveUrl(url);
            options.dataType = "text";
            options.headers = this.getHeaders();
            return options;
        },
        
        getHeaders: function() {
            var dimensions = dimensionsAdapter.getInstance();    // dimensions contains all the headers
            var dimClone = _.clone(dimensions);                  // keep it intact by cloning a new copy
            var parent = this.parent;                
            
            if (parent) {
                var user = parent.get("user");           
                if (user && user.get("username"))
                    dimClone["X-WF-Username"] = user.get("username");
            }
            
            var accessToken = this.get("accessToken");        // copy over the access token, if there is one yet
            if(accessToken) 
                dimClone["X-WF-Token"] = accessToken;
            dimClone["Accept-Language"] = settingsAdapter.get("language");
            return dimClone;
        },
        
        isLocalAddress: function(url) {
            return this.isLoopbackAddress(url) || ipParser.isPrivateAddress(url);
        },
        
        isLoopbackAddress: function(url) {
            return "localhost" === url || ipParser.isLoopbackAddress(url);
        },
        
        /*isPlexDirectAddress: function(e) {
            console.log('isPlexDirectAddress');
            //return /plex\.direct$/.test(e)
        },*/
        
        isIpAddress: function(e) {
            console.log('isIpAddress');
            //return a.isValidAddress(e)
        },
        
        hasLocalConnection: function() {
            console.log('hasLocalConnection');
            //var e = this.get("activeConnection");
            //return e ? e.isLocalAddress : null
        },
        
        hasLoopbackConnection: function() {
            console.log('hasLoopbackConnection');
            //var e = this.get("activeConnection");
            //return e ? e.isLoopbackAddress : null
        },
        
        isSecure: function(protocol) {
            return "https" === protocol;
        },
        
        getSectionForItem: r,
        
        supports: function(feature) {
            var featureVersion = this.features[feature];
            return featureVersion ? this.isVersionOrNewer(featureVersion) : false;
        },
        
        isVersionOrNewer: function(serverVersion) {
            var version = this.get("version");
            
            if (serverVersion && version) {
                serverVersion = serverVersion.split("."); 
                version = version.split(".");
                
                for (var idx = 0, serverCount = serverVersion.length; serverCount > idx; idx++) {
                    if (idx < version.length) {
                        var serverIncrement = parseInt(serverVersion[idx], 10);
                        var increment = parseInt(version[idx], 10);
                        
                        if (increment > serverIncrement) 
                            return true;
                            
                        if (serverIncrement > increment) 
                            return false;
                    }
                }
                
                return true;
            }
            
            return true;
        },
        /*
        getIpAddressFromPlexDirectAddress: function(e) {
            console.log('getIpAddressFromPlexDirectAddress');
            if (this.isPlexDirectAddress(e)) {
                var t = e.indexOf(".");
                t > 0 && (e = e.substring(0, t), e = e.replace(/\-/g, "."))
            }
            return e
        },*/
        
        getAddressFromUri: function(e) {
            console.log('getAddressFromUri');
            //var t = document.createElement("a");
            //return t.href = e, t.hostname
        }
    };
    
    return serverMixin;
});