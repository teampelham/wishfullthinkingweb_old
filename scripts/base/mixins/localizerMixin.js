define([
	"underscore", 
	"jquery", 
	"base/utils/formatters/stringUtil", 
	"base/utils/dispatcher"
], function(_, $, stringUtil, dispatcher) {
    "use strict";
	
    var languageContainer = {}; 
    var localizerMixin = {
        languages: {
            en: "en.js",              // TODO: Upload via ftp and change this back to json
            "en-GB": "en-GB.json",
            af: "af.json",
            my: "my.json",
            ca: "ca.json",
            cs: "cs.json",
            sr: "sr.json",
            da: "da.json",
            de: "de.json",
            et: "et.json",
            es: "es.json",
            "es-419": "es-419.json",
            el: "el.json",
            fr: "fr.json",
            "fr-CA": "fr-CA.json",
            hr: "hr.json",
            is: "is.json",
            it: "it.json",
            lt: "lt.json",
            hu: "hu.json",
            nl: "nl.json",
            no: "no.json",
            pl: "pl.json",
            pt: "pt.json",
            "pt-BR": "pt-BR.json",
            ru: "ru.json",
            ro: "ro.json",
            sk: "sk.json",
            sl: "sl.json",
            fi: "fi.json",
            sv: "sv.json",
            vi: "vi.json",
            tr: "tr.json",
            uk: "uk.json",
            bg: "bg.json",
            zh: "zh.json",
            "zh-TW": "zh-TW.json",
            ja: "ja.json",
            ko: "ko.json",
            th: "th.json",
            fa: "fa.json",
            ar: "ar.json",
            he: "he.json"
        },
            
        loadingPromise: null,
        loadingLanguage: null,
        
        getLanguages: function() {
            return this.languages;
        },
        
        getTranslation: function(translation) {
            return this.translations[this.language] ? this.translations[this.language][translation] : void 0;
        },
        
        load: function(language) {
            if (this.loadingLanguage && this.loadingLanguage === language) 
                return this.loadingPromise;
            var languagePromise;
            var languages = this.getLanguages();
            
            if (_.has(languages, language)) {
                if (_.has(this.translations, language)) {
                    languagePromise = $.Deferred().resolve().promise();
                }
                else {
                     languagePromise = $.ajax({
                        dataType: "json",
                        url: this.basePath + languages[language]
                    });
                }
            }
            else {
                languagePromise = $.Deferred().reject().promise();
            }
            
            this.loadingLanguage = language;
            this.loadingPromise = languagePromise;
            languagePromise.done(_.bind(this.onLoadSuccess, this, languagePromise));
            languagePromise.fail(_.bind(this.onLoadError, this, languagePromise));
            
            return languagePromise;
        },
            
        t: function(text) {
            if (!text) 
                return "";
            
            var language;
            var arg;
            var r = 1;
            var argLength = arguments.length;
            
            if (_.isObject(arguments[argLength - 1])) {
                arg = arguments[argLength - 1];
                argLength -= 1;
            }
            
            var translation = this.getTranslation(text) || text;
            if (arg && arg.escape)
                translation = _.escape(translation);
                
            for (; argLength > r;) {
                language = languageContainer[r];
                if (!language) {
                    language = new RegExp(stringUtil.escapeRegExp("{" + r + "}"), "g");
                    languageContainer[r] = language;
                }
                translation = translation.replace(language, arguments[r]);
                r++;
            }
            return translation;
        },
        
        onLoadSuccess: function(promise, translation) {
            var loadingLanguage = this.loadingLanguage;
            
            if (promise === this.loadingPromise) {
                this.loadingPromise = null; 
                this.loadingLanguage = null;
                this.language = loadingLanguage;
                this.translations[loadingLanguage] = translation;
                dispatcher.trigger("change:language", loadingLanguage);
            }
        },
        
        onLoadError: function(promise) {
            if (promise === this.loadingPromise) {
                this.loadingPromise = null;
                this.loadingLanguage = null;
            }
        }
    };
    return localizerMixin;
});