define([
    "underscore", 
    "base/mixins/details/baseDetailsMixin"
], function(_, baseDetailsMixin) {
    "use strict";
    var kinectDetailsMixin = {
        bindings: _.pick(baseDetailsMixin.defaultBindings, 
            [".item-title", ".item-year", ".item-duration", ".item-genre", ".item-content-rating", ".details-unwatched-count-badge", ".content-rating-flag-container", ".studio-flag-container"]);
    };
    
    return kinectDetailsMixin;
});