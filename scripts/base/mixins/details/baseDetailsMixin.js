define([
    "underscore", 
    "base/utils/mediaUtil", 
    "base/utils/formatters/dateTimeUtil", 
    "base/utils/formatters/tagUtil"
], function(e, t, i, n) {
    "use strict";
    var s = {
        tagsRenderer: n.truncateLinkedTagList,
        tagsRendererOptions: {},
        defaultBindings: {
            ".item-title": {
                modelAttribute: "title"
            },
            ".item-year": {
                modelAttribute: "year"
            },
            ".item-progress": {
                modelAttribute: "viewOffset",
                manual: !0,
                formatter: function(e, t) {
                    var i = this.model.get("duration"),
                        n = !(!e || !i);
                    if (n) {
                        var s = Math.round(e / i * 100);
                        t.css("width", s + "%")
                    }
                    t.parent().toggleClass("hidden", !n)
                }
            },
            ".item-duration": {
                modelAttribute: "duration",
                autohide: !0,
                formatter: function(e) {
                    return e ? i.formatHoursMinutes(e) : 0
                }
            },
            ".item-unwatched-label": {
                modelAttribute: "viewCount",
                modelListeners: ["viewCount", "viewOffset"],
                manual: !0,
                autohide: function(e) {
                    if (this.shared && !this.multiuser) return !1;
                    var t = this.model.get("viewOffset");
                    return !e && !t
                }
            },
            ".item-unavailable-btn": {
                modelAttribute: "Media",
                manual: !0,
                autohide: function(e) {
                    var i = t.getMissingParts(e);
                    return !!i.length
                }
            },
            ".item-available-at": {
                modelAttribute: "originallyAvailableAt",
                autohide: !0,
                formatter: function(e) {
                    return e ? i.formatDate(e) : ""
                }
            },
            ".item-tagline": {
                modelAttribute: "tagline",
                autohide: !0
            },
            ".item-genre": {
                modelAttribute: "Genre",
                autohide: !0,
                isHtml: !0,
                formatter: function(t) {
                    if (!t || !t.length) return "";
                    var i = this.tagsRenderer(e.extend({}, this.tagsRendererOptions, {
                        model: this.model,
                        key: "genre",
                        tags: t,
                        maxLen: 50,
                        maxTags: 2
                    }));
                    return i
                }
            },
            ".item-director": {
                modelAttribute: "Director",
                manual: !0,
                autohide: !0,
                formatter: function(t, i) {
                    if (!t || !t.length) return "";
                    var n = this.tagsRenderer(e.extend({}, this.tagsRendererOptions, {
                        model: this.model,
                        key: "director",
                        tags: t,
                        maxLen: 50,
                        maxTags: 3
                    }));
                    return i.find(".metadata-tag-list").html(n), n
                }
            },
            ".item-writer": {
                modelAttribute: "Writer",
                manual: !0,
                autohide: !0,
                formatter: function(t, i) {
                    if (!t || !t.length) return "";
                    var n = this.tagsRenderer(e.extend({}, this.tagsRendererOptions, {
                        model: this.model,
                        key: "writer",
                        tags: t,
                        maxLen: 50,
                        maxTags: 3
                    }));
                    return i.find(".metadata-tag-list").html(n), n
                }
            },
            ".item-cast": {
                modelAttribute: "Role",
                manual: !0,
                autohide: !0,
                formatter: function(t, i) {
                    if (!t || !t.length) return "";
                    var n = this.tagsRenderer(e.extend({}, this.tagsRendererOptions, {
                        model: this.model,
                        key: "actor",
                        tags: t,
                        maxLen: 50,
                        maxTags: 5
                    }));
                    return i.find(".metadata-tag-list").html(n), n
                }
            },
            ".item-summary": {
                modelAttribute: "summary",
                autohide: !0
            },
            ".item-content-rating": {
                modelAttribute: "contentRating",
                autohide: !0
            },
            ".media-count-badge": {
                modelAttribute: "Media",
                formatter: function(e) {
                    return e.length
                },
                autohide: function(e) {
                    return e > 1
                }
            },
            ".details-unwatched-count-badge": {
                modelAttribute: "unviewedLeafCount",
                autohide: function(e) {
                    return this.shared && !this.multiuser ? !1 : !! e
                }
            },
            ".content-rating-flag-container": {
                modelAttribute: "contentRating",
                manual: !0,
                autohide: !0
            },
            ".studio-flag-container": {
                modelAttribute: "studio",
                manual: !0,
                autohide: !0
            }
        }
    };
    return s
});