define([
    "underscore", 
    "base/mixins/details/baseDetailsMixin"
], function(e, t) {
    "use strict";
    var i = {
        bindings: e.pick(t.defaultBindings, [".item-title", ".item-year", ".item-duration", ".item-genre", ".item-content-rating", ".details-unwatched-count-badge", ".content-rating-flag-container", ".studio-flag-container"])
    };
    return i
});