define(["underscore"], function(_) {
    "use strict";
    var bindingsMixin = {
		
        bindToModel: function() {
            if (!this.bindings)
                return;
            _.each(_.keys(this.bindings), function(bindingKey) {
                var binding = this.bindings[bindingKey];
                var bindingModel = binding.model ? this[binding.model] : this[this.bindingsModel] || this.model
                
                if (bindingModel) {
                    var listeners = binding.modelListeners || [binding.modelAttribute];
                    
                    _.each(listeners, function(listener) {
                        var eventListener = {
                            obj: bindingModel,
                            name: "change:" + listener,
                            callback: _.bind(this.renderBinding, this, bindingKey, binding, bindingModel)
                        };
                        
                        if (!this._bindingListeners) 
                            this._bindingListeners = [];
                        
                        this._bindingListeners.push(eventListener);
                        this.listenTo(eventListener.obj, eventListener.name, eventListener.callback);
                    }, this);
                    
                    this.renderBinding(bindingKey, binding, bindingModel);
                }
            }, this);
        },

        unbindFromModel: function() {
			if (!this.bindings)
                return;
                
            _.each(this._bindingListeners, function(listener) {
                this.stopListening(listener.obj, listener.name, listener.callback);
            }, this);
            
            this._bindingListeners = null;
        },
		
        renderBinding: function(bindingKey, binding, bindingModel) {
            var $key = this.$(bindingKey);
            var formatter = binding.formatter;
            var isHtml = !! binding.isHtml;
            var autoHide = binding.autohide;
            var attr = bindingModel.get(binding.modelAttribute);
            
            if (formatter) 
                attr = formatter.call(this, attr, $key);
                
            if (autoHide) {
                var isAutoHide = _.isFunction(autoHide) ? autoHide.call(this, attr) : attr;
                $key.toggleClass("hidden", !isAutoHide);
            }
            
            if (!binding.manual) {
                if (!isHtml && _.isString(attr)) 
                    attr = _.escape(attr); 
                
                $key.html(attr);
            }
        }
    };
	
    return bindingsMixin;
});