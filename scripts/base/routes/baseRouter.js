define([
	"underscore", 
	"backbone", 
	"marionette", 
	"base/utils/dispatcher", 
	"base/models/appModel", 
	"base/adapters/analyticsAdapter"
], function(_, bb, marionette, dispatcher, appModel, analyticsAdapter) {
    "use strict";
	
    var baseRouter = marionette.AppRouter.extend({    
        initialize: function() {
            this.uniqueRoutes = _.uniq(_.values(this.appRoutes)); 
            this.fragments = _.invert(this.appRoutes);
            this.listenTo(this, "route", this.onRoute);
            this.listenTo(dispatcher, "navigate", this.onNavigate);
        },
        
        resolveUrlFragment: function(url, arg2) {
            console.log('baseRouter - resolveUrlFragment');
            /*var regMatches = url.match(/:\w+/g);
            
            if (regMatches)
                _.each(regMatches, function(match) {
                    var first = match.slice(1);
                    var subArg = arg2[first];
                    
                    if (null === subArg) {
                        var regex = new RegExp("\\([^\\)]*" + e + "[^\\)]*\\)", "g");
                        url = url.replace(regex, "");
                    } 
                    else 
                        url = url.replace(match, encodeURIComponent(subArg));
                });
            return url.replace(/\(|\)/g, "");*/
        },
        
        onRoute: function(route) {
			// base router captures the route, compares it with the history stack, and logs it accordingly
            var history = appModel.get("history");
            var forward = appModel.get("forwardHistory");
            var histFragment = bb.history.getFragment();    // fragment... current route?
            
            if (histFragment === forward[0])
                history.push(forward.shift());
                
            else if (histFragment !== _.last(history)) {
                history.push(histFragment);
                forward.splice(0);
            }
            
            analyticsAdapter.trackView(route); 
            appModel.trigger("change:history", appModel, history);
        },
        
        onNavigate: function(route, resource) {
			if ( _.indexOf(this.uniqueRoutes, route) === -1) 
                return;
            
            if (!resource) 
                resource = {};
                
            var isSilent = false;
            
            if(null !== resource.silent) {
                isSilent = resource.silent; 
                delete resource.silent;
            }
            
            if (null === resource.trigger)
                resource.trigger = true;
            
            if (isSilent) 
                this.controller[route].apply(this.controller, _.values(resource));
            else {
                console.warn('loud navigation occurred');
                var url = this.resolveUrlFragment(this.fragments[route], resource);
                this.navigate(url, {
                    trigger: resource.trigger,
                    replace: resource.replace
                });
            }
        }
    });
    
    return baseRouter;
});