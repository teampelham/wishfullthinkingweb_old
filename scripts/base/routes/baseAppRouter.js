define([
	"underscore", 
	"backbone", 
	"base/routes/baseRouter", 
	"base/utils/dispatcher",
    "base/models/appModel"
], function(_, bb, baseRouter, dispatcher, appModel) {
    "use strict";

    var baseAppRouter = baseRouter.extend({
        
        initialize: function() {
            baseRouter.prototype.initialize.apply(this, arguments); 
            this.listenTo(dispatcher, {
                "navigate:back": this.onNavigateBack,
                "navigate:forward": this.onNavigateForward,
                "navigate:refresh": this.onNavigateRefresh
            });
        },
        
        onNavigateBack: function() {
            var history = appModel.get("history");
            var forward = appModel.get("forwardHistory");
            
            if (history.length < 2)
                return this.navigate("", { trigger: true });
                
            forward.unshift(history.pop());
            return void this.navigate(_.last(history), { trigger: true });
        },
        
        onNavigateForward: function() {
            console.log('baseAppRouter - onNavigateForward');
            //var forward = appModel.get("forwardHistory");
            
            //if (forward.length) 
            //    this.navigate(forward[0], { trigger: true });
        },
        
        onNavigateRefresh: function() {
            bb.history.loadUrl(bb.history.getFragment());
        }
    });
	
    return baseAppRouter;
});