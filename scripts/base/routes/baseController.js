define([
	"underscore", 
	"jquery", 
	"marionette", 
	"base/utils/dispatcher", 
	"base/utils/xhrTracker", 
	"base/models/appModel",
    "base/models/library/metadataModel", 
    //"base/models/channel/channelModel", 
    "base/models/library/hubCollection"
], function(_, $, marionette, dispatcher, xhrTracker, appModel, MetadataModel, hubCollection) {
    "use strict";
	
    var promise;
    var authorizing = false;     // c
    var baseController = marionette.Controller.extend({
            
        populateView: function(options) {   // e
            if (!options)
                options = {};
                
            dispatcher.trigger("show:loading");     // show the spinner while loading
            
            if (promise && promise.state() === "pending")
                authorizing = true;
            
            xhrTracker.abortAll("page");            // cancel all other pending queries
            
            var args = { options: options };
            var serverPromise = this.populateServer(options.serverID, args);    // empty fetch promise
            
            // figure out what type of server to fill the promise with
            if (options.primaryServer) 
                serverPromise = this.populatePrimaryServer(serverPromise, args);
            
            if (options.sectionID)
                serverPromise = this.populateSection(options, serverPromise, args);
            
            if (options.key)
                serverPromise = this.populateMetadata(options, serverPromise, args);
            
            if (options.channelID)
                serverPromise = this.populateChannel(options, serverPromise, args);
            
            if (options.channelKey)
                serverPromise = this.populateChannelMetadata(options, serverPromise, args);
            
            serverPromise = serverPromise.then(function() {     // resolve function
                return $.Deferred().resolve(args);              // pass back a resolved promise
            }, function() {                                     // reject function
                if (authorizing)    
                    authorizing = false;                        // authorization has failed
                else
                    dispatcher.trigger("navigate", "unauthorized", { result: args, silent: true });
                $.Deferred().reject(args);
            });
            
            promise = serverPromise;        // capture the latest promise in a private variable
            return serverPromise;
        },
        
        populateServer: function(serverID, args) { // e, i
            var servers = appModel.get("servers");
            if (!serverID)
                return servers.populateAnyServer();     // don't care what server, just get me a server!
            
            var promise;  
            if (/^transient/.test(serverID)) {
                var transientServer = appModel.get("transientServer");      // the heck is a transient server?
                
                if (!transientServer || transientServer.id !== serverID) {
                    dispatcher.trigger("navigate:refresh:dashboard");
                    return $.Deferred().reject().promise();                 // we don't have a transient server, rejected!
                }
                promise = $.Deferred().resolve(transientServer).promise();  // send back the trans
            }
            else
                promise = servers.populateServer(serverID);
                
            promise.always(function(result) {
               args.server = result; 
            });
            
            return promise;
        },
        
        populatePrimaryServer: function(e, i) {
            console.log('basecontroller - populatePrimaryServer');
            /*return e = e.then(function() {
                function e(t, s) {
                    appModel.off("change:primaryServer", e), s ? (i.server = s, n.resolve()) : n.reject()
                }
                var n = $.Deferred(),
                    s = appModel.get("primaryServer");
                return s ? (i.server = s, n.resolve().promise()) : s === !1 ? n.reject().promise() : (appModel.on("change:primaryServer", e), n.promise())
            })*/
        },
        
        populateSection: function(sectionInit, serverPromise, options) {
            serverPromise = serverPromise.then(function() {
                return options.server.get("sections").populate()
            }); 
            
            serverPromise = serverPromise.then(function(result) {
                var item = options.item;
                var sectionID = sectionInit.sectionID;
                
                if(item) 
                    sectionID = item.get("librarySectionID");
                    
                if (!sectionID || options.server.get("synced")) 
                    return $.Deferred().resolve();
                
                var section = result.get(sectionID);
                
                if (!section) 
                    return $.Deferred().reject();
                
                options.section = section;
                if (item) {
                    item.section = section;
                    var sectionAssignment = function(items) {
                        if (items) { 
                            items.section = section; 
                            items.each(function(item) {
                                item.section = section;
                            });
                        }
                    };
                    sectionAssignment(item.get("children")); 
                    sectionAssignment(item.get("extras"))
                }
                return $.Deferred().resolve(section);
            });
            
            return serverPromise;
        },
        
        populateMetadata: function(options, promise, args) {
            promise = promise.then(function() {
                var model = new MetadataModel({
                    key: options.sectionID + "/details/" + options.key
                }, {
                    server: args.server
                });
                args.item = model; 
                
                return model.fetch({
                    scope: "page",
                    data: _.extend({
                        checkFiles: "1",
                        includeExtras: "1",
                        includeRelated: "1",
                        includeRelatedCount: "0",
                        includeOnDeck: "1"
                    }, options.populateData)
                });
            }); 
            promise = this.populateSection(options, promise, args);
            return promise;
        },
        
        populateChannel: function(e, t, i) {
            console.log('basecontroller - populateChannel');
            /*return t = t.then(function() {
                var t = e.channelID,
                    n = {};
                /^\/\w+\//.test(t) ? n.key = t : n.identifier = t;
                var s = new o(n, {
                    server: i.server
                });
                return i.channel = s, s.fetch({
                    scope: "page"
                });
            });*/
        },
        
        populateChannelMetadata: function(e, t, i) {
            console.log('basecontroller - populateChannelMetadata');
            /*return t = t.then(function() {
                var t = null;/*new o({
                    key: e.channelKey
                }, {
                    server: i.server,
                    channel: i.channel
                });
                return i.channelItem = t, t.fetch({
                    scope: "page"
                })
            });*/
        }
    });
		
    return baseController;
});