define([
	"underscore", 
	"base/routes/baseController", 
	"base/utils/dispatcher", 
	"base/models/appModel"
], function(_, baseController, dispatcher, appModel) {
    "use strict";
    
    var baseAppController = baseController.extend({
        
        initialize: function() {
            _.bindAll(this, "viewFor");
        },
        
        viewFor: function(viewType, viewArgs) {
            var View = this.viewsByType[viewType];
            
            if (View) 
                return new View(viewArgs);
                
            throw new Error("No view defined for " + viewType);
        },
        
        error: function() {
			console.warn('aaaaaah');
            dispatcher.trigger("show:view", this.viewFor("notFound", {
                type: "404"
            }));
        },
        
        unauthorized: function(args) {
            // no servers available with autologin, time to handle it
            var handled = _.some(this.getUnauthorizedHandlers(), function(handler) {
                return handler.call(this, args);
            }, this);
            
            if (!handled) {
                dispatcher.trigger("show:view", this.viewFor("notFound", {
                    type: "device"
                }));
            }
        },
        
        getUnauthorizedHandlers: function() {
            return [this.handleAuthorized, this.handleServer, this.handleChannelError];
        },
        
        login: function() {
            var viewFor = this.viewFor;
            var servers = appModel.get("servers");
            var cloudServer = servers.findWhere({
                cloud: true
            });
            
            dispatcher.trigger("show:loading");
            
            cloudServer.populate().done(function() {
                dispatcher.trigger("navigate", "dashboard");
            }).fail(function() {
                var view = cloudServer.get("available") ? viewFor("signIn") : viewFor("unavailable", {
                    model: cloudServer
                });
                dispatcher.trigger("show:view", view);
            });
        },
        
        handleAuthorized: function() {
            var noServers = !appModel.get("servers").findWhere({
                authorized: true
            });
            
            if (noServers)  // are there reeeeeally no authorized servers??
                this.populateServersThenRecover();      // ok, go get some
                
            return noServers;
        },
        
        handleServer: function(serverMgr) {
            var promise;
            var view;
            var viewFor = this.viewFor;
            var options = serverMgr.options;
            var server = serverMgr.server;
            var cloud = appModel.get("servers").findWhere({
                cloud: true
            });
            
            if (!(options.primaryServer && options.serverID)) {
                return false;
            }
            
            if (!server && options.primaryServer)
                view = viewFor("serverRequired");
            else if (server) {
                if (server.get("available")) {
                    if (server.get("authorized") || "resolved" !== cloud.populatePromise.state()) {
                        if (!server.get("authorized")) {
                            promise = this.populateServersThenRecover();
                        }
                        else {
                            view = viewFor("unauthorized")
                        }
                    }
                    else {
                        view = viewFor("unavailable", { model: server });
                    }
                }
                else {
                    view = viewFor("notFound", { type: "server" });
                }
            }
            
            if (view)
                dispatcher.trigger("show:view", view);
                
            return !(!view && !promise);
        },
        
        handleChannelError: function(e) {
            var hasChannel = !! e.options.channelID;

            if (hasChannel)
                dispatcher.trigger("show:view", this.viewFor("channelError"));
            return hasChannel;
        },
        
        populateServersThenRecover: function() {
			var servers = appModel.get("servers")
            var cloud = servers.findWhere({
                cloud: true
            });
            var viewFor = this.viewFor;
            
            return servers.populate().done(function() {
                cloud.get("authorized") ? dispatcher.trigger("navigate:refresh") : dispatcher.trigger("navigate", "login", {
                    silent: true
                });
            }).fail(function() {
                dispatcher.trigger("show:view", viewFor("unavailable", {
                    model: cloud
                }));
            });
        }
    });
    
    return baseAppController;
});