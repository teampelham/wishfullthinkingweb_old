define([
    "underscore", 
    "jquery", 
    "base/utils/t", 
    "base/utils/dispatcher", 
    "base/utils/parser", 
    "base/adapters/storageAdapter", 
    "base/models/appModel"
], function(_, $, t, dispatcher, parser, storageAdapter, appModel) {
    "use strict";

    function signUpCommand(userInfo) {
        
        var user = appModel.get("user");       
        var loginCredentials = _.pick(userInfo, "username", "email", "password");
        
        if (userInfo.birthday) 
            loginCredentials.birthday_str = userInfo.birthday.format("YYYY/MM/DD");
            
        
        var xhrOptions = user.server.getXHROptions("/users.php?format=xml");
        xhrOptions.type = "POST";
        xhrOptions.data = loginCredentials;
        
        var xhr = $.ajax(xhrOptions);
        
        xhr.done(function(result) {
            var userParse = user.parse(result);     // feed the result through the model
            var accessToken = userParse.accessToken;    // extract the accessToken
            
            user.server.set("accessToken", accessToken);    // set the token for logging in
            user.set(userParse);
            user.resolveServerPromise();                    // execute all the stored callbacks
            storageAdapter.set("myWFAccessToken", accessToken, {
                session: !userInfo.remember
            });
        }); 
        
        xhr.fail(function(response) {
            var message;
            if (422 === response.status) {
                var errMsgXml = response.responseText;
                var error = parser.parseXML(errMsgXml);
                
                parser.parseArray(error, "errors");
                message = t(error.errors.length ? _.first(error.errors).error : "This account information is invalid.");
            } 
            else 
                message = t("There was a problem connecting to WishFull Thinking.");
            
            dispatcher.trigger("show:alert", {
                type: "error",
                message: message
            });
        }); 
        
        return xhr.promise();
    }
    
    return signUpCommand;
});
