define([
    "underscore", 
    "jquery", 
    "base/utils/dispatcher", 
    "base/utils/xhrTracker", 
    "base/adapters/loggerAdapter", 
    "base/models/config", 
    "base/models/appModel"
], function(_, $, dispatcher, xhrTracker, loggerAdapter, config, appModel) {
    "use strict";

    function signOutCommand(arg) {
        arg = arg || {};
        var user = appModel.get("user");
        var clearTokenPromise = user.clearAccessToken();
        
        clearTokenPromise.fail(function() {
            loggerAdapter.warn("Failed to clear session and server storage");
        });
        clearTokenPromise.always(function() {
            var servers = appModel.get("servers");
            var userServer = user.server;
            
            if (config.get("hosted") && !arg.skipHostedRedirect) {
                window.location = "users/sign_out.html";
                return $.Deferred().reject().promise();
            }
            xhrTracker.abortAll();
            user.stopListening();
            user.clear();
            user.set(_.result(user, "defaults"));
            user.initialize();
            userServer.resetPopulatePromise($.Deferred);
            userServer.connections = [];
            userServer.clear();
            userServer.set(_.result(userServer, "defaults"));
            servers.reset([userServer]);
            dispatcher.trigger("clearAll:cache");
            userServer.cache.store(userServer);
            appModel.set({
                authorized: false,
                primaryServer: null,
                history: []
            });
            dispatcher.trigger("request:disconnect:serverEvents");
            return void dispatcher.trigger("initialize:servers");
        });
        
        return clearTokenPromise;
        
    }
    
    return signOutCommand;
});