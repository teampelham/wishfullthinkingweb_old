define([
	"jquery", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/utils/formatters/stringUtil", 
	"base/models/appModel"
], function($, t, dispatcher, stringUtil, appModel) {
    "use strict";

    function signInCommand(params) {
        var xhrOptions;		// r
		var user = appModel.get("user");		// o
        if (params.username && params.password) {
            xhrOptions = user.server.getXHROptions("/signIn.php");
			xhrOptions.type = "POST";
            
			var encodedCredentials = "Basic " + stringUtil.encode64(stringUtil.toUtf8(params.username + ":" + params.password)); // l
            xhrOptions.headers.BasicAuth = encodedCredentials; 
        } 
		else {
			xhrOptions = user.server.getXHROptions("/users/account.xml?auth_token=" + params.accessToken); 
			xhrOptions.type = "GET";
		}
		
        var xhr = $.ajax(xhrOptions);
        xhr.then(function(result) {
            return user.switchUser(result, {
                remember: params.remember
            });
        }); 
		
		xhr.fail(function(result) {
            var message;
            if (!params.silent) {
				message = t(401 === result.status && params.username && params.password ? "The username or password is incorrect." : "There was a problem connecting to WishFull Thinking.");
				dispatcher.trigger("show:alert", {
                	type: "error",
                	message: message
            	});
			}
        });
		
		return xhr.promise();
    }
	
    return signInCommand;
});