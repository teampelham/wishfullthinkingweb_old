define([
    "underscore", 
    //"base/utils/t", 
    "base/utils/dispatcher", 
    "base/utils/sendAutomatedState", 
    //"base/models/appModel", 
    //"desktop/views/modals/ConfirmModal"
], function(_, dispatcher, sendAutomatedState) {
    "use strict";

    function onOffCommand(commandObj) {
        
        // Takes an automatedDeviceModel and a boolean onOff
        // The model should track its serverModel
        var listItem = commandObj.item;
        var autoDeviceModel = listItem.model;
        var isOnState = autoDeviceModel.get("OnLevel") == 0;
        var model = _.clone(autoDeviceModel);
        
        listItem.setTransitionaryState();
        
        // make a util that sends commands to primary server
        var statePromise = sendAutomatedState(model, isOnState);
        
        statePromise.done(function(e) {
            autoDeviceModel.setNewOnLevel(e);
        });
        
        statePromise.fail(function(e) {
            alert("WHACK");     // TODO: No alerts
        });
    }
    
    return onOffCommand;
});