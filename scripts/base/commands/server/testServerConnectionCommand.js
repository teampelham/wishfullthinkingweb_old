define([
	"underscore", 
	"jquery", 
	"base/adapters/loggerAdapter"
], function(_, $, loggerAdapter) {
    "use strict";

    function testServerConnectionCommand(serverConnection) {   // n
        var server = serverConnection.server;
        var connection = serverConnection.connection;
		var friendlyName = server.get("friendlyName");
        var url = server.url(connection) + "web/ping";
        var deferred = $.Deferred();
        
        if (connection.test && connection.test.state() === "pending") {
            connection.aborting = true;
            connection.test.abort();
            connection.aborting = false;
        }
        
        loggerAdapter.log("Testing connection to " + friendlyName + " at " + url);
        
        var xhrOptions = _.extend(server.getXHROptions(url), {
            timeout: server.timeout
        });
        
        var xhr = $.ajax(xhrOptions);
        server.text = xhr;
        
        xhr.always(function() {
            server.text = null;
        });
        
        xhr.done(function(event) {
            var result = server.parse(event);
            var hasIdentifier = result && result.machineIdentifier;
            
            if (!hasIdentifier || server.id && result.machineIdentifier !== server.id) {
                loggerAdapter.warn(friendlyName + " is unavailable at " + url + " (Invalid Response)");
                deferred.reject(server, connection, serverConnection);
            }
            else {
                loggerAdapter.success(friendlyName + " is available at " + url);
                deferred.resolve(server, connection, result, url);
            }
        });
        
        xhr.fail(function(response) { 
            loggerAdapter.warn(friendlyName + " is unavailable at " + url + " (Status " + response.status + ")");
            deferred.reject(server, connection, response);
        });
        
        return deferred;
    }
	
    return testServerConnectionCommand;
});