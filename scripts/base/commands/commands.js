define([
	"underscore", 
	"base/utils/dispatcher", 
	"base/commands/user/signUpCommand",
    "base/commands/user/signInCommand", 
	"base/commands/server/testServerConnectionCommand", 
	"base/commands/server/testServerImageCommand",
    "base/commands/server/relayServerCommand", 
    "base/commands/user/signOutCommand",
    "base/commands/user/switchUserCommand", 
	"base/commands/user/shareCommand", 
	"base/commands/security/getTransientTokenCommand",
    "base/commands/automatedItem/onOffCommand"
], function(_, dispatcher, signUpCommand, signInCommand, testServerConnectionCommand, testServerImageCommand, relayServerCommand, signOutCommand, switchUserCommand, shareCommand, getTransientTokenCommand, onOffCommand) {
    "use strict";
    var commands = {
       map: {
			signUp: signUpCommand,
            signIn: signInCommand,
            testServerConnection: testServerConnectionCommand,
            testServerImage: testServerImageCommand,
            relayServer: relayServerCommand,
            signOut: signOutCommand,
            switchUser: switchUserCommand,
            share: shareCommand,
            getTransientToken: getTransientTokenCommand,
            onOff: onOffCommand
        },
		
        execute: function(command) {
            var commandMap = commands.map;
            if (commandMap.hasOwnProperty(command)) 
				return commandMap[command].apply(commands, _.rest(arguments));
				
            throw new Error("Bad command " + command);
        }
    };
	
    dispatcher.on("command", function(commandName, arg, callback) {
        var result = commands.execute.apply(this, arguments);
        if (_.isFunction(callback))
			callback(result);
    });
	
	return commands;
});