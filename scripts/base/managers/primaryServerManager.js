define([
	"underscore", 
	"base/utils/dispatcher", 
	"base/utils/baseClass", 
	"base/mixins/eventMixin", 
	"base/models/appModel", 
	"base/models/config", 
	"base/adapters/analyticsAdapter", 
	"base/adapters/loggerAdapter", 
	"base/adapters/metricsAdapter"
], function(_, dispatcher, baseClass, eventMixin, appModel, config, analyticsAdapter, loggerAdapter, metricsAdapter) {
    "use strict";
    
    var primaryServerManager = baseClass.extend({
        mixins: eventMixin,
        waitingForServers: false,
        
        initialize: function(initObj) {
            _.bindAll(this, "checkPendingServers");
            
            if (!initObj)
                initObj = {};
                
            this.servers = initObj.servers;
            this.lastPrimaryServerID = initObj.lastPrimaryServerID,
            this.startListening();
        },
        
        startListening: function() {
            this.listenTo(this.servers, "add", this.checkPendingServers);
            this.listenTo(this.servers, "remove", this.onServerRemove);
            this.listenTo(this.servers, "change:connected", this.onServerConnectedChange);
            this.listenTo(appModel, "change:primaryServer", this.onPrimaryServerChange);
        },
        
        checkPendingServers: function() {
            if (this.servers.populatePromise) {
                if ("pending" === this.servers.populatePromise.state()) {
                    if (this.waitingForServers) 
                        return;
                    this.waitingForServers = true; 
                    
                    loggerAdapter.log("Primary server is waiting for servers to populate");
                    if (appModel.get("primaryServer") === false)
                        appModel.set("primaryServer", null);
                    
                    return void this.servers.populate().always(this.checkPendingServers);
                }
                
                if (!appModel.get("primaryServer")) {
                    this.waitingForServers = false;
                    var disconnectedServers = this.servers.filter(function(server) {
                        return null === server.get("connected");
                    });
                    
                    var disconnectedPrimaries = _.filter(disconnectedServers, function(server) {
                        return !server.get("shared") && !server.get("synced");
                    });
                    
                    if (this.primaryCandidate) {
                        var lastPrimaryServer = this.servers.get(this.lastPrimaryServerID);
                        var candidateShared = this.primaryCandidate.get("shared") && !! disconnectedPrimaries.length;
                        var isPending = false;
                        
                        if (this.lastPrimaryServerID && lastPrimaryServer) {
                            isPending = "pending" === lastPrimaryServer.populatePromise.state();
                            if (!disconnectedServers.length || !isPending && !candidateShared) 
                                return void this.setPrimaryServer(this.primaryCandidate);
                        }
                        
                        if (!disconnectedServers.length) {
                            this.setPrimaryServer(false);
							console.warn("enabled the analytics");
                            //analyticsAdapter.enable();
                            //metricsAdapter.enable();
                        }
                    }
                }
            }
        },
        
        findBestPrimaryServer: function() {
            console.log('findBestPrimaryServer');
            /*var t = this.servers.filter(function(e) {
                return e.canBePrimary(a.get("minServerVersion"))
            });
            if (!t.length) return !1;
            var i = e.findWhere(t, {
                id: this.lastPrimaryServerID
            });
            if (this.lastPrimaryServerID && i) return i;
            var n = e.sortBy(t, function(e) {
                return e.get("shared")
            });
            return e.first(n)*/
        },
        
        setPrimaryServer: function(server) {
            appModel.set("primaryServer", server);
        },
        
        fetchRelatedCollections: function(server) {
            var shared = server.get("shared");
            if (!shared || server.supports("sharedSettings")) {
                var settings = server.get("settings");
                this.listenTo(settings, "reset", this.onPrimaryServerSettingsReset); 
                settings.populate({
                    reset: true
                });
            }
        },
        
        onServerRemove: function(server) {
            var primaryServer = appModel.get("primaryServer");
            
            if (primaryServer && primaryServer.id === server.id && !server.get("isMerged")) 
                this.setPrimaryServer(this.findBestPrimaryServer());
            else if (primaryServer == null)
                this.checkPendingServers();
        },
        
        onServerConnectedChange: function(serverModel) {
            if (!appModel.get("primaryServer")) {
                if (!serverModel.canBePrimary(config.get("minServerVersion"))) 
                    return void this.checkPendingServers();

                var isLastServer = serverModel.id === this.lastPrimaryServerID;
                var isCandidate = !this.lastPrimaryServerID && !serverModel.get("shared");
                
                if (isLastServer || isCandidate) {
                    loggerAdapter.log("Setting primary server immediately to " + serverModel.get("friendlyName"));
                    return void this.setPrimaryServer(serverModel);
                }
                
                if (!this.primaryCandidate || this.primaryCandidate.get("shared") && !serverModel.get("shared")) { 
                    loggerAdapter.log("Setting primary server candidate to " + serverModel.get("friendlyName"));
                    this.primaryCandidate = serverModel;
                }
                
                this.checkPendingServers();
            }
        },
        
        onPrimaryServerChange: function(appModel, server) {
            this.stopListening(); 
            this.startListening(); 
            if (null == server) 
                this.checkPendingServers();
            else if (server) 
                loggerAdapter.success("Setting primary server to " + server.get("friendlyName")); 
            else
                loggerAdapter.warn("No primary server"); 
            
            if(server) {
                this.primaryCandidate = null; 
                this.listenTo(server, "change:connected", this.onPrimaryServerConnectedChange); 
                this.listenTo(server, "destroy", this.onPrimaryServerDestroy); 
                this.fetchRelatedCollections(server);
            }
        },
        
        onPrimaryServerSettingsReset: function(setting) {
            var collect = setting.get("collectUsageData");
            if (collect && collect.get("value") === true) {
                metricsAdapter.enable(); 
                analyticsAdapter.enable(); 
            } 
            else {
                analyticsAdapter.disable(); 
                metricsAdapter.disable(); 
            }
        },
        
        onPrimaryServerConnectedChange: function(e, t) {
            console.log('onPrimaryServerConnectedChange');
            //t || (l.warn("The primary server lost its connection"), this.setPrimaryServer(this.findBestPrimaryServer()))
        }
    });
    
    return primaryServerManager;
});