define([
	"underscore", 
	"jquery", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/utils/baseClass", 
	"base/utils/parser", 
	"base/mixins/eventMixin", 
	"base/models/config", 
	"base/adapters/loggerAdapter", 
	"base/adapters/settingsAdapter",
    "base/utils/workers/pessimisticServerRetryWorker", 
    "base/utils/workers/optimisticServerRetryWorker", 
    "base/utils/constants/httpFallbackOptions"
], function(_, $, t, dispatcher, baseClass, parser, eventMixin, config, loggerAdapter, settingsAdapter, PessimisticServerRetryWorker, OptimisticServerRetryWorker, httpFallbackOptions) {
    "use strict";
    
    var serverConnectionManager = baseClass.extend({
        mixins: eventMixin,
        browserConnectionChangeDelay: 2e3,
        
        initialize: function(initializer) {
            _.bindAll(this, "onBrowserConnectionChange", "onConnectionTestSuccess", "onConnectionTestError");
            this.servers = initializer.servers;
            
            this.listenTo(this.servers, {
                add: this.onServerAdd,
                remove: this.onServerRemove,
                reset: this.onServersReset,
                "error:connection": this.onServerConnectionError,
                "change:connections": this.onServerConnectionsChange,
                "change:accessToken": this.onServerAccessTokenChange,
                "change:connected": this.onServerConnectedChange
            });
            
            this.listenTo(dispatcher, {
                "test:servers": this.testAllServers,
                "test:server": this.testAllConnections
            });
            
            $(window).on("online", this.onBrowserConnectionChange);
            $(window).on("offline", this.onBrowserConnectionChange);
            
            this.pessimisticServerRetryWorker = new PessimisticServerRetryWorker(this);
            this.optimisticServerRetryWorker = new OptimisticServerRetryWorker(this);
            this.testAllPendingConnections();
        },
        
        testAllServers: function() {
            this.servers.each(this.testAllConnections, this);
        },
        
        testAllConnections: function(serverManager) {
            this.resetServerAvailability(serverManager);
            
            _.each(serverManager.connections, function(connection) {
                connection.available = null; 
                this.testConnection(serverManager, connection);
            }, this);
            
            return serverManager.populatePromise;
        },
        
        testAllPendingConnections: function() {
            this.servers.each(this.testPendingConnections, this);
        },
        
        testPendingConnections: function(serverMgr) {       // t
            // filter the list of connections to only the pending ones
            var pendingConnections = _.where(serverMgr.connections, {        // i
                pending: true
            });
            
            _.each(pendingConnections, function(connection) {       
                // test each connection
                this.testConnection(serverMgr, connection);
            }, this);
        },
        
        testConnection: function(serverMgr, connection) {     // t, i
            connection.pending = false;
            
            if (_.isFunction(serverMgr.testConnection)) // If the server model has its own test method, use that
                return void serverMgr.testConnection(connection);
            
            var serverConnection = {               
                server: serverMgr,
                connection: connection
            };
            
            if (!config.get("allowMixedContent") && serverMgr.useHostedHttps && "http" === connection.scheme) {
                // violates security settings
                loggerAdapter.warn("Connection test aborted for " + serverMgr.get("friendlyName") + " due to mixed content prevention");
                return void this.onConnectionTestError(serverMgr, connection);
            }
            
            // trigger whatever listener wants to test the connection
            return void dispatcher.trigger("command", "testServerConnection", serverConnection, _.bind(function(ajaxCall) {     // e
                ajaxCall.done(this.onConnectionTestSuccess);
                ajaxCall.fail(this.onConnectionTestError);
            }, this));
        },
        
        findBestConnection: function(server) {
            var availableConnections = _.where(server.connections, {
                available: true
            });
            
            var bestConnection = _.reduce(availableConnections, function(connection, memo) {
                if (connection) {
                    if (!connection.isSecure && memo.isSecure)
                        return memo;
                    if (!connection.isLocalAddress && memo.isLocalAddress)
                        return memo;
                    if (!connection.isLoopbackAddress && memo.isLoopbackAddress)
                        return memo;
                    return connection;
                }
                return memo;
            });
            
            return bestConnection;
        },
        
        resolveServer: function(server, device) {    // t, i
            device = _.extend({}, device, {
                connected: true,
                available: true,
                authorized: true
            }); 
            server.populatePromise.resolveWith(server, [server, device]); 
            server.set(device); 
            this.removeUnavailableServer(server);
        },
        
        rejectServer: function(server, i, options) {   // t, i, n
            var friendlyName = server.get("friendlyName");
            var serverOptions = _.extend({
                connected: false,
                available: false,
                authorized: false
            }, options);
            
            if (serverOptions.available) {
                loggerAdapter.warn("Authorization for " + friendlyName + " failed");
                this.removeUnavailableServer(server);
            }
            else {
                loggerAdapter.warn("All connections for " + friendlyName + " failed");
                this.addUnavailableServer(server);
            }
            server.populatePromise.rejectWith(server, [server, i]);
            server.set(serverOptions);
        },
        
        resetServerAvailability: function(serverManager) {
            serverManager.resetPopulatePromise($.Deferred()); 
            serverManager.set({
                available: null,
                authorized: null
            });
        },
        
        addUnavailableServer: function(server) {
            var update = server.get("update");
            if (update && update.get("state") === "installing")
                this.optimisticServerRetryWorker.add(server);
            else
                this.pessimisticServerRetryWorker.add(server);
        },
        
        removeUnavailableServer: function(server) {
            this.pessimisticServerRetryWorker.remove(server); 
            this.optimisticServerRetryWorker.remove(server);
        },
        
        abortConnectionTests: function(server) {
            var connections = server.connections;
            _.each(connections, function(connection) {
                if (connection.test && "pending" === connection.test.state()) { 
                    connection.aborting = true; 
                    connection.test.abort(); 
                    connection.aborting = false;
                }
            });
        },
        
        onServerAdd: function(server) {
            if (server.connections.length)
                this.testPendingConnections(server);
        },
        
        onServerRemove: function(server) {
            this.abortConnectionTests(server); 
            this.removeUnavailableServer(server);
        },
        
        onServersReset: function(t, i) {
            console.log('onServersReset');
            /*var n = i.previousModels;
            e.each(n, function(e) {
                t.contains(e) || (this.abortConnectionTests(e), this.removeUnavailableServer(e))
            }, this)*/
        },
        
        onServerConnectionError: function(serverMgr) {
            this.testAllConnections(serverMgr);
        },
        
        onServerConnectionsChange: function(serverMgr) {
            // A new connection was triggered, test it
            this.testPendingConnections(serverMgr);
        },
        
        onServerAccessTokenChange: function(serverManager, token) {
            if (!serverManager.get("cloud") && token)       // test the connections to find out if the token is valid
                this.testAllConnections(serverManager);
        },
        
        onServerConnectedChange: function(serverModel, connected) {
            if (null !== connected && null !== serverModel.previous("connected") && !serverModel.get("cloud")) {
                var statusMsg = connected ? t("Found {1}", serverModel.get("friendlyName")) : t("{1} went away", serverModel.get("friendlyName"));
                dispatcher.trigger("show:status", serverModel, statusMsg);
            }
        },
        
        onBrowserConnectionChange: function() {
            loggerAdapter.info("Online status: " + window.navigator.onLine);
            clearTimeout(this.browserConnectionChangeTimeout);
            this.browserConnectionChangeTimeout = setTimeout(_.bind(this.testAllServers, this), this.browserConnectionChangeDelay);
        },
        
        onConnectionTestSuccess: function(server, connection, device) { // e, t, i
            connection.available = true;
            var bestConnection = this.findBestConnection(server);
            loggerAdapter.success("Setting active connection for " + server.get("friendlyName") + " (" + server.url(bestConnection) + ")"); 
            server.set({
                activeConnection: bestConnection,
                relayed: !! bestConnection.relay
            }); 
            this.resolveServer(server, device);
        },
        
        onConnectionTestError: function(connection, jqXHR, errorThrown) {      // i, n, s
            if (!jqXHR.aborting) {
                jqXHR.available = false;
                var allowFallback = settingsAdapter.get("allowHttpFallback")                         // r
                var fallback = allowFallback === httpFallbackOptions.ALWAYS || 
                    connection.get("publicAddressMatches") && 
                    allowFallback === httpFallbackOptions.SAME_NETWORK;    // l
                var allowMix = !connection.useHostedHttps || config.get("allowMixedContent");        // c
                var offline = !(connection.get("cloud") || connection.get("synced"));                       // p
                
                if ("https" === jqXHR.scheme && allowMix && fallback && offline) {
                    var newConnection = _.pick(jqXHR, ["accessToken", "address", "port"]);  // m
                    newConnection.scheme = "http";
                    if (connection.addConnection(newConnection).pending) 
                        return;
                }
                
                if (!connection.get("available") && "pending" === connection.populatePromise.state()) {
                    var failedConnections = _.where(connection.connections, {        // f
                        available: false
                    });
                    var isFailedStatus = errorThrown && (401 === errorThrown.status || 403 === errorThrown.status || 406 === errorThrown.status);    // g
                    var isTotalFailure = connection.connections.length === failedConnections.length;      // v
                    if (isFailedStatus || isTotalFailure) {
                        if (connection.id || !isFailedStatus) 
                            return void this.rejectServer(connection, errorThrown, {
                                available: isFailedStatus
                            });
                        var identityUrl = connection.resolveUrl("/identity", {        // y
                            connection: connection
                        });
                        var xhrOptions = _.extend(connection.getXHROptions(identityUrl), {                      // b
                                timeout: 5e3
                            });
                        var xhr = $.ajax(xhrOptions);
                        
                        loggerAdapter.log("Requesting server identity at " + identityUrl); 
                        xhr = xhr.then(function(result) {
                            var identity = parser.parseXML(result);
                            var deferred = $.Deferred();
                            
                            if (identity && identity.machineIdentifier) 
                                deferred.resolve(identity.machineIdentifier); 
                            else
                                deferred.reject();
                                
                            return deferred.promise();
                        }); 
                        
                        xhr.done(function(machineIdentifier) {
                            loggerAdapter.log("Server identity at " + identityUrl + " is " + machineIdentifier); 
                            connection.set("machineIdentifier", machineIdentifier);
                        }); 
                        
                        xhr.fail(function() {
                            loggerAdapter.warn("Server identity is unavailable at " + identityUrl);
                        }); 
                        
                        xhr.always(_.bind(function() {
                            this.rejectServer(connection, errorThrown, {
                                available: true
                            })
                        }, this))
                    }
                }
            }
        }
    });
    
    return serverConnectionManager;
});