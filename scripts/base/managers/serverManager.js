define([
	"underscore", 
	"jquery", 
	"base/utils/baseClass", 
	"base/utils/dispatcher", 
	"base/adapters/loggerAdapter", 
	"base/mixins/eventMixin", 
	"base/models/appModel", 
	"base/models/server/serverStorageModel", 
	"base/models/server/serverModel"
], function(_, $, baseClass, dispatcher, loggerAdapter, eventMixin, appModel, serverStorageModel, ServerModel) {
    "use strict";

    function getServerNames(servers) {
        var serverNames = _.map(servers, function(server) {
            return server.get("friendlyName")
        });
        return serverNames.join(", ");
    }
    
    var serverManager = baseClass.extend({
        mixins: eventMixin,
        
        initialize: function(initializer) {
            this.servers = initializer.servers;
            this.savePrimaryOnly = initializer.savePrimaryOnly;
            this.pendingResources = [];
            
            this.listenTo(this.servers, {
                add: this.onServerAdd,
                remove: this.onServerRemove,
                "change:authorized": this.onServerAuthorizedChange,
                "change:sources": this.onServerSourcesChange
            });
            
            var cloudServers = this.servers.where({
                cloud: true
            });
            
            _.each(cloudServers, function(server) {
                this.listenTo(server, {
                    "change:resources:add": this.onResourceAdd,
                    "change:resources:remove": this.onResourceRemove,
                    "change:resources:reset": this.onResourcesReset
                });
            }, this);
            
            this.listenTo(appModel, {
                "change:primaryServer": this.onPrimaryServerChange
            });
        },
        
        resolveServersPromise: function() {
			if (this.pendingResources.length) {
                loggerAdapter.log("Waiting to resolve servers promise, pending resources"); 
                return void this.servers.resetPopulatePromise($.Deferred());
            }
            
            var pendingBundleClouds = this.servers.filter(function(server) {
                return (server.get("bundled") || server.get("cloud")) && server.hasPendingPopulate();
            });
            
            if (pendingBundleClouds.length) {
                loggerAdapter.log("Waiting to resolve servers promise, pending initial servers = " + getServerNames(pendingBundleClouds));
                return void this.servers.resetPopulatePromise($.Deferred());
            }
            
            var availableServers = this.servers.where({
                available: true
            });
            
            if (availableServers.length) {
                loggerAdapter.log("Resolving servers promise, available servers = " + getServerNames(availableServers)); 
                this.servers.updatePopulatePromise("resolved");
            }
            else {
                var pendingServers = this.servers.filter(function(server) {
                    return "pending" === server.populatePromise.state();
                });
                
                if (pendingServers.length) { 
                    loggerAdapter.log("Waiting to resolve servers promise, pending servers = " + getServerNames(pendingServers)); 
                    return void this.servers.resetPopulatePromise($.Deferred());
                }
                
                loggerAdapter.warn("Rejecting servers promise, no available servers"); 
                this.servers.updatePopulatePromise("rejected");
            }
        },
        
        fetchResources: function(server) {      // t
            var resources = server.get("resources");
            if (_.contains(this.pendingResources, resources.populatePromise))
                return void this.resolveServersPromise();
            
            loggerAdapter.log("Finding servers through " + server.get("friendlyName"));
            
            var resourcePromise = resources.fetch();
            this.pendingResources.push(resourcePromise);
            
            resourcePromise.done(_.bind(function() {
                loggerAdapter.log("Found " + resources.length + " resources through " + server.get("friendlyName"));
                serverStorageModel.pruneLiteServers(this.servers, resources);
                this.pruneUnavailableServers();
            }, this));
            
            resourcePromise.always(_.bind(function() {
                this.pendingResources.splice(_.indexOf(this.pendingResources, resourcePromise), 1);
                this.resolveServersPromise();
            }, this));
            
            return resourcePromise;
        },
        
        abortResources: function(server) {
            var resources = server.get("resources");
            resources.abortPendingPopulate(); 
            this.resolveServersPromise();
        },
        
        pruneUnavailableServers: function() {
            this.servers.set(this.servers.reject(function(server) {
                return server.get("authorized") === false && !server.id;
            }));
        },
        
        saveOnlyServer: function(server) {
            var removePromise;
            if (this.savePrimaryOnly) {
                removePromise = serverStorageModel.removeAllServers(); 
                if (server) 
                    removePromise.then(_.bind(serverStorageModel.updateConnections, serverStorageModel, server, server.connections));
            }
            return removePromise;
        },
        
        onServerAdd: function(server) {
            if (server.get("cloud")) 
                this.listenTo(server, {
                    "change:resources:add": this.onResourceAdd,
                    "change:resources:remove": this.onResourceRemove,
                    "change:resources:reset": this.onResourcesReset
                });
        },
        
        onServerRemove: function(server) {
            loggerAdapter.log("Removed server " + server.get("friendlyName")); 
            this.stopListening(server); 
            server.cache.remove(server); 
            dispatcher.trigger("clearWhere:cache", {
                server: server
            }), 
            this.resolveServersPromise();
        },
        
        onServerAuthorizedChange: function(server, authorized) { // e, t
			var refresh;
            var authServers = this.servers.where({
                authorized: true,
                cloud: false
            });
            
            appModel.set("multiserver", authServers.length > 1);
            
            if (authorized) 
                appModel.set("authorized", true);
                
            else if (authorized === false) {
                
                var allUnauth = this.servers.every(function(server) {
                    return server.get("authorized") === false;
                });
                
                if (allUnauth) 
                    appModel.set("authorized", false);
                    
                if (appModel.previous("authorized")) 
                    refresh = true;
            }
            
            if (server.get("cloud"))
                if (authorized) 
                    this.fetchResources(server); 
                else
                    this.abortResources(server);
            else
                this.resolveServersPromise(); 
            
            if (refresh) 
                dispatcher.trigger("navigate:refresh");
        },
        
        onServerSourcesChange: function(serverMgr, sources) { // e, t
            if (sources.length) 
                return void serverStorageModel.updateLiteServer(serverMgr);
            
            serverMgr.set("authorized", false);
            serverMgr.trigger("destroy", serverMgr, serverMgr.collection);
            serverMgr.cache.remove(serverMgr);
            
            return void serverStorageModel.removeLiteServer(serverMgr);
        },
        
        onPrimaryServerChange: function(e, server) {
            this.saveOnlyServer(server);
        },
        
        onResourceAdd: function(device) {
            if (!_.contains(device.get("provides"), "server")) 
                return;
            
            loggerAdapter.log("Found additional server " + device.get("name"));
            
            var serverModel = new ServerModel({
                machineIdentifier: device.id,
                name: device.get("name"),
                sourceTitle: device.get("sourceTitle"),
                accessToken: device.get("accessToken"),
                version: device.get("productVersion"),
                owned: device.get("owned"),
                synced: device.get("synced"),
                updatedAt: device.get("lastSeenAt"),
                presence: device.get("presence"),
                publicAddressMatches: device.get("publicAddressMatches")
            }, {
                parse: true,
                server: device.server,
                parent: device.server.parent
            });
            serverModel.addSource(device.server.id);
             
            this.servers.add(serverModel);
            var connections = [];
            
            if (serverModel.get("bundled")) {
                var bundles = _.findWhere(serverModel.connections, {
                    bundled: true
                });
                if (bundles) 
                    connections.push(bundles);
            }
            _.each(device.get("Connection"), function(connection) {
                connections.push(serverModel.addConnection({
                    scheme: connection.protocol,
                    address: connection.address,
                    port: connection.port,
                    accessToken: device.get("accessToken"),
                    uri: connection.uri
                }));
            }); 
            
            if (!this.savePrimaryOnly) 
                serverStorageModel.updateConnections(serverModel, connections); 
            
            this.resolveServersPromise();
        },
        
        onResourceRemove: function(t) {
            console.log('');
            /*if (e.contains(t.get("provides"), "server")) {
                var i = this.servers.get(t.id);
                i && i.removeSource(t.server.id)
            }*/
        },
        
        onResourcesReset: function(serverResources) {
            var serverModels = this.servers.models;
            for (var i = serverModels.length - 1; i > -1; i--) {
                var serverModel = serverModels[i];
                
                if (!serverResources.contains(serverModel.id)) 
                    serverModel.removeSource(serverResources.server.id);
            }
        }
    });
    
    return serverManager;
});