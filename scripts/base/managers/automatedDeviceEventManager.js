define([
	"underscore", 
	"base/utils/baseClass", 
    "base/utils/dispatcher",
	"base/utils/workers/serverEventRetryWorker", 
	"base/mixins/eventMixin",
    "base/models/appModel"
], function(_, baseClass, dispatcher, ServerEventRetryWorker, eventMixin, appModel) {
    "use strict";
    
	var automatedDeviceEventManager = baseClass.extend({
        mixins: eventMixin,
        
		initialize: function(eventConnection) {
            this.ServerEventConnection = eventConnection.ServerEventConnection;
			this.retryWorker = new ServerEventRetryWorker(this); 
			this.connections = {};
            this.listenTo(appModel, "change:primaryServer", this.onPrimaryServerChange);
        },
        
		connect: function(connectObj) {
            var server = connectObj.server;
            var connection = this.connections[server.id];
            
            if (!connection) {
                connection = new this.ServerEventConnection({
                    server: server
                });
                this.connections[connection.id] = connection;
                this.listenTo(connection, {
                    open: this.onOpen,
                    close: this.onClose,
                    error: this.onError,
                    message: this.onMessage
                });
            }
            return connection.open();
        },
        
        disconnectAll: function() {
            _.each(this.connections, function(connection) {
                this.disconnect({
                    connection: connection
                });
            }, this);
        },
        
        sendMessage: function(message,sender) {
            _.each(this.connections, function(connection) {
                connection.send(message, sender);
            });
        },
        
        disconnect: function(server) {
            var connection = server.connection;
            
            if (!connection) 
                connection = this.connections[server.server.id];
            
            if (connection) {
                this.stopListening(connection);
                this.retryWorker.remove(connection.server);
                
                connection.close(); 
                delete this.connections[connection.server.id];
            }
        },
        
        onOpen: function(server, e) {
            this.retryWorker.remove(server);
            var websocket = e.target;
            websocket.send(JSON.stringify({ RegisterEventListener: 'Device Action' }));     
        },
		
        onClose: function(e) {
            this.retryWorker.add(e);
        },
        
        onMessage: function(e, data) {
            if(data.action)
                dispatcher.trigger("pubsub:" + data.action.replace(/\s/g, '_'), e, data);  
            if(data.type)
                dispatcher.trigger("pubsub:" + data.type.replace(/\s/g, '_'), e, data);
        },
        
        onPrimaryServerChange: function (e, t) {
            this.connect({
                server: t
            });
        }
    });
	
    return automatedDeviceEventManager;
});