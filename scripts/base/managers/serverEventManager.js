define([
	"underscore", 
	"base/utils/baseClass", 
	"base/utils/workers/serverEventRetryWorker", 
	"base/mixins/eventMixin"
], function(_, baseClass, ServerEventRetryWorker, eventMixin) {
    "use strict";
    
	var serverEventManager = baseClass.extend({
        mixins: eventMixin,
        
		initialize: function(eventConnection) {
            this.ServerEventConnection = eventConnection.ServerEventConnection;
			this.retryWorker = new ServerEventRetryWorker(this); 
			this.connections = {};
        },
        
		connect: function(connectObj) {
            var server = connectObj.server;
            var connection = this.connections[server.id];
            
            if (!connection) {
                connection = new this.ServerEventConnection({
                    server: server
                });
                this.connections[connection.id] = connection;
                this.listenTo(connection, {
                    open: this.onOpen,
                    close: this.onClose,
                    error: this.onError
                });
            }
            return connection.open();
        },
        
        disconnectAll: function() {
            _.each(this.connections, function(connection) {
                this.disconnect({
                    connection: connection
                });
            }, this);
        },
        
        disconnect: function(server) {
            var connection = server.connection;
            
            if (!connection) 
                connection = this.connections[server.server.id];
            
            if (connection) {
                this.stopListening(connection);
                this.retryWorker.remove(connection.server);
                
                connection.close(); 
                delete this.connections[connection.server.id];
            }
        }
        /*
        onOpen: function(e) {
            this.retryWorker.remove(e)
        },
        onClose: function(e) {
            this.retryWorker.add(e)
        }*/
    });
	
    return serverEventManager;
});