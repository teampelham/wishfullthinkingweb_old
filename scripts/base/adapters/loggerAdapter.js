define(["base/adapters/adapter"], function(Adapter) {
    "use strict";
	
    var loggerAdapter = new Adapter({
        "interface": ["log", "success", "info", "warn", "error", "debug", "group"]
    });
	
    return loggerAdapter;
});