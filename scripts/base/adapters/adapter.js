define([
	"underscore", 
	"base/utils/baseClass", 
	"base/mixins/eventMixin"
], function(_, baseClass, eventMixin) {
    "use strict";
	
    var adapter = baseClass.extend({
        mixins: eventMixin,
		
        initialize: function(interfaceDef) {
            if (!interfaceDef)
                interfaceDef = {};
               
            if (interfaceDef["interface"])  // adapters define either interfaces or properties
                this["interface"] = interfaceDef["interface"];
            
            if (interfaceDef.props)
                this.props = interfaceDef.props;
            
            // Assign all the instance methods defined by the adapter's superclass interface to this object
            _.each(this["interface"], function(interfaceFn) {
                // the function name is bound to the _proxy method, which gets called with the method name as an argument
                this[interfaceFn] = _.bind(this._proxy, this, interfaceFn);
            }, this);
        },
		
        _proxy: function(method) {
            var instance = this.getInstance();
            return instance[method].apply(instance, _.rest(arguments));
        },
		
        register: function(instance) {
            this._instance = instance;
			this.trigger("change:instance", instance);   // certain adapters need to update when their instances change 
        },
		
        getInstance: function() {
            if (this.hasInstance()) 
				return this._instance;
            throw new Error("Nothing has been registered to this adapter.");
        },
		
        hasInstance: function() {
            return null != this._instance;
        },
		
        prop: function(propName) {
            if (_.contains(this.props, propName)) 
				return this.getInstance()[propName];
				
            throw new Error("The " + propName + " property has not been defined for this adapter");
        }
    });
	
    return adapter;
});