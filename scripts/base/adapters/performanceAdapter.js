define(["base/adapters/adapter"], function(Adapter) {
    "use strict";
    
	var performanceAdapter = new Adapter({
        props: ["name", "taskThrottle"]
    });
	
    return performanceAdapter;
});