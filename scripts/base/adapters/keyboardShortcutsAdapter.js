define(["base/adapters/adapter"], function(Adapter) {
    "use strict";
	
    var keyboardShortcutsAdapter = new Adapter({
        props: ["shortcuts"]
    });
	
    return keyboardShortcutsAdapter;
});