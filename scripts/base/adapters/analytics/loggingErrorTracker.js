define(["base/adapters/loggerAdapter"], function(loggerAdapter) {
    "use strict";
    var loggingErrorTracker = {
        
		init: function() {
            loggerAdapter.log("Error tracker initialized")
        },
        
		enable: function() {
            loggerAdapter.log("Error tracker enabled");
        },
		
        disable: function() {
            loggerAdapter.log("Error tracker disabled");
        },
		
        set: function(t) {
            loggerAdapter.log("Error tracker set options " + JSON.stringify(t, null, 2));
        },
		
        capture: function(t, i) {
			console.warn('capture');
            loggerAdapter.error(t);
			loggerAdapter.log("Tracked error " + JSON.stringify(i, null, 2));
        }
    };
    return loggingErrorTracker;
});