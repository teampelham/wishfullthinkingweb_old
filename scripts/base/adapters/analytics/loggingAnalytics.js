define([
	"base/utils/baseClass", 
	"base/adapters/loggerAdapter"
], function(baseClass, loggerAdapter) {
    "use strict";
	
    var loggingAnalytics = baseClass.extend({
		
        enable: function() {
            loggerAdapter.log("Analytics enabled")
        },
		
        disable: function() {
            loggerAdapter.log("Analytics disabled")
        },
		
        set: function(e) {
            loggerAdapter.log("Analytics set options " + JSON.stringify(e, null, 2))
        },
		
        trackView: function(e) {
            loggerAdapter.log('Analytics tracked view "' + e + '"')
        },
		
        trackEvent: function(e) {
            loggerAdapter.log("Analytics tracked event " + JSON.stringify(e, null, 2))
        }
    });
    return loggingAnalytics;
});