define([
	"underscore", 
	"jquery"
], function(_, $) {
    "use strict";
    var wfMetrics = {
        defaults: {},
        enabled: false,
        url: "https://metrics.wishfullthinking.net/collect/event",	// TODO: Figure out how/when plex writes and what it writes
        
		enable: function() {
            this.enabled = true;
        },
		
        disable: function() {
            this.enabled = false;
        },
		
        set: function(t) {
			console.warn("metrics!");
            //e.extend(this.defaults, t)
        },
		
        trackEvent: function(category, action, s) {
            if (!this.enabled)
                return;
            
            if (!s)
                s = {};
            else
                console.warn('s');
            
            var defaults = _.defaults({
                category: category,
                action: action
            }, s, this.defaults);
            
            $.ajax({
                type: "POST",
                url: this.url,
                contentType: "application/json",
                data: JSON.stringify(defaults)
            });
        }
    };
	
    return wfMetrics;
});