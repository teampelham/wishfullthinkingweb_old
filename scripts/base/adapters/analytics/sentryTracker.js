define([
	//"raven"
], function() {
    "use strict";
    var t = {};
	var sentryTracker = {
		
        init: function(t, i) {
			console.warn('');
            //e.config(t).install(); 
			this.set("version", i)
        },
		
        enable: function() {
			console.warn('');
            //e.install()
        },
		
        disable: function() {
            //e.uninstall()
        },
		
        set: function(i, n) {
			console.warn('');
            t[i] = n;
			//e.setTagsContext(t)
        },
		
        capture: function(t, i) {
			console.warn('');
            //e.captureException(t, i)
        }
    };
    return sentryTracker;
});