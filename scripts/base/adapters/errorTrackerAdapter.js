define(["base/adapters/adapter"], function(Adapter) {
    "use strict";
	
    var errorTrackerAdapter = new Adapter({
        "interface": ["init", "enable", "disable", "set", "capture"]
    });
	
    return errorTrackerAdapter;
});