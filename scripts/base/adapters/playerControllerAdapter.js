define([
    "underscore", 
    "base/utils/t", 
    "base/utils/dispatcher", 
    "base/adapters/adapter", 
    "base/adapters/loggerAdapter", 
    "base/models/appModel"
], function(e, t, i, n, s, a) {
    "use strict";
    var r = n.extend({
        initialize: function() {
            this.controllers = []
        },
        register: function(e) {
            this.controllers.push(e)
        },
        getInstance: function(n, r) {
            r || (r = {});
            var o, l = a.get("primaryPlayer");
            o = l && !r.forceLocal ? l.get("protocol") || "legacy" : "local";
            var c = e.find(this.controllers, function(e) {
                var t = o === e.protocol,
                    i = n === e.type || "*" === e.type;
                return t && i
            });
            if (c) return c.create(n, r);
            if (r.forceLocal) throw new Error("No player controller found for: " + n);
            return s.warn("A player did not match the protocol (" + o + ") and type (" + n + ")."), i.trigger("show:alert", {
                type: "error",
                message: t("Playback of this media is not supported with the selected player.")
            }), this.getInstance(n, {
                forceLocal: !0
            })
        }
    });
    return new r
});