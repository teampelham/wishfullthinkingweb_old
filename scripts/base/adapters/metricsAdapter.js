define(["base/adapters/adapter"], function(Adapter) {
    "use strict";
    
    var metricsAdapter = new Adapter({
        "interface": ["enable", "disable", "set", "trackEvent"]
    });
    
    return metricsAdapter;
});