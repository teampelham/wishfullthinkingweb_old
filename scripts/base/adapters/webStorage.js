define([
	"jquery", 
	"base/adapters/loggerAdapter"
], function($, loggerAdapter) {
    "use strict";

    function getStorage(useSession) {
        return useSession ? window.sessionStorage : window.localStorage;
    }

    function resolveStorage(callback, context) {
        var storage = getStorage(context.session);
        var deferred = $.Deferred();
        
		try {
            deferred.resolve(callback(storage))
        } 
		catch (err) {
            loggerAdapter.log(err.message); 
			deferred.reject(err);
        }
        return deferred.promise();
    }
	
    try {
        if (typeof localStorage.getItem == "string")
			delete localStorage.getItem;
		if (typeof localStorage.setItem == "string")
			delete localStorage.setItem;
		if (typeof localStorage.removeItem == "string") 
			delete localStorage.removeItem;
		if (typeof localStorage.clear == "string")
			delete localStorage.clear;
    } 
	catch (s) {}
    
	var webStorage = {
		// Get returns a promise!
        get: function(key, t) {
			if (!t)
				t = {};
			
			return resolveStorage(function(storage) {
				var wfObj = window.WFWEB;
				var item = (wfObj && key in wfObj) ? wfObj[key] : storage.getItem(key);
				
				if (item == null) {
					var storeName = t.session ? "session" : "local";
					throw new Error(key + " is not in " + storeName + " storage");
				}
				
				return item;
			}, t);
        },
		
        set: function(key, value, sessionObj) {
			if (!sessionObj)
                sessionObj = {};
            
            return resolveStorage(function(storage) {
                return storage.setItem(key, value);
            }, sessionObj);
        },
        
		remove: function(item, storageType) {
            if (!storageType) 
                storageType = {}; 
            
            return resolveStorage(function(storage) {
                return storage.removeItem(item);
            }, storageType);
        },
        
        clear: function(storageType) {
            if (!storageType) 
                storageType = {};
                 
            return resolveStorage(function(storage) {
                return storage.clear();
            }, storageType);
        }
    };
	
    return webStorage;
});