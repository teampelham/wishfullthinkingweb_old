define(["base/adapters/adapter"], function(Adapter) {
    "use strict";
    
	return new Adapter({
        "interface": ["getLanguages", "load", "t"]
    });
});