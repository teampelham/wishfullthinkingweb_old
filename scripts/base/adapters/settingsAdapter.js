define(["base/adapters/adapter"], function(Adapter) {
    "use strict";
	
    var settingsAdapter = new Adapter({
        "interface": ["get", "save", "populate"]
    });
	
    return settingsAdapter;
});