define(["base/adapters/adapter"], function(Adapter) {
    "use strict";
    
	var dimensionsAdapter = new Adapter({
        props: ["X-WF-Product", "X-WF-Version", "X-WF-Client-Identifier", "X-WF-Platform", "X-WF-Platform-Version", "X-WF-Device", "X-WF-Device-Name"]
    });
	
    return dimensionsAdapter;
});