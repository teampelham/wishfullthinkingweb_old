define(["base/adapters/adapter"], function(adapter) {
    "use strict";
	
    var AnalyticsAdapter = adapter.extend({
        "interface": ["enable", "disable", "set", "trackView", "trackEvent"],
        
        register: function(anal) {
            this._Analytics = anal;
        },
        
        init: function(e, t) {
            this._instance = new this._Analytics(e, t);
            return this._instance;
        }
    });
	
    return new AnalyticsAdapter();
});