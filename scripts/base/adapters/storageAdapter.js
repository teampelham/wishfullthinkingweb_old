define(["base/adapters/adapter"], function(Adapter) {
    "use strict";
    
	var storageAdapter = new Adapter({
        "interface": ["get", "set", "remove", "clear"]
    });
	
    return storageAdapter;
});