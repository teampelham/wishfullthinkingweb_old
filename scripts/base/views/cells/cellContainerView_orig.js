define([
    "underscore", 
    "marionette", 
    "base/utils/throttledTaskQueue", 
    "base/utils/viewPool"
 ], function(e, t, i, n) {
    "use strict";
    var s = t.View.extend({
        initialize: function() {
            t.View.prototype.initialize.apply(this, arguments), this.pool = new n(this)
        },
        render: function() {
            this.isRendered || this.triggerMethod("before:render", this), this.isRendered = !0;
            var e = this.getLockIndexes();
            this.pool.lockOrCreate(e, this.createCellView, this), this.scheduleFilling(e), this.prepareCells(e), this.pool.each(this.moveCell, this), this.moveElement(), this.clearVisibleReusedCells()
        },
        createCellView: function(i) {
            var n = t.getOption(this, "itemView"),
                s = t.getOption(this, "itemViewOptions");
            e.isFunction(s) && (s = s.call(this, i));
            var a = new n(s);
            return a.render(), this.$el.append(a.$el), a
        },
        scheduleFilling: function(t) {
            i.cancel(this.cid), e.each(t, function(e) {
                var t = this.pool.find(e);
                t.filledWithIndex !== t.id && i.enqueue(function() {
                    var e = this.fillCell(t);
                    return e && (t.filledWithIndex = t.id), e
                }, this, this.cid)
            }, this)
        },
        prepareCells: function() {
            this.pool.each(this.prepareCell, this)
        },
        clearVisibleReusedCells: function() {
            var e = this.getVisibleBounds();
            this.pool.each(function(t) {
                var i = t.filledWithIndex === t.id,
                    n = -1 === t.filledWithIndex,
                    s = t.id >= e.from && t.id <= e.to;
                !s || i || n || (this.clearCell(t), t.filledWithIndex = -1)
            }, this)
        },
        _getImmediateChildren: function() {
            return e.pluck(this.pool.pool, "view")
        },
        onClose: function() {
            i.cancel(this.cid), this.pool.close()
        }
    });
    return s
})