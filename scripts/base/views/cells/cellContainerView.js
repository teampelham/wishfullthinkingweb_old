define([
	"underscore", 
	"marionette", 
	"base/utils/throttledTaskQueue", 
	"base/utils/viewPool"
], function(_, marionette, throttledTaskQueue, ViewPool) {
    "use strict";
	
    var cellContainerView = marionette.View.extend({
        
        initialize: function() {
            marionette.View.prototype.initialize.apply(this, arguments); 
            this.pool = new ViewPool(this);
        },
		
        render: function() {
            if (!this.isRendered) {
                this.triggerMethod("before:render", this);
            }
            this.isRendered = true;
            
            var lockIndexes = this.getLockIndexes();
            
            this.pool.lockOrCreate(lockIndexes, this.createCellView, this); 
            this.scheduleFilling(lockIndexes); 
            this.prepareCells(lockIndexes); 
            this.pool.each(this.moveCell, this); 
            this.moveElement(); 
            this.clearVisibleReusedCells();
        },
		
        createCellView: function(i) {
            var ItemView = marionette.getOption(this, "itemView");
            var itemViewOptions = marionette.getOption(this, "itemViewOptions");
            
            if (_.isFunction(itemViewOptions)) 
                itemViewOptions = itemViewOptions.call(this, throttledTaskQueue);
            
            var itemView = new ItemView(itemViewOptions);
            itemView.render(); 
            
            this.$el.append(itemView.$el);
            return itemView;
        },
        
        scheduleFilling: function(fills) {
            throttledTaskQueue.cancel(this.cid); 
            _.each(fills, function(fill) {
                var view = this.pool.find(fill);
                if (view.filledWithIndex !== view.id) { 
                    throttledTaskQueue.enqueue(function() {
                        var cellFilled = this.fillCell(view);
                        if (cellFilled) 
                            view.filledWithIndex = view.id;
                        return cellFilled;
                    
                    }, this, this.cid);
                }
            }, this);
        },
		
        prepareCells: function() {
            this.pool.each(this.prepareCell, this);
        },
        
        clearVisibleReusedCells: function() {
            var visibleBounds = this.getVisibleBounds();
            this.pool.each(function(poolView) {
                var idsMatch = poolView.filledWithIndex === poolView.id;
                var notFilled = -1 === poolView.filledWithIndex;
                var bounded = poolView.id >= visibleBounds.from && poolView.id <= visibleBounds.to;
                
                if (bounded && !idsMatch && !notFilled) {
                    this.clearCell(poolView); 
                    poolView.filledWithIndex = -1;
                }
            }, this);
        },
		
        _getImmediateChildren: function() {
            return _.pluck(this.pool.pool, "view");
        },
		
        onClose: function() {
            throttledTaskQueue.cancel(this.cid); 
			this.pool.close();
        }
    });
	
    return cellContainerView;
});