define(["marionette"], function(hb) {
    "use strict";
    
	var abstractErrorView = hb.Layout.extend({
        regions: {
            content: ".content"
        }
    });
    
	return abstractErrorView;
});