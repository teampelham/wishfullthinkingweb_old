define([
	"base/utils/t", 
	"base/views/error/abstractErrorView"
], function(t, abstractErrorView) {
    "use strict";
	
    var baseNotFoundView = abstractErrorView.extend({
        initialize: function(err) {
            this.type = err.type || "404";
        },
		
        serializeData: function() {
            var title, info;
            if("404" === this.type) {
				title = t("You must be lost."); 
				info = t("This page doesn't exist.");
			}
			else if ("server" === this.type) {
				title = t("Home server not found."); 
				info = t("The server you're trying to access doesn't seem to exist.");
			} 
			else { 
				title = t("Device not found."); 
				info = t("The device you're trying to access doesn't seem to exist.");
			} 
			
			return {
                title: title,
                subtitle: info
            }
        }
    });
	
    return baseNotFoundView;
});