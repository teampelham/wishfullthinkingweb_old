define([
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/views/error/abstractErrorView"
], function(t, dispatcher, abstractErrorView) {
    "use strict";
	
    var baseUnavailableView = abstractErrorView.extend({
        modelEvents: {
            "change:connected": "onConnectedChange"
        },
		
        serializeData: function() {
            var title;
			var subtitle; 
			var model = this.model;
            var friendlyName = model.get("friendlyName");
			
			if (model.get("cloud")) {
				if (model.get("downForMaintenance")) {
					title = t("WishFull Thinking is down for maintenance."); 
					subtitle = t("Don't worry, it'll be back soon.");
				}
				else {
					title = t("WishFull Thinking is not reachable."); 
					subtitle = t("Make sure your server has an internet connection and any firewalls or other programs are set to allow access.");
				}
			}
			else {
				title = t("Server unavailable."); 
				subtitle = friendlyName ? 
					t("We couldn't reach \"{1}\". Make sure it's online and try again.", friendlyName) : 
					t("We couldn't reach this server. Make sure it's online and try again.");
			}
            return {
                title: title,
                subtitle: subtitle
            };
        },
		
        onConnectedChange: function() {
            t.trigger("navigate:refresh")
        }
    });
	
    return baseUnavailableView;
});