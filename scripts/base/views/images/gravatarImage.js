define(["base/views/images/baseImage"], function(baseImage) {
    "use strict";
    
	var gravatarImage = baseImage.extend({
        getUrl: function(baseUrl) {
            var url;
            
			if(baseUrl) {
				url = baseUrl + (baseUrl.indexOf("?") > 0 ? "&" : "?"); 
				url += "size=" + this.width;
			}
			
			return url;
        }
    });
    
	return gravatarImage;
});