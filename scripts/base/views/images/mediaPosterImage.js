define([
	"base/utils/posterSizes", 
	"base/utils/posterPlaceholders", 
	"base/views/images/baseImage"
], function(posterSizes, posterPlaceholders, baseImage) {
    "use strict";
	
    var mediaPosterImage = baseImage.extend({
        background: true,
		
        setup: function(initObj) {
            this.minSize = true; 
			this.type = initObj.type; 
			this.modelAttribute = initObj.modelAttribute || "thumb"; 
			
			if (this.model && this.model.get("isFolder")) 
				this.placeholderClass = posterPlaceholders.folder;
			else if (!initObj.placeholderClass)
				this.placeholderClass = posterPlaceholders[this.type]; 
			
			baseImage.prototype.setup.apply(this, arguments);
        },
		
        setDimensions: function(poster) {
            if (poster.width) 
				poster.height = posterSizes.getPosterHeight(this.type, poster.width);
			else if (poster.height)
				poster.width = posterSizes.getPosterWidth(this.type, poster.height); 
				
			baseImage.prototype.setDimensions.call(this, poster);
        },
		
        setModel: function() {
            baseImage.prototype.setModel.apply(this, arguments); 
			
			if (this.model && this.model.get("isFolder")) 
				this.placeholderClass = posterPlaceholders.folder;
        },
        
        render: function(renderArgs) {
            if (this.src && this.src.startsWith("data")) {
                this.$el[0].style.backgroundSize = this.backgroundSize ? this.backgroundSize : this.width + "px " + this.height + "px";
                this.$el[0].style.height = this.height + "px";
                return void(renderArgs.force && this.showImage(this.src));
            }
            
            return baseImage.prototype.render.apply(this, arguments);
        }
    });
	
    return mediaPosterImage;
});