define([
	"underscore", 
    "jquery", 
    "marionette", 
    "base/utils/capabilities", 
    "base/utils/transcoder", 
    "base/utils/cache/imageCache", 
    "base/models/appModel"
], function(_, $, marionette, capabilities, transcoder, ImageCache, appModel) {
    "use strict";
	
    var defaultImg = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    var baseImage = marionette.ItemView.extend({
        tagName: "img", // required for ItemView
        background: false,
        backgroundSize: "",
        cache: new ImageCache(),
        
        initialize: function(imgProperties) {
			if (!imgProperties)
                imgProperties = {};
            
            _.bindAll(this, "onLoad", "onError", "loadImage");
            this.pixelRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
            // extract as many properties from the init object as were provided
            this.url = imgProperties.url;
            this.server = imgProperties.server;
            this.placeholder = imgProperties.placeholder;
            this.placeholderClass = imgProperties.placeholderClass;
            this.modelAttribute = imgProperties.modelAttribute;
            this.ignoreAttributeChanges = imgProperties.ignoreAttributeChanges;
            this.exposeUrl = imgProperties.exposeUrl;
            this.resetOnError = imgProperties.resetOnError;
            this.emptyImage = imgProperties.explicitEmpty ? "url(" + defaultImg + ")" : "";
            
            if (imgProperties.background)
                this.background = imgProperties.background;
            
            if (imgProperties.backgroundSize)
                this.backgroundSize = imgProperties.backgroundSize;
            
            if (imgProperties.format) 
                this.format = imgProperties.format;
            
            if (_.isFunction(imgProperties.getUrl))
                this.getUrl = imgProperties.getUrl;
                
            this.setup(imgProperties);
        },
        
        setup: function(imgProperties) {
			this.setDimensions(imgProperties); 
            this.setFilters(imgProperties.filters); 
            this.setModel(imgProperties.model, imgProperties);
        },
        
        setDimensions: function(imgProperties) {
			if (imgProperties.width && imgProperties.height) {
                this.transcode = true; 
                this.width = imgProperties.width; 
                this.height = imgProperties.height;
            }
        },
        
        setFilters: function(filters) {
            this.filters = filters || void 0;
        },
        
        hasFilters: function() {
            return void 0 !== this.filters;
        },
        
        setModel: function(model, imgProperties) {
            imgProperties = imgProperties || {};
            
			this.stopListening();
            this.model = model;
            
            if (imgProperties.url) 
                this.url = imgProperties.url;
            else if (this.modelAttribute) {   
                this.url = this.getUrl(model.get(this.modelAttribute));
            }
            
            if (model) { 
                if (!this.server)
                    this.server = model.server;
                
                if (model.get("cameraID")) {     // TODO: Use some other property
                    var dataUri = model.get("thumb");
                    if(dataUri) {
                        this.src = "data:image/gif;base64," + dataUri.substring(1, dataUri.length - 1);
                        this.url = null;
                    }
                }
            }
            
            if (!this.ignoreAttributeChanges && this.modelAttribute)
                this.listenTo(model, "change:" + this.modelAttribute, this.onChange);
        },
        
        // This method is called by the base View Marionette constructor
        setElement: function() {
            marionette.ItemView.prototype.setElement.apply(this, arguments);
            
            this.render({
                force: true
            });
        },
        
        getUrl: function(url) {
            return url;
        },
        
        getTranscodeUrl: function(url) {
            var primaryServer = appModel.get("primaryServer");
            
            if(primaryServer && primaryServer.get("activeConnection"))
                url = primaryServer.get("activeConnection").uri + url;
            
            if (this.server && this.transcode) 
                transcoder.image(this.server, url, {
                    fallbackServer: appModel.get("primaryServer"),
                    width: this.width * this.pixelRatio,
                    height: this.height * this.pixelRatio,
                    minSize: this.minSize,
                    filters: this.filters,
                    format: this.format
                });
                
            return url;
        },
        
        render: function(renderArgs) {
            if (!renderArgs)
                renderArgs = {};
                
            if (this.url || this.placeholder || this.placeholderClass) {
                if (this.src)
                    return void(renderArgs.force && this.showImage(this.src));
                else {
                    if (!(this.isPlaceholderRendered || this.url))
                        this.showPlaceholder();
                    
                    if(this.url)
                        this.load(this.url, this.transcode)
                            .then(this.onLoad, this.onError, this.onLoad);
                }
            }
            return this;
        },
        
        showPlaceholder: function() {
            this.isPlaceholderRendered = true; 
            if (this.placeholderClass) {
                this.$el.addClass(this.placeholderClass);
                return void this.$el.css("background-image", this.emptyImage); 
            }
            if (this.placeholder) 
                return void this.showImage(this.placeholder); 
            
            return void(this.background ? this.$el.css("background-image", this.emptyImage) : this.showImage(defaultImg));
        },
        
        showImage: function(img) {
            if (this.background) 
                return void this.showBackgroundImage(img); 
                
            return void this.$el.attr("src", img).removeAttr("width").removeAttr("height");
        },
        
        showBackgroundImage: function(url) {
            this.$el.css("background-image", "url(" + url + ")");
        },
        
        load: function(img) {
            if (!img) 
                return $.Deferred().reject().promise();
                
            img = this.getTranscodeUrl(img); 
            if (this.exposeUrl) 
                this.$el.attr("data-image-url", img);
                
            if (capabilities.hasBlob && capabilities.hasObjectUrl) {
                var blob = this.loadBlob(img);
                return blob = blob.then(this.loadImage, null, this.loadImage);
            }
            
            return this.loadImage(img);
        },
        
        loadBlob: function(blob) {
            var defer = $.Deferred();
            this.cancel();
            
            var blobUrl = this.cache.getBlobUrl(blob);
            
            if (blobUrl) { 
                defer.resolve(blobUrl); 
                return defer.promise();
            }
             
            var placeHolder = this.cache.getPlaceholder(blob);
            if (placeHolder) 
                defer.notify(placeHolder, true);
            
            var xhr = new XMLHttpRequest;
            
            xhr.open("GET", blob, true); 
            xhr.responseType = "arraybuffer"; 
            this.xhr = xhr;
             
            xhr.onreadystatechange = _.bind(function() {
                if (4 === xhr.readyState) {
                    xhr.onreadystatechange = null; 
                    this.xhr = null; 
                    
                    if (200 === xhr.status) {
                        var blobResponse = new Blob([xhr.response], {
                            type: "image/jpeg"
                        });
                        
                        var objUrl = window.URL.createObjectURL(blobResponse);
                        this.cache.store({
                            url: blob,
                            blobUrl: objUrl
                        });
                        defer.resolve(objUrl);
                    } 
                    else 
                        defer.reject();
                }
            }, this); 
            xhr.send();
            
            return defer.promise();
        },
        
        loadImage: function(src, notify) {
            var defer = $.Deferred();
            var img = new Image;
            
            img.onload = function() {
                img.onload = img.onabort = img.onerror = null; 
                if (notify) 
                    defer.notify(src, img.width, img.height); 
                else
                    defer.resolve(src, img.width, img.height);
            }; 
            
            img.onabort = img.onerror = function() {
                img.onload = img.onabort = img.onerror = null; 
                defer.reject();
            };
             
            img.src = src; 
            
            return defer.promise();
        },
        
        cancel: function() {
            if (this.xhr) {
                this.xhr.onreadystatechange = null; 
                this.xhr.abort(); 
                this.xhr = null;
            }
        },
        
        onLoad: function(img, width, height) {
            if (this.backgroundSize) {
                var size = width && width > this.width || height && height > this.height ? this.backgroundSize : "auto";
                this.$el[0].style.backgroundSize = size;
            }
            this.src = img; 
            if (this.placeholderClass)
                this.$el.removeClass(this.placeholderClass); 
            
            this.showImage(img); 
            this.trigger("load", img);
        },
            
        onError: function() {
            if (this.resetOnError) 
                this.showPlaceholder(); 
            this.trigger("error");
        },        
        
        onClose: function() {
            this.cancel();
        },
        
        onChange: function(e, src) {
            src = this.getUrl(src); 
            if (this.url === src || e.get("type") == "kinect") 
                return this; 
            
            this.url = src; 
            this.src = null;
            
            return void this.render();       
        }
    });
	
    return baseImage;
});