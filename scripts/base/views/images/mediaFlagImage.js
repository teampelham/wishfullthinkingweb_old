define(["base/views/images/baseImage"], function(baseImage) {
    "use strict";
    var mediaFlagImage = baseImage.extend({
        setup: function(mediaObject) {
            this.model = mediaObject.model;
            this.server = mediaObject.model.server;
            
            if (mediaObject.className)
                this.$el.addClass(mediaObject.className);
            
            if (mediaObject.flag) {
                var media = this.model.get("Media");
                this.flag = mediaObject.flag;
                
                if (media && media.length) 
                    this.path = media[0][this.flag];
                
                if (!this.ignoreAttributeChanges)
                    this.listenTo(this.model, "change:Media", this.onMediaChange);
            }
            else {
                this.flag = mediaObject.modelAttribute;
                this.path = this.model.get(this.flag);
            
                if (!this.ignoreAttributeChanges)
                    this.listenTo(this.model, "change:" + this.flag, this.onChange);
            }
            
            this.prefix = this.model.get("mediaTagPrefix");
            this.version = this.model.get("mediaTagVersion");
            
            if (this.path)
                this.getUrl(this.path);
            
            this.setDimensions(mediaObject);
        },
        
        getUrl: function(e) {
            return this.prefix + this.flag + "/" + encodeURIComponent(e) + "?t=" + this.version;
        },
        
        onMediaChange: function(e, t) {
            this.onChange(e, t && t.length ? t[0][this.flag] : null);
        }
    });
    
    return mediaFlagImage;
});