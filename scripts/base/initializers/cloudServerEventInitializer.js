define([
	"base/utils/dispatcher", 
	"base/utils/pubsub/cloudServerEventConnection", 
	"base/managers/serverEventManager", 
	"base/models/appModel"
], function(dispatcher, cloudServerEventConnection, ServerEventManager, appModel) {
    "use strict";

    function onSignedInChange(e, signedIn) {
        if (signedIn)
            serverEventManager.connect({
                server: user.server
            });
        else
            serverEventManager.disconnect({
                server: user.server
            });
    }

    function disconnectAll() {
        serverEventManager.disconnectAll();
    }
	
    var serverEventManager = new ServerEventManager({
        ServerEventConnection: cloudServerEventConnection
    });
	
    var user = appModel.get("user");		// this isn't called until App Model initialization has completed
    
	var cloudServerEventInitializer = {
        init: function() {
            user.on("change:signedIn", onSignedInChange);	// when a user is signed in or out, connect/disconnect their cloud server connection
			dispatcher.on("request:disconnect:serverEvents", disconnectAll);	// register for disconnect all events
        }
    };
    
	return cloudServerEventInitializer;
});