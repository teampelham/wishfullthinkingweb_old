define([
    "base/adapters/dimensionsAdapter", 
    "base/adapters/errorTrackerAdapter", 
    "base/adapters/analyticsAdapter", 
    //"base/adapters/metricsAdapter", 
    "base/adapters/analytics/loggingErrorTracker", 
    "base/adapters/analytics/loggingAnalytics", 
    //"base/adapters/analytics/loggingMetrics", 
    "base/models/appModel", 
    "base/models/config", 
    "base/models/sessionStatsModel"
 ], function(dimensionsAdapter, errorTrackerAdapter, analyticsAdapter, loggingErrorTracker, loggingAnalytics, config, sessionStatsModel) {
    "use strict";

    function setUpErrorTracking(initObj) {
        if(config.get("env") !== "production" || analyticsAdapter.disableErrorTracking) {
            errorTrackerAdapter.disable();
            errorTrackerAdapter.register(loggingErrorTracker);
        }
        m = initObj.errorTrackerID || m;
        errorTrackerAdapter.init(m, dimensionsAdapter.prop("X-WF-Version"));
        errorTrackerAdapter.set("X-WF-Product", dimensionsAdapter.prop("X-WF-Product"));
        errorTrackerAdapter.set("X-WF-Device", dimensionsAdapter.prop("X-WF-Device")); 
        errorTrackerAdapter.set("X-WF-Platform", dimensionsAdapter.prop("X-WF-Platform")); 
        errorTrackerAdapter.set("X-WF-Platform-Version", dimensionsAdapter.prop("X-WF-Platform-Version"));
    }

    function setUpAnalytics(initObj) {
        if(config.get("env") !== "production" || initObj.disableAnalytics) 
            analyticsAdapter.register(loggingAnalytics);
        p = initObj.googleAnalyticsID || p;
        
        analyticsAdapter.init(p, {
            name: dimensionsAdapter.prop("X-WF-Product"),
            version: dimensionsAdapter.prop("X-WF-Version")
        });
        analyticsAdapter.set({
            dimension1: dimensionsAdapter.prop("X-WF-Product"),
            dimension2: dimensionsAdapter.prop("X-WF-Client-Identifier"),
            dimension3: dimensionsAdapter.prop("X-WF-Platform"),
            dimension4: dimensionsAdapter.prop("X-WF-Platform-Version"),
            dimension5: dimensionsAdapter.prop("X-WF-Device"),
            dimension6: dimensionsAdapter.prop("X-WF-Version")
        });
        
        // TODO: Figure out why this doesn't work
        /*sessionStatsModel.populate().done(function() {
            var e = sessionStatsModel.get("heartbeatTime") - sessionStatsModel.get("startTime"),
                t = Math.round(e / 1e3),
                n = sessionStatsModel.get("playbackCount");
            analyticsAdapter.trackEvent({
                eventCategory: "App",
                eventAction: "Shutdown",
                eventValue: t,
                metric1: n
            })
        });
        
        sessionStatsModel.populate().always(function() {
            sessionStatsModel.startSession();
            analyticsAdapter.trackEvent({
                eventCategory: "App",
                eventAction: "Start",
                eventValue: 1,
                sessionControl: "start"
            });
        });*/
    }

    function h(initObj) {
        /*if (config.get("env") !== "production" || initObj.disableMetrics) 
            metricsAdapter.register(loggingMetrics);
        
        var user = appModel.get("user");
        
        metricsAdapter.set({
            product: dimensionsAdapter.prop("X-WF-Product"),
            platform: dimensionsAdapter.prop("X-WF-Platform"),
            subplatform: dimensionsAdapter.prop("X-WF-Device"),
            visitorID: dimensionsAdapter.prop("X-WF-Client-Identifier"),
            userID: user.id
        }); 
        user.on("change:id", function(user) {
            metricsAdapter.set({
                userID: user.id
            });
        }); 
        appModel.on("change:primaryServer", function(e, server) {
            if (server)
                metricsAdapter.set({
                    serverID: t.id
                });
        });*/
    }
    var p = "UA-6111912-18";
    var m = "";// "https://67349abf654648e18514f1fbcb1603c8@app.getsentry.com/34246",
	
	var analyticsInitializer = {
		init: function(initObj) {
            if (!initObj)
                initObj = {};
            setUpErrorTracking(initObj);
            setUpAnalytics(initObj);
            h(initObj);
		}
	};
	
    return analyticsInitializer;
});