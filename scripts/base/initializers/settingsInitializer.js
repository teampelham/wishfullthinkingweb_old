define([
	"jquery", 
	"base/adapters/localizerAdapter", 
	"base/adapters/settingsAdapter", 
	"base/models/server/serverStorageModel"
], function($, localizerAdapter, settingsAdapter, serverStorageModel) {
    "use strict";
	
    var settingsInitializer = {
        init: function() {
            var deferred = $.Deferred();
            var settingsPromise = settingsAdapter.populate({	// forces the underlying storage model to retrieve its contents into memory
                reset: true
            });
            var serverPromise = serverStorageModel.populate({
                reset: true
            });
			
            $.when(settingsPromise, serverPromise).always(function() {
				// return the deferred promise for asynchronous loading,
				// once settings are loaded, retrieve the default language and set up localizations
                var language = settingsAdapter.get("language");
                localizerAdapter.load(language).always(function() {
                    deferred.resolve();
                });
            });
			return deferred.promise();
        }
    };
	
    return settingsInitializer;
});