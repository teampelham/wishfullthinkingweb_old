define("plex/base/initializers/mediaServerEventInitializer", ["plex/base/utils/t", "plex/base/utils/dispatcher", "plex/base/utils/capabilities", "plex/base/utils/constants/timelineEvents", "plex/base/utils/pubsub/MediaServerEventConnection", "plex/base/managers/ServerEventManager", "plex/base/models/appModel"], function(e, t, i, n, s, a, r) {
    "use strict";

    function o(e, t) {
        e.get("shared") && !e.supports("sharedSyncStatus") || e.get("synced") || e.get("cloud") || (t ? i.hasWebSocket ? u.connect({
            server: e
        }) : e.trigger("socket:websocket", {
            error: !0
        }) : u.disconnect({
            server: e
        }))
    }

    function l(e) {
        if (u.disconnect({
            server: e
        }), e.get("isMerged")) {
            var t = h.get(e.id);
            t.get("connected") && u.connect({
                server: t
            })
        }
    }

    function c() {
        u.disconnectAll()
    }

    function d(i) {
        var s;
        switch (i.state) {
            case n.CREATED:
                s = "Created {1} in {2} on {3}";
                break;
            case n.MATCHING:
                s = "Matching {1} in {2} on {3}";
                break;
            case n.DOWNLOADING_METADATA:
                s = "Downloading metadata for {1} in {2} on {3}";
                break;
            case n.LOADING_METADATA:
                s = "Loading metadata for {1} in {2} on {3}";
                break;
            case n.PROCESSED:
                s = "Finished processing {1} in {2} on {3}";
                break;
            case n.ANALYZING:
                s = "Analyzing {1} in {2} on {3}";
                break;
            case n.DELETED:
                s = "Deleted {1} in {2} on {3}"
        }
        if (s && !(i.sectionID < 0)) {
            var a = i.title || e("an item"),
                r = i.section ? i.section.get("title") : "section " + i.sectionID;
            t.trigger("show:status", i.server, e(s, a, r, i.server.get("friendlyName")))
        }
    }
    var u = new a({
        ServerEventConnection: s
    }),
        h = r.get("servers"),
        p = {
            init: function() {
                h.on({
                    "change:connected": o,
                    remove: l
                }), t.on({
                    "request:disconnect:serverEvents": c,
                    timeline: d
                })
            }
        };
    return p
});