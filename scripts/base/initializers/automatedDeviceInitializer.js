define([
	"base/utils/dispatcher", 
	"base/utils/pubsub/automatedDeviceEventConnection", 
	"base/managers/automatedDeviceEventManager", 
	"base/models/appModel"
], function(dispatcher, automatedDeviceEventConnection, AutomatedDeviceEventManager, appModel) {
    "use strict";

    function onSignedInChange(e, signedIn) {
        if (signedIn)
            serverEventManager.connect({
                server: user.server
            });
        else
            serverEventManager.disconnect({
                server: user.server
            });
    }

    function disconnectAll() {
        serverEventManager.disconnectAll();
    }
    
    function sendMessage(message,sender) {
        serverEventManager.sendMessage(message, sender);
    }
	
    var serverEventManager = new AutomatedDeviceEventManager({
        ServerEventConnection: automatedDeviceEventConnection
    });
	
    var user = appModel.get("user");		// this isn't called until App Model initialization has completed
    
	var automatedDeviceEventInitializer = {
        init: function() {
            //user.on("change:signedIn", onSignedInChange);	// when a user is signed in or out, connect/disconnect their cloud server connection
			dispatcher.on("request:disconnect:serverEvents", disconnectAll);	// register for disconnect all events
            dispatcher.on("send:serverMessage", sendMessage);
        }
    };
    
	return automatedDeviceEventInitializer;
});