// The AppInitializer sets the the Version and ClientID properties on the AppModel.
// It returns a Deferred promise to let the Main initializer know when its completed its operations
define([
	"jquery", 
	"base/utils/xhrTracker", 
	"base/utils/formatters/stringUtil", 
	"base/adapters/storageAdapter"
], function($, xhrTracker, stringUtil, storageAdapter) {
    "use strict";
    
	var appInitializer = {
        init: function(appModel) {
            var deferred = $.Deferred();
			
			// Set the App's Version property
            if (window.WFWEB && window.WFWEB.appVersion) 
				appModel.set("version", window.WFWEB.appVersion);
            else {
                var script = $("#wf-script");
                if (script.length) {
                    // Version is also appended to the script output
                    var scriptResult = /version=([\.\w]+)$/.exec(script[0].src);
                    if (scriptResult)
						appModel.set("version", scriptResult[1]);
                }
            }
			
            xhrTracker.startListening();
			
            var clientIDPromise = storageAdapter.get("clientID");		// Client ID is sent along with Cloud Event calls
			clientIDPromise.done(function(clientID) {
                appModel.set("clientID", clientID);
            }); 
			
			clientIDPromise.fail(function() {
				// Unable to retrieve clientID, time to create a new one
                var randomID = stringUtil.random();
                storageAdapter.set("clientID", randomID);
				appModel.set("clientID", randomID);
            });
			
			clientIDPromise.always(function() {
                deferred.resolve();			// AppInitializer is initialized
            });
			
			return deferred.promise();
        }
    };
	
    return appInitializer;
});