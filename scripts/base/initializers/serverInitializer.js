define([
	"underscore", 
	"jquery", 
	"base/utils/dispatcher", 
	"base/adapters/storageAdapter", 
	"base/adapters/settingsAdapter", 
	"base/managers/serverConnectionManager", 
	"base/managers/serverManager", 
	"base/managers/primaryServerManager", 
	"base/models/config", 
	"base/models/server/serverStorageModel",
    "base/models/appModel",
    "base/models/server/serverModel"
], function(_, $, dispatcher, storageAdapter, settingsAdapter, ServerConnectionManager, ServerManager, PrimaryServerManager, config, serverStorageModel, appModel, ServerModel) {
    "use strict";
    var serverInitializer = {
        
        init: function(serverInitializer) {
            if (!serverInitializer) 
                serverInitializer = {}; 
                
            this.servers = serverInitializer.servers || appModel.get("servers");
            
            this.connectionManager = serverInitializer.connectionManager || new ServerConnectionManager({
                servers: this.servers
            }); 
            
            this.serverManager = serverInitializer.serverManager || new ServerManager({
                servers: this.servers,
                savePrimaryOnly: config.get("serverSavePrimaryOnly")
            }); 
            
            this.primaryServerManager = serverInitializer.primaryServerManager || new PrimaryServerManager({
                servers: this.servers
            }); 
            
            this.initServers();
        },
        
        initServers: function() {
            this.servers.resetPopulatePromise($.Deferred()); 
            this.primaryServerManager.lastPrimaryServerID = serverStorageModel.get("lastPrimaryServerID");
            this.primaryServerManager.checkPendingServers();
            
            storageAdapter.get("myWFAccessToken").done(_.bind(this.initPersistedServers, this));
            
            this.initCloudServer();
            this.initBundledServer();
        },
        
        initPersistedServers: function(token) {
            var users = serverStorageModel.get("users");
            var registeredUser = _.findWhere(users, {
                accessToken: token
            });
            
            if(registeredUser) {
                var servers = registeredUser.servers;
                var invalidServers = [];
                
                _.each(servers, function(server) {
                    var connections = server.connections;
                    if (_.isEmpty(connections))
                        return void invalidServers.push(server);
                    
                    var user = appModel.get("user");
                    var sources = server.sources;
                    var noBundles = _.difference(sources, _.without(sources, "bundle", user.server.id));
                    
                    if (_.isEmpty(noBundles))
                        return void invalidServers.push(server);
                    
                    var omitted = _.omit(server, "connections");
                    omitted.sources = noBundles;
                    
                    var newServer = new ServerModel(omitted, {
                        parent: appModel
                    });
                    
                    this.servers.add(newServer);
                    _.each(connections, function(connection) {
                       var newConnection = _.clone(connection);
                       newConnection.accessToken = token.accessToken;
                       newServer.addConnection(newConnection); 
                    });
                }, this);
                
                if (invalidServers.length) {
                    _.each(invalidServers, function(server) {
                       servers.splice(_.indexOf(servers, server), 1); 
                    });
                    serverStorageModel.save();
                }
            }
        },
        
        initCloudServer: function() {
            var cloudServer = this.servers.findWhere({        // e
                cloud: true
            });
            
            var connection = cloudServer.addConnection({                       // t
                scheme: config.get("myWFScheme") || settingsAdapter.get("myWFScheme") || "https",
                address: config.get("myWFUrl") || settingsAdapter.get("myWFUrl") || "wishfullthinking.net"
            });
            cloudServer.set("activeConnection", connection);
        },
        
        initBundledServer: function() {
            var primaryServer = config.get("primaryServer");
            if (primaryServer || config.get("bundled")) {
                var connection;
                var initOptions = {
                    parent: appModel,
                    source: "bundle"
                };
                var bundle = {
                    bundled: true
                };
                
                if (primaryServer) {
                    initOptions.parse = true; 
                    _.extend(bundle, primaryServer);
                }
                else {
                    var port;
                    var protocol = window.location.protocol.replace(":", "");
                    var host = window.location.hostname;
                    port = "production" === config.get("env") ? window.location.port : "32400";
                    connection = {
                        scheme: protocol,
                        address: host,
                        port: port,
                        bundled: true
                    };
                }
                
                var server = new ServerModel(bundle, initOptions);
                this.servers.add(server);
                if (connection)
                    server.addConnection(connection);
            }
        }
    };
    
    dispatcher.on("initialize:servers", _.bind(serverInitializer.initServers, serverInitializer));
    
    return serverInitializer;
});