// Main starting point for WishFull Thinking Web
define([
    "marionette",   // Marionette.Application provides the initialization
    "mobile/initializers/mainInitializer",         // Initializes the goodies
    "mobile/adapters/mobileAdapters",             // Associates all the adapter interfaces to actual objects
    //"desktop/initializers/configs/desktopPartials", // Adds some plugins to Handlebars
    "mobile/commands/mobileCommands"              // 
], function(marionette, mainInitializer, mobileAdapters) {
    "use strict";
    // Bind each adapter interface to its instance
    mobileAdapters.registerAll();
    
    var app = new marionette.Application();

    app.addInitializer(mainInitializer);
    return app.start();
});