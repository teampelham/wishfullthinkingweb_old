// Main starting point for WishFull Thinking Web
define([
    "marionette",   // Marionette.Application provides the initialization
    "desktop/initializers/mainInitializer",         // Initializes the goodies
    "desktop/adapters/desktopAdapters",             // Associates all the adapter interfaces to actual objects
    //"desktop/initializers/configs/desktopPartials", // Adds some plugins to Handlebars
    "desktop/commands/desktopCommands"              // 
], function(marionette, mainInitializer, desktopAdapters) {
    "use strict";
    // Bind each adapter interface to its instance
    desktopAdapters.registerAll();
    
    var app = new marionette.Application();

    app.addInitializer(mainInitializer);
    return app.start();
});