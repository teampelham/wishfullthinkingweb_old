define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a = "";
            return a += '\n  <p class="text-muted">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "None of your shared libraries are available.", s) : p.call(e, "t", "None of your shared libraries are available.", s))) + "</p>\n"
        }

        function r(e, t) {
            var n;
            return n = i["if"].call(e, e && e.isManagedUser, {
                hash: {},
                inverse: f.program(6, l, t),
                fn: f.program(4, o, t),
                data: t
            }), n || 0 === n ? n : ""
        }

        function o(e, t) {
            var n, s, a = "";
            return a += '\n  <p class="text-muted">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "No one in your home has shared a library with you.", s) : p.call(e, "t", "No one in your home has shared a library with you.", s))) + "</p>\n"
        }

        function l(e, t) {
            var n;
            return n = i["if"].call(e, e && e.bundled, {
                hash: {},
                inverse: f.program(9, d, t),
                fn: f.program(7, c, t),
                data: t
            }), n || 0 === n ? n : ""
        }

        function c(e, t) {
            var n, s, a = "";
            return a += '\n  <p class="text-muted">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "You do not have permission to access this server.", s) : p.call(e, "t", "You do not have permission to access this server.", s))) + "</p>\n"
        }

        function d(e, t) {
            var n, s, a = "";
            return a += '\n  <p class="text-muted">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "We couldn't find any media servers for you. {1}Download and install{2} one, be sure to {3}sign the server in{4}, and it'll appear here.", "<a href='https://plex.tv/downloads' target='_blank'>", "</a>", "<a href='https://support.plex.tv/hc/en-us/articles/200878643-Sign-in-to-Your-Plex-Account' target='_blank'>", "</a>", s) : p.call(e, "t", "We couldn't find any media servers for you. {1}Download and install{2} one, be sure to {3}sign the server in{4}, and it'll appear here.", "<a href='https://plex.tv/downloads' target='_blank'>", "</a>", "<a href='https://support.plex.tv/hc/en-us/articles/200878643-Sign-in-to-Your-Plex-Account' target='_blank'>", "</a>", s))) + "</p>\n  <p class=\"text-muted\"><a class='local-server-test-link' href='#'>", s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "Already running a media server?", s) : p.call(e, "t", "Already running a media server?", s))) + "</a></p>\n"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var u, h = "",
            p = i.helperMissing,
            m = this.escapeExpression,
            f = this;
        return h += '<i class="plex-image server-sm"></i>\n\n', u = i["if"].call(t, t && t.hasSharedServers, {
            hash: {},
            inverse: f.program(3, r, s),
            fn: f.program(1, a, s),
            data: s
        }), (u || 0 === u) && (h += u), h += "\n"
    });
});