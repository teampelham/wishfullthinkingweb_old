define(["handlebars"], function(hb) {
	
    return hb.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"];
		i = this.merge(i, e.helpers);
		s = s || {}; 
		
		return '<div class="priority-1-container"></div>\n<div class="priority-2-container"></div>\n<div class="priority-3-container"></div>\n<div class="priority-4-container"></div>\n<div class="priority-5-container"></div>\n';
    });
});