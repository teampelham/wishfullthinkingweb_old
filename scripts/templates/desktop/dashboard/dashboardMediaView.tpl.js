define(["handlebars"], function(hb) {
    
    return hb.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"]; 
        i = this.merge(i, e.helpers);
        s = s || {};
        
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        return o += '<ul class="nav nav-header nav-dashboard pull-right">\n  <li><a class="previous-btn btn-gray" href="#"><i class="glyphicon chevron-left"></i></a></li>\n  <li><a class="next-btn btn-gray" href="#"><i class="glyphicon chevron-right"></i></a></li>\n</ul>\n\n<h2>', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, t && t.label, r) : l.call(t, "t", t && t.label, r))) + '</h2>\n\n<div class="dashboard-list-container"></div>\n'
    });
});