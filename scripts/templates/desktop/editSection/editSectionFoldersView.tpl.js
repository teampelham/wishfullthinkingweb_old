define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        return o += '<form class="edit-section-folders-form settings-container">\n  <label>\n    ', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Add folders to your library", r) : l.call(t, "t", "Add folders to your library", r))) + '\n  </label>\n\n  <div class="form-group Location-group">\n    <div class="paths-region"></div>\n\n    <p class="add-folder-container">\n      <button type="button" class="add-folder-btn btn btn-icon-label btn-default"><i class="glyphicon folder-plus"></i> ', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Browse for media folder", r) : l.call(t, "t", "Browse for media folder", r))) + '</button>\n    </p>\n\n    <p class="control-error">', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "At least one folder is required.", r) : l.call(t, "t", "At least one folder is required.", r))) + '</p>\n  </div>\n\n  <p class="help-block">', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Help us out by following {1}our guide{2} to naming and organizing your media.", '<a href="http://support.plex.tv/hc/en-us/categories/200028098-Media-Preparation" target="_blank">', "</a>", r) : l.call(t, "t", "Help us out by following {1}our guide{2} to naming and organizing your media.", '<a href="http://support.plex.tv/hc/en-us/categories/200028098-Media-Preparation" target="_blank">', "</a>", r))) + "</p>\n\n</form>\n"
    })
});