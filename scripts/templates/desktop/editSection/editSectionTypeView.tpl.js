define(["handlebars"], function(hb) {
    return hb.template(function(e, t, info, n, s) {
        
        function a() {
            return "section-type-chosen"
        }

        function tThisStuff(e, t, text) {
            var s = {
                hash: {},
                data: t
            };
            var translate = info.t || e && e.t;
            var tText = translate ? translate.call(e, text, s) : y.call(e, text, s);
            return b(tText);
        }
        
        function r(e, t) {
            var html = '\n    <p class="prompt">' + tThisStuff(e, t, 'Select your library type') + '</p>\n  ';
            return html;
        }

        function o(e, t) {
            var html = '\n    <label>' + tThisStuff(e, t, 'Library Type') + '</label>\n  ';
            return html;
        }

        function l(e, t, n) {
            var html = '\n      <a class="wizard-block section-type ';
            var s = info["if"].call(e, n && n.add, {
                hash: {},
                inverse: w.noop,
                fn: w.program(8, c, t),
                data: t
            });
            var r = {
                hash: {},
                inverse: w.noop,
                fn: w.program(10, d, t),
                data: t
            };
            if (s || s === 0)
                html += s;
            
            html += " ";
            
            s = info.if_eq || n && n.if_eq;
            var a = s ? s.call(e, n && n.currentSectionKey, e && e.key, r) :
                y.call(e, "if_eq", n && n.currentSectionKey, e && e.key, r);
                
            if (a || a === 0)
                html += a;
            
            html += '" data-key="';
            
            if (info.key)
                a = info.key.call(e, { hash: {}, data: t });
            else if (e && e.key && typeof e.key === x) 
                a = e.key.call(e, { hash: {}, data: t });
            else
                a = e && e.key;
            
            html += b(a) + '" href="#">\n        <i class="wizard-block-icon plex-icon ';
            if (info.sectionTypeIcon)
                a = info.sectionTypeIcon.call(e, { hash: {}, data: t });
            else if (e && e.sectionTypeIcon && typeof e.sectionTypeIcon === x)
                a = e.sectionTypeIcon.call(e, { hash: {}, data: t });
            else 
                a = e && e.sectionTypeIcon;
            
            html += b(a) + '"></i> ';
            r = tThisStuff(e, e && e.title, { hash: {}, data: t });
            
            html += b(r) + "\n      </a>\n    ";
            
            return html;
 
            /*var s, a, r, o = "";
            return o += '\n      <a class="wizard-block section-type ', s = i["if"].call(e, n && n.add, {
                hash: {},
                inverse: w.noop,
                fn: w.program(8, c, t),
                data: t
            }), (s || 0 === s) && (o += s), o += " ", r = {
                hash: {},
                inverse: w.noop,
                fn: w.program(10, d, t),
                data: t
            }, s = i.if_eq || n && n.if_eq, a = s ? s.call(e, n && n.currentSectionKey, e && e.key, r) : y.call(e, "if_eq", n && n.currentSectionKey, e && e.key, r), (a || 0 === a) && (o += a), o += '" data-key="', 
            (a = i.key) ? a = a.call(e, {
                hash: {},
                data: t
            }) : (a = e && e.key, a = typeof a === x ? a.call(e, {
                hash: {},
                data: t
            }) : a), o += b(a) + '" href="#">\n        <i class="wizard-block-icon plex-icon ', (a = i.sectionTypeIcon) ? a = a.call(e, {
                hash: {},
                data: t
            }) : (a = e && e.sectionTypeIcon, a = typeof a === x ? a.call(e, {
                hash: {},
                data: t
            }) : a), o += b(a) + '"></i> ', r = {
                hash: {},
                data: t
            }, o += b((s = i.t || e && e.t, s ? s.call(e, e && e.title, r) : y.call(e, "t", e && e.title, r))) + "\n      </a>\n    "*/
        }

        function c() {
            return "selectable"
        }

        function d() {
            return "selected"
        }

        function u(e, t) {
            var n, s, a = "";
            return a += '\n      <div class="row">\n        <div class="col-xs-12">\n          <p class="prompt">', s = {
                hash: {},
                data: t
            }, a += b((n = info.t || e && e.t, n ? n.call(e, "Name your library and choose language.", s) : y.call(e, "t", "Name your library and choose language.", s))) + "</p>\n        </div>\n      </div>\n    "
        }

        function h(e, t) {
            var n, s, a = "";
            return a += "\n            ", s = {
                hash: {},
                data: t
            }, a += b((n = info.t || e && e.t, n ? n.call(e, "Last updated {1}", e && e.updatedAt, s) : y.call(e, "t", "Last updated {1}", e && e.updatedAt, s))) + "\n          "
        }

        function p(e, t) {
            var n, s, a = "";
            return a += "\n            ", s = {
                hash: {},
                data: t
            }, a += b((n = info.t || e && e.t, n ? n.call(e, "Each library should only contain one type of media.", s) : y.call(e, "t", "Each library should only contain one type of media.", s))) + "\n          "
        }
        
        this.compilerInfo = [4, ">= 1.0.0"]; 
        info = this.merge(info, e.helpers); 
        s = s || {};
        
        var m, f, g, v = "";
        var y = info.helperMissing;
        var b = this.escapeExpression;
        var w = this;
        var x = "function";
        
        v += '<form class="edit-section-type-form settings-container '; 
        f = info["if"].call(t, (m = t && t.section, null == m || m === !1 ? m : m.type), {
            hash: {},
            inverse: w.noop,
            fn: w.program(1, a, s),
            data: s
        });
        (f || 0 === f) && (v += f); 
        v += '">\n  '; 
        f = info["if"].call(t, t && t.add, {
            hash: {},
            inverse: w.program(5, o, s),
            fn: w.program(3, r, s),
            data: s
        }); 
        (f || 0 === f) && (v += f); 
        v += '\n\n  <div class="wizard-blocks-group form-group type-group">\n    '; 
        f = info.each.call(t, t && t.sectionTypes, {
            hash: {},
            inverse: w.noop,
            fn: w.programWithDepth(7, l, s, t),
            data: s
        }); 
        (f || 0 === f) && (v += f); 
        v += '\n\n    <label class="control-label">\n      <span class="control-error">'; 
        g = {
            hash: {},
            data: s
        }; 
        v += b((m = info.t || t && t.t, m ? m.call(t, "You must select a media type.", g) : y.call(t, "t", "You must select a media type.", g))) + '</span>\n    </label>\n  </div>\n\n  <div class="visible-once-section-type-chosen">\n    '; 
        f = info["if"].call(t, t && t.add, {
            hash: {},
            inverse: w.noop,
            fn: w.program(12, u, s),
            data: s
        }); 
        (f || 0 === f) && (v += f); 
        v += '\n\n    <div class="row">\n      <div class="col-md-7">\n        <div class="form-group title-group">\n          <div>\n            <label for="edit-section-title" class="control-label">\n              '; 
        g = {
            hash: {},
            data: s
        }; 
        v += b((m = info.t || t && t.t, m ? m.call(t, "Name", g) : y.call(t, "t", "Name", g))) + '\n              <span class="control-error">'; 
        g = {
            hash: {},
            data: s
        }; 
        v += b((m = info.t || t && t.t, m ? m.call(t, "A library name is required.", g) : y.call(t, "t", "A library name is required.", g))) + '</span>\n            </label>\n          </div>\n          <input type="text" id="edit-section-title" name="title" class="form-control" value="' + b((m = t && t.section, m = null == m || m === !1 ? m : m.title, typeof m === x ? m.apply(t) : m)) + '">\n        </div>\n      </div>\n\n      <div class="col-md-5">\n        <div class="form-group language-group">\n          <div>\n            <label for="edit-section-language" class="control-label">\n              '; 
        
        g = {
            hash: {},
            data: s
        }; 
        v += b((m = info.t || t && t.t, m ? m.call(t, "Language", g) : y.call(t, "t", "Language", g))) + '\n              <span class="control-error">'; 
        g = {
            hash: {},
            data: s
        }; 
        v += b((m = info.t || t && t.t, m ? m.call(t, "A language is required.", g) : y.call(t, "t", "A language is required.", g))) + '</span>\n            </label>\n          </div>\n          <select id="edit-section-language" name="language"></select>\n        </div>\n      </div>\n    </div>\n\n    <div class="row">\n      <div class="col-md-12">\n        <span class="text-muted">\n          '; 
        f = info["if"].call(t, t && t.updatedAt, {
            hash: {},
            inverse: w.program(16, p, s),
            fn: w.program(14, h, s),
            data: s
        }); 
        (f || 0 === f) && (v += f); 
        v += "\n        </span>\n      </div>\n    </div>\n  </div>\n\n</form>\n";
        
        return v;
    });
});