define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = "function",
            c = this.escapeExpression,
            d = i.helperMissing;
        return o += '<form class="edit-section-advanced-form settings-container">\n  <div class="language-group">\n    <input type="hidden" name="language" value="' + c((a = t && t.section, a = null == a || a === !1 ? a : a.language, typeof a === l ? a.apply(t) : a)) + '">\n  </div>\n\n  <div class="row">\n    <div class="col-md-12">\n      <div class="form-group scanner-group hidden">\n        <label for="edit-section-scanner" class="control-label">\n          ', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Scanner", r) : d.call(t, "t", "Scanner", r))) + '\n          <span class="control-error">', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "A scanner is required.", r) : d.call(t, "t", "A scanner is required.", r))) + '</span>\n        </label>\n        <select id="edit-section-scanner" name="scanner"></select>\n      </div>\n    </div>\n  </div>\n\n  <div class="row">\n    <div class="col-md-12">\n      <div class="form-group agent-group hidden">\n        <label for="edit-section-agent" class="control-label">\n          ', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Agent", r) : d.call(t, "t", "Agent", r))) + '\n          <span class="control-error">', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "An agent is required.", r) : d.call(t, "t", "An agent is required.", r))) + '</span>\n        </label>\n        <select id="edit-section-agent" name="agent"></select>\n      </div>\n    </div>\n  </div>\n\n  <div class="row">\n    <div class="col-md-12 section-settings-region"></div>\n  </div>\n\n  <div class="row">\n    <div class="col-md-12 section-agent-settings-region"></div>\n  </div>\n</form>\n'
    })
});