define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r = "",
            o = "function",
            l = this.escapeExpression;
        return r += '<div class="col-md-12">\n  <div class="form-group path-group">\n    <input type="text" name="path" class="form-control" value="', (a = i.path) ? a = a.call(t, {
            hash: {},
            data: s
        }) : (a = t && t.path, a = typeof a === o ? a.call(t, {
            hash: {},
            data: s
        }) : a), r += l(a) + '">\n    <div class="path-actions">\n      <a href="#" class="remove-path-btn btn-link btn-gray"><i class="glyphicon remove-2"></i></a>\n    </div>\n  </div>\n</div>\n'
    })
});