define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a = "";
            return a += '\n  <div class="form-group">\n    <label class="control-label" for="input_importFromItunes_c">\n      <input type="checkbox" id="input_importFromItunes_c" name="importFromiTunes" value="1" ', n = i["if"].call(e, e && e.value, {
                hash: {},
                inverse: h.noop,
                fn: h.program(2, r, t),
                data: t
            }), (n || 0 === n) && (a += n), a += "> ", s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "Import from iTunes", s) : p.call(e, "t", "Import from iTunes", s))) + '\n    </label>\n\n    <p class="help-block">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "Importing from iTunes will import your music playlists. It will also import your ratings, the date you added the music, as well as play and skip counts for tracks in your library.", s) : p.call(e, "t", "Importing from iTunes will import your music playlists. It will also import your ratings, the date you added the music, as well as play and skip counts for tracks in your library.", s))) + "</p>\n  </div>\n"
        }

        function r() {
            return "checked"
        }

        function o(e, t, n) {
            var s, a, r, o = "";
            return o += "\n  ", r = {
                hash: {
                    idSuffix: "_c"
                },
                data: t
            }, o += m((s = i.partial || e && e.partial, s ? s.call(e, "setting", r) : p.call(e, "partial", "setting", r))) + "\n\n  ", a = i["if"].call(e, n && n.edit, {
                hash: {},
                inverse: h.noop,
                fn: h.program(5, l, t),
                data: t
            }), (a || 0 === a) && (o += a), o += "\n"
        }

        function l(e, t) {
            var n, s, a, r = "";
            return r += "\n    ", a = {
                hash: {},
                inverse: h.noop,
                fn: h.program(6, c, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, e && e.id, "enableBIFGeneration", a) : p.call(e, "if_eq", e && e.id, "enableBIFGeneration", a), (s || 0 === s) && (r += s), r += "\n  "
        }

        function c(e, t) {
            var n, s, a = "";
            return a += '\n      <div class="form-group">\n        <button type="button" class="delete-thumbnails-btn btn btn-danger">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "Delete Preview Thumbnails", s) : p.call(e, "t", "Delete Preview Thumbnails", s))) + "</button>\n      </div>\n    "
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var d, u = "",
            h = this,
            p = i.helperMissing,
            m = this.escapeExpression;
        return d = i["if"].call(t, t && t.isNewMusicSection, {
            hash: {},
            inverse: h.noop,
            fn: h.program(1, a, s),
            data: s
        }), (d || 0 === d) && (u += d), u += "\n\n", d = i.each.call(t, t && t.settings, {
            hash: {},
            inverse: h.noop,
            fn: h.programWithDepth(4, o, s, t),
            data: s
        }), (d || 0 === d) && (u += d), u += "\n"
    })
});