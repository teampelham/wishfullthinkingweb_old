define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        return o += '<form class="edit-section-type-form settings-container">\n  <p>', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "There was an error adding this library.", r) : l.call(t, "t", "There was an error adding this library.", r))) + "</p>\n  <p>", r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "You need to {1}follow these instructions{2} to troubleshoot the Plex Media Server.", "<a href='https://support.plex.tv/hc/en-us/articles/201119258-Resetting-System-and-Framework-bundles' target='_blank'>", "</a>", r) : l.call(t, "t", "You need to {1}follow these instructions{2} to troubleshoot the Plex Media Server.", "<a href='https://support.plex.tv/hc/en-us/articles/201119258-Resetting-System-and-Framework-bundles' target='_blank'>", "</a>", r))) + "</p>\n</form>"
    })
});