define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s = "";
            return s += '\n      <h2 class="text-muted">', (n = i.subtitle) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.subtitle, n = typeof n === c ? n.call(e, {
                hash: {},
                data: t
            }) : n), (n || 0 === n) && (s += n), s += "</h2>\n    "
        }

        function r(e, t) {
            var n, s = "";
            return s += "\n      <p>", (n = i.message) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.message, n = typeof n === c ? n.call(e, {
                hash: {},
                data: t
            }) : n), (n || 0 === n) && (s += n), s += "</p>\n    "
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var o, l = "",
            c = "function",
            d = this;
        return l += '<div class="row row-padded">\n  <div class="col-md-8 col-md-offset-2">\n    <h1>', (o = i.title) ? o = o.call(t, {
            hash: {},
            data: s
        }) : (o = t && t.title, o = typeof o === c ? o.call(t, {
            hash: {},
            data: s
        }) : o), (o || 0 === o) && (l += o), l += "</h1>\n    ", o = i["if"].call(t, t && t.subtitle, {
            hash: {},
            inverse: d.noop,
            fn: d.program(1, a, s),
            data: s
        }), (o || 0 === o) && (l += o), l += "\n    ", o = i["if"].call(t, t && t.message, {
            hash: {},
            inverse: d.noop,
            fn: d.program(3, r, s),
            data: s
        }), (o || 0 === o) && (l += o), l += "\n  </div>\n</div>"
    });
});