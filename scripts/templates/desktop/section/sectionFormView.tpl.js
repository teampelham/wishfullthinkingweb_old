define(["handlebars"], function(hb) {
	
    return hb.template(function(e, t, i, n, s) {
		this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var r, o, l, c = "",
            d = i.helperMissing,
            u = this.escapeExpression,
            h = this;
			
	 	c += '<div class="section-form-container">\n  <div class="settings-group active">\n';
		c += '<h4 class="section-title">Audio Controls</h4>\n';
		c += '<div class="form-group">\n<label class="control-label">\n<input type="checkbox" id="useVoiceRecognition"/> Enable Voice Recognition</label>\n </div>\n'
		c += '<div class="form-group">\n<label class="control-label">\n<input type="text" id="sayInput" placeholder="Say something" /><button id="speak">Speak</button></label>\n</div>\n';
		c += '</div>\n</div>';
		return c;
		
    });
});