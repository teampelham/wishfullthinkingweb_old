define(["handlebars"], function(hb) {
	
    return hb.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a = "";
            return a += "\n      ", s = {
                hash: {},
                data: t
            }, a += u((n = i.t || e && e.t, n ? n.call(e, "Click {1}here{2} to add content to the library.", '<a href="#" class="add-to-library-link">', "</a>", s) : d.call(e, "t", "Click {1}here{2} to add content to the library.", '<a href="#" class="add-to-library-link">', "</a>", s))) + "\n    "
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var r, o, l, c = "",
            d = i.helperMissing,
            u = this.escapeExpression,
            h = this;
        return c += '<div class="section-loading-container hidden">\n  <div class="loading loading-inline"></div>\n</div>\n\n<h4 class="empty-message text-muted text-center">\n  <span class="empty-default">\n    ', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "The library has no content yet.", l) : d.call(t, "t", "The library has no content yet.", l))) + "\n\n    ", o = i["if"].call(t, t && t.canEdit, {
            hash: {},
            inverse: h.noop,
            fn: h.program(1, a, s),
            data: s
        }), (o || 0 === o) && (c += o), c += '\n  </span>\n  <span class="empty-refreshing">\n    ', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "The library is still being refreshed. Hang on while we look for your content.", l) : d.call(t, "t", "The library is still being refreshed. Hang on while we look for your content.", l))) + '\n  </span>\n  <span class="empty-filter">\n    ', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "There is no content matching your active filter.", l) : d.call(t, "t", "There is no content matching your active filter.", l))) + '\n  </span>\n  <span class="empty-folder">\n    ', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "There is no content in the selected folder.", l) : d.call(t, "t", "There is no content in the selected folder.", l))) + "\n  </span>\n</h4>\n"
    });
});