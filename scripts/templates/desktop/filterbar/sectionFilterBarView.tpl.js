define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r = "",
            o = "function",
            l = this.escapeExpression;
        return r += '<div class="filter-bar-right">\n  <a class="clear-filters-btn btn-gray" href="#"><i class="glyphicon remove-2"></i></a>\n</div>\n\n<h4>', (a = i.titles) ? a = a.call(t, {
            hash: {},
            data: s
        }) : (a = t && t.titles, a = typeof a === o ? a.call(t, {
            hash: {},
            data: s
        }) : a), r += l(a) + "</h4>\n"
    });
});