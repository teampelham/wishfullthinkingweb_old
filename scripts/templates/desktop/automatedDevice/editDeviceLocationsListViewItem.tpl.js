define(["handlebars"], function(e) {
    return e.template(function(e, t, info, n, s) {
        
        function conditionalHtml(op, text, data, each) {
            var fn = each ? info.each : info[op];
			var fnResult = fn.call(t, text, data);
        	if (fnResult || fnResult === 0) 
				return fnResult;
			return '';
		}
        
        function locationList(locations) {
            var ol = '    <ul>\n';
            ol += conditionalHtml("if", locations, {
                hash: {},
                inverse: that.noop,
                fn: that.programWithDepth(9, renderLocations, s, locations),
                data: s
            }, true);
            ol += '    </ul>\n';
            return ol;
        }
        
        function renderLocations(location) {
            //var li = '        <li data-id="';
            //li += location.key + '">\n' + location.title;
            //li += '        <div class="dd-handle">' + location.title + '</div>\n';
            var li = '       <li class="jstree-open" id="location-' + location.key + '">' + location.title;
            li += (location.childLocations && location.childLocations.length) ? locationList(location.childLocations) : '';
            li += '    </li>\n';
            
            return li;
        }
        
        this.compilerInfo = [4, ">= 1.0.0"], info = this.merge(info, e.helpers), s = s || {};
        var that = this;
        var html = '<div class="dd">\n  ';
        html += conditionalHtml("if", t && t.length, {
                hash: {},
                inverse: that.noop,
                fn: that.program(9, locationList, s, t),
                data: s
            });
        html += '</div>';
        return html;
    });
});
    