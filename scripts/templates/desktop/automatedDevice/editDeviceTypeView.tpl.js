define(["handlebars"], function(hb) {
    return hb.template(function(e, t, info, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"]; 
        info = this.merge(info, e.helpers); 
        s = s || {};
        
        var translatorFn = info.t || t && t.t;
        var missing = info.helperMissing;
        var esc = this.escapeExpression;
        var that = this;
        
        function dataHash(data) {
            return {
                hash: {},
                data: data
            };
        }
        
        function translateText(text) {
            if(translatorFn)
                text = translatorFn.call(t, text, dataHash(s));
            else
                text = missing.call(t, "t", text, dataHash(s));  
            return esc(text);
        }
        
        function conditionalHtml(op, text, data, each) {
            var fn = each ? info.each : info[op];
			var fnResult = fn.call(t, text, data);
        	if (fnResult || fnResult === 0) 
				return fnResult;
			return '';
		}
        
        function equalityHtml(op, text1, text2, data) {
            var result;
            if(op)   
                result = op.call(t, text1, text2, data);
            else
                result = missing.call(t, "if_eq", text1, text2, data);
                
            if (result || result === 0) 
				return result;
			return '';
        }
        
        function chosen() { // a
            return "section-type-chosen"
        }
        
        function selectDevice(e, t) {   // r
            var html = '\n    <p class="prompt">' + translateText('Select your Device type') + '</p>\n  ';
            return html;
        }

        function deviceTypeLabel(e, t) {    // o
            return '\n    <label>' + translateText('Device Type') + '</label>\n  ';
        }

        function deviceTypeWizard(e, t, n) {        // l
            var html = '\n      <a class="wizard-block section-type ';
            html += conditionalHtml("if", n && n.add, {
                hash: {},
                inverse: that.noop,
                fn: that.program(8, selectable, t),
                data: t
            });
            
            html += " ";
            html += equalityHtml(info.if_eq || n && n.if_eq, n && n.currentType, e && e.typeID, {
                hash: {},
                inverse: that.noop,
                fn: that.program(10, selected, t),
                data: t
            });
            
            html += '" data-key="';
            
            var dataHolder;
            if (info.typeID)
                dataHolder = info.typeID.call(e, { hash: {}, data: t });
            else if (e && e.typeID && typeof e.typeID === "function") 
                dataHolder = e.typeID.call(e, { hash: {}, data: t });
            else
                dataHolder = e && e.typeID;
            
            html += esc(dataHolder) + '" href="#">\n        <i class="wizard-block-icon plex-icon ';
            
            if (info.sectionTypeIcon)
                dataHolder = info.sectionTypeIcon.call(e, { hash: {}, data: t });
            else if (e && e.sectionTypeIcon && typeof e.sectionTypeIcon === "function")
                dataHolder = e.sectionTypeIcon.call(e, { hash: {}, data: t });
            else 
                dataHolder = e && e.sectionTypeIcon;
            
            html += esc(dataHolder) + '"></i> ';
            html += esc(translateText(e && e.title));
            html += "\n      </a>\n    ";
            
            return html;
        }

        function selectable() { //c 
            return "selectable"
        }

        function selected() {   // d
            return "selected"
        }

        function setTitle(e, t) {
            return e.currentTitle;
        }
        
        function deviceTitleTypeLabel(e, t) {
            return '\n      <div class="row">\n        <div class="col-xs-12">\n          <p class="prompt">' +
                translateText("Name your device and choose what type it is") + "</p>\n        </div>\n      </div>\n    ";
                
            /*var n, s, a = "";
            return a += '\n      <div class="row">\n        <div class="col-xs-12">\n          <p class="prompt">', s = {
                hash: {},
                data: t
            }, a += b((n = info.t || e && e.t, n ? n.call(e, "Name your device and choose a location.", s) : y.call(e, "t", "Name your library and choose location.", s))) + "</p>\n        </div>\n      </div>\n    "*/
        }

        /*
        function h(e, t) {
            var n, s, a = "";
            return a += "\n            ", s = {
                hash: {},
                data: t
            }, a += b((n = info.t || e && e.t, n ? n.call(e, "Last updated {1}", e && e.updatedAt, s) : y.call(e, "t", "Last updated {1}", e && e.updatedAt, s))) + "\n          "
        }

        function p(e, t) {      // p
            var n, s, a = "";
            return a += "\n            ", s = {
                hash: {},
                data: t
            }, a += b((n = info.t || e && e.t, n ? n.call(e, "Each library should only contain one type of media.", s) : y.call(e, "t", "Each library should only contain one type of media.", s))) + "\n          "
        }
        */
        function renderDeviceTypeOptions(deviceType, t, n) {
            if (n.currentDeviceType == deviceType.typeID)
                return '<option value="' + deviceType.typeID + '" selected="selected">' + deviceType.title + '</option>'; 
            return '<option value="' + deviceType.typeID + '">' + deviceType.title + '</option>';
        }
		
        var dataHolder;
        var html = '<form class="edit-section-type-form settings-container '; 
		dataHolder = t && t.section;
		dataHolder = (dataHolder == null || dataHolder === false) ? dataHolder : dataHolder.type;
		html += conditionalHtml("if", dataHolder, {
            hash: {},
            inverse: that.noop,
            fn: that.program(1, chosen, s),
            data: s
        });
        
        html += '">\n  '; 
        
		html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.program(5, deviceTypeLabel, s),
            fn: that.program(3, selectDevice, s),
            data: s
        });
        html += '\n\n  <div class="wizard-blocks-group form-group type-group">\n    '; 
        
		html += conditionalHtml("if", t && t.protocolTypes, {
            hash: {},
            inverse: that.noop,
            fn: that.programWithDepth(7, deviceTypeWizard, s, t),
            data: s
        }, true);
        html += '\n\n    <label class="control-label">\n      <span class="control-error">'; 
        
		html += translateText("You must select a device type.") + '</span>\n    </label>\n  </div>\n\n  <div class="visible-once-section-type-chosen">\n    '; 
		html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.noop,
            fn: that.program(12, deviceTitleTypeLabel, s),
            data: s
        });
        html += '\n\n    <div class="row">\n      <div class="col-md-7">\n        <div class="form-group title-group">\n          <div>\n            <label for="edit-device-title" class="control-label">\n              '; 
        
		html += translateText("Name") + '\n              <span class="control-error">';
		html += translateText("A name for the device is required.") + '</span>\n            </label>\n          </div>\n          <input type="text" id="edit-device-title" name="title" class="form-control" value="';
        html += conditionalHtml("if", t && t.currentTitle, {
            hash: {},
            inverse: that.noop,
            fn: that.program(14, setTitle, s),
            data: s
        });
		html += '">\n        </div>\n      </div>\n\n      <div class="col-md-5">\n        <div class="form-group language-group">\n          <div>\n            <label for="edit-device-type" class="control-label">\n              '; 
		html += translateText("Device Type") + '\n              <span class="control-error">'; 
		html += translateText("A device type is required") + '</span>\n            </label>\n          </div>\n          <select id="edit-device-type" name="deviceType" class="form-control"><option value="">Select Device Type</option>';

        html += conditionalHtml("if", t && t.deviceTypes, {
            hash: {},
            inverse: that.noop,
            fn: that.programWithDepth(9, renderDeviceTypeOptions, s, t),
            data: s
        }, true);        
        html += '</select>\n        </div>\n      </div>\n    </div>\n\n    <div class="row">\n      <div class="col-md-12">\n        <span class="text-muted">\n          '; 
        
        /*
		html += conditionalHtml("if", t && t.updatedAt, {
            hash: {},
            inverse: that.program(16, p, s),
            fn: that.program(14, h, s),
            data: s
        });*/
        html += "\n        </span>\n      </div>\n    </div>\n  </div>\n\n</form>\n";
        
        return html;
    });
});