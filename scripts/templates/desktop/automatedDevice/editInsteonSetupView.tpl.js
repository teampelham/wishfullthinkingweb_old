define(["handlebars"], function(hb) {
    return hb.template(function(e, t, info, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"]; 
        info = this.merge(info, e.helpers); 
        s = s || {};
        
        var translatorFn = info.t || t && t.t;
        var missing = info.helperMissing;
        var esc = this.escapeExpression;
        var that = this;
        
        function dataHash(data) {
            return {
                hash: {},
                data: data
            };
        }
        
        function translateText(text) {
            if(translatorFn)
                text = translatorFn.call(t, text, dataHash(s));
            else
                text = missing.call(t, "t", text, dataHash(s));  
            return esc(text);
        }
        
        function conditionalHtml(op, text, data, each) {
            var fn = each ? info.each : info[op];
			var fnResult = fn.call(t, text, data);
        	if (fnResult || fnResult === 0) 
				return fnResult;
			return '';
		}
        
        function buttonStateAutoRegister() {
            return ' auto-register">Auto-Register';    
        }
        
        function buttonStateRegister() {
            return '">Register';    
        }
        
        function renderHardwares(hardware, t, n) {
            if (n.currentHardware == hardware.key)
                return '<option value="' + hardware.key + '" selected="selected">' + hardware.title + '</option>';
            return '<option value="' + hardware.key + '">' + hardware.title + '</option>';
        }
        
        function getAddress(t) {
            var m = t && t.section;
            m = (m == null || m === false) ? m : m.address;
            return esc(typeof m === "function" ? m.apply(t) : m);
        }
        
        function setDimmable() {
            return 'checked="checked"';
        }
        
        var html = '<form class="edit-device-hardware-form settings-container">\n  ';   
        html += '    <div class="row">\n      <div class="col-md-8">\n        <div class="form-group address-group">\n          <div>\n            <label for="edit-address-title" class="control-label">\n              '; 
        html += translateText("Device Address")
        html += '\n              <span class="control-error">';
        html += translateText("An Address is required");            // TODO: Remove inline style below
        html += '</span>\n            </label>\n          </div>\n          <input type="text" id="edit-address-title" style="display:inline-block; width: 100px;" name="address" class="form-control" value="';
        html += getAddress(t);   
        html += '">\n    <button id="btnRegister" class="btn btn-default';
        html += conditionalHtml("if", t && t.address, {
            hash: {},
            inverse: that.program(1, buttonStateAutoRegister, s),
            fn: that.program(3, buttonStateRegister, s),
            data: s
        });
        html += '</button>\n    </div>\n    </div>\n\n     </div>\n <div class="form-group hardware-group">\n';
        html += '\n\n    <div class="row">\n      <div class="col-md-8">\n        <div class="form-group hardware-group">\n          <div>\n            <label for="edit-hardware" class="control-label">\n              '; 
        html += translateText("Device Hardware")
        html += '\n              <span class="control-error">';
        html += translateText("Hardware is required");
		html += '</span>\n            </label>\n          </div>\n          <select id="edit-hardware" name="hardware" class="form-control">';
        html += '<option value="">Select Hardware</option>';
        html += conditionalHtml("if", t && t.hardware, {
            hash: {},
            inverse: that.noop,
            fn: that.programWithDepth(9, renderHardwares, s, t),
            data: s
        }, true);		
        html += '</select>\n        </div>\n      </div>\n\n      </div>\n <div class="row">\n <div class="col-md-8">\n        <div class="form-group dimmable-group">\n          <div>\n            <label for="edit-dimmable" class="control-label">\n              ';
        
		html += translateText("Allow your device to Dim?");
		html += '</label>\n          </div>\n          <input type="checkbox" id="edit-dimmable" name="dimmable"';
        html += conditionalHtml("if", t && t.currentDimmable, {
            hash: {},
            inverse: that.noop,
            fn: that.program(10, setDimmable, s),
            data: s
        });		
        html += '/></div>\n      </div>\n\n      </form>\n              ';
        
        return html;
    });
});