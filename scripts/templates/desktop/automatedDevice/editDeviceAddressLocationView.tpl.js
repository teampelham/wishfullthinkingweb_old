define(["handlebars"], function(hb) {
    return hb.template(function(e, t, info, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"]; 
        info = this.merge(info, e.helpers); 
        s = s || {};
        
        var translatorFn = info.t || t && t.t;
        var missing = info.helperMissing;
        var esc = this.escapeExpression;
        var that = this;
        
        function dataHash(data) {
            return {
                hash: {},
                data: data
            };
        }
        
        function translateText(text) {
            if(translatorFn)
                text = translatorFn.call(t, text, dataHash(s));
            else
                text = missing.call(t, "t", text, dataHash(s));  
            return esc(text);
        }
        
        function conditionalHtml(op, text, data, each) {
            var fn = each ? info.each : info[op];
			var fnResult = fn.call(t, text, data);
        	if (fnResult || fnResult === 0) 
				return fnResult;
			return '';
		}
        
        function renderLocations(location) {
            var chosenLocation = (t && t.section && t.section.location) ? t.section.location : "";
            if (chosenLocation == location.key)
                return '<option value="' + location.key + '" selected="selected">' + location.title + '</option>';    
            return '<option value="' + location.key + '">' + location.title + '</option>';
        }
        
        
        
        var html = '<form class="settings-container">\n  ';      
        
        html += '<div class="row">\n <div class="col-md-8">\n        <div class="form-group location-group">\n          <div>\n            <label for="edit-location" class="control-label">\n              ';
        html += translateText("Assign a Location to your device");
        html += '\n              <span class="control-error">';
        html += translateText("A Location is required");
        html += '</span>\n            </label>\n          </div>\n          ';/*<select id="edit-location" name="location" class="form-control">';
        html += '<option value="">Select Location</option>';
        html += conditionalHtml("if", t && t.locations, {
            hash: {},
            inverse: that.noop,
            fn: that.programWithDepth(9, renderLocations, s, t),
            data: s
        }, true);
        html += '"</select>\n        </div>\n    <div class="paths-region"></div>\n      </div>\n\n      </form>\n              ';*/
        html += '</div>\n    <div class="paths-region"></div>\n      </div>\n\n      </form>\n              ';
        
        /*o += c((a = i.t || t && t.t, a ? a.call(t, "Browse for media folder", r) : l.call(t, "t", "Browse for media folder", r))) + '</button>\n    </p>\n\n    <p class="control-error">', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "At least one folder is required.", r) : l.call(t, "t", "At least one folder is required.", r))) + '</p>\n  </div>\n\n  <p class="help-block">', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Help us out by following {1}our guide{2} to naming and organizing your media.", '<a href="http://support.plex.tv/hc/en-us/categories/200028098-Media-Preparation" target="_blank">', "</a>", r) : l.call(t, "t", "Help us out by following {1}our guide{2} to naming and organizing your media.", '<a href="http://support.plex.tv/hc/en-us/categories/200028098-Media-Preparation" target="_blank">', "</a>", r))) + "</p>\n\n</form>\n";
        */
        return html;
    });
});