define(["handlebars"], function(hb) {
    
	return hb.template(function(e, t, i, n, s) {
        
        function a(e, t) {
            var n, s, html = "";
            return html += '\n    <div class="remember-group checkbox">\n      <label class="control-label">\n        <input type="checkbox" id="remember" checked> ', s = {
                hash: {},
                data: t
            }, html += "Remember me\n      </label>\n    </div>\n  ";
        }
        
        this.compilerInfo = [4, ">= 1.0.0"];
        i = this.merge(i, e.helpers);
        s = s || {};
        
        var r, o, l, c = "",
            d = i.helperMissing,
            u = this.escapeExpression,
            h = this;
            
        c += '<div class="row">\n  <div class="col-sm-12">\n    <h2 class="sign-in-header">\n      <a class="sign-in-btn selected" href="#">', l = {
            hash: {},
            data: s
        };
        
        r = i.t || t && t.t;
        
        c += 'Sign In</a>\n      <span class="sign-in-header-divider">';
        l = {
            hash: {},
            data: s
        }; 
        c += 'or</span>\n      <a class="sign-up-btn" href="#">'; 
        l = {
            hash: {},
            data: s
        };
        c += 'Sign Up</a>\n    </h2>\n  </div>\n</div>\n\n<div class="username-group form-group">\n  <label for="username" class="control-label">\n    ';
        l = {
            hash: {},
            data: s
        };
        c += 'Username <span class="control-error">';
        l = {
            hash: {},
            data: s
        };
        c += 'Required</span>\n  </label>\n  <input type="text" id="username" class="form-control" autocorrect="off" autocapitalize="off">\n</div>\n\n<div class="email-group form-group">\n  <label for="email" class="control-label">\n    ';
        l = {
            hash: {},
            data: s
        };
        c += 'Email <span class="control-error">';
        l = {
            hash: {},
            data: s
        };
        c += 'Required</span>\n  </label>\n  <input type="text" id="email" class="form-control" autocorrect="off" autocapitalize="off">\n</div>\n\n<div class="password-group form-group">\n  <label for="password" class="control-label">\n    '; 
        l = {
            hash: {},
            data: s
        };
        c += 'Password <span class="control-error">';
        l = {
            hash: {},
            data: s
        }; 
        c += 'Required</span>\n  </label>\n  <input type="password" id="password" class="form-control">\n</div>\n\n<div class="confirm-group form-group">\n  <label for="confirm" class="control-label">\n    ';
        l = {
            hash: {},
            data: s
        };
        c += 'Confirm Password <span class="control-error">', l = {
            hash: {},
            data: s
        } 
        c += 'Passwords do not match</span>\n  </label>\n  <input type="password" id="confirm" class="form-control">\n</div>\n\n<div class="age-group form-group">\n  <label for="month" class="control-label">\n    ';
        l = {
            hash: {},
            data: s
        }; 
        c += 'Date of Birth <span class="invalid-error control-error">';
        l = {
            hash: {},
            data: s
        };
        c += 'This can not be your birthday</span> <span class="underage-error control-error">', l = {
            hash: {},
            data: s
        }, c += 'You must be 13 years or older</span>\n  </label>\n  <div class="row">\n    <div class="birthday-col col-xs-2">\n      <input type="text" id="month" class="form-control" placeholder="01">\n    </div>\n    <div class="birthday-col col-xs-2">\n      <input type="text" id="day" class="form-control" placeholder="31">\n    </div>\n    <div class="birthday-col col-xs-3">\n      <input type="text" id="age" class="form-control" placeholder="1985">\n    </div>\n  </div>\n</div>\n\n<div class="form-footer">\n  <button type="submit" class="btn btn-lg btn-primary btn-loading pull-right">\n    <div class="loading loading-sm"></div>\n    <span class="sign-in-label btn-label">', l = {
            hash: {},
            data: s
        }, c += 'Sign In</span>\n    <span class="sign-up-label btn-label">', l = {
            hash: {},
            data: s
        }, c += 'Sign Up</span>\n  </button>\n\n  <div class="terms-group checkbox">\n    <label class="control-label">\n      <input type="checkbox" id="terms" checked> ', l = {
            hash: {},
            data: s
        }, c += "<a href='https://app.wishfullthinking.net/legal' target='_blank'>I agree to the {1}terms of service{2}</a>\n    </label>\n  </div>\n\n  ";
        
        o = i["if"].call(t, t && t.showRememberMe, {
            hash: {},
            inverse: h.noop,
            fn: h.program(1, a, s),
            data: s
        });
        
        if (o || o === 0)
            c += o;
        c += "\n</div>\n";
        return c;
    });
});