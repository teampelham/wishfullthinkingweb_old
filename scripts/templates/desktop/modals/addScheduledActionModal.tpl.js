define(["handlebars"], function(hb) {
    return hb.template(function(e, t, info, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"]; 
        info = this.merge(info, e.helpers); 
        s = s || {};
        
        var translatorFn = info.t || t && t.t;
        var missing = info.helperMissing;
        var esc = this.escapeExpression;
        var that = this;
        
        function dataHash(data) {
            return {
                hash: {},
                data: data
            };
        }
        
        function translateText(text) {
            if(translatorFn)
                text = translatorFn.call(t, text, dataHash(s));
            else
                text = missing.call(t, "t", text, dataHash(s));  
            return esc(text);
        }
        
        function conditionalHtml(op, text, data, each) {
            var fn = each ? info.each : info[op];
			var fnResult = fn.call(t, text, data);
        	if (fnResult || fnResult === 0) 
				return fnResult;
			return '';
		}
        
        function equalityHtml(op, text1, text2, data) {
            var result;
            if(op)   
                result = op.call(t, text1, text2, data);
            else
                result = missing.call(t, "if_eq", text1, text2, data);
                
            if (result || result === 0) 
				return result;
			return '';
        }
        
        function editScheduledAction(e, t) {
            return '\n          <i class="modal-icon glyphicon pencil"></i> ' + translateText("Edit") + '<span class="schedule-title"></span>\n        ';
        }

        function addScheduledAction(e, t) {
            return '\n          <i class="modal-icon glyphicon plus"></i> ' + translateText("Add Scheduled Action") + "\n        ";
        }

        function isSelected() {
            return " selected";
        }

        function selectDate(e, t) {
            return "\n                " + translateText("Select Date(s)") + "\n              ";
        }

        function general(e, t) {
            return "\n                " + translateText("General") + "\n              ";
        }

        function disabled() {
            return "disabled";
        }

        function create(e, t) {
            var html = '\n            <li>\n              <a class="btn-gray change-pane-btn create-btn';
            html += equalityHtml(info.if_eq || t && t.if_eq, t && t.selectedPane, "create", {
                hash: {},
                inverse: that.noop,
                fn: that.program(5, isSelected, t),
                data: t
            });
            html += conditionalHtml("if", t && t.add, {
                hash: {},
                inverse: that.noop,
                fn: that.program(7, disabled, s),
                data: s
            });
            html += ' hidden" data-pane="create" href="#">\n                <i class="modal-nav-icon glyphicon magic"></i>\n                ';
            html += translateText("Create") + "\n              </a>\n            </li>\n          ";
            
            return html;
        }

        function deleteDevice(e, t) {
            return '\n        <button type="button" class="delete-btn btn btn-danger pull-left" data-dismiss="modal">' + translateText("Delete Scheduled Action") + "</button>\n      ";
        }

        function addDeviceText(e, t) {
            return "\n              " + translateText("Add Scheduled Action") + "\n            ";
        }

        function saveChanges(e, t) {
            return "\n              " + translateText("Save Changes") + "\n            ";
        }
        
        var html = '<div class="modal-dialog">\n  <div class="modal-content">\n    <div class="modal-header">\n      <button type="button" class="close" data-dismiss="modal"><i class="glyphicon remove-2"></i></button>\n      <h4 class="modal-title">\n        ';
        
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.program(3, editScheduledAction, s),
            fn: that.program(1, addScheduledAction, s),
            data: s
        });
        
        html += '\n      </h4>\n    </div>\n\n    <div class="modal-body modal-body-scroll modal-body-with-panes">\n      <div class="modal-nav-pane dark-scrollbar">\n        <ul class="list modal-nav-list">\n          <li>\n            <a class="btn-gray change-pane-btn date-btn';       
        html += equalityHtml(info.if_eq || t && t.if_eq, t && t.selectedPane, "date", {
            hash: {},
            inverse: that.noop,
            fn: that.program(5, isSelected, s),
            data: s
        });

        html += '" data-pane="date" href="#">\n              <i class="modal-nav-icon glyphicon align-center"></i>\n              ';
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.program(9, general, s),
            fn: that.program(7, selectDate, s),
            data: s
        });
        
        html += '\n            </a>\n          </li>\n\n          <li>\n            <a class="btn-gray change-pane-btn time-btn';
        
        html += equalityHtml(info.if_eq || t && t.if_eq, t && t.selectedPane, "time", {
            hash: {},
            inverse: that.noop,
            fn: that.program(5, isSelected, s),
            data: s
        });
        html += " ";
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.noop,
            fn: that.program(11, disabled, s),
            data: s
        });
        
        html += '" data-pane="time" href="#">\n              <i class="modal-nav-icon glyphicon folder-open"></i>\n              ';
        html += translateText("Set Time") + "\n            </a>\n          </li>\n\n          ";
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.noop,
            fn: that.program(13, create, s),
            data: s
        });
        
        html += '\n        </ul>\n      </div>\n\n      <div class="modal-body-pane pane-region dark-scrollbar">\n      </div>\n    </div>\n\n    <div class="modal-footer">\n      ';
        
        html += conditionalHtml("if", t && t.edit, {
            hash: {},
            inverse: that.noop,
            fn: that.program(15, deleteDevice, s),
            data: s
        });
        html += '\n\n      <span class="form-message"></span>\n\n      <button type="button" class="cancel-btn btn btn-default" data-dismiss="modal">';
        html += translateText("Cancel") + '</button>\n      <button type="button" class="save-btn btn btn-primary btn-loading ';
        
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.noop,
            fn: that.program(11, disabled, s),
            data: s
        });
        html += '">\n        <div class="loading loading-sm"></div>\n        <span class="btn-label">\n          <span class="save-btn-label-next">';
        html += translateText("Next") + '</span>\n          <span class="save-btn-label-save">\n            ';
        
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.program(19, saveChanges, s),
            fn: that.program(17, addDeviceText, s),
            data: s
        });
        html += "\n          </span>\n        </span>\n      </button>\n    </div>\n  </div>\n</div>\n";
        
        return html;
    });
});