define(["handlebars"], function(hb) {
    return hb.template(function(e, t, info, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"]; 
        info = this.merge(info, e.helpers); 
        s = s || {};
        
        var translatorFn = info.t || t && t.t;
        var missing = info.helperMissing;
        var esc = this.escapeExpression;
        var that = this;
        
        function dataHash(data) {
            return {
                hash: {},
                data: data
            };
        }
        
        function translateText(text) {
            if(translatorFn)
                text = translatorFn.call(t, text, dataHash(s));
            else
                text = missing.call(t, "t", text, dataHash(s));  
            return esc(text);
        }
        
        function conditionalHtml(op, text, data, each) {
            var fn = each ? info.each : info[op];
			var fnResult = fn.call(t, text, data);
        	if (fnResult || fnResult === 0) 
				return fnResult;
			return '';
		}
        
        function equalityHtml(op, text1, text2, data) {
            var result;
            if(op)   
                result = op.call(t, text1, text2, data);
            else
                result = missing.call(t, "if_eq", text1, text2, data);
                
            if (result || result === 0) 
				return result;
			return '';
        }
        
        function editAutomatedDevice(e, t) {
            return '\n          <i class="modal-icon glyphicon pencil"></i> ' + translateText("Edit") + '<span class="device-title"></span>\n        ';
            /*var s = {
                hash: {},
                data: t
            };
            var html = '\n          <i class="modal-icon glyphicon plus"></i> '; 
            
            var translate = (i.t || e && e.t);  // n
            var midText = '';
            if (translate)
                midText = translate.call(e, "Add {1} Library", '<span class="section-type-title"></span>', s);
            else
                midText = b.call(e, "t", "Add {1} Library", '<span class="section-type-title"></span>', s);
                
            html += w(midText);
            html += w((n = i.t || e && e.t, n ? n.call(e, "Add {1} Library", '<span class="section-type-title"></span>', s) : b.call(e, "t", "Add {1} Library", '<span class="section-type-title"></span>', s))) + "\n        ";
            return html;*/
        }

        function addAutomatedDevice(e, t) {
            return '\n          <i class="modal-icon glyphicon plus"></i> ' + translateText("Add Automated Device") + "\n        ";
        }

        function isSelected() {
            return " selected";
        }

        function selectType(e, t) {
            return "\n                " + translateText("Select type") + "\n              ";
            /*var n, s, a = "";
            return a += "\n                ", s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Select type", s) : b.call(e, "t", "Select type", s))) + "\n              "*/
        }

        function general(e, t) {
            return "\n                " + translateText("General") + "\n              ";
            /*var n, s, a = "";
            return a += "\n                ", s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "General", s) : b.call(e, "t", "General", s))) + "\n              "*/
        }

        function disabled() {
            return "disabled";
        }

        function create(e, t) {
            var html = '\n            <li>\n              <a class="btn-gray change-pane-btn create-btn';
            html += equalityHtml(info.if_eq || t && t.if_eq, t && t.selectedPane, "create", {
                hash: {},
                inverse: that.noop,
                fn: that.program(5, isSelected, t),
                data: t
            });
            html += conditionalHtml("if", t && t.add, {
                hash: {},
                inverse: that.noop,
                fn: that.program(7, disabled, s),
                data: s
            });
            html += ' hidden" data-pane="create" href="#">\n                <i class="modal-nav-icon glyphicon magic"></i>\n                ';
            html += translateText("Create") + "\n              </a>\n            </li>\n          ";
            
            return html;
                
            /*var n, s, a, r = "";
            return r += '\n            <li>\n              <a class="btn-gray change-pane-btn create-btn', a = {
                hash: {},
                inverse: x.noop,
                fn: x.program(5, o, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, e && e.selectedPane, "create", a) : b.call(e, "if_eq", e && e.selectedPane, "create", a), (s || 0 === s) && (r += s), r += " ", s = i["if"].call(e, e && e.add, {
                hash: {},
                inverse: x.noop,
                fn: x.program(11, d, t),
                data: t
            }), (s || 0 === s) && (r += s), r += ' hidden" data-pane="create" href="#">\n                <i class="modal-nav-icon glyphicon magic"></i>\n                ', a = {
                hash: {},
                data: t
            }, r += w((n = i.t || e && e.t, n ? n.call(e, "Create", a) : b.call(e, "t", "Create", a))) + "\n              </a>\n            </li>\n          "*/
        }

        function deleteDevice(e, t) {
            return '\n        <button type="button" class="delete-btn btn btn-danger pull-left" data-dismiss="modal">' + translateText("Delete Automated Device") + "</button>\n      ";
            /*var n, s, a = "";
            return a += '\n        <button type="button" class="delete-btn btn btn-danger pull-left" data-dismiss="modal">', s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Delete Library", s) : b.call(e, "t", "Delete Library", s))) + "</button>\n      "*/
        }

        function addDeviceText(e, t) {
            return "\n              " + translateText("Add Automated Device") + "\n            ";
            /*var n, s, a = "";
            return a += "\n              ", s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Add Library", s) : b.call(e, "t", "Add Library", s))) + "\n            "*/
        }

        function saveChanges(e, t) {
            return "\n              " + translateText("Save Changes") + "\n            ";
            /*var n, s, a = "";
            return a += "\n              ", s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Save Changes", s) : b.call(e, "t", "Save Changes", s))) + "\n            "*/
        }
        
        var html = '<div class="modal-dialog">\n  <div class="modal-content">\n    <div class="modal-header">\n      <button type="button" class="close" data-dismiss="modal"><i class="glyphicon remove-2"></i></button>\n      <h4 class="modal-title">\n        ';
        
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.program(3, editAutomatedDevice, s),
            fn: that.program(1, addAutomatedDevice, s),
            data: s
        });
        
        html += '\n      </h4>\n    </div>\n\n    <div class="modal-body modal-body-scroll modal-body-with-panes">\n      <div class="modal-nav-pane dark-scrollbar">\n        <ul class="list modal-nav-list">\n';
        
                // location button
        html+= '          <li>\n            <a class="btn-gray change-pane-btn locations-btn';
        html += equalityHtml(info.if_eq || t && t.if_eq, t && t.selectedPane, "locations", {
            hash: {},
            inverse: that.noop,
            fn: that.program(5, isSelected, s),
            data: s
        });
        html += " ";
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.noop,
            fn: that.program(11, disabled, s),
            data: s
        });
        
        html += '" data-pane="locations" href="#">\n              <i class="modal-nav-icon glyphicon folder-open"></i>\n              ';
        html += translateText("Add Location") + "\n            </a>\n          </li>\n\n          ";
        
        // Type button
        html += '          <li>\n            <a class="btn-gray change-pane-btn type-btn';       
        html += equalityHtml(info.if_eq || t && t.if_eq, t && t.selectedPane, "type", {
            hash: {},
            inverse: that.noop,
            fn: that.program(5, isSelected, s),
            data: s
        });

        html += '" data-pane="type" href="#">\n              <i class="modal-nav-icon glyphicon align-center"></i>\n              ';
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.program(9, general, s),
            fn: that.program(7, selectType, s),
            data: s
        });
        
        html += '\n            </a>\n          </li>\n\n';
                
        // hidden WTF button
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.noop,
            fn: that.program(13, create, s),
            data: s
        });
        
        // hardware button
        html += '\n\n          <li>\n            <a class="btn-gray change-pane-btn hardware-btn';
        html += equalityHtml(info.if_eq || t && t.if_eq, t && t.selectedPane, "hardware", {
            hash: {},
            inverse: that.noop,
            fn: that.program(5, isSelected, s),
            data: s
        });
        html += " ";
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.noop,
            fn: that.program(11, disabled, s),
            data: s
        });
        html += '" data-pane="hardware" href="#">\n              <i class="modal-nav-icon glyphicon cogwheel"></i>\n              ';
        html += translateText("Pick Hardware") + '\n            </a>\n          </li>\n        </ul>\n      </div>\n\n      <div class="modal-body-pane pane-region dark-scrollbar">\n      </div>\n    </div>\n\n    <div class="modal-footer">\n      ';
        
        html += conditionalHtml("if", t && t.edit, {
            hash: {},
            inverse: that.noop,
            fn: that.program(15, deleteDevice, s),
            data: s
        });
        html += '\n\n      <span class="form-message"></span>\n\n      <button type="button" class="cancel-btn btn btn-default" data-dismiss="modal">';
        html += translateText("Cancel") + '</button>\n      <button type="button" class="save-btn btn btn-primary btn-loading ';
        
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.noop,
            fn: that.program(11, disabled, s),
            data: s
        });
        html += '">\n        <div class="loading loading-sm"></div>\n        <span class="btn-label">\n          <span class="save-btn-label-next">';
        html += translateText("Next") + '</span>\n          <span class="save-btn-label-save">\n            ';
        
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.program(19, saveChanges, s),
            fn: that.program(17, addDeviceText, s),
            data: s
        });
        html += "\n          </span>\n        </span>\n      </button>\n    </div>\n  </div>\n</div>\n";
        
        return html;
        
        
        /*var f, g, v, y = "",
            b = i.helperMissing,
            w = this.escapeExpression,
            x = this;
        return y += '<div class="modal-dialog">\n  <div class="modal-content">\n    <div class="modal-header">\n      <button type="button" class="close" data-dismiss="modal"><i class="glyphicon remove-2"></i></button>\n      <h4 class="modal-title">\n        ', f = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.program(3, r, s),
            fn: x.program(1, a, s),
            data: s
        }), (f || 0 === f) && (y += f), y += '\n      </h4>\n    </div>\n\n    <div class="modal-body modal-body-scroll modal-body-with-panes">\n      <div class="modal-nav-pane dark-scrollbar">\n        <ul class="list modal-nav-list">\n          <li>\n            <a class="btn-gray change-pane-btn type-btn', v = {
            hash: {},
            inverse: x.noop,
            fn: x.program(5, o, s),
            data: s
        }, f = i.if_eq || t && t.if_eq, g = f ? f.call(t, t && t.selectedPane, "type", v) : b.call(t, "if_eq", t && t.selectedPane, "type", v), (g || 0 === g) && (y += g), y += '" data-pane="type" href="#">\n              <i class="modal-nav-icon glyphicon align-center"></i>\n              ', g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.program(9, c, s),
            fn: x.program(7, l, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '\n            </a>\n          </li>\n\n          <li>\n            <a class="btn-gray change-pane-btn locations-btn', v = {
            hash: {},
            inverse: x.noop,
            fn: x.program(5, o, s),
            data: s
        }, f = i.if_eq || t && t.if_eq, g = f ? f.call(t, t && t.selectedPane, "locations", v) : b.call(t, "if_eq", t && t.selectedPane, "locations", v), (g || 0 === g) && (y += g), y += " ", g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.noop,
            fn: x.program(11, d, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '" data-pane="locations" href="#">\n              <i class="modal-nav-icon glyphicon folder-open"></i>\n              ', v = {
            hash: {},
            data: s
        }, y += w((f = i.t || t && t.t, f ? f.call(t, "Add Location", v) : b.call(t, "t", "Add Location", v))) + "\n            </a>\n          </li>\n\n          ", g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.noop,
            fn: x.program(13, u, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '\n\n          <li>\n            <a class="btn-gray change-pane-btn hardware-btn', v = {
            hash: {},
            inverse: x.noop,
            fn: x.program(5, o, s),
            data: s
        }, f = i.if_eq || t && t.if_eq, g = f ? f.call(t, t && t.selectedPane, "hardware", v) : b.call(t, "if_eq", t && t.selectedPane, "hardware", v), (g || 0 === g) && (y += g), y += " ", g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.noop,
            fn: x.program(11, d, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '" data-pane="hardware" href="#">\n              <i class="modal-nav-icon glyphicon cogwheel"></i>\n              ', v = {
            hash: {},
            data: s
        }, y += w((f = i.t || t && t.t, f ? f.call(t, "Assign Hardware", v) : b.call(t, "t", "Assign Hardware", v))) + '\n            </a>\n          </li>\n        </ul>\n      </div>\n\n      <div class="modal-body-pane pane-region dark-scrollbar">\n      </div>\n    </div>\n\n    <div class="modal-footer">\n      ', g = i["if"].call(t, t && t.edit, {
            hash: {},
            inverse: x.noop,
            fn: x.program(15, h, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '\n\n      <span class="form-message"></span>\n\n      <button type="button" class="cancel-btn btn btn-default" data-dismiss="modal">', v = {
            hash: {},
            data: s
        }, y += w((f = i.t || t && t.t, f ? f.call(t, "Cancel", v) : b.call(t, "t", "Cancel", v))) + '</button>\n      <button type="button" class="save-btn btn btn-primary btn-loading ', g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.noop,
            fn: x.program(11, d, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '">\n        <div class="loading loading-sm"></div>\n        <span class="btn-label">\n          <span class="save-btn-label-next">', v = {
            hash: {},
            data: s
        }, y += w((f = i.t || t && t.t, f ? f.call(t, "Next", v) : b.call(t, "t", "Next", v))) + '</span>\n          <span class="save-btn-label-save">\n            ', g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.program(19, m, s),
            fn: x.program(17, p, s),
            data: s
        }), (g || 0 === g) && (y += g), y += "\n          </span>\n        </span>\n      </button>\n    </div>\n  </div>\n</div>\n"
        */
    });
});