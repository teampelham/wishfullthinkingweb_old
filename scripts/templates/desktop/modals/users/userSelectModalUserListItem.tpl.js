define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a() {
            return '\n  <div class="user-select-container">\n'
        }

        function r() {
            return '\n  <a class="user-select-container" href="#">\n'
        }

        function o() {
            return "\n  </div>\n"
        }

        function l() {
            return "\n  </a>\n"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var c, d = "",
            u = this;
        return c = i["if"].call(t, t && t.disabled, {
            hash: {},
            inverse: u.program(3, r, s),
            fn: u.program(1, a, s),
            data: s
        }), (c || 0 === c) && (d += c), d += '\n  <div class="user-select-top">\n    <div class="user-poster img-circle"></div>\n    <i class="protected-icon user-icon glyphicon lock"></i>\n    <i class="admin-icon user-icon glyphicon crown"></i>\n  </div>\n\n  <div class="username"></div>\n', c = i["if"].call(t, t && t.disabled, {
            hash: {},
            inverse: u.program(7, l, s),
            fn: u.program(5, o, s),
            data: s
        }), (c || 0 === c) && (d += c), d += "\n"
    });
});