define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        return o += '<div class="modal-header">\n  <h4 class="modal-title">\n    ', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Select User", r) : l.call(t, "t", "Select User", r))) + '\n\n    <span class="loading loading-sm"></span>\n  </h4>\n</div>\n\n<div class="modal-body"></div>\n'
    });
})