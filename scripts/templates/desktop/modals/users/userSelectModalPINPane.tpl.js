define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a() {
            return '\n      <a class="back-btn btn-gray" href="#"><i class="glyphicon chevron-left"></i></a>\n    '
        }

        function r(e, t) {
            var n, s, a = "";
            return a += '\n      <i class="modal-icon glyphicon pencil"></i>\n      ', s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Edit PIN", s) : b.call(e, "t", "Edit PIN", s))) + "\n    "
        }

        function o(e, t) {
            var n;
            return n = i["if"].call(e, e && e.isEditConfirmation, {
                hash: {},
                inverse: x.program(8, c, t),
                fn: x.program(6, l, t),
                data: t
            }), n || 0 === n ? n : ""
        }

        function l(e, t) {
            var n, s, a = "";
            return a += "\n      ", s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Confirm Current PIN", s) : b.call(e, "t", "Confirm Current PIN", s))) + "\n    "
        }

        function c(e, t) {
            var n, s, a = "";
            return a += "\n      ", s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Enter PIN", s) : b.call(e, "t", "Enter PIN", s))) + "\n    "
        }

        function d() {
            return "number"
        }

        function u() {
            return "password"
        }

        function h(e, t) {
            var n, s, a = "";
            return a += '\n      <p class="help-block">\n        ', s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "You do not currently have a PIN. This will allow any users in this home to switch to your account.", s) : b.call(e, "t", "You do not currently have a PIN. This will allow any users in this home to switch to your account.", s))) + "\n      </p>\n    "
        }

        function p(e, t) {
            var n, s, a = "";
            return a += '\n    <div class="modal-footer">\n      ', n = i["if"].call(e, e && e.hasCurrentPIN, {
                hash: {},
                inverse: x.noop,
                fn: x.program(17, m, t),
                data: t
            }), (n || 0 === n) && (a += n), a += "\n\n      ", n = i["if"].call(e, e && e.isEditWarning, {
                hash: {},
                inverse: x.program(21, g, t),
                fn: x.program(19, f, t),
                data: t
            }), (n || 0 === n) && (a += n), a += '\n\n      <button type="submit" class="save-btn btn-link">', s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Save", s) : b.call(e, "t", "Save", s))) + "</button>\n    </div>\n  "
        }

        function m(e, t) {
            var n, s, a = "";
            return a += '\n        <button type="button" class="remove-btn btn-gray pull-left">', s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Remove", s) : b.call(e, "t", "Remove", s))) + "</button>\n      "
        }

        function f(e, t) {
            var n, s, a = "";
            return a += '\n        <button type="button" class="skip-btn btn-gray" data-dismiss="modal">', s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Skip", s) : b.call(e, "t", "Skip", s))) + "</button>\n      "
        }

        function g(e, t) {
            var n, s, a = "";
            return a += '\n        <button type="button" class="cancel-btn btn-gray" data-dismiss="modal">', s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Cancel", s) : b.call(e, "t", "Cancel", s))) + "</button>\n      "
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var v, y = "",
            b = i.helperMissing,
            w = this.escapeExpression,
            x = this;
        return y += '<div class="modal-header">\n  <div class="home-user-dropdown-container pull-right"></div>\n\n  <h4 class="modal-title">\n    ', v = i["if"].call(t, t && t.showBackButton, {
            hash: {},
            inverse: x.noop,
            fn: x.program(1, a, s),
            data: s
        }), (v || 0 === v) && (y += v), y += "\n\n    ", v = i["if"].call(t, t && t.isEditing, {
            hash: {},
            inverse: x.program(5, o, s),
            fn: x.program(3, r, s),
            data: s
        }), (v || 0 === v) && (y += v), y += '\n\n    <span class="loading loading-sm hidden"></span>\n  </h4>\n</div>\n\n<form id="pin-form">\n  <div class="modal-body">\n    <ul class="tile-list user-select-list disabled"></ul>\n\n    <div class="pin-inputs-container">\n      <input type="', v = i["if"].call(t, t && t.isEditing, {
            hash: {},
            inverse: x.program(12, u, s),
            fn: x.program(10, d, s),
            data: s
        }), (v || 0 === v) && (y += v), y += '" pattern="\\d*" class="form-control">\n      <input type="', v = i["if"].call(t, t && t.isEditing, {
            hash: {},
            inverse: x.program(12, u, s),
            fn: x.program(10, d, s),
            data: s
        }), (v || 0 === v) && (y += v), y += '" pattern="\\d*" class="form-control">\n      <input type="', v = i["if"].call(t, t && t.isEditing, {
            hash: {},
            inverse: x.program(12, u, s),
            fn: x.program(10, d, s),
            data: s
        }), (v || 0 === v) && (y += v), y += '" pattern="\\d*" class="form-control">\n      <input type="', v = i["if"].call(t, t && t.isEditing, {
            hash: {},
            inverse: x.program(12, u, s),
            fn: x.program(10, d, s),
            data: s
        }), (v || 0 === v) && (y += v), y += '" pattern="\\d*" class="form-control">\n    </div>\n\n    ', v = i["if"].call(t, t && t.isEditWarning, {
            hash: {},
            inverse: x.noop,
            fn: x.program(14, h, s),
            data: s
        }), (v || 0 === v) && (y += v), y += "\n  </div>\n\n  ", v = i["if"].call(t, t && t.isEditing, {
            hash: {},
            inverse: x.noop,
            fn: x.program(16, p, s),
            data: s
        }), (v || 0 === v) && (y += v), y += "\n</form>\n"
    })
});