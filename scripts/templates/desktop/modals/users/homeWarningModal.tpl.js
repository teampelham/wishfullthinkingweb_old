define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        return o += '<div class="modal-dialog">\n  <div class="modal-content">\n    <div class="modal-header">\n      <button type="button" class="close" data-dismiss="modal"><i class="glyphicon remove-2"></i></button>\n      <h4 class="modal-title"><i class="modal-icon glyphicon circle-info"></i> ', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Welcome to Plex Home", r) : l.call(t, "t", "Welcome to Plex Home", r))) + '</h4>\n    </div>\n\n    <div class="modal-body modal-body-scroll dark-scrollbar">\n      <p>', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "{1}, so we've made a few changes to help keep your media safe.", t && t.intro, r) : l.call(t, "t", "{1}, so we've made a few changes to help keep your media safe.", t && t.intro, r))) + "</p>\n\n      <ol>\n        <li>", r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "We've increased the security requirements on this server, so anything communicating with it will need to be signed in to a Plex account.", r) : l.call(t, "t", "We've increased the security requirements on this server, so anything communicating with it will need to be signed in to a Plex account.", r))) + "</li>\n\n        <li>", r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "We've disabled DLNA to prevent any DLNA-enabled device from browsing and playing media without a password or PIN. You can change this preference in your {1}server settings{2}.", "<a class='server-settings-btn' href='#'>", "</a>", r) : l.call(t, "t", "We've disabled DLNA to prevent any DLNA-enabled device from browsing and playing media without a password or PIN. You can change this preference in your {1}server settings{2}.", "<a class='server-settings-btn' href='#'>", "</a>", r))) + "</li>\n      </ol>\n\n      <p>", r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "For more information on Plex Home security settings, please see the {1}Plex Home section{2} on our support site.", "<a href='https://support.plex.tv/hc/en-us/sections/200641063' target='_blank'>", "</a>", r) : l.call(t, "t", "For more information on Plex Home security settings, please see the {1}Plex Home section{2} on our support site.", "<a href='https://support.plex.tv/hc/en-us/sections/200641063' target='_blank'>", "</a>", r))) + '</p>\n    </div>\n\n    <div class="modal-footer">\n      <button type="button" class="btn btn-lg btn-primary" data-dismiss="modal">', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "OK", r) : l.call(t, "t", "OK", r))) + "</button>\n    </div>\n  </div>\n</div>\n"
    });
});