define(["handlebars"], function(hb) {
	
    return hb.template(function(e, t, i, n, s) {
        /*function a(e, t) {
            var n, s = "";
            return s += '\n        <h5 class="keyboard-shortcuts-header">', (n = i.label) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.label, n = typeof n === f ? n.call(e, {
                hash: {},
                data: t
            }) : n), s += g(n) + '</h5>\n\n        <ul class="list keyboard-shortcuts-list">\n          ', n = i.each.call(e, e && e.shortcuts, {
                hash: {},
                inverse: v.noop,
                fn: v.program(2, r, t),
                data: t
            }), (n || 0 === n) && (s += n), s += "\n        </ul>\n      "
        }

        function r(e, t) {
            var n, s, a = "";
            return a += '\n            <li>\n              <span class="keyboard-shortcuts-keys">\n                ', n = i.each.call(e, e && e.keys, {
                hash: {},
                inverse: v.noop,
                fn: v.program(3, o, t),
                data: t
            }), (n || 0 === n) && (a += n), a += '\n              </span>\n              <span class="keyboard-shortcuts-label text-muted">', s = {
                hash: {},
                data: t
            }, a += g((n = i.t || e && e.t, n ? n.call(e, e && e.label, s) : y.call(e, "t", e && e.label, s))) + "</span>\n            </li>\n          "
        }

        function o(e, t) {
            var n, s = "";
            return s += "\n\n                  ", n = i["if"].call(e, e && e.plain, {
                hash: {},
                inverse: v.noop,
                fn: v.program(4, l, t),
                data: t
            }), (n || 0 === n) && (s += n), s += "\n\n                  ", n = i.unless.call(e, e && e.plain, {
                hash: {},
                inverse: v.noop,
                fn: v.program(6, c, t),
                data: t
            }), (n || 0 === n) && (s += n), s += "\n\n                  ", n = i["if"].call(e, e && e.split, {
                hash: {},
                inverse: v.noop,
                fn: v.program(8, d, t),
                data: t
            }), (n || 0 === n) && (s += n), s += "\n\n                "
        }

        function l(e, t) {
            var n, s = "";
            return s += '\n                    <span class="keyboard-shortcuts-keys-plain">', (n = i.s) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.s, n = typeof n === f ? n.call(e, {
                hash: {},
                data: t
            }) : n), s += g(n) + "</span>\n                  "
        }

        function c(e, t) {
            var n, s = "";
            return s += '\n                    <span class="keyboard-shortcuts-keys-key">', (n = i.s) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.s, n = typeof n === f ? n.call(e, {
                hash: {},
                data: t
            }) : n), s += g(n) + "</span>\n                  "
        }

        function d() {
            return "\n                    <br>\n                  "
        }*/
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
		console.warn('keyboardModal');
		return '<div class="modal-dialog"></div>';
        /*var u, h, p, m = "",
            f = "function",
            g = this.escapeExpression,
            v = this,
            y = i.helperMissing;
        return m += '<div class="modal-dialog">\n  <div class="modal-content">\n    <div class="modal-header">\n      <button type="button" class="close" data-dismiss="modal"><i class="glyphicon remove-2"></i></button>\n      <h4 class="modal-title">\n        <i class="modal-icon glyphicon keyboard-wireless"></i>\n        ', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Keyboard Shortcuts", p) : y.call(t, "t", "Keyboard Shortcuts", p))) + '\n      </h4>\n    </div>\n\n    <div class="modal-body modal-body-scroll dark-scrollbar">\n      ', h = i.each.call(t, t && t.groups, {
            hash: {},
            inverse: v.noop,
            fn: v.program(1, a, s),
            data: s
        }), (h || 0 === h) && (m += h), m += "\n    </div>\n  </div>\n</div>\n"*/
    });
});