define(["handlebars"], function(hb) {
    
	return hb.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        return o += '<div class="modal-dialog">\n  <div class="modal-content">\n    <div class="modal-header">\n      <button type="button" class="close" data-dismiss="modal"><i class="glyphicon remove-2"></i></button>\n      <h4 class="modal-title"><i class="modal-icon glyphicon circle-info"></i> <span id="title">', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Media Server Detection", r) : l.call(t, "t", "Media Server Detection", r))) + '</span></h4>\n    </div>\n\n    <div class="modal-body">\n      <div class="loading loading-inline"></div>\n\n      <div class="server-found hidden">\n        ', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "We've detected a Plex Media Server running on this computer, but we can't connect to it. {1}Sign in to your account{2} on the server so that you can access it.", "<a href='http://127.0.0.1:32400/web/index.html#!/settings/server' target='_blank'>", "</a>", r) : l.call(t, "t", "We've detected a Plex Media Server running on this computer, but we can't connect to it. {1}Sign in to your account{2} on the server so that you can access it.", "<a href='http://127.0.0.1:32400/web/index.html#!/settings/server' target='_blank'>", "</a>", r))) + '\n      </div>\n\n      <div class="server-not-found hidden">\n        <p>', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "We were unable to find a Plex Media Server running on this computer. If you have one on this or another computer, make sure it's running and signed in to your account.", r) : l.call(t, "t", "We were unable to find a Plex Media Server running on this computer. If you have one on this or another computer, make sure it's running and signed in to your account.", r))) + "</p>\n        <p>", r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "For more information, please see {1}this support article{2}.", "<a href='http://help.plex.tv/noserver' target='_blank'>", "</a>", r) : l.call(t, "t", "For more information, please see {1}this support article{2}.", "<a href='http://help.plex.tv/noserver' target='_blank'>", "</a>", r))) + '</p>\n      </div>\n    </div>\n\n    <div class="modal-footer">\n      <button type="button" class="confirm-btn btn btn-lg btn-primary">', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "OK", r) : l.call(t, "t", "OK", r))) + "</button>\n    </div>\n  </div>\n</div>\n"
    });
});