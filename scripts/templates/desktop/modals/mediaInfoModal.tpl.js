define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a() {
            return "hide-streams-column"
        }

        function r(e, t) {
            var n, s, a, r = "";
            return r += '\n          <div class="files">\n            <h4>', a = {
                hash: {},
                data: t
            }, r += N((n = i.t || e && e.t, n ? n.call(e, "Files", a) : O.call(e, "t", "Files", a))) + "</h4>\n            ", s = i.each.call(e, e && e.media, {
                hash: {},
                inverse: V.noop,
                fn: V.programWithDepth(4, o, t, e),
                data: t
            }), (s || 0 === s) && (r += s), r += "\n          </div>\n        "
        }

        function o(e, t, n) {
            var s, a = "";
            return a += '\n              <ul class="media-info-file-list well">\n                ', s = i["if"].call(e, n && n.showDeleteFile, {
                hash: {},
                inverse: V.noop,
                fn: V.program(5, l, t),
                data: t
            }), (s || 0 === s) && (a += s), a += "\n                ", s = i.each.call(e, e && e.Part, {
                hash: {},
                inverse: V.noop,
                fn: V.programWithDepth(10, u, t, n),
                data: t
            }), (s || 0 === s) && (a += s), a += "\n              </ul>\n            "
        }

        function l(e, t) {
            var n, s, a, r = "";
            return r += '\n                  <a href="#" data-media-item="' + N((n = t, n = null == n || n === !1 ? n : n.index, typeof n === F ? n.apply(e) : n)) + '" class="delete-btn text-danger pull-right">\n                    <i class="glyphicon bin"></i>\n                    ', a = {
                hash: {},
                inverse: V.program(8, d, t),
                fn: V.program(6, c, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, (n = e && e.Part, null == n || n === !1 ? n : n.length), "1", a) : O.call(e, "if_eq", (n = e && e.Part, null == n || n === !1 ? n : n.length), "1", a), (s || 0 === s) && (r += s), r += "\n                  </a>\n                "
        }

        function c(e, t) {
            var n, s, a = "";
            return a += "\n                      ", s = {
                hash: {},
                data: t
            }, a += N((n = i.t || e && e.t, n ? n.call(e, "Delete File", s) : O.call(e, "t", "Delete File", s))) + "\n                    "
        }

        function d(e, t) {
            var n, s, a = "";
            return a += "\n                      ", s = {
                hash: {},
                data: t
            }, a += N((n = i.t || e && e.t, n ? n.call(e, "Delete Files", s) : O.call(e, "t", "Delete Files", s))) + "\n                    "
        }

        function u(e, t, n) {
            var s, a, r, o = "";
            return o += "\n                  <li>\n                    ", s = i["if"].call(e, n && n.showFilepath, {
                hash: {},
                inverse: V.program(13, p, t),
                fn: V.program(11, h, t),
                data: t
            }), (s || 0 === s) && (o += s), o += "\n\n                    ", r = {
                hash: {},
                inverse: V.noop,
                fn: V.program(15, m, t),
                data: t
            }, s = i.if_neq || e && e.if_neq, a = s ? s.call(e, e && e.accessible, "1", r) : O.call(e, "if_neq", e && e.accessible, "1", r), (a || 0 === a) && (o += a), o += "\n                  </li>\n                "
        }

        function h(e, t) {
            var n, s = "";
            return s += "\n                      ", (n = i.file) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.file, n = typeof n === F ? n.call(e, {
                hash: {},
                data: t
            }) : n), s += N(n) + "\n                    "
        }

        function p(e, t) {
            var n, s, a = "";
            return a += "\n                      ", s = {
                hash: {},
                data: t
            }, a += N((n = i.getLastSegment || e && e.getLastSegment, n ? n.call(e, e && e.file, s) : O.call(e, "getLastSegment", e && e.file, s))) + "\n                    "
        }

        function m(e, t) {
            var n, s, a, r = "";
            return r += "\n                      ", a = {
                hash: {},
                inverse: V.program(18, g, t),
                fn: V.program(16, f, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, e && e.exists, "1", a) : O.call(e, "if_eq", e && e.exists, "1", a), (s || 0 === s) && (r += s), r += "\n                    "
        }

        function f(e, t) {
            var n, s, a = "";
            return a += '\n                        <span class="label label-danger" data-toggle="tooltip" title="', s = {
                hash: {},
                data: t
            }, a += N((n = i.t || e && e.t, n ? n.call(e, "Please check the permissions for this file.", s) : O.call(e, "t", "Please check the permissions for this file.", s))) + '">', s = {
                hash: {},
                data: t
            }, a += N((n = i.t || e && e.t, n ? n.call(e, "Inaccessible", s) : O.call(e, "t", "Inaccessible", s))) + "</span>\n                      "
        }

        function g(e, t) {
            var n, s, a = "";
            return a += '\n                        <span class="label label-danger" data-toggle="tooltip" title="', s = {
                hash: {},
                data: t
            }, a += N((n = i.t || e && e.t, n ? n.call(e, "Please check that this file exists and the necessary drive is mounted.", s) : O.call(e, "t", "Please check that this file exists and the necessary drive is mounted.", s))) + '">', s = {
                hash: {},
                data: t
            }, a += N((n = i.t || e && e.t, n ? n.call(e, "Unavailable", s) : O.call(e, "t", "Unavailable", s))) + "</span>\n                      "
        }

        function v(e, t, n) {
            var s, a, r, o = "";
            return o += '\n          <div class="media">\n            <div class="props">\n              <h4>', r = {
                hash: {},
                data: t
            }, o += N((s = i.t || e && e.t, s ? s.call(e, "Media", r) : O.call(e, "t", "Media", r))) + "</h4>\n              <ul>\n                ", a = i.each.call(e, e && e.properties, {
                hash: {},
                inverse: V.noop,
                fn: V.program(21, y, t),
                data: t
            }), (a || 0 === a) && (o += a), o += "\n              </ul>\n            </div>\n\n            ", a = i.each.call(e, e && e.Part, {
                hash: {},
                inverse: V.noop,
                fn: V.programWithDepth(23, b, t, n),
                data: t
            }), (a || 0 === a) && (o += a), o += "\n          </div>\n        "
        }

        function y(e, t) {
            var n, s, a, r = "";
            return r += '\n                  <li>\n                    <span class="detail-label">', a = {
                hash: {},
                data: t
            }, r += N((n = i.t || e && e.t, n ? n.call(e, e && e.name, a) : O.call(e, "t", e && e.name, a))) + '</span>\n                    <span class="detail">', (s = i.value) ? s = s.call(e, {
                hash: {},
                data: t
            }) : (s = e && e.value, s = typeof s === F ? s.call(e, {
                hash: {},
                data: t
            }) : s), r += N(s) + "</span>\n                  </li>\n                "
        }

        function b(e, t, n) {
            var s, a, r, o = "";
            return o += '\n              <div class="part">\n                <div class="props">\n                  <h4>', r = {
                hash: {},
                data: t
            }, o += N((s = i.t || e && e.t, s ? s.call(e, "Part", r) : O.call(e, "t", "Part", r))) + "</h4>\n                  <ul>\n                    ", a = i.each.call(e, e && e.properties, {
                hash: {},
                inverse: V.noop,
                fn: V.program(24, w, t),
                data: t
            }), (a || 0 === a) && (o += a), o += '\n                  </ul>\n                </div>\n\n                <div class="streams">\n                  ', a = i.each.call(e, e && e.Stream, {
                hash: {},
                inverse: V.noop,
                fn: V.program(26, x, t),
                data: t
            }), (a || 0 === a) && (o += a), o += "\n                  ", a = i["if"].call(e, n && n.loading, {
                hash: {},
                inverse: V.noop,
                fn: V.program(37, T, t),
                data: t
            }), (a || 0 === a) && (o += a), o += "\n                </div>\n              </div>\n            "
        }

        function w(e, t) {
            var n, s, a, r = "";
            return r += '\n                      <li>\n                        <span class="detail-label">', a = {
                hash: {},
                data: t
            }, r += N((n = i.t || e && e.t, n ? n.call(e, e && e.name, a) : O.call(e, "t", e && e.name, a))) + '</span>\n                        <span class="detail">', (s = i.value) ? s = s.call(e, {
                hash: {},
                data: t
            }) : (s = e && e.value, s = typeof s === F ? s.call(e, {
                hash: {},
                data: t
            }) : s), r += N(s) + "</span>\n                      </li>\n                    "
        }

        function x(e, t) {
            var n, s, a, r = "";
            return r += '\n                    <div class="props">\n                      ', a = {
                hash: {},
                inverse: V.program(29, S, t),
                fn: V.program(27, k, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, e && e.streamType, "1", a) : O.call(e, "if_eq", e && e.streamType, "1", a), (s || 0 === s) && (r += s), r += "\n\n                      <ul>\n                        ", s = i.each.call(e, e && e.properties, {
                hash: {},
                inverse: V.noop,
                fn: V.program(35, _, t),
                data: t
            }), (s || 0 === s) && (r += s), r += "\n                      </ul>\n                    </div>\n                  "
        }

        function k(e, t) {
            var n, s, a = "";
            return a += "\n                        <h4>", s = {
                hash: {},
                data: t
            }, a += N((n = i.t || e && e.t, n ? n.call(e, "Video", s) : O.call(e, "t", "Video", s))) + "</h4>\n                      "
        }

        function S(e, t) {
            var n, s, a;
            return a = {
                hash: {},
                inverse: V.program(32, P, t),
                fn: V.program(30, C, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, e && e.streamType, "2", a) : O.call(e, "if_eq", e && e.streamType, "2", a), s || 0 === s ? s : ""
        }

        function C(e, t) {
            var n, s, a = "";
            return a += "\n                        <h4>", s = {
                hash: {},
                data: t
            }, a += N((n = i.t || e && e.t, n ? n.call(e, "Audio", s) : O.call(e, "t", "Audio", s))) + "</h4>\n                      "
        }

        function P(e, t) {
            var n, s, a;
            return a = {
                hash: {},
                inverse: V.noop,
                fn: V.program(33, A, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, e && e.streamType, "3", a) : O.call(e, "if_eq", e && e.streamType, "3", a), s || 0 === s ? s : ""
        }

        function A(e, t) {
            var n, s, a = "";
            return a += "\n                        <h4>", s = {
                hash: {},
                data: t
            }, a += N((n = i.t || e && e.t, n ? n.call(e, "Subtitles", s) : O.call(e, "t", "Subtitles", s))) + "</h4>\n                      "
        }

        function _(e, t) {
            var n, s, a, r = "";
            return r += '\n                          <li>\n                            <span class="detail-label">', a = {
                hash: {},
                data: t
            }, r += N((n = i.t || e && e.t, n ? n.call(e, e && e.name, a) : O.call(e, "t", e && e.name, a))) + '</span>\n                            <span class="detail">', (s = i.value) ? s = s.call(e, {
                hash: {},
                data: t
            }) : (s = e && e.value, s = typeof s === F ? s.call(e, {
                hash: {},
                data: t
            }) : s), r += N(s) + "</span>\n                          </li>\n                        "
        }

        function T(e, t) {
            var n, s, a = "";
            return a += '\n                    <div class="props">\n                      <h4>', s = {
                hash: {},
                data: t
            }, a += N((n = i.t || e && e.t, n ? n.call(e, "Loading", s) : O.call(e, "t", "Loading", s))) + "...</h4>\n                    </div>\n                  "
        }

        function I(e, t, n) {
            var s;
            return s = i["if"].call(e, n && n.showFilepath, {
                hash: {},
                inverse: V.noop,
                fn: V.program(40, M, t),
                data: t
            }), s || 0 === s ? s : ""
        }

        function M(e, t) {
            var n, s, a = "";
            return a += '\n      <div class="modal-footer">\n        <div class="pull-left">\n          <a href="', (n = i.url) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.url, n = typeof n === F ? n.call(e, {
                hash: {},
                data: t
            }) : n), a += N(n) + '" target="_blank">', s = {
                hash: {},
                data: t
            }, a += N((n = i.t || e && e.t, n ? n.call(e, "View XML", s) : O.call(e, "t", "View XML", s))) + "</a>\n        </div>\n      </div>\n    "
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var D, E, L, R = "",
            O = i.helperMissing,
            N = this.escapeExpression,
            F = "function",
            V = this;
        return R += '<div class="modal-dialog">\n  <div class="modal-content">\n    <div class="modal-header">\n      <button type="button" class="close" data-dismiss="modal"><i class="glyphicon remove-2"></i></button>\n      <h4 class="modal-title"><i class="modal-icon glyphicon circle-info"></i> ', L = {
            hash: {},
            data: s
        }, R += N((D = i.t || t && t.t, D ? D.call(t, "Media Info", L) : O.call(t, "t", "Media Info", L))) + '</h4>\n    </div>\n\n    <div class="modal-body modal-body-scroll dark-scrollbar">\n      <div class="position-ref ', E = i.unless.call(t, t && t.hasStreams, {
            hash: {},
            inverse: V.noop,
            fn: V.program(1, a, s),
            data: s
        }), (E || 0 === E) && (R += E), R += '">\n        ', E = i["if"].call(t, t && t.hasFiles, {
            hash: {},
            inverse: V.noop,
            fn: V.program(3, r, s),
            data: s
        }), (E || 0 === E) && (R += E), R += "\n\n        ", E = i.each.call(t, t && t.media, {
            hash: {},
            inverse: V.noop,
            fn: V.programWithDepth(20, v, s, t),
            data: s
        }), (E || 0 === E) && (R += E), R += "\n      </div>\n    </div>\n\n    ", E = i["if"].call(t, t && t.key, {
            hash: {},
            inverse: V.noop,
            fn: V.programWithDepth(39, I, s, t),
            data: s
        }), (E || 0 === E) && (R += E), R += "\n  </div>\n</div>\n"
    })
});