define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a = "";
            return a += "\n          ", s = {
                hash: {},
                data: t
            }, a += y((n = i.t || e && e.t, n ? n.call(e, "Internet Required", s) : v.call(e, "t", "Internet Required", s))) + "\n        "
        }

        function r(e, t) {
            var n, s, a = "";
            return a += "\n          ", s = {
                hash: {},
                data: t
            }, a += y((n = i.t || e && e.t, n ? n.call(e, "Plex Pass Required", s) : v.call(e, "t", "Plex Pass Required", s))) + "\n        "
        }

        function o(e, t) {
            var n, s, a = "";
            return a += "\n          ", s = {
                hash: {},
                data: t
            }, a += y((n = i.t || e && e.t, n ? n.call(e, "This server cannot reach the Internet. Please verify connectivity and try again.", s) : v.call(e, "t", "This server cannot reach the Internet. Please verify connectivity and try again.", s))) + "\n        "
        }

        function l(e, t) {
            var n, s, a = "";
            return a += "\n          ", s = {
                hash: {},
                data: t
            }, a += y((n = i.t || e && e.t, n ? n.call(e, "This library requires your server be signed in to a Plex Pass account.", s) : v.call(e, "t", "This library requires your server be signed in to a Plex Pass account.", s))) + "\n        "
        }

        function c(e, t) {
            var n, s, a = "";
            return a += '\n        <button type="button" class="confirm-btn btn btn-lg btn-primary">', s = {
                hash: {},
                data: t
            }, a += y((n = i.t || e && e.t, n ? n.call(e, "OK", s) : v.call(e, "t", "OK", s))) + "</button>\n      "
        }

        function d(e, t) {
            var n, s = "";
            return s += '\n        <a class="confirm-btn btn btn-lg btn-primary" href="#!/settings/server/', (n = i.serverID) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.serverID, n = typeof n === b ? n.call(e, {
                hash: {},
                data: t
            }) : n), s += y(n) + '">\n          ', n = i["if"].call(e, e && e.isSignedIn, {
                hash: {},
                inverse: w.program(14, h, t),
                fn: w.program(12, u, t),
                data: t
            }), (n || 0 === n) && (s += n), s += "\n        </a>\n      "
        }

        function u(e, t) {
            var n, s, a = "";
            return a += "\n            ", s = {
                hash: {},
                data: t
            }, a += y((n = i.t || e && e.t, n ? n.call(e, "Settings", s) : v.call(e, "t", "Settings", s))) + "\n          "
        }

        function h(e, t) {
            var n, s, a = "";
            return a += "\n            ", s = {
                hash: {},
                data: t
            }, a += y((n = i.t || e && e.t, n ? n.call(e, "Sign In", s) : v.call(e, "t", "Sign In", s))) + "\n          "
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var p, m, f, g = "",
            v = i.helperMissing,
            y = this.escapeExpression,
            b = "function",
            w = this;
        return g += '<div class="modal-dialog">\n  <div class="modal-content">\n    <div class="modal-header">\n      <button type="button" class="close" data-dismiss="modal"><i class="glyphicon remove-2"></i></button>\n      <h4 class="modal-title">\n        <i class="modal-icon glyphicon circle-exclamation-mark"></i>\n        ', p = i["if"].call(t, t && t.hasPlexPass, {
            hash: {},
            inverse: w.program(3, r, s),
            fn: w.program(1, a, s),
            data: s
        }), (p || 0 === p) && (g += p), g += '\n      </h4>\n    </div>\n\n    <div class="modal-body">\n      <p class="confirm-message">\n        ', p = i["if"].call(t, t && t.hasPlexPass, {
            hash: {},
            inverse: w.program(7, l, s),
            fn: w.program(5, o, s),
            data: s
        }), (p || 0 === p) && (g += p), g += '\n      </p>\n    </div>\n\n    <div class="modal-footer">\n      <button type="button" class="cancel-btn btn btn-lg btn-default" data-dismiss="modal">', f = {
            hash: {},
            data: s
        }, g += y((p = i.t || t && t.t, p ? p.call(t, "Ignore", f) : v.call(t, "t", "Ignore", f))) + "</button>\n      ", m = i["if"].call(t, t && t.hasPlexPass, {
            hash: {},
            inverse: w.program(11, d, s),
            fn: w.program(9, c, s),
            data: s
        }), (m || 0 === m) && (g += m), g += "\n    </div>\n  </div>\n</div>\n"
    })
});