define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a = "";
            return a += '\n          <i class="modal-icon glyphicon plus"></i> ', s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Add {1} Library", '<span class="section-type-title"></span>', s) : b.call(e, "t", "Add {1} Library", '<span class="section-type-title"></span>', s))) + "\n        "
        }

        function r(e, t) {
            var n, s, a = "";
            return a += '\n          <i class="modal-icon glyphicon pencil"></i> ', s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Edit Library", s) : b.call(e, "t", "Edit Library", s))) + "\n        "
        }

        function o() {
            return " selected"
        }

        function l(e, t) {
            var n, s, a = "";
            return a += "\n                ", s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Select type", s) : b.call(e, "t", "Select type", s))) + "\n              "
        }

        function c(e, t) {
            var n, s, a = "";
            return a += "\n                ", s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "General", s) : b.call(e, "t", "General", s))) + "\n              "
        }

        function d() {
            return "disabled"
        }

        function u(e, t) {
            var n, s, a, r = "";
            return r += '\n            <li>\n              <a class="btn-gray change-pane-btn create-btn', a = {
                hash: {},
                inverse: x.noop,
                fn: x.program(5, o, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, e && e.selectedPane, "create", a) : b.call(e, "if_eq", e && e.selectedPane, "create", a), (s || 0 === s) && (r += s), r += " ", s = i["if"].call(e, e && e.add, {
                hash: {},
                inverse: x.noop,
                fn: x.program(11, d, t),
                data: t
            }), (s || 0 === s) && (r += s), r += ' hidden" data-pane="create" href="#">\n                <i class="modal-nav-icon glyphicon magic"></i>\n                ', a = {
                hash: {},
                data: t
            }, r += w((n = i.t || e && e.t, n ? n.call(e, "Create", a) : b.call(e, "t", "Create", a))) + "\n              </a>\n            </li>\n          "
        }

        function h(e, t) {
            var n, s, a = "";
            return a += '\n        <button type="button" class="delete-btn btn btn-danger pull-left" data-dismiss="modal">', s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Delete Library", s) : b.call(e, "t", "Delete Library", s))) + "</button>\n      "
        }

        function p(e, t) {
            var n, s, a = "";
            return a += "\n              ", s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Add Library", s) : b.call(e, "t", "Add Library", s))) + "\n            "
        }

        function m(e, t) {
            var n, s, a = "";
            return a += "\n              ", s = {
                hash: {},
                data: t
            }, a += w((n = i.t || e && e.t, n ? n.call(e, "Save Changes", s) : b.call(e, "t", "Save Changes", s))) + "\n            "
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var f, g, v, y = "",
            b = i.helperMissing,
            w = this.escapeExpression,
            x = this;
        return y += '<div class="modal-dialog">\n  <div class="modal-content">\n    <div class="modal-header">\n      <button type="button" class="close" data-dismiss="modal"><i class="glyphicon remove-2"></i></button>\n      <h4 class="modal-title">\n        ', f = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.program(3, r, s),
            fn: x.program(1, a, s),
            data: s
        }), (f || 0 === f) && (y += f), y += '\n      </h4>\n    </div>\n\n    <div class="modal-body modal-body-scroll modal-body-with-panes">\n      <div class="modal-nav-pane dark-scrollbar">\n        <ul class="list modal-nav-list">\n          <li>\n            <a class="btn-gray change-pane-btn type-btn', v = {
            hash: {},
            inverse: x.noop,
            fn: x.program(5, o, s),
            data: s
        }, f = i.if_eq || t && t.if_eq, g = f ? f.call(t, t && t.selectedPane, "type", v) : b.call(t, "if_eq", t && t.selectedPane, "type", v), (g || 0 === g) && (y += g), y += '" data-pane="type" href="#">\n              <i class="modal-nav-icon glyphicon align-center"></i>\n              ', g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.program(9, c, s),
            fn: x.program(7, l, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '\n            </a>\n          </li>\n\n          <li>\n            <a class="btn-gray change-pane-btn folders-btn', v = {
            hash: {},
            inverse: x.noop,
            fn: x.program(5, o, s),
            data: s
        }, f = i.if_eq || t && t.if_eq, g = f ? f.call(t, t && t.selectedPane, "folders", v) : b.call(t, "if_eq", t && t.selectedPane, "folders", v), (g || 0 === g) && (y += g), y += " ", g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.noop,
            fn: x.program(11, d, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '" data-pane="folders" href="#">\n              <i class="modal-nav-icon glyphicon folder-open"></i>\n              ', v = {
            hash: {},
            data: s
        }, y += w((f = i.t || t && t.t, f ? f.call(t, "Add folders", v) : b.call(t, "t", "Add folders", v))) + "\n            </a>\n          </li>\n\n          ", g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.noop,
            fn: x.program(13, u, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '\n\n          <li>\n            <a class="btn-gray change-pane-btn advanced-btn', v = {
            hash: {},
            inverse: x.noop,
            fn: x.program(5, o, s),
            data: s
        }, f = i.if_eq || t && t.if_eq, g = f ? f.call(t, t && t.selectedPane, "advanced", v) : b.call(t, "if_eq", t && t.selectedPane, "advanced", v), (g || 0 === g) && (y += g), y += " ", g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.noop,
            fn: x.program(11, d, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '" data-pane="advanced" href="#">\n              <i class="modal-nav-icon glyphicon cogwheel"></i>\n              ', v = {
            hash: {},
            data: s
        }, y += w((f = i.t || t && t.t, f ? f.call(t, "Advanced", v) : b.call(t, "t", "Advanced", v))) + '\n            </a>\n          </li>\n        </ul>\n      </div>\n\n      <div class="modal-body-pane pane-region dark-scrollbar">\n      </div>\n    </div>\n\n    <div class="modal-footer">\n      ', g = i["if"].call(t, t && t.edit, {
            hash: {},
            inverse: x.noop,
            fn: x.program(15, h, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '\n\n      <span class="form-message"></span>\n\n      <button type="button" class="cancel-btn btn btn-default" data-dismiss="modal">', v = {
            hash: {},
            data: s
        }, y += w((f = i.t || t && t.t, f ? f.call(t, "Cancel", v) : b.call(t, "t", "Cancel", v))) + '</button>\n      <button type="button" class="save-btn btn btn-primary btn-loading ', g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.noop,
            fn: x.program(11, d, s),
            data: s
        }), (g || 0 === g) && (y += g), y += '">\n        <div class="loading loading-sm"></div>\n        <span class="btn-label">\n          <span class="save-btn-label-next">', v = {
            hash: {},
            data: s
        }, y += w((f = i.t || t && t.t, f ? f.call(t, "Next", v) : b.call(t, "t", "Next", v))) + '</span>\n          <span class="save-btn-label-save">\n            ', g = i["if"].call(t, t && t.add, {
            hash: {},
            inverse: x.program(19, m, s),
            fn: x.program(17, p, s),
            data: s
        }), (g || 0 === g) && (y += g), y += "\n          </span>\n        </span>\n      </button>\n    </div>\n  </div>\n</div>\n"
    });
});