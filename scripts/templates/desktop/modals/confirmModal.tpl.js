define(["handlebars"], function(hb) { 
    return hb.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, rv = "";
            return rv += '\n        <button type="button" class="cancel-btn btn btn-lg btn-default" data-dismiss="modal">', s = {
                hash: {},
                data: t
            }, rv += h((n = i.t || e && e.t, n ? n.call(e, e && e.cancelLabel, s) : u.call(e, "t", e && e.cancelLabel, s))) + "</button>\n      ";
        }

        function r() {
            return "btn-danger";
        }

        function o() {
            return "btn-primary";
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var l, c, d = "",
            u = i.helperMissing,
            h = this.escapeExpression,
            p = "function",
            m = this;
        return d += '<div class="modal-dialog">\n  <div class="modal-content">\n    <div class="modal-header">\n      <button type="button" class="close" data-dismiss="modal"><i class="glyphicon remove-2"></i></button>\n      <h4 class="modal-title"><i class="modal-icon glyphicon circle-exclamation-mark"></i> ', (l = i.title) ? l = l.call(t, {
            hash: {},
            data: s
        }) : (l = t && t.title, l = typeof l === p ? l.call(t, {
            hash: {},
            data: s
        }) : l), d += h(l) + '</h4>\n    </div>\n\n    <div class="modal-body">\n      <p class="confirm-message">', (l = i.message) ? l = l.call(t, {
            hash: {},
            data: s
        }) : (l = t && t.message, l = typeof l === p ? l.call(t, {
            hash: {},
            data: s
        }) : l), d += h(l) + '</p>\n      <p class="double-confirm-message text-warning"></p>\n    </div>\n\n    <div class="modal-footer">\n      ', l = i.unless.call(t, t && t.hideCancel, {
            hash: {},
            inverse: m.noop,
            fn: m.program(1, a, s),
            data: s
        }), (l || 0 === l) && (d += l), d += '\n      <button type="button" class="confirm-btn btn btn-lg ', l = i["if"].call(t, t && t.hasDanger, {
            hash: {},
            inverse: m.program(5, o, s),
            fn: m.program(3, r, s),
            data: s
        }), (l || 0 === l) && (d += l), d += ' btn-loading">\n        <div class="loading loading-sm"></div>\n        <span class="btn-label">', c = {
            hash: {},
            data: s
        }, d += h((l = i.t || t && t.t, l ? l.call(t, t && t.confirmLabel, c) : u.call(t, "t", t && t.confirmLabel, c))) + "</span>\n      </button>\n    </div>\n  </div>\n</div>\n"
    });
});