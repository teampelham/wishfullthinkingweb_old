define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        return this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {}, '<div class="player-slider-track">\n  <div class="player-slider-progress"></div>\n</div>\n\n<div class="player-slider-thumb"></div>\n'
    })
})