define(["handlebars"], function(hb) {
    
    return hb.template(function(hbEnv, obj, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"];
        i = this.merge(i, hbEnv.helpers);
        s = s || {};
        
        return '<div id="content" class="scroll-container dark-scrollbar"></div>\n<div class="scroll-shadow transition-out"></div>\n';
    });
});