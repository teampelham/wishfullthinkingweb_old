define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a() {
            return '\n  <a class="media-poster-container play-btn" href="#">\n    <div class="show-poster item-poster media-poster">\n      <div class="media-poster-overlay"></div>\n      <div class="media-poster-overlay-icons">\n        <i class="media-poster-overlay-icon glyphicon play"></i>\n      </div>\n\n      <span class="unwatched-count-badge details-unwatched-count-badge badge badge-lg hidden"></span>\n    </div>\n  </a>\n  '
        }

        function r() {
            return '\n  <div class="show-poster item-poster media-poster">\n    <span class="unwatched-count-badge details-unwatched-count-badge badge badge-lg hidden"></span>\n  </div>\n  '
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var o, l = "",
            c = this;
        return l += '<div class="details-title-container">\n  <h1 class="item-title"></h1>\n  <h2 class="item-year"></h2>\n</div>\n\n<div class="details-metadata-container">\n  <div class="show-details-metadata-container">\n    <div class="metadata-right pull-right">\n      <div class="item-genre metadata-tags"></div>\n    </div>\n\n    <p class="metadata-labels">\n      <span class="item-duration metadata-duration"></span>\n\n      <span class="rating-container rating-container-inline"></span>\n\n      <span class="content-rating-flag-container content-rating-flag-container-inline"></span>\n    </p>\n\n    <div class="summary-container"></div>\n\n    <div class="studio-flag-container"></div>\n  </div>\n\n  <div class="season-list-container details-list-region"></div>\n</div>\n\n<div class="details-poster-container">\n  ', o = i["if"].call(t, t && t.canPlay, {
            hash: {},
            inverse: c.program(3, r, s),
            fn: c.program(1, a, s),
            data: s
        }), (o || 0 === o) && (l += o), l += "\n</div>\n"
    })
});