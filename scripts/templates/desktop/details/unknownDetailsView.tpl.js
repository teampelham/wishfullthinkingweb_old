define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        return o += '<div class="row row-padded">\n  <div class="col-md-8 col-md-offset-2">\n    <h1>', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "This media type does not have a details page.", r) : l.call(t, "t", "This media type does not have a details page.", r))) + "</h1>\n  </div>\n</div>\n"
    })
});