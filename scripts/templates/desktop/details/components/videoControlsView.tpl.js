define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        o += '<p class="item-summary metadata-summary"></p>\n\n<div class="summary-divider">\n  <button id="videoOnButton" type="button" class="summary-divider-btn">'; 
		r = {
            hash: {},
            data: s
        }; 
		o += c((a = i.t || t && t.t, a ? a.call(t, "Start", r) : l.call(t, "t", "Start", r))) + "</button>\n</div>\n";
		o += '<div class="summary-divider">\n  <button id="videoOffButton" type="button" class="summary-divider-btn">';
		o += c((a = i.t || t && t.t, a ? a.call(t, "Stop", r) : l.call(t, "t", "Stop", r))) + "</button>\n</div>\n";
		
		return o;
    })
});