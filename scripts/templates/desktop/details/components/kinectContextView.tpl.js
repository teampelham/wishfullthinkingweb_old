define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var o = "";
        o += '<div class="canvas-container">\n<canvas id="kinectImg" width="600" height="400">\n\n</div>'; 
		
		return o;
    })
});