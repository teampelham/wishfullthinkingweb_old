define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        return o += '<p class="item-summary metadata-summary"></p>\n\n<div class="summary-divider hidden">\n  <button type="button" class="summary-divider-btn">', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "More", r) : l.call(t, "t", "More", r))) + "</button>\n</div>\n"
    })
});