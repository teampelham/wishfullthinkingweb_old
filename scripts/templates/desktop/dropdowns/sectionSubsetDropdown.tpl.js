define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a, o = "";
            return o += '\n  <a class="btn-gray dropdown-toggle" href="#section-subset-dropdown" data-toggle="dropdown">', a = {
                hash: {},
                data: t
            }, o += p((n = i.t || e && e.t, n ? n.call(e, (n = e && e.currentSubset, null == n || n === !1 ? n : n.title), a) : h.call(e, "t", (n = e && e.currentSubset, null == n || n === !1 ? n : n.title), a))) + ' <i class="caret-icon"></i></a>\n\n  <ul class="dropdown-menu">\n    ', s = i.each.call(e, e && e.subsets, {
                hash: {},
                inverse: m.noop,
                fn: m.programWithDepth(2, r, t, e),
                data: t
            }), (s || 0 === s) && (o += s), o += "\n  </ul>\n"
        }

        function r(e, t, n) {
            var s, a = "";
            return a += "\n      ", s = i["if"].call(e, e && e.primary, {
                hash: {},
                inverse: m.noop,
                fn: m.programWithDepth(3, o, t, n),
                data: t
            }), (s || 0 === s) && (a += s), a += "\n    "
        }

        function o(e, t, n) {
            var s, a, r, o = "";
            return o += '\n        <li><a href="#!/server/', r = {
                hash: {},
                data: t
            }, o += p((s = i.encodeUrl || n && n.encodeUrl, s ? s.call(e, n && n.serverID, r) : h.call(e, "encodeUrl", n && n.serverID, r))) + "/section/", r = {
                hash: {},
                data: t
            }, o += p((s = i.encodeUrl || n && n.encodeUrl, s ? s.call(e, n && n.sectionID, r) : h.call(e, "encodeUrl", n && n.sectionID, r))) + "/", r = {
                hash: {},
                data: t
            }, o += p((s = i.encodeUrl || e && e.encodeUrl, s ? s.call(e, e && e.key, r) : h.call(e, "encodeUrl", e && e.key, r))) + '">', r = {
                hash: {},
                data: t
            }, o += p((s = i.t || e && e.t, s ? s.call(e, e && e.title, r) : h.call(e, "t", e && e.title, r))) + " ", r = {
                hash: {},
                inverse: m.noop,
                fn: m.program(4, l, t),
                data: t
            }, s = i.if_eq || n && n.if_eq, a = s ? s.call(e, (s = n && n.currentSubset, null == s || s === !1 ? s : s.key), e && e.key, r) : h.call(e, "if_eq", (s = n && n.currentSubset, null == s || s === !1 ? s : s.key), e && e.key, r), (a || 0 === a) && (o += a), o += "</a></li>\n      "
        }

        function l() {
            return '<i class="dropdown-selected-icon glyphicon ok-2"></i>'
        }

        function c(e, t) {
            var n, s, a = "";
            return a += '\n  <span class="dropdown-placeholder">', s = {
                hash: {},
                data: t
            }, a += p((n = i.t || e && e.t, n ? n.call(e, (n = e && e.currentSubset, null == n || n === !1 ? n : n.title), s) : h.call(e, "t", (n = e && e.currentSubset, null == n || n === !1 ? n : n.title), s))) + "</span>\n"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var d, u = "",
            h = i.helperMissing,
            p = this.escapeExpression,
            m = this;
        return d = i["if"].call(t, t && t.showDropdown, {
            hash: {},
            inverse: m.program(6, c, s),
            fn: m.program(1, a, s),
            data: s
        }), (d || 0 === d) && (u += d), u += "\n"
    });
});