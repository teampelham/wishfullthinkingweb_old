define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a = "";
            return a += '\n  <a class="breadcrumb-btn btn-gray" data-key="' + p((n = e && e.item, n = null == n || n === !1 ? n : n.key, typeof n === h ? n.apply(e) : n)) + '" href="#"><span class="breadcrumb-truncated-label">' + p((n = e && e.item, n = null == n || n === !1 ? n : n.title, typeof n === h ? n.apply(e) : n)) + '</span></a>\n\n  <a class="btn-gray dropdown-toggle split-dropdown-toggle" href="#', (s = i.id) ? s = s.call(e, {
                hash: {},
                data: t
            }) : (s = e && e.id, s = typeof s === h ? s.call(e, {
                hash: {},
                data: t
            }) : s), a += p(s) + '" data-toggle="dropdown">\n    <i class="caret-icon caret-icon-sm"></i>\n  </a>\n\n  <ul class="dropdown-menu">\n    ', s = i.each.call(e, e && e.items, {
                hash: {},
                inverse: m.noop,
                fn: m.programWithDepth(2, r, t, e),
                data: t
            }), (s || 0 === s) && (a += s), a += "\n  </ul>\n"
        }

        function r(e, t, n) {
            var s, a, r, l = "";
            return l += '\n      <li><a class="breadcrumb-btn" data-key="', (s = i.key) ? s = s.call(e, {
                hash: {},
                data: t
            }) : (s = e && e.key, s = typeof s === h ? s.call(e, {
                hash: {},
                data: t
            }) : s), l += p(s) + '" href="#"><span class="dropdown-truncated-label">', (s = i.title) ? s = s.call(e, {
                hash: {},
                data: t
            }) : (s = e && e.title, s = typeof s === h ? s.call(e, {
                hash: {},
                data: t
            }) : s), l += p(s) + '</span><i class="dropdown-selected-icon glyphicon ok-2 ', r = {
                hash: {},
                inverse: m.noop,
                fn: m.program(3, o, t),
                data: t
            }, s = i.if_neq || n && n.if_neq, a = s ? s.call(e, (s = n && n.item, null == s || s === !1 ? s : s.key), e && e.key, r) : f.call(e, "if_neq", (s = n && n.item, null == s || s === !1 ? s : s.key), e && e.key, r), (a || 0 === a) && (l += a), l += '"></i></a></li>\n    '
        }

        function o() {
            return "hidden"
        }

        function l(e, t) {
            var n, s = "";
            return s += '\n  <a class="breadcrumb-btn btn-gray" ', n = i.unless.call(e, e && e.hasDetails, {
                hash: {},
                inverse: m.noop,
                fn: m.program(6, c, t),
                data: t
            }), (n || 0 === n) && (s += n), s += ' href="#">\n    <span class="breadcrumb-truncated-label">' + p((n = e && e.item, n = null == n || n === !1 ? n : n.title, typeof n === h ? n.apply(e) : n)) + "</span>\n  </a>\n"
        }

        function c(e) {
            var t, i = "";
            return i += 'data-key="' + p((t = e && e.item, t = null == t || t === !1 ? t : t.key, typeof t === h ? t.apply(e) : t)) + '"'
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var d, u = "",
            h = "function",
            p = this.escapeExpression,
            m = this,
            f = i.helperMissing;
        return d = i["if"].call(t, t && t.showDropdown, {
            hash: {},
            inverse: m.program(5, l, s),
            fn: m.program(1, a, s),
            data: s
        }), (d || 0 === d) && (u += d), u += "\n"
    })
})