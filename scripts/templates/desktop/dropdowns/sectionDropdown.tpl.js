define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a = "";
            return a += '\n  <a class="dropdown-toggle" href="#section-dropdown" data-toggle="dropdown">' + p((n = e && e.currentSection, n = null == n || n === !1 ? n : n.title, typeof n === f ? n.apply(e) : n)) + ' <i class="caret-icon"></i></a>\n\n  <ul class="dropdown-menu">\n    ', s = i["if"].call(e, e && e.showPlaylists, {
                hash: {},
                inverse: m.noop,
                fn: m.programWithDepth(2, r, t, e),
                data: t
            }), (s || 0 === s) && (a += s), a += "\n    ", s = i.each.call(e, e && e.sections, {
                hash: {},
                inverse: m.noop,
                fn: m.programWithDepth(5, l, t, e),
                data: t
            }), (s || 0 === s) && (a += s), a += "\n  </ul>\n"
        }

        function r(e, t, n) {
            var s, a, r, l = "";
            return l += '\n    <li><a href="#!/server/', r = {
                hash: {},
                data: t
            }, l += p((s = i.encodeUrl || n && n.encodeUrl, s ? s.call(e, n && n.serverID, r) : h.call(e, "encodeUrl", n && n.serverID, r))) + '/playlists"><span class="dropdown-truncated-label">', r = {
                hash: {},
                data: t
            }, l += p((s = i.t || n && n.t, s ? s.call(e, "Playlists", r) : h.call(e, "t", "Playlists", r))) + "</span>", a = i.unless.call(e, (s = e && e.currentSection, null == s || s === !1 ? s : s.key), {
                hash: {},
                inverse: m.noop,
                fn: m.program(3, o, t),
                data: t
            }), (a || 0 === a) && (l += a), l += "</a></li>\n    "
        }

        function o() {
            return '<i class="dropdown-selected-icon glyphicon ok-2"></i>'
        }

        function l(e, t, n) {
            var s, a, r, l = "";
            return l += '\n      <li><a href="#!/server/', r = {
                hash: {},
                data: t
            }, l += p((s = i.encodeUrl || n && n.encodeUrl, s ? s.call(e, n && n.serverID, r) : h.call(e, "encodeUrl", n && n.serverID, r))) + "/section/", r = {
                hash: {},
                data: t
            }, l += p((s = i.encodeUrl || e && e.encodeUrl, s ? s.call(e, e && e.key, r) : h.call(e, "encodeUrl", e && e.key, r))) + '"><span class="dropdown-truncated-label">', (a = i.title) ? a = a.call(e, {
                hash: {},
                data: t
            }) : (a = e && e.title, a = typeof a === f ? a.call(e, {
                hash: {},
                data: t
            }) : a), l += p(a) + "</span>", r = {
                hash: {},
                inverse: m.noop,
                fn: m.program(3, o, t),
                data: t
            }, s = i.if_eq || n && n.if_eq, a = s ? s.call(e, (s = n && n.currentSection, null == s || s === !1 ? s : s.key), e && e.key, r) : h.call(e, "if_eq", (s = n && n.currentSection, null == s || s === !1 ? s : s.key), e && e.key, r), (a || 0 === a) && (l += a), l += "</a></li>\n    "
        }

        function c(e) {
            var t, i = "";
            return i += "\n  " + p((t = e && e.currentSection, t = null == t || t === !1 ? t : t.title, typeof t === f ? t.apply(e) : t)) + "\n"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var d, u = "",
            h = i.helperMissing,
            p = this.escapeExpression,
            m = this,
            f = "function";
        return d = i["if"].call(t, t && t.showDropdown, {
            hash: {},
            inverse: m.program(7, c, s),
            fn: m.program(1, a, s),
            data: s
        }), (d || 0 === d) && (u += d), u += "\n"
    });
});