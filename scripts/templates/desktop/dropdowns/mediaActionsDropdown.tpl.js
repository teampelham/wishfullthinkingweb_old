define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a = "";
            return a += '<li><a class="download-btn" href="', (n = i.downloadUrl) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.downloadUrl, n = typeof n === d ? n.call(e, {
                hash: {},
                data: t
            }) : n), a += u(n) + '" download="', (n = i.downloadName) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.downloadName, n = typeof n === d ? n.call(e, {
                hash: {},
                data: t
            }) : n), a += u(n) + '" tabindex="-1">', s = {
                hash: {},
                data: t
            }, a += u((n = i.t || e && e.t, n ? n.call(e, "Download", s) : h.call(e, "t", "Download", s))) + "</a></li>"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var r, o, l, c = "",
            d = "function",
            u = this.escapeExpression,
            h = i.helperMissing,
            p = this;
        return c += '<li><a class="play-all-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Play All", l) : h.call(t, "t", "Play All", l))) + '</a></li>\n<li><a class="play-shuffled-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Shuffle", l) : h.call(t, "t", "Shuffle", l))) + '</a></li>\n<li><a class="play-next-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Play Next", l) : h.call(t, "t", "Play Next", l))) + '</a></li>\n<li><a class="add-to-up-next-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Add to Up Next", l) : h.call(t, "t", "Add to Up Next", l))) + '</a></li>\n<li><a class="play-mix-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Play Plex Mix", l) : h.call(t, "t", "Play Plex Mix", l))) + '</a></li>\n<li><a class="play-trailer-btn play-primary-extra-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Play Trailer", l) : h.call(t, "t", "Play Trailer", l))) + '</a></li>\n<li><a class="play-music-video-btn play-primary-extra-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Play Music Video", l) : h.call(t, "t", "Play Music Video", l))) + '</a></li>\n<li><a class="add-to-playlist-btn btn-link" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Add to Playlist...", l) : h.call(t, "t", "Add to Playlist...", l))) + '</a></li>\n\n<li class="divider"></li>\n\n<li><a class="refresh-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Refresh", l) : h.call(t, "t", "Refresh", l))) + '</a></li>\n<li><a class="mark-watched-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Mark as Watched", l) : h.call(t, "t", "Mark as Watched", l))) + '</a></li>\n<li><a class="mark-unwatched-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Mark as Unwatched", l) : h.call(t, "t", "Mark as Unwatched", l))) + '</a></li>\n<li><a class="sync-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Sync...", l) : h.call(t, "t", "Sync...", l))) + "</a></li>\n", o = i["if"].call(t, t && t.downloadUrl, {
            hash: {},
            inverse: p.noop,
            fn: p.program(1, a, s),
            data: s
        }), (o || 0 === o) && (c += o), c += '\n<li><a class="watch-later-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Watch Later", l) : h.call(t, "t", "Watch Later", l))) + '</a></li>\n<li><a class="share-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Share...", l) : h.call(t, "t", "Share...", l))) + '</a></li>\n<li><a class="recommend-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Recommend...", l) : h.call(t, "t", "Recommend...", l))) + '</a></li>\n<li><a class="delete-btn has-danger" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Delete", l) : h.call(t, "t", "Delete", l))) + '</a></li>\n<li><a class="info-btn" href="#" tabindex="-1">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, "Info", l) : h.call(t, "t", "Info", l))) + "</a></li>\n"
    })
})