define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a() {
            return '<i class="dropdown-selected-icon glyphicon ok-2"></i>'
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var r, o, l, c = "",
            d = i.helperMissing,
            u = this.escapeExpression,
            h = this;
        return c += '<a href="#">', l = {
            hash: {},
            data: s
        }, c += u((r = i.t || t && t.t, r ? r.call(t, t && t.plural, l) : d.call(t, "t", t && t.plural, l))) + " ", o = i["if"].call(t, t && t.selected, {
            hash: {},
            inverse: h.noop,
            fn: h.program(1, a, s),
            data: s
        }), (o || 0 === o) && (c += o), c += "</a>\n"
    });
});