define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        return o += '<a class="btn-gray dropdown-toggle" href="#section-type-dropdown" data-toggle="dropdown">', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, (a = t && t.currentType, null == a || a === !1 ? a : a.plural), r) : l.call(t, "t", (a = t && t.currentType, null == a || a === !1 ? a : a.plural), r))) + ' <i class="caret-icon"></i></a>\n\n<ul class="dropdown-menu"></ul>\n'
    });
});