define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a() {
            return "hidden"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var r, o, l = "",
            c = this,
            d = i.helperMissing,
            u = this.escapeExpression;
        return l += '<div class="side-bar-page main-page dark-scrollbar">\n  <div class="section-current-filters-container">\n    <div class="side-bar-actions">\n      <a class="new-filter-icon-btn new-filter-btn btn-gray ', r = i.unless.call(t, t && t.showNewFilterIconBtn, {
            hash: {},
            inverse: c.noop,
            fn: c.program(1, a, s),
            data: s
        }), (r || 0 === r) && (l += r), l += '" href="#"><i class="glyphicon plus"></i></a>\n    </div>\n\n    <h5>', o = {
            hash: {},
            data: s
        }, l += u((r = i.t || t && t.t, r ? r.call(t, "Filters", o) : d.call(t, "t", "Filters", o))) + '</h5>\n    <div class="section-current-filters-list-container"></div>\n  </div>\n\n  <div class="section-sort-container hidden">\n    <h5>', o = {
            hash: {},
            data: s
        }, l += u((r = i.t || t && t.t, r ? r.call(t, "Sort", o) : d.call(t, "t", "Sort", o))) + '</h5>\n    <div class="section-sort-list-container"></div>\n  </div>\n</div>\n\n<div class="side-bar-page filters-page dark-scrollbar">\n  <div class="section-filters-container">\n    <div class="side-bar-actions">\n      <a class="hide-btn btn-gray" href="#"><i class="glyphicon chevron-left"></i></a>\n    </div>\n\n    <h5>', o = {
            hash: {},
            data: s
        }, l += u((r = i.t || t && t.t, r ? r.call(t, "Select A Filter", o) : d.call(t, "t", "Select A Filter", o))) + '</h5>\n    <div class="section-filters-list-container"></div>\n  </div>\n</div>\n\n<div class="side-bar-page filter-options-page dark-scrollbar">\n  <div class="section-filter-options-container">\n    <div class="side-bar-actions">\n      <a class="hide-btn btn-gray" href="#"><i class="glyphicon chevron-left"></i></a>\n    </div>\n\n    <h5 class="filter-options-header"></h5>\n    <div class="section-filter-option-list-container"></div>\n  </div>\n</div>\n'
    });
});