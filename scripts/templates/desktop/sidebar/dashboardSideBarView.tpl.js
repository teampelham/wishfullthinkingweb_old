define(["handlebars"], function(hb) {
    
	return hb.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"]; 
		i = this.merge(i, e.helpers); 
		s = s || {};
		return '<div class="dashboard-empty-servers-container"></div>\n<div class="dashboard-servers-container"></div>\n';
    });
});