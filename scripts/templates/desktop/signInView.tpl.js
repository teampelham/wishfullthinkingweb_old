define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"];
        i = this.merge(i, e.helpers); 
        s = s || {}; 
        
		return '<div class="sign-in-logo text-center">\n  <i class="wf-image logo-sm"></i>\n</div>\n\n<div class="row row-padded">\n  <div class="col-sm-6 col-sm-offset-3">\n    <form id="user-account-form"></form>\n  </div>\n</div>\n';
       // return '<div class="text-center">\n  WISHFULL THINKING</div>\n<div class="row row-padded">\n  <div class="col-sm-6 col-sm-offset-3">\n    <form id="user-account-form"></form>\n  </div></div>';
    });
});