define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a() {
            return "boolean-btn"
        }

        function r() {
            return "filter-btn"
        }

        function o() {
            return "selected"
        }

        function l() {
            return "hidden"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var c, d, u, h = "",
            p = this,
            m = i.helperMissing,
            f = this.escapeExpression;
        return h += '<a class="', u = {
            hash: {},
            inverse: p.program(3, r, s),
            fn: p.program(1, a, s),
            data: s
        }, c = i.if_eq || t && t.if_eq, d = c ? c.call(t, (c = t && t.filter, null == c || c === !1 ? c : c.filterType), "boolean", u) : m.call(t, "if_eq", (c = t && t.filter, null == c || c === !1 ? c : c.filterType), "boolean", u), (d || 0 === d) && (h += d), h += " btn-gray ", d = i["if"].call(t, t && t.selected, {
            hash: {},
            inverse: p.noop,
            fn: p.program(5, o, s),
            data: s
        }), (d || 0 === d) && (h += d), h += '" href="#">\n  ', u = {
            hash: {},
            data: s
        }, h += f((c = i.t || t && t.t, c ? c.call(t, (c = t && t.filter, null == c || c === !1 ? c : c.title), u) : m.call(t, "t", (c = t && t.filter, null == c || c === !1 ? c : c.title), u))) + '\n  <i class="selected-icon glyphicon ok-2 ', d = i.unless.call(t, t && t.selected, {
            hash: {},
            inverse: p.noop,
            fn: p.program(7, l, s),
            data: s
        }), (d || 0 === d) && (h += d), h += '"></i>\n</a>\n'
    });
});