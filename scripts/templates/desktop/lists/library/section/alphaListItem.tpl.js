define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r = "",
            o = "function",
            l = this.escapeExpression;
        return r += '<a class="btn-gray" href="#">', (a = i.title) ? a = a.call(t, {
            hash: {},
            data: s
        }) : (a = t && t.title, a = typeof a === o ? a.call(t, {
            hash: {},
            data: s
        }) : a), r += l(a) + "</a>\n"
    })
})