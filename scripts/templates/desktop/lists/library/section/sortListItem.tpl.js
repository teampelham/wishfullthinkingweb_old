define("hb!templates/desktop/lists/library/section/SortListItem", ["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        return o += '<a class="sort-btn btn-gray" href="#">\n  ', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, t && t.title, r) : l.call(t, "t", t && t.title, r))) + '\n  <i class="desc-icon caret-icon caret-icon-sm hidden"></i>\n  <i class="asc-icon caret-icon caret-icon-sm caret-icon-reverse hidden"></i>\n</a>\n'
    });
});