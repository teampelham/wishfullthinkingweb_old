define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a = "";
            return a += '\n  <div class="label-list-item">\n    <a class="new-filter-btn label label-btn label-default" href="#"><i class="label-icon glyphicon circle-plus"></i> ', s = {
                hash: {},
                data: t
            }, a += d((n = i.t || e && e.t, n ? n.call(e, "New Filter", s) : c.call(e, "t", "New Filter", s))) + "</a>\n  </div>\n"
        }

        function r(e, t) {
            var n, s, a = "";
            return a += '\n  <div class="label-list-item">\n    <a class="remove-filter-btn label label-btn label-primary" data-key="', (n = i.key) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.key, n = typeof n === u ? n.call(e, {
                hash: {},
                data: t
            }) : n), a += d(n) + '" href="#"><i class="label-icon glyphicon circle-remove"></i> <span class="label-truncate">', s = {
                hash: {},
                data: t
            }, a += d((n = i.csl || e && e.csl, n ? n.call(e, e && e.titles, s) : c.call(e, "csl", e && e.titles, s))) + "</span></a>\n  </div>\n"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var o, l = "",
            c = i.helperMissing,
            d = this.escapeExpression,
            u = "function",
            h = this;
        return o = i.unless.call(t, t && t.currentFilters, {
            hash: {},
            inverse: h.noop,
            fn: h.program(1, a, s),
            data: s
        }), (o || 0 === o) && (l += o), l += "\n\n", o = i.each.call(t, t && t.currentFilters, {
            hash: {},
            inverse: h.noop,
            fn: h.program(3, r, s),
            data: s
        }), (o || 0 === o) && (l += o), l += "\n"
    });
});