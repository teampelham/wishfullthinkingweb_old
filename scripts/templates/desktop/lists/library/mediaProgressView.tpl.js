define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        return this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {}, '<div class="media-progress-track"></div>\n<div class="media-progress-pct"></div>\n'
    })
});