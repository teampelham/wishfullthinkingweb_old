define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a() {
            return "hide-actions"
        }

        function r() {
            return "selected"
        }

        function o() {
            return "hidden"
        }

        function l() {
            return '\n      <span class="media-added-at"></span>\n    '
        }

        function c(e, t) {
            var n, s, a;
            return a = {
                hash: {},
                inverse: x.program(12, u, t),
                fn: x.program(10, d, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, e && e.sort, "originallyAvailableAt", a) : k.call(e, "if_eq", e && e.sort, "originallyAvailableAt", a), s || 0 === s ? s : ""
        }

        function d() {
            return '\n      <span class="media-available-at"></span>\n    '
        }

        function u(e, t) {
            var n, s, a;
            return a = {
                hash: {},
                inverse: x.program(15, p, t),
                fn: x.program(13, h, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, e && e.sort, "rating", a) : k.call(e, "if_eq", e && e.sort, "rating", a), s || 0 === s ? s : ""
        }

        function h() {
            return '\n      <span class="media-rating"></span>\n    '
        }

        function p(e, t) {
            var n, s, a;
            return a = {
                hash: {},
                inverse: x.program(18, f, t),
                fn: x.program(16, m, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, e && e.sort, "mediaHeight", a) : k.call(e, "if_eq", e && e.sort, "mediaHeight", a), s || 0 === s ? s : ""
        }

        function m() {
            return '\n      <span class="media-resolution"></span>\n    '
        }

        function f(e, t) {
            var n, s, a;
            return a = {
                hash: {},
                inverse: x.noop,
                fn: x.program(19, g, t),
                data: t
            }, n = i.if_eq || e && e.if_eq, s = n ? n.call(e, e && e.sort, "duration", a) : k.call(e, "if_eq", e && e.sort, "duration", a), s || 0 === s ? s : ""
        }

        function g() {
            return '\n      <span class="media-duration"></span>\n    '
        }
        this.compilerInfo = [4, ">= 1.0.0"]; 
        i = this.merge(i, e.helpers);
         s = s || {};
        
        var v, y, b, w = "",
            x = this,
            k = i.helperMissing,
            S = "function",
            C = this.escapeExpression;
            
        w += '<a class="media-poster-container '; 
        v = i.unless.call(t, t && t.showActions, {
            hash: {},
            inverse: x.noop,
            fn: x.program(1, a, s),
            data: s
        }); 
        
        (v || 0 === v) && (w += v); 
        
        w += " "; 
        
        v = i["if"].call(t, t && t.selected, {
            hash: {},
            inverse: x.noop,
            fn: x.program(3, r, s),
            data: s
        }); 
        
        (v || 0 === v) && (w += v); 
        
        w += '" href="';
        (v = i.url) ? v = v.call(t, {
            hash: {},
            data: s
        }) : (v = t && t.url, v = typeof v === S ? v.call(t, {
            hash: {},
            data: s
        }) : v); 
        
        w += C(v) + '">\n  <div class="media-poster">\n    <div class="media-poster-overlay"></div>\n\n    <div class="media-poster-overlay-icons with-actions">\n      <i class="analyzing-icon media-poster-overlay-icon glyphicon magic hidden"></i>\n      <i class="matching-icon media-poster-overlay-icon glyphicon search hidden"></i>\n      <i class="metadata-icon media-poster-overlay-icon glyphicon tags hidden"></i>\n      <i class="trash-icon media-poster-overlay-icon glyphicon bin warning hidden"></i>\n    </div>\n\n    <div class="media-poster-actions '; 
        
        v = i.unless.call(t, t && t.showActions, {
            hash: {},
            inverse: x.noop,
            fn: x.program(5, o, s),
            data: s
        }); 
        
        (v || 0 === v) && (w += v); 
        
        w += '">\n      <button class="play-btn media-poster-btn btn-link" tabindex="-1"><i class="glyphicon power"></i></button>\n      <button class="play-unwatched-btn media-poster-btn btn-link" tabindex="-1"><i class="glyphicon power"></i></button>\n      <button class="edit-btn media-poster-btn btn-link" tabindex="-1"><i class="glyphicon pencil"></i></button>\n      <button class="more-btn media-poster-btn btn-link" tabindex="-1"><i class="glyphicon more"></i></button>\n    </div>\n\n    <span class="media-count-badge badge badge-lg hidden"></span>\n    <span class="unwatched-count-badge badge badge-lg hidden"></span>\n\n    <div class="media-progress"></div>\n    <div class="loading loading-inline"></div>\n  </div>\n\n  <span class="unwatched-icon"></span>\n\n  <div class="media-title media-heading"></div>\n  <div class="media-subtitle-1 media-heading secondary"></div>\n  <div class="media-subtitle-2 media-heading secondary"></div>\n  <div class="media-context media-heading secondary"></div>\n\n  <div class="media-sort media-heading secondary">\n    '; 
        
        b = {
            hash: {},
            inverse: x.program(9, c, s),
            fn: x.program(7, l, s),
            data: s
        }; 
        v = i.if_eq || t && t.if_eq; 
        
        y = v ? v.call(t, t && t.sort, "addedAt", b) : k.call(t, "if_eq", t && t.sort, "addedAt", b); 
        (y || 0 === y) && (w += y); 
        
        w += '\n  </div>\n\n  <div class="media-subtitle-1-fill media-heading hidden"></div>\n  <div class="media-subtitle-2-fill media-heading hidden"></div>\n</a>\n\n<div class="media-actions-dropdown dropdown">\n  <div class="dropdown-toggle" data-toggle="dropdown"></div>\n</div>\n';
        
        return w;
    });
});