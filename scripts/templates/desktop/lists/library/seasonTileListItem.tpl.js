define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a() {
            return "hide-actions"
        }

        function r() {
            return "hidden"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var o, l = "",
            c = this,
            d = "function",
            u = this.escapeExpression;
        return l += '<a class="media-poster-container ', o = i.unless.call(t, t && t.showActions, {
            hash: {},
            inverse: c.noop,
            fn: c.program(1, a, s),
            data: s
        }), (o || 0 === o) && (l += o), l += '" href="', (o = i.url) ? o = o.call(t, {
            hash: {},
            data: s
        }) : (o = t && t.url, o = typeof o === d ? o.call(t, {
            hash: {},
            data: s
        }) : o), l += u(o) + '">\n  <div class="media-poster">\n    <div class="media-poster-overlay"></div>\n\n    <div class="media-poster-actions ', o = i.unless.call(t, t && t.showActions, {
            hash: {},
            inverse: c.noop,
            fn: c.program(3, r, s),
            data: s
        }), (o || 0 === o) && (l += o), l += '">\n      <button class="play-btn media-poster-btn btn-link" tabindex="-1"><i class="glyphicon play"></i></button>\n      <button class="play-unwatched-btn media-poster-btn btn-link" tabindex="-1"><i class="glyphicon play"></i></button>\n      <button class="edit-btn media-poster-btn btn-link" tabindex="-1"><i class="glyphicon pencil"></i></button>\n      <button class="more-btn media-poster-btn btn-link" tabindex="-1"><i class="glyphicon more"></i></button>\n    </div>\n\n    <span class="unwatched-count-badge badge badge-lg hidden"></span>\n  </div>\n\n  <div class="media-title media-heading"></div>\n  <div class="media-subtitle-1 media-heading secondary"></div>\n</a>\n\n<div class="media-actions-dropdown dropdown">\n  <div class="dropdown-toggle" data-toggle="dropdown"></div>\n</div>\n'
    })
})