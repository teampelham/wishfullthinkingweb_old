define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a() {
            return "left-arrow"
        }

        function r(e, t) {
            var n;
            return n = i["if"].call(e, e && e.home, {
                hash: {},
                inverse: f.program(6, l, t),
                fn: f.program(4, o, t),
                data: t
            }), n || 0 === n ? n : ""
        }

        function o() {
            return "home"
        }

        function l(e, t) {
            var n;
            return n = i["if"].call(e, e && e.network, {
                hash: {},
                inverse: f.program(9, d, t),
                fn: f.program(7, c, t),
                data: t
            }), n || 0 === n ? n : ""
        }

        function c() {
            return "globe"
        }

        function d(e, t) {
            var n;
            return n = i.unless.call(e, e && e.parent, {
                hash: {},
                inverse: f.program(12, h, t),
                fn: f.program(10, u, t),
                data: t
            }), n || 0 === n ? n : ""
        }

        function u() {
            return "hdd"
        }

        function h() {
            return "folder-open"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var p, m = "",
            f = this,
            g = "function",
            v = this.escapeExpression;
        return m += '<a class="striped-list-item" href="#" data-key="', (p = i.key) ? p = p.call(t, {
            hash: {},
            data: s
        }) : (p = t && t.key, p = typeof p === g ? p.call(t, {
            hash: {},
            data: s
        }) : p), m += v(p) + '">\n  <i class="folder-icon glyphicon ', p = i["if"].call(t, t && t.back, {
            hash: {},
            inverse: f.program(3, r, s),
            fn: f.program(1, a, s),
            data: s
        }), (p || 0 === p) && (m += p), m += '"></i> ', (p = i.title) ? p = p.call(t, {
            hash: {},
            data: s
        }) : (p = t && t.title, p = typeof p === g ? p.call(t, {
            hash: {},
            data: s
        }) : p), m += v(p) + ' <div class="loading loading-sm loading-inline hidden"></div>\n</a>\n'
    })
});