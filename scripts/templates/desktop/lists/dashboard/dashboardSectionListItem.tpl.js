define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        function a() {
            return "active"
        }

        function r() {
            return '\n    <div class="section-actions pull-right">\n      <button class="edit-section-btn btn-gray" href="#"><i class="glyphicon pencil"></i></button>\n    </div>\n  '
        }

        function o() {
            return "show-actions"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var l, c, d, u = "",
            h = this,
            p = i.helperMissing,
            m = this.escapeExpression,
            f = "function";
        return u += '<a class="btn-gray btn-loading btn-loading-static ', l = i["if"].call(t, t && t.refreshing, {
            hash: {},
            inverse: h.noop,
            fn: h.program(1, a, s),
            data: s
        }), (l || 0 === l) && (u += l), u += '" href="#!/server/', d = {
            hash: {},
            data: s
        }, u += m((l = i.encodeUrl || t && t.encodeUrl, l ? l.call(t, t && t.serverID, d) : p.call(t, "encodeUrl", t && t.serverID, d))) + "/section/", d = {
            hash: {},
            data: s
        }, u += m((l = i.encodeUrl || t && t.encodeUrl, l ? l.call(t, (l = t && t.section, null == l || l === !1 ? l : l.key), d) : p.call(t, "encodeUrl", (l = t && t.section, null == l || l === !1 ? l : l.key), d))) + '">\n  ', c = i["if"].call(t, t && t.showActions, {
            hash: {},
            inverse: h.noop,
            fn: h.program(3, r, s),
            data: s
        }), (c || 0 === c) && (u += c), u += '\n  <div class="loading loading-xs"></div>\n  <i class="btn-label section-icon glyphicon ', (c = i.icon) ? c = c.call(t, {
            hash: {},
            data: s
        }) : (c = t && t.icon, c = typeof c === f ? c.call(t, {
            hash: {},
            data: s
        }) : c), u += m(c) + '"></i> <span class="section-title ', c = i["if"].call(t, t && t.showActions, {
            hash: {},
            inverse: h.noop,
            fn: h.program(5, o, s),
            data: s
        }), (c || 0 === c) && (u += c), u += '"></span>\n</a>\n'
    });
});