define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a = "";
            return a += '\n  <div class="side-bar-actions pull-right ', n = i["if"].call(e, e && e.signedIn, {
                hash: {},
                inverse: _.noop,
                fn: _.program(2, r, t),
                data: t
            }), (n || 0 === n) && (a += n), a += '">\n    ', s = i["if"].call(e, (n = e && e.server, null == n || n === !1 ? n : n.connected), {
                hash: {},
                inverse: _.noop,
                fn: _.program(4, o, t),
                data: t
            }), (s || 0 === s) && (a += s), a += '\n\n    <span id="' + A((n = e && e.server, n = null == n || n === !1 ? n : n.machineIdentifier, typeof n === T ? n.apply(e) : n)) + '-dropdown" class="dropdown">\n      <a class="btn-gray dropdown-toggle" href="#' + A((n = e && e.server, n = null == n || n === !1 ? n : n.machineIdentifier, typeof n === T ? n.apply(e) : n)) + '-dropdown" data-toggle="dropdown"><i class="glyphicon cogwheel"></i> <i class="caret-icon caret-icon-sm"></i></a>\n\n      <ul class="dropdown-menu pull-right">\n        ', s = i["if"].call(e, (n = e && e.server, null == n || n === !1 ? n : n.connected), {
                hash: {},
                inverse: _.program(11, u, t),
                fn: _.program(6, l, t),
                data: t
            }), (s || 0 === s) && (a += s), a += "\n      </ul>\n    </span>\n  </div>\n"
        }

        function r() {
            return "signed-in"
        }

        function o() {
            return '\n      <a class="add-library-btn btn-gray" href="#"><i class="glyphicon plus"></i></a>\n    '
        }

        function l(e, t) {
            var n, s, a, r = "";
            return r += '\n          <li><a class="settings-btn" href="#"><span class="dropdown-truncated-label">', a = {
                hash: {},
                data: t
            }, r += A((n = i.t || e && e.t, n ? n.call(e, "Settings", a) : P.call(e, "t", "Settings", a))) + '</span></a></li>\n          <li class="signed-in-item"><a class="share-btn" href="#"><span class="dropdown-truncated-label">', a = {
                hash: {},
                data: t
            }, r += A((n = i.t || e && e.t, n ? n.call(e, "Share...", a) : P.call(e, "t", "Share...", a))) + '</span></a></li>\n          <li><a class="update-btn" href="#"><span class="dropdown-truncated-label">', s = i["if"].call(e, e && e.hasRefreshingSection, {
                hash: {},
                inverse: _.program(9, d, t),
                fn: _.program(7, c, t),
                data: t
            }), (s || 0 === s) && (r += s), r += '</span></a></li>\n          <li><a class="optimize-btn" href="#"><span class="dropdown-truncated-label">', a = {
                hash: {},
                data: t
            }, r += A((n = i.t || e && e.t, n ? n.call(e, "Optimize", a) : P.call(e, "t", "Optimize", a))) + '</span></a></li>\n          <li><a class="clean-bundles-btn" href="#"><span class="dropdown-truncated-label">', a = {
                hash: {},
                data: t
            }, r += A((n = i.t || e && e.t, n ? n.call(e, "Clean Bundles", a) : P.call(e, "t", "Clean Bundles", a))) + '</span></a></li>\n          <li><a class="empty-trash-btn" href="#"><span class="dropdown-truncated-label">', a = {
                hash: {},
                data: t
            }, r += A((n = i.t || e && e.t, n ? n.call(e, "Empty Trash", a) : P.call(e, "t", "Empty Trash", a))) + "</span></a></li>\n        "
        }

        function c(e, t) {
            var n, s;
            return s = {
                hash: {},
                data: t
            }, A((n = i.t || e && e.t, n ? n.call(e, "Cancel Update", s) : P.call(e, "t", "Cancel Update", s)))
        }

        function d(e, t) {
            var n, s;
            return s = {
                hash: {},
                data: t
            }, A((n = i.t || e && e.t, n ? n.call(e, "Update Libraries", s) : P.call(e, "t", "Update Libraries", s)))
        }

        function u(e, t) {
            var n, s, a = "";
            return a += '\n          <li class="signed-in-item"><a class="share-btn" href="#"><span class="dropdown-truncated-label">', s = {
                hash: {},
                data: t
            }, a += A((n = i.t || e && e.t, n ? n.call(e, "Share...", s) : P.call(e, "t", "Share...", s))) + '</span></a></li>\n          <li><a class="remove-btn has-danger" href="#"><span class="dropdown-truncated-label">', s = {
                hash: {},
                data: t
            }, a += A((n = i.t || e && e.t, n ? n.call(e, "Remove", s) : P.call(e, "t", "Remove", s))) + "</span></a></li>\n        "
        }

        function h() {
            return "selected"
        }

        function p() {
            return "disabled"
        }

        function m() {
            return "show-actions"
        }

        function f() {
            return '<i class="cloud-sync-icon glyphicon cloud"></i>'
        }

        function g() {
            return "hidden"
        }

        function v(e, t) {
            var n, s, a = "";
            return a += '\n  <span class="mixed-content-error">', s = {
                hash: {},
                data: t
            }, a += A((n = i.t || e && e.t, n ? n.call(e, "We're sorry, but we can't reach this server.", s) : P.call(e, "t", "We're sorry, but we can't reach this server.", s))) + "</span>\n"
        }

        function y(e, t) {
            var n, s, a = "";
            return a += '\n  <span class="mixed-content-error">', s = {
                hash: {},
                data: t
            }, a += A((n = i.t || e && e.t, n ? n.call(e, "We're sorry, but we can't reach this server securely. {1}Click here{2} to reload the app using insecure connections.", "<a class='mixed-content-btn' href='#' target='_blank'>", "</a>", s) : P.call(e, "t", "We're sorry, but we can't reach this server securely. {1}Click here{2} to reload the app using insecure connections.", "<a class='mixed-content-btn' href='#' target='_blank'>", "</a>", s))) + " <a href='https://support.plex.tv/hc/en-us/articles/206225077'>", s = {
                hash: {},
                data: t
            }, a += A((n = i.t || e && e.t, n ? n.call(e, "More Info", s) : P.call(e, "t", "More Info", s))) + "</a></span>\n"
        }

        function b(e, t) {
            var n, s, a = "";
            return a += '\n  <span class="mixed-content-error">', s = {
                hash: {},
                data: t
            }, a += A((n = i.t || e && e.t, n ? n.call(e, "We're sorry, but we can't reach this server securely. {1}Click here{2} to allow insecure connections.", "<a class='http-fallback-btn' href='#' target='_blank'>", "</a>", s) : P.call(e, "t", "We're sorry, but we can't reach this server securely. {1}Click here{2} to allow insecure connections.", "<a class='http-fallback-btn' href='#' target='_blank'>", "</a>", s))) + "</span>\n"
        }

        function w(e, t) {
            var n, s, a = "";
            return a += '\n    <li>\n      <a class="btn-gray" href="#!/server/', s = {
                hash: {},
                data: t
            }, a += A((n = i.encodeUrl || e && e.encodeUrl, n ? n.call(e, (n = e && e.server, null == n || n === !1 ? n : n.machineIdentifier), s) : P.call(e, "encodeUrl", (n = e && e.server, null == n || n === !1 ? n : n.machineIdentifier), s))) + '/channels">\n        <i class="section-icon glyphicon show-big-thumbnails"></i> ', s = {
                hash: {},
                data: t
            }, a += A((n = i.t || e && e.t, n ? n.call(e, "Channels", s) : P.call(e, "t", "Channels", s))) + "\n      </a>\n    </li>\n  "
        }

        function x(e, t) {
            var n, s, a = "";
            return a += '\n    <li>\n      <a class="btn-gray" href="#!/server/', s = {
                hash: {},
                data: t
            }, a += A((n = i.encodeUrl || e && e.encodeUrl, n ? n.call(e, (n = e && e.server, null == n || n === !1 ? n : n.machineIdentifier), s) : P.call(e, "encodeUrl", (n = e && e.server, null == n || n === !1 ? n : n.machineIdentifier), s))) + '/playlists">\n        <i class="section-icon plex-icon playlist-alt"></i> ', s = {
                hash: {},
                data: t
            }, a += A((n = i.t || e && e.t, n ? n.call(e, "Playlists", s) : P.call(e, "t", "Playlists", s))) + "\n      </a>\n    </li>\n  "
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var k, S, C = "",
            P = i.helperMissing,
            A = this.escapeExpression,
            _ = this,
            T = "function";
        return k = i["if"].call(t, t && t.showActions, {
            hash: {},
            inverse: _.noop,
            fn: _.program(1, a, s),
            data: s
        }), (k || 0 === k) && (C += k), C += '\n\n<h5>\n  <a class="select-server-btn ', k = i["if"].call(t, t && t.selected, {
            hash: {},
            inverse: _.noop,
            fn: _.program(13, h, s),
            data: s
        }), (k || 0 === k) && (C += k), C += " ", k = i.unless.call(t, t && t.canBePrimary, {
            hash: {},
            inverse: _.noop,
            fn: _.program(15, p, s),
            data: s
        }), (k || 0 === k) && (C += k), C += '" href="#">\n    <span class="server-title ', k = i["if"].call(t, t && t.showActions, {
            hash: {},
            inverse: _.noop,
            fn: _.program(17, m, s),
            data: s
        }), (k || 0 === k) && (C += k), C += '">' + A((k = t && t.server, k = null == k || k === !1 ? k : k.friendlyName, typeof k === T ? k.apply(t) : k)) + "</span>\n  </a>\n  ", S = i["if"].call(t, (k = t && t.server, null == k || k === !1 ? k : k.isCloudSync), {
            hash: {},
            inverse: _.noop,
            fn: _.program(19, f, s),
            data: s
        }), (S || 0 === S) && (C += S), C += '\n  <i class="secure-connection-icon plex-icon padlock-round ', S = i.unless.call(t, t && t.hasSecureConnection, {
            hash: {},
            inverse: _.noop,
            fn: _.program(21, g, s),
            data: s
        }), (S || 0 === S) && (C += S), C += " ", S = i.unless.call(t, (k = t && t.server, null == k || k === !1 ? k : k.connected), {
            hash: {},
            inverse: _.noop,
            fn: _.program(21, g, s),
            data: s
        }), (S || 0 === S) && (C += S), C += '"></i>\n</h5>\n\n', S = i["if"].call(t, t && t.showUnavailableMessage, {
            hash: {},
            inverse: _.noop,
            fn: _.program(23, v, s),
            data: s
        }), (S || 0 === S) && (C += S), C += "\n", S = i["if"].call(t, t && t.showMixedContentMessage, {
            hash: {},
            inverse: _.noop,
            fn: _.program(25, y, s),
            data: s
        }), (S || 0 === S) && (C += S), C += "\n", S = i["if"].call(t, t && t.showHttpFallbackMessgae, {
            hash: {},
            inverse: _.noop,
            fn: _.program(27, b, s),
            data: s
        }), (S || 0 === S) && (C += S), C += '\n\n<ul class="list side-bar-list dashboard-section-list ', S = i.unless.call(t, (k = t && t.server, null == k || k === !1 ? k : k.connected), {
            hash: {},
            inverse: _.noop,
            fn: _.program(21, g, s),
            data: s
        }), (S || 0 === S) && (C += S), C += '">\n  ', S = i["if"].call(t, t && t.showChannels, {
            hash: {},
            inverse: _.noop,
            fn: _.program(29, w, s),
            data: s
        }), (S || 0 === S) && (C += S), C += "\n\n  ", S = i["if"].call(t, t && t.showPlaylists, {
            hash: {},
            inverse: _.noop,
            fn: _.program(31, x, s),
            data: s
        }), (S || 0 === S) && (C += S), C += "\n</ul>\n"
    })
})