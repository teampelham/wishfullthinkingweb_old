define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s = "";
            return s += '<span class="star"><i class="star-icon glyphicon ', n = i["if"].call(e, e, {
                hash: {},
                inverse: d.program(4, o, t),
                fn: d.program(2, r, t),
                data: t
            }), (n || 0 === n) && (s += n), s += '"></i><span class="empty-icon"></span></span>'
        }

        function r() {
            return "star"
        }

        function o() {
            return "dislikes"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var l, c = "",
            d = this;
        return l = i.each.call(t, t && t.stars, {
            hash: {},
            inverse: d.noop,
            fn: d.program(1, a, s),
            data: s
        }), (l || 0 === l) && (c += l), c += "\n"
    })
})