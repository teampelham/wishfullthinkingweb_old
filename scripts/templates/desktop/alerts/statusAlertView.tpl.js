define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"]; 
        i = this.merge(i, e.helpers); 
        s = s || {};
        return '<i class="alert-icon glyphicon cardio"></i>\n\n<span class="status"></span>\n';
    });
});