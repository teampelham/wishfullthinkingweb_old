define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a() {
            return "circle-ok"
        }

        function r() {
            return "circle-exclamation-mark"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var o, l, c, d = "",
            u = this,
            h = i.helperMissing,
            p = "function";
        return d += '<button class="close-btn btn-link"><i class="glyphicon remove-2"></i></button>\n\n<i class="alert-icon glyphicon ', c = {
            hash: {},
            inverse: u.program(3, r, s),
            fn: u.program(1, a, s),
            data: s
        }, o = i.if_eq || t && t.if_eq, l = o ? o.call(t, t && t.type, "success", c) : h.call(t, "if_eq", t && t.type, "success", c), (l || 0 === l) && (d += l), d += '"></i>\n<h4>', (l = i.message) ? l = l.call(t, {
            hash: {},
            data: s
        }) : (l = t && t.message, l = typeof l === p ? l.call(t, {
            hash: {},
            data: s
        }) : l), (l || 0 === l) && (d += l), d += "</h4>\n"
    })
});