define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r = "",
            o = "function",
            l = this.escapeExpression;
        return r += '<button type="button" class="close-btn btn-link"><i class="glyphicon remove-2"></i></button>\n\n<span class="message">', (a = i.message) ? a = a.call(t, {
            hash: {},
            data: s
        }) : (a = t && t.message, a = typeof a === o ? a.call(t, {
            hash: {},
            data: s
        }) : a), r += l(a) + "</span>\n"
    });
});