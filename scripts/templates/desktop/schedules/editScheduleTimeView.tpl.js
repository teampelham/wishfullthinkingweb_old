define(["handlebars"], function(hb) {
    return hb.template(function(e, t, info, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"]; 
        info = this.merge(info, e.helpers); 
        s = s || {};
        
        var translatorFn = info.t || t && t.t;
        var missing = info.helperMissing;
        var esc = this.escapeExpression;
        var that = this;
        
        function dataHash(data) {
            return {
                hash: {},
                data: data
            };
        }
        
        function translateText(text) {
            if(translatorFn)
                text = translatorFn.call(t, text, dataHash(s));
            else
                text = missing.call(t, "t", text, dataHash(s));  
            return esc(text);
        }
        
        function conditionalHtml(op, text, data, each) {
            var fn = each ? info.each : info[op];
			var fnResult = fn.call(t, text, data);
        	if (fnResult || fnResult === 0) 
				return fnResult;
			return '';
		}
        
        function equalityHtml(op, text1, text2, data) {
            var result;
            if(op)   
                result = op.call(t, text1, text2, data);
            else
                result = missing.call(t, "if_eq", text1, text2, data);
                
            if (result || result === 0) 
				return result;
			return '';
        }
        
        function chosen() { // a
            return "section-type-chosen"
        }
        
        function selectScheduleType(e, t) {   // r
            var html = '\n    <p class="prompt">' + translateText('Choose how to time your schedule') + '</p>\n  ';
            return html;
        }

        function scheduleTypeLabel(e, t) {    // o
            return '\n    <label>' + translateText('Schedule Type') + '</label>\n  ';
        }
        
        function dayofWeekCheckboxes(e, t) {
            var html = '';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbSunday" value="1" /> Sun</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbMonday" value="2" /> Mon</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbTuesday" value="4" /> Tue</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbWednesday" value="8" /> Wed</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbThursday" value="16" /> Thu</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbFriday" value="32" /> Fri</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbSaturday" value="64" /> Sat</label>';
            
            return html;
        }

        function deviceTypeWizard(e, t, n) {        // l
            var html = '\n      <a class="wizard-block section-type ';
            html += conditionalHtml("if", n && n.add, {
                hash: {},
                inverse: that.noop,
                fn: that.program(8, selectable, t),
                data: t
            });
            
            html += " ";
            html += equalityHtml(info.if_eq || n && n.if_eq, n && n.currentSectionKey, e && e.key, {
                hash: {},
                inverse: that.noop,
                fn: that.program(10, selected, t),
                data: t
            });
            
            html += '" data-key="';
            
            var dataHolder;
            if (info.typeID)
                dataHolder = info.typeID.call(e, { hash: {}, data: t });
            else if (e && e.typeID && typeof e.typeID === "function") 
                dataHolder = e.typeID.call(e, { hash: {}, data: t });
            else
                dataHolder = e && e.typeID;
            
            html += esc(dataHolder) + '" href="#">\n        <i class="wizard-block-icon plex-icon ';
            
            if (info.sectionTypeIcon)
                dataHolder = info.sectionTypeIcon.call(e, { hash: {}, data: t });
            else if (e && e.sectionTypeIcon && typeof e.sectionTypeIcon === "function")
                dataHolder = e.sectionTypeIcon.call(e, { hash: {}, data: t });
            else 
                dataHolder = e && e.sectionTypeIcon;
            
            html += esc(dataHolder) + '"></i> ';
            html += esc(translateText(e && e.title));
            html += "\n      </a>\n    ";
            
            return html;
        }

        function selectable() { //c 
            return "selectable"
        }

        function selected() {   // d
            return "selected"
        }

        function scheduleTimingLabel(e, t) {
            return '\n      <div class="row">\n        <div class="col-xs-12">\n          <p class="prompt">' +
                translateText("When will the schedule execute?") + "</p>\n        </div>\n      </div>\n    ";
        }
		
        var html = '<form class="edit-section-type-form settings-container">\n '; 
		
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.program(5, scheduleTypeLabel, s),
            fn: that.program(3, selectScheduleType, s),
            data: s
        });
        html += '\n\n  <div class="wizard-blocks-group form-group type-group">\n    '; 
        html += '<div><label class="radio-inline"><input type="radio" name="timeBase" id="timeRadio" /> Set Time</label><label class="radio-inline"><input type="radio" name="timeBase" id="sunRadio" /> Sunset/Sunrise</label></div>';
        html += '\n\n    <label class="control-label">\n      <span class="control-error">'; 
        
		html += translateText("You must select a device type.") + '</span>\n    </label>\n  </div>\n\n  <div id="timeChosen">\n    '; 
		html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.noop,
            fn: that.program(12, scheduleTimingLabel, s),
            data: s
        });
        html += '\n\n    <div class="row visible-once-section-type-chosen">\n      <div class="col-md-7">\n        <div class="form-group time-group">\n          <div>\n            <label for="edit-time" class="control-label">\n              '; 
        
		html += translateText("Time") + '\n              <span class="control-error">';
		html += translateText("A time for the Schedule is required.") + '</span>\n            </label>\n          </div>\n          <input type="time" id="edit-time" name="time" class="form-control" value="';
		html += '">\n        </div>\n      </div>\n\n      ';
        html += ' </div>\n    </div>\n\n    <div id="sunCycleChosen">\n    <div class="row visible-once-section-type-chosen">\n      <div class="col-md-12">\n        <span class="text-muted">\n          ';      
        html += '<label class="radio-inline"><input type="radio" name="sunCycle" id="sunriseRadio" /> Sunrise</label><label class="radio-inline"><input type="radio" name="sunCycle" id="sunsetRadio" /> Sunset</label>';
        html += "\n        </span>\n      </div>\n  "; 
        html += '<div class="col-md-5">\n        <div class="form-group language-group">\n          <div>\n            <label for="edit-offset" class="control-label">\n              '; 
		html += translateText("Offset (in minutes)") + '\n              <span class="control-error">'; 
		html += translateText("A device type is required") + '</span>\n            </label>\n          </div>\n          <input type="time" id="edit-offset" name="deviceType" class="form-control" />';
        html += "</div>\n    </div>\n     </div>\n    </div>\n   </div>\n\n</form>\n";
        
        return html;
    });
});