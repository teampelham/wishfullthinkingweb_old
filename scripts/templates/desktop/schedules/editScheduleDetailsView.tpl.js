define(["handlebars"], function(hb) {
    return hb.template(function(e, t, info, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"]; 
        info = this.merge(info, e.helpers); 
        s = s || {};
        
        var translatorFn = info.t || t && t.t;
        var missing = info.helperMissing;
        var esc = this.escapeExpression;
        var that = this;
        
        function dataHash(data) {
            return {
                hash: {},
                data: data
            };
        }
        
        function translateText(text) {
            if(translatorFn)
                text = translatorFn.call(t, text, dataHash(s));
            else
                text = missing.call(t, "t", text, dataHash(s));  
            return esc(text);
        }
        
        function conditionalHtml(op, text, data, each) {
            var fn = each ? info.each : info[op];
			var fnResult = fn.call(t, text, data);
        	if (fnResult || fnResult === 0) 
				return fnResult;
			return '';
		}
        
        function equalityHtml(op, text1, text2, data) {
            var result;
            if(op)   
                result = op.call(t, text1, text2, data);
            else
                result = missing.call(t, "if_eq", text1, text2, data);
                
            if (result || result === 0) 
				return result;
			return '';
        }
        
        function chosen() { // a
            return "section-type-chosen"
        }
        
        function selectScheduleType(e, t) {   // r
            var html = '\n    <p class="prompt">' + translateText('Select the Schedule type') + '</p>\n  ';
            return html;
        }

        function scheduleTypeLabel(e, t) {    // o
            return '\n    <label>' + translateText('Schedule Type') + '</label>\n  ';
        }
        
        function dayofWeekCheckboxes(e, t) {
            var html = '';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbSunday" value="1" /> Sun</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbMonday" value="2" /> Mon</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbTuesday" value="4" /> Tue</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbWednesday" value="8" /> Wed</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbThursday" value="16" /> Thu</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbFriday" value="32" /> Fri</label>';
            html += '<label class="checkbox-inline"><input type="checkbox" id="cbSaturday" value="64" /> Sat</label>';
            
            return html;
        }

        
        function selectable() { //c 
            return "selectable"
        }

        function selected() {   // d
            return "selected"
        }

        function renderDeviceTypeOptions(deviceType, t) {
            return '<option value="' + deviceType.typeID + '">' + deviceType.title + '</option>';
        }
		
        var html = '<form class="edit-section-type-form settings-container">\n '; 
		
        html += conditionalHtml("if", t && t.add, {
            hash: {},
            inverse: that.program(5, scheduleTypeLabel, s),
            fn: that.program(3, selectScheduleType, s),
            data: s
        });
        html += '\n\n  <div class="wizard-blocks-group form-group repeat-group">\n    '; 
        html += '<div><label class="radio-inline"><input type="radio" name="repeatRadios" id="repeatRb" /> Repeat</label><label class="radio-inline"><input type="radio" name="repeatRadios" id="singleRb" />Single</label></div>';
        html += '\n\n    <label class="control-label">\n      <span class="control-error">'; 
        
		html += 'Say what now?</span>\n    </label>\n  </div>\n\n  <div id="dateTimeChosen">\n    '; 
        html += '\n\n    <div class="row visible-once-section-type-chosen">\n      <div class="col-md-7">\n        <div class="form-group date-group">\n          <div>\n            <label for="edit-single-date" class="control-label">\n              '; 
        
		html += translateText("Date") + '\n              <span class="control-error">';
		html += translateText("A date for the Schedule is required.") + '</span>\n            </label>\n          </div>\n          <input type="date" id="edit-single-date" name="date" class="form-control" value="';
		html += '">\n        </div>\n      </div>\n\n      </div>\n     '; 
        html += '</div>\n\n    <div id="repeatChosen">\n    <div class="row visible-once-section-type-chosen">\n      <div class="col-md-12">\n        <span class="text-muted">\n          ';      
        html += dayofWeekCheckboxes();
        
        /*
		html += conditionalHtml("if", t && t.updatedAt, {
            hash: {},
            inverse: that.program(16, p, s),
            fn: that.program(14, h, s),
            data: s
        });*/
        html += "\n        </span>\n      </div>\n    </div>\n  </div>\n   </div>\n\n</form>\n";
        
        return html;
    });
});