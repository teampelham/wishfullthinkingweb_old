define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        function a() {
            return "hidden"
        }

        function r() {
            return "signed-in"
        }

        function o() {
            return "full-user"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var l, c, d, u = "",
            h = this,
            p = "function",
            m = this.escapeExpression,
            f = i.helperMissing;
        return u += '<a class="dropdown-toggle dropdown-poster-toggle" href="#nav-dropdown" data-toggle="dropdown">\n  <div class="dropdown-poster-container">\n    <div class="media-poster img-circle"></div>\n    <i class="caret-icon"></i>\n  </div>\n  <span class="total-badge badge ', l = i.unless.call(t, t && t.totalCount, {
            hash: {},
            inverse: h.noop,
            fn: h.program(1, a, s),
            data: s
        }), (l || 0 === l) && (u += l), u += '">', (l = i.totalCount) ? l = l.call(t, {
            hash: {},
            data: s
        }) : (l = t && t.totalCount, l = typeof l === p ? l.call(t, {
            hash: {},
            data: s
        }) : l), u += m(l) + '</span>\n</a>\n\n<ul class="dropdown-menu ', l = i["if"].call(t, t && t.signedIn, {
            hash: {},
            inverse: h.noop,
            fn: h.program(3, r, s),
            data: s
        }), (l || 0 === l) && (u += l), u += " ", l = i["if"].call(t, t && t.isFullUser, {
            hash: {},
            inverse: h.noop,
            fn: h.program(5, o, s),
            data: s
        }), (l || 0 === l) && (u += l), u += '">\n  <li class="signed-in-item dropdown-header username-header"></li>\n  <li class="signed-in-item"><a class="users-btn" href="#">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Users", d) : f.call(t, "t", "Users", d))) + ' <span class="invites-badge badge ', c = i.unless.call(t, t && t.invitesCount, {
            hash: {},
            inverse: h.noop,
            fn: h.program(1, a, s),
            data: s
        }), (c || 0 === c) && (u += c), u += '">', (c = i.invitesCount) ? c = c.call(t, {
            hash: {},
            data: s
        }) : (c = t && t.invitesCount, c = typeof c === p ? c.call(t, {
            hash: {},
            data: s
        }) : c), u += m(c) + '</span></a></li>\n\n  <li class="signed-in-item full-user-item divider"></li>\n\n  <li class="signed-in-item full-user-item"><a href="#!/playlist/queue">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Watch Later", d) : f.call(t, "t", "Watch Later", d))) + '</a></li>\n  <li class="signed-in-item full-user-item"><a href="#!/playlist/recommendations">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Recommended", d) : f.call(t, "t", "Recommended", d))) + '</a></li>\n\n  <li class="signed-in-item divider"></li>\n\n  <li><a href="#!/announcements">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Announcements", d) : f.call(t, "t", "Announcements", d))) + ' <span class="announcements-badge badge ', c = i.unless.call(t, t && t.unreadAnnouncementsCount, {
            hash: {},
            inverse: h.noop,
            fn: h.program(1, a, s),
            data: s
        }), (c || 0 === c) && (u += c), u += '">', (c = i.unreadAnnouncementsCount) ? c = c.call(t, {
            hash: {},
            data: s
        }) : (c = t && t.unreadAnnouncementsCount, c = typeof c === p ? c.call(t, {
            hash: {},
            data: s
        }) : c), u += m(c) + '</span></a></li>\n  <li class="signed-in-item full-user-item"><a href="', (c = i.accountUrl) ? c = c.call(t, {
            hash: {},
            data: s
        }) : (c = t && t.accountUrl, c = typeof c === p ? c.call(t, {
            hash: {},
            data: s
        }) : c), u += m(c) + '" target="_blank">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Account", d) : f.call(t, "t", "Account", d))) + '</a></li>\n  <li><a href="', (c = i.appsUrl) ? c = c.call(t, {
            hash: {},
            data: s
        }) : (c = t && t.appsUrl, c = typeof c === p ? c.call(t, {
            hash: {},
            data: s
        }) : c), u += m(c) + '" target="_blank">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Apps", d) : f.call(t, "t", "Apps", d))) + '</a></li>\n  <li><a href="', (c = i.helpUrl) ? c = c.call(t, {
            hash: {},
            data: s
        }) : (c = t && t.helpUrl, c = typeof c === p ? c.call(t, {
            hash: {},
            data: s
        }) : c), u += m(c) + '" target="_blank">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Help", d) : f.call(t, "t", "Help", d))) + '</a></li>\n\n  <li class="divider"></li>\n\n  <li class="signed-in-item switch-user-item ', c = i.unless.call(t, t && t.home, {
            hash: {},
            inverse: h.noop,
            fn: h.program(1, a, s),
            data: s
        }), (c || 0 === c) && (u += c), u += '"><a class="switch-user-btn" href="#">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Switch User...", d) : f.call(t, "t", "Switch User...", d))) + '</a></li>\n  <li class="signed-out-item"><a class="sign-in-btn" href="#">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Sign In", d) : f.call(t, "t", "Sign In", d))) + '</a></li>\n  <li class="signed-in-item full-user-item"><a class="sign-out-btn" href="#">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Sign Out", d) : f.call(t, "t", "Sign Out", d))) + "</a></li>\n</ul>\n"
    });
});