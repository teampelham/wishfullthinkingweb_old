define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        function a() {
            return "hidden"
        }

        function r() {
            return "active"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var o, l, c, d = "",
            u = this,
            h = "function",
            p = this.escapeExpression,
            m = i.helperMissing;
        return d += '<ul class="nav nav-bar-nav">\n  <li><a class="back-btn ', o = i.unless.call(t, t && t.hasHistory, {
            hash: {},
            inverse: u.noop,
            fn: u.program(1, a, s),
            data: s
        }), (o || 0 === o) && (d += o), d += '" href="#"><i class="glyphicon chevron-left"></i></a></li>\n  <li><a class="home-btn" href="#"><i class="glyphicon home"></i></a></li>\n</ul>\n\n<div class="nav-bar-search-container"></div>\n\n<ul class="nav nav-bar-nav nav-bar-right">\n  <li><a class="plex-pass-btn ', o = i.unless.call(t, t && t.canGoPremium, {
            hash: {},
            inverse: u.noop,
            fn: u.program(1, a, s),
            data: s
        }), (o || 0 === o) && (d += o), d += '" href="', (o = i.plexPassUrl) ? o = o.call(t, {
            hash: {},
            data: s
        }) : (o = t && t.plexPassUrl, o = typeof o === h ? o.call(t, {
            hash: {},
            data: s
        }) : o), d += p(o) + '" target="_blank" data-toggle="tooltip-holiday"><span class="plex-pass-label label label-primary">', c = {
            hash: {},
            data: s
        }, d += p((o = i.t || t && t.t, o ? o.call(t, "Go Premium", c) : m.call(t, "t", "Go Premium", c))) + '</span></a></li>\n\n  <li class="', l = i["if"].call(t, t && t.activityCount, {
            hash: {},
            inverse: u.noop,
            fn: u.program(3, r, s),
            data: s
        }), (l || 0 === l) && (d += l), d += '"><a class="activity-btn ', l = i.unless.call(t, t && t.showActivity, {
            hash: {},
            inverse: u.noop,
            fn: u.program(1, a, s),
            data: s
        }), (l || 0 === l) && (d += l), d += '" href="#!/activity" title="', c = {
            hash: {},
            data: s
        }, d += p((o = i.t || t && t.t, o ? o.call(t, "Activity", c) : m.call(t, "t", "Activity", c))) + '" data-toggle="tooltip"><span class="activity-badge badge badge-transparent ', l = i.unless.call(t, t && t.activityCount, {
            hash: {},
            inverse: u.noop,
            fn: u.program(1, a, s),
            data: s
        }), (l || 0 === l) && (d += l), d += '">', (l = i.activityCount) ? l = l.call(t, {
            hash: {},
            data: s
        }) : (l = t && t.activityCount, l = typeof l === h ? l.call(t, {
            hash: {},
            data: s
        }) : l), d += p(l) + '</span><i class="glyphicon cardio"></i></a></li>\n\n  <li><a class="settings-btn" href="#!/settings" title="', c = {
            hash: {},
            data: s
        }, d += p((o = i.t || t && t.t, o ? o.call(t, "Settings", c) : m.call(t, "t", "Settings", c))) + '" data-toggle="tooltip"><i class="glyphicon settings"></i></a></li>\n</ul>\n'
    });
});