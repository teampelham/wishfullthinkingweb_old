define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        function a() {
            return "hidden"
        }

        function r(e, t) {
            var n, s;
            return s = {
                hash: {},
                data: t
            }, u((n = i.t || e && e.t, n ? n.call(e, "Search {1}", e && e.searchPlaceholder, s) : d.call(e, "t", "Search {1}", e && e.searchPlaceholder, s)))
        }

        function o(e, t) {
            var n;
            return (n = i.searchValue) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.searchValue, n = typeof n === h ? n.call(e, {
                hash: {},
                data: t
            }) : n), u(n)
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var l, c = "",
            d = i.helperMissing,
            u = this.escapeExpression,
            h = "function",
            p = this;
        return c += '<div class="form-group form-group-search">\n  <label class="control-label-search" for="nav-bar-search">\n    <i class="glyphicon search"></i>\n    <a class="clear-search-btn ', l = i.unless.call(t, t && t.searchValue, {
            hash: {},
            inverse: p.noop,
            fn: p.program(1, a, s),
            data: s
        }), (l || 0 === l) && (c += l), c += '" href="#"><i class="glyphicon circle-remove"></i></a>\n  </label>\n\n  <input type="search" id="nav-bar-search" class="form-control form-control-search" placeholder="', l = i["if"].call(t, t && t.searchPlaceholder, {
            hash: {},
            inverse: p.noop,
            fn: p.program(3, r, s),
            data: s
        }), (l || 0 === l) && (c += l), c += '" value="', l = i["if"].call(t, t && t.searchValue, {
            hash: {},
            inverse: p.noop,
            fn: p.program(5, o, s),
            data: s
        }), (l || 0 === l) && (c += l), c += '">\n</div>\n'
    });
});