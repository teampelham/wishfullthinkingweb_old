define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a = "";
            return a += '\n    <a class="add-btn btn-gray" title="Add Automated Device" href="#"><i class="btn-label glyphicon plus"></i></a>', a += '\n    <a class="update-btn btn-gray btn-loading btn-loading-replace btn-loading-static ', n = i["if"].call(e, e && e.refreshing, {
                hash: {},
                inverse: h.noop,
                fn: h.program(2, r, t),
                data: t
            }), (n || 0 === n) && (a += n), a += '" href="#" title="', (n = i.updateTitle) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.updateTitle, n = typeof n === p ? n.call(e, {
                hash: {},
                data: t
            }) : n), a += m(n) + '" data-title-alt="', (n = i.updateTitleAlt) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.updateTitleAlt, n = typeof n === p ? n.call(e, {
                hash: {},
                data: t
            }) : n), a += m(n) + '" data-toggle="tooltip">\n      <span class="loading loading-bc"></span>\n      <i class="btn-label-alt glyphicon ban"></i>\n      <i class="btn-label glyphicon repeat"></i>\n    </a>\n\n    <span id="section-actions-dropdown" class="dropdown">\n      <a class="btn-gray dropdown-toggle" href="#section-actions-dropdown" data-toggle="dropdown"><i class="glyphicon cogwheel"></i> <i class="caret-icon caret-icon-sm"></i></a>\n\n      <ul class="dropdown-menu pull-right">\n        <li><a class="edit-btn btn-gray" href="#">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "Edit Library", s) : f.call(e, "t", "Edit Library", s))) + '</a></li>\n        <li><a class="share-btn" href="#">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "Share...", s) : f.call(e, "t", "Share...", s))) + '</a></li>\n        <li><a class="analyze-btn" href="#">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "Analyze", s) : f.call(e, "t", "Analyze", s))) + '</a></li>\n        <li><a class="force-refresh-btn" href="#">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "Force Refresh", s) : f.call(e, "t", "Force Refresh", s))) + '</a></li>\n        <li><a class="empty-trash-btn" href="#">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "Empty Trash", s) : f.call(e, "t", "Empty Trash", s))) + '</a></li>\n        <li><a class="delete-btn has-danger" href="#">', s = {
                hash: {},
                data: t
            }, a += m((n = i.t || e && e.t, n ? n.call(e, "Delete Library", s) : f.call(e, "t", "Delete Library", s))) + "</a></li>\n      </ul>\n    </span>\n  "
        }

        function r() {
            return "active"
        }

        function o() {
            return "hidden"
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var l, c, d, u = "",
            h = this,
            p = "function",
            m = this.escapeExpression,
            f = i.helperMissing;
        return u += '<div class="breadcrumb-actions pull-right">\n  ', l = i.unless.call(t, t && t.limited, {
            hash: {},
            inverse: h.noop,
            fn: h.program(1, a, s),
            data: s
        }), (l || 0 === l) && (u += l), u += '\n\n  <span id="section-list-dropdown" class="dropdown">\n    <a class="btn-gray dropdown-toggle" href="#section-list-dropdown" data-toggle="dropdown"><i class="glyphicon show-thumbnails"></i> <i class="caret-icon caret-icon-sm"></i></a>\n\n    <ul class="dropdown-menu pull-right">\n      <li><a class="list-btn" data-type="media-tile-list" href="#">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Posters", d) : f.call(t, "t", "Posters", d))) + '<i class="dropdown-selected-icon glyphicon ok-2 ', d = {
            hash: {},
            inverse: h.noop,
            fn: h.program(4, o, s),
            data: s
        }, l = i.if_neq || t && t.if_neq, c = l ? l.call(t, t && t.listType, "media-tile-list", d) : f.call(t, "if_neq", t && t.listType, "media-tile-list", d), (c || 0 === c) && (u += c), u += '"></i></a></li>\n      <li><a class="list-btn" data-type="media-details-list" href="#">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "Details", d) : f.call(t, "t", "Details", d))) + '<i class="dropdown-selected-icon glyphicon ok-2 ', d = {
            hash: {},
            inverse: h.noop,
            fn: h.program(4, o, s),
            data: s
        }, l = i.if_neq || t && t.if_neq, c = l ? l.call(t, t && t.listType, "media-details-list", d) : f.call(t, "if_neq", t && t.listType, "media-details-list", d), (c || 0 === c) && (u += c), u += '"></i></a></li>\n      <li><a class="list-btn" data-type="media-list" href="#">', d = {
            hash: {},
            data: s
        }, u += m((l = i.t || t && t.t, l ? l.call(t, "List", d) : f.call(t, "t", "List", d))) + '<i class="dropdown-selected-icon glyphicon ok-2 ', d = {
            hash: {},
            inverse: h.noop,
            fn: h.program(4, o, s),
            data: s
        }, l = i.if_neq || t && t.if_neq, c = l ? l.call(t, t && t.listType, "media-list", d) : f.call(t, "if_neq", t && t.listType, "media-list", d), (c || 0 === c) && (u += c), u += '"></i></a></li>\n    </ul>\n  </span>\n</div>\n\n<span class="breadcrumb-dropdown"></span>\n<span class="breadcrumb-subset-dropdown"></span>\n<span class="breadcrumb-type-dropdown"></span>\n\n<span class="breadcrumb-items-dropdown">\n  <span class="items-badge badge badge-lg">', (c = i.totalSize) ? c = c.call(t, {
            hash: {},
            data: s
        }) : (c = t && t.totalSize, c = typeof c === p ? c.call(t, {
            hash: {},
            data: s
        }) : c), u += m(c) + "</span>\n</span>\n"
    })
});