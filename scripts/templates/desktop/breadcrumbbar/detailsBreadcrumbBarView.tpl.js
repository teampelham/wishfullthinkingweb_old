define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s, a, o = "";
            return o += '\n    <span id="section-list-dropdown" class="dropdown">\n      <a class="btn-gray dropdown-toggle" href="#section-list-dropdown" data-toggle="dropdown"><i class="glyphicon show-thumbnails"></i> <i class="caret-icon caret-icon-sm"></i></a>\n\n      <ul class="dropdown-menu pull-right">\n        <li><a class="list-btn" data-type="episode-tile-list" href="#">', a = {
                hash: {},
                data: t
            }, o += g((n = i.t || e && e.t, n ? n.call(e, "Posters", a) : f.call(e, "t", "Posters", a))) + '<i class="dropdown-selected-icon glyphicon ok-2 ', a = {
                hash: {},
                inverse: v.noop,
                fn: v.program(2, r, t),
                data: t
            }, n = i.if_neq || e && e.if_neq, s = n ? n.call(e, e && e.listType, "episode-tile-list", a) : f.call(e, "if_neq", e && e.listType, "episode-tile-list", a), (s || 0 === s) && (o += s), o += '"></i></a></li>\n        <li><a class="list-btn" data-type="episode-details-list" href="#">', a = {
                hash: {},
                data: t
            }, o += g((n = i.t || e && e.t, n ? n.call(e, "Details", a) : f.call(e, "t", "Details", a))) + '<i class="dropdown-selected-icon glyphicon ok-2 ', a = {
                hash: {},
                inverse: v.noop,
                fn: v.program(2, r, t),
                data: t
            }, n = i.if_neq || e && e.if_neq, s = n ? n.call(e, e && e.listType, "episode-details-list", a) : f.call(e, "if_neq", e && e.listType, "episode-details-list", a), (s || 0 === s) && (o += s), o += '"></i></a></li>\n        <li><a class="list-btn" data-type="episode-list" href="#">', a = {
                hash: {},
                data: t
            }, o += g((n = i.t || e && e.t, n ? n.call(e, "List", a) : f.call(e, "t", "List", a))) + '<i class="dropdown-selected-icon glyphicon ok-2 ', a = {
                hash: {},
                inverse: v.noop,
                fn: v.program(2, r, t),
                data: t
            }, n = i.if_neq || e && e.if_neq, s = n ? n.call(e, e && e.listType, "episode-list", a) : f.call(e, "if_neq", e && e.listType, "episode-list", a), (s || 0 === s) && (o += s), o += '"></i></a></li>\n      </ul>\n    </span>\n  '
        }

        function r() {
            return "hidden"
        }

        function o(e, t) {
            var n, s = "";
            return s += '\n  <span id="section-dropdown" class="details-breadcrumb dropdown">\n\n    ', n = i["if"].call(e, e && e.section, {
                hash: {},
                inverse: v.program(7, c, t),
                fn: v.program(5, l, t),
                data: t
            }), (n || 0 === n) && (s += n), s += '\n\n    <a class="btn-gray dropdown-toggle split-dropdown-toggle" href="#section-dropdown" data-toggle="dropdown">\n      <i class="caret-icon caret-icon-sm"></i>\n    </a>\n\n    <ul class="dropdown-menu">\n      ', n = i["if"].call(e, e && e.showPlaylists, {
                hash: {},
                inverse: v.noop,
                fn: v.program(9, d, t),
                data: t
            }), (n || 0 === n) && (s += n), s += "\n\n      ", n = i.each.call(e, e && e.sections, {
                hash: {},
                inverse: v.noop,
                fn: v.programWithDepth(12, h, t, e),
                data: t
            }), (n || 0 === n) && (s += n), s += "\n    </ul>\n  </span>\n"
        }

        function l(e) {
            var t, i = "";
            return i += '\n      <a class="section-btn btn-gray" data-key="' + g((t = e && e.section, t = null == t || t === !1 ? t : t.key, typeof t === y ? t.apply(e) : t)) + '" href="#">' + g((t = e && e.section, t = null == t || t === !1 ? t : t.title, typeof t === y ? t.apply(e) : t)) + "</a>\n    "
        }

        function c(e, t) {
            var n, s, a = "";
            return a += '\n      <a class="playlists-btn btn-gray" href="#">', s = {
                hash: {},
                data: t
            }, a += g((n = i.t || e && e.t, n ? n.call(e, "Playlists", s) : f.call(e, "t", "Playlists", s))) + "</a>\n    "
        }

        function d(e, t) {
            var n, s, a, r = "";
            return r += '\n        <li><a class="playlists-btn" href="#"><span class="dropdown-truncated-label">', a = {
                hash: {},
                data: t
            }, r += g((n = i.t || e && e.t, n ? n.call(e, "Playlists", a) : f.call(e, "t", "Playlists", a))) + "</span>", s = i.unless.call(e, (n = e && e.section, null == n || n === !1 ? n : n.key), {
                hash: {},
                inverse: v.noop,
                fn: v.program(10, u, t),
                data: t
            }), (s || 0 === s) && (r += s), r += "</a></li>\n      "
        }

        function u() {
            return '<i class="dropdown-selected-icon glyphicon ok-2"></i>'
        }

        function h(e, t, n) {
            var s, a, o, l = "";
            return l += '\n        <li><a class="section-btn" data-key="', (s = i.key) ? s = s.call(e, {
                hash: {},
                data: t
            }) : (s = e && e.key, s = typeof s === y ? s.call(e, {
                hash: {},
                data: t
            }) : s), l += g(s) + '" href="#"><span class="dropdown-truncated-label">', (s = i.title) ? s = s.call(e, {
                hash: {},
                data: t
            }) : (s = e && e.title, s = typeof s === y ? s.call(e, {
                hash: {},
                data: t
            }) : s), l += g(s) + '</span><i class="dropdown-selected-icon glyphicon ok-2 ', o = {
                hash: {},
                inverse: v.noop,
                fn: v.program(2, r, t),
                data: t
            }, s = i.if_neq || n && n.if_neq, a = s ? s.call(e, (s = n && n.section, null == s || s === !1 ? s : s.key), e && e.key, o) : f.call(e, "if_neq", (s = n && n.section, null == s || s === !1 ? s : s.key), e && e.key, o), (a || 0 === a) && (l += a), l += '"></i></a></li>\n      '
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var p, m = "",
            f = i.helperMissing,
            g = this.escapeExpression,
            v = this,
            y = "function";
        return m += '<div class="breadcrumb-actions pull-right">\n  ', p = i["if"].call(t, t && t.listType, {
            hash: {},
            inverse: v.noop,
            fn: v.program(1, a, s),
            data: s
        }), (p || 0 === p) && (m += p), m += '\n\n  <a class="previous-btn btn-gray hidden" href="#"><i class="glyphicon chevron-left"></i></a>\n  <a class="next-btn btn-gray hidden" href="#"><i class="glyphicon chevron-right"></i></a>\n</div>\n\n', p = i["if"].call(t, t && t.showSectionDropdown, {
            hash: {},
            inverse: v.noop,
            fn: v.program(4, o, s),
            data: s
        }), (p || 0 === p) && (m += p), m += '\n\n<span class="grandparent-item hidden"></span>\n<span class="parents-dropdown hidden"></span>\n<span class="siblings-dropdown hidden"></span>\n<span class="current-item hidden"></span>\n'
    })
});