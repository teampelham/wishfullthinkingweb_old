define(["handlebars"], function(hb) {
    return hb.template(function(e, t, i, n, s) {
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var a, r, o = "",
            l = i.helperMissing,
            c = this.escapeExpression;
        return o += '<div class="action-bar">\n  <ul class="nav action-bar-nav">\n    <li>\n      <a class="options-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Options", r) : l.call(t, "t", "Options", r))) + '" data-toggle="tooltip"><i class="glyphicon show-lines"></i></a>\n    </li>\n    <li>\n      <a class="play-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Play All", r) : l.call(t, "t", "Play All", r))) + '" data-toggle="tooltip"><i class="glyphicon play"></i></a>\n    </li>\n    <li>\n      <a class="play-shuffled-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Shuffle", r) : l.call(t, "t", "Shuffle", r))) + '" data-toggle="tooltip"><i class="glyphicon random"></i></a>\n    </li>\n    <li>\n      <a class="add-to-playlist-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Add to playlist", r) : l.call(t, "t", "Add to playlist", r))) + '" data-toggle="tooltip"><i class="plex-icon add-to-playlist"></i></a>\n    </li>\n    <li>\n      <a class="select-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Select", r) : l.call(t, "t", "Select", r))) + '" data-toggle="tooltip"><i class="glyphicon check"></i></a>\n    </li>\n    <li>\n      <a class="deselect-btn btn-gray selected" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Deselect All", r) : l.call(t, "t", "Deselect All", r))) + '" data-toggle="tooltip"><i class="glyphicon unchecked"></i></a>\n    </li>\n    <li>\n      <a class="edit-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Edit", r) : l.call(t, "t", "Edit", r))) + '" data-toggle="tooltip"><i class="glyphicon pencil"></i></a>\n    </li>\n    <li>\n      <a class="refresh-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Refresh", r) : l.call(t, "t", "Refresh", r))) + '" data-toggle="tooltip"><i class="glyphicon repeat"></i></a>\n    </li>\n    <li>\n      <a class="analyze-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Analyze", r) : l.call(t, "t", "Analyze", r))) + '" data-toggle="tooltip"><i class="glyphicon magic"></i></a>\n    </li>\n    <li>\n      <a class="mark-watched-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Mark as Watched", r) : l.call(t, "t", "Mark as Watched", r))) + '" data-toggle="tooltip"><i class="glyphicon circle-ok"></i></a>\n    </li>\n    <li>\n      <a class="mark-unwatched-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Mark as Unwatched", r) : l.call(t, "t", "Mark as Unwatched", r))) + '" data-toggle="tooltip"><i class="glyphicon circle-minus"></i></a>\n    </li>\n    <li>\n      <a class="add-to-collection-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Add to Collection", r) : l.call(t, "t", "Add to Collection", r))) + '" data-toggle="tooltip"><i class="glyphicon folder-plus"></i></a>\n    </li>\n    <li>\n      <a class="merge-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Merge", r) : l.call(t, "t", "Merge", r))) + '" data-toggle="tooltip"><i class="glyphicon resize-small"></i></a>\n    </li>\n    <li>\n      <a class="sync-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Sync", r) : l.call(t, "t", "Sync", r))) + '" data-toggle="tooltip"><i class="glyphicon download"></i></a>\n    </li>\n    <li>\n      <a class="delete-btn btn-gray" href="#" title="', r = {
            hash: {},
            data: s
        }, o += c((a = i.t || t && t.t, a ? a.call(t, "Delete", r) : l.call(t, "t", "Delete", r))) + '" data-toggle="tooltip"><i class="glyphicon bin"></i></a>\n    </li>\n  </ul>\n</div>\n'
    });
});