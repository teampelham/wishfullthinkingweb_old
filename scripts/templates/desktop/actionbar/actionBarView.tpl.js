define(["handlebars"], function(e) {
    return e.template(function(e, t, i, n, s) {
        function a(e, t) {
            var n, s;
            return s = {
                hash: {},
                data: t
            }, g((n = i.t || e && e.t, n ? n.call(e, "Play", s) : f.call(e, "t", "Play", s)))
        }

        function r(e, t) {
            var n, s;
            return s = {
                hash: {},
                data: t
            }, g((n = i.t || e && e.t, n ? n.call(e, "Play All", s) : f.call(e, "t", "Play All", s)))
        }

        function o(e, t) {
            var n, s, a = "";
            return a += '<li><a class="play-next-btn secondary-only ', n = i.unless.call(e, e && e.canQueue, {
                hash: {},
                inverse: v.noop,
                fn: v.program(6, l, t),
                data: t
            }), (n || 0 === n) && (a += n), a += '" href="#">', s = {
                hash: {},
                data: t
            }, a += g((n = i.t || e && e.t, n ? n.call(e, "Play Next", s) : f.call(e, "t", "Play Next", s))) + "</a></li>"
        }

        function l() {
            return "disabled"
        }

        function c(e, t) {
            var n, s, a = "";
            return a += '<li><a class="add-to-up-next-btn secondary-only ', n = i.unless.call(e, e && e.canQueue, {
                hash: {},
                inverse: v.noop,
                fn: v.program(6, l, t),
                data: t
            }), (n || 0 === n) && (a += n), a += '" href="#">', s = {
                hash: {},
                data: t
            }, a += g((n = i.t || e && e.t, n ? n.call(e, "Add to Up Next", s) : f.call(e, "t", "Add to Up Next", s))) + "</a></li>"
        }

        function d(e, t) {
            var n, s, a = "";
            return a += '\n        <li><a class="download-btn secondary-only" href="', (n = i.downloadUrl) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.downloadUrl, n = typeof n === y ? n.call(e, {
                hash: {},
                data: t
            }) : n), a += g(n) + '" download="', (n = i.downloadName) ? n = n.call(e, {
                hash: {},
                data: t
            }) : (n = e && e.downloadName, n = typeof n === y ? n.call(e, {
                hash: {},
                data: t
            }) : n), a += g(n) + '">', s = {
                hash: {},
                data: t
            }, a += g((n = i.t || e && e.t, n ? n.call(e, "Download", s) : f.call(e, "t", "Download", s))) + "</a></li>\n      "
        }
        this.compilerInfo = [4, ">= 1.0.0"], i = this.merge(i, e.helpers), s = s || {};
        var u, h, p, m = "",
            f = i.helperMissing,
            g = this.escapeExpression,
            v = this,
            y = "function";
        return m += '<ul class="nav action-bar-nav">\n  <li>\n    <a class="play-btn btn-gray" href="#" title="', u = i["if"].call(t, t && t.isSingleItem, {
            hash: {},
            inverse: v.program(3, r, s),
            fn: v.program(1, a, s),
            data: s
        }), (u || 0 === u) && (m += u), m += '" data-toggle="tooltip"><i class="glyphicon play"></i></a>\n  </li>\n  <li>\n    <a class="play-unwatched-btn btn-gray" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Play Unwatched", p) : f.call(t, "t", "Play Unwatched", p))) + '" data-toggle="tooltip"><i class="glyphicon play"></i></a>\n  </li>\n  <li>\n    <a class="play-shuffled-btn btn-gray" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Shuffle", p) : f.call(t, "t", "Shuffle", p))) + '" data-toggle="tooltip"><i class="glyphicon random"></i></a>\n  </li>\n  <li>\n    <a class="play-trailer-btn btn-gray" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Play Trailer", p) : f.call(t, "t", "Play Trailer", p))) + '" data-toggle="tooltip"><i class="plex-icon play-strip"></i></a>\n  </li>\n  <li>\n    <a class="edit-btn btn-gray" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Edit", p) : f.call(t, "t", "Edit", p))) + '" data-toggle="tooltip"><i class="glyphicon pencil"></i></a>\n  </li>\n  <li>\n    <a class="add-to-playlist-btn btn-gray" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Add to playlist", p) : f.call(t, "t", "Add to playlist", p))) + '" data-toggle="tooltip"><i class="plex-icon add-to-playlist"></i></a>\n  </li>\n  <li>\n    <a class="refresh-btn btn-gray" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Refresh", p) : f.call(t, "t", "Refresh", p))) + '" data-toggle="tooltip"><i class="glyphicon repeat"></i></a>\n  </li>\n  <li>\n    <a class="analyze-btn btn-gray secondary-action" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Analyze", p) : f.call(t, "t", "Analyze", p))) + '" data-toggle="tooltip"><i class="glyphicon magic"></i></a>\n  </li>\n  <li>\n    <a class="mark-watched-btn btn-gray" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Mark as Watched", p) : f.call(t, "t", "Mark as Watched", p))) + '" data-toggle="tooltip"><i class="glyphicon circle-ok"></i></a>\n  </li>\n  <li>\n    <a class="mark-unwatched-btn btn-gray" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Mark as Unwatched", p) : f.call(t, "t", "Mark as Unwatched", p))) + '" data-toggle="tooltip"><i class="glyphicon circle-minus"></i></a>\n  </li>\n  <li>\n    <a class="select-btn btn-gray" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Select", p) : f.call(t, "t", "Select", p))) + '" data-toggle="tooltip"><i class="glyphicon check"></i></a>\n  </li>\n  <li>\n    <a class="deselect-btn btn-gray selected" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Deselect All", p) : f.call(t, "t", "Deselect All", p))) + '" data-toggle="tooltip"><i class="glyphicon unchecked"></i></a>\n  </li>\n  <li>\n    <a class="sync-btn btn-gray" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Sync", p) : f.call(t, "t", "Sync", p))) + '" data-toggle="tooltip"><i class="glyphicon download"></i></a>\n  </li>\n  <li>\n    <a class="share-btn btn-gray secondary-action" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Share", p) : f.call(t, "t", "Share", p))) + '" data-toggle="tooltip"><i class="glyphicon share"></i></a>\n  </li>\n  <li>\n    <a class="recommend-btn btn-gray secondary-action" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Recommend", p) : f.call(t, "t", "Recommend", p))) + '" data-toggle="tooltip"><i class="glyphicon share-alt"></i></a>\n  </li>\n  <li>\n    <a class="delete-btn btn-gray secondary-action" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Delete", p) : f.call(t, "t", "Delete", p))) + '" data-toggle="tooltip"><i class="glyphicon bin"></i></a>\n  </li>\n  <li>\n    <a class="delete-from-user-playlist-btn btn-gray secondary-action" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Delete", p) : f.call(t, "t", "Delete", p))) + '" data-toggle="tooltip"><i class="glyphicon bin"></i></a>\n  </li>\n  <li>\n    <a class="info-btn btn-gray secondary-action" href="#" title="', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Info", p) : f.call(t, "t", "Info", p))) + '" data-toggle="tooltip"><i class="glyphicon circle-info"></i></a>\n  </li>\n\n  <li class="secondary-actions-dropdown dropdown">\n    <a class="more-btn btn-gray dropdown-toggle" href="#" data-toggle="dropdown"><i class="glyphicon more"></i></a>\n    <ul class="dropdown-menu">\n      <li><a class="play-all-btn secondary-only" href="#">', h = i["if"].call(t, t && t.isSingleItem, {
            hash: {},
            inverse: v.program(3, r, s),
            fn: v.program(1, a, s),
            data: s
        }), (h || 0 === h) && (m += h), m += "</a></li>\n      ", h = i["if"].call(t, t && t.showQueue, {
            hash: {},
            inverse: v.noop,
            fn: v.program(5, o, s),
            data: s
        }), (h || 0 === h) && (m += h), m += "\n      ", h = i["if"].call(t, t && t.showQueue, {
            hash: {},
            inverse: v.noop,
            fn: v.program(8, c, s),
            data: s
        }), (h || 0 === h) && (m += h), m += '\n      <li class="secondary-divider divider"></li>\n      <li><a class="analyze-btn" href="#">', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Analyze", p) : f.call(t, "t", "Analyze", p))) + '</a></li>\n      <li><a class="match-btn secondary-only" href="#">', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Match", p) : f.call(t, "t", "Match", p))) + '</a></li>\n      <li><a class="fix-match-btn secondary-only" href="#">', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Fix Incorrect Match", p) : f.call(t, "t", "Fix Incorrect Match", p))) + '</a></li>\n      <li><a class="unmatch-btn secondary-only" href="#">', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Unmatch", p) : f.call(t, "t", "Unmatch", p))) + '</a></li>\n      <li><a class="split-apart-btn secondary-only" href="#">', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Split Apart", p) : f.call(t, "t", "Split Apart", p))) + "</a></li>\n      ", h = i["if"].call(t, t && t.downloadUrl, {
            hash: {},
            inverse: v.noop,
            fn: v.program(10, d, s),
            data: s
        }), (h || 0 === h) && (m += h), m += '\n      <li><a class="watch-later-btn" href="#" tabindex="-1">', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Watch Later", p) : f.call(t, "t", "Watch Later", p))) + '</a></li>\n      <li><a class="share-btn" href="#">', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Share", p) : f.call(t, "t", "Share", p))) + '</a></li>\n      <li><a class="recommend-btn" href="#">', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Recommend", p) : f.call(t, "t", "Recommend", p))) + '</a></li>\n      <li><a class="delete-btn has-danger" href="#">', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Delete", p) : f.call(t, "t", "Delete", p))) + '</a></li>\n      <li><a class="delete-from-user-playlist-btn has-danger" href="#">', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Delete", p) : f.call(t, "t", "Delete", p))) + '</a></li>\n      <li><a class="info-btn" href="#">', p = {
            hash: {},
            data: s
        }, m += g((u = i.t || t && t.t, u ? u.call(t, "Info", p) : f.call(t, "t", "Info", p))) + "</a></li>\n    </ul>\n  </li>\n</ul>\n"
    })
})