define([
	"underscore", 
	"jquery-lib", 
	"base/utils/jquery/selectText", 
	"base/utils/jquery/isDisabled", 
	"base/utils/jquery/triggerReflow"
], function(_, $) {
    "use strict";
    
	if (!$.support.cors) {		// if there's no cors support in this version
			console.error('no cors, you are screeeeewed');
		$.ajaxPrefilter(function(options) {
	        if (options.crossDomain && options.async) {
	            var type = options.type.toUpperCase();
	            if(options.data && (type == "GET" || type == "HEAD")) {
					options.url += "?" + options.data; 
					delete options.data;
				}
				
				if (!options.headers)
					options.headers = {};
					
	            var xHeader = "X-WF-";
	            var xHeaders = _.filter(_.keys(options.headers), function(header) {
                    return header.lastIndexOf(xHeader, 0) === 0;
                });
				
	            _.each(xHeaders, function(header) {
	                options.headers["X-WF-Proxy-" + header.slice(xHeader.length)] = options.headers[header]; 
					delete options.headers[header];
	            });
				 
				options.headers["X-WF-Url"] = options.url; 
				options.url = "/system/proxy"; 
				options.crossDomain = false; 
				
				if (options.headers["X-WF-Proxy-Token"]) 
					options.headers["X-WF-Token"] = options.headers["X-WF-Proxy-Token"];
	        }
	    });
	}
	
    var standardHeaders = ["Cache-Control", "Content-Language", "Content-Type", "Expires", "Last-Modified", "Pragma", "Location"];
    var xhrOrig = $.ajaxSettings.xhr;
		
    $.ajaxSettings.xhr = function() {
        var xhr = xhrOrig();
        var allHeadersOrig = xhr.getAllResponseHeaders;
        
		xhr.getAllResponseHeaders = function() {
            var allHeaders = allHeadersOrig.apply(xhr);
            if (allHeaders) 
				return allHeaders;
            
			var headerArray = [];
            return _.each(standardHeaders, function(header) {
                var headerValue = xhr.getResponseHeader(header);
                if (headerValue)
					headerArray.push(header + ": " + headerValue);
            });
			
			headerArray.join("\n");
        };
		
		return xhr;
    };
	
	$.ajaxPrefilter(function(options) {
        return options.crossDomain && !options.skipCorsWithRedirect ? "cors_with_redirect" : void 0;
    }); 
	
	$.ajaxTransport("cors_with_redirect", function(options, originalOptions, jqXhr) {		// i, n, s
        options.dataTypes.shift();
        var xhr; 
		var newOptions = _.extend({}, originalOptions, {
            skipCorsWithRedirect: true,
            complete: null,
            error: null,
            success: null
        });
        return {
            send: function(i, callback) {
                newOptions.error = function(response, statusText) {
                    _.extend(jqXhr, response); 
					(response.status, statusText, {
                        alreadyConverted: response.responseText
                    }, response.getAllResponseHeaders());
                }; 
				newOptions.success = function(i, l, response) {
                    _.extend(jqXhr, response);
                    var location = response.getResponseHeader("Location");
                    if (200 == response.status && location && xhrOrig && !xhrOrig.skipCorsLocationRequest){
						 newOptions.url = location; 
						 xhr = $.ajax(newOptions);
					}
					else 
						callback(response.status, l, {
                        	alreadyConverted: response.responseText
                    	}, response.getAllResponseHeaders());
                };
				xhr = $.ajax(newOptions);
            },
            abort: function() {
                if (xhr) 
					xhr.abort();
            }
        };
    });
	
	return $;
});