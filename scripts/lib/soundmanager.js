define(["soundmanager-lib", "base/models/config"], function(e, t) {
    "use strict";
    return e = new SoundManager(); 
	window.soundManager = e; 
	e.url = "swf/soundmanager"; 
	e.debugMode = !1; 
	e.preferFlash = t.get("flash"), e.flashVersion = 9, e.flashLoadTimeout = 2e3, e.useFastPolling = !0, e.useHighPerformance = !0, e.forceUseGlobalHTML5Audio = !0, e.beginDelayedInit(), e
});