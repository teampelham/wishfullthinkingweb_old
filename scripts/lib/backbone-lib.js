+function(e, t) {
    if ("function" == typeof define && define.amd) define("backbone-lib", ["underscore", "jquery", "exports"], function(i, n, s) {
        e.Backbone = t(e, s, i, n)
    });
    else if ("undefined" != typeof exports) {
        var i = require("underscore");
        t(e, exports, i)
    } else e.Backbone = t(e, {}, e._, e.jQuery || e.Zepto || e.ender || e.$)
}(this, function(e, t, i, n) {
    {
        var s = e.Backbone,
            a = [],
            r = (a.push, a.slice);
        a.splice
    }
    t.VERSION = "1.1.2", t.$ = n, t.noConflict = function() {
        return e.Backbone = s, this
    }, t.emulateHTTP = !1, t.emulateJSON = !1;
    var o = t.Events = {
        on: function(e, t, i) {
            if (!c(this, "on", e, [t, i]) || !t) return this;
            this._events || (this._events = {});
            var n = this._events[e] || (this._events[e] = []);
            return n.push({
                callback: t,
                context: i,
                ctx: i || this
            }), this
        },
        once: function(e, t, n) {
            if (!c(this, "once", e, [t, n]) || !t) return this;
            var s = this,
                a = i.once(function() {
                    s.off(e, a), t.apply(this, arguments)
                });
            return a._callback = t, this.on(e, a, n)
        },
        off: function(e, t, n) {
            var s, a, r, o, l, d, u, h;
            if (!this._events || !c(this, "off", e, [t, n])) return this;
            if (!e && !t && !n) return this._events = void 0, this;
            for (o = e ? [e] : i.keys(this._events), l = 0, d = o.length; d > l; l++)
                if (e = o[l], r = this._events[e]) {
                    if (this._events[e] = s = [], t || n)
                        for (u = 0, h = r.length; h > u; u++) a = r[u], (t && t !== a.callback && t !== a.callback._callback || n && n !== a.context) && s.push(a);
                    s.length || delete this._events[e]
                }
            return this
        },
        trigger: function(e) {
            if (!this._events) return this;
            var t = r.call(arguments, 1);
            if (!c(this, "trigger", e, t)) return this;
            var i = this._events[e],
                n = this._events.all;
            return i && d(i, t), n && d(n, arguments), this
        },
        stopListening: function(e, t, n) {
            var s = this._listeningTo;
            if (!s) return this;
            var a = !t && !n;
            n || "object" != typeof t || (n = this), e && ((s = {})[e._listenId] = e);
            for (var r in s) e = s[r], e.off(t, n, this), (a || i.isEmpty(e._events)) && delete this._listeningTo[r];
            return this
        }
    }, l = /\s+/,
        c = function(e, t, i, n) {
            if (!i) return !0;
            if ("object" == typeof i) {
                for (var s in i) e[t].apply(e, [s, i[s]].concat(n));
                return !1
            }
            if (l.test(i)) {
                for (var a = i.split(l), r = 0, o = a.length; o > r; r++) e[t].apply(e, [a[r]].concat(n));
                return !1
            }
            return !0
        }, d = function(e, t) {
            var i, n = -1,
                s = e.length,
                a = t[0],
                r = t[1],
                o = t[2];
            switch (t.length) {
                case 0:
                    for (; ++n < s;)(i = e[n]).callback.call(i.ctx);
                    return;
                case 1:
                    for (; ++n < s;)(i = e[n]).callback.call(i.ctx, a);
                    return;
                case 2:
                    for (; ++n < s;)(i = e[n]).callback.call(i.ctx, a, r);
                    return;
                case 3:
                    for (; ++n < s;)(i = e[n]).callback.call(i.ctx, a, r, o);
                    return;
                default:
                    for (; ++n < s;)(i = e[n]).callback.apply(i.ctx, t);
                    return
            }
        }, u = {
            listenTo: "on",
            listenToOnce: "once"
        };
    i.each(u, function(e, t) {
        o[t] = function(t, n, s) {
            if(typeof t == "undefined")
                return;
            var a = this._listeningTo || (this._listeningTo = {}),
                r = t._listenId || (t._listenId = i.uniqueId("l"));
            return a[r] = t, s || "object" != typeof n || (s = this), t[e](n, s, this), this
        }
    }), o.bind = o.on, o.unbind = o.off, i.extend(t, o);
    var h = t.Model = function(e, t) {
        var n = e || {};
        t || (t = {}), this.cid = i.uniqueId("c"), this.attributes = {}, t.collection && (this.collection = t.collection), t.parse && (n = this.parse(n, t) || {}), n = i.defaults({}, n, i.result(this, "defaults")), this.set(n, t), this.changed = {}, this.initialize.apply(this, arguments)
    };
    i.extend(h.prototype, o, {
        changed: null,
        validationError: null,
        idAttribute: "id",
        initialize: function() {},
        toJSON: function() {
            return i.clone(this.attributes)
        },
        sync: function() {
            return t.sync.apply(this, arguments)
        },
        get: function(e) {
            return this.attributes[e]
        },
        escape: function(e) {
            return i.escape(this.get(e))
        },
        has: function(e) {
            return null != this.get(e)
        },
        set: function(e, t, n) {
            var s, a, r, o, l, c, d, u;
            if (null == e) return this;
            if ("object" == typeof e ? (a = e, n = t) : (a = {})[e] = t, n || (n = {}), !this._validate(a, n)) return !1;
            r = n.unset, l = n.silent, o = [], c = this._changing, this._changing = !0, c || (this._previousAttributes = i.clone(this.attributes), this.changed = {}), u = this.attributes, d = this._previousAttributes, this.idAttribute in a && (this.id = a[this.idAttribute]);
            for (s in a) t = a[s], i.isEqual(u[s], t) || o.push(s), i.isEqual(d[s], t) ? delete this.changed[s] : this.changed[s] = t, r ? delete u[s] : u[s] = t;
            if (!l) {
                o.length && (this._pending = n);
                for (var h = 0, p = o.length; p > h; h++) this.trigger("change:" + o[h], this, u[o[h]], n)
            }
            if (c) return this;
            if (!l)
                for (; this._pending;) n = this._pending, this._pending = !1, this.trigger("change", this, n);
            return this._pending = !1, this._changing = !1, this
        },
        unset: function(e, t) {
            return this.set(e, void 0, i.extend({}, t, {
                unset: !0
            }))
        },
        clear: function(e) {
            var t = {};
            for (var n in this.attributes) t[n] = void 0;
            return this.set(t, i.extend({}, e, {
                unset: !0
            }))
        },
        hasChanged: function(e) {
            return null == e ? !i.isEmpty(this.changed) : i.has(this.changed, e)
        },
        changedAttributes: function(e) {
            if (!e) return this.hasChanged() ? i.clone(this.changed) : !1;
            var t, n = !1,
                s = this._changing ? this._previousAttributes : this.attributes;
            for (var a in e) i.isEqual(s[a], t = e[a]) || ((n || (n = {}))[a] = t);
            return n
        },
        previous: function(e) {
            return null != e && this._previousAttributes ? this._previousAttributes[e] : null
        },
        previousAttributes: function() {
            return i.clone(this._previousAttributes)
        },
        fetch: function(e) {
            e = e ? i.clone(e) : {}, void 0 === e.parse && (e.parse = !0);
            var t = this,
                n = e.success;
            return e.success = function(i) {
                return t.set(t.parse(i, e), e) ? (n && n(t, i, e), void t.trigger("sync", t, i, e)) : !1
            }, F(this, e), this.sync("read", this, e)
        },
        save: function(e, t, n) {
            var s, a, r, o = this.attributes;
            if (null == e || "object" == typeof e ? (s = e, n = t) : (s = {})[e] = t, n = i.extend({
                validate: !0
            }, n), s && !n.wait) {
                if (!this.set(s, n)) return !1
            } else if (!this._validate(s, n)) return !1;
            s && n.wait && (this.attributes = i.extend({}, o, s)), void 0 === n.parse && (n.parse = !0);
            var l = this,
                c = n.success;
            return n.success = function(e) {
                l.attributes = o;
                var t = l.parse(e, n);
                return n.wait && (t = i.extend(s || {}, t)), i.isObject(t) && !l.set(t, n) ? !1 : (c && c(l, e, n), void l.trigger("sync", l, e, n))
            }, F(this, n), a = this.isNew() ? "create" : n.patch ? "patch" : "update", "patch" === a && (n.attrs = s), r = this.sync(a, this, n), s && n.wait && (this.attributes = o), r
        },
        destroy: function(e) {
            e = e ? i.clone(e) : {};
            var t = this,
                n = e.success,
                s = function() {
                    t.trigger("destroy", t, t.collection, e)
                };
            if (e.success = function(i) {
                (e.wait || t.isNew()) && s(), n && n(t, i, e), t.isNew() || t.trigger("sync", t, i, e)
            }, this.isNew()) return e.success(), !1;
            F(this, e);
            var a = this.sync("delete", this, e);
            return e.wait || s(), a
        },
        url: function() {
            var e = i.result(this, "urlRoot") || i.result(this.collection, "url") || N();
            return this.isNew() ? e : e.replace(/([^\/])$/, "$1/") + encodeURIComponent(this.id)
        },
        parse: function(e) {
            return e
        },
        clone: function() {
            return new this.constructor(this.attributes)
        },
        isNew: function() {
            return !this.has(this.idAttribute)
        },
        isValid: function(e) {
            return this._validate({}, i.extend(e || {}, {
                validate: !0
            }))
        },
        _validate: function(e, t) {
            if (!t.validate || !this.validate) return !0;
            e = i.extend({}, this.attributes, e);
            var n = this.validationError = this.validate(e, t) || null;
            return n ? (this.trigger("invalid", this, n, i.extend(t, {
                validationError: n
            })), !1) : !0
        }
    });
    var p = ["keys", "values", "pairs", "invert", "pick", "omit"];
    i.each(p, function(e) {
        h.prototype[e] = function() {
            var t = r.call(arguments);
            return t.unshift(this.attributes), i[e].apply(i, t)
        }
    });
    var m = t.Collection = function(e, t) {
        t || (t = {}), t.model && (this.model = t.model), void 0 !== t.comparator && (this.comparator = t.comparator), this._reset(), this.initialize.apply(this, arguments), e && this.reset(e, i.extend({
            silent: !0
        }, t))
    }, f = {
            add: !0,
            remove: !0,
            merge: !0
        }, g = {
            add: !0,
            remove: !1
        };
    i.extend(m.prototype, o, {
        model: h,
        initialize: function() {},
        toJSON: function(e) {
            return this.map(function(t) {
                return t.toJSON(e)
            })
        },
        sync: function() {
            return t.sync.apply(this, arguments)
        },
        add: function(e, t) {
            return this.set(e, i.extend({
                merge: !1
            }, t, g))
        },
        remove: function(e, t) {
            var n = !i.isArray(e);
            e = n ? [e] : i.clone(e), t || (t = {});
            var s, a, r, o;
            for (s = 0, a = e.length; a > s; s++) o = e[s] = this.get(e[s]), o && (delete this._byId[o.id], delete this._byId[o.cid], r = this.indexOf(o), this.models.splice(r, 1), this.length--, t.silent || (t.index = r, o.trigger("remove", o, this, t)), this._removeReference(o, t));
            return n ? e[0] : e
        },
        set: function(e, t) {
            t = i.defaults({}, t, f), t.parse && (e = this.parse(e, t));
            var n = !i.isArray(e);
            e = n ? e ? [e] : [] : i.clone(e);
            var s, a, r, o, l, c, d, u = t.at,
                p = this.model,
                m = this.comparator && null == u && t.sort !== !1,
                g = i.isString(this.comparator) ? this.comparator : null,
                v = [],
                y = [],
                b = {}, w = t.add,
                x = t.merge,
                k = t.remove,
                S = !m && w && k ? [] : !1;
            for (s = 0, a = e.length; a > s; s++) {
                if (l = e[s] || {}, r = l instanceof h ? o = l : l[p.prototype.idAttribute || "id"], c = this.get(r)) k && (b[c.cid] = !0), x && (l = l === o ? o.attributes : l, t.parse && (l = c.parse(l, t)), c.set(l, t), m && !d && c.hasChanged(g) && (d = !0)), e[s] = c;
                else if (w) {
                    if (o = e[s] = this._prepareModel(l, t), !o) continue;
                    v.push(o), this._addReference(o, t)
                }
                o = c || o, !S || !o.isNew() && b[o.id] || S.push(o), b[o.id] = !0
            }
            if (k) {
                for (s = 0, a = this.length; a > s; ++s) b[(o = this.models[s]).cid] || y.push(o);
                y.length && this.remove(y, t)
            }
            if (v.length || S && S.length)
                if (m && (d = !0), this.length += v.length, null != u)
                    for (s = 0, a = v.length; a > s; s++) this.models.splice(u + s, 0, v[s]);
                else {
                    S && (this.models.length = 0);
                    var C = S || v;
                    for (s = 0, a = C.length; a > s; s++) this.models.push(C[s])
                }
            if (d && this.sort({
                silent: !0
            }), !t.silent) {
                for (s = 0, a = v.length; a > s; s++)(o = v[s]).trigger("add", o, this, t);
                (d || S && S.length) && this.trigger("sort", this, t)
            }
            return n ? e[0] : e
        },
        reset: function(e, t) {
            t || (t = {});
            for (var n = 0, s = this.models.length; s > n; n++) this._removeReference(this.models[n], t);
            return t.previousModels = this.models, this._reset(), e = this.add(e, i.extend({
                silent: !0
            }, t)), t.silent || this.trigger("reset", this, t), e
        },
        push: function(e, t) {
            return this.add(e, i.extend({
                at: this.length
            }, t))
        },
        pop: function(e) {
            var t = this.at(this.length - 1);
            return this.remove(t, e), t
        },
        unshift: function(e, t) {
            return this.add(e, i.extend({
                at: 0
            }, t))
        },
        shift: function(e) {
            var t = this.at(0);
            return this.remove(t, e), t
        },
        slice: function() {
            return r.apply(this.models, arguments)
        },
        get: function(e) {
            return null == e ? void 0 : this._byId[e] || this._byId[e.id] || this._byId[e.cid]
        },
        at: function(e) {
            return this.models[e]
        },
        where: function(e, t) {
            return i.isEmpty(e) ? t ? void 0 : [] : this[t ? "find" : "filter"](function(t) {
                for (var i in e)
                    if (e[i] !== t.get(i)) return !1;
                return !0
            })
        },
        findWhere: function(e) {
            return this.where(e, !0)
        },
        sort: function(e) {
            if (!this.comparator) throw new Error("Cannot sort a set without a comparator");
            return e || (e = {}), i.isString(this.comparator) || 1 === this.comparator.length ? this.models = this.sortBy(this.comparator, this) : this.models.sort(i.bind(this.comparator, this)), e.silent || this.trigger("sort", this, e), this
        },
        pluck: function(e) {
            return i.invoke(this.models, "get", e)
        },
        fetch: function(e) {
            e = e ? i.clone(e) : {}, void 0 === e.parse && (e.parse = !0);
            var t = e.success,
                n = this;
            return e.success = function(i) {
                var s = e.reset ? "reset" : "set";
                n[s](i, e), t && t(n, i, e), n.trigger("sync", n, i, e)
            }, F(this, e), this.sync("read", this, e)
        },
        create: function(e, t) {
            if (t = t ? i.clone(t) : {}, !(e = this._prepareModel(e, t))) return !1;
            t.wait || this.add(e, t);
            var n = this,
                s = t.success;
            return t.success = function(e, i) {
                t.wait && n.add(e, t), s && s(e, i, t)
            }, e.save(null, t), e
        },
        parse: function(e) {
            return e
        },
        clone: function() {
            return new this.constructor(this.models)
        },
        _reset: function() {
            this.length = 0, this.models = [], this._byId = {}
        },
        _prepareModel: function(e, t) {
            if (e instanceof h) return e;
            t = t ? i.clone(t) : {}, t.collection = this;
            var n = new this.model(e, t);
            return n.validationError ? (this.trigger("invalid", this, n.validationError, t), !1) : n
        },
        _addReference: function(e) {
            this._byId[e.cid] = e, null != e.id && (this._byId[e.id] = e), e.collection || (e.collection = this), e.on("all", this._onModelEvent, this)
        },
        _removeReference: function(e) {
            this === e.collection && delete e.collection, e.off("all", this._onModelEvent, this)
        },
        _onModelEvent: function(e, t, i, n) {
            ("add" !== e && "remove" !== e || i === this) && ("destroy" === e && this.remove(t, n), t && e === "change:" + t.idAttribute && (delete this._byId[t.previous(t.idAttribute)], null != t.id && (this._byId[t.id] = t)), this.trigger.apply(this, arguments))
        }
    });
    var v = ["forEach", "each", "map", "collect", "reduce", "foldl", "inject", "reduceRight", "foldr", "find", "detect", "filter", "select", "reject", "every", "all", "some", "any", "include", "contains", "invoke", "max", "min", "toArray", "size", "first", "head", "take", "initial", "rest", "tail", "drop", "last", "without", "difference", "indexOf", "shuffle", "lastIndexOf", "isEmpty", "chain", "sample"];
    i.each(v, function(e) {
        m.prototype[e] = function() {
            var t = r.call(arguments);
            return t.unshift(this.models), i[e].apply(i, t)
        }
    });
    var y = ["groupBy", "countBy", "sortBy", "indexBy"];
    i.each(y, function(e) {
        m.prototype[e] = function(t, n) {
            var s = i.isFunction(t) ? t : function(e) {
                    return e.get(t)
                };
            return i[e](this.models, s, n)
        }
    });
    var b = t.View = function(e) {
        this.cid = i.uniqueId("view"), e || (e = {}), i.extend(this, i.pick(e, x)), this._ensureElement(), this.initialize.apply(this, arguments), this.delegateEvents()
    }, w = /^(\S+)\s*(.*)$/,
        x = ["model", "collection", "el", "id", "attributes", "className", "tagName", "events"];
    i.extend(b.prototype, o, {
        tagName: "div",
        $: function(e) {
            return this.$el.find(e)
        },
        initialize: function() {},
        render: function() {
            return this
        },
        remove: function() {
            return this.$el.remove(), this.stopListening(), this
        },
        setElement: function(e, i) {
            return this.$el && this.undelegateEvents(), this.$el = e instanceof t.$ ? e : t.$(e), this.el = this.$el[0], i !== !1 && this.delegateEvents(), this
        },
        delegateEvents: function(e) {
            if (!e && !(e = i.result(this, "events"))) return this;
            this.undelegateEvents();
            for (var t in e) {
                var n = e[t];
                if (i.isFunction(n) || (n = this[e[t]]), n) {
                    var s = t.match(w),
                        a = s[1],
                        r = s[2];
                    n = i.bind(n, this), a += ".delegateEvents" + this.cid, "" === r ? this.$el.on(a, n) : this.$el.on(a, r, n)
                }
            }
            return this
        },
        undelegateEvents: function() {
            return this.$el.off(".delegateEvents" + this.cid), this
        },
        _ensureElement: function() {
            if (this.el) this.setElement(i.result(this, "el"), !1);
            else {
                var e = i.extend({}, i.result(this, "attributes"));
                this.id && (e.id = i.result(this, "id")), this.className && (e["class"] = i.result(this, "className"));
                var n = t.$("<" + i.result(this, "tagName") + ">").attr(e);
                this.setElement(n, !1)
            }
        }
    }), t.sync = function(e, n, s) {
        var a = S[e];
        i.defaults(s || (s = {}), {
            emulateHTTP: t.emulateHTTP,
            emulateJSON: t.emulateJSON
        });
        var r = {
            type: a,
            dataType: "json"
        };
        if (s.url || (r.url = i.result(n, "url") || N()), null != s.data || !n || "create" !== e && "update" !== e && "patch" !== e || (r.contentType = "application/json", r.data = JSON.stringify(s.attrs || n.toJSON(s))), s.emulateJSON && (r.contentType = "application/x-www-form-urlencoded", r.data = r.data ? {
            model: r.data
        } : {}), s.emulateHTTP && ("PUT" === a || "DELETE" === a || "PATCH" === a)) {
            r.type = "POST", s.emulateJSON && (r.data._method = a);
            var o = s.beforeSend;
            s.beforeSend = function(e) {
                return e.setRequestHeader("X-HTTP-Method-Override", a), o ? o.apply(this, arguments) : void 0
            }
        }
        "GET" === r.type || s.emulateJSON || (r.processData = !1), "PATCH" === r.type && k && (r.xhr = function() {
            return new ActiveXObject("Microsoft.XMLHTTP")
        });
        var l = s.xhr = t.ajax(i.extend(r, s));
        return n.trigger("request", n, l, s), l
    };
    var k = !("undefined" == typeof window || !window.ActiveXObject || window.XMLHttpRequest && (new XMLHttpRequest).dispatchEvent),
        S = {
            create: "POST",
            update: "PUT",
            patch: "PATCH",
            "delete": "DELETE",
            read: "GET"
        };
    t.ajax = function() {
        return t.$.ajax.apply(t.$, arguments)
    };
    var C = t.Router = function(e) {
        e || (e = {}), e.routes && (this.routes = e.routes), this._bindRoutes(), this.initialize.apply(this, arguments)
    }, P = /\((.*?)\)/g,
        A = /(\(\?)?:\w+/g,
        _ = /\*\w+/g,
        T = /[\-{}\[\]+?.,\\\^$|#\s]/g;
    i.extend(C.prototype, o, {
        initialize: function() {},
        route: function(e, n, s) {
            i.isRegExp(e) || (e = this._routeToRegExp(e)), i.isFunction(n) && (s = n, n = ""), s || (s = this[n]);
            var a = this;
            return t.history.route(e, function(i) {
                var r = a._extractParameters(e, i);
                a.execute(s, r), a.trigger.apply(a, ["route:" + n].concat(r)), a.trigger("route", n, r), t.history.trigger("route", a, n, r)
            }), this
        },
        execute: function(e, t) {
            e && e.apply(this, t)
        },
        navigate: function(e, i) {
            return t.history.navigate(e, i), this
        },
        _bindRoutes: function() {
            if (this.routes) {
                this.routes = i.result(this, "routes");
                for (var e, t = i.keys(this.routes); null != (e = t.pop());) this.route(e, this.routes[e])
            }
        },
        _routeToRegExp: function(e) {
            return e = e.replace(T, "\\$&").replace(P, "(?:$1)?").replace(A, function(e, t) {
                return t ? e : "([^/?]+)"
            }).replace(_, "([^?]*?)"), new RegExp("^" + e + "(?:\\?([\\s\\S]*))?$")
        },
        _extractParameters: function(e, t) {
            var n = e.exec(t).slice(1);
            return i.map(n, function(e, t) {
                return t === n.length - 1 ? e || null : e ? decodeURIComponent(e) : null
            })
        }
    });
    var I = t.History = function() {
        this.handlers = [], i.bindAll(this, "checkUrl"), "undefined" != typeof window && (this.location = window.location, this.history = window.history)
    }, M = /^[#\/]|\s+$/g,
        D = /^\/+|\/+$/g,
        E = /msie [\w.]+/,
        L = /\/$/,
        R = /#.*$/;
    I.started = !1, i.extend(I.prototype, o, {
        interval: 50,
        atRoot: function() {
            return this.location.pathname.replace(/[^\/]$/, "$&/") === this.root
        },
        getHash: function(e) {
            var t = (e || this).location.href.match(/#(.*)$/);
            return t ? t[1] : ""
        },
        getFragment: function(e, t) {
            if (null == e)
                if (this._hasPushState || !this._wantsHashChange || t) {
                    e = decodeURI(this.location.pathname + this.location.search);
                    var i = this.root.replace(L, "");
                    e.indexOf(i) || (e = e.slice(i.length))
                } else e = this.getHash();
            return e.replace(M, "")
        },
        start: function(e) {
            if (I.started) throw new Error("Backbone.history has already been started");
            I.started = !0, this.options = i.extend({
                root: "/"
            }, this.options, e), this.root = this.options.root, this._wantsHashChange = this.options.hashChange !== !1, this._wantsPushState = !! this.options.pushState, this._hasPushState = !! (this.options.pushState && this.history && this.history.pushState);
            var n = this.getFragment(),
                s = document.documentMode,
                a = E.exec(navigator.userAgent.toLowerCase()) && (!s || 7 >= s);
            if (this.root = ("/" + this.root + "/").replace(D, "/"), a && this._wantsHashChange) {
                var r = t.$('<iframe src="javascript:0" tabindex="-1">');
                this.iframe = r.hide().appendTo("body")[0].contentWindow, this.navigate(n)
            }
            this._hasPushState ? t.$(window).on("popstate", this.checkUrl) : this._wantsHashChange && "onhashchange" in window && !a ? t.$(window).on("hashchange", this.checkUrl) : this._wantsHashChange && (this._checkUrlInterval = setInterval(this.checkUrl, this.interval)), this.fragment = n;
            var o = this.location;
            if (this._wantsHashChange && this._wantsPushState) {
                if (!this._hasPushState && !this.atRoot()) return this.fragment = this.getFragment(null, !0), this.location.replace(this.root + "#" + this.fragment), !0;
                this._hasPushState && this.atRoot() && o.hash && (this.fragment = this.getHash().replace(M, ""), this.history.replaceState({}, document.title, this.root + this.fragment))
            }
            return this.options.silent ? void 0 : this.loadUrl()
        },
        stop: function() {
            t.$(window).off("popstate", this.checkUrl).off("hashchange", this.checkUrl), this._checkUrlInterval && clearInterval(this._checkUrlInterval), I.started = !1
        },
        route: function(e, t) {
            this.handlers.unshift({
                route: e,
                callback: t
            })
        },
        checkUrl: function() {
            var e = this.getFragment();
            return e === this.fragment && this.iframe && (e = this.getFragment(this.getHash(this.iframe))), e === this.fragment ? !1 : (this.iframe && this.navigate(e), void this.loadUrl())
        },
        loadUrl: function(e) {
            return e = this.fragment = this.getFragment(e), i.any(this.handlers, function(t) {
                return t.route.test(e) ? (t.callback(e), !0) : void 0
            })
        },
        navigate: function(e, t) {
            if (!I.started) return !1;
            t && t !== !0 || (t = {
                trigger: !! t
            });
            var i = this.root + (e = this.getFragment(e || ""));
            if (e = e.replace(R, ""), this.fragment !== e) {
                if (this.fragment = e, "" === e && "/" !== i && (i = i.slice(0, -1)), this._hasPushState) this.history[t.replace ? "replaceState" : "pushState"]({}, document.title, i);
                else {
                    if (!this._wantsHashChange) return this.location.assign(i);
                    this._updateHash(this.location, e, t.replace), this.iframe && e !== this.getFragment(this.getHash(this.iframe)) && (t.replace || this.iframe.document.open().close(), this._updateHash(this.iframe.location, e, t.replace))
                }
                return t.trigger ? this.loadUrl(e) : void 0
            }
        },
        _updateHash: function(e, t, i) {
            if (i) {
                var n = e.href.replace(/(javascript:|#).*$/, "");
                e.replace(n + "#" + t)
            } else e.hash = "#" + t
        }
    }), t.history = new I;
    var O = function(e, t) {
        var n, s = this;
        n = e && i.has(e, "constructor") ? e.constructor : function() {
            return s.apply(this, arguments)
        }, i.extend(n, s, t);
        var a = function() {
            this.constructor = n
        };
        return a.prototype = s.prototype, n.prototype = new a, e && i.extend(n.prototype, e), n.__super__ = s.prototype, n
    };
    h.extend = m.extend = C.extend = b.extend = I.extend = O;
    var N = function() {
        throw new Error('A "url" property or function must be specified')
    }, F = function(e, t) {
            var i = t.error;
            t.error = function(n) {
                i && i(e, n, t), e.trigger("error", e, n, t)
            }
        };
    return t
}), define("plex/base/utils/BaseClass", ["underscore"], function(e) {
    "use strict";

    function t() {
        e.isFunction(this.initialize) && this.initialize.apply(this, arguments)
    }
    var i = function(t, i) {
        var n, s = this;
        n = t && e.has(t, "constructor") ? t.constructor : function() {
            return s.apply(this, arguments) || null
        }, e.extend(n, s, i);
        var a = function() {
            this.constructor = n
        };
        if (a.prototype = s.prototype, n.prototype = new a, t && t.hasOwnProperty("mixins")) {
            var r = t.mixins;
            delete t.mixins, e.isArray(r) || (r = [r]), e.each(r, function(t) {
                e.extend(n.prototype, t)
            })
        }
        return t && e.extend(n.prototype, t), n.__super__ = s.prototype, n
    };
    return t.extend = i, t
})