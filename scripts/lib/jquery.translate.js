define([
	"jquery", 
	"modernizr"
], function($, mod) {
    "use strict";
	
    $.fn.translate = mod.csstransforms3d ? function(e, t) {
        this.css("transform", "translate3d(" + e + "px, " + t + "px, 0)");
    } : function(e, t) {
        this.css("transform", "translate(" + e + "px, " + t + "px)");
    }
});