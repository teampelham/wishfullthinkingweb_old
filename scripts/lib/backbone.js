define([
	"underscore", 
	"jquery", 
	"backbone-lib", 
	"base/utils/baseClass", 
	"base/mixins/views/bindingsMixin"
], function(_, $, bb, baseClass, bindingsMixin) {
    "use strict";
    
	bb = bb.noConflict();		// run backbone in module form
    var syncHolder = bb.sync;	// hold the original method, we're about to override it
	
    bb.sync = function(op, model, syncParams) {	// override default method
        if (!syncParams.url) {
            if (model.syncUrl) 
                syncParams.url = _.result(model, "syncUrl");
            else if (model.url) 
                syncParams.url = _.result(model, "url");
        } 
        
        if (_.isObject(syncParams.data))  
            syncParams.data = $.param(syncParams.data, true); 
        
        _.defaults(syncParams, {
            headers: {},
            dataType: "text",
            processData: false,
            timeout: syncParams.data && syncParams.data.indexOf("checkFiles=1") > -1 ? 6e4 : 15e3
        });
        
        var server = syncParams.server || model.server || model;
        
        if (_.isFunction(server.resolveUrl))  {
            syncParams.url = server.resolveUrl(syncParams.url);
            if ("cloudsync" === server.get("platform")) {  
                syncParams.url += syncParams.url.indexOf("?") > -1 ? "&" : "?"; 
                syncParams.url += "redirect=0";
            } 
            _.extend(syncParams.headers, server.getHeaders());
        }
        
        var originalRv = syncHolder(op, model, syncParams);
        originalRv.fail(function(response) {
            if (!response.aborting && (0 === response.status || 401 === response.status))
                server.trigger("error:connection", server);
        });
        
        return originalRv;
    };
	

	bb.wrapError = function(superClass, deferred) { 
		var errorCb = deferred.error;         // Capture the original callback
        deferred.error = function(err) {      // Add a new callback and call in the original 
            if(errorCb)
                errorCb(superClass, err, deferred);
                
            superClass.trigger("error", superClass, err, deferred); // allow the original caller to capture the error event too
        }
    }; 
	
	_.extend(bb.View.prototype, bindingsMixin);
	bb.Model.extend = baseClass.extend;
	bb.Collection.extend = baseClass.extend;
	bb.Router.extend = baseClass.extend;
	bb.View.extend = baseClass.extend;
	bb.History.extend = baseClass.extend;
	
	return bb;
});