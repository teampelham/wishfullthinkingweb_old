define([
	"underscore", 
	"handlebars-lib", 
	"base/utils/t", 
	"base/utils/formatters/stringUtil", 
	"base/utils/formatters/dateTimeUtil", 
	"base/utils/formatters/metadataUtil"
], function(_, hb, t, stringUtil, dateTimeUtil, metadataUtil) {
    "use strict";
	
    hb.registerHelper("if_eq", function(compare1, compare2, callback) {
        return compare1 == compare2 ? callback.fn(this) : callback.inverse(this);
    });
	
	hb.registerHelper("if_neq", function(compare1, compare2, callback) {
        return compare1 != compare2 ? callback.fn(this) : callback.inverse(this);
    });
	 
	hb.registerHelper("t", function() {
        var firstArg = _.initial(arguments).concat({
            escape: true
        });
        return new hb.SafeString(t.apply(t, firstArg));
    }); 
	
	hb.registerHelper("csl", function(e) {
		console.warn('hb');
        //return e.join(", ")
    }); 
	
	hb.registerHelper("encodeUrl", function(str) {
        return encodeURIComponent(str || "");
    }); 
	
	hb.registerHelper("tags", function(t) {
		console.warn('hb');
        //return e.pluck(t, "tag").join(",")
    });
	
	hb.registerHelper("json", function(e, t) {
		console.warn('hb');
        //return 2 !== arguments.length || t || (t = 2), 1 === arguments.length && (e = this), JSON.stringify(e, null, t)
    });
	
    var utils = [{
        util: stringUtil,
        helpers: ["truncate", "truncateMiddle", "formatLabel", "formatBoolean", "upperCase", "getLastSegment", "shortFilename"]
    }, 
	{
        util: metadataUtil,
        helpers: ["formatBitrate", "formatVideoQuality", "formatBytes", "formatVideoLevel", "formatResolution", "formatAudioCodec", "formatAudioChannels", "formatStreamLanguage"]
    }, 
	{
        util: dateTimeUtil,
        helpers: ["formatDate", "formatTimestamp", "formatDuration", "formatTimeAgo"]
    }];
	
	
    _.each(utils, function(utilFn) {					// each util object 
        _.each(utilFn.helpers, function(helper) {		// for each helper thingy
            hb.registerHelper(helper, function() {
				// ghetto interface, all the methods of the util class are represented in the string array
                return utilFn.util[helper].apply(utilFn.util, _.initial(arguments));
            });
        });
    });
	
	hb.registerHelper("partial", function(i, n) {
		console.warn('hb');
        /*var s = t.partials[i],
            a = e.extend({}, n.hash.context || this, n.hash);
        return new t.SafeString(s(a))*/
    });
	
	return hb;
});