define(["underscore", "handlebars-lib", "text"], function(e, t, i) {
    "use strict";
    var n = {}, s = function(s, a, r, o) {
            if (o.isBuild && "undefined" != typeof process && process.versions && process.versions.node) {
                var l = require.nodeRequire("fs"),
                    c = e.chain(o.templatePathPrefixs).map(function(e) {
                        return e + s + ".tpl"
                    }).filter(function(e) {
                        return l.existsSync(e)
                    }).map(function(e) {
                        return l.readFileSync(e, "utf8")
                    }).first().value(),
                    d = t.precompile(c);
                n[s] = d, r(d)
            } else i.get(a.toUrl(s + ".tpl"), function(e) {
                var i = t.compile(e);
                r(i)
            })
        }, a = function(e, t, i) {
            if (t in n) {
                var s = n[t];
                i('define("' + e + "!" + t + '", ["handlebars"], function (Handlebars){return Handlebars.template(' + s + ");});\n")
            }
        }, r = function(e, t) {
            return ".tpl" == e.substr(e.length - 4, 4) && (e = e.substr(0, e.length - 4)), t(e)
        };
    return {
        load: s,
        write: a,
        normalize: r
    }
});