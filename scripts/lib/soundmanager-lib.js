var SM2_DEFER = !0;
! function(e, t) {
    "use strict";

    function i(i, n) {
        function s(e) {
            return pt.preferFlash && at && !pt.ignoreFlash && pt.flash[e] !== t && pt.flash[e]
        }

        function a(e) {
            return function(t) {
                var i, n = this._s;
                return i = n && n._a ? e.call(this, t) : null
            }
        }
        this.setupOptions = {
            url: i || null,
            flashVersion: 8,
            debugMode: !0,
            debugFlash: !1,
            useConsole: !0,
            consoleOnly: !0,
            waitForWindowLoad: !1,
            bgColor: "#ffffff",
            useHighPerformance: !1,
            flashPollingInterval: null,
            html5PollingInterval: null,
            flashLoadTimeout: 1e3,
            wmode: null,
            allowScriptAccess: "always",
            useFlashBlock: !1,
            useHTML5Audio: !0,
            forceUseGlobalHTML5Audio: !1,
            ignoreMobileRestrictions: !1,
            html5Test: /^(probably|maybe)$/i,
            preferFlash: !1,
            noSWFCache: !1,
            idPrefix: "sound"
        }, this.defaultOptions = {
            autoLoad: !1,
            autoPlay: !1,
            from: null,
            loops: 1,
            onid3: null,
            onload: null,
            whileloading: null,
            onplay: null,
            onpause: null,
            onresume: null,
            whileplaying: null,
            onposition: null,
            onstop: null,
            onfailure: null,
            onfinish: null,
            multiShot: !0,
            multiShotEvents: !1,
            position: null,
            pan: 0,
            stream: !0,
            to: null,
            type: null,
            usePolicyFile: !1,
            volume: 100
        }, this.flash9Options = {
            isMovieStar: null,
            usePeakData: !1,
            useWaveformData: !1,
            useEQData: !1,
            onbufferchange: null,
            ondataerror: null
        }, this.movieStarOptions = {
            bufferTime: 3,
            serverURL: null,
            onconnect: null,
            duration: null
        }, this.audioFormats = {
            mp3: {
                type: ['audio/mpeg; codecs="mp3"', "audio/mpeg", "audio/mp3", "audio/MPA", "audio/mpa-robust"],
                required: !0
            },
            mp4: {
                related: ["aac", "m4a", "m4b"],
                type: ['audio/mp4; codecs="mp4a.40.2"', "audio/aac", "audio/x-m4a", "audio/MP4A-LATM", "audio/mpeg4-generic"],
                required: !1
            },
            ogg: {
                type: ["audio/ogg; codecs=vorbis"],
                required: !1
            },
            opus: {
                type: ["audio/ogg; codecs=opus", "audio/opus"],
                required: !1
            },
            wav: {
                type: ['audio/wav; codecs="1"', "audio/wav", "audio/wave", "audio/x-wav"],
                required: !1
            }
        }, this.movieID = "sm2-container", this.id = n || "sm2movie", this.debugID = "soundmanager-debug", this.debugURLParam = /([#?&])debug=1/i, this.versionNumber = "V2.97a.20150601", this.version = null, this.movieURL = null, this.altURL = null, this.swfLoaded = !1, this.enabled = !1, this.oMC = null, this.sounds = {}, this.soundIDs = [], this.muted = !1, this.didFlashBlock = !1, this.filePattern = null, this.filePatterns = {
            flash8: /\.mp3(\?.*)?$/i,
            flash9: /\.mp3(\?.*)?$/i
        }, this.features = {
            buffering: !1,
            peakData: !1,
            waveformData: !1,
            eqData: !1,
            movieStar: !1
        }, this.sandbox = {}, this.html5 = {
            usingFlash: null
        }, this.flash = {}, this.html5Only = !1, this.ignoreFlash = !1;
        var r, o, l, c, d, u, h, p, m, f, g, v, y, b, w, x, k, S, C, P, A, _, T, I, M, D, E, L, R, O, N, F, V, B, U, z, j, q, H, W, $, Y, Q, K, X, G, J, Z, et, tt, it, nt, st, at, rt, ot, lt, ct, dt, ut, ht, pt = this,
            mt = null,
            ft = null,
            gt = navigator.userAgent,
            vt = e.location.href.toString(),
            yt = document,
            bt = [],
            wt = !1,
            xt = !1,
            kt = !1,
            St = !1,
            Ct = !1,
            Pt = 8,
            At = null,
            _t = null,
            Tt = !1,
            It = !1,
            Mt = 0,
            Dt = null,
            Et = [],
            Lt = null,
            Rt = Array.prototype.slice,
            Ot = !1,
            Nt = 0,
            Ft = 1e3,
            Vt = gt.match(/(ipad|iphone|ipod)/i),
            Bt = gt.match(/android/i),
            Ut = gt.match(/msie/i),
            zt = gt.match(/webkit/i),
            jt = gt.match(/safari/i) && !gt.match(/chrome/i),
            qt = gt.match(/opera/i),
            Ht = gt.match(/(mobile|pre\/|xoom)/i) || Vt || Bt,
            Wt = !vt.match(/usehtml5audio/i) && !vt.match(/sm2\-ignorebadua/i) && jt && !gt.match(/silk/i) && gt.match(/OS X 10_6_([3-7])/i),
            $t = (e.console !== t && console.log !== t, yt.hasFocus !== t ? yt.hasFocus() : null),
            Yt = jt && (yt.hasFocus === t || !yt.hasFocus()),
            Qt = !Yt,
            Kt = /(mp3|mp4|mpa|m4a|m4b)/i,
            Xt = "about:blank",
            Gt = "data:audio/wave;base64,/UklGRiYAAABXQVZFZm10IBAAAAABAAEARKwAAIhYAQACABAAZGF0YQIAAAD//w==",
            Jt = yt.location ? yt.location.protocol.match(/http/i) : null,
            Zt = Jt ? "" : "http://",
            ei = /^\s*audio\/(?:x-)?(?:mpeg4|aac|flv|mov|mp4||m4v|m4a|m4b|mp4v|3gp|3g2)\s*(?:$|;)/i,
            ti = ["mpeg4", "aac", "flv", "mov", "mp4", "m4v", "f4v", "m4a", "m4b", "mp4v", "3gp", "3g2"],
            ii = new RegExp("\\.(" + ti.join("|") + ")(\\?.*)?$", "i");
        this.mimePattern = /^\s*audio\/(?:x-)?(?:mp(?:eg|3))\s*(?:$|;)/i, this.useAltURL = !Jt, U = {
            swfBox: "sm2-object-box",
            swfDefault: "movieContainer",
            swfError: "swf_error",
            swfTimedout: "swf_timedout",
            swfLoaded: "swf_loaded",
            swfUnblocked: "swf_unblocked",
            sm2Debug: "sm2_debug",
            highPerf: "high_performance",
            flashDebug: "flash_debug"
        }, this.hasHTML5 = function() {
            try {
                return Audio !== t && (qt && opera !== t && opera.version() < 10 ? new Audio(null) : new Audio).canPlayType !== t
            } catch (e) {
                return !1
            }
        }(), this.setup = function(e) {
            var i = !pt.url;
            return e !== t && kt && Lt && pt.ok() && (e.flashVersion !== t || e.url !== t || e.html5Test !== t), m(e), Ot || (Ht ? (!pt.setupOptions.ignoreMobileRestrictions || pt.setupOptions.forceUseGlobalHTML5Audio) && (Et.push(C.globalHTML5), Ot = !0) : pt.setupOptions.forceUseGlobalHTML5Audio && (Et.push(C.globalHTML5), Ot = !0)), !ht && Ht && (pt.setupOptions.ignoreMobileRestrictions ? Et.push(C.ignoreMobile) : (pt.setupOptions.useHTML5Audio = !0, pt.setupOptions.preferFlash = !1, Vt ? pt.ignoreFlash = !0 : (Bt && !gt.match(/android\s2\.3/i) || !Bt) && (Ot = !0))), e && (i && T && e.url !== t && pt.beginDelayedInit(), T || e.url === t || "complete" !== yt.readyState || setTimeout(A, 1)), ht = !0, pt
        }, this.ok = function() {
            return Lt ? kt && !St : pt.useHTML5Audio && pt.hasHTML5
        }, this.supported = this.ok, this.getMovie = function(t) {
            return o(t) || yt[t] || e[t]
        }, this.createSound = function(e, i) {
            function n() {
                return a = z(a), pt.sounds[a.id] = new r(a), pt.soundIDs.push(a.id), pt.sounds[a.id]
            }
            var s, a, o = null;
            if (!kt || !pt.ok()) return !1;
            if (i !== t && (e = {
                id: e,
                url: i
            }), a = p(e), a.url = Q(a.url), a.id === t && (a.id = pt.setupOptions.idPrefix + Nt++), H(a.id, !0)) return pt.sounds[a.id];
            if (G(a)) o = n(), o._setup_html5(a);
            else {
                if (pt.html5Only) return n();
                if (pt.html5.usingFlash && a.url && a.url.match(/data\:/i)) return n();
                u > 8 && null === a.isMovieStar && (a.isMovieStar = !! (a.serverURL || (a.type ? a.type.match(ei) : !1) || a.url && a.url.match(ii))), a = j(a, s), o = n(), 8 === u ? ft._createSound(a.id, a.loops || 1, a.usePolicyFile) : (ft._createSound(a.id, a.url, a.usePeakData, a.useWaveformData, a.useEQData, a.isMovieStar, a.isMovieStar ? a.bufferTime : !1, a.loops || 1, a.serverURL, a.duration || null, a.autoPlay, !0, a.autoLoad, a.usePolicyFile), a.serverURL || (o.connected = !0, a.onconnect && a.onconnect.apply(o))), a.serverURL || !a.autoLoad && !a.autoPlay || o.load(a)
            }
            return !a.serverURL && a.autoPlay && o.play(), o
        }, this.destroySound = function(e, t) {
            if (!H(e)) return !1;
            var i, n = pt.sounds[e];
            for (n.stop(), n._iO = {}, n.unload(), i = 0; i < pt.soundIDs.length; i++)
                if (pt.soundIDs[i] === e) {
                    pt.soundIDs.splice(i, 1);
                    break
                }
            return t || n.destruct(!0), n = null, delete pt.sounds[e], !0
        }, this.load = function(e, t) {
            return H(e) ? pt.sounds[e].load(t) : !1
        }, this.unload = function(e) {
            return H(e) ? pt.sounds[e].unload() : !1
        }, this.onPosition = function(e, t, i, n) {
            return H(e) ? pt.sounds[e].onposition(t, i, n) : !1
        }, this.onposition = this.onPosition, this.clearOnPosition = function(e, t, i) {
            return H(e) ? pt.sounds[e].clearOnPosition(t, i) : !1
        }, this.play = function(e, t) {
            var i = null,
                n = t && !(t instanceof Object);
            if (!kt || !pt.ok()) return !1;
            if (H(e, n)) n && (t = {
                url: t
            });
            else {
                if (!n) return !1;
                n && (t = {
                    url: t
                }), t && t.url && (t.id = e, i = pt.createSound(t).play())
            }
            return null === i && (i = pt.sounds[e].play(t)), i
        }, this.start = this.play, this.setPosition = function(e, t) {
            return H(e) ? pt.sounds[e].setPosition(t) : !1
        }, this.stop = function(e) {
            return H(e) ? pt.sounds[e].stop() : !1
        }, this.stopAll = function() {
            var e;
            for (e in pt.sounds) pt.sounds.hasOwnProperty(e) && pt.sounds[e].stop()
        }, this.pause = function(e) {
            return H(e) ? pt.sounds[e].pause() : !1
        }, this.pauseAll = function() {
            var e;
            for (e = pt.soundIDs.length - 1; e >= 0; e--) pt.sounds[pt.soundIDs[e]].pause()
        }, this.resume = function(e) {
            return H(e) ? pt.sounds[e].resume() : !1
        }, this.resumeAll = function() {
            var e;
            for (e = pt.soundIDs.length - 1; e >= 0; e--) pt.sounds[pt.soundIDs[e]].resume()
        }, this.togglePause = function(e) {
            return H(e) ? pt.sounds[e].togglePause() : !1
        }, this.setPan = function(e, t) {
            return H(e) ? pt.sounds[e].setPan(t) : !1
        }, this.setVolume = function(e, i) {
            var n, s; {
                if (e === t || isNaN(e) || i !== t) return H(e) ? pt.sounds[e].setVolume(i) : !1;
                for (n = 0, s = pt.soundIDs.length; s > n; n++) pt.sounds[pt.soundIDs[n]].setVolume(e)
            }
        }, this.mute = function(e) {
            var t = 0;
            if (e instanceof String && (e = null), e) return H(e) ? pt.sounds[e].mute() : !1;
            for (t = pt.soundIDs.length - 1; t >= 0; t--) pt.sounds[pt.soundIDs[t]].mute();
            return pt.muted = !0, !0
        }, this.muteAll = function() {
            pt.mute()
        }, this.unmute = function(e) {
            var t;
            if (e instanceof String && (e = null), e) return H(e) ? pt.sounds[e].unmute() : !1;
            for (t = pt.soundIDs.length - 1; t >= 0; t--) pt.sounds[pt.soundIDs[t]].unmute();
            return pt.muted = !1, !0
        }, this.unmuteAll = function() {
            pt.unmute()
        }, this.toggleMute = function(e) {
            return H(e) ? pt.sounds[e].toggleMute() : !1
        }, this.getMemoryUse = function() {
            var e = 0;
            return ft && 8 !== u && (e = parseInt(ft._getMemoryUse(), 10)), e
        }, this.disable = function(i) {
            var n;
            if (i === t && (i = !1), St) return !1;
            for (St = !0, n = pt.soundIDs.length - 1; n >= 0; n--) R(pt.sounds[pt.soundIDs[n]]);
            return h(i), nt.remove(e, "load", y), !0
        }, this.canPlayMIME = function(e) {
            var t;
            return pt.hasHTML5 && (t = J({
                type: e
            })), !t && Lt && (t = e && pt.ok() ? !! ((u > 8 ? e.match(ei) : null) || e.match(pt.mimePattern)) : null), t
        }, this.canPlayURL = function(e) {
            var t;
            return pt.hasHTML5 && (t = J({
                url: e
            })), !t && Lt && (t = e && pt.ok() ? !! e.match(pt.filePattern) : null), t
        }, this.canPlayLink = function(e) {
            return e.type !== t && e.type && pt.canPlayMIME(e.type) ? !0 : pt.canPlayURL(e.href)
        }, this.getSoundById = function(e) {
            if (!e) return null;
            var t = pt.sounds[e];
            return t
        }, this.onready = function(t, i) {
            var n = "onready",
                s = !1;
            if ("function" != typeof t) throw F("needFunction", n);
            return i || (i = e), g(n, t, i), v(), s = !0, s
        }, this.ontimeout = function(t, i) {
            var n = "ontimeout",
                s = !1;
            if ("function" != typeof t) throw F("needFunction", n);
            return i || (i = e), g(n, t, i), v({
                type: n
            }), s = !0, s
        }, this._writeDebug = function() {
            return !0
        }, this._wD = this._writeDebug, this._debug = function() {}, this.reboot = function(t, i) {
            var n, s, a;
            for (n = pt.soundIDs.length - 1; n >= 0; n--) pt.sounds[pt.soundIDs[n]].destruct();
            if (ft) try {
                Ut && (_t = ft.innerHTML), At = ft.parentNode.removeChild(ft)
            } catch (r) {}
            if (_t = At = Lt = ft = null, pt.enabled = T = kt = Tt = It = wt = xt = St = pt.swfLoaded = !1, pt.soundIDs = [], pt.sounds = {}, Nt = 0, ht = !1, t) bt = [];
            else
                for (n in bt)
                    if (bt.hasOwnProperty(n))
                        for (s = 0, a = bt[n].length; a > s; s++) bt[n][s].fired = !1;
            return pt.html5 = {
                usingFlash: null
            }, pt.flash = {}, pt.html5Only = !1, pt.ignoreFlash = !1, e.setTimeout(function() {
                i || pt.beginDelayedInit()
            }, 20), pt
        }, this.reset = function() {
            return pt.reboot(!0, !0)
        }, this.getMoviePercent = function() {
            return ft && "PercentLoaded" in ft ? ft.PercentLoaded() : null
        }, this.beginDelayedInit = function() {
            Ct = !0, A(), setTimeout(function() {
                return It ? !1 : (M(), P(), It = !0, !0)
            }, 20), b()
        }, this.destruct = function() {
            pt.disable(!0)
        }, r = function(e) {
            var i, n, s, a, r, o, l, c, d, h, m = this,
                f = !1,
                g = [],
                v = 0,
                y = null;
            d = {
                duration: null,
                time: null
            }, this.id = e.id, this.sID = this.id, this.url = e.url, this.options = p(e), this.instanceOptions = this.options, this._iO = this.instanceOptions, this.pan = this.options.pan, this.volume = this.options.volume, this.isHTML5 = !1, this._a = null, h = this.url ? !1 : !0, this.id3 = {}, this._debug = function() {}, this.load = function(e) {
                var i, n = null;
                if (e !== t ? m._iO = p(e, m.options) : (e = m.options, m._iO = e, y && y !== m.url && (m._iO.url = m.url, m.url = null)), m._iO.url || (m._iO.url = m.url), m._iO.url = Q(m._iO.url), m.instanceOptions = m._iO, i = m._iO, !i.url && !m.url) return m;
                if (i.url === m.url && 0 !== m.readyState && 2 !== m.readyState) return 3 === m.readyState && i.onload && ut(m, function() {
                    i.onload.apply(m, [ !! m.duration])
                }), m;
                if (m.loaded = !1, m.readyState = 1, m.playState = 0, m.id3 = {}, G(i)) n = m._setup_html5(i), n._called_load || (m._html5_canplay = !1, m.url !== i.url && (m._a.src = i.url, m.setPosition(0)), m._a.autobuffer = "auto", m._a.preload = "auto", m._a._called_load = !0);
                else {
                    if (pt.html5Only) return m;
                    if (m._iO.url && m._iO.url.match(/data\:/i)) return m;
                    try {
                        m.isHTML5 = !1, m._iO = j(z(i)), m._iO.autoPlay && (m._iO.position || m._iO.from) && (m._iO.autoPlay = !1), i = m._iO, 8 === u ? ft._load(m.id, i.url, i.stream, i.autoPlay, i.usePolicyFile) : ft._load(m.id, i.url, !! i.stream, !! i.autoPlay, i.loops || 1, !! i.autoLoad, i.usePolicyFile)
                    } catch (s) {
                        D({
                            type: "SMSOUND_LOAD_JS_EXCEPTION",
                            fatal: !0
                        })
                    }
                }
                return m.url = i.url, m
            }, this.unload = function() {
                return 0 !== m.readyState && (m.isHTML5 ? (a(), m._a && (m._a.pause(), y = et(m._a))) : 8 === u ? ft._unload(m.id, Xt) : ft._unload(m.id), i()), m
            }, this.destruct = function(e) {
                m.isHTML5 ? (a(), m._a && (m._a.pause(), et(m._a), Ot || s(), m._a._s = null, m._a = null)) : (m._iO.onfailure = null, ft._destroySound(m.id)), e || pt.destroySound(m.id, !0)
            }, this.play = function(e, i) {
                var n, s, a, l, d, g, v = !0,
                    y = null;
                if (i = i === t ? !0 : i, e || (e = {}), m.url && (m._iO.url = m.url), m._iO = p(m._iO, m.options), m._iO = p(e, m._iO), m._iO.url = Q(m._iO.url), m.instanceOptions = m._iO, !m.isHTML5 && m._iO.serverURL && !m.connected) return m.getAutoPlay() || m.setAutoPlay(!0), m;
                if (G(m._iO) && (m._setup_html5(m._iO), r()), 1 !== m.playState || m.paused || (n = m._iO.multiShot, n || (m.isHTML5 && m.setPosition(m._iO.position), y = m)), null !== y) return y;
                if (e.url && e.url !== m.url && (m.readyState || m.isHTML5 || 8 !== u || !h ? m.load(m._iO) : h = !1), m.loaded || (0 === m.readyState ? (m.isHTML5 || pt.html5Only ? m.isHTML5 ? m.load(m._iO) : y = m : (m._iO.autoPlay = !0, m.load(m._iO)), m.instanceOptions = m._iO) : 2 === m.readyState && (y = m)), null !== y) return y;
                if (!m.isHTML5 && 9 === u && m.position > 0 && m.position === m.duration && (e.position = 0), m.paused && m.position >= 0 && (!m._iO.serverURL || m.position > 0)) m.resume();
                else {
                    if (m._iO = p(e, m._iO), (!m.isHTML5 && null !== m._iO.position && m._iO.position > 0 || null !== m._iO.from && m._iO.from > 0 || null !== m._iO.to) && 0 === m.instanceCount && 0 === m.playState && !m._iO.serverURL) {
                        if (a = function() {
                            m._iO = p(e, m._iO), m.play(m._iO)
                        }, m.isHTML5 && !m._html5_canplay ? (m.load({
                            _oncanplay: a
                        }), y = !1) : m.isHTML5 || m.loaded || m.readyState && 2 === m.readyState || (m.load({
                            onload: a
                        }), y = !1), null !== y) return y;
                        m._iO = c()
                    }(!m.instanceCount || m._iO.multiShotEvents || m.isHTML5 && m._iO.multiShot && !Ot || !m.isHTML5 && u > 8 && !m.getAutoPlay()) && m.instanceCount++, m._iO.onposition && 0 === m.playState && o(m), m.playState = 1, m.paused = !1, m.position = m._iO.position === t || isNaN(m._iO.position) ? 0 : m._iO.position, m.isHTML5 || (m._iO = j(z(m._iO))), m._iO.onplay && i && (m._iO.onplay.apply(m), f = !0), m.setVolume(m._iO.volume, !0), m.setPan(m._iO.pan, !0), m.isHTML5 ? m.instanceCount < 2 ? (r(), s = m._setup_html5(), m.setPosition(m._iO.position), s.play()) : (l = new Audio(m._iO.url), d = function() {
                        nt.remove(l, "ended", d), m._onfinish(m), et(l), l = null
                    }, g = function() {
                        nt.remove(l, "canplay", g);
                        try {
                            l.currentTime = m._iO.position / Ft
                        } catch (e) {}
                        l.play()
                    }, nt.add(l, "ended", d), m._iO.volume !== t && (l.volume = Math.max(0, Math.min(1, m._iO.volume / 100))), m.muted && (l.muted = !0), m._iO.position ? nt.add(l, "canplay", g) : l.play()) : (v = ft._start(m.id, m._iO.loops || 1, 9 === u ? m.position : m.position / Ft, m._iO.multiShot || !1), 9 !== u || v || m._iO.onplayerror && m._iO.onplayerror.apply(m))
                }
                return m
            }, this.start = this.play, this.stop = function(e) {
                var t, i = m._iO;
                return 1 === m.playState && (m._onbufferchange(0), m._resetOnPosition(0), m.paused = !1, m.isHTML5 || (m.playState = 0), l(), i.to && m.clearOnPosition(i.to), m.isHTML5 ? m._a && (t = m.position, m.setPosition(0), m.position = t, m._a.pause(), m.playState = 0, m._onTimer(), a()) : (ft._stop(m.id, e), i.serverURL && m.unload()), m.instanceCount = 0, m._iO = {}, i.onstop && i.onstop.apply(m)), m
            }, this.setAutoPlay = function(e) {
                m._iO.autoPlay = e, m.isHTML5 || (ft._setAutoPlay(m.id, e), e && (m.instanceCount || 1 !== m.readyState || m.instanceCount++))
            }, this.getAutoPlay = function() {
                return m._iO.autoPlay
            }, this.setPosition = function(e) {
                e === t && (e = 0);
                var i, n, s = m.isHTML5 ? Math.max(e, 0) : Math.min(m.duration || m._iO.duration, Math.max(e, 0));
                if (m.position = s, n = m.position / Ft, m._resetOnPosition(m.position), m._iO.position = s, m.isHTML5) {
                    if (m._a) {
                        if (m._html5_canplay) {
                            if (m._a.currentTime !== n) try {
                                m._a.currentTime = n, (0 === m.playState || m.paused) && m._a.pause()
                            } catch (a) {}
                        } else if (n) return m;
                        m.paused && m._onTimer(!0)
                    }
                } else i = 9 === u ? m.position : n, m.readyState && 2 !== m.readyState && ft._setPosition(m.id, i, m.paused || !m.playState, m._iO.multiShot);
                return m
            }, this.pause = function(e) {
                return m.paused || 0 === m.playState && 1 !== m.readyState ? m : (m.paused = !0, m.isHTML5 ? (m._setup_html5().pause(), a()) : (e || e === t) && ft._pause(m.id, m._iO.multiShot), m._iO.onpause && m._iO.onpause.apply(m), m)
            }, this.resume = function() {
                var e = m._iO;
                return m.paused ? (m.paused = !1, m.playState = 1, m.isHTML5 ? (m._setup_html5().play(), r()) : (e.isMovieStar && !e.serverURL && m.setPosition(m.position), ft._pause(m.id, e.multiShot)), !f && e.onplay ? (e.onplay.apply(m), f = !0) : e.onresume && e.onresume.apply(m), m) : m
            }, this.togglePause = function() {
                return 0 === m.playState ? (m.play({
                    position: 9 !== u || m.isHTML5 ? m.position / Ft : m.position
                }), m) : (m.paused ? m.resume() : m.pause(), m)
            }, this.setPan = function(e, i) {
                return e === t && (e = 0), i === t && (i = !1), m.isHTML5 || ft._setPan(m.id, e), m._iO.pan = e, i || (m.pan = e, m.options.pan = e), m
            }, this.setVolume = function(e, i) {
                return e === t && (e = 100), i === t && (i = !1), m.isHTML5 ? m._a && (pt.muted && !m.muted && (m.muted = !0, m._a.muted = !0), m._a.volume = Math.max(0, Math.min(1, e / 100))) : ft._setVolume(m.id, pt.muted && !m.muted || m.muted ? 0 : e), m._iO.volume = e, i || (m.volume = e, m.options.volume = e), m
            }, this.mute = function() {
                return m.muted = !0, m.isHTML5 ? m._a && (m._a.muted = !0) : ft._setVolume(m.id, 0), m
            }, this.unmute = function() {
                m.muted = !1;
                var e = m._iO.volume !== t;
                return m.isHTML5 ? m._a && (m._a.muted = !1) : ft._setVolume(m.id, e ? m._iO.volume : m.options.volume), m
            }, this.toggleMute = function() {
                return m.muted ? m.unmute() : m.mute()
            }, this.onPosition = function(e, i, n) {
                return g.push({
                    position: parseInt(e, 10),
                    method: i,
                    scope: n !== t ? n : m,
                    fired: !1
                }), m
            }, this.onposition = this.onPosition, this.clearOnPosition = function(e, t) {
                var i;
                if (e = parseInt(e, 10), isNaN(e)) return !1;
                for (i = 0; i < g.length; i++) e === g[i].position && (t && t !== g[i].method || (g[i].fired && v--, g.splice(i, 1)))
            }, this._processOnPosition = function() {
                var e, t, i = g.length;
                if (!i || !m.playState || v >= i) return !1;
                for (e = i - 1; e >= 0; e--) t = g[e], !t.fired && m.position >= t.position && (t.fired = !0, v++, t.method.apply(t.scope, [t.position]), i = g.length);
                return !0
            }, this._resetOnPosition = function(e) {
                var t, i, n = g.length;
                if (!n) return !1;
                for (t = n - 1; t >= 0; t--) i = g[t], i.fired && e <= i.position && (i.fired = !1, v--);
                return !0
            }, c = function() {
                var e, t, i = m._iO,
                    n = i.from,
                    s = i.to;
                return t = function() {
                    m.clearOnPosition(s, t), m.stop()
                }, e = function() {
                    null === s || isNaN(s) || m.onPosition(s, t)
                }, null === n || isNaN(n) || (i.position = n, i.multiShot = !1, e()), i
            }, o = function() {
                var e, t = m._iO.onposition;
                if (t)
                    for (e in t) t.hasOwnProperty(e) && m.onPosition(parseInt(e, 10), t[e])
            }, l = function() {
                var e, t = m._iO.onposition;
                if (t)
                    for (e in t) t.hasOwnProperty(e) && m.clearOnPosition(parseInt(e, 10))
            }, r = function() {
                m.isHTML5 && W(m)
            }, a = function() {
                m.isHTML5 && $(m)
            }, i = function(e) {
                e || (g = [], v = 0), f = !1, m._hasTimer = null, m._a = null, m._html5_canplay = !1, m.bytesLoaded = null, m.bytesTotal = null, m.duration = m._iO && m._iO.duration ? m._iO.duration : null, m.durationEstimate = null, m.buffered = [], m.eqData = [], m.eqData.left = [], m.eqData.right = [], m.failures = 0, m.isBuffering = !1, m.instanceOptions = {}, m.instanceCount = 0, m.loaded = !1, m.metadata = {}, m.readyState = 0, m.muted = !1, m.paused = !1, m.peakData = {
                    left: 0,
                    right: 0
                }, m.waveformData = {
                    left: [],
                    right: []
                }, m.playState = 0, m.position = null, m.id3 = {}
            }, i(), this._onTimer = function(e) {
                var t, i, n = !1,
                    s = {};
                return m._hasTimer || e ? (m._a && (e || (m.playState > 0 || 1 === m.readyState) && !m.paused) && (t = m._get_html5_duration(), t !== d.duration && (d.duration = t, m.duration = t, n = !0), m.durationEstimate = m.duration, i = m._a.currentTime * Ft || 0, i !== d.time && (d.time = i, n = !0), (n || e) && m._whileplaying(i, s, s, s, s)), n) : void 0
            }, this._get_html5_duration = function() {
                var e = m._iO,
                    t = m._a && m._a.duration ? m._a.duration * Ft : e && e.duration ? e.duration : null,
                    i = t && !isNaN(t) && 1 / 0 !== t ? t : null;
                return i
            }, this._apply_loop = function(e, t) {
                e.loop = t > 1 ? "loop" : ""
            }, this._setup_html5 = function(e) {
                var t, s = p(m._iO, e),
                    a = Ot ? mt : m._a,
                    r = decodeURI(s.url);
                if (Ot ? r === decodeURI(st) && (t = !0) : r === decodeURI(y) && (t = !0), a) {
                    if (a._s)
                        if (Ot) a._s && a._s.playState && !t && a._s.stop();
                        else if (!Ot && r === decodeURI(y)) return m._apply_loop(a, s.loops), a;
                    t || (y && i(!1), a.src = s.url, m.url = s.url, y = s.url, st = s.url, a._called_load = !1)
                } else s.autoLoad || s.autoPlay ? (m._a = new Audio(s.url), m._a.load()) : m._a = qt && opera.version() < 10 ? new Audio(null) : new Audio, a = m._a, a._called_load = !1, Ot && (mt = a);
                return m.isHTML5 = !0, m._a = a, a._s = m, n(), m._apply_loop(a, s.loops), s.autoLoad || s.autoPlay ? m.load() : (a.autobuffer = !1, a.preload = "auto"), a
            }, n = function() {
                function e(e, t, i) {
                    return m._a ? m._a.addEventListener(e, t, i || !1) : null
                }
                if (m._a._added_events) return !1;
                var t;
                m._a._added_events = !0;
                for (t in lt) lt.hasOwnProperty(t) && e(t, lt[t]);
                return !0
            }, s = function() {
                function e(e, t, i) {
                    return m._a ? m._a.removeEventListener(e, t, i || !1) : null
                }
                var t;
                m._a._added_events = !1;
                for (t in lt) lt.hasOwnProperty(t) && e(t, lt[t])
            }, this._onload = function(e) {
                var t = !! e || !m.isHTML5 && 8 === u && m.duration;
                return m.loaded = t, m.readyState = t ? 3 : 2, m._onbufferchange(0), m._iO.onload && ut(m, function() {
                    m._iO.onload.apply(m, [t])
                }), !0
            }, this._onbufferchange = function(e) {
                return 0 === m.playState ? !1 : e && m.isBuffering || !e && !m.isBuffering ? !1 : (m.isBuffering = 1 === e, m._iO.onbufferchange && m._iO.onbufferchange.apply(m, [e]), !0)
            }, this._onsuspend = function() {
                return m._iO.onsuspend && m._iO.onsuspend.apply(m), !0
            }, this._onfailure = function(e, t, i) {
                m.failures++, m._iO.onfailure && 1 === m.failures && m._iO.onfailure(e, t, i)
            }, this._onwarning = function(e, t, i) {
                m._iO.onwarning && m._iO.onwarning(e, t, i)
            }, this._onfinish = function() {
                var e = m._iO.onfinish;
                m._onbufferchange(0), m._resetOnPosition(0), m.instanceCount && (m.instanceCount--, m.instanceCount || (l(), m.playState = 0, m.paused = !1, m.instanceCount = 0, m.instanceOptions = {}, m._iO = {}, a(), m.isHTML5 && (m.position = 0)), (!m.instanceCount || m._iO.multiShotEvents) && e && ut(m, function() {
                    e.apply(m)
                }))
            }, this._whileloading = function(e, t, i, n) {
                var s = m._iO;
                m.bytesLoaded = e, m.bytesTotal = t, m.duration = Math.floor(i), m.bufferLength = n, m.durationEstimate = m.isHTML5 || s.isMovieStar ? m.duration : s.duration ? m.duration > s.duration ? m.duration : s.duration : parseInt(m.bytesTotal / m.bytesLoaded * m.duration, 10), m.isHTML5 || (m.buffered = [{
                    start: 0,
                    end: m.duration
                }]), (3 !== m.readyState || m.isHTML5) && s.whileloading && s.whileloading.apply(m)
            }, this._whileplaying = function(e, i, n, s, a) {
                var r, o = m._iO;
                return isNaN(e) || null === e ? !1 : (m.position = Math.max(0, e), m._processOnPosition(), !m.isHTML5 && u > 8 && (o.usePeakData && i !== t && i && (m.peakData = {
                    left: i.leftPeak,
                    right: i.rightPeak
                }), o.useWaveformData && n !== t && n && (m.waveformData = {
                    left: n.split(","),
                    right: s.split(",")
                }), o.useEQData && a !== t && a && a.leftEQ && (r = a.leftEQ.split(","), m.eqData = r, m.eqData.left = r, a.rightEQ !== t && a.rightEQ && (m.eqData.right = a.rightEQ.split(",")))), 1 === m.playState && (m.isHTML5 || 8 !== u || m.position || !m.isBuffering || m._onbufferchange(0), o.whileplaying && o.whileplaying.apply(m)), !0)
            }, this._oncaptiondata = function(e) {
                m.captiondata = e, m._iO.oncaptiondata && m._iO.oncaptiondata.apply(m, [e])
            }, this._onmetadata = function(e, t) {
                var i, n, s = {};
                for (i = 0, n = e.length; n > i; i++) s[e[i]] = t[i];
                m.metadata = s, m._iO.onmetadata && m._iO.onmetadata.call(m, m.metadata)
            }, this._onid3 = function(e, t) {
                var i, n, s = [];
                for (i = 0, n = e.length; n > i; i++) s[e[i]] = t[i];
                m.id3 = p(m.id3, s), m._iO.onid3 && m._iO.onid3.apply(m)
            }, this._onconnect = function(e) {
                e = 1 === e, m.connected = e, e && (m.failures = 0, H(m.id) && (m.getAutoPlay() ? m.play(t, m.getAutoPlay()) : m._iO.autoLoad && m.load()), m._iO.onconnect && m._iO.onconnect.apply(m, [e]))
            }, this._ondataerror = function() {
                m.playState > 0 && m._iO.ondataerror && m._iO.ondataerror.apply(m)
            }
        }, I = function() {
            return yt.body || yt.getElementsByTagName("div")[0]
        }, o = function(e) {
            return yt.getElementById(e)
        }, p = function(e, i) {
            var n, s, a = e || {};
            n = i === t ? pt.defaultOptions : i;
            for (s in n) n.hasOwnProperty(s) && a[s] === t && (a[s] = "object" != typeof n[s] || null === n[s] ? n[s] : p(a[s], n[s]));
            return a
        }, ut = function(t, i) {
            t.isHTML5 || 8 !== u ? i() : e.setTimeout(i, 0)
        }, f = {
            onready: 1,
            ontimeout: 1,
            defaultOptions: 1,
            flash9Options: 1,
            movieStarOptions: 1
        }, m = function(e, i) {
            var n, s = !0,
                a = i !== t,
                r = pt.setupOptions,
                o = f;
            for (n in e)
                if (e.hasOwnProperty(n))
                    if ("object" != typeof e[n] || null === e[n] || e[n] instanceof Array || e[n] instanceof RegExp) a && o[i] !== t ? pt[i][n] = e[n] : r[n] !== t ? (pt.setupOptions[n] = e[n], pt[n] = e[n]) : o[n] === t ? s = !1 : pt[n] instanceof Function ? pt[n].apply(pt, e[n] instanceof Array ? e[n] : [e[n]]) : pt[n] = e[n];
                    else {
                        if (o[n] !== t) return m(e[n], n);
                        s = !1
                    }
            return s
        }, nt = function() {
            function t(e) {
                var t = Rt.call(e),
                    i = t.length;
                return a ? (t[1] = "on" + t[1], i > 3 && t.pop()) : 3 === i && t.push(!1), t
            }

            function i(e, t) {
                var i = e.shift(),
                    n = [r[t]];
                a ? i[n](e[0], e[1]) : i[n].apply(i, e)
            }

            function n() {
                i(t(arguments), "add")
            }

            function s() {
                i(t(arguments), "remove")
            }
            var a = e.attachEvent,
                r = {
                    add: a ? "attachEvent" : "addEventListener",
                    remove: a ? "detachEvent" : "removeEventListener"
                };
            return {
                add: n,
                remove: s
            }
        }(), lt = {
            abort: a(function() {}),
            canplay: a(function() {
                var e, i = this._s;
                if (i._html5_canplay) return !0;
                if (i._html5_canplay = !0, i._onbufferchange(0), e = i._iO.position === t || isNaN(i._iO.position) ? null : i._iO.position / Ft, this.currentTime !== e) try {
                    this.currentTime = e
                } catch (n) {}
                i._iO._oncanplay && i._iO._oncanplay()
            }),
            canplaythrough: a(function() {
                var e = this._s;
                e.loaded || (e._onbufferchange(0), e._whileloading(e.bytesLoaded, e.bytesTotal, e._get_html5_duration()), e._onload(!0))
            }),
            durationchange: a(function() {
                var e, t = this._s;
                e = t._get_html5_duration(), isNaN(e) || e === t.duration || (t.durationEstimate = t.duration = e)
            }),
            ended: a(function() {
                var e = this._s;
                e._onfinish()
            }),
            error: a(function() {
                this._s._onload(!1)
            }),
            loadeddata: a(function() {
                var e = this._s;
                e._loaded || jt || (e.duration = e._get_html5_duration())
            }),
            loadedmetadata: a(function() {}),
            loadstart: a(function() {
                this._s._onbufferchange(1)
            }),
            play: a(function() {
                this._s._onbufferchange(0)
            }),
            playing: a(function() {
                this._s._onbufferchange(0)
            }),
            progress: a(function(e) {
                var t, i, n = this._s,
                    s = 0,
                    a = ("progress" === e.type, e.target.buffered),
                    r = e.loaded || 0,
                    o = e.total || 1;
                if (n.buffered = [], a && a.length) {
                    for (t = 0, i = a.length; i > t; t++) n.buffered.push({
                        start: a.start(t) * Ft,
                        end: a.end(t) * Ft
                    });
                    s = (a.end(0) - a.start(0)) * Ft, r = Math.min(1, s / (e.target.duration * Ft))
                }
                isNaN(r) || (n._whileloading(r, o, n._get_html5_duration()), r && o && r === o && lt.canplaythrough.call(this, e))
            }),
            ratechange: a(function() {}),
            suspend: a(function(e) {
                var t = this._s;
                lt.progress.call(this, e), t._onsuspend()
            }),
            stalled: a(function() {}),
            timeupdate: a(function() {
                this._s._onTimer()
            }),
            waiting: a(function() {
                var e = this._s;
                e._onbufferchange(1)
            })
        }, G = function(e) {
            var t;
            return t = e && (e.type || e.url || e.serverURL) ? e.serverURL || e.type && s(e.type) ? !1 : e.type ? J({
                type: e.type
            }) : J({
                url: e.url
            }) || pt.html5Only || e.url.match(/data\:/i) : !1
        }, et = function(e) {
            var i;
            return e && (i = jt ? Xt : pt.html5.canPlayType("audio/wav") ? Gt : Xt, e.src = i, e._called_unload !== t && (e._called_load = !1)), Ot && (st = null), i
        }, J = function(e) {
            if (!pt.useHTML5Audio || !pt.hasHTML5) return !1;
            var i, n, a, r, o = e.url || null,
                l = e.type || null,
                c = pt.audioFormats;
            if (l && pt.html5[l] !== t) return pt.html5[l] && !s(l);
            if (!Z) {
                Z = [];
                for (r in c) c.hasOwnProperty(r) && (Z.push(r), c[r].related && (Z = Z.concat(c[r].related)));
                Z = new RegExp("\\.(" + Z.join("|") + ")(\\?.*)?$", "i")
            }
            return a = o ? o.toLowerCase().match(Z) : null, a && a.length ? a = a[1] : l ? (n = l.indexOf(";"), a = (-1 !== n ? l.substr(0, n) : l).substr(6)) : i = !1, a && pt.html5[a] !== t ? i = pt.html5[a] && !s(a) : (l = "audio/" + a, i = pt.html5.canPlayType({
                type: l
            }), pt.html5[a] = i, i = i && pt.html5[l] && !s(l)), i
        }, it = function() {
            function e(e) {
                var t, i, n = !1,
                    s = !1;
                if (!r || "function" != typeof r.canPlayType) return n;
                if (e instanceof Array) {
                    for (a = 0, i = e.length; i > a; a++)(pt.html5[e[a]] || r.canPlayType(e[a]).match(pt.html5Test)) && (s = !0, pt.html5[e[a]] = !0, pt.flash[e[a]] = !! e[a].match(Kt));
                    n = s
                } else t = r && "function" == typeof r.canPlayType ? r.canPlayType(e) : !1, n = !(!t || !t.match(pt.html5Test));
                return n
            }
            if (!pt.useHTML5Audio || !pt.hasHTML5) return pt.html5.usingFlash = !0, Lt = !0, !1;
            var i, n, s, a, r = Audio !== t ? qt && opera.version() < 10 ? new Audio(null) : new Audio : null,
                o = {};
            s = pt.audioFormats;
            for (i in s)
                if (s.hasOwnProperty(i) && (n = "audio/" + i, o[i] = e(s[i].type), o[n] = o[i], i.match(Kt) ? (pt.flash[i] = !0, pt.flash[n] = !0) : (pt.flash[i] = !1, pt.flash[n] = !1), s[i] && s[i].related))
                    for (a = s[i].related.length - 1; a >= 0; a--) o["audio/" + s[i].related[a]] = o[i], pt.html5[s[i].related[a]] = o[i], pt.flash[s[i].related[a]] = o[i];
            return o.canPlayType = r ? e : null, pt.html5 = p(pt.html5, o), pt.html5.usingFlash = X(), Lt = pt.html5.usingFlash, !0
        }, C = {}, F = function() {}, z = function(e) {
            return 8 === u && e.loops > 1 && e.stream && (e.stream = !1), e
        }, j = function(e) {
            return e && !e.usePolicyFile && (e.onid3 || e.usePeakData || e.useWaveformData || e.useEQData) && (e.usePolicyFile = !0), e
        }, q = function() {}, l = function() {
            return !1
        }, R = function(e) {
            var t;
            for (t in e) e.hasOwnProperty(t) && "function" == typeof e[t] && (e[t] = l);
            t = null
        }, O = function(e) {
            e === t && (e = !1), (St || e) && pt.disable(e)
        }, N = function(e) {
            var t, i = null;
            if (e)
                if (e.match(/\.swf(\?.*)?$/i)) {
                    if (i = e.substr(e.toLowerCase().lastIndexOf(".swf?") + 4)) return e
                } else e.lastIndexOf("/") !== e.length - 1 && (e += "/");
            return t = (e && -1 !== e.lastIndexOf("/") ? e.substr(0, e.lastIndexOf("/") + 1) : "./") + pt.movieURL, pt.noSWFCache && (t += "?ts=" + (new Date).getTime()), t
        }, k = function() {
            u = parseInt(pt.flashVersion, 10), 8 !== u && 9 !== u && (pt.flashVersion = u = Pt);
            var e = pt.debugMode || pt.debugFlash ? "_debug.swf" : ".swf";
            pt.useHTML5Audio && !pt.html5Only && pt.audioFormats.mp4.required && 9 > u && (pt.flashVersion = u = 9), pt.version = pt.versionNumber + (pt.html5Only ? " (HTML5-only mode)" : 9 === u ? " (AS3/Flash 9)" : " (AS2/Flash 8)"), u > 8 ? (pt.defaultOptions = p(pt.defaultOptions, pt.flash9Options), pt.features.buffering = !0, pt.defaultOptions = p(pt.defaultOptions, pt.movieStarOptions), pt.filePatterns.flash9 = new RegExp("\\.(mp3|" + ti.join("|") + ")(\\?.*)?$", "i"), pt.features.movieStar = !0) : pt.features.movieStar = !1, pt.filePattern = pt.filePatterns[8 !== u ? "flash9" : "flash8"], pt.movieURL = (8 === u ? "soundmanager2.swf" : "soundmanager2_flash9.swf").replace(".swf", e), pt.features.peakData = pt.features.waveformData = pt.features.eqData = u > 8
        }, E = function(e, t) {
            return ft ? void ft._setPolling(e, t) : !1
        }, L = function() {}, H = this.getSoundById, B = function() {
            var e = [];
            return pt.debugMode && e.push(U.sm2Debug), pt.debugFlash && e.push(U.flashDebug), pt.useHighPerformance && e.push(U.highPerf), e.join(" ")
        }, V = function() {
            var e = (F("fbHandler"), pt.getMoviePercent()),
                t = U,
                i = {
                    type: "FLASHBLOCK"
                };
            return pt.html5Only ? !1 : void(pt.ok() ? pt.oMC && (pt.oMC.className = [B(), t.swfDefault, t.swfLoaded + (pt.didFlashBlock ? " " + t.swfUnblocked : "")].join(" ")) : (Lt && (pt.oMC.className = B() + " " + t.swfDefault + " " + (null === e ? t.swfTimedout : t.swfError)), pt.didFlashBlock = !0, v({
                type: "ontimeout",
                ignoreInit: !0,
                error: i
            }), D(i)))
        }, g = function(e, i, n) {
            bt[e] === t && (bt[e] = []), bt[e].push({
                method: i,
                scope: n || null,
                fired: !1
            })
        }, v = function(e) {
            if (e || (e = {
                type: pt.ok() ? "onready" : "ontimeout"
            }), !kt && e && !e.ignoreInit) return !1;
            if ("ontimeout" === e.type && (pt.ok() || St && !e.ignoreInit)) return !1;
            var t, i, n = {
                    success: e && e.ignoreInit ? pt.ok() : !St
                }, s = e && e.type ? bt[e.type] || [] : [],
                a = [],
                r = [n],
                o = Lt && !pt.ok();
            for (e.error && (r[0].error = e.error), t = 0, i = s.length; i > t; t++) s[t].fired !== !0 && a.push(s[t]);
            if (a.length)
                for (t = 0, i = a.length; i > t; t++) a[t].scope ? a[t].method.apply(a[t].scope, r) : a[t].method.apply(this, r), o || (a[t].fired = !0);
            return !0
        }, y = function() {
            e.setTimeout(function() {
                pt.useFlashBlock && V(), v(), "function" == typeof pt.onload && pt.onload.apply(e), pt.waitForWindowLoad && nt.add(e, "load", y)
            }, 1)
        }, rt = function() {
            if (at !== t) return at;
            var i, n, s, a = !1,
                r = navigator,
                o = r.plugins,
                l = e.ActiveXObject;
            if (o && o.length) n = "application/x-shockwave-flash", s = r.mimeTypes, s && s[n] && s[n].enabledPlugin && s[n].enabledPlugin.description && (a = !0);
            else if (l !== t && !gt.match(/MSAppHost/i)) {
                try {
                    i = new l("ShockwaveFlash.ShockwaveFlash")
                } catch (c) {
                    i = null
                }
                a = !! i, i = null
            }
            return at = a, a
        }, X = function() {
            var e, t, i = pt.audioFormats,
                n = Vt && !! gt.match(/os (1|2|3_0|3_1)\s/i);
            if (n ? (pt.hasHTML5 = !1, pt.html5Only = !0, pt.oMC && (pt.oMC.style.display = "none")) : pt.useHTML5Audio && (pt.html5 && pt.html5.canPlayType || (pt.hasHTML5 = !1)), pt.useHTML5Audio && pt.hasHTML5) {
                K = !0;
                for (t in i) i.hasOwnProperty(t) && i[t].required && (pt.html5.canPlayType(i[t].type) ? pt.preferFlash && (pt.flash[t] || pt.flash[i[t].type]) && (e = !0) : (K = !1, e = !0))
            }
            return pt.ignoreFlash && (e = !1, K = !0), pt.html5Only = pt.hasHTML5 && pt.useHTML5Audio && !e, !pt.html5Only
        }, Q = function(e) {
            var t, i, n, s = 0;
            if (e instanceof Array) {
                for (t = 0, i = e.length; i > t; t++)
                    if (e[t] instanceof Object) {
                        if (pt.canPlayMIME(e[t].type)) {
                            s = t;
                            break
                        }
                    } else if (pt.canPlayURL(e[t])) {
                    s = t;
                    break
                }
                e[s].url && (e[s] = e[s].url), n = e[s]
            } else n = e;
            return n
        }, W = function(e) {
            e._hasTimer || (e._hasTimer = !0, !Ht && pt.html5PollingInterval && (null === Dt && 0 === Mt && (Dt = setInterval(Y, pt.html5PollingInterval)), Mt++))
        }, $ = function(e) {
            e._hasTimer && (e._hasTimer = !1, !Ht && pt.html5PollingInterval && Mt--)
        }, Y = function() {
            var e;
            if (null !== Dt && !Mt) return clearInterval(Dt), Dt = null, !1;
            for (e = pt.soundIDs.length - 1; e >= 0; e--) pt.sounds[pt.soundIDs[e]].isHTML5 && pt.sounds[pt.soundIDs[e]]._hasTimer && pt.sounds[pt.soundIDs[e]]._onTimer()
        }, D = function(i) {
            i = i !== t ? i : {}, "function" == typeof pt.onerror && pt.onerror.apply(e, [{
                type: i.type !== t ? i.type : null
            }]), i.fatal !== t && i.fatal && pt.disable()
        }, ot = function() {
            if (!Wt || !rt()) return !1;
            var e, t, i = pt.audioFormats;
            for (t in i)
                if (i.hasOwnProperty(t) && ("mp3" === t || "mp4" === t) && (pt.html5[t] = !1, i[t] && i[t].related))
                    for (e = i[t].related.length - 1; e >= 0; e--) pt.html5[i[t].related[e]] = !1
        }, this._setSandboxType = function() {}, this._externalInterfaceOK = function() {
            if (pt.swfLoaded) return !1;
            pt.swfLoaded = !0, Yt = !1, Wt && ot(), setTimeout(d, Ut ? 100 : 1)
        }, M = function(e, i) {
            function n() {}

            function s(e, t) {
                return '<param name="' + e + '" value="' + t + '" />'
            }
            if (wt && xt) return !1;
            if (pt.html5Only) return k(), n(), pt.oMC = o(pt.movieID), d(), wt = !0, xt = !0, !1;
            var a, r, l, c, u, h, p, m, f = i || pt.url,
                g = pt.altURL || f,
                v = "JS/Flash audio component (SoundManager 2)",
                y = I(),
                b = B(),
                w = null,
                x = yt.getElementsByTagName("html")[0];
            if (w = x && x.dir && x.dir.match(/rtl/i), e = e === t ? pt.id : e, k(), pt.url = N(Jt ? f : g), i = pt.url, pt.wmode = !pt.wmode && pt.useHighPerformance ? "transparent" : pt.wmode, null !== pt.wmode && (gt.match(/msie 8/i) || !Ut && !pt.useHighPerformance) && navigator.platform.match(/win32|win64/i) && (Et.push(C.spcWmode), pt.wmode = null), a = {
                name: e,
                id: e,
                src: i,
                quality: "high",
                allowScriptAccess: pt.allowScriptAccess,
                bgcolor: pt.bgColor,
                pluginspage: Zt + "www.macromedia.com/go/getflashplayer",
                title: v,
                type: "application/x-shockwave-flash",
                wmode: pt.wmode,
                hasPriority: "true"
            }, pt.debugFlash && (a.FlashVars = "debug=1"), pt.wmode || delete a.wmode, Ut) r = yt.createElement("div"), c = ['<object id="' + e + '" data="' + i + '" type="' + a.type + '" title="' + a.title + '" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0">', s("movie", i), s("AllowScriptAccess", pt.allowScriptAccess), s("quality", a.quality), pt.wmode ? s("wmode", pt.wmode) : "", s("bgcolor", pt.bgColor), s("hasPriority", "true"), pt.debugFlash ? s("FlashVars", a.FlashVars) : "", "</object>"].join("");
            else {
                r = yt.createElement("embed");
                for (l in a) a.hasOwnProperty(l) && r.setAttribute(l, a[l])
            } if (L(), b = B(), y = I())
                if (pt.oMC = o(pt.movieID) || yt.createElement("div"), pt.oMC.id) m = pt.oMC.className, pt.oMC.className = (m ? m + " " : U.swfDefault) + (b ? " " + b : ""), pt.oMC.appendChild(r), Ut && (u = pt.oMC.appendChild(yt.createElement("div")), u.className = U.swfBox, u.innerHTML = c), xt = !0;
                else {
                    if (pt.oMC.id = pt.movieID, pt.oMC.className = U.swfDefault + " " + b, h = null, u = null, pt.useFlashBlock || (pt.useHighPerformance ? h = {
                        position: "fixed",
                        width: "8px",
                        height: "8px",
                        bottom: "0px",
                        left: "0px",
                        overflow: "hidden"
                    } : (h = {
                        position: "absolute",
                        width: "6px",
                        height: "6px",
                        top: "-9999px",
                        left: "-9999px"
                    }, w && (h.left = Math.abs(parseInt(h.left, 10)) + "px"))), zt && (pt.oMC.style.zIndex = 1e4), !pt.debugFlash)
                        for (p in h) h.hasOwnProperty(p) && (pt.oMC.style[p] = h[p]);
                    try {
                        Ut || pt.oMC.appendChild(r), y.appendChild(pt.oMC), Ut && (u = pt.oMC.appendChild(yt.createElement("div")), u.className = U.swfBox, u.innerHTML = c), xt = !0
                    } catch (S) {
                        throw new Error(F("domError") + " \n" + S.toString())
                    }
                }
            return wt = !0, n(), !0
        }, P = function() {
            return pt.html5Only ? (M(), !1) : ft ? !1 : pt.url ? (ft = pt.getMovie(pt.id), ft || (At ? (Ut ? pt.oMC.innerHTML = _t : pt.oMC.appendChild(At), At = null, wt = !0) : M(pt.id, pt.url), ft = pt.getMovie(pt.id)), "function" == typeof pt.oninitmovie && setTimeout(pt.oninitmovie, 1), !0) : !1
        }, b = function() {
            setTimeout(w, 1e3)
        }, x = function() {
            e.setTimeout(function() {
                pt.setup({
                    preferFlash: !1
                }).reboot(), pt.didFlashBlock = !0, pt.beginDelayedInit()
            }, 1)
        }, w = function() {
            var t, i = !1;
            return pt.url ? Tt ? !1 : (Tt = !0, nt.remove(e, "load", b), at && Yt && !$t ? !1 : (kt || (t = pt.getMoviePercent(), t > 0 && 100 > t && (i = !0)), void setTimeout(function() {
                return t = pt.getMoviePercent(), i ? (Tt = !1, e.setTimeout(b, 1), !1) : void(!kt && Qt && (null === t ? pt.useFlashBlock || 0 === pt.flashLoadTimeout ? pt.useFlashBlock && V() : !pt.useFlashBlock && K ? x() : v({
                    type: "ontimeout",
                    ignoreInit: !0,
                    error: {
                        type: "INIT_FLASHBLOCK"
                    }
                }) : 0 === pt.flashLoadTimeout || (!pt.useFlashBlock && K ? x() : O(!0))))
            }, pt.flashLoadTimeout))) : !1
        }, S = function() {
            function t() {
                nt.remove(e, "focus", S)
            }
            return $t || !Yt ? (t(), !0) : (Qt = !0, $t = !0, Tt = !1, b(), t(), !0)
        }, dt = function() {}, ct = function() {}, h = function(t) {
            if (kt) return !1;
            if (pt.html5Only) return kt = !0, y(), !0;
            var i, n = pt.useFlashBlock && pt.flashLoadTimeout && !pt.getMoviePercent(),
                s = !0;
            return n || (kt = !0), i = {
                type: !at && Lt ? "NO_FLASH" : "INIT_TIMEOUT"
            }, (St || t) && (pt.useFlashBlock && pt.oMC && (pt.oMC.className = B() + " " + (null === pt.getMoviePercent() ? U.swfTimedout : U.swfError)), v({
                type: "ontimeout",
                error: i,
                ignoreInit: !0
            }), D(i), s = !1), St || (pt.waitForWindowLoad && !Ct ? nt.add(e, "load", y) : y()), s
        }, c = function() {
            var e, i = pt.setupOptions;
            for (e in i) i.hasOwnProperty(e) && (pt[e] === t ? pt[e] = i[e] : pt[e] !== i[e] && (pt.setupOptions[e] = pt[e]))
        }, d = function() {
            function t() {
                nt.remove(e, "load", pt.beginDelayedInit)
            }
            if (kt) return !1;
            if (pt.html5Only) return kt || (t(), pt.enabled = !0, h()), !0;
            P();
            try {
                ft._externalInterfaceTest(!1), E(!0, pt.flashPollingInterval || (pt.useHighPerformance ? 10 : 50)), pt.debugMode || ft._disableDebug(), pt.enabled = !0, pt.html5Only || nt.add(e, "unload", l)
            } catch (i) {
                return D({
                    type: "JS_TO_FLASH_EXCEPTION",
                    fatal: !0
                }), O(!0), h(), !1
            }
            return h(), t(), !0
        }, A = function() {
            return T ? !1 : (T = !0, c(), L(), !at && pt.hasHTML5 && pt.setup({
                useHTML5Audio: !0,
                preferFlash: !1
            }), it(), !at && Lt && (Et.push(C.needFlash), pt.setup({
                flashLoadTimeout: 1
            })), yt.removeEventListener && yt.removeEventListener("DOMContentLoaded", A, !1), P(), !0)
        }, tt = function() {
            return "complete" === yt.readyState && (A(), yt.detachEvent("onreadystatechange", tt)), !0
        }, _ = function() {
            Ct = !0, A(), nt.remove(e, "load", _)
        }, rt(), nt.add(e, "focus", S), nt.add(e, "load", b), nt.add(e, "load", _), yt.addEventListener ? yt.addEventListener("DOMContentLoaded", A, !1) : yt.attachEvent ? yt.attachEvent("onreadystatechange", tt) : D({
            type: "NO_DOM2_EVENTS",
            fatal: !0
        })
    }
    if (!e || !e.document) throw new Error("SoundManager requires a browser with window and document objects.");
    var n = null;
    e.SM2_DEFER !== t && SM2_DEFER || (n = new i), "object" == typeof module && module && "object" == typeof module.exports ? (module.exports.SoundManager = i, module.exports.soundManager = n) : "function" == typeof define && define.amd && define("soundmanager-lib", [], function() {
        function t(t) {
            if (!e.soundManager && t instanceof Function) {
                var n = t(i);
                n instanceof i && (e.soundManager = n)
            }
            return e.soundManager
        }
        return {
            constructor: i,
            getInstance: t
        }
    }), e.SoundManager = i, e.soundManager = n
}(window);