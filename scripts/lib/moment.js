+function(e) {
    function t(e, t) {
        return function(i) {
            return l(e.call(this, i), t)
        }
    }

    function i(e, t) {
        return function(i) {
            return this.lang().ordinal(e.call(this, i), t)
        }
    }

    function n() {}

    function s(e) {
        w(e), r(this, e)
    }

    function a(e) {
        var t = m(e),
            i = t.year || 0,
            n = t.month || 0,
            s = t.week || 0,
            a = t.day || 0,
            r = t.hour || 0,
            o = t.minute || 0,
            l = t.second || 0,
            c = t.millisecond || 0;
        this._input = e, this._milliseconds = +c + 1e3 * l + 6e4 * o + 36e5 * r, this._days = +a + 7 * s, this._months = +n + 12 * i, this._data = {}, this._bubble()
    }

    function r(e, t) {
        for (var i in t) t.hasOwnProperty(i) && (e[i] = t[i]);
        return t.hasOwnProperty("toString") && (e.toString = t.toString), t.hasOwnProperty("valueOf") && (e.valueOf = t.valueOf), e
    }

    function o(e) {
        return 0 > e ? Math.ceil(e) : Math.floor(e)
    }

    function l(e, t) {
        for (var i = e + ""; i.length < t;) i = "0" + i;
        return i
    }

    function c(e, t, i, n) {
        var s, a, r = t._milliseconds,
            o = t._days,
            l = t._months;
        r && e._d.setTime(+e._d + r * i), (o || l) && (s = e.minute(), a = e.hour()), o && e.date(e.date() + o * i), l && e.month(e.month() + l * i), r && !n && tt.updateOffset(e), (o || l) && (e.minute(s), e.hour(a))
    }

    function d(e) {
        return "[object Array]" === Object.prototype.toString.call(e)
    }

    function u(e) {
        return "[object Date]" === Object.prototype.toString.call(e) || e instanceof Date
    }

    function h(e, t, i) {
        var n, s = Math.min(e.length, t.length),
            a = Math.abs(e.length - t.length),
            r = 0;
        for (n = 0; s > n; n++)(i && e[n] !== t[n] || !i && g(e[n]) !== g(t[n])) && r++;
        return r + a
    }

    function p(e) {
        if (e) {
            var t = e.toLowerCase().replace(/(.)s$/, "$1");
            e = Nt[e] || Ft[t] || t
        }
        return e
    }

    function m(e) {
        var t, i, n = {};
        for (i in e) e.hasOwnProperty(i) && (t = p(i), t && (n[t] = e[i]));
        return n
    }

    function f(t) {
        var i, n;
        if (0 === t.indexOf("week")) i = 7, n = "day";
        else {
            if (0 !== t.indexOf("month")) return;
            i = 12, n = "month"
        }
        tt[t] = function(s, a) {
            var r, o, l = tt.fn._lang[t],
                c = [];
            if ("number" == typeof s && (a = s, s = e), o = function(e) {
                var t = tt().utc().set(n, e);
                return l.call(tt.fn._lang, t, s || "")
            }, null != a) return o(a);
            for (r = 0; i > r; r++) c.push(o(r));
            return c
        }
    }

    function g(e) {
        var t = +e,
            i = 0;
        return 0 !== t && isFinite(t) && (i = t >= 0 ? Math.floor(t) : Math.ceil(t)), i
    }

    function v(e, t) {
        return new Date(Date.UTC(e, t + 1, 0)).getUTCDate()
    }

    function y(e) {
        return b(e) ? 366 : 365
    }

    function b(e) {
        return 0 === e % 4 && 0 !== e % 100 || 0 === e % 400
    }

    function w(e) {
        var t;
        e._a && -2 === e._pf.overflow && (t = e._a[rt] < 0 || e._a[rt] > 11 ? rt : e._a[ot] < 1 || e._a[ot] > v(e._a[at], e._a[rt]) ? ot : e._a[lt] < 0 || e._a[lt] > 23 ? lt : e._a[ct] < 0 || e._a[ct] > 59 ? ct : e._a[dt] < 0 || e._a[dt] > 59 ? dt : e._a[ut] < 0 || e._a[ut] > 999 ? ut : -1, e._pf._overflowDayOfYear && (at > t || t > ot) && (t = ot), e._pf.overflow = t)
    }

    function x(e) {
        e._pf = {
            empty: !1,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: !1,
            invalidMonth: null,
            invalidFormat: !1,
            userInvalidated: !1,
            iso: !1
        }
    }

    function k(e) {
        return null == e._isValid && (e._isValid = !isNaN(e._d.getTime()) && e._pf.overflow < 0 && !e._pf.empty && !e._pf.invalidMonth && !e._pf.nullInput && !e._pf.invalidFormat && !e._pf.userInvalidated, e._strict && (e._isValid = e._isValid && 0 === e._pf.charsLeftOver && 0 === e._pf.unusedTokens.length)), e._isValid
    }

    function S(e) {
        return e ? e.toLowerCase().replace("_", "-") : e
    }

    function C(e, t) {
        return t.abbr = e, ht[e] || (ht[e] = new n), ht[e].set(t), ht[e]
    }

    function P(e) {
        delete ht[e]
    }

    function A(e) {
        var t, i, n, s, a = 0,
            r = function(e) {
                if (!ht[e] && pt) try {
                    require("./lang/" + e)
                } catch (t) {}
                return ht[e]
            };
        if (!e) return tt.fn._lang;
        if (!d(e)) {
            if (i = r(e)) return i;
            e = [e]
        }
        for (; a < e.length;) {
            for (s = S(e[a]).split("-"), t = s.length, n = S(e[a + 1]), n = n ? n.split("-") : null; t > 0;) {
                if (i = r(s.slice(0, t).join("-"))) return i;
                if (n && n.length >= t && h(s, n, !0) >= t - 1) break;
                t--
            }
            a++
        }
        return tt.fn._lang
    }

    function _(e) {
        return e.match(/\[[\s\S]/) ? e.replace(/^\[|\]$/g, "") : e.replace(/\\/g, "")
    }

    function T(e) {
        var t, i, n = e.match(vt);
        for (t = 0, i = n.length; i > t; t++) n[t] = zt[n[t]] ? zt[n[t]] : _(n[t]);
        return function(s) {
            var a = "";
            for (t = 0; i > t; t++) a += n[t] instanceof Function ? n[t].call(s, e) : n[t];
            return a
        }
    }

    function I(e, t) {
        return e.isValid() ? (t = M(t, e.lang()), Vt[t] || (Vt[t] = T(t)), Vt[t](e)) : e.lang().invalidDate()
    }

    function M(e, t) {
        function i(e) {
            return t.longDateFormat(e) || e
        }
        var n = 5;
        for (yt.lastIndex = 0; n >= 0 && yt.test(e);) e = e.replace(yt, i), yt.lastIndex = 0, n -= 1;
        return e
    }

    function D(e, t) {
        var i;
        switch (e) {
            case "DDDD":
                return xt;
            case "YYYY":
            case "GGGG":
            case "gggg":
                return kt;
            case "YYYYY":
            case "GGGGG":
            case "ggggg":
                return St;
            case "S":
            case "SS":
            case "SSS":
            case "DDD":
                return wt;
            case "MMM":
            case "MMMM":
            case "dd":
            case "ddd":
            case "dddd":
                return Pt;
            case "a":
            case "A":
                return A(t._l)._meridiemParse;
            case "X":
                return Tt;
            case "Z":
            case "ZZ":
                return At;
            case "T":
                return _t;
            case "SSSS":
                return Ct;
            case "MM":
            case "DD":
            case "YY":
            case "GG":
            case "gg":
            case "HH":
            case "hh":
            case "mm":
            case "ss":
            case "M":
            case "D":
            case "d":
            case "H":
            case "h":
            case "m":
            case "s":
            case "w":
            case "ww":
            case "W":
            case "WW":
            case "e":
            case "E":
                return bt;
            default:
                return i = new RegExp(B(V(e.replace("\\", "")), "i"))
        }
    }

    function E(e) {
        var t = (At.exec(e) || [])[0],
            i = (t + "").match(Lt) || ["-", 0, 0],
            n = +(60 * i[1]) + g(i[2]);
        return "+" === i[0] ? -n : n
    }

    function L(e, t, i) {
        var n, s = i._a;
        switch (e) {
            case "M":
            case "MM":
                null != t && (s[rt] = g(t) - 1);
                break;
            case "MMM":
            case "MMMM":
                n = A(i._l).monthsParse(t), null != n ? s[rt] = n : i._pf.invalidMonth = t;
                break;
            case "D":
            case "DD":
                null != t && (s[ot] = g(t));
                break;
            case "DDD":
            case "DDDD":
                null != t && (i._dayOfYear = g(t));
                break;
            case "YY":
                s[at] = g(t) + (g(t) > 68 ? 1900 : 2e3);
                break;
            case "YYYY":
            case "YYYYY":
                s[at] = g(t);
                break;
            case "a":
            case "A":
                i._isPm = A(i._l).isPM(t);
                break;
            case "H":
            case "HH":
            case "h":
            case "hh":
                s[lt] = g(t);
                break;
            case "m":
            case "mm":
                s[ct] = g(t);
                break;
            case "s":
            case "ss":
                s[dt] = g(t);
                break;
            case "S":
            case "SS":
            case "SSS":
            case "SSSS":
                s[ut] = g(1e3 * ("0." + t));
                break;
            case "X":
                i._d = new Date(1e3 * parseFloat(t));
                break;
            case "Z":
            case "ZZ":
                i._useUTC = !0, i._tzm = E(t);
                break;
            case "w":
            case "ww":
            case "W":
            case "WW":
            case "d":
            case "dd":
            case "ddd":
            case "dddd":
            case "e":
            case "E":
                e = e.substr(0, 1);
            case "gg":
            case "gggg":
            case "GG":
            case "GGGG":
            case "GGGGG":
                e = e.substr(0, 2), t && (i._w = i._w || {}, i._w[e] = t)
        }
    }

    function R(e) {
        var t, i, n, s, a, r, o, l, c, d, u = [];
        if (!e._d) {
            for (n = N(e), e._w && null == e._a[ot] && null == e._a[rt] && (a = function(t) {
                return t ? t.length < 3 ? parseInt(t, 10) > 68 ? "19" + t : "20" + t : t : null == e._a[at] ? tt().weekYear() : e._a[at]
            }, r = e._w, null != r.GG || null != r.W || null != r.E ? o = K(a(r.GG), r.W || 1, r.E, 4, 1) : (l = A(e._l), c = null != r.d ? W(r.d, l) : null != r.e ? parseInt(r.e, 10) + l._week.dow : 0, d = parseInt(r.w, 10) || 1, null != r.d && c < l._week.dow && d++, o = K(a(r.gg), d, c, l._week.doy, l._week.dow)), e._a[at] = o.year, e._dayOfYear = o.dayOfYear), e._dayOfYear && (s = null == e._a[at] ? n[at] : e._a[at], e._dayOfYear > y(s) && (e._pf._overflowDayOfYear = !0), i = H(s, 0, e._dayOfYear), e._a[rt] = i.getUTCMonth(), e._a[ot] = i.getUTCDate()), t = 0; 3 > t && null == e._a[t]; ++t) e._a[t] = u[t] = n[t];
            for (; 7 > t; t++) e._a[t] = u[t] = null == e._a[t] ? 2 === t ? 1 : 0 : e._a[t];
            u[lt] += g((e._tzm || 0) / 60), u[ct] += g((e._tzm || 0) % 60), e._d = (e._useUTC ? H : q).apply(null, u)
        }
    }

    function O(e) {
        var t;
        e._d || (t = m(e._i), e._a = [t.year, t.month, t.day, t.hour, t.minute, t.second, t.millisecond], R(e))
    }

    function N(e) {
        var t = new Date;
        return e._useUTC ? [t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate()] : [t.getFullYear(), t.getMonth(), t.getDate()]
    }

    function F(e) {
        e._a = [], e._pf.empty = !0;
        var t, i, n, s, a, r = A(e._l),
            o = "" + e._i,
            l = o.length,
            c = 0;
        for (n = M(e._f, r).match(vt) || [], t = 0; t < n.length; t++) s = n[t], i = (D(s, e).exec(o) || [])[0], i && (a = o.substr(0, o.indexOf(i)), a.length > 0 && e._pf.unusedInput.push(a), o = o.slice(o.indexOf(i) + i.length), c += i.length), zt[s] ? (i ? e._pf.empty = !1 : e._pf.unusedTokens.push(s), L(s, i, e)) : e._strict && !i && e._pf.unusedTokens.push(s);
        e._pf.charsLeftOver = l - c, o.length > 0 && e._pf.unusedInput.push(o), e._isPm && e._a[lt] < 12 && (e._a[lt] += 12), e._isPm === !1 && 12 === e._a[lt] && (e._a[lt] = 0), R(e), w(e)
    }

    function V(e) {
        return e.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function(e, t, i, n, s) {
            return t || i || n || s
        })
    }

    function B(e) {
        return e.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
    }

    function U(e) {
        var t, i, n, s, a;
        if (0 === e._f.length) return e._pf.invalidFormat = !0, void(e._d = new Date(0 / 0));
        for (s = 0; s < e._f.length; s++) a = 0, t = r({}, e), x(t), t._f = e._f[s], F(t), k(t) && (a += t._pf.charsLeftOver, a += 10 * t._pf.unusedTokens.length, t._pf.score = a, (null == n || n > a) && (n = a, i = t));
        r(e, i || t)
    }

    function z(e) {
        var t, i = e._i,
            n = It.exec(i);
        if (n) {
            for (e._pf.iso = !0, t = 4; t > 0; t--)
                if (n[t]) {
                    e._f = Dt[t - 1] + (n[6] || " ");
                    break
                }
            for (t = 0; 4 > t; t++)
                if (Et[t][1].exec(i)) {
                    e._f += Et[t][0];
                    break
                }
            At.exec(i) && (e._f += "Z"), F(e)
        } else e._d = new Date(i)
    }

    function j(t) {
        var i = t._i,
            n = mt.exec(i);
        i === e ? t._d = new Date : n ? t._d = new Date(+n[1]) : "string" == typeof i ? z(t) : d(i) ? (t._a = i.slice(0), R(t)) : u(i) ? t._d = new Date(+i) : "object" == typeof i ? O(t) : t._d = new Date(i)
    }

    function q(e, t, i, n, s, a, r) {
        var o = new Date(e, t, i, n, s, a, r);
        return 1970 > e && o.setFullYear(e), o
    }

    function H(e) {
        var t = new Date(Date.UTC.apply(null, arguments));
        return 1970 > e && t.setUTCFullYear(e), t
    }

    function W(e, t) {
        if ("string" == typeof e)
            if (isNaN(e)) {
                if (e = t.weekdaysParse(e), "number" != typeof e) return null
            } else e = parseInt(e, 10);
        return e
    }

    function $(e, t, i, n, s) {
        return s.relativeTime(t || 1, !! i, e, n)
    }

    function Y(e, t, i) {
        var n = st(Math.abs(e) / 1e3),
            s = st(n / 60),
            a = st(s / 60),
            r = st(a / 24),
            o = st(r / 365),
            l = 45 > n && ["s", n] || 1 === s && ["m"] || 45 > s && ["mm", s] || 1 === a && ["h"] || 22 > a && ["hh", a] || 1 === r && ["d"] || 25 >= r && ["dd", r] || 45 >= r && ["M"] || 345 > r && ["MM", st(r / 30)] || 1 === o && ["y"] || ["yy", o];
        return l[2] = t, l[3] = e > 0, l[4] = i, $.apply({}, l)
    }

    function Q(e, t, i) {
        var n, s = i - t,
            a = i - e.day();
        return a > s && (a -= 7), s - 7 > a && (a += 7), n = tt(e).add("d", a), {
            week: Math.ceil(n.dayOfYear() / 7),
            year: n.year()
        }
    }

    function K(e, t, i, n, s) {
        var a, r, o = new Date(Date.UTC(e, 0)).getUTCDay();
        return i = null != i ? i : s, a = s - o + (o > n ? 7 : 0), r = 7 * (t - 1) + (i - s) + a + 1, {
            year: r > 0 ? e : e - 1,
            dayOfYear: r > 0 ? r : y(e - 1) + r
        }
    }

    function X(e) {
        var t = e._i,
            i = e._f;
        return "undefined" == typeof e._pf && x(e), null === t ? tt.invalid({
            nullInput: !0
        }) : ("string" == typeof t && (e._i = t = A().preparse(t)), tt.isMoment(t) ? (e = r({}, t), e._d = new Date(+t._d)) : i ? d(i) ? U(e) : F(e) : j(e), new s(e))
    }

    function G(e, t) {
        tt.fn[e] = tt.fn[e + "s"] = function(e) {
            var i = this._isUTC ? "UTC" : "";
            return null != e ? (this._d["set" + i + t](e), tt.updateOffset(this), this) : this._d["get" + i + t]()
        }
    }

    function J(e) {
        tt.duration.fn[e] = function() {
            return this._data[e]
        }
    }

    function Z(e, t) {
        tt.duration.fn["as" + e] = function() {
            return +this / t
        }
    }

    function et(e) {
        var t = !1,
            i = tt;
        "undefined" == typeof ender && (this.moment = e ? function() {
            return !t && console && console.warn && (t = !0, console.warn("Accessing Moment through the global scope is deprecated, and will be removed in an upcoming release.")), i.apply(null, arguments)
        } : tt)
    }
    for (var tt, it, nt = "2.4.0", st = Math.round, at = 0, rt = 1, ot = 2, lt = 3, ct = 4, dt = 5, ut = 6, ht = {}, pt = "undefined" != typeof module && module.exports, mt = /^\/?Date\((\-?\d+)/i, ft = /(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/, gt = /^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/, vt = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|X|zz?|ZZ?|.)/g, yt = /(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g, bt = /\d\d?/, wt = /\d{1,3}/, xt = /\d{3}/, kt = /\d{1,4}/, St = /[+\-]?\d{1,6}/, Ct = /\d+/, Pt = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i, At = /Z|[\+\-]\d\d:?\d\d/i, _t = /T/i, Tt = /[\+\-]?\d+(\.\d{1,3})?/, It = /^\s*\d{4}-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d:?\d\d|Z)?)?$/, Mt = "YYYY-MM-DDTHH:mm:ssZ", Dt = ["YYYY-MM-DD", "GGGG-[W]WW", "GGGG-[W]WW-E", "YYYY-DDD"], Et = [
            ["HH:mm:ss.SSSS", /(T| )\d\d:\d\d:\d\d\.\d{1,3}/],
            ["HH:mm:ss", /(T| )\d\d:\d\d:\d\d/],
            ["HH:mm", /(T| )\d\d:\d\d/],
            ["HH", /(T| )\d\d/]
        ], Lt = /([\+\-]|\d\d)/gi, Rt = "Date|Hours|Minutes|Seconds|Milliseconds".split("|"), Ot = {
            Milliseconds: 1,
            Seconds: 1e3,
            Minutes: 6e4,
            Hours: 36e5,
            Days: 864e5,
            Months: 2592e6,
            Years: 31536e6
        }, Nt = {
            ms: "millisecond",
            s: "second",
            m: "minute",
            h: "hour",
            d: "day",
            D: "date",
            w: "week",
            W: "isoWeek",
            M: "month",
            y: "year",
            DDD: "dayOfYear",
            e: "weekday",
            E: "isoWeekday",
            gg: "weekYear",
            GG: "isoWeekYear"
        }, Ft = {
            dayofyear: "dayOfYear",
            isoweekday: "isoWeekday",
            isoweek: "isoWeek",
            weekyear: "weekYear",
            isoweekyear: "isoWeekYear"
        }, Vt = {}, Bt = "DDD w W M D d".split(" "), Ut = "M D H h m s w W".split(" "), zt = {
            M: function() {
                return this.month() + 1
            },
            MMM: function(e) {
                return this.lang().monthsShort(this, e)
            },
            MMMM: function(e) {
                return this.lang().months(this, e)
            },
            D: function() {
                return this.date()
            },
            DDD: function() {
                return this.dayOfYear()
            },
            d: function() {
                return this.day()
            },
            dd: function(e) {
                return this.lang().weekdaysMin(this, e)
            },
            ddd: function(e) {
                return this.lang().weekdaysShort(this, e)
            },
            dddd: function(e) {
                return this.lang().weekdays(this, e)
            },
            w: function() {
                return this.week()
            },
            W: function() {
                return this.isoWeek()
            },
            YY: function() {
                return l(this.year() % 100, 2)
            },
            YYYY: function() {
                return l(this.year(), 4)
            },
            YYYYY: function() {
                return l(this.year(), 5)
            },
            gg: function() {
                return l(this.weekYear() % 100, 2)
            },
            gggg: function() {
                return this.weekYear()
            },
            ggggg: function() {
                return l(this.weekYear(), 5)
            },
            GG: function() {
                return l(this.isoWeekYear() % 100, 2)
            },
            GGGG: function() {
                return this.isoWeekYear()
            },
            GGGGG: function() {
                return l(this.isoWeekYear(), 5)
            },
            e: function() {
                return this.weekday()
            },
            E: function() {
                return this.isoWeekday()
            },
            a: function() {
                return this.lang().meridiem(this.hours(), this.minutes(), !0)
            },
            A: function() {
                return this.lang().meridiem(this.hours(), this.minutes(), !1)
            },
            H: function() {
                return this.hours()
            },
            h: function() {
                return this.hours() % 12 || 12
            },
            m: function() {
                return this.minutes()
            },
            s: function() {
                return this.seconds()
            },
            S: function() {
                return g(this.milliseconds() / 100)
            },
            SS: function() {
                return l(g(this.milliseconds() / 10), 2)
            },
            SSS: function() {
                return l(this.milliseconds(), 3)
            },
            SSSS: function() {
                return l(this.milliseconds(), 3)
            },
            Z: function() {
                var e = -this.zone(),
                    t = "+";
                return 0 > e && (e = -e, t = "-"), t + l(g(e / 60), 2) + ":" + l(g(e) % 60, 2)
            },
            ZZ: function() {
                var e = -this.zone(),
                    t = "+";
                return 0 > e && (e = -e, t = "-"), t + l(g(10 * e / 6), 4)
            },
            z: function() {
                return this.zoneAbbr()
            },
            zz: function() {
                return this.zoneName()
            },
            X: function() {
                return this.unix()
            }
        }, jt = ["months", "monthsShort", "weekdays", "weekdaysShort", "weekdaysMin"]; Bt.length;) it = Bt.pop(), zt[it + "o"] = i(zt[it], it);
    for (; Ut.length;) it = Ut.pop(), zt[it + it] = t(zt[it], 2);
    for (zt.DDDD = t(zt.DDD, 3), r(n.prototype, {
        set: function(e) {
            var t, i;
            for (i in e) t = e[i], "function" == typeof t ? this[i] = t : this["_" + i] = t
        },
        _months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
        months: function(e) {
            return this._months[e.month()]
        },
        _monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
        monthsShort: function(e) {
            return this._monthsShort[e.month()]
        },
        monthsParse: function(e) {
            var t, i, n;
            for (this._monthsParse || (this._monthsParse = []), t = 0; 12 > t; t++)
                if (this._monthsParse[t] || (i = tt.utc([2e3, t]), n = "^" + this.months(i, "") + "|^" + this.monthsShort(i, ""), this._monthsParse[t] = new RegExp(n.replace(".", ""), "i")), this._monthsParse[t].test(e)) return t
        },
        _weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
        weekdays: function(e) {
            return this._weekdays[e.day()]
        },
        _weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
        weekdaysShort: function(e) {
            return this._weekdaysShort[e.day()]
        },
        _weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
        weekdaysMin: function(e) {
            return this._weekdaysMin[e.day()]
        },
        weekdaysParse: function(e) {
            var t, i, n;
            for (this._weekdaysParse || (this._weekdaysParse = []), t = 0; 7 > t; t++)
                if (this._weekdaysParse[t] || (i = tt([2e3, 1]).day(t), n = "^" + this.weekdays(i, "") + "|^" + this.weekdaysShort(i, "") + "|^" + this.weekdaysMin(i, ""), this._weekdaysParse[t] = new RegExp(n.replace(".", ""), "i")), this._weekdaysParse[t].test(e)) return t
        },
        _longDateFormat: {
            LT: "h:mm A",
            L: "MM/DD/YYYY",
            LL: "MMMM D YYYY",
            LLL: "MMMM D YYYY LT",
            LLLL: "dddd, MMMM D YYYY LT"
        },
        longDateFormat: function(e) {
            var t = this._longDateFormat[e];
            return !t && this._longDateFormat[e.toUpperCase()] && (t = this._longDateFormat[e.toUpperCase()].replace(/MMMM|MM|DD|dddd/g, function(e) {
                return e.slice(1)
            }), this._longDateFormat[e] = t), t
        },
        isPM: function(e) {
            return "p" === (e + "").toLowerCase().charAt(0)
        },
        _meridiemParse: /[ap]\.?m?\.?/i,
        meridiem: function(e, t, i) {
            return e > 11 ? i ? "pm" : "PM" : i ? "am" : "AM"
        },
        _calendar: {
            sameDay: "[Today at] LT",
            nextDay: "[Tomorrow at] LT",
            nextWeek: "dddd [at] LT",
            lastDay: "[Yesterday at] LT",
            lastWeek: "[Last] dddd [at] LT",
            sameElse: "L"
        },
        calendar: function(e, t) {
            var i = this._calendar[e];
            return "function" == typeof i ? i.apply(t) : i
        },
        _relativeTime: {
            future: "in %s",
            past: "%s ago",
            s: "a few seconds",
            m: "a minute",
            mm: "%d minutes",
            h: "an hour",
            hh: "%d hours",
            d: "a day",
            dd: "%d days",
            M: "a month",
            MM: "%d months",
            y: "a year",
            yy: "%d years"
        },
        relativeTime: function(e, t, i, n) {
            var s = this._relativeTime[i];
            return "function" == typeof s ? s(e, t, i, n) : s.replace(/%d/i, e)
        },
        pastFuture: function(e, t) {
            var i = this._relativeTime[e > 0 ? "future" : "past"];
            return "function" == typeof i ? i(t) : i.replace(/%s/i, t)
        },
        ordinal: function(e) {
            return this._ordinal.replace("%d", e)
        },
        _ordinal: "%d",
        preparse: function(e) {
            return e
        },
        postformat: function(e) {
            return e
        },
        week: function(e) {
            return Q(e, this._week.dow, this._week.doy).week
        },
        _week: {
            dow: 0,
            doy: 6
        },
        _invalidDate: "Invalid date",
        invalidDate: function() {
            return this._invalidDate
        }
    }), tt = function(t, i, n, s) {
        return "boolean" == typeof n && (s = n, n = e), X({
            _i: t,
            _f: i,
            _l: n,
            _strict: s,
            _isUTC: !1
        })
    }, tt.utc = function(t, i, n, s) {
        var a;
        return "boolean" == typeof n && (s = n, n = e), a = X({
            _useUTC: !0,
            _isUTC: !0,
            _l: n,
            _i: t,
            _f: i,
            _strict: s
        }).utc()
    }, tt.unix = function(e) {
        return tt(1e3 * e)
    }, tt.duration = function(e, t) {
        var i, n, s, r = tt.isDuration(e),
            o = "number" == typeof e,
            l = r ? e._input : o ? {} : e,
            c = null;
        return o ? t ? l[t] = e : l.milliseconds = e : (c = ft.exec(e)) ? (i = "-" === c[1] ? -1 : 1, l = {
            y: 0,
            d: g(c[ot]) * i,
            h: g(c[lt]) * i,
            m: g(c[ct]) * i,
            s: g(c[dt]) * i,
            ms: g(c[ut]) * i
        }) : (c = gt.exec(e)) && (i = "-" === c[1] ? -1 : 1, s = function(e) {
            var t = e && parseFloat(e.replace(",", "."));
            return (isNaN(t) ? 0 : t) * i
        }, l = {
            y: s(c[2]),
            M: s(c[3]),
            d: s(c[4]),
            h: s(c[5]),
            m: s(c[6]),
            s: s(c[7]),
            w: s(c[8])
        }), n = new a(l), r && e.hasOwnProperty("_lang") && (n._lang = e._lang), n
    }, tt.version = nt, tt.defaultFormat = Mt, tt.updateOffset = function() {}, tt.lang = function(e, t) {
        var i;
        return e ? (t ? C(S(e), t) : null === t ? (P(e), e = "en") : ht[e] || A(e), i = tt.duration.fn._lang = tt.fn._lang = A(e), i._abbr) : tt.fn._lang._abbr
    }, tt.langData = function(e) {
        return e && e._lang && e._lang._abbr && (e = e._lang._abbr), A(e)
    }, tt.isMoment = function(e) {
        return e instanceof s
    }, tt.isDuration = function(e) {
        return e instanceof a
    }, it = jt.length - 1; it >= 0; --it) f(jt[it]);
    for (tt.normalizeUnits = function(e) {
        return p(e)
    }, tt.invalid = function(e) {
        var t = tt.utc(0 / 0);
        return null != e ? r(t._pf, e) : t._pf.userInvalidated = !0, t
    }, tt.parseZone = function(e) {
        return tt(e).parseZone()
    }, r(tt.fn = s.prototype, {
        clone: function() {
            return tt(this)
        },
        valueOf: function() {
            return +this._d + 6e4 * (this._offset || 0)
        },
        unix: function() {
            return Math.floor(+this / 1e3)
        },
        toString: function() {
            return this.clone().lang("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")
        },
        toDate: function() {
            return this._offset ? new Date(+this) : this._d
        },
        toISOString: function() {
            return I(tt(this).utc(), "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]")
        },
        toArray: function() {
            var e = this;
            return [e.year(), e.month(), e.date(), e.hours(), e.minutes(), e.seconds(), e.milliseconds()]
        },
        isValid: function() {
            return k(this)
        },
        isDSTShifted: function() {
            return this._a ? this.isValid() && h(this._a, (this._isUTC ? tt.utc(this._a) : tt(this._a)).toArray()) > 0 : !1
        },
        parsingFlags: function() {
            return r({}, this._pf)
        },
        invalidAt: function() {
            return this._pf.overflow
        },
        utc: function() {
            return this.zone(0)
        },
        local: function() {
            return this.zone(0), this._isUTC = !1, this
        },
        format: function(e) {
            var t = I(this, e || tt.defaultFormat);
            return this.lang().postformat(t)
        },
        add: function(e, t) {
            var i;
            return i = "string" == typeof e ? tt.duration(+t, e) : tt.duration(e, t), c(this, i, 1), this
        },
        subtract: function(e, t) {
            var i;
            return i = "string" == typeof e ? tt.duration(+t, e) : tt.duration(e, t), c(this, i, -1), this
        },
        diff: function(e, t, i) {
            var n, s, a = this._isUTC ? tt(e).zone(this._offset || 0) : tt(e).local(),
                r = 6e4 * (this.zone() - a.zone());
            return t = p(t), "year" === t || "month" === t ? (n = 432e5 * (this.daysInMonth() + a.daysInMonth()), s = 12 * (this.year() - a.year()) + (this.month() - a.month()), s += (this - tt(this).startOf("month") - (a - tt(a).startOf("month"))) / n, s -= 6e4 * (this.zone() - tt(this).startOf("month").zone() - (a.zone() - tt(a).startOf("month").zone())) / n, "year" === t && (s /= 12)) : (n = this - a, s = "second" === t ? n / 1e3 : "minute" === t ? n / 6e4 : "hour" === t ? n / 36e5 : "day" === t ? (n - r) / 864e5 : "week" === t ? (n - r) / 6048e5 : n), i ? s : o(s)
        },
        from: function(e, t) {
            return tt.duration(this.diff(e)).lang(this.lang()._abbr).humanize(!t)
        },
        fromNow: function(e) {
            return this.from(tt(), e)
        },
        calendar: function() {
            var e = this.diff(tt().zone(this.zone()).startOf("day"), "days", !0),
                t = -6 > e ? "sameElse" : -1 > e ? "lastWeek" : 0 > e ? "lastDay" : 1 > e ? "sameDay" : 2 > e ? "nextDay" : 7 > e ? "nextWeek" : "sameElse";
            return this.format(this.lang().calendar(t, this))
        },
        isLeapYear: function() {
            return b(this.year())
        },
        isDST: function() {
            return this.zone() < this.clone().month(0).zone() || this.zone() < this.clone().month(5).zone()
        },
        day: function(e) {
            var t = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
            return null != e ? (e = W(e, this.lang()), this.add({
                d: e - t
            })) : t
        },
        month: function(e) {
            var t, i = this._isUTC ? "UTC" : "";
            return null != e ? "string" == typeof e && (e = this.lang().monthsParse(e), "number" != typeof e) ? this : (t = this.date(), this.date(1), this._d["set" + i + "Month"](e), this.date(Math.min(t, this.daysInMonth())), tt.updateOffset(this), this) : this._d["get" + i + "Month"]()
        },
        startOf: function(e) {
            switch (e = p(e)) {
                case "year":
                    this.month(0);
                case "month":
                    this.date(1);
                case "week":
                case "isoWeek":
                case "day":
                    this.hours(0);
                case "hour":
                    this.minutes(0);
                case "minute":
                    this.seconds(0);
                case "second":
                    this.milliseconds(0)
            }
            return "week" === e ? this.weekday(0) : "isoWeek" === e && this.isoWeekday(1), this
        },
        endOf: function(e) {
            return e = p(e), this.startOf(e).add("isoWeek" === e ? "week" : e, 1).subtract("ms", 1)
        },
        isAfter: function(e, t) {
            return t = "undefined" != typeof t ? t : "millisecond", +this.clone().startOf(t) > +tt(e).startOf(t)
        },
        isBefore: function(e, t) {
            return t = "undefined" != typeof t ? t : "millisecond", +this.clone().startOf(t) < +tt(e).startOf(t)
        },
        isSame: function(e, t) {
            return t = "undefined" != typeof t ? t : "millisecond", +this.clone().startOf(t) === +tt(e).startOf(t)
        },
        min: function(e) {
            return e = tt.apply(null, arguments), this > e ? this : e
        },
        max: function(e) {
            return e = tt.apply(null, arguments), e > this ? this : e
        },
        zone: function(e) {
            var t = this._offset || 0;
            return null == e ? this._isUTC ? t : this._d.getTimezoneOffset() : ("string" == typeof e && (e = E(e)), Math.abs(e) < 16 && (e = 60 * e), this._offset = e, this._isUTC = !0, t !== e && c(this, tt.duration(t - e, "m"), 1, !0), this)
        },
        zoneAbbr: function() {
            return this._isUTC ? "UTC" : ""
        },
        zoneName: function() {
            return this._isUTC ? "Coordinated Universal Time" : ""
        },
        parseZone: function() {
            return "string" == typeof this._i && this.zone(this._i), this
        },
        hasAlignedHourOffset: function(e) {
            return e = e ? tt(e).zone() : 0, 0 === (this.zone() - e) % 60
        },
        daysInMonth: function() {
            return v(this.year(), this.month())
        },
        dayOfYear: function(e) {
            var t = st((tt(this).startOf("day") - tt(this).startOf("year")) / 864e5) + 1;
            return null == e ? t : this.add("d", e - t)
        },
        weekYear: function(e) {
            var t = Q(this, this.lang()._week.dow, this.lang()._week.doy).year;
            return null == e ? t : this.add("y", e - t)
        },
        isoWeekYear: function(e) {
            var t = Q(this, 1, 4).year;
            return null == e ? t : this.add("y", e - t)
        },
        week: function(e) {
            var t = this.lang().week(this);
            return null == e ? t : this.add("d", 7 * (e - t))
        },
        isoWeek: function(e) {
            var t = Q(this, 1, 4).week;
            return null == e ? t : this.add("d", 7 * (e - t))
        },
        weekday: function(e) {
            var t = (this.day() + 7 - this.lang()._week.dow) % 7;
            return null == e ? t : this.add("d", e - t)
        },
        isoWeekday: function(e) {
            return null == e ? this.day() || 7 : this.day(this.day() % 7 ? e : e - 7)
        },
        get: function(e) {
            return e = p(e), this[e]()
        },
        set: function(e, t) {
            return e = p(e), "function" == typeof this[e] && this[e](t), this
        },
        lang: function(t) {
            return t === e ? this._lang : (this._lang = A(t), this)
        }
    }), it = 0; it < Rt.length; it++) G(Rt[it].toLowerCase().replace(/s$/, ""), Rt[it]);
    G("year", "FullYear"), tt.fn.days = tt.fn.day, tt.fn.months = tt.fn.month, tt.fn.weeks = tt.fn.week, tt.fn.isoWeeks = tt.fn.isoWeek, tt.fn.toJSON = tt.fn.toISOString, r(tt.duration.fn = a.prototype, {
        _bubble: function() {
            var e, t, i, n, s = this._milliseconds,
                a = this._days,
                r = this._months,
                l = this._data;
            l.milliseconds = s % 1e3, e = o(s / 1e3), l.seconds = e % 60, t = o(e / 60), l.minutes = t % 60, i = o(t / 60), l.hours = i % 24, a += o(i / 24), l.days = a % 30, r += o(a / 30), l.months = r % 12, n = o(r / 12), l.years = n
        },
        weeks: function() {
            return o(this.days() / 7)
        },
        valueOf: function() {
            return this._milliseconds + 864e5 * this._days + 2592e6 * (this._months % 12) + 31536e6 * g(this._months / 12)
        },
        humanize: function(e) {
            var t = +this,
                i = Y(t, !e, this.lang());
            return e && (i = this.lang().pastFuture(t, i)), this.lang().postformat(i)
        },
        add: function(e, t) {
            var i = tt.duration(e, t);
            return this._milliseconds += i._milliseconds, this._days += i._days, this._months += i._months, this._bubble(), this
        },
        subtract: function(e, t) {
            var i = tt.duration(e, t);
            return this._milliseconds -= i._milliseconds, this._days -= i._days, this._months -= i._months, this._bubble(), this
        },
        get: function(e) {
            return e = p(e), this[e.toLowerCase() + "s"]()
        },
        as: function(e) {
            return e = p(e), this["as" + e.charAt(0).toUpperCase() + e.slice(1) + "s"]()
        },
        lang: tt.fn.lang,
        toIsoString: function() {
            var e = Math.abs(this.years()),
                t = Math.abs(this.months()),
                i = Math.abs(this.days()),
                n = Math.abs(this.hours()),
                s = Math.abs(this.minutes()),
                a = Math.abs(this.seconds() + this.milliseconds() / 1e3);
            return this.asSeconds() ? (this.asSeconds() < 0 ? "-" : "") + "P" + (e ? e + "Y" : "") + (t ? t + "M" : "") + (i ? i + "D" : "") + (n || s || a ? "T" : "") + (n ? n + "H" : "") + (s ? s + "M" : "") + (a ? a + "S" : "") : "P0D"
        }
    });
    for (it in Ot) Ot.hasOwnProperty(it) && (Z(it, Ot[it]), J(it.toLowerCase()));
    Z("Weeks", 6048e5), tt.duration.fn.asMonths = function() {
        return (+this - 31536e6 * this.years()) / 2592e6 + 12 * this.years()
    }, tt.lang("en", {
        ordinal: function(e) {
            var t = e % 10,
                i = 1 === g(e % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th";
            return e + i
        }
    }), pt ? (module.exports = tt, et(!0)) : "function" == typeof define && define.amd ? define("moment", ["require", "exports", "module"], function(t, i, n) {
        return n.config().noGlobal !== !0 && et(n.config().noGlobal === e), tt
    }) : et()
}.call(this);