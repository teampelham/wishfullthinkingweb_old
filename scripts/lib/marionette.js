define(["underscore", "backbone", "marionette-lib"], function(_, bb, marionette) {
    "use strict";
    
    bb.isNodeAttached = function(node) {
        return bb.$.contains(document.documentElement, node)
    };
    
    var newView = _.clone(marionette.View.prototype);      // n
    var newCollectionView = _.clone(marionette.CollectionView.prototype);    // s
    var newRegion = _.clone(marionette.Region.prototype);    // a
    var marionetteExt = {
    
        constructor: function() {
            this.wrapAfterRender(); 
            newView.constructor.apply(this, arguments);
        },
        
        bindAllWhenRendered: function() {
            if (arguments.length <= 0) 
                throw new Error("bindAllWhenRendered requires at least one function");
            
            var args = _.isArray(arguments[0]) ? arguments[0] : arguments;
            _.each(args, function(arg) {
                try {
                    this[arg] = this.whenRendered(this[arg]);
                } catch (err) {
                    throw new Error("bindAllWhenRendered unable to wrap " + JSON.stringify(arg))
                }
            }, this);
        },
        
        whenRendered: function(fn) {
            if (!_.isFunction(fn)) 
                throw new Error("whenRendered must be called with a single function");
                
            return _.bind(function() {
                if (!this.isClosed) {
                    var args = _.toArray(arguments);   // i
                    var applyFn = _.bind(function() {
                        if (this.isRendered && !this.isClosed)
                            fn.apply(this, args);
                    }, this);
                    
                    if (this.isRendered)
                        return void _.defer(applyFn);
                    return void this.listenToOnce(this, "render", function() {
                        _.defer(applyFn)
                    });
                }
            }, this);
        },
        
        wrapAfterRender: function() {
            if (void 0 !== this.onlyAfterRender) {
                if (!_.isArray(this.onlyAfterRender)) 
                    throw new Error("onlyAfterRender must be an array");
                this.bindAllWhenRendered.apply(this, this.onlyAfterRender);
            }
        },
        
        close: function() {
            if (!this.isClosed) {
                var rv = this.triggerMethod("before:close");
                if (rv !== false) {
                    this.isClosed = true; 
                    this.triggerMethod("close");
                    this.unbindUIElements(); 
                    this.remove();
                }
            }
        },
        
        bindUIElements: function() {
            newView.bindUIElements.apply(this, arguments);
            this.bindToModel();
            this.isRendered = true;
        },
        
        unbindUIElements: function() {
            newView.unbindUIElements.apply(this, arguments);    // applies to library
            this.unbindFromModel();                             // calls into BindingsMixin
        },
        
        _getImmediateChildren: function() {
            return [];
        },
        
        _getNestedViews: function() {
            // takes a tree of nested views and recursively builds them into a single array to show them 
            var immediateChildren = this._getImmediateChildren();
            
            if (immediateChildren.length) {
                return _.reduce(immediateChildren, function(memo, value) {
                    return value._getNestedViews ? memo.concat(value._getNestedViews()) : memo;
                }, immediateChildren);
            }
            return immediateChildren;
        }
    };
    
    var marionetteCollectionExt = {
        
        constructor: function() { 
            newCollectionView.constructor.apply(this, arguments);
        },
        
        triggerRendered: function() {
            this.isRendered = true; 
            return newCollectionView.triggerRendered.apply(this, arguments);
        },
        
        rebase: function(collection) {
            this.stopListening(this.collection);
            this.collection = collection;
            this._initialEvents();
            
            if(_.isFunction(this._renderChildren))
                this.renderChildren();
            else
                this.render();
            return this;
        },
        
        _getImmediateChildren: function() {
            return _.values(this.children._views);
        },
        
        childView: function() {
            return this._getImmediateChildren();  
        }
    };
        
    var marionetteCompositeExt = {
        constructor: function() {
            newCollectionView.constructor.apply(this, arguments);
        }
    };
    
    var marionetteRegionExt = {
        append: function(view, parentView, prepend) {
            var renderParent = view.isClosed || _.isUndefined(view.$el);    // sometimes views can't be rendered...
            var notCurrent = view !== this.currentView;
            
            view.render();      // make it so, Number One
            
            if (notCurrent || renderParent) {
                if (!parentView)
                    parentView = this.getEl(this.el);
                
                // push the view's element into its parent view
                if(prepend)
                    parentView.prepend(view.el);
                else
                    parentView.append(view.el);
            }
            
            this.attachView(view);
            marionette.triggerMethod.call(this, "show", view);  // trigger custom events
            marionette.triggerMethod.call(view, "show");
            this._triggerAttached(view, parentView);            // trigger internal events
            
            return this;
        },
        
        prepend: function(view, parentView) {
            return this.append(view, parentView, true);
        },
        
        show: function(view) {
            newRegion.show.apply(this, arguments);
            this._triggerAttached(view, this.$el);
        },
        
        _triggerAttached: function(view, el) {
            
            if (bb.isNodeAttached(el.get(0))) {
                var allViews = _.union([view], _.result(view, "_getNestedViews") || []);
                _.each(allViews, function(viewItem) {
                    if (viewItem.triggerMethod)
                        viewItem.triggerMethod("attached", this);
                }, this);
            }
        }
    };
    
    var marionetteLayoutExt = {
        _getImmediateChildren: function() {
            return _.chain(this.regionManager._regions).values().pluck("currentView").compact().value();
        }
    };
    
    _.extend(marionette.View.prototype, marionetteExt);
    _.extend(marionette.ItemView.prototype, marionetteExt);
    _.extend(marionette.CollectionView.prototype, marionetteExt, marionetteCollectionExt);
    _.extend(marionette.CompositeView.prototype, marionetteExt, marionetteCompositeExt);
    _.extend(marionette.Region.prototype, marionetteRegionExt);
    _.extend(marionette.Layout.prototype, marionetteLayoutExt);

    return marionette;
});