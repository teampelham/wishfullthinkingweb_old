window.Modernizr = function(e, t, i) {
    function n(e) {
        v.cssText = e
    }

    function s(e, t) {
        return typeof e === t
    }

    function a(e, t) {
        return !!~("" + e).indexOf(t)
    }

    function r(e, t) {
        for (var n in e) {
            var s = e[n];
            if (!a(s, "-") && v[s] !== i) return "pfx" == t ? s : !0
        }
        return !1
    }

    function o(e, t, n) {
        for (var a in e) {
            var r = t[e[a]];
            if (r !== i) return n === !1 ? e[a] : s(r, "function") ? r.bind(n || t) : r
        }
        return !1
    }

    function l(e, t, i) {
        var n = e.charAt(0).toUpperCase() + e.slice(1),
            a = (e + " " + w.join(n + " ") + n).split(" ");
        return s(t, "string") || s(t, "undefined") ? r(a, t) : (a = (e + " " + x.join(n + " ") + n).split(" "), o(a, t, i))
    }
    var c, d, u, h = "2.8.3",
        p = {}, m = t.documentElement,
        f = "modernizr",
        g = t.createElement(f),
        v = g.style,
        y = ({}.toString, " -webkit- -moz- -o- -ms- ".split(" ")),
        b = "Webkit Moz O ms",
        w = b.split(" "),
        x = b.toLowerCase().split(" "),
        k = {}, S = [],
        C = S.slice,
        P = function(e, i, n, s) {
            var a, r, o, l, c = t.createElement("div"),
                d = t.body,
                u = d || t.createElement("body");
            if (parseInt(n, 10))
                for (; n--;) o = t.createElement("div"), o.id = s ? s[n] : f + (n + 1), c.appendChild(o);
            return a = ["&#173;", '<style id="s', f, '">', e, "</style>"].join(""), c.id = f, (d ? c : u).innerHTML += a, u.appendChild(c), d || (u.style.background = "", u.style.overflow = "hidden", l = m.style.overflow, m.style.overflow = "hidden", m.appendChild(u)), r = i(c, e), d ? c.parentNode.removeChild(c) : (u.parentNode.removeChild(u), m.style.overflow = l), !! r
        }, A = {}.hasOwnProperty;
    u = s(A, "undefined") || s(A.call, "undefined") ? function(e, t) {
        return t in e && s(e.constructor.prototype[t], "undefined")
    } : function(e, t) {
        return A.call(e, t)
    }, Function.prototype.bind || (Function.prototype.bind = function(e) {
        var t = this;
        if ("function" != typeof t) throw new TypeError;
        var i = C.call(arguments, 1),
            n = function() {
                if (this instanceof n) {
                    var s = function() {};
                    s.prototype = t.prototype;
                    var a = new s,
                        r = t.apply(a, i.concat(C.call(arguments)));
                    return Object(r) === r ? r : a
                }
                return t.apply(e, i.concat(C.call(arguments)))
            };
        return n
    }), k.csstransforms3d = function() {
        var e = !! l("perspective");
        return e && "webkitPerspective" in m.style && P("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(t) {
            e = 9 === t.offsetLeft && 3 === t.offsetHeight
        }), e
    };
    for (var _ in k) u(k, _) && (d = _.toLowerCase(), p[d] = k[_](), S.push((p[d] ? "" : "no-") + d));
    return p.addTest = function(e, t) {
        if ("object" == typeof e)
            for (var n in e) u(e, n) && p.addTest(n, e[n]);
        else {
            if (e = e.toLowerCase(), p[e] !== i) return p;
            t = "function" == typeof t ? t() : t, "undefined" != typeof enableClasses && enableClasses && (m.className += " " + (t ? "" : "no-") + e), p[e] = t
        }
        return p
    }, n(""), g = c = null, p._version = h, p._prefixes = y, p._domPrefixes = x, p._cssomPrefixes = w, p.testProp = function(e) {
        return r([e])
    }, p.testAllProps = l, p.testStyles = P, p
}(this, this.document), define("modernizr", function(e) {
    return function() {
        var t;
        return t || e.Modernizr
    }
}(this));