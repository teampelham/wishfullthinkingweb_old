+ function(e) {
    "use strict";
    var t = function(e, t) {
        this.type = this.options = this.enabled = this.timeout = this.hoverState = this.$element = null, this.init("tooltip", e, t)
    };
    t.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1
    }, t.prototype.init = function(t, i, n) {
        this.enabled = !0, this.type = t, this.$element = e(i), this.options = this.getOptions(n);
        for (var s = this.options.trigger.split(" "), a = s.length; a--;) {
            var r = s[a];
            if ("click" == r) this.$element.on("click." + this.type, this.options.selector, e.proxy(this.toggle, this));
            else if ("manual" != r) {
                var o = "hover" == r ? "mouseenter" : "focus",
                    l = "hover" == r ? "mouseleave" : "blur";
                this.$element.on(o + "." + this.type, this.options.selector, e.proxy(this.enter, this)), this.$element.on(l + "." + this.type, this.options.selector, e.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = e.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }, t.prototype.getDefaults = function() {
        return t.DEFAULTS
    }, t.prototype.getOptions = function(t) {
        return t = e.extend({}, this.getDefaults(), this.$element.data(), t), t.delay && "number" == typeof t.delay && (t.delay = {
            show: t.delay,
            hide: t.delay
        }), t
    }, t.prototype.getDelegateOptions = function() {
        var t = {}, i = this.getDefaults();
        return this._options && e.each(this._options, function(e, n) {
            i[e] != n && (t[e] = n)
        }), t
    }, t.prototype.enter = function(t) {
        var i = t instanceof this.constructor ? t : e(t.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
        return clearTimeout(i.timeout), i.hoverState = "in", i.options.delay && i.options.delay.show ? void(i.timeout = setTimeout(function() {
            "in" == i.hoverState && i.show()
        }, i.options.delay.show)) : i.show()
    }, t.prototype.leave = function(t) {
        var i = t instanceof this.constructor ? t : e(t.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
        return clearTimeout(i.timeout), i.hoverState = "out", i.options.delay && i.options.delay.hide ? void(i.timeout = setTimeout(function() {
            "out" == i.hoverState && i.hide()
        }, i.options.delay.hide)) : i.hide()
    }, t.prototype.show = function() {
        var t = e.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            if (this.$element.trigger(t), t.isDefaultPrevented()) return;
            var i = this.tip();
            this.setContent(), this.options.animation && i.addClass("fade");
            var n = "function" == typeof this.options.placement ? this.options.placement.call(this, i[0], this.$element[0]) : this.options.placement,
                s = /\s?auto?\s?/i,
                a = s.test(n);
            a && (n = n.replace(s, "") || "top"), i.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(n), this.options.container ? i.appendTo(this.options.container) : i.insertAfter(this.$element);
            var r = this.getPosition(),
                o = i[0].offsetWidth,
                l = i[0].offsetHeight;
            if (a) {
                var c = this.$element.parent(),
                    d = n,
                    u = document.documentElement.scrollTop || document.body.scrollTop,
                    h = "body" == this.options.container ? window.innerWidth : c.outerWidth(),
                    p = "body" == this.options.container ? window.innerHeight : c.outerHeight(),
                    m = "body" == this.options.container ? 0 : c.offset().left;
                n = "bottom" == n && r.top + r.height + l - u > p ? "top" : "top" == n && r.top - u - l < 0 ? "bottom" : "right" == n && r.right + o > h ? "left" : "left" == n && r.left - o < m ? "right" : n, i.removeClass(d).addClass(n)
            }
            var f = this.getCalculatedOffset(n, r, o, l);
            this.applyPlacement(f, n), this.$element.trigger("shown.bs." + this.type)
        }
    }, t.prototype.applyPlacement = function(e, t) {
        var i, n = this.tip(),
            s = n[0].offsetWidth,
            a = n[0].offsetHeight,
            r = parseInt(n.css("margin-top"), 10),
            o = parseInt(n.css("margin-left"), 10);
        isNaN(r) && (r = 0), isNaN(o) && (o = 0), e.top = e.top + r, e.left = e.left + o, n.offset(e).addClass("in");
        var l = n[0].offsetWidth,
            c = n[0].offsetHeight;
        if ("top" == t && c != a && (i = !0, e.top = e.top + a - c), /bottom|top/.test(t)) {
            var d = 0;
            e.left < 0 && (d = -2 * e.left, e.left = 0, n.offset(e), l = n[0].offsetWidth, c = n[0].offsetHeight), this.replaceArrow(d - s + l, l, "left")
        } else this.replaceArrow(c - a, c, "top");
        i && n.offset(e)
    }, t.prototype.replaceArrow = function(e, t, i) {
        this.arrow().css(i, e ? 50 * (1 - e / t) + "%" : "")
    }, t.prototype.setContent = function() {
        var e = this.tip(),
            t = this.getTitle();
        e.find(".tooltip-inner")[this.options.html ? "html" : "text"](t), e.removeClass("fade in top bottom left right")
    }, t.prototype.hide = function() {
        function t() {
            "in" != i.hoverState && n.detach()
        }
        var i = this,
            n = this.tip(),
            s = e.Event("hide.bs." + this.type);
        return this.$element.trigger(s), s.isDefaultPrevented() ? void 0 : (n.removeClass("in"), e.support.transition && this.$tip.hasClass("fade") ? n.one(e.support.transition.end, t).emulateTransitionEnd(150) : t(), this.$element.trigger("hidden.bs." + this.type), this)
    }, t.prototype.fixTitle = function() {
        var e = this.$element;
        (e.attr("title") || "string" != typeof e.attr("data-original-title")) && e.attr("data-original-title", e.attr("title") || "").attr("title", "")
    }, t.prototype.hasContent = function() {
        return this.getTitle()
    }, t.prototype.getPosition = function() {
        var t = this.$element[0];
        return e.extend({}, "function" == typeof t.getBoundingClientRect ? t.getBoundingClientRect() : {
            width: t.offsetWidth,
            height: t.offsetHeight
        }, this.$element.offset())
    }, t.prototype.getCalculatedOffset = function(e, t, i, n) {
        return "bottom" == e ? {
            top: t.top + t.height,
            left: t.left + t.width / 2 - i / 2
        } : "top" == e ? {
            top: t.top - n,
            left: t.left + t.width / 2 - i / 2
        } : "left" == e ? {
            top: t.top + t.height / 2 - n / 2,
            left: t.left - i
        } : {
            top: t.top + t.height / 2 - n / 2,
            left: t.left + t.width
        }
    }, t.prototype.getTitle = function() {
        var e, t = this.$element,
            i = this.options;
        return e = t.attr("data-original-title") || ("function" == typeof i.title ? i.title.call(t[0]) : i.title)
    }, t.prototype.tip = function() {
        return this.$tip = this.$tip || e(this.options.template)
    }, t.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, t.prototype.validate = function() {
        this.$element[0].parentNode || (this.hide(), this.$element = null, this.options = null)
    }, t.prototype.enable = function() {
        this.enabled = !0
    }, t.prototype.disable = function() {
        this.enabled = !1
    }, t.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled
    }, t.prototype.toggle = function(t) {
      var i = t ? e(t.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type) : this;
        i.tip().hasClass("in") ? i.leave(i) : i.enter(i)
    }, t.prototype.destroy = function() {
        this.hide().$element.off("." + this.type).removeData("bs." + this.type)
    };
    var i = e.fn.tooltip;
    e.fn.tooltip = function(i) {
        return this.each(function() {
            var n = e(this),
                s = n.data("bs.tooltip"),
                a = "object" == typeof i && i;
            s || n.data("bs.tooltip", s = new t(this, a)), "string" == typeof i && s[i]()
        })
    }, e.fn.tooltip.Constructor = t, e.fn.tooltip.noConflict = function() {
        return e.fn.tooltip = i, this
    }
}(window.jQuery), define("tooltip", ["jquery"], function() {})