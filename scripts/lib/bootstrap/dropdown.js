+ function(e) {
    "use strict";

    function t() {
        e(n).remove(), e(s).each(function(t) {
            var n = i(e(this));
            n.hasClass("open") && (n.trigger(t = e.Event("hide.bs.dropdown")), t.isDefaultPrevented() || n.removeClass("open").trigger("hidden.bs.dropdown"))
        })
    }

    function i(t) {
        var i = t.attr("data-target");
        i || (i = t.attr("href"), i = i && /#/.test(i) && i.replace(/.*(?=#[^\s]*$)/, ""));
        var n = i && e(i);
        return n && n.length ? n : t.parent()
    }
    var n = ".dropdown-backdrop",
        s = "[data-toggle=dropdown]",
        a = function(t) {
            e(t).on("click.bs.dropdown", this.toggle)
        };
    a.prototype.toggle = function(n) {
        var s = e(this);
        if (!s.is(".disabled, :disabled")) {
            var a = i(s),
                r = a.hasClass("open");
            if (t(), !r) {
                if ("ontouchstart" in document.documentElement && !a.closest(".navbar-nav").length && e('<div class="dropdown-backdrop"/>').insertAfter(e(this)).on("click", t), a.trigger(n = e.Event("show.bs.dropdown")), n.isDefaultPrevented()) return;
                a.toggleClass("open").trigger("shown.bs.dropdown"), s.focus()
            }
            return !1
        }
    }, a.prototype.keydown = function(t) {
        if (/(38|40|27)/.test(t.keyCode)) {
            var n = e(this);
            if (t.preventDefault(), t.stopPropagation(), !n.is(".disabled, :disabled")) {
                var a = i(n),
                    r = a.hasClass("open");
                if (!r || r && 27 == t.keyCode) return 27 == t.which && a.find(s).focus(), n.click();
                var o = e("[role=menu] li:not(.divider):visible a", a);
                if (o.length) {
                    var l = o.index(o.filter(":focus"));
                    38 == t.keyCode && l > 0 && l--, 40 == t.keyCode && l < o.length - 1 && l++, ~l || (l = 0), o.eq(l).focus()
                }
            }
        }
    };
    var r = e.fn.dropdown;
    e.fn.dropdown = function(t) {
        return this.each(function() {
            var i = e(this),
                n = i.data("dropdown");
            n || i.data("dropdown", n = new a(this)), "string" == typeof t && n[t].call(i)
        })
    }, e.fn.dropdown.Constructor = a, e.fn.dropdown.noConflict = function() {
        return e.fn.dropdown = r, this
    }, e(document).on("click.bs.dropdown.data-api", t).on("click.bs.dropdown.data-api", ".dropdown form", function(e) {
        e.stopPropagation()
    }).on("click.bs.dropdown.data-api", s, a.prototype.toggle).on("keydown.bs.dropdown.data-api", s + ", [role=menu]", a.prototype.keydown)
}(window.jQuery), define("dropdown", ["jquery"], function() {})