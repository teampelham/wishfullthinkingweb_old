+ function(e) {
    "use strict";
    var t = function(i, n) {
        this.$element = e(i), this.options = e.extend({}, t.DEFAULTS, n)
    };
    t.DEFAULTS = {
        loadingText: "loading..."
    }, t.prototype.setState = function(e) {
        var t = "disabled",
            i = this.$element,
            n = i.is("input") ? "val" : "html",
            s = i.data();
        e += "Text", s.resetText || i.data("resetText", i[n]()), i[n](s[e] || this.options[e]), setTimeout(function() {
            "loadingText" == e ? i.addClass(t).attr(t, t) : i.removeClass(t).removeAttr(t)
        }, 0)
    }, t.prototype.toggle = function() {
        var e = this.$element.closest('[data-toggle="buttons"]');
        if (e.length) {
            var t = this.$element.find("input").prop("checked", !this.$element.hasClass("active")).trigger("change");
            "radio" === t.prop("type") && e.find(".active").removeClass("active")
        }
        this.$element.toggleClass("active")
    };
    var i = e.fn.button;
    e.fn.button = function(i) {
        return this.each(function() {
            var n = e(this),
                s = n.data("bs.button"),
                a = "object" == typeof i && i;
            s || n.data("bs.button", s = new t(this, a)), "toggle" == i ? s.toggle() : i && s.setState(i)
        })
    }, e.fn.button.Constructor = t, e.fn.button.noConflict = function() {
        return e.fn.button = i, this
    }, e(document).on("click.bs.button.data-api", "[data-toggle^=button]", function(t) {
        var i = e(t.target);
        i.hasClass("btn") || (i = i.closest(".btn")), i.button("toggle"), t.preventDefault()
    })
}(window.jQuery), define("button", ["jquery"], function() {})