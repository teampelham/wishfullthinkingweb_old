+ function(e) {
    "use strict";

    function t() {
        var e = document.createElement("bootstrap"),
            t = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd otransitionend",
                transition: "transitionend"
            };
        for (var i in t)
            if (void 0 !== e.style[i]) return {
                end: t[i]
            }
    }
    e.fn.emulateTransitionEnd = function(t) {
        var i = !1,
            n = this;
        e(this).one(e.support.transition.end, function() {
            i = !0
        });
        var s = function() {
            i || e(n).trigger(e.support.transition.end)
        };
        return setTimeout(s, t), this
    }, e(function() {
        e.support.transition = t()
    })
}(window.jQuery), define("transition", ["jquery"], function() {})