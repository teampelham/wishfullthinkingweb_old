+function(e, t) {
    function i(e) {
        var t = e.length,
            i = dt.type(e);
        return dt.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === i || "function" !== i && (0 === t || "number" == typeof t && t > 0 && t - 1 in e)
    }

    function n(e) {
        var t = Pt[e] = {};
        return dt.each(e.match(ht) || [], function(e, i) {
            t[i] = !0
        }), t
    }

    function s(e, i, n, s) {
        if (dt.acceptData(e)) {
            var a, r, o = dt.expando,
                l = e.nodeType,
                c = l ? dt.cache : e,
                d = l ? e[o] : e[o] && o;
            if (d && c[d] && (s || c[d].data) || n !== t || "string" != typeof i) return d || (d = l ? e[o] = tt.pop() || dt.guid++ : o), c[d] || (c[d] = l ? {} : {
                toJSON: dt.noop
            }), ("object" == typeof i || "function" == typeof i) && (s ? c[d] = dt.extend(c[d], i) : c[d].data = dt.extend(c[d].data, i)), r = c[d], s || (r.data || (r.data = {}), r = r.data), n !== t && (r[dt.camelCase(i)] = n), "string" == typeof i ? (a = r[i], null == a && (a = r[dt.camelCase(i)])) : a = r, a
        }
    }

    function a(e, t, i) {
        if (dt.acceptData(e)) {
            var n, s, a = e.nodeType,
                r = a ? dt.cache : e,
                l = a ? e[dt.expando] : dt.expando;
            if (r[l]) {
                if (t && (n = i ? r[l] : r[l].data)) {
                    dt.isArray(t) ? t = t.concat(dt.map(t, dt.camelCase)) : t in n ? t = [t] : (t = dt.camelCase(t), t = t in n ? [t] : t.split(" ")), s = t.length;
                    for (; s--;) delete n[t[s]];
                    if (i ? !o(n) : !dt.isEmptyObject(n)) return
                }(i || (delete r[l].data, o(r[l]))) && (a ? dt.cleanData([e], !0) : dt.support.deleteExpando || r != r.window ? delete r[l] : r[l] = null)
            }
        }
    }

    function r(e, i, n) {
        if (n === t && 1 === e.nodeType) {
            var s = "data-" + i.replace(_t, "-$1").toLowerCase();
            if (n = e.getAttribute(s), "string" == typeof n) {
                try {
                    n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : At.test(n) ? dt.parseJSON(n) : n
                } catch (a) {}
                dt.data(e, i, n)
            } else n = t
        }
        return n
    }

    function o(e) {
        var t;
        for (t in e)
            if (("data" !== t || !dt.isEmptyObject(e[t])) && "toJSON" !== t) return !1;
        return !0
    }

    function l() {
        return !0
    }

    function c() {
        return !1
    }

    function d() {
        try {
            return X.activeElement
        } catch (e) {}
    }

    function u(e, t) {
        do e = e[t]; while (e && 1 !== e.nodeType);
        return e
    }

    function h(e, t, i) {
        if (dt.isFunction(t)) return dt.grep(e, function(e, n) {
            return !!t.call(e, n, e) !== i
        });
        if (t.nodeType) return dt.grep(e, function(e) {
            return e === t !== i
        });
        if ("string" == typeof t) {
            if (jt.test(t)) return dt.filter(t, e, i);
            t = dt.filter(t, e)
        }
        return dt.grep(e, function(e) {
            return dt.inArray(e, t) >= 0 !== i
        })
    }

    function p(e) {
        var t = $t.split("|"),
            i = e.createDocumentFragment();
        if (i.createElement)
            for (; t.length;) i.createElement(t.pop());
        return i
    }

    function m(e, t) {
        return dt.nodeName(e, "table") && dt.nodeName(1 === t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
    }

    function f(e) {
        return e.type = (null !== dt.find.attr(e, "type")) + "/" + e.type, e
    }

    function g(e) {
        var t = si.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function v(e, t) {
        for (var i, n = 0; null != (i = e[n]); n++) dt._data(i, "globalEval", !t || dt._data(t[n], "globalEval"))
    }

    function y(e, t) {
        if (1 === t.nodeType && dt.hasData(e)) {
            var i, n, s, a = dt._data(e),
                r = dt._data(t, a),
                o = a.events;
            if (o) {
                delete r.handle, r.events = {};
                for (i in o)
                    for (n = 0, s = o[i].length; s > n; n++) dt.event.add(t, i, o[i][n])
            }
            r.data && (r.data = dt.extend({}, r.data))
        }
    }

    function b(e, t) {
        var i, n, s;
        if (1 === t.nodeType) {
            if (i = t.nodeName.toLowerCase(), !dt.support.noCloneEvent && t[dt.expando]) {
                s = dt._data(t);
                for (n in s.events) dt.removeEvent(t, n, s.handle);
                t.removeAttribute(dt.expando)
            }
            "script" === i && t.text !== e.text ? (f(t).text = e.text, g(t)) : "object" === i ? (t.parentNode && (t.outerHTML = e.outerHTML), dt.support.html5Clone && e.innerHTML && !dt.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === i && ti.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === i ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === i || "textarea" === i) && (t.defaultValue = e.defaultValue)
        }
    }

    function w(e, i) {
        var n, s, a = 0,
            r = typeof e.getElementsByTagName !== Q ? e.getElementsByTagName(i || "*") : typeof e.querySelectorAll !== Q ? e.querySelectorAll(i || "*") : t;
        if (!r)
            for (r = [], n = e.childNodes || e; null != (s = n[a]); a++)!i || dt.nodeName(s, i) ? r.push(s) : dt.merge(r, w(s, i));
        return i === t || i && dt.nodeName(e, i) ? dt.merge([e], r) : r
    }

    function x(e) {
        ti.test(e.type) && (e.defaultChecked = e.checked)
    }

    function k(e, t) {
        if (t in e) return t;
        for (var i = t.charAt(0).toUpperCase() + t.slice(1), n = t, s = Ci.length; s--;)
            if (t = Ci[s] + i, t in e) return t;
        return n
    }

    function S(e, t) {
        return e = t || e, "none" === dt.css(e, "display") || !dt.contains(e.ownerDocument, e)
    }

    function C(e, t) {
        for (var i, n, s, a = [], r = 0, o = e.length; o > r; r++) n = e[r], n.style && (a[r] = dt._data(n, "olddisplay"), i = n.style.display, t ? (a[r] || "none" !== i || (n.style.display = ""), "" === n.style.display && S(n) && (a[r] = dt._data(n, "olddisplay", T(n.nodeName)))) : a[r] || (s = S(n), (i && "none" !== i || !s) && dt._data(n, "olddisplay", s ? i : dt.css(n, "display"))));
        for (r = 0; o > r; r++) n = e[r], n.style && (t && "none" !== n.style.display && "" !== n.style.display || (n.style.display = t ? a[r] || "" : "none"));
        return e
    }

    function P(e, t, i) {
        var n = vi.exec(t);
        return n ? Math.max(0, n[1] - (i || 0)) + (n[2] || "px") : t
    }

    function A(e, t, i, n, s) {
        for (var a = i === (n ? "border" : "content") ? 4 : "width" === t ? 1 : 0, r = 0; 4 > a; a += 2) "margin" === i && (r += dt.css(e, i + Si[a], !0, s)), n ? ("content" === i && (r -= dt.css(e, "padding" + Si[a], !0, s)), "margin" !== i && (r -= dt.css(e, "border" + Si[a] + "Width", !0, s))) : (r += dt.css(e, "padding" + Si[a], !0, s), "padding" !== i && (r += dt.css(e, "border" + Si[a] + "Width", !0, s)));
        return r
    }

    function _(e, t, i) {
        var n = !0,
            s = "width" === t ? e.offsetWidth : e.offsetHeight,
            a = di(e),
            r = dt.support.boxSizing && "border-box" === dt.css(e, "boxSizing", !1, a);
        if (0 >= s || null == s) {
            if (s = ui(e, t, a), (0 > s || null == s) && (s = e.style[t]), yi.test(s)) return s;
            n = r && (dt.support.boxSizingReliable || s === e.style[t]), s = parseFloat(s) || 0
        }
        return s + A(e, t, i || (r ? "border" : "content"), n, a) + "px"
    }

    function T(e) {
        var t = X,
            i = wi[e];
        return i || (i = I(e, t), "none" !== i && i || (ci = (ci || dt("<iframe frameborder='0' width='0' height='0'/>").css("cssText", "display:block !important")).appendTo(t.documentElement), t = (ci[0].contentWindow || ci[0].contentDocument).document, t.write("<!doctype html><html><body>"), t.close(), i = I(e, t), ci.detach()), wi[e] = i), i
    }

    function I(e, t) {
        var i = dt(t.createElement(e)).appendTo(t.body),
            n = dt.css(i[0], "display");
        return i.remove(), n
    }

    function M(e, t, i, n) {
        var s;
        if (dt.isArray(t)) dt.each(t, function(t, s) {
            i || Ai.test(e) ? n(e, s) : M(e + "[" + ("object" == typeof s ? t : "") + "]", s, i, n)
        });
        else if (i || "object" !== dt.type(t)) n(e, t);
        else
            for (s in t) M(e + "[" + s + "]", t[s], i, n)
    }

    function D(e) {
        return function(t, i) {
            "string" != typeof t && (i = t, t = "*");
            var n, s = 0,
                a = t.toLowerCase().match(ht) || [];
            if (dt.isFunction(i))
                for (; n = a[s++];) "+" === n[0] ? (n = n.slice(1) || "*", (e[n] = e[n] || []).unshift(i)) : (e[n] = e[n] || []).push(i)
        }
    }

    function E(e, t, i, n) {
        function s(o) {
            var l;
            return a[o] = !0, dt.each(e[o] || [], function(e, o) {
                var c = o(t, i, n);
                return "string" != typeof c || r || a[c] ? r ? !(l = c) : void 0 : (t.dataTypes.unshift(c), s(c), !1)
            }), l
        }
        var a = {}, r = e === qi;
        return s(t.dataTypes[0]) || !a["*"] && s("*")
    }

    function L(e, i) {
        var n, s, a = dt.ajaxSettings.flatOptions || {};
        for (s in i) i[s] !== t && ((a[s] ? e : n || (n = {}))[s] = i[s]);
        return n && dt.extend(!0, e, n), e
    }

    function R(e, i, n) {
        for (var s, a, r, o, l = e.contents, c = e.dataTypes;
            "*" === c[0];) c.shift(), a === t && (a = e.mimeType || i.getResponseHeader("Content-Type"));
        if (a)
            for (o in l)
                if (l[o] && l[o].test(a)) {
                    c.unshift(o);
                    break
                }
        if (c[0] in n) r = c[0];
        else {
            for (o in n) {
                if (!c[0] || e.converters[o + " " + c[0]]) {
                    r = o;
                    break
                }
                s || (s = o)
            }
            r = r || s
        }
        return r ? (r !== c[0] && c.unshift(r), n[r]) : void 0
    }

    function O(e, t, i, n) {
        var s, a, r, o, l, c = {}, d = e.dataTypes.slice();
        if (d[1])
            for (r in e.converters) c[r.toLowerCase()] = e.converters[r];
        for (a = d.shift(); a;)
            if (e.responseFields[a] && (i[e.responseFields[a]] = t), !l && n && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = a, a = d.shift())
                if ("*" === a) a = l;
                else if ("*" !== l && l !== a) {
            if (r = c[l + " " + a] || c["* " + a], !r)
                for (s in c)
                    if (o = s.split(" "), o[1] === a && (r = c[l + " " + o[0]] || c["* " + o[0]])) {
                        r === !0 ? r = c[s] : c[s] !== !0 && (a = o[0], d.unshift(o[1]));
                        break
                    }
            if (r !== !0)
                if (r && e["throws"]) t = r(t);
                else try {
                    t = r(t)
                } catch (u) {
                    return {
                        state: "parsererror",
                        error: r ? u : "No conversion from " + l + " to " + a
                    }
                }
        }
        return {
            state: "success",
            data: t
        }
    }

    function N() {
        try {
            return new e.XMLHttpRequest
        } catch (t) {}
    }

    function F() {
        try {
            return new e.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) {}
    }

    function V() {
        return setTimeout(function() {
            Ji = t
        }), Ji = dt.now()
    }

    function B(e, t, i) {
        for (var n, s = (an[t] || []).concat(an["*"]), a = 0, r = s.length; r > a; a++)
            if (n = s[a].call(i, t, e)) return n
    }

    function U(e, t, i) {
        var n, s, a = 0,
            r = sn.length,
            o = dt.Deferred().always(function() {
                delete l.elem
            }),
            l = function() {
                if (s) return !1;
                for (var t = Ji || V(), i = Math.max(0, c.startTime + c.duration - t), n = i / c.duration || 0, a = 1 - n, r = 0, l = c.tweens.length; l > r; r++) c.tweens[r].run(a);
                return o.notifyWith(e, [c, a, i]), 1 > a && l ? i : (o.resolveWith(e, [c]), !1)
            }, c = o.promise({
                elem: e,
                props: dt.extend({}, t),
                opts: dt.extend(!0, {
                    specialEasing: {}
                }, i),
                originalProperties: t,
                originalOptions: i,
                startTime: Ji || V(),
                duration: i.duration,
                tweens: [],
                createTween: function(t, i) {
                    var n = dt.Tween(e, c.opts, t, i, c.opts.specialEasing[t] || c.opts.easing);
                    return c.tweens.push(n), n
                },
                stop: function(t) {
                    var i = 0,
                        n = t ? c.tweens.length : 0;
                    if (s) return this;
                    for (s = !0; n > i; i++) c.tweens[i].run(1);
                    return t ? o.resolveWith(e, [c, t]) : o.rejectWith(e, [c, t]), this
                }
            }),
            d = c.props;
        for (z(d, c.opts.specialEasing); r > a; a++)
            if (n = sn[a].call(c, e, d, c.opts)) return n;
        return dt.map(d, B, c), dt.isFunction(c.opts.start) && c.opts.start.call(e, c), dt.fx.timer(dt.extend(l, {
            elem: e,
            anim: c,
            queue: c.opts.queue
        })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
    }

    function z(e, t) {
        var i, n, s, a, r;
        for (i in e)
            if (n = dt.camelCase(i), s = t[n], a = e[i], dt.isArray(a) && (s = a[1], a = e[i] = a[0]), i !== n && (e[n] = a, delete e[i]), r = dt.cssHooks[n], r && "expand" in r) {
                a = r.expand(a), delete e[n];
                for (i in a) i in e || (e[i] = a[i], t[i] = s)
            } else t[n] = s
    }

    function j(e, t, i) {
        var n, s, a, r, o, l, c = this,
            d = {}, u = e.style,
            h = e.nodeType && S(e),
            p = dt._data(e, "fxshow");
        i.queue || (o = dt._queueHooks(e, "fx"), null == o.unqueued && (o.unqueued = 0, l = o.empty.fire, o.empty.fire = function() {
            o.unqueued || l()
        }), o.unqueued++, c.always(function() {
            c.always(function() {
                o.unqueued--, dt.queue(e, "fx").length || o.empty.fire()
            })
        })), 1 === e.nodeType && ("height" in t || "width" in t) && (i.overflow = [u.overflow, u.overflowX, u.overflowY], "inline" === dt.css(e, "display") && "none" === dt.css(e, "float") && (dt.support.inlineBlockNeedsLayout && "inline" !== T(e.nodeName) ? u.zoom = 1 : u.display = "inline-block")), i.overflow && (u.overflow = "hidden", dt.support.shrinkWrapBlocks || c.always(function() {
            u.overflow = i.overflow[0], u.overflowX = i.overflow[1], u.overflowY = i.overflow[2]
        }));
        for (n in t)
            if (s = t[n], en.exec(s)) {
                if (delete t[n], a = a || "toggle" === s, s === (h ? "hide" : "show")) continue;
                d[n] = p && p[n] || dt.style(e, n)
            }
        if (!dt.isEmptyObject(d)) {
            p ? "hidden" in p && (h = p.hidden) : p = dt._data(e, "fxshow", {}), a && (p.hidden = !h), h ? dt(e).show() : c.done(function() {
                dt(e).hide()
            }), c.done(function() {
                var t;
                dt._removeData(e, "fxshow");
                for (t in d) dt.style(e, t, d[t])
            });
            for (n in d) r = B(h ? p[n] : 0, n, c), n in p || (p[n] = r.start, h && (r.end = r.start, r.start = "width" === n || "height" === n ? 1 : 0))
        }
    }

    function q(e, t, i, n, s) {
        return new q.prototype.init(e, t, i, n, s)
    }

    function H(e, t) {
        var i, n = {
                height: e
            }, s = 0;
        for (t = t ? 1 : 0; 4 > s; s += 2 - t) i = Si[s], n["margin" + i] = n["padding" + i] = e;
        return t && (n.opacity = n.width = e), n
    }

    function W(e) {
        return dt.isWindow(e) ? e : 9 === e.nodeType ? e.defaultView || e.parentWindow : !1
    }
    var $, Y, Q = typeof t,
        K = e.location,
        X = e.document,
        G = X.documentElement,
        J = e.jQuery,
        Z = e.$,
        et = {}, tt = [],
        it = "1.10.2",
        nt = tt.concat,
        st = tt.push,
        at = tt.slice,
        rt = tt.indexOf,
        ot = et.toString,
        lt = et.hasOwnProperty,
        ct = it.trim,
        dt = function(e, t) {
            return new dt.fn.init(e, t, Y)
        }, ut = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        ht = /\S+/g,
        pt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        mt = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        ft = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        gt = /^[\],:{}\s]*$/,
        vt = /(?:^|:|,)(?:\s*\[)+/g,
        yt = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
        bt = /"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g,
        wt = /^-ms-/,
        xt = /-([\da-z])/gi,
        kt = function(e, t) {
            return t.toUpperCase()
        }, St = function(e) {
            (X.addEventListener || "load" === e.type || "complete" === X.readyState) && (Ct(), dt.ready())
        }, Ct = function() {
            X.addEventListener ? (X.removeEventListener("DOMContentLoaded", St, !1), e.removeEventListener("load", St, !1)) : (X.detachEvent("onreadystatechange", St), e.detachEvent("onload", St))
        };
    dt.fn = dt.prototype = {
        jquery: it,
        constructor: dt,
        init: function(e, i, n) {
            var s, a;
            if (!e) return this;
            if ("string" == typeof e) {
                if (s = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : mt.exec(e), !s || !s[1] && i) return !i || i.jquery ? (i || n).find(e) : this.constructor(i).find(e);
                if (s[1]) {
                    if (i = i instanceof dt ? i[0] : i, dt.merge(this, dt.parseHTML(s[1], i && i.nodeType ? i.ownerDocument || i : X, !0)), ft.test(s[1]) && dt.isPlainObject(i))
                        for (s in i) dt.isFunction(this[s]) ? this[s](i[s]) : this.attr(s, i[s]);
                    return this
                }
                if (a = X.getElementById(s[2]), a && a.parentNode) {
                    if (a.id !== s[2]) return n.find(e);
                    this.length = 1, this[0] = a
                }
                return this.context = X, this.selector = e, this
            }
            return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : dt.isFunction(e) ? n.ready(e) : (e.selector !== t && (this.selector = e.selector, this.context = e.context), dt.makeArray(e, this))
        },
        selector: "",
        length: 0,
        toArray: function() {
            return at.call(this)
        },
        get: function(e) {
            return null == e ? this.toArray() : 0 > e ? this[this.length + e] : this[e]
        },
        pushStack: function(e) {
            var t = dt.merge(this.constructor(), e);
            return t.prevObject = this, t.context = this.context, t
        },
        each: function(e, t) {
            return dt.each(this, e, t)
        },
        ready: function(e) {
            return dt.ready.promise().done(e), this
        },
        slice: function() {
            return this.pushStack(at.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var t = this.length,
                i = +e + (0 > e ? t : 0);
            return this.pushStack(i >= 0 && t > i ? [this[i]] : [])
        },
        map: function(e) {
            return this.pushStack(dt.map(this, function(t, i) {
                return e.call(t, i, t)
            }))
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: st,
        sort: [].sort,
        splice: [].splice
    }, dt.fn.init.prototype = dt.fn, dt.extend = dt.fn.extend = function() {
        var e, i, n, s, a, r, o = arguments[0] || {}, l = 1,
            c = arguments.length,
            d = !1;
        for ("boolean" == typeof o && (d = o, o = arguments[1] || {}, l = 2), "object" == typeof o || dt.isFunction(o) || (o = {}), c === l && (o = this, --l); c > l; l++)
            if (null != (a = arguments[l]))
                for (s in a) e = o[s], n = a[s], o !== n && (d && n && (dt.isPlainObject(n) || (i = dt.isArray(n))) ? (i ? (i = !1, r = e && dt.isArray(e) ? e : []) : r = e && dt.isPlainObject(e) ? e : {}, o[s] = dt.extend(d, r, n)) : n !== t && (o[s] = n));
        return o
    }, dt.extend({
        expando: "jQuery" + (it + Math.random()).replace(/\D/g, ""),
        noConflict: function(t) {
            return e.$ === dt && (e.$ = Z), t && e.jQuery === dt && (e.jQuery = J), dt
        },
        isReady: !1,
        readyWait: 1,
        holdReady: function(e) {
            e ? dt.readyWait++ : dt.ready(!0)
        },
        ready: function(e) {
            if (e === !0 ? !--dt.readyWait : !dt.isReady) {
                if (!X.body) return setTimeout(dt.ready);
                dt.isReady = !0, e !== !0 && --dt.readyWait > 0 || ($.resolveWith(X, [dt]), dt.fn.trigger && dt(X).trigger("ready").off("ready"))
            }
        },
        isFunction: function(e) {
            return "function" === dt.type(e)
        },
        isArray: Array.isArray || function(e) {
            return "array" === dt.type(e)
        },
        isWindow: function(e) {
            return null != e && e == e.window
        },
        isNumeric: function(e) {
            return !isNaN(parseFloat(e)) && isFinite(e)
        },
        type: function(e) {
            return null == e ? String(e) : "object" == typeof e || "function" == typeof e ? et[ot.call(e)] || "object" : typeof e
        },
        isPlainObject: function(e) {
            var i;
            if (!e || "object" !== dt.type(e) || e.nodeType || dt.isWindow(e)) return !1;
            try {
                if (e.constructor && !lt.call(e, "constructor") && !lt.call(e.constructor.prototype, "isPrototypeOf")) return !1
            } catch (n) {
                return !1
            }
            if (dt.support.ownLast)
                for (i in e) return lt.call(e, i);
            for (i in e);
            return i === t || lt.call(e, i)
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e) return !1;
            return !0
        },
        error: function(e) {
            throw new Error(e)
        },
        parseHTML: function(e, t, i) {
            if (!e || "string" != typeof e) return null;
            "boolean" == typeof t && (i = t, t = !1), t = t || X;
            var n = ft.exec(e),
                s = !i && [];
            return n ? [t.createElement(n[1])] : (n = dt.buildFragment([e], t, s), s && dt(s).remove(), dt.merge([], n.childNodes))
        },
        parseJSON: function(t) {
            return e.JSON && e.JSON.parse ? e.JSON.parse(t) : null === t ? t : "string" == typeof t && (t = dt.trim(t), t && gt.test(t.replace(yt, "@").replace(bt, "]").replace(vt, ""))) ? new Function("return " + t)() : void dt.error("Invalid JSON: " + t)
        },
        parseXML: function(i) {
            var n, s;
            if (!i || "string" != typeof i) return null;
            try {
                e.DOMParser ? (s = new DOMParser, n = s.parseFromString(i, "text/xml")) : (n = new ActiveXObject("Microsoft.XMLDOM"), n.async = "false", n.loadXML(i))
            } catch (a) {
                n = t
            }
            return n && n.documentElement && !n.getElementsByTagName("parsererror").length || dt.error("Invalid XML: " + i), n
        },
        noop: function() {},
        globalEval: function(t) {
            t && dt.trim(t) && (e.execScript || function(t) {
                e.eval.call(e, t)
            })(t)
        },
        camelCase: function(e) {
            return e.replace(wt, "ms-").replace(xt, kt)
        },
        nodeName: function(e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        },
        each: function(e, t, n) {
            var s, a = 0,
                r = e.length,
                o = i(e);
            if (n) {
                if (o)
                    for (; r > a && (s = t.apply(e[a], n), s !== !1); a++);
                else
                    for (a in e)
                        if (s = t.apply(e[a], n), s === !1) break
            } else if (o)
                for (; r > a && (s = t.call(e[a], a, e[a]), s !== !1); a++);
            else
                for (a in e)
                    if (s = t.call(e[a], a, e[a]), s === !1) break; return e
        },
        trim: ct && !ct.call("\ufeff\xa0") ? function(e) {
            return null == e ? "" : ct.call(e)
        } : function(e) {
            return null == e ? "" : (e + "").replace(pt, "")
        },
        makeArray: function(e, t) {
            var n = t || [];
            return null != e && (i(Object(e)) ? dt.merge(n, "string" == typeof e ? [e] : e) : st.call(n, e)), n
        },
        inArray: function(e, t, i) {
            var n;
            if (t) {
                if (rt) return rt.call(t, e, i);
                for (n = t.length, i = i ? 0 > i ? Math.max(0, n + i) : i : 0; n > i; i++)
                    if (i in t && t[i] === e) return i
            }
            return -1
        },
        merge: function(e, i) {
            var n = i.length,
                s = e.length,
                a = 0;
            if ("number" == typeof n)
                for (; n > a; a++) e[s++] = i[a];
            else
                for (; i[a] !== t;) e[s++] = i[a++];
            return e.length = s, e
        },
        grep: function(e, t, i) {
            var n, s = [],
                a = 0,
                r = e.length;
            for (i = !! i; r > a; a++) n = !! t(e[a], a), i !== n && s.push(e[a]);
            return s
        },
        map: function(e, t, n) {
            var s, a = 0,
                r = e.length,
                o = i(e),
                l = [];
            if (o)
                for (; r > a; a++) s = t(e[a], a, n), null != s && (l[l.length] = s);
            else
                for (a in e) s = t(e[a], a, n), null != s && (l[l.length] = s);
            return nt.apply([], l)
        },
        guid: 1,
        proxy: function(e, i) {
            var n, s, a;
            return "string" == typeof i && (a = e[i], i = e, e = a), dt.isFunction(e) ? (n = at.call(arguments, 2), s = function() {
                return e.apply(i || this, n.concat(at.call(arguments)))
            }, s.guid = e.guid = e.guid || dt.guid++, s) : t
        },
        access: function(e, i, n, s, a, r, o) {
            var l = 0,
                c = e.length,
                d = null == n;
            if ("object" === dt.type(n)) {
                a = !0;
                for (l in n) dt.access(e, i, l, n[l], !0, r, o)
            } else if (s !== t && (a = !0, dt.isFunction(s) || (o = !0), d && (o ? (i.call(e, s), i = null) : (d = i, i = function(e, t, i) {
                return d.call(dt(e), i)
            })), i))
                for (; c > l; l++) i(e[l], n, o ? s : s.call(e[l], l, i(e[l], n)));
            return a ? e : d ? i.call(e) : c ? i(e[0], n) : r
        },
        now: function() {
            return (new Date).getTime()
        },
        swap: function(e, t, i, n) {
            var s, a, r = {};
            for (a in t) r[a] = e.style[a], e.style[a] = t[a];
            s = i.apply(e, n || []);
            for (a in t) e.style[a] = r[a];
            return s
        }
    }), dt.ready.promise = function(t) {
        if (!$)
            if ($ = dt.Deferred(), "complete" === X.readyState) setTimeout(dt.ready);
            else if (X.addEventListener) X.addEventListener("DOMContentLoaded", St, !1), e.addEventListener("load", St, !1);
        else {
            X.attachEvent("onreadystatechange", St), e.attachEvent("onload", St);
            var i = !1;
            try {
                i = null == e.frameElement && X.documentElement
            } catch (n) {}
            i && i.doScroll && ! function s() {
                if (!dt.isReady) {
                    try {
                        i.doScroll("left")
                    } catch (e) {
                        return setTimeout(s, 50)
                    }
                    Ct(), dt.ready()
                }
            }()
        }
        return $.promise(t)
    }, dt.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
        et["[object " + t + "]"] = t.toLowerCase()
    }), Y = dt(X),
    function(e, t) {
        function i(e, t, i, n) {
            var s, a, r, o, l, c, d, u, m, f;
            if ((t ? t.ownerDocument || t : U) !== E && D(t), t = t || E, i = i || [], !e || "string" != typeof e) return i;
            if (1 !== (o = t.nodeType) && 9 !== o) return [];
            if (R && !n) {
                if (s = bt.exec(e))
                    if (r = s[1]) {
                        if (9 === o) {
                            if (a = t.getElementById(r), !a || !a.parentNode) return i;
                            if (a.id === r) return i.push(a), i
                        } else if (t.ownerDocument && (a = t.ownerDocument.getElementById(r)) && V(t, a) && a.id === r) return i.push(a), i
                    } else {
                        if (s[2]) return et.apply(i, t.getElementsByTagName(e)), i;
                        if ((r = s[3]) && S.getElementsByClassName && t.getElementsByClassName) return et.apply(i, t.getElementsByClassName(r)), i
                    }
                if (S.qsa && (!O || !O.test(e))) {
                    if (u = d = B, m = t, f = 9 === o && e, 1 === o && "object" !== t.nodeName.toLowerCase()) {
                        for (c = h(e), (d = t.getAttribute("id")) ? u = d.replace(kt, "\\$&") : t.setAttribute("id", u), u = "[id='" + u + "'] ", l = c.length; l--;) c[l] = u + p(c[l]);
                        m = pt.test(e) && t.parentNode || t, f = c.join(",")
                    }
                    if (f) try {
                        return et.apply(i, m.querySelectorAll(f)), i
                    } catch (g) {} finally {
                        d || t.removeAttribute("id")
                    }
                }
            }
            return x(e.replace(ct, "$1"), t, i, n)
        }

        function n() {
            function e(i, n) {
                return t.push(i += " ") > P.cacheLength && delete e[t.shift()], e[i] = n
            }
            var t = [];
            return e
        }

        function s(e) {
            return e[B] = !0, e
        }

        function a(e) {
            var t = E.createElement("div");
            try {
                return !!e(t)
            } catch (i) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function r(e, t) {
            for (var i = e.split("|"), n = e.length; n--;) P.attrHandle[i[n]] = t
        }

        function o(e, t) {
            var i = t && e,
                n = i && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || K) - (~e.sourceIndex || K);
            if (n) return n;
            if (i)
                for (; i = i.nextSibling;)
                    if (i === t) return -1;
            return e ? 1 : -1
        }

        function l(e) {
            return function(t) {
                var i = t.nodeName.toLowerCase();
                return "input" === i && t.type === e
            }
        }

        function c(e) {
            return function(t) {
                var i = t.nodeName.toLowerCase();
                return ("input" === i || "button" === i) && t.type === e
            }
        }

        function d(e) {
            return s(function(t) {
                return t = +t, s(function(i, n) {
                    for (var s, a = e([], i.length, t), r = a.length; r--;) i[s = a[r]] && (i[s] = !(n[s] = i[s]))
                })
            })
        }

        function u() {}

        function h(e, t) {
            var n, s, a, r, o, l, c, d = H[e + " "];
            if (d) return t ? 0 : d.slice(0);
            for (o = e, l = [], c = P.preFilter; o;) {
                (!n || (s = ut.exec(o))) && (s && (o = o.slice(s[0].length) || o), l.push(a = [])), n = !1, (s = ht.exec(o)) && (n = s.shift(), a.push({
                    value: n,
                    type: s[0].replace(ct, " ")
                }), o = o.slice(n.length));
                for (r in P.filter)!(s = vt[r].exec(o)) || c[r] && !(s = c[r](s)) || (n = s.shift(), a.push({
                    value: n,
                    type: r,
                    matches: s
                }), o = o.slice(n.length));
                if (!n) break
            }
            return t ? o.length : o ? i.error(e) : H(e, l).slice(0)
        }

        function p(e) {
            for (var t = 0, i = e.length, n = ""; i > t; t++) n += e[t].value;
            return n
        }

        function m(e, t, i) {
            var n = t.dir,
                s = i && "parentNode" === n,
                a = j++;
            return t.first ? function(t, i, a) {
                for (; t = t[n];)
                    if (1 === t.nodeType || s) return e(t, i, a)
            } : function(t, i, r) {
                var o, l, c, d = z + " " + a;
                if (r) {
                    for (; t = t[n];)
                        if ((1 === t.nodeType || s) && e(t, i, r)) return !0
                } else
                    for (; t = t[n];)
                        if (1 === t.nodeType || s)
                            if (c = t[B] || (t[B] = {}), (l = c[n]) && l[0] === d) {
                                if ((o = l[1]) === !0 || o === C) return o === !0
                            } else if (l = c[n] = [d], l[1] = e(t, i, r) || C, l[1] === !0) return !0
            }
        }

        function f(e) {
            return e.length > 1 ? function(t, i, n) {
                for (var s = e.length; s--;)
                    if (!e[s](t, i, n)) return !1;
                return !0
            } : e[0]
        }

        function g(e, t, i, n, s) {
            for (var a, r = [], o = 0, l = e.length, c = null != t; l > o; o++)(a = e[o]) && (!i || i(a, n, s)) && (r.push(a), c && t.push(o));
            return r
        }

        function v(e, t, i, n, a, r) {
            return n && !n[B] && (n = v(n)), a && !a[B] && (a = v(a, r)), s(function(s, r, o, l) {
                var c, d, u, h = [],
                    p = [],
                    m = r.length,
                    f = s || w(t || "*", o.nodeType ? [o] : o, []),
                    v = !e || !s && t ? f : g(f, h, e, o, l),
                    y = i ? a || (s ? e : m || n) ? [] : r : v;
                if (i && i(v, y, o, l), n)
                    for (c = g(y, p), n(c, [], o, l), d = c.length; d--;)(u = c[d]) && (y[p[d]] = !(v[p[d]] = u));
                if (s) {
                    if (a || e) {
                        if (a) {
                            for (c = [], d = y.length; d--;)(u = y[d]) && c.push(v[d] = u);
                            a(null, y = [], c, l)
                        }
                        for (d = y.length; d--;)(u = y[d]) && (c = a ? it.call(s, u) : h[d]) > -1 && (s[c] = !(r[c] = u))
                    }
                } else y = g(y === r ? y.splice(m, y.length) : y), a ? a(null, r, y, l) : et.apply(r, y)
            })
        }

        function y(e) {
            for (var t, i, n, s = e.length, a = P.relative[e[0].type], r = a || P.relative[" "], o = a ? 1 : 0, l = m(function(e) {
                    return e === t
                }, r, !0), c = m(function(e) {
                    return it.call(t, e) > -1
                }, r, !0), d = [
                    function(e, i, n) {
                        return !a && (n || i !== I) || ((t = i).nodeType ? l(e, i, n) : c(e, i, n))
                    }
                ]; s > o; o++)
                if (i = P.relative[e[o].type]) d = [m(f(d), i)];
                else {
                    if (i = P.filter[e[o].type].apply(null, e[o].matches), i[B]) {
                        for (n = ++o; s > n && !P.relative[e[n].type]; n++);
                        return v(o > 1 && f(d), o > 1 && p(e.slice(0, o - 1).concat({
                            value: " " === e[o - 2].type ? "*" : ""
                        })).replace(ct, "$1"), i, n > o && y(e.slice(o, n)), s > n && y(e = e.slice(n)), s > n && p(e))
                    }
                    d.push(i)
                }
            return f(d)
        }

        function b(e, t) {
            var n = 0,
                a = t.length > 0,
                r = e.length > 0,
                o = function(s, o, l, c, d) {
                    var u, h, p, m = [],
                        f = 0,
                        v = "0",
                        y = s && [],
                        b = null != d,
                        w = I,
                        x = s || r && P.find.TAG("*", d && o.parentNode || o),
                        k = z += null == w ? 1 : Math.random() || .1;
                    for (b && (I = o !== E && o, C = n); null != (u = x[v]); v++) {
                        if (r && u) {
                            for (h = 0; p = e[h++];)
                                if (p(u, o, l)) {
                                    c.push(u);
                                    break
                                }
                            b && (z = k, C = ++n)
                        }
                        a && ((u = !p && u) && f--, s && y.push(u))
                    }
                    if (f += v, a && v !== f) {
                        for (h = 0; p = t[h++];) p(y, m, o, l);
                        if (s) {
                            if (f > 0)
                                for (; v--;) y[v] || m[v] || (m[v] = J.call(c));
                            m = g(m)
                        }
                        et.apply(c, m), b && !s && m.length > 0 && f + t.length > 1 && i.uniqueSort(c)
                    }
                    return b && (z = k, I = w), y
                };
            return a ? s(o) : o
        }

        function w(e, t, n) {
            for (var s = 0, a = t.length; a > s; s++) i(e, t[s], n);
            return n
        }

        function x(e, t, i, n) {
            var s, a, r, o, l, c = h(e);
            if (!n && 1 === c.length) {
                if (a = c[0] = c[0].slice(0), a.length > 2 && "ID" === (r = a[0]).type && S.getById && 9 === t.nodeType && R && P.relative[a[1].type]) {
                    if (t = (P.find.ID(r.matches[0].replace(St, Ct), t) || [])[0], !t) return i;
                    e = e.slice(a.shift().value.length)
                }
                for (s = vt.needsContext.test(e) ? 0 : a.length; s-- && (r = a[s], !P.relative[o = r.type]);)
                    if ((l = P.find[o]) && (n = l(r.matches[0].replace(St, Ct), pt.test(a[0].type) && t.parentNode || t))) {
                        if (a.splice(s, 1), e = n.length && p(a), !e) return et.apply(i, n), i;
                        break
                    }
            }
            return T(e, c)(n, t, !R, i, pt.test(e)), i
        }
        var k, S, C, P, A, _, T, I, M, D, E, L, R, O, N, F, V, B = "sizzle" + -new Date,
            U = e.document,
            z = 0,
            j = 0,
            q = n(),
            H = n(),
            W = n(),
            $ = !1,
            Y = function(e, t) {
                return e === t ? ($ = !0, 0) : 0
            }, Q = typeof t,
            K = 1 << 31,
            X = {}.hasOwnProperty,
            G = [],
            J = G.pop,
            Z = G.push,
            et = G.push,
            tt = G.slice,
            it = G.indexOf || function(e) {
                for (var t = 0, i = this.length; i > t; t++)
                    if (this[t] === e) return t;
                return -1
            }, nt = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            st = "[\\x20\\t\\r\\n\\f]",
            at = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            rt = at.replace("w", "w#"),
            ot = "\\[" + st + "*(" + at + ")" + st + "*(?:([*^$|!~]?=)" + st + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + rt + ")|)|)" + st + "*\\]",
            lt = ":(" + at + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + ot.replace(3, 8) + ")*)|.*)\\)|)",
            ct = new RegExp("^" + st + "+|((?:^|[^\\\\])(?:\\\\.)*)" + st + "+$", "g"),
            ut = new RegExp("^" + st + "*," + st + "*"),
            ht = new RegExp("^" + st + "*([>+~]|" + st + ")" + st + "*"),
            pt = new RegExp(st + "*[+~]"),
            mt = new RegExp("=" + st + "*([^\\]'\"]*)" + st + "*\\]", "g"),
            ft = new RegExp(lt),
            gt = new RegExp("^" + rt + "$"),
            vt = {
                ID: new RegExp("^#(" + at + ")"),
                CLASS: new RegExp("^\\.(" + at + ")"),
                TAG: new RegExp("^(" + at.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + ot),
                PSEUDO: new RegExp("^" + lt),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + st + "*(even|odd|(([+-]|)(\\d*)n|)" + st + "*(?:([+-]|)" + st + "*(\\d+)|))" + st + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + nt + ")$", "i"),
                needsContext: new RegExp("^" + st + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + st + "*((?:-\\d)?\\d*)" + st + "*\\)|)(?=[^-]|$)", "i")
            }, yt = /^[^{]+\{\s*\[native \w/,
            bt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            wt = /^(?:input|select|textarea|button)$/i,
            xt = /^h\d$/i,
            kt = /'|\\/g,
            St = new RegExp("\\\\([\\da-f]{1,6}" + st + "?|(" + st + ")|.)", "ig"),
            Ct = function(e, t, i) {
                var n = "0x" + t - 65536;
                return n !== n || i ? t : 0 > n ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320)
            };
        try {
            et.apply(G = tt.call(U.childNodes), U.childNodes), G[U.childNodes.length].nodeType
        } catch (Pt) {
            et = {
                apply: G.length ? function(e, t) {
                    Z.apply(e, tt.call(t))
                } : function(e, t) {
                    for (var i = e.length, n = 0; e[i++] = t[n++];);
                    e.length = i - 1
                }
            }
        }
        _ = i.isXML = function(e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return t ? "HTML" !== t.nodeName : !1
        }, S = i.support = {}, D = i.setDocument = function(e) {
            var t = e ? e.ownerDocument || e : U,
                i = t.defaultView;
            return t !== E && 9 === t.nodeType && t.documentElement ? (E = t, L = t.documentElement, R = !_(t), i && i.attachEvent && i !== i.top && i.attachEvent("onbeforeunload", function() {
                D()
            }), S.attributes = a(function(e) {
                return e.className = "i", !e.getAttribute("className")
            }), S.getElementsByTagName = a(function(e) {
                return e.appendChild(t.createComment("")), !e.getElementsByTagName("*").length
            }), S.getElementsByClassName = a(function(e) {
                return e.innerHTML = "<div class='a'></div><div class='a i'></div>", e.firstChild.className = "i", 2 === e.getElementsByClassName("i").length
            }), S.getById = a(function(e) {
                return L.appendChild(e).id = B, !t.getElementsByName || !t.getElementsByName(B).length
            }), S.getById ? (P.find.ID = function(e, t) {
                if (typeof t.getElementById !== Q && R) {
                    var i = t.getElementById(e);
                    return i && i.parentNode ? [i] : []
                }
            }, P.filter.ID = function(e) {
                var t = e.replace(St, Ct);
                return function(e) {
                    return e.getAttribute("id") === t
                }
            }) : (delete P.find.ID, P.filter.ID = function(e) {
                var t = e.replace(St, Ct);
                return function(e) {
                    var i = typeof e.getAttributeNode !== Q && e.getAttributeNode("id");
                    return i && i.value === t
                }
            }), P.find.TAG = S.getElementsByTagName ? function(e, t) {
                return typeof t.getElementsByTagName !== Q ? t.getElementsByTagName(e) : void 0
            } : function(e, t) {
                var i, n = [],
                    s = 0,
                    a = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; i = a[s++];) 1 === i.nodeType && n.push(i);
                    return n
                }
                return a
            }, P.find.CLASS = S.getElementsByClassName && function(e, t) {
                return typeof t.getElementsByClassName !== Q && R ? t.getElementsByClassName(e) : void 0
            }, N = [], O = [], (S.qsa = yt.test(t.querySelectorAll)) && (a(function(e) {
                e.innerHTML = "<select><option selected=''></option></select>", e.querySelectorAll("[selected]").length || O.push("\\[" + st + "*(?:value|" + nt + ")"), e.querySelectorAll(":checked").length || O.push(":checked")
            }), a(function(e) {
                var i = t.createElement("input");
                i.setAttribute("type", "hidden"), e.appendChild(i).setAttribute("t", ""), e.querySelectorAll("[t^='']").length && O.push("[*^$]=" + st + "*(?:''|\"\")"), e.querySelectorAll(":enabled").length || O.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), O.push(",.*:")
            })), (S.matchesSelector = yt.test(F = L.webkitMatchesSelector || L.mozMatchesSelector || L.oMatchesSelector || L.msMatchesSelector)) && a(function(e) {
                S.disconnectedMatch = F.call(e, "div"), F.call(e, "[s!='']:x"), N.push("!=", lt)
            }), O = O.length && new RegExp(O.join("|")), N = N.length && new RegExp(N.join("|")), V = yt.test(L.contains) || L.compareDocumentPosition ? function(e, t) {
                var i = 9 === e.nodeType ? e.documentElement : e,
                    n = t && t.parentNode;
                return e === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(n)))
            } : function(e, t) {
                if (t)
                    for (; t = t.parentNode;)
                        if (t === e) return !0;
                return !1
            }, Y = L.compareDocumentPosition ? function(e, i) {
                if (e === i) return $ = !0, 0;
                var n = i.compareDocumentPosition && e.compareDocumentPosition && e.compareDocumentPosition(i);
                return n ? 1 & n || !S.sortDetached && i.compareDocumentPosition(e) === n ? e === t || V(U, e) ? -1 : i === t || V(U, i) ? 1 : M ? it.call(M, e) - it.call(M, i) : 0 : 4 & n ? -1 : 1 : e.compareDocumentPosition ? -1 : 1
            } : function(e, i) {
                var n, s = 0,
                    a = e.parentNode,
                    r = i.parentNode,
                    l = [e],
                    c = [i];
                if (e === i) return $ = !0, 0;
                if (!a || !r) return e === t ? -1 : i === t ? 1 : a ? -1 : r ? 1 : M ? it.call(M, e) - it.call(M, i) : 0;
                if (a === r) return o(e, i);
                for (n = e; n = n.parentNode;) l.unshift(n);
                for (n = i; n = n.parentNode;) c.unshift(n);
                for (; l[s] === c[s];) s++;
                return s ? o(l[s], c[s]) : l[s] === U ? -1 : c[s] === U ? 1 : 0
            }, t) : E
        }, i.matches = function(e, t) {
            return i(e, null, null, t)
        }, i.matchesSelector = function(e, t) {
            if ((e.ownerDocument || e) !== E && D(e), t = t.replace(mt, "='$1']"), !(!S.matchesSelector || !R || N && N.test(t) || O && O.test(t))) try {
                var n = F.call(e, t);
                if (n || S.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
            } catch (s) {}
            return i(t, E, null, [e]).length > 0
        }, i.contains = function(e, t) {
            return (e.ownerDocument || e) !== E && D(e), V(e, t)
        }, i.attr = function(e, i) {
            (e.ownerDocument || e) !== E && D(e);
            var n = P.attrHandle[i.toLowerCase()],
                s = n && X.call(P.attrHandle, i.toLowerCase()) ? n(e, i, !R) : t;
            return s === t ? S.attributes || !R ? e.getAttribute(i) : (s = e.getAttributeNode(i)) && s.specified ? s.value : null : s
        }, i.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, i.uniqueSort = function(e) {
            var t, i = [],
                n = 0,
                s = 0;
            if ($ = !S.detectDuplicates, M = !S.sortStable && e.slice(0), e.sort(Y), $) {
                for (; t = e[s++];) t === e[s] && (n = i.push(s));
                for (; n--;) e.splice(i[n], 1)
            }
            return e
        }, A = i.getText = function(e) {
            var t, i = "",
                n = 0,
                s = e.nodeType;
            if (s) {
                if (1 === s || 9 === s || 11 === s) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) i += A(e)
                } else if (3 === s || 4 === s) return e.nodeValue
            } else
                for (; t = e[n]; n++) i += A(t);
            return i
        }, P = i.selectors = {
            cacheLength: 50,
            createPseudo: s,
            match: vt,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(e) {
                    return e[1] = e[1].replace(St, Ct), e[3] = (e[4] || e[5] || "").replace(St, Ct), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                },
                CHILD: function(e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || i.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && i.error(e[0]), e
                },
                PSEUDO: function(e) {
                    var i, n = !e[5] && e[2];
                    return vt.CHILD.test(e[0]) ? null : (e[3] && e[4] !== t ? e[2] = e[4] : n && ft.test(n) && (i = h(n, !0)) && (i = n.indexOf(")", n.length - i) - n.length) && (e[0] = e[0].slice(0, i), e[2] = n.slice(0, i)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function(e) {
                    var t = e.replace(St, Ct).toLowerCase();
                    return "*" === e ? function() {
                        return !0
                    } : function(e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                },
                CLASS: function(e) {
                    var t = q[e + " "];
                    return t || (t = new RegExp("(^|" + st + ")" + e + "(" + st + "|$)")) && q(e, function(e) {
                        return t.test("string" == typeof e.className && e.className || typeof e.getAttribute !== Q && e.getAttribute("class") || "")
                    })
                },
                ATTR: function(e, t, n) {
                    return function(s) {
                        var a = i.attr(s, e);
                        return null == a ? "!=" === t : t ? (a += "", "=" === t ? a === n : "!=" === t ? a !== n : "^=" === t ? n && 0 === a.indexOf(n) : "*=" === t ? n && a.indexOf(n) > -1 : "$=" === t ? n && a.slice(-n.length) === n : "~=" === t ? (" " + a + " ").indexOf(n) > -1 : "|=" === t ? a === n || a.slice(0, n.length + 1) === n + "-" : !1) : !0
                    }
                },
                CHILD: function(e, t, i, n, s) {
                    var a = "nth" !== e.slice(0, 3),
                        r = "last" !== e.slice(-4),
                        o = "of-type" === t;
                    return 1 === n && 0 === s ? function(e) {
                        return !!e.parentNode
                    } : function(t, i, l) {
                        var c, d, u, h, p, m, f = a !== r ? "nextSibling" : "previousSibling",
                            g = t.parentNode,
                            v = o && t.nodeName.toLowerCase(),
                            y = !l && !o;
                        if (g) {
                            if (a) {
                                for (; f;) {
                                    for (u = t; u = u[f];)
                                        if (o ? u.nodeName.toLowerCase() === v : 1 === u.nodeType) return !1;
                                    m = f = "only" === e && !m && "nextSibling"
                                }
                                return !0
                            }
                            if (m = [r ? g.firstChild : g.lastChild], r && y) {
                                for (d = g[B] || (g[B] = {}), c = d[e] || [], p = c[0] === z && c[1], h = c[0] === z && c[2], u = p && g.childNodes[p]; u = ++p && u && u[f] || (h = p = 0) || m.pop();)
                                    if (1 === u.nodeType && ++h && u === t) {
                                        d[e] = [z, p, h];
                                        break
                                    }
                            } else if (y && (c = (t[B] || (t[B] = {}))[e]) && c[0] === z) h = c[1];
                            else
                                for (;
                                    (u = ++p && u && u[f] || (h = p = 0) || m.pop()) && ((o ? u.nodeName.toLowerCase() !== v : 1 !== u.nodeType) || !++h || (y && ((u[B] || (u[B] = {}))[e] = [z, h]), u !== t)););
                            return h -= s, h === n || h % n === 0 && h / n >= 0
                        }
                    }
                },
                PSEUDO: function(e, t) {
                    var n, a = P.pseudos[e] || P.setFilters[e.toLowerCase()] || i.error("unsupported pseudo: " + e);
                    return a[B] ? a(t) : a.length > 1 ? (n = [e, e, "", t], P.setFilters.hasOwnProperty(e.toLowerCase()) ? s(function(e, i) {
                        for (var n, s = a(e, t), r = s.length; r--;) n = it.call(e, s[r]), e[n] = !(i[n] = s[r])
                    }) : function(e) {
                        return a(e, 0, n)
                    }) : a
                }
            },
            pseudos: {
                not: s(function(e) {
                    var t = [],
                        i = [],
                        n = T(e.replace(ct, "$1"));
                    return n[B] ? s(function(e, t, i, s) {
                        for (var a, r = n(e, null, s, []), o = e.length; o--;)(a = r[o]) && (e[o] = !(t[o] = a))
                    }) : function(e, s, a) {
                        return t[0] = e, n(t, null, a, i), !i.pop()
                    }
                }),
                has: s(function(e) {
                    return function(t) {
                        return i(e, t).length > 0
                    }
                }),
                contains: s(function(e) {
                    return function(t) {
                        return (t.textContent || t.innerText || A(t)).indexOf(e) > -1
                    }
                }),
                lang: s(function(e) {
                    return gt.test(e || "") || i.error("unsupported lang: " + e), e = e.replace(St, Ct).toLowerCase(),
                    function(t) {
                        var i;
                        do
                            if (i = R ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return i = i.toLowerCase(), i === e || 0 === i.indexOf(e + "-"); while ((t = t.parentNode) && 1 === t.nodeType);
                        return !1
                    }
                }),
                target: function(t) {
                    var i = e.location && e.location.hash;
                    return i && i.slice(1) === t.id
                },
                root: function(e) {
                    return e === L
                },
                focus: function(e) {
                    return e === E.activeElement && (!E.hasFocus || E.hasFocus()) && !! (e.type || e.href || ~e.tabIndex)
                },
                enabled: function(e) {
                    return e.disabled === !1
                },
                disabled: function(e) {
                    return e.disabled === !0
                },
                checked: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !! e.checked || "option" === t && !! e.selected
                },
                selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                },
                empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling)
                        if (e.nodeName > "@" || 3 === e.nodeType || 4 === e.nodeType) return !1;
                    return !0
                },
                parent: function(e) {
                    return !P.pseudos.empty(e)
                },
                header: function(e) {
                    return xt.test(e.nodeName)
                },
                input: function(e) {
                    return wt.test(e.nodeName)
                },
                button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                },
                text: function(e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || t.toLowerCase() === e.type)
                },
                first: d(function() {
                    return [0]
                }),
                last: d(function(e, t) {
                    return [t - 1]
                }),
                eq: d(function(e, t, i) {
                    return [0 > i ? i + t : i]
                }),
                even: d(function(e, t) {
                    for (var i = 0; t > i; i += 2) e.push(i);
                    return e
                }),
                odd: d(function(e, t) {
                    for (var i = 1; t > i; i += 2) e.push(i);
                    return e
                }),
                lt: d(function(e, t, i) {
                    for (var n = 0 > i ? i + t : i; --n >= 0;) e.push(n);
                    return e
                }),
                gt: d(function(e, t, i) {
                    for (var n = 0 > i ? i + t : i; ++n < t;) e.push(n);
                    return e
                })
            }
        }, P.pseudos.nth = P.pseudos.eq;
        for (k in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        }) P.pseudos[k] = l(k);
        for (k in {
            submit: !0,
            reset: !0
        }) P.pseudos[k] = c(k);
        u.prototype = P.filters = P.pseudos, P.setFilters = new u, T = i.compile = function(e, t) {
            var i, n = [],
                s = [],
                a = W[e + " "];
            if (!a) {
                for (t || (t = h(e)), i = t.length; i--;) a = y(t[i]), a[B] ? n.push(a) : s.push(a);
                a = W(e, b(s, n))
            }
            return a
        }, S.sortStable = B.split("").sort(Y).join("") === B, S.detectDuplicates = $, D(), S.sortDetached = a(function(e) {
            return 1 & e.compareDocumentPosition(E.createElement("div"))
        }), a(function(e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || r("type|href|height|width", function(e, t, i) {
            return i ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), S.attributes && a(function(e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || r("value", function(e, t, i) {
            return i || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
        }), a(function(e) {
            return null == e.getAttribute("disabled")
        }) || r(nt, function(e, t, i) {
            var n;
            return i ? void 0 : (n = e.getAttributeNode(t)) && n.specified ? n.value : e[t] === !0 ? t.toLowerCase() : null
        }), dt.find = i, dt.expr = i.selectors, dt.expr[":"] = dt.expr.pseudos, dt.unique = i.uniqueSort, dt.text = i.getText, dt.isXMLDoc = i.isXML, dt.contains = i.contains
    }(e);
    var Pt = {};
    dt.Callbacks = function(e) {
        e = "string" == typeof e ? Pt[e] || n(e) : dt.extend({}, e);
        var i, s, a, r, o, l, c = [],
            d = !e.once && [],
            u = function(t) {
                for (s = e.memory && t, a = !0, o = l || 0, l = 0, r = c.length, i = !0; c && r > o; o++)
                    if (c[o].apply(t[0], t[1]) === !1 && e.stopOnFalse) {
                        s = !1;
                        break
                    }
                i = !1, c && (d ? d.length && u(d.shift()) : s ? c = [] : h.disable())
            }, h = {
                add: function() {
                    if (c) {
                        var t = c.length;
                        ! function n(t) {
                            dt.each(t, function(t, i) {
                                var s = dt.type(i);
                                "function" === s ? e.unique && h.has(i) || c.push(i) : i && i.length && "string" !== s && n(i)
                            })
                        }(arguments), i ? r = c.length : s && (l = t, u(s))
                    }
                    return this
                },
                remove: function() {
                    return c && dt.each(arguments, function(e, t) {
                        for (var n;
                            (n = dt.inArray(t, c, n)) > -1;) c.splice(n, 1), i && (r >= n && r--, o >= n && o--)
                    }), this
                },
                has: function(e) {
                    return e ? dt.inArray(e, c) > -1 : !(!c || !c.length)
                },
                empty: function() {
                    return c = [], r = 0, this
                },
                disable: function() {
                    return c = d = s = t, this
                },
                disabled: function() {
                    return !c
                },
                lock: function() {
                    return d = t, s || h.disable(), this
                },
                locked: function() {
                    return !d
                },
                fireWith: function(e, t) {
                    return !c || a && !d || (t = t || [], t = [e, t.slice ? t.slice() : t], i ? d.push(t) : u(t)), this
                },
                fire: function() {
                    return h.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!a
                }
            };
        return h
    }, dt.extend({
        Deferred: function(e) {
            var t = [
                ["resolve", "done", dt.Callbacks("once memory"), "resolved"],
                ["reject", "fail", dt.Callbacks("once memory"), "rejected"],
                ["notify", "progress", dt.Callbacks("memory")]
            ],
                i = "pending",
                n = {
                    state: function() {
                        return i
                    },
                    always: function() {
                        return s.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var e = arguments;
                        return dt.Deferred(function(i) {
                            dt.each(t, function(t, a) {
                                var r = a[0],
                                    o = dt.isFunction(e[t]) && e[t];
                                s[a[1]](function() {
                                    var e = o && o.apply(this, arguments);
                                    e && dt.isFunction(e.promise) ? e.promise().done(i.resolve).fail(i.reject).progress(i.notify) : i[r + "With"](this === n ? i.promise() : this, o ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    },
                    promise: function(e) {
                        return null != e ? dt.extend(e, n) : n
                    }
                }, s = {};
            return n.pipe = n.then, dt.each(t, function(e, a) {
                var r = a[2],
                    o = a[3];
                n[a[1]] = r.add, o && r.add(function() {
                    i = o
                }, t[1 ^ e][2].disable, t[2][2].lock), s[a[0]] = function() {
                    return s[a[0] + "With"](this === s ? n : this, arguments), this
                }, s[a[0] + "With"] = r.fireWith
            }), n.promise(s), e && e.call(s, s), s
        },
        when: function(e) {
            var t, i, n, s = 0,
                a = at.call(arguments),
                r = a.length,
                o = 1 !== r || e && dt.isFunction(e.promise) ? r : 0,
                l = 1 === o ? e : dt.Deferred(),
                c = function(e, i, n) {
                    return function(s) {
                        i[e] = this, n[e] = arguments.length > 1 ? at.call(arguments) : s, n === t ? l.notifyWith(i, n) : --o || l.resolveWith(i, n)
                    }
                };
            if (r > 1)
                for (t = new Array(r), i = new Array(r), n = new Array(r); r > s; s++) a[s] && dt.isFunction(a[s].promise) ? a[s].promise().done(c(s, n, a)).fail(l.reject).progress(c(s, i, t)) : --o;
            return o || l.resolveWith(n, a), l.promise()
        }
    }), dt.support = function(t) {
        var i, n, s, a, r, o, l, c, d, u = X.createElement("div");
        if (u.setAttribute("className", "t"), u.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", i = u.getElementsByTagName("*") || [], n = u.getElementsByTagName("a")[0], !n || !n.style || !i.length) return t;
        a = X.createElement("select"), o = a.appendChild(X.createElement("option")), s = u.getElementsByTagName("input")[0], n.style.cssText = "top:1px;float:left;opacity:.5", t.getSetAttribute = "t" !== u.className, t.leadingWhitespace = 3 === u.firstChild.nodeType, t.tbody = !u.getElementsByTagName("tbody").length, t.htmlSerialize = !! u.getElementsByTagName("link").length, t.style = /top/.test(n.getAttribute("style")), t.hrefNormalized = "/a" === n.getAttribute("href"), t.opacity = /^0.5/.test(n.style.opacity), t.cssFloat = !! n.style.cssFloat, t.checkOn = !! s.value, t.optSelected = o.selected, t.enctype = !! X.createElement("form").enctype, t.html5Clone = "<:nav></:nav>" !== X.createElement("nav").cloneNode(!0).outerHTML, t.inlineBlockNeedsLayout = !1, t.shrinkWrapBlocks = !1, t.pixelPosition = !1, t.deleteExpando = !0, t.noCloneEvent = !0, t.reliableMarginRight = !0, t.boxSizingReliable = !0, s.checked = !0, t.noCloneChecked = s.cloneNode(!0).checked, a.disabled = !0, t.optDisabled = !o.disabled;
        try {
            delete u.test
        } catch (h) {
            t.deleteExpando = !1
        }
        s = X.createElement("input"), s.setAttribute("value", ""), t.input = "" === s.getAttribute("value"), s.value = "t", s.setAttribute("type", "radio"), t.radioValue = "t" === s.value, s.setAttribute("checked", "t"), s.setAttribute("name", "t"), r = X.createDocumentFragment(), r.appendChild(s), t.appendChecked = s.checked, t.checkClone = r.cloneNode(!0).cloneNode(!0).lastChild.checked, u.attachEvent && (u.attachEvent("onclick", function() {
            t.noCloneEvent = !1
        }), u.cloneNode(!0).click());
        for (d in {
            submit: !0,
            change: !0,
            focusin: !0
        }) u.setAttribute(l = "on" + d, "t"), t[d + "Bubbles"] = l in e || u.attributes[l].expando === !1;
        u.style.backgroundClip = "content-box", u.cloneNode(!0).style.backgroundClip = "", t.clearCloneStyle = "content-box" === u.style.backgroundClip;
        for (d in dt(t)) break;
        return t.ownLast = "0" !== d, dt(function() {
            var i, n, s, a = "padding:0;margin:0;border:0;display:block;box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;",
                r = X.getElementsByTagName("body")[0];
            r && (i = X.createElement("div"), i.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px", r.appendChild(i).appendChild(u), u.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", s = u.getElementsByTagName("td"), s[0].style.cssText = "padding:0;margin:0;border:0;display:none", c = 0 === s[0].offsetHeight, s[0].style.display = "", s[1].style.display = "none", t.reliableHiddenOffsets = c && 0 === s[0].offsetHeight, u.innerHTML = "", u.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;", dt.swap(r, null != r.style.zoom ? {
                zoom: 1
            } : {}, function() {
                t.boxSizing = 4 === u.offsetWidth
            }), e.getComputedStyle && (t.pixelPosition = "1%" !== (e.getComputedStyle(u, null) || {}).top, t.boxSizingReliable = "4px" === (e.getComputedStyle(u, null) || {
                width: "4px"
            }).width, n = u.appendChild(X.createElement("div")), n.style.cssText = u.style.cssText = a, n.style.marginRight = n.style.width = "0", u.style.width = "1px", t.reliableMarginRight = !parseFloat((e.getComputedStyle(n, null) || {}).marginRight)), typeof u.style.zoom !== Q && (u.innerHTML = "", u.style.cssText = a + "width:1px;padding:1px;display:inline;zoom:1", t.inlineBlockNeedsLayout = 3 === u.offsetWidth, u.style.display = "block", u.innerHTML = "<div></div>", u.firstChild.style.width = "5px", t.shrinkWrapBlocks = 3 !== u.offsetWidth, t.inlineBlockNeedsLayout && (r.style.zoom = 1)), r.removeChild(i), i = u = s = n = null)
        }), i = a = r = o = n = s = null, t
    }({});
    var At = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
        _t = /([A-Z])/g;
    dt.extend({
        cache: {},
        noData: {
            applet: !0,
            embed: !0,
            object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
        },
        hasData: function(e) {
            return e = e.nodeType ? dt.cache[e[dt.expando]] : e[dt.expando], !! e && !o(e)
        },
        data: function(e, t, i) {
            return s(e, t, i)
        },
        removeData: function(e, t) {
            return a(e, t)
        },
        _data: function(e, t, i) {
            return s(e, t, i, !0)
        },
        _removeData: function(e, t) {
            return a(e, t, !0)
        },
        acceptData: function(e) {
            if (e.nodeType && 1 !== e.nodeType && 9 !== e.nodeType) return !1;
            var t = e.nodeName && dt.noData[e.nodeName.toLowerCase()];
            return !t || t !== !0 && e.getAttribute("classid") === t
        }
    }), dt.fn.extend({
        data: function(e, i) {
            var n, s, a = null,
                o = 0,
                l = this[0];
            if (e === t) {
                if (this.length && (a = dt.data(l), 1 === l.nodeType && !dt._data(l, "parsedAttrs"))) {
                    for (n = l.attributes; o < n.length; o++) s = n[o].name, 0 === s.indexOf("data-") && (s = dt.camelCase(s.slice(5)), r(l, s, a[s]));
                    dt._data(l, "parsedAttrs", !0)
                }
                return a
            }
            return "object" == typeof e ? this.each(function() {
                dt.data(this, e)
            }) : arguments.length > 1 ? this.each(function() {
                dt.data(this, e, i)
            }) : l ? r(l, e, dt.data(l, e)) : null
        },
        removeData: function(e) {
            return this.each(function() {
                dt.removeData(this, e)
            })
        }
    }), dt.extend({
        queue: function(e, t, i) {
            var n;
            return e ? (t = (t || "fx") + "queue", n = dt._data(e, t), i && (!n || dt.isArray(i) ? n = dt._data(e, t, dt.makeArray(i)) : n.push(i)), n || []) : void 0
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var i = dt.queue(e, t),
                n = i.length,
                s = i.shift(),
                a = dt._queueHooks(e, t),
                r = function() {
                    dt.dequeue(e, t)
                };
            "inprogress" === s && (s = i.shift(), n--), s && ("fx" === t && i.unshift("inprogress"), delete a.stop, s.call(e, r, a)), !n && a && a.empty.fire()
        },
        _queueHooks: function(e, t) {
            var i = t + "queueHooks";
            return dt._data(e, i) || dt._data(e, i, {
                empty: dt.Callbacks("once memory").add(function() {
                    dt._removeData(e, t + "queue"), dt._removeData(e, i)
                })
            })
        }
    }), dt.fn.extend({
        queue: function(e, i) {
            var n = 2;
            return "string" != typeof e && (i = e, e = "fx", n--), arguments.length < n ? dt.queue(this[0], e) : i === t ? this : this.each(function() {
                var t = dt.queue(this, e, i);
                dt._queueHooks(this, e), "fx" === e && "inprogress" !== t[0] && dt.dequeue(this, e)
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                dt.dequeue(this, e)
            })
        },
        delay: function(e, t) {
            return e = dt.fx ? dt.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, i) {
                var n = setTimeout(t, e);
                i.stop = function() {
                    clearTimeout(n)
                }
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(e, i) {
            var n, s = 1,
                a = dt.Deferred(),
                r = this,
                o = this.length,
                l = function() {
                    --s || a.resolveWith(r, [r])
                };
            for ("string" != typeof e && (i = e, e = t), e = e || "fx"; o--;) n = dt._data(r[o], e + "queueHooks"), n && n.empty && (s++, n.empty.add(l));
            return l(), a.promise(i)
        }
    });
    var Tt, It, Mt = /[\t\r\n\f]/g,
        Dt = /\r/g,
        Et = /^(?:input|select|textarea|button|object)$/i,
        Lt = /^(?:a|area)$/i,
        Rt = /^(?:checked|selected)$/i,
        Ot = dt.support.getSetAttribute,
        Nt = dt.support.input;
    dt.fn.extend({
        attr: function(e, t) {
            return dt.access(this, dt.attr, e, t, arguments.length > 1)
        },
        removeAttr: function(e) {
            return this.each(function() {
                dt.removeAttr(this, e)
            })
        },
        prop: function(e, t) {
            return dt.access(this, dt.prop, e, t, arguments.length > 1)
        },
        removeProp: function(e) {
            return e = dt.propFix[e] || e, this.each(function() {
                try {
                    this[e] = t, delete this[e]
                } catch (i) {}
            })
        },
        addClass: function(e) {
            var t, i, n, s, a, r = 0,
                o = this.length,
                l = "string" == typeof e && e;
            if (dt.isFunction(e)) return this.each(function(t) {
                dt(this).addClass(e.call(this, t, this.className))
            });
            if (l)
                for (t = (e || "").match(ht) || []; o > r; r++)
                    if (i = this[r], n = 1 === i.nodeType && (i.className ? (" " + i.className + " ").replace(Mt, " ") : " ")) {
                        for (a = 0; s = t[a++];) n.indexOf(" " + s + " ") < 0 && (n += s + " ");
                        i.className = dt.trim(n)
                    }
            return this
        },
        removeClass: function(e) {
            var t, i, n, s, a, r = 0,
                o = this.length,
                l = 0 === arguments.length || "string" == typeof e && e;
            if (dt.isFunction(e)) return this.each(function(t) {
                dt(this).removeClass(e.call(this, t, this.className))
            });
            if (l)
                for (t = (e || "").match(ht) || []; o > r; r++)
                    if (i = this[r], n = 1 === i.nodeType && (i.className ? (" " + i.className + " ").replace(Mt, " ") : "")) {
                        for (a = 0; s = t[a++];)
                            for (; n.indexOf(" " + s + " ") >= 0;) n = n.replace(" " + s + " ", " ");
                        i.className = e ? dt.trim(n) : ""
                    }
            return this
        },
        toggleClass: function(e, t) {
            var i = typeof e;
            return "boolean" == typeof t && "string" === i ? t ? this.addClass(e) : this.removeClass(e) : this.each(dt.isFunction(e) ? function(i) {
                dt(this).toggleClass(e.call(this, i, this.className, t), t)
            } : function() {
                if ("string" === i)
                    for (var t, n = 0, s = dt(this), a = e.match(ht) || []; t = a[n++];) s.hasClass(t) ? s.removeClass(t) : s.addClass(t);
                else(i === Q || "boolean" === i) && (this.className && dt._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : dt._data(this, "__className__") || "")
            })
        },
        hasClass: function(e) {
            for (var t = " " + e + " ", i = 0, n = this.length; n > i; i++)
                if (1 === this[i].nodeType && (" " + this[i].className + " ").replace(Mt, " ").indexOf(t) >= 0) return !0;
            return !1
        },
        val: function(e) {
            var i, n, s, a = this[0]; {
                if (arguments.length) return s = dt.isFunction(e), this.each(function(i) {
                    var a;
                    1 === this.nodeType && (a = s ? e.call(this, i, dt(this).val()) : e, null == a ? a = "" : "number" == typeof a ? a += "" : dt.isArray(a) && (a = dt.map(a, function(e) {
                        return null == e ? "" : e + ""
                    })), n = dt.valHooks[this.type] || dt.valHooks[this.nodeName.toLowerCase()], n && "set" in n && n.set(this, a, "value") !== t || (this.value = a))
                });
                if (a) return n = dt.valHooks[a.type] || dt.valHooks[a.nodeName.toLowerCase()], n && "get" in n && (i = n.get(a, "value")) !== t ? i : (i = a.value, "string" == typeof i ? i.replace(Dt, "") : null == i ? "" : i)
            }
        }
    }), dt.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = dt.find.attr(e, "value");
                    return null != t ? t : e.text
                }
            },
            select: {
                get: function(e) {
                    for (var t, i, n = e.options, s = e.selectedIndex, a = "select-one" === e.type || 0 > s, r = a ? null : [], o = a ? s + 1 : n.length, l = 0 > s ? o : a ? s : 0; o > l; l++)
                        if (i = n[l], !(!i.selected && l !== s || (dt.support.optDisabled ? i.disabled : null !== i.getAttribute("disabled")) || i.parentNode.disabled && dt.nodeName(i.parentNode, "optgroup"))) {
                            if (t = dt(i).val(), a) return t;
                            r.push(t)
                        }
                    return r
                },
                set: function(e, t) {
                    for (var i, n, s = e.options, a = dt.makeArray(t), r = s.length; r--;) n = s[r], (n.selected = dt.inArray(dt(n).val(), a) >= 0) && (i = !0);
                    return i || (e.selectedIndex = -1), a
                }
            }
        },
        attr: function(e, i, n) {
            var s, a, r = e.nodeType;
            if (e && 3 !== r && 8 !== r && 2 !== r) return typeof e.getAttribute === Q ? dt.prop(e, i, n) : (1 === r && dt.isXMLDoc(e) || (i = i.toLowerCase(), s = dt.attrHooks[i] || (dt.expr.match.bool.test(i) ? It : Tt)), n === t ? s && "get" in s && null !== (a = s.get(e, i)) ? a : (a = dt.find.attr(e, i), null == a ? t : a) : null !== n ? s && "set" in s && (a = s.set(e, n, i)) !== t ? a : (e.setAttribute(i, n + ""), n) : void dt.removeAttr(e, i))
        },
        removeAttr: function(e, t) {
            var i, n, s = 0,
                a = t && t.match(ht);
            if (a && 1 === e.nodeType)
                for (; i = a[s++];) n = dt.propFix[i] || i, dt.expr.match.bool.test(i) ? Nt && Ot || !Rt.test(i) ? e[n] = !1 : e[dt.camelCase("default-" + i)] = e[n] = !1 : dt.attr(e, i, ""), e.removeAttribute(Ot ? i : n)
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!dt.support.radioValue && "radio" === t && dt.nodeName(e, "input")) {
                        var i = e.value;
                        return e.setAttribute("type", t), i && (e.value = i), t
                    }
                }
            }
        },
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(e, i, n) {
            var s, a, r, o = e.nodeType;
            if (e && 3 !== o && 8 !== o && 2 !== o) return r = 1 !== o || !dt.isXMLDoc(e), r && (i = dt.propFix[i] || i, a = dt.propHooks[i]), n !== t ? a && "set" in a && (s = a.set(e, n, i)) !== t ? s : e[i] = n : a && "get" in a && null !== (s = a.get(e, i)) ? s : e[i]
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var t = dt.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : Et.test(e.nodeName) || Lt.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        }
    }), It = {
        set: function(e, t, i) {
            return t === !1 ? dt.removeAttr(e, i) : Nt && Ot || !Rt.test(i) ? e.setAttribute(!Ot && dt.propFix[i] || i, i) : e[dt.camelCase("default-" + i)] = e[i] = !0, i
        }
    }, dt.each(dt.expr.match.bool.source.match(/\w+/g), function(e, i) {
        var n = dt.expr.attrHandle[i] || dt.find.attr;
        dt.expr.attrHandle[i] = Nt && Ot || !Rt.test(i) ? function(e, i, s) {
            var a = dt.expr.attrHandle[i],
                r = s ? t : (dt.expr.attrHandle[i] = t) != n(e, i, s) ? i.toLowerCase() : null;
            return dt.expr.attrHandle[i] = a, r
        } : function(e, i, n) {
            return n ? t : e[dt.camelCase("default-" + i)] ? i.toLowerCase() : null
        }
    }), Nt && Ot || (dt.attrHooks.value = {
        set: function(e, t, i) {
            return dt.nodeName(e, "input") ? void(e.defaultValue = t) : Tt && Tt.set(e, t, i)
        }
    }), Ot || (Tt = {
        set: function(e, i, n) {
            var s = e.getAttributeNode(n);
            return s || e.setAttributeNode(s = e.ownerDocument.createAttribute(n)), s.value = i += "", "value" === n || i === e.getAttribute(n) ? i : t
        }
    }, dt.expr.attrHandle.id = dt.expr.attrHandle.name = dt.expr.attrHandle.coords = function(e, i, n) {
        var s;
        return n ? t : (s = e.getAttributeNode(i)) && "" !== s.value ? s.value : null
    }, dt.valHooks.button = {
        get: function(e, i) {
            var n = e.getAttributeNode(i);
            return n && n.specified ? n.value : t
        },
        set: Tt.set
    }, dt.attrHooks.contenteditable = {
        set: function(e, t, i) {
            Tt.set(e, "" === t ? !1 : t, i)
        }
    }, dt.each(["width", "height"], function(e, t) {
        dt.attrHooks[t] = {
            set: function(e, i) {
                return "" === i ? (e.setAttribute(t, "auto"), i) : void 0
            }
        }
    })), dt.support.hrefNormalized || dt.each(["href", "src"], function(e, t) {
        dt.propHooks[t] = {
            get: function(e) {
                return e.getAttribute(t, 4)
            }
        }
    }), dt.support.style || (dt.attrHooks.style = {
        get: function(e) {
            return e.style.cssText || t
        },
        set: function(e, t) {
            return e.style.cssText = t + ""
        }
    }), dt.support.optSelected || (dt.propHooks.selected = {
        get: function(e) {
            var t = e.parentNode;
            return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
        }
    }), dt.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        dt.propFix[this.toLowerCase()] = this
    }), dt.support.enctype || (dt.propFix.enctype = "encoding"), dt.each(["radio", "checkbox"], function() {
        dt.valHooks[this] = {
            set: function(e, t) {
                return dt.isArray(t) ? e.checked = dt.inArray(dt(e).val(), t) >= 0 : void 0
            }
        }, dt.support.checkOn || (dt.valHooks[this].get = function(e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    });
    var Ft = /^(?:input|select|textarea)$/i,
        Vt = /^key/,
        Bt = /^(?:mouse|contextmenu)|click/,
        Ut = /^(?:focusinfocus|focusoutblur)$/,
        zt = /^([^.]*)(?:\.(.+)|)$/;
    dt.event = {
        global: {},
        add: function(e, i, n, s, a) {
            var r, o, l, c, d, u, h, p, m, f, g, v = dt._data(e);
            if (v) {
                for (n.handler && (c = n, n = c.handler, a = c.selector), n.guid || (n.guid = dt.guid++), (o = v.events) || (o = v.events = {}), (u = v.handle) || (u = v.handle = function(e) {
                    return typeof dt === Q || e && dt.event.triggered === e.type ? t : dt.event.dispatch.apply(u.elem, arguments)
                }, u.elem = e), i = (i || "").match(ht) || [""], l = i.length; l--;) r = zt.exec(i[l]) || [], m = g = r[1], f = (r[2] || "").split(".").sort(), m && (d = dt.event.special[m] || {}, m = (a ? d.delegateType : d.bindType) || m, d = dt.event.special[m] || {}, h = dt.extend({
                    type: m,
                    origType: g,
                    data: s,
                    handler: n,
                    guid: n.guid,
                    selector: a,
                    needsContext: a && dt.expr.match.needsContext.test(a),
                    namespace: f.join(".")
                }, c), (p = o[m]) || (p = o[m] = [], p.delegateCount = 0, d.setup && d.setup.call(e, s, f, u) !== !1 || (e.addEventListener ? e.addEventListener(m, u, !1) : e.attachEvent && e.attachEvent("on" + m, u))), d.add && (d.add.call(e, h), h.handler.guid || (h.handler.guid = n.guid)), a ? p.splice(p.delegateCount++, 0, h) : p.push(h), dt.event.global[m] = !0);
                e = null
            }
        },
        remove: function(e, t, i, n, s) {
            var a, r, o, l, c, d, u, h, p, m, f, g = dt.hasData(e) && dt._data(e);
            if (g && (d = g.events)) {
                for (t = (t || "").match(ht) || [""], c = t.length; c--;)
                    if (o = zt.exec(t[c]) || [], p = f = o[1], m = (o[2] || "").split(".").sort(), p) {
                        for (u = dt.event.special[p] || {}, p = (n ? u.delegateType : u.bindType) || p, h = d[p] || [], o = o[2] && new RegExp("(^|\\.)" + m.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = a = h.length; a--;) r = h[a], !s && f !== r.origType || i && i.guid !== r.guid || o && !o.test(r.namespace) || n && n !== r.selector && ("**" !== n || !r.selector) || (h.splice(a, 1), r.selector && h.delegateCount--, u.remove && u.remove.call(e, r));
                        l && !h.length && (u.teardown && u.teardown.call(e, m, g.handle) !== !1 || dt.removeEvent(e, p, g.handle), delete d[p])
                    } else
                        for (p in d) dt.event.remove(e, p + t[c], i, n, !0);
                dt.isEmptyObject(d) && (delete g.handle, dt._removeData(e, "events"))
            }
        },
        trigger: function(i, n, s, a) {
            var r, o, l, c, d, u, h, p = [s || X],
                m = lt.call(i, "type") ? i.type : i,
                f = lt.call(i, "namespace") ? i.namespace.split(".") : [];
            if (l = u = s = s || X, 3 !== s.nodeType && 8 !== s.nodeType && !Ut.test(m + dt.event.triggered) && (m.indexOf(".") >= 0 && (f = m.split("."), m = f.shift(), f.sort()), o = m.indexOf(":") < 0 && "on" + m, i = i[dt.expando] ? i : new dt.Event(m, "object" == typeof i && i), i.isTrigger = a ? 2 : 3, i.namespace = f.join("."), i.namespace_re = i.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, i.result = t, i.target || (i.target = s), n = null == n ? [i] : dt.makeArray(n, [i]), d = dt.event.special[m] || {}, a || !d.trigger || d.trigger.apply(s, n) !== !1)) {
                if (!a && !d.noBubble && !dt.isWindow(s)) {
                    for (c = d.delegateType || m, Ut.test(c + m) || (l = l.parentNode); l; l = l.parentNode) p.push(l), u = l;
                    u === (s.ownerDocument || X) && p.push(u.defaultView || u.parentWindow || e)
                }
                for (h = 0;
                    (l = p[h++]) && !i.isPropagationStopped();) i.type = h > 1 ? c : d.bindType || m, r = (dt._data(l, "events") || {})[i.type] && dt._data(l, "handle"), r && r.apply(l, n), r = o && l[o], r && dt.acceptData(l) && r.apply && r.apply(l, n) === !1 && i.preventDefault();
                if (i.type = m, !a && !i.isDefaultPrevented() && (!d._default || d._default.apply(p.pop(), n) === !1) && dt.acceptData(s) && o && s[m] && !dt.isWindow(s)) {
                    u = s[o], u && (s[o] = null), dt.event.triggered = m;
                    try {
                        s[m]()
                    } catch (g) {}
                    dt.event.triggered = t, u && (s[o] = u)
                }
                return i.result
            }
        },
        dispatch: function(e) {
            e = dt.event.fix(e);
            var i, n, s, a, r, o = [],
                l = at.call(arguments),
                c = (dt._data(this, "events") || {})[e.type] || [],
                d = dt.event.special[e.type] || {};
            if (l[0] = e, e.delegateTarget = this, !d.preDispatch || d.preDispatch.call(this, e) !== !1) {
                for (o = dt.event.handlers.call(this, e, c), i = 0;
                    (a = o[i++]) && !e.isPropagationStopped();)
                    for (e.currentTarget = a.elem, r = 0;
                        (s = a.handlers[r++]) && !e.isImmediatePropagationStopped();)(!e.namespace_re || e.namespace_re.test(s.namespace)) && (e.handleObj = s, e.data = s.data, n = ((dt.event.special[s.origType] || {}).handle || s.handler).apply(a.elem, l), n !== t && (e.result = n) === !1 && (e.preventDefault(), e.stopPropagation()));
                return d.postDispatch && d.postDispatch.call(this, e), e.result
            }
        },
        handlers: function(e, i) {
            var n, s, a, r, o = [],
                l = i.delegateCount,
                c = e.target;
            if (l && c.nodeType && (!e.button || "click" !== e.type))
                for (; c != this; c = c.parentNode || this)
                    if (1 === c.nodeType && (c.disabled !== !0 || "click" !== e.type)) {
                        for (a = [], r = 0; l > r; r++) s = i[r], n = s.selector + " ", a[n] === t && (a[n] = s.needsContext ? dt(n, this).index(c) >= 0 : dt.find(n, this, null, [c]).length), a[n] && a.push(s);
                        a.length && o.push({
                            elem: c,
                            handlers: a
                        })
                    }
            return l < i.length && o.push({
                elem: this,
                handlers: i.slice(l)
            }), o
        },
        fix: function(e) {
            if (e[dt.expando]) return e;
            var t, i, n, s = e.type,
                a = e,
                r = this.fixHooks[s];
            for (r || (this.fixHooks[s] = r = Bt.test(s) ? this.mouseHooks : Vt.test(s) ? this.keyHooks : {}), n = r.props ? this.props.concat(r.props) : this.props, e = new dt.Event(a), t = n.length; t--;) i = n[t], e[i] = a[i];
            return e.target || (e.target = a.srcElement || X), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !! e.metaKey, r.filter ? r.filter(e, a) : e
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(e, t) {
                return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(e, i) {
                var n, s, a, r = i.button,
                    o = i.fromElement;
                return null == e.pageX && null != i.clientX && (s = e.target.ownerDocument || X, a = s.documentElement, n = s.body, e.pageX = i.clientX + (a && a.scrollLeft || n && n.scrollLeft || 0) - (a && a.clientLeft || n && n.clientLeft || 0), e.pageY = i.clientY + (a && a.scrollTop || n && n.scrollTop || 0) - (a && a.clientTop || n && n.clientTop || 0)), !e.relatedTarget && o && (e.relatedTarget = o === e.target ? i.toElement : o), e.which || r === t || (e.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0), e
            }
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== d() && this.focus) try {
                        return this.focus(), !1
                    } catch (e) {}
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    return this === d() && this.blur ? (this.blur(), !1) : void 0
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    return dt.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
                },
                _default: function(e) {
                    return dt.nodeName(e.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(e) {
                    e.result !== t && (e.originalEvent.returnValue = e.result)
                }
            }
        },
        simulate: function(e, t, i, n) {
            var s = dt.extend(new dt.Event, i, {
                type: e,
                isSimulated: !0,
                originalEvent: {}
            });
            n ? dt.event.trigger(s, null, t) : dt.event.dispatch.call(t, s), s.isDefaultPrevented() && i.preventDefault()
        }
    }, dt.removeEvent = X.removeEventListener ? function(e, t, i) {
        e.removeEventListener && e.removeEventListener(t, i, !1)
    } : function(e, t, i) {
        var n = "on" + t;
        e.detachEvent && (typeof e[n] === Q && (e[n] = null), e.detachEvent(n, i))
    }, dt.Event = function(e, t) {
        return this instanceof dt.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || e.returnValue === !1 || e.getPreventDefault && e.getPreventDefault() ? l : c) : this.type = e, t && dt.extend(this, t), this.timeStamp = e && e.timeStamp || dt.now(), void(this[dt.expando] = !0)) : new dt.Event(e, t)
    }, dt.Event.prototype = {
        isDefaultPrevented: c,
        isPropagationStopped: c,
        isImmediatePropagationStopped: c,
        preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = l, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
        },
        stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = l, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
        },
        stopImmediatePropagation: function() {
            this.isImmediatePropagationStopped = l, this.stopPropagation()
        }
    }, dt.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout"
    }, function(e, t) {
        dt.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function(e) {
                var i, n = this,
                    s = e.relatedTarget,
                    a = e.handleObj;
                return (!s || s !== n && !dt.contains(n, s)) && (e.type = a.origType, i = a.handler.apply(this, arguments), e.type = t), i
            }
        }
    }), dt.support.submitBubbles || (dt.event.special.submit = {
        setup: function() {
            return dt.nodeName(this, "form") ? !1 : void dt.event.add(this, "click._submit keypress._submit", function(e) {
                var i = e.target,
                    n = dt.nodeName(i, "input") || dt.nodeName(i, "button") ? i.form : t;
                n && !dt._data(n, "submitBubbles") && (dt.event.add(n, "submit._submit", function(e) {
                    e._submit_bubble = !0
                }), dt._data(n, "submitBubbles", !0))
            })
        },
        postDispatch: function(e) {
            e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && dt.event.simulate("submit", this.parentNode, e, !0))
        },
        teardown: function() {
            return dt.nodeName(this, "form") ? !1 : void dt.event.remove(this, "._submit")
        }
    }), dt.support.changeBubbles || (dt.event.special.change = {
        setup: function() {
            return Ft.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (dt.event.add(this, "propertychange._change", function(e) {
                "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
            }), dt.event.add(this, "click._change", function(e) {
                this._just_changed && !e.isTrigger && (this._just_changed = !1), dt.event.simulate("change", this, e, !0)
            })), !1) : void dt.event.add(this, "beforeactivate._change", function(e) {
                var t = e.target;
                Ft.test(t.nodeName) && !dt._data(t, "changeBubbles") && (dt.event.add(t, "change._change", function(e) {
                    !this.parentNode || e.isSimulated || e.isTrigger || dt.event.simulate("change", this.parentNode, e, !0)
                }), dt._data(t, "changeBubbles", !0))
            })
        },
        handle: function(e) {
            var t = e.target;
            return this !== t || e.isSimulated || e.isTrigger || "radio" !== t.type && "checkbox" !== t.type ? e.handleObj.handler.apply(this, arguments) : void 0
        },
        teardown: function() {
            return dt.event.remove(this, "._change"), !Ft.test(this.nodeName)
        }
    }), dt.support.focusinBubbles || dt.each({
        focus: "focusin",
        blur: "focusout"
    }, function(e, t) {
        var i = 0,
            n = function(e) {
                dt.event.simulate(t, e.target, dt.event.fix(e), !0)
            };
        dt.event.special[t] = {
            setup: function() {
                0 === i++ && X.addEventListener(e, n, !0)
            },
            teardown: function() {
                0 === --i && X.removeEventListener(e, n, !0)
            }
        }
    }), dt.fn.extend({
        on: function(e, i, n, s, a) {
            var r, o;
            if ("object" == typeof e) {
                "string" != typeof i && (n = n || i, i = t);
                for (r in e) this.on(r, i, n, e[r], a);
                return this
            }
            if (null == n && null == s ? (s = i, n = i = t) : null == s && ("string" == typeof i ? (s = n, n = t) : (s = n, n = i, i = t)), s === !1) s = c;
            else if (!s) return this;
            return 1 === a && (o = s, s = function(e) {
                return dt().off(e), o.apply(this, arguments)
            }, s.guid = o.guid || (o.guid = dt.guid++)), this.each(function() {
                dt.event.add(this, e, s, n, i)
            })
        },
        one: function(e, t, i, n) {
            return this.on(e, t, i, n, 1)
        },
        off: function(e, i, n) {
            var s, a;
            if (e && e.preventDefault && e.handleObj) return s = e.handleObj, dt(e.delegateTarget).off(s.namespace ? s.origType + "." + s.namespace : s.origType, s.selector, s.handler), this;
            if ("object" == typeof e) {
                for (a in e) this.off(a, i, e[a]);
                return this
            }
            return (i === !1 || "function" == typeof i) && (n = i, i = t), n === !1 && (n = c), this.each(function() {
                dt.event.remove(this, e, n, i)
            })
        },
        trigger: function(e, t) {
            return this.each(function() {
                dt.event.trigger(e, t, this)
            })
        },
        triggerHandler: function(e, t) {
            var i = this[0];
            return i ? dt.event.trigger(e, t, i, !0) : void 0
        }
    });
    var jt = /^.[^:#\[\.,]*$/,
        qt = /^(?:parents|prev(?:Until|All))/,
        Ht = dt.expr.match.needsContext,
        Wt = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    dt.fn.extend({
        find: function(e) {
            var t, i = [],
                n = this,
                s = n.length;
            if ("string" != typeof e) return this.pushStack(dt(e).filter(function() {
                for (t = 0; s > t; t++)
                    if (dt.contains(n[t], this)) return !0
            }));
            for (t = 0; s > t; t++) dt.find(e, n[t], i);
            return i = this.pushStack(s > 1 ? dt.unique(i) : i), i.selector = this.selector ? this.selector + " " + e : e, i
        },
        has: function(e) {
            var t, i = dt(e, this),
                n = i.length;
            return this.filter(function() {
                for (t = 0; n > t; t++)
                    if (dt.contains(this, i[t])) return !0
            })
        },
        not: function(e) {
            return this.pushStack(h(this, e || [], !0))
        },
        filter: function(e) {
            return this.pushStack(h(this, e || [], !1))
        },
        is: function(e) {
            return !!h(this, "string" == typeof e && Ht.test(e) ? dt(e) : e || [], !1).length
        },
        closest: function(e, t) {
            for (var i, n = 0, s = this.length, a = [], r = Ht.test(e) || "string" != typeof e ? dt(e, t || this.context) : 0; s > n; n++)
                for (i = this[n]; i && i !== t; i = i.parentNode)
                    if (i.nodeType < 11 && (r ? r.index(i) > -1 : 1 === i.nodeType && dt.find.matchesSelector(i, e))) {
                        i = a.push(i);
                        break
                    }
            return this.pushStack(a.length > 1 ? dt.unique(a) : a)
        },
        index: function(e) {
            return e ? "string" == typeof e ? dt.inArray(this[0], dt(e)) : dt.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(e, t) {
            var i = "string" == typeof e ? dt(e, t) : dt.makeArray(e && e.nodeType ? [e] : e),
                n = dt.merge(this.get(), i);
            return this.pushStack(dt.unique(n))
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), dt.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(e) {
            return dt.dir(e, "parentNode")
        },
        parentsUntil: function(e, t, i) {
            return dt.dir(e, "parentNode", i)
        },
        next: function(e) {
            return u(e, "nextSibling")
        },
        prev: function(e) {
            return u(e, "previousSibling")
        },
        nextAll: function(e) {
            return dt.dir(e, "nextSibling")
        },
        prevAll: function(e) {
            return dt.dir(e, "previousSibling")
        },
        nextUntil: function(e, t, i) {
            return dt.dir(e, "nextSibling", i)
        },
        prevUntil: function(e, t, i) {
            return dt.dir(e, "previousSibling", i)
        },
        siblings: function(e) {
            return dt.sibling((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return dt.sibling(e.firstChild)
        },
        contents: function(e) {
            return dt.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : dt.merge([], e.childNodes)
        }
    }, function(e, t) {
        dt.fn[e] = function(i, n) {
            var s = dt.map(this, t, i);
            return "Until" !== e.slice(-5) && (n = i), n && "string" == typeof n && (s = dt.filter(n, s)), this.length > 1 && (Wt[e] || (s = dt.unique(s)), qt.test(e) && (s = s.reverse())), this.pushStack(s)
        }
    }), dt.extend({
        filter: function(e, t, i) {
            var n = t[0];
            return i && (e = ":not(" + e + ")"), 1 === t.length && 1 === n.nodeType ? dt.find.matchesSelector(n, e) ? [n] : [] : dt.find.matches(e, dt.grep(t, function(e) {
                return 1 === e.nodeType
            }))
        },
        dir: function(e, i, n) {
            for (var s = [], a = e[i]; a && 9 !== a.nodeType && (n === t || 1 !== a.nodeType || !dt(a).is(n));) 1 === a.nodeType && s.push(a), a = a[i];
            return s
        },
        sibling: function(e, t) {
            for (var i = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && i.push(e);
            return i
        }
    });
    var $t = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        Yt = / jQuery\d+="(?:null|\d+)"/g,
        Qt = new RegExp("<(?:" + $t + ")[\\s/>]", "i"),
        Kt = /^\s+/,
        Xt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        Gt = /<([\w:]+)/,
        Jt = /<tbody/i,
        Zt = /<|&#?\w+;/,
        ei = /<(?:script|style|link)/i,
        ti = /^(?:checkbox|radio)$/i,
        ii = /checked\s*(?:[^=]|=\s*.checked.)/i,
        ni = /^$|\/(?:java|ecma)script/i,
        si = /^true\/(.*)/,
        ai = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        ri = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: dt.support.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        }, oi = p(X),
        li = oi.appendChild(X.createElement("div"));
    ri.optgroup = ri.option, ri.tbody = ri.tfoot = ri.colgroup = ri.caption = ri.thead, ri.th = ri.td, dt.fn.extend({
        text: function(e) {
            return dt.access(this, function(e) {
                return e === t ? dt.text(this) : this.empty().append((this[0] && this[0].ownerDocument || X).createTextNode(e))
            }, null, e, arguments.length)
        },
        append: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = m(this, e);
                    t.appendChild(e)
                }
            })
        },
        prepend: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = m(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        },
        before: function() {
            return this.domManip(arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function() {
            return this.domManip(arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        remove: function(e, t) {
            for (var i, n = e ? dt.filter(e, this) : this, s = 0; null != (i = n[s]); s++) t || 1 !== i.nodeType || dt.cleanData(w(i)), i.parentNode && (t && dt.contains(i.ownerDocument, i) && v(w(i, "script")), i.parentNode.removeChild(i));
            return this
        },
        empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++) {
                for (1 === e.nodeType && dt.cleanData(w(e, !1)); e.firstChild;) e.removeChild(e.firstChild);
                e.options && dt.nodeName(e, "select") && (e.options.length = 0)
            }
            return this
        },
        clone: function(e, t) {
            return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function() {
                return dt.clone(this, e, t)
            })
        },
        html: function(e) {
            return dt.access(this, function(e) {
                var i = this[0] || {}, n = 0,
                    s = this.length;
                if (e === t) return 1 === i.nodeType ? i.innerHTML.replace(Yt, "") : t;
                if (!("string" != typeof e || ei.test(e) || !dt.support.htmlSerialize && Qt.test(e) || !dt.support.leadingWhitespace && Kt.test(e) || ri[(Gt.exec(e) || ["", ""])[1].toLowerCase()])) {
                    e = e.replace(Xt, "<$1></$2>");
                    try {
                        for (; s > n; n++) i = this[n] || {}, 1 === i.nodeType && (dt.cleanData(w(i, !1)), i.innerHTML = e);
                        i = 0
                    } catch (a) {}
                }
                i && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function() {
            var e = dt.map(this, function(e) {
                return [e.nextSibling, e.parentNode]
            }),
                t = 0;
            return this.domManip(arguments, function(i) {
                var n = e[t++],
                    s = e[t++];
                s && (n && n.parentNode !== s && (n = this.nextSibling), dt(this).remove(), s.insertBefore(i, n))
            }, !0), t ? this : this.remove()
        },
        detach: function(e) {
            return this.remove(e, !0)
        },
        domManip: function(e, t, i) {
            e = nt.apply([], e);
            var n, s, a, r, o, l, c = 0,
                d = this.length,
                u = this,
                h = d - 1,
                p = e[0],
                m = dt.isFunction(p);
            if (m || !(1 >= d || "string" != typeof p || dt.support.checkClone) && ii.test(p)) return this.each(function(n) {
                var s = u.eq(n);
                m && (e[0] = p.call(this, n, s.html())), s.domManip(e, t, i)
            });
            if (d && (l = dt.buildFragment(e, this[0].ownerDocument, !1, !i && this), n = l.firstChild, 1 === l.childNodes.length && (l = n), n)) {
                for (r = dt.map(w(l, "script"), f), a = r.length; d > c; c++) s = l, c !== h && (s = dt.clone(s, !0, !0), a && dt.merge(r, w(s, "script"))), t.call(this[c], s, c);
                if (a)
                    for (o = r[r.length - 1].ownerDocument, dt.map(r, g), c = 0; a > c; c++) s = r[c], ni.test(s.type || "") && !dt._data(s, "globalEval") && dt.contains(o, s) && (s.src ? dt._evalUrl(s.src) : dt.globalEval((s.text || s.textContent || s.innerHTML || "").replace(ai, "")));
                l = n = null
            }
            return this
        }
    }), dt.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, t) {
        dt.fn[e] = function(e) {
            for (var i, n = 0, s = [], a = dt(e), r = a.length - 1; r >= n; n++) i = n === r ? this : this.clone(!0), dt(a[n])[t](i), st.apply(s, i.get());
            return this.pushStack(s)
        }
    }), dt.extend({
        clone: function(e, t, i) {
            var n, s, a, r, o, l = dt.contains(e.ownerDocument, e);
            if (dt.support.html5Clone || dt.isXMLDoc(e) || !Qt.test("<" + e.nodeName + ">") ? a = e.cloneNode(!0) : (li.innerHTML = e.outerHTML, li.removeChild(a = li.firstChild)), !(dt.support.noCloneEvent && dt.support.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || dt.isXMLDoc(e)))
                for (n = w(a), o = w(e), r = 0; null != (s = o[r]); ++r) n[r] && b(s, n[r]);
            if (t)
                if (i)
                    for (o = o || w(e), n = n || w(a), r = 0; null != (s = o[r]); r++) y(s, n[r]);
                else y(e, a);
            return n = w(a, "script"), n.length > 0 && v(n, !l && w(e, "script")), n = o = s = null, a
        },
        buildFragment: function(e, t, i, n) {
            for (var s, a, r, o, l, c, d, u = e.length, h = p(t), m = [], f = 0; u > f; f++)
                if (a = e[f], a || 0 === a)
                    if ("object" === dt.type(a)) dt.merge(m, a.nodeType ? [a] : a);
                    else if (Zt.test(a)) {
                for (o = o || h.appendChild(t.createElement("div")), l = (Gt.exec(a) || ["", ""])[1].toLowerCase(), d = ri[l] || ri._default, o.innerHTML = d[1] + a.replace(Xt, "<$1></$2>") + d[2], s = d[0]; s--;) o = o.lastChild;
                if (!dt.support.leadingWhitespace && Kt.test(a) && m.push(t.createTextNode(Kt.exec(a)[0])), !dt.support.tbody)
                    for (a = "table" !== l || Jt.test(a) ? "<table>" !== d[1] || Jt.test(a) ? 0 : o : o.firstChild, s = a && a.childNodes.length; s--;) dt.nodeName(c = a.childNodes[s], "tbody") && !c.childNodes.length && a.removeChild(c);
                for (dt.merge(m, o.childNodes), o.textContent = ""; o.firstChild;) o.removeChild(o.firstChild);
                o = h.lastChild
            } else m.push(t.createTextNode(a));
            for (o && h.removeChild(o), dt.support.appendChecked || dt.grep(w(m, "input"), x), f = 0; a = m[f++];)
                if ((!n || -1 === dt.inArray(a, n)) && (r = dt.contains(a.ownerDocument, a), o = w(h.appendChild(a), "script"), r && v(o), i))
                    for (s = 0; a = o[s++];) ni.test(a.type || "") && i.push(a);
            return o = null, h
        },
        cleanData: function(e, t) {
            for (var i, n, s, a, r = 0, o = dt.expando, l = dt.cache, c = dt.support.deleteExpando, d = dt.event.special; null != (i = e[r]); r++)
                if ((t || dt.acceptData(i)) && (s = i[o], a = s && l[s])) {
                    if (a.events)
                        for (n in a.events) d[n] ? dt.event.remove(i, n) : dt.removeEvent(i, n, a.handle);
                    l[s] && (delete l[s], c ? delete i[o] : typeof i.removeAttribute !== Q ? i.removeAttribute(o) : i[o] = null, tt.push(s))
                }
        },
        _evalUrl: function(e) {
            return dt.ajax({
                url: e,
                type: "GET",
                dataType: "script",
                async: !1,
                global: !1,
                "throws": !0
            })
        }
    }), dt.fn.extend({
        wrapAll: function(e) {
            if (dt.isFunction(e)) return this.each(function(t) {
                dt(this).wrapAll(e.call(this, t))
            });
            if (this[0]) {
                var t = dt(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                    for (var e = this; e.firstChild && 1 === e.firstChild.nodeType;) e = e.firstChild;
                    return e
                }).append(this)
            }
            return this
        },
        wrapInner: function(e) {
            return this.each(dt.isFunction(e) ? function(t) {
                dt(this).wrapInner(e.call(this, t))
            } : function() {
                var t = dt(this),
                    i = t.contents();
                i.length ? i.wrapAll(e) : t.append(e)
            })
        },
        wrap: function(e) {
            var t = dt.isFunction(e);
            return this.each(function(i) {
                dt(this).wrapAll(t ? e.call(this, i) : e)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                dt.nodeName(this, "body") || dt(this).replaceWith(this.childNodes)
            }).end()
        }
    });
    var ci, di, ui, hi = /alpha\([^)]*\)/i,
        pi = /opacity\s*=\s*([^)]*)/,
        mi = /^(top|right|bottom|left)$/,
        fi = /^(none|table(?!-c[ea]).+)/,
        gi = /^margin/,
        vi = new RegExp("^(" + ut + ")(.*)$", "i"),
        yi = new RegExp("^(" + ut + ")(?!px)[a-z%]+$", "i"),
        bi = new RegExp("^([+-])=(" + ut + ")", "i"),
        wi = {
            BODY: "block"
        }, xi = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        }, ki = {
            letterSpacing: 0,
            fontWeight: 400
        }, Si = ["Top", "Right", "Bottom", "Left"],
        Ci = ["Webkit", "O", "Moz", "ms"];
    dt.fn.extend({
        css: function(e, i) {
            return dt.access(this, function(e, i, n) {
                var s, a, r = {}, o = 0;
                if (dt.isArray(i)) {
                    for (a = di(e), s = i.length; s > o; o++) r[i[o]] = dt.css(e, i[o], !1, a);
                    return r
                }
                return n !== t ? dt.style(e, i, n) : dt.css(e, i)
            }, e, i, arguments.length > 1)
        },
        show: function() {
            return C(this, !0)
        },
        hide: function() {
            return C(this)
        },
        toggle: function(e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                S(this) ? dt(this).show() : dt(this).hide()
            })
        }
    }), dt.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var i = ui(e, "opacity");
                        return "" === i ? "1" : i
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": dt.support.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(e, i, n, s) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var a, r, o, l = dt.camelCase(i),
                    c = e.style;
                if (i = dt.cssProps[l] || (dt.cssProps[l] = k(c, l)), o = dt.cssHooks[i] || dt.cssHooks[l], n === t) return o && "get" in o && (a = o.get(e, !1, s)) !== t ? a : c[i];
                if (r = typeof n, "string" === r && (a = bi.exec(n)) && (n = (a[1] + 1) * a[2] + parseFloat(dt.css(e, i)), r = "number"), !(null == n || "number" === r && isNaN(n) || ("number" !== r || dt.cssNumber[l] || (n += "px"), dt.support.clearCloneStyle || "" !== n || 0 !== i.indexOf("background") || (c[i] = "inherit"), o && "set" in o && (n = o.set(e, n, s)) === t))) try {
                    c[i] = n
                } catch (d) {}
            }
        },
        css: function(e, i, n, s) {
            var a, r, o, l = dt.camelCase(i);
            return i = dt.cssProps[l] || (dt.cssProps[l] = k(e.style, l)), o = dt.cssHooks[i] || dt.cssHooks[l], o && "get" in o && (r = o.get(e, !0, n)), r === t && (r = ui(e, i, s)), "normal" === r && i in ki && (r = ki[i]), "" === n || n ? (a = parseFloat(r), n === !0 || dt.isNumeric(a) ? a || 0 : r) : r
        }
    }), e.getComputedStyle ? (di = function(t) {
        return e.getComputedStyle(t, null)
    }, ui = function(e, i, n) {
        var s, a, r, o = n || di(e),
            l = o ? o.getPropertyValue(i) || o[i] : t,
            c = e.style;
        return o && ("" !== l || dt.contains(e.ownerDocument, e) || (l = dt.style(e, i)), yi.test(l) && gi.test(i) && (s = c.width, a = c.minWidth, r = c.maxWidth, c.minWidth = c.maxWidth = c.width = l, l = o.width, c.width = s, c.minWidth = a, c.maxWidth = r)), l
    }) : X.documentElement.currentStyle && (di = function(e) {
        return e.currentStyle
    }, ui = function(e, i, n) {
        var s, a, r, o = n || di(e),
            l = o ? o[i] : t,
            c = e.style;
        return null == l && c && c[i] && (l = c[i]), yi.test(l) && !mi.test(i) && (s = c.left, a = e.runtimeStyle, r = a && a.left, r && (a.left = e.currentStyle.left), c.left = "fontSize" === i ? "1em" : l, l = c.pixelLeft + "px", c.left = s, r && (a.left = r)), "" === l ? "auto" : l
    }), dt.each(["height", "width"], function(e, t) {
        dt.cssHooks[t] = {
            get: function(e, i, n) {
                return i ? 0 === e.offsetWidth && fi.test(dt.css(e, "display")) ? dt.swap(e, xi, function() {
                    return _(e, t, n)
                }) : _(e, t, n) : void 0
            },
            set: function(e, i, n) {
                var s = n && di(e);
                return P(e, i, n ? A(e, t, n, dt.support.boxSizing && "border-box" === dt.css(e, "boxSizing", !1, s), s) : 0)
            }
        }
    }), dt.support.opacity || (dt.cssHooks.opacity = {
        get: function(e, t) {
            return pi.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
        },
        set: function(e, t) {
            var i = e.style,
                n = e.currentStyle,
                s = dt.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                a = n && n.filter || i.filter || "";
            i.zoom = 1, (t >= 1 || "" === t) && "" === dt.trim(a.replace(hi, "")) && i.removeAttribute && (i.removeAttribute("filter"), "" === t || n && !n.filter) || (i.filter = hi.test(a) ? a.replace(hi, s) : a + " " + s)
        }
    }), dt(function() {
        dt.support.reliableMarginRight || (dt.cssHooks.marginRight = {
            get: function(e, t) {
                return t ? dt.swap(e, {
                    display: "inline-block"
                }, ui, [e, "marginRight"]) : void 0
            }
        }), !dt.support.pixelPosition && dt.fn.position && dt.each(["top", "left"], function(e, t) {
            dt.cssHooks[t] = {
                get: function(e, i) {
                    return i ? (i = ui(e, t), yi.test(i) ? dt(e).position()[t] + "px" : i) : void 0
                }
            }
        })
    }), dt.expr && dt.expr.filters && (dt.expr.filters.hidden = function(e) {
        return e.offsetWidth <= 0 && e.offsetHeight <= 0 || !dt.support.reliableHiddenOffsets && "none" === (e.style && e.style.display || dt.css(e, "display"))
    }, dt.expr.filters.visible = function(e) {
        return !dt.expr.filters.hidden(e)
    }), dt.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(e, t) {
        dt.cssHooks[e + t] = {
            expand: function(i) {
                for (var n = 0, s = {}, a = "string" == typeof i ? i.split(" ") : [i]; 4 > n; n++) s[e + Si[n] + t] = a[n] || a[n - 2] || a[0];
                return s
            }
        }, gi.test(e) || (dt.cssHooks[e + t].set = P)
    });
    var Pi = /%20/g,
        Ai = /\[\]$/,
        _i = /\r?\n/g,
        Ti = /^(?:submit|button|image|reset|file)$/i,
        Ii = /^(?:input|select|textarea|keygen)/i;
    dt.fn.extend({
        serialize: function() {
            return dt.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = dt.prop(this, "elements");
                return e ? dt.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !dt(this).is(":disabled") && Ii.test(this.nodeName) && !Ti.test(e) && (this.checked || !ti.test(e))
            }).map(function(e, t) {
                var i = dt(this).val();
                return null == i ? null : dt.isArray(i) ? dt.map(i, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(_i, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: i.replace(_i, "\r\n")
                }
            }).get()
        }
    }), dt.param = function(e, i) {
        var n, s = [],
            a = function(e, t) {
                t = dt.isFunction(t) ? t() : null == t ? "" : t, s[s.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
            };
        if (i === t && (i = dt.ajaxSettings && dt.ajaxSettings.traditional), dt.isArray(e) || e.jquery && !dt.isPlainObject(e)) dt.each(e, function() {
            a(this.name, this.value)
        });
        else
            for (n in e) M(n, e[n], i, a);
        return s.join("&").replace(Pi, "+")
    }, dt.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
        dt.fn[t] = function(e, i) {
            return arguments.length > 0 ? this.on(t, null, e, i) : this.trigger(t)
        }
    }), dt.fn.extend({
        hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        },
        bind: function(e, t, i) {
            return this.on(e, null, t, i)
        },
        unbind: function(e, t) {
            return this.off(e, null, t)
        },
        delegate: function(e, t, i, n) {
            return this.on(t, e, i, n)
        },
        undelegate: function(e, t, i) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", i)
        }
    });
    var Mi, Di, Ei = dt.now(),
        Li = /\?/,
        Ri = /#.*$/,
        Oi = /([?&])_=[^&]*/,
        Ni = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Fi = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Vi = /^(?:GET|HEAD)$/,
        Bi = /^\/\//,
        Ui = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
        zi = dt.fn.load,
        ji = {}, qi = {}, Hi = "*/".concat("*");
    try {
        Di = K.href
    } catch (Wi) {
        Di = X.createElement("a"), Di.href = "", Di = Di.href
    }
    Mi = Ui.exec(Di.toLowerCase()) || [], dt.fn.load = function(e, i, n) {
        if ("string" != typeof e && zi) return zi.apply(this, arguments);
        var s, a, r, o = this,
            l = e.indexOf(" ");
        return l >= 0 && (s = e.slice(l, e.length), e = e.slice(0, l)), dt.isFunction(i) ? (n = i, i = t) : i && "object" == typeof i && (r = "POST"), o.length > 0 && dt.ajax({
            url: e,
            type: r,
            dataType: "html",
            data: i
        }).done(function(e) {
            a = arguments, o.html(s ? dt("<div>").append(dt.parseHTML(e)).find(s) : e)
        }).complete(n && function(e, t) {
            o.each(n, a || [e.responseText, t, e])
        }), this
    }, dt.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        dt.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), dt.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Di,
            type: "GET",
            isLocal: Fi.test(Mi[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Hi,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": dt.parseJSON,
                "text xml": dt.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? L(L(e, dt.ajaxSettings), t) : L(dt.ajaxSettings, e)
        },
        ajaxPrefilter: D(ji),
        ajaxTransport: D(qi),
        ajax: function(e, i) {
            function n(e, i, n, s) {
                var a, u, y, b, x, S = i;
                2 !== w && (w = 2, l && clearTimeout(l), d = t, o = s || "", k.readyState = e > 0 ? 4 : 0, a = e >= 200 && 300 > e || 304 === e, n && (b = R(h, k, n)), b = O(h, b, k, a), a ? (h.ifModified && (x = k.getResponseHeader("Last-Modified"), x && (dt.lastModified[r] = x), x = k.getResponseHeader("etag"), x && (dt.etag[r] = x)), 204 === e || "HEAD" === h.type ? S = "nocontent" : 304 === e ? S = "notmodified" : (S = b.state, u = b.data, y = b.error, a = !y)) : (y = S, (e || !S) && (S = "error", 0 > e && (e = 0))), k.status = e, k.statusText = (i || S) + "", a ? f.resolveWith(p, [u, S, k]) : f.rejectWith(p, [k, S, y]), k.statusCode(v), v = t, c && m.trigger(a ? "ajaxSuccess" : "ajaxError", [k, h, a ? u : y]), g.fireWith(p, [k, S]), c && (m.trigger("ajaxComplete", [k, h]), --dt.active || dt.event.trigger("ajaxStop")))
            }
            "object" == typeof e && (i = e, e = t), i = i || {};
            var s, a, r, o, l, c, d, u, h = dt.ajaxSetup({}, i),
                p = h.context || h,
                m = h.context && (p.nodeType || p.jquery) ? dt(p) : dt.event,
                f = dt.Deferred(),
                g = dt.Callbacks("once memory"),
                v = h.statusCode || {}, y = {}, b = {}, w = 0,
                x = "canceled",
                k = {
                    readyState: 0,
                    getResponseHeader: function(e) {
                        var t;
                        if (2 === w) {
                            if (!u)
                                for (u = {}; t = Ni.exec(o);) u[t[1].toLowerCase()] = t[2];
                            t = u[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function() {
                        return 2 === w ? o : null
                    },
                    setRequestHeader: function(e, t) {
                        var i = e.toLowerCase();
                        return w || (e = b[i] = b[i] || e, y[e] = t), this
                    },
                    overrideMimeType: function(e) {
                        return w || (h.mimeType = e), this
                    },
                    statusCode: function(e) {
                        var t;
                        if (e)
                            if (2 > w)
                                for (t in e) v[t] = [v[t], e[t]];
                            else k.always(e[k.status]);
                        return this
                    },
                    abort: function(e) {
                        var t = e || x;
                        return d && d.abort(t), n(0, t), this
                    }
                };
            if (f.promise(k).complete = g.add, k.success = k.done, k.error = k.fail, h.url = ((e || h.url || Di) + "").replace(Ri, "").replace(Bi, Mi[1] + "//"), h.type = i.method || i.type || h.method || h.type, h.dataTypes = dt.trim(h.dataType || "*").toLowerCase().match(ht) || [""], null == h.crossDomain && (s = Ui.exec(h.url.toLowerCase()), h.crossDomain = !(!s || s[1] === Mi[1] && s[2] === Mi[2] && (s[3] || ("http:" === s[1] ? "80" : "443")) === (Mi[3] || ("http:" === Mi[1] ? "80" : "443")))), h.data && h.processData && "string" != typeof h.data && (h.data = dt.param(h.data, h.traditional)), E(ji, h, i, k), 2 === w) return k;
            c = h.global, c && 0 === dt.active++ && dt.event.trigger("ajaxStart"), h.type = h.type.toUpperCase(), h.hasContent = !Vi.test(h.type), r = h.url, h.hasContent || (h.data && (r = h.url += (Li.test(r) ? "&" : "?") + h.data, delete h.data), h.cache === !1 && (h.url = Oi.test(r) ? r.replace(Oi, "$1_=" + Ei++) : r + (Li.test(r) ? "&" : "?") + "_=" + Ei++)), h.ifModified && (dt.lastModified[r] && k.setRequestHeader("If-Modified-Since", dt.lastModified[r]), dt.etag[r] && k.setRequestHeader("If-None-Match", dt.etag[r])), (h.data && h.hasContent && h.contentType !== !1 || i.contentType) && k.setRequestHeader("Content-Type", h.contentType), k.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + Hi + "; q=0.01" : "") : h.accepts["*"]);
            for (a in h.headers) k.setRequestHeader(a, h.headers[a]);
            if (h.beforeSend && (h.beforeSend.call(p, k, h) === !1 || 2 === w)) return k.abort();
            x = "abort";
            for (a in {
                success: 1,
                error: 1,
                complete: 1
            }) k[a](h[a]);
            if (d = E(qi, h, i, k)) {
                k.readyState = 1, c && m.trigger("ajaxSend", [k, h]), h.async && h.timeout > 0 && (l = setTimeout(function() {
                    k.abort("timeout")
                }, h.timeout));
                try {
                    w = 1, d.send(y, n)
                } catch (S) {
                    if (!(2 > w)) throw S;
                    n(-1, S)
                }
            } else n(-1, "No Transport");
            return k
        },
        getJSON: function(e, t, i) {
            return dt.get(e, t, i, "json")
        },
        getScript: function(e, i) {
            return dt.get(e, t, i, "script")
        }
    }), dt.each(["get", "post"], function(e, i) {
        dt[i] = function(e, n, s, a) {
            return dt.isFunction(n) && (a = a || s, s = n, n = t), dt.ajax({
                url: e,
                type: i,
                dataType: a,
                data: n,
                success: s
            })
        }
    }), dt.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(e) {
                return dt.globalEval(e), e
            }
        }
    }), dt.ajaxPrefilter("script", function(e) {
        e.cache === t && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
    }), dt.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var i, n = X.head || dt("head")[0] || X.documentElement;
            return {
                send: function(t, s) {
                    i = X.createElement("script"), i.async = !0, e.scriptCharset && (i.charset = e.scriptCharset), i.src = e.url, i.onload = i.onreadystatechange = function(e, t) {
                        (t || !i.readyState || /loaded|complete/.test(i.readyState)) && (i.onload = i.onreadystatechange = null, i.parentNode && i.parentNode.removeChild(i), i = null, t || s(200, "success"))
                    }, n.insertBefore(i, n.firstChild)
                },
                abort: function() {
                    i && i.onload(t, !0)
                }
            }
        }
    });
    var $i = [],
        Yi = /(=)\?(?=&|$)|\?\?/;
    dt.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = $i.pop() || dt.expando + "_" + Ei++;
            return this[e] = !0, e
        }
    }), dt.ajaxPrefilter("json jsonp", function(i, n, s) {
        var a, r, o, l = i.jsonp !== !1 && (Yi.test(i.url) ? "url" : "string" == typeof i.data && !(i.contentType || "").indexOf("application/x-www-form-urlencoded") && Yi.test(i.data) && "data");
        return l || "jsonp" === i.dataTypes[0] ? (a = i.jsonpCallback = dt.isFunction(i.jsonpCallback) ? i.jsonpCallback() : i.jsonpCallback, l ? i[l] = i[l].replace(Yi, "$1" + a) : i.jsonp !== !1 && (i.url += (Li.test(i.url) ? "&" : "?") + i.jsonp + "=" + a), i.converters["script json"] = function() {
            return o || dt.error(a + " was not called"), o[0]
        }, i.dataTypes[0] = "json", r = e[a], e[a] = function() {
            o = arguments
        }, s.always(function() {
            e[a] = r, i[a] && (i.jsonpCallback = n.jsonpCallback, $i.push(a)), o && dt.isFunction(r) && r(o[0]), o = r = t
        }), "script") : void 0
    });
    var Qi, Ki, Xi = 0,
        Gi = e.ActiveXObject && function() {
            var e;
            for (e in Qi) Qi[e](t, !0)
        };
    dt.ajaxSettings.xhr = e.ActiveXObject ? function() {
        return !this.isLocal && N() || F()
    } : N, Ki = dt.ajaxSettings.xhr(), dt.support.cors = !! Ki && "withCredentials" in Ki, Ki = dt.support.ajax = !! Ki, Ki && dt.ajaxTransport(function(i) {
        if (!i.crossDomain || dt.support.cors) {
            var n;
            return {
                send: function(s, a) {
                    var r, o, l = i.xhr();
                    if (i.username ? l.open(i.type, i.url, i.async, i.username, i.password) : l.open(i.type, i.url, i.async), i.xhrFields)
                        for (o in i.xhrFields) l[o] = i.xhrFields[o];
                    i.mimeType && l.overrideMimeType && l.overrideMimeType(i.mimeType), i.crossDomain || s["X-Requested-With"] || (s["X-Requested-With"] = "XMLHttpRequest");
                    try {
                        for (o in s) l.setRequestHeader(o, s[o])
                    } catch (c) {}
                    l.send(i.hasContent && i.data || null), n = function(e, s) {
                        var o, c, d, u;
                        try {
                            if (n && (s || 4 === l.readyState))
                                if (n = t, r && (l.onreadystatechange = dt.noop, Gi && delete Qi[r]), s) 4 !== l.readyState && l.abort();
                                else {
                                    u = {}, o = l.status, c = l.getAllResponseHeaders(), "string" == typeof l.responseText && (u.text = l.responseText);
                                    try {
                                        d = l.statusText
                                    } catch (h) {
                                        d = ""
                                    }
                                    o || !i.isLocal || i.crossDomain ? 1223 === o && (o = 204) : o = u.text ? 200 : 404
                                }
                        } catch (p) {
                            s || a(-1, p)
                        }
                        u && a(o, d, u, c)
                    }, i.async ? 4 === l.readyState ? setTimeout(n) : (r = ++Xi, Gi && (Qi || (Qi = {}, dt(e).unload(Gi)), Qi[r] = n), l.onreadystatechange = n) : n()
                },
                abort: function() {
                    n && n(t, !0)
                }
            }
        }
    });
    var Ji, Zi, en = /^(?:toggle|show|hide)$/,
        tn = new RegExp("^(?:([+-])=|)(" + ut + ")([a-z%]*)$", "i"),
        nn = /queueHooks$/,
        sn = [j],
        an = {
            "*": [
                function(e, t) {
                    var i = this.createTween(e, t),
                        n = i.cur(),
                        s = tn.exec(t),
                        a = s && s[3] || (dt.cssNumber[e] ? "" : "px"),
                        r = (dt.cssNumber[e] || "px" !== a && +n) && tn.exec(dt.css(i.elem, e)),
                        o = 1,
                        l = 20;
                    if (r && r[3] !== a) {
                        a = a || r[3], s = s || [], r = +n || 1;
                        do o = o || ".5", r /= o, dt.style(i.elem, e, r + a); while (o !== (o = i.cur() / n) && 1 !== o && --l)
                    }
                    return s && (r = i.start = +r || +n || 0, i.unit = a, i.end = s[1] ? r + (s[1] + 1) * s[2] : +s[2]), i
                }
            ]
        };
    dt.Animation = dt.extend(U, {
        tweener: function(e, t) {
            dt.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
            for (var i, n = 0, s = e.length; s > n; n++) i = e[n], an[i] = an[i] || [], an[i].unshift(t)
        },
        prefilter: function(e, t) {
            t ? sn.unshift(e) : sn.push(e)
        }
    }), dt.Tween = q, q.prototype = {
        constructor: q,
        init: function(e, t, i, n, s, a) {
            this.elem = e, this.prop = i, this.easing = s || "swing", this.options = t, this.start = this.now = this.cur(), this.end = n, this.unit = a || (dt.cssNumber[i] ? "" : "px")
        },
        cur: function() {
            var e = q.propHooks[this.prop];
            return e && e.get ? e.get(this) : q.propHooks._default.get(this)
        },
        run: function(e) {
            var t, i = q.propHooks[this.prop];
            return this.pos = t = this.options.duration ? dt.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), i && i.set ? i.set(this) : q.propHooks._default.set(this), this
        }
    }, q.prototype.init.prototype = q.prototype, q.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = dt.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
            },
            set: function(e) {
                dt.fx.step[e.prop] ? dt.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[dt.cssProps[e.prop]] || dt.cssHooks[e.prop]) ? dt.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
            }
        }
    }, q.propHooks.scrollTop = q.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, dt.each(["toggle", "show", "hide"], function(e, t) {
        var i = dt.fn[t];
        dt.fn[t] = function(e, n, s) {
            return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(H(t, !0), e, n, s)
        }
    }), dt.fn.extend({
        fadeTo: function(e, t, i, n) {
            return this.filter(S).css("opacity", 0).show().end().animate({
                opacity: t
            }, e, i, n)
        },
        animate: function(e, t, i, n) {
            var s = dt.isEmptyObject(e),
                a = dt.speed(t, i, n),
                r = function() {
                    var t = U(this, dt.extend({}, e), a);
                    (s || dt._data(this, "finish")) && t.stop(!0)
                };
            return r.finish = r, s || a.queue === !1 ? this.each(r) : this.queue(a.queue, r)
        },
        stop: function(e, i, n) {
            var s = function(e) {
                var t = e.stop;
                delete e.stop, t(n)
            };
            return "string" != typeof e && (n = i, i = e, e = t), i && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                var t = !0,
                    i = null != e && e + "queueHooks",
                    a = dt.timers,
                    r = dt._data(this);
                if (i) r[i] && r[i].stop && s(r[i]);
                else
                    for (i in r) r[i] && r[i].stop && nn.test(i) && s(r[i]);
                for (i = a.length; i--;) a[i].elem !== this || null != e && a[i].queue !== e || (a[i].anim.stop(n), t = !1, a.splice(i, 1));
                (t || !n) && dt.dequeue(this, e)
            })
        },
        finish: function(e) {
            return e !== !1 && (e = e || "fx"), this.each(function() {
                var t, i = dt._data(this),
                    n = i[e + "queue"],
                    s = i[e + "queueHooks"],
                    a = dt.timers,
                    r = n ? n.length : 0;
                for (i.finish = !0, dt.queue(this, e, []), s && s.stop && s.stop.call(this, !0), t = a.length; t--;) a[t].elem === this && a[t].queue === e && (a[t].anim.stop(!0), a.splice(t, 1));
                for (t = 0; r > t; t++) n[t] && n[t].finish && n[t].finish.call(this);
                delete i.finish
            })
        }
    }), dt.each({
        slideDown: H("show"),
        slideUp: H("hide"),
        slideToggle: H("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(e, t) {
        dt.fn[e] = function(e, i, n) {
            return this.animate(t, e, i, n)
        }
    }), dt.speed = function(e, t, i) {
        var n = e && "object" == typeof e ? dt.extend({}, e) : {
            complete: i || !i && t || dt.isFunction(e) && e,
            duration: e,
            easing: i && t || t && !dt.isFunction(t) && t
        };
        return n.duration = dt.fx.off ? 0 : "number" == typeof n.duration ? n.duration : n.duration in dt.fx.speeds ? dt.fx.speeds[n.duration] : dt.fx.speeds._default, (null == n.queue || n.queue === !0) && (n.queue = "fx"), n.old = n.complete, n.complete = function() {
            dt.isFunction(n.old) && n.old.call(this), n.queue && dt.dequeue(this, n.queue)
        }, n
    }, dt.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }
    }, dt.timers = [], dt.fx = q.prototype.init, dt.fx.tick = function() {
        var e, i = dt.timers,
            n = 0;
        for (Ji = dt.now(); n < i.length; n++) e = i[n], e() || i[n] !== e || i.splice(n--, 1);
        i.length || dt.fx.stop(), Ji = t
    }, dt.fx.timer = function(e) {
        e() && dt.timers.push(e) && dt.fx.start()
    }, dt.fx.interval = 13, dt.fx.start = function() {
        Zi || (Zi = setInterval(dt.fx.tick, dt.fx.interval))
    }, dt.fx.stop = function() {
        clearInterval(Zi), Zi = null
    }, dt.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, dt.fx.step = {}, dt.expr && dt.expr.filters && (dt.expr.filters.animated = function(e) {
        return dt.grep(dt.timers, function(t) {
            return e === t.elem
        }).length
    }), dt.fn.offset = function(e) {
        if (arguments.length) return e === t ? this : this.each(function(t) {
            dt.offset.setOffset(this, e, t)
        });
        var i, n, s = {
                top: 0,
                left: 0
            }, a = this[0],
            r = a && a.ownerDocument;
        if (r) return i = r.documentElement, dt.contains(i, a) ? (typeof a.getBoundingClientRect !== Q && (s = a.getBoundingClientRect()), n = W(r), {
            top: s.top + (n.pageYOffset || i.scrollTop) - (i.clientTop || 0),
            left: s.left + (n.pageXOffset || i.scrollLeft) - (i.clientLeft || 0)
        }) : s
    }, dt.offset = {
        setOffset: function(e, t, i) {
            var n = dt.css(e, "position");
            "static" === n && (e.style.position = "relative");
            var s, a, r = dt(e),
                o = r.offset(),
                l = dt.css(e, "top"),
                c = dt.css(e, "left"),
                d = ("absolute" === n || "fixed" === n) && dt.inArray("auto", [l, c]) > -1,
                u = {}, h = {};
            d ? (h = r.position(), s = h.top, a = h.left) : (s = parseFloat(l) || 0, a = parseFloat(c) || 0), dt.isFunction(t) && (t = t.call(e, i, o)), null != t.top && (u.top = t.top - o.top + s), null != t.left && (u.left = t.left - o.left + a), "using" in t ? t.using.call(e, u) : r.css(u)
        }
    }, dt.fn.extend({
        position: function() {
            if (this[0]) {
                var e, t, i = {
                        top: 0,
                        left: 0
                    }, n = this[0];
                return "fixed" === dt.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), dt.nodeName(e[0], "html") || (i = e.offset()), i.top += dt.css(e[0], "borderTopWidth", !0), i.left += dt.css(e[0], "borderLeftWidth", !0)), {
                    top: t.top - i.top - dt.css(n, "marginTop", !0),
                    left: t.left - i.left - dt.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var e = this.offsetParent || G; e && !dt.nodeName(e, "html") && "static" === dt.css(e, "position");) e = e.offsetParent;
                return e || G
            })
        }
    }), dt.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, i) {
        var n = /Y/.test(i);
        dt.fn[e] = function(s) {
            return dt.access(this, function(e, s, a) {
                var r = W(e);
                return a === t ? r ? i in r ? r[i] : r.document.documentElement[s] : e[s] : void(r ? r.scrollTo(n ? dt(r).scrollLeft() : a, n ? a : dt(r).scrollTop()) : e[s] = a)
            }, e, s, arguments.length, null)
        }
    }), dt.each({
        Height: "height",
        Width: "width"
    }, function(e, i) {
        dt.each({
            padding: "inner" + e,
            content: i,
            "": "outer" + e
        }, function(n, s) {
            dt.fn[s] = function(s, a) {
                var r = arguments.length && (n || "boolean" != typeof s),
                    o = n || (s === !0 || a === !0 ? "margin" : "border");
                return dt.access(this, function(i, n, s) {
                    var a;
                    return dt.isWindow(i) ? i.document.documentElement["client" + e] : 9 === i.nodeType ? (a = i.documentElement, Math.max(i.body["scroll" + e], a["scroll" + e], i.body["offset" + e], a["offset" + e], a["client" + e])) : s === t ? dt.css(i, n, o) : dt.style(i, n, s, o)
                }, i, r ? s : t, r, null)
            }
        })
    }), dt.fn.size = function() {
        return this.length
    }, dt.fn.andSelf = dt.fn.addBack, "object" == typeof module && module && "object" == typeof module.exports ? module.exports = dt : (e.jQuery = e.$ = dt, "function" == typeof define && define.amd && define("jquery-lib", [], function() {
        return dt
    }))
}(window)