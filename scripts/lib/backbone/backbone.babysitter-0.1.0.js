+function(e, t) {
    if ("object" == typeof exports) {
        var i = require("underscore"),
            n = require("backbone");
        module.exports = t(i, n)
    } else "function" == typeof define && define.amd && define("backbone.babysitter", ["underscore", "backbone"], t)
}(this, function(e, t) {
    "option strict";
    return t.ChildViewContainer = function(e, t) {
        var i = function(e) {
            this._views = {}, this._indexByModel = {}, this._indexByCustom = {}, this._updateLength(), t.each(e, this.add, this)
        };
        t.extend(i.prototype, {
            add: function(e, t) {
                var i = e.cid;
                return this._views[i] = e, e.model && (this._indexByModel[e.model.cid] = i), t && (this._indexByCustom[t] = i), this._updateLength(), this
            },
            findByModel: function(e) {
                return this.findByModelCid(e.cid)
            },
            findByModelCid: function(e) {
                var t = this._indexByModel[e];
                return this.findByCid(t)
            },
            findByCustom: function(e) {
                var t = this._indexByCustom[e];
                return this.findByCid(t)
            },
            findByIndex: function(e) {
                return t.values(this._views)[e]
            },
            findByCid: function(e) {
                return this._views[e]
            },
            remove: function(e) {
                var i = e.cid;
                return e.model && delete this._indexByModel[e.model.cid], t.any(this._indexByCustom, function(e, t) {
                    return e === i ? (delete this._indexByCustom[t], !0) : void 0
                }, this), delete this._views[i], this._updateLength(), this
            },
            call: function(e) {
                this.apply(e, t.tail(arguments))
            },
            apply: function(e, i) {
                t.each(this._views, function(n) {
                    t.isFunction(n[e]) && n[e].apply(n, i || [])
                })
            },
            _updateLength: function() {
                this.length = t.size(this._views)
            }
        });
        var n = ["forEach", "each", "map", "find", "detect", "filter", "select", "reject", "every", "all", "some", "any", "include", "contains", "invoke", "toArray", "first", "initial", "rest", "last", "without", "isEmpty", "pluck"];
        return t.each(n, function(e) {
            i.prototype[e] = function() {
                var i = t.values(this._views),
                    n = [i].concat(t.toArray(arguments));
                return t[e].apply(t, n)
            }
        }), i
    }(t, e), t.ChildViewContainer
});