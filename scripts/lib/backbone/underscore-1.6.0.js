function() {
    var e = this,
        t = e._,
        i = {}, n = Array.prototype,
        s = Object.prototype,
        a = Function.prototype,
        r = n.push,
        o = n.slice,
        l = n.concat,
        c = s.toString,
        d = s.hasOwnProperty,
        u = n.forEach,
        h = n.map,
        p = n.reduce,
        m = n.reduceRight,
        f = n.filter,
        g = n.every,
        v = n.some,
        y = n.indexOf,
        b = n.lastIndexOf,
        w = Array.isArray,
        x = Object.keys,
        k = a.bind,
        S = function(e) {
            return e instanceof S ? e : this instanceof S ? void(this._wrapped = e) : new S(e)
        };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = S), exports._ = S) : e._ = S, S.VERSION = "1.6.0";
    var C = S.each = S.forEach = function(e, t, n) {
        if (null == e) return e;
        if (u && e.forEach === u) e.forEach(t, n);
        else if (e.length === +e.length) {
            for (var s = 0, a = e.length; a > s; s++)
                if (t.call(n, e[s], s, e) === i) return
        } else
            for (var r = S.keys(e), s = 0, a = r.length; a > s; s++)
                if (t.call(n, e[r[s]], r[s], e) === i) return; return e
    };
    S.map = S.collect = function(e, t, i) {
        var n = [];
        return null == e ? n : h && e.map === h ? e.map(t, i) : (C(e, function(e, s, a) {
            n.push(t.call(i, e, s, a))
        }), n)
    };
    var P = "Reduce of empty array with no initial value";
    S.reduce = S.foldl = S.inject = function(e, t, i, n) {
        var s = arguments.length > 2;
        if (null == e && (e = []), p && e.reduce === p) return n && (t = S.bind(t, n)), s ? e.reduce(t, i) : e.reduce(t);
        if (C(e, function(e, a, r) {
            s ? i = t.call(n, i, e, a, r) : (i = e, s = !0)
        }), !s) throw new TypeError(P);
        return i
    }, S.reduceRight = S.foldr = function(e, t, i, n) {
        var s = arguments.length > 2;
        if (null == e && (e = []), m && e.reduceRight === m) return n && (t = S.bind(t, n)), s ? e.reduceRight(t, i) : e.reduceRight(t);
        var a = e.length;
        if (a !== +a) {
            var r = S.keys(e);
            a = r.length
        }
        if (C(e, function(o, l, c) {
            l = r ? r[--a] : --a, s ? i = t.call(n, i, e[l], l, c) : (i = e[l], s = !0)
        }), !s) throw new TypeError(P);
        return i
    }, S.find = S.detect = function(e, t, i) {
        var n;
        return A(e, function(e, s, a) {
            return t.call(i, e, s, a) ? (n = e, !0) : void 0
        }), n
    }, S.filter = S.select = function(e, t, i) {
        var n = [];
        return null == e ? n : f && e.filter === f ? e.filter(t, i) : (C(e, function(e, s, a) {
            t.call(i, e, s, a) && n.push(e)
        }), n)
    }, S.reject = function(e, t, i) {
        return S.filter(e, function(e, n, s) {
            return !t.call(i, e, n, s)
        }, i)
    }, S.every = S.all = function(e, t, n) {
        t || (t = S.identity);
        var s = !0;
        return null == e ? s : g && e.every === g ? e.every(t, n) : (C(e, function(e, a, r) {
            return (s = s && t.call(n, e, a, r)) ? void 0 : i
        }), !! s)
    };
    var A = S.some = S.any = function(e, t, n) {
        t || (t = S.identity);
        var s = !1;
        return null == e ? s : v && e.some === v ? e.some(t, n) : (C(e, function(e, a, r) {
            return s || (s = t.call(n, e, a, r)) ? i : void 0
        }), !! s)
    };
    S.contains = S.include = function(e, t) {
        return null == e ? !1 : y && e.indexOf === y ? -1 != e.indexOf(t) : A(e, function(e) {
            return e === t
        })
    }, S.invoke = function(e, t) {
        var i = o.call(arguments, 2),
            n = S.isFunction(t);
        return S.map(e, function(e) {
            return (n ? t : e[t]).apply(e, i)
        })
    }, S.pluck = function(e, t) {
        return S.map(e, S.property(t))
    }, S.where = function(e, t) {
        return S.filter(e, S.matches(t))
    }, S.findWhere = function(e, t) {
        return S.find(e, S.matches(t))
    }, S.max = function(e, t, i) {
        if (!t && S.isArray(e) && e[0] === +e[0] && e.length < 65535) return Math.max.apply(Math, e);
        var n = -1 / 0,
            s = -1 / 0;
        return C(e, function(e, a, r) {
            var o = t ? t.call(i, e, a, r) : e;
            o > s && (n = e, s = o)
        }), n
    }, S.min = function(e, t, i) {
        if (!t && S.isArray(e) && e[0] === +e[0] && e.length < 65535) return Math.min.apply(Math, e);
        var n = 1 / 0,
            s = 1 / 0;
        return C(e, function(e, a, r) {
            var o = t ? t.call(i, e, a, r) : e;
            s > o && (n = e, s = o)
        }), n
    }, S.shuffle = function(e) {
        var t, i = 0,
            n = [];
        return C(e, function(e) {
            t = S.random(i++), n[i - 1] = n[t], n[t] = e
        }), n
    }, S.sample = function(e, t, i) {
        return null == t || i ? (e.length !== +e.length && (e = S.values(e)), e[S.random(e.length - 1)]) : S.shuffle(e).slice(0, Math.max(0, t))
    };
    var _ = function(e) {
        return null == e ? S.identity : S.isFunction(e) ? e : S.property(e)
    };
    S.sortBy = function(e, t, i) {
        return t = _(t), S.pluck(S.map(e, function(e, n, s) {
            return {
                value: e,
                index: n,
                criteria: t.call(i, e, n, s)
            }
        }).sort(function(e, t) {
            var i = e.criteria,
                n = t.criteria;
            if (i !== n) {
                if (i > n || void 0 === i) return 1;
                if (n > i || void 0 === n) return -1
            }
            return e.index - t.index
        }), "value")
    };
    var T = function(e) {
        return function(t, i, n) {
            var s = {};
            return i = _(i), C(t, function(a, r) {
                var o = i.call(n, a, r, t);
                e(s, o, a)
            }), s
        }
    };
    S.groupBy = T(function(e, t, i) {
        S.has(e, t) ? e[t].push(i) : e[t] = [i]
    }), S.indexBy = T(function(e, t, i) {
        e[t] = i
    }), S.countBy = T(function(e, t) {
        S.has(e, t) ? e[t]++ : e[t] = 1
    }), S.sortedIndex = function(e, t, i, n) {
        i = _(i);
        for (var s = i.call(n, t), a = 0, r = e.length; r > a;) {
            var o = a + r >>> 1;
            i.call(n, e[o]) < s ? a = o + 1 : r = o
        }
        return a
    }, S.toArray = function(e) {
        return e ? S.isArray(e) ? o.call(e) : e.length === +e.length ? S.map(e, S.identity) : S.values(e) : []
    }, S.size = function(e) {
        return null == e ? 0 : e.length === +e.length ? e.length : S.keys(e).length
    }, S.first = S.head = S.take = function(e, t, i) {
        return null == e ? void 0 : null == t || i ? e[0] : 0 > t ? [] : o.call(e, 0, t)
    }, S.initial = function(e, t, i) {
        return o.call(e, 0, e.length - (null == t || i ? 1 : t))
    }, S.last = function(e, t, i) {
        return null == e ? void 0 : null == t || i ? e[e.length - 1] : o.call(e, Math.max(e.length - t, 0))
    }, S.rest = S.tail = S.drop = function(e, t, i) {
        return o.call(e, null == t || i ? 1 : t)
    }, S.compact = function(e) {
        return S.filter(e, S.identity)
    };
    var I = function(e, t, i) {
        return t && S.every(e, S.isArray) ? l.apply(i, e) : (C(e, function(e) {
            S.isArray(e) || S.isArguments(e) ? t ? r.apply(i, e) : I(e, t, i) : i.push(e)
        }), i)
    };
    S.flatten = function(e, t) {
        return I(e, t, [])
    }, S.without = function(e) {
        return S.difference(e, o.call(arguments, 1))
    }, S.partition = function(e, t, i) {
        t = _(t);
        var n = [],
            s = [];
        return C(e, function(e) {
            (t.call(i, e) ? n : s).push(e)
        }), [n, s]
    }, S.uniq = S.unique = function(e, t, i, n) {
        S.isFunction(t) && (n = i, i = t, t = !1);
        var s = i ? S.map(e, i, n) : e,
            a = [],
            r = [];
        return C(s, function(i, n) {
            (t ? n && r[r.length - 1] === i : S.contains(r, i)) || (r.push(i), a.push(e[n]))
        }), a
    }, S.union = function() {
        return S.uniq(S.flatten(arguments, !0))
    }, S.intersection = function(e) {
        var t = o.call(arguments, 1);
        return S.filter(S.uniq(e), function(e) {
            return S.every(t, function(t) {
                return S.contains(t, e)
            })
        })
    }, S.difference = function(e) {
        var t = l.apply(n, o.call(arguments, 1));
        return S.filter(e, function(e) {
            return !S.contains(t, e)
        })
    }, S.zip = function() {
        for (var e = S.max(S.pluck(arguments, "length").concat(0)), t = new Array(e), i = 0; e > i; i++) t[i] = S.pluck(arguments, "" + i);
        return t
    }, S.object = function(e, t) {
        if (null == e) return {};
        for (var i = {}, n = 0, s = e.length; s > n; n++) t ? i[e[n]] = t[n] : i[e[n][0]] = e[n][1];
        return i
    }, S.indexOf = function(e, t, i) {
        if (null == e) return -1;
        var n = 0,
            s = e.length;
        if (i) {
            if ("number" != typeof i) return n = S.sortedIndex(e, t), e[n] === t ? n : -1;
            n = 0 > i ? Math.max(0, s + i) : i
        }
        if (y && e.indexOf === y) return e.indexOf(t, i);
        for (; s > n; n++)
            if (e[n] === t) return n;
        return -1
    }, S.lastIndexOf = function(e, t, i) {
        if (null == e) return -1;
        var n = null != i;
        if (b && e.lastIndexOf === b) return n ? e.lastIndexOf(t, i) : e.lastIndexOf(t);
        for (var s = n ? i : e.length; s--;)
            if (e[s] === t) return s;
        return -1
    }, S.range = function(e, t, i) {
        arguments.length <= 1 && (t = e || 0, e = 0), i = arguments[2] || 1;
        for (var n = Math.max(Math.ceil((t - e) / i), 0), s = 0, a = new Array(n); n > s;) a[s++] = e, e += i;
        return a
    };
    var M = function() {};
    S.bind = function(e, t) {
        var i, n;
        if (k && e.bind === k) return k.apply(e, o.call(arguments, 1));
        if (!S.isFunction(e)) throw new TypeError;
        return i = o.call(arguments, 2), n = function() {
            if (!(this instanceof n)) return e.apply(t, i.concat(o.call(arguments)));
            M.prototype = e.prototype;
            var s = new M;
            M.prototype = null;
            var a = e.apply(s, i.concat(o.call(arguments)));
            return Object(a) === a ? a : s
        }
    }, S.partial = function(e) {
        var t = o.call(arguments, 1);
        return function() {
            for (var i = 0, n = t.slice(), s = 0, a = n.length; a > s; s++) n[s] === S && (n[s] = arguments[i++]);
            for (; i < arguments.length;) n.push(arguments[i++]);
            return e.apply(this, n)
        }
    }, S.bindAll = function(e) {
        var t = o.call(arguments, 1);
        if (0 === t.length) throw new Error("bindAll must be passed function names");
        return C(t, function(t) {
            e[t] = S.bind(e[t], e)
        }), e
    }, S.memoize = function(e, t) {
        var i = {};
        return t || (t = S.identity),
        function() {
            var n = t.apply(this, arguments);
            return S.has(i, n) ? i[n] : i[n] = e.apply(this, arguments)
        }
    }, S.delay = function(e, t) {
        var i = o.call(arguments, 2);
        return setTimeout(function() {
            return e.apply(null, i)
        }, t)
    }, S.defer = function(e) {
        return S.delay.apply(S, [e, 1].concat(o.call(arguments, 1)))
    }, S.throttle = function(e, t, i) {
        var n, s, a, r = null,
            o = 0;
        i || (i = {});
        var l = function() {
            o = i.leading === !1 ? 0 : S.now(), r = null, a = e.apply(n, s), n = s = null
        };
        return function() {
            var c = S.now();
            o || i.leading !== !1 || (o = c);
            var d = t - (c - o);
            return n = this, s = arguments, 0 >= d ? (clearTimeout(r), r = null, o = c, a = e.apply(n, s), n = s = null) : r || i.trailing === !1 || (r = setTimeout(l, d)), a
        }
    }, S.debounce = function(e, t, i) {
        var n, s, a, r, o, l = function() {
                var c = S.now() - r;
                t > c ? n = setTimeout(l, t - c) : (n = null, i || (o = e.apply(a, s), a = s = null))
            };
        return function() {
            a = this, s = arguments, r = S.now();
            var c = i && !n;
            return n || (n = setTimeout(l, t)), c && (o = e.apply(a, s), a = s = null), o
        }
    }, S.once = function(e) {
        var t, i = !1;
        return function() {
            return i ? t : (i = !0, t = e.apply(this, arguments), e = null, t)
        }
    }, S.wrap = function(e, t) {
        return S.partial(t, e)
    }, S.compose = function() {
        var e = arguments;
        return function() {
            for (var t = arguments, i = e.length - 1; i >= 0; i--) t = [e[i].apply(this, t)];
            return t[0]
        }
    }, S.after = function(e, t) {
        return function() {
            return --e < 1 ? t.apply(this, arguments) : void 0
        }
    }, S.keys = function(e) {
        if (!S.isObject(e)) return [];
        if (x) return x(e);
        var t = [];
        for (var i in e) S.has(e, i) && t.push(i);
        return t
    }, S.values = function(e) {
        for (var t = S.keys(e), i = t.length, n = new Array(i), s = 0; i > s; s++) n[s] = e[t[s]];
        return n
    }, S.pairs = function(e) {
        for (var t = S.keys(e), i = t.length, n = new Array(i), s = 0; i > s; s++) n[s] = [t[s], e[t[s]]];
        return n
    }, S.invert = function(e) {
        for (var t = {}, i = S.keys(e), n = 0, s = i.length; s > n; n++) t[e[i[n]]] = i[n];
        return t
    }, S.functions = S.methods = function(e) {
        var t = [];
        for (var i in e) S.isFunction(e[i]) && t.push(i);
        return t.sort()
    }, S.extend = function(e) {
        return C(o.call(arguments, 1), function(t) {
            if (t)
                for (var i in t) e[i] = t[i]
        }), e
    }, S.pick = function(e) {
        var t = {}, i = l.apply(n, o.call(arguments, 1));
        return C(i, function(i) {
            i in e && (t[i] = e[i])
        }), t
    }, S.omit = function(e) {
        var t = {}, i = l.apply(n, o.call(arguments, 1));
        for (var s in e) S.contains(i, s) || (t[s] = e[s]);
        return t
    }, S.defaults = function(e) {
        return C(o.call(arguments, 1), function(t) {
            if (t)
                for (var i in t) void 0 === e[i] && (e[i] = t[i])
        }), e
    }, S.clone = function(e) {
        return S.isObject(e) ? S.isArray(e) ? e.slice() : S.extend({}, e) : e
    }, S.tap = function(e, t) {
        return t(e), e
    };
    var D = function(e, t, i, n) {
        if (e === t) return 0 !== e || 1 / e == 1 / t;
        if (null == e || null == t) return e === t;
        e instanceof S && (e = e._wrapped), t instanceof S && (t = t._wrapped);
        var s = c.call(e);
        if (s != c.call(t)) return !1;
        switch (s) {
            case "[object String]":
                return e == String(t);
            case "[object Number]":
                return e != +e ? t != +t : 0 == e ? 1 / e == 1 / t : e == +t;
            case "[object Date]":
            case "[object Boolean]":
                return +e == +t;
            case "[object RegExp]":
                return e.source == t.source && e.global == t.global && e.multiline == t.multiline && e.ignoreCase == t.ignoreCase
        }
        if ("object" != typeof e || "object" != typeof t) return !1;
        for (var a = i.length; a--;)
            if (i[a] == e) return n[a] == t;
        var r = e.constructor,
            o = t.constructor;
        if (r !== o && !(S.isFunction(r) && r instanceof r && S.isFunction(o) && o instanceof o) && "constructor" in e && "constructor" in t) return !1;
        i.push(e), n.push(t);
        var l = 0,
            d = !0;
        if ("[object Array]" == s) {
            if (l = e.length, d = l == t.length)
                for (; l-- && (d = D(e[l], t[l], i, n)););
        } else {
            for (var u in e)
                if (S.has(e, u) && (l++, !(d = S.has(t, u) && D(e[u], t[u], i, n)))) break;
            if (d) {
                for (u in t)
                    if (S.has(t, u) && !l--) break;
                d = !l
            }
        }
        return i.pop(), n.pop(), d
    };
    S.isEqual = function(e, t) {
        return D(e, t, [], [])
    }, 
    S.isEmpty = function(e) {
        if (null == e) return !0;
        if (S.isArray(e) || S.isString(e)) return 0 === e.length;
        for (var t in e)
            if (S.has(e, t)) return !1;
        return !0
    }, 
    S.isElement = function(e) {
        return !(!e || 1 !== e.nodeType)
    }, 
    S.isArray = w || function(e) {
        return "[object Array]" == c.call(e)
    }, 
    S.isObject = function(e) {
        return e === Object(e)
    }, 
    C(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function(e) {
        S["is" + e] = function(t) {
            return c.call(t) == "[object " + e + "]"
        }
    }), S.isArguments(arguments) || (S.isArguments = function(e) {
        return !(!e || !S.has(e, "callee"))
    }), "function" != typeof / . / && (S.isFunction = function(e) {
        return "function" == typeof e
    }), S.isFinite = function(e) {
        return isFinite(e) && !isNaN(parseFloat(e))
    }, S.isNaN = function(e) {
        return S.isNumber(e) && e != +e
    }, S.isBoolean = function(e) {
        return e === !0 || e === !1 || "[object Boolean]" == c.call(e)
    }, S.isNull = function(e) {
        return null === e
    }, S.isUndefined = function(e) {
        return void 0 === e
    }, S.has = function(e, t) {
        return d.call(e, t)
    }, S.noConflict = function() {
        return e._ = t, this
    }, S.identity = function(e) {
        return e
    }, S.constant = function(e) {
        return function() {
            return e
        }
    }, S.property = function(e) {
        return function(t) {
            return t[e]
        }
    }, S.matches = function(e) {
        return function(t) {
            if (t === e) return !0;
            for (var i in e)
                if (e[i] !== t[i]) return !1;
            return !0
        }
    }, S.times = function(e, t, i) {
        for (var n = Array(Math.max(0, e)), s = 0; e > s; s++) n[s] = t.call(i, s);
        return n
    }, S.random = function(e, t) {
        return null == t && (t = e, e = 0), e + Math.floor(Math.random() * (t - e + 1))
    }, S.now = Date.now || function() {
        return (new Date).getTime()
    };
    var E = {
        escape: {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#x27;"
        }
    };
    E.unescape = S.invert(E.escape);
    var L = {
        escape: new RegExp("[" + S.keys(E.escape).join("") + "]", "g"),
        unescape: new RegExp("(" + S.keys(E.unescape).join("|") + ")", "g")
    };
    S.each(["escape", "unescape"], function(e) {
        S[e] = function(t) {
            return null == t ? "" : ("" + t).replace(L[e], function(t) {
                return E[e][t]
            })
        }
    }), S.result = function(e, t) {
        if (null == e) return void 0;
        var i = e[t];
        return S.isFunction(i) ? i.call(e) : i
    }, S.mixin = function(e) {
        C(S.functions(e), function(t) {
            var i = S[t] = e[t];
            S.prototype[t] = function() {
                var e = [this._wrapped];
                return r.apply(e, arguments), V.call(this, i.apply(S, e))
            }
        })
    };
    var R = 0;
    S.uniqueId = function(e) {
        var t = ++R + "";
        return e ? e + t : t
    }, S.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    var O = /(.)^/,
        N = {
            "'": "'",
            "\\": "\\",
            "\r": "r",
            "\n": "n",
            "	": "t",
            "\u2028": "u2028",
            "\u2029": "u2029"
        }, F = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    S.template = function(e, t, i) {
        var n;
        i = S.defaults({}, i, S.templateSettings);
        var s = new RegExp([(i.escape || O).source, (i.interpolate || O).source, (i.evaluate || O).source].join("|") + "|$", "g"),
            a = 0,
            r = "__p+='";
        e.replace(s, function(t, i, n, s, o) {
            return r += e.slice(a, o).replace(F, function(e) {
                return "\\" + N[e]
            }), i && (r += "'+\n((__t=(" + i + "))==null?'':_.escape(__t))+\n'"), n && (r += "'+\n((__t=(" + n + "))==null?'':__t)+\n'"), s && (r += "';\n" + s + "\n__p+='"), a = o + t.length, t
        }), r += "';\n", i.variable || (r = "with(obj||{}){\n" + r + "}\n"), r = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + r + "return __p;\n";
        try {
            n = new Function(i.variable || "obj", "_", r)
        } catch (o) {
            throw o.source = r, o
        }
        if (t) return n(t, S);
        var l = function(e) {
            return n.call(this, e, S)
        };
        return l.source = "function(" + (i.variable || "obj") + "){\n" + r + "}", l
    }, S.chain = function(e) {
        return S(e).chain()
    };
    var V = function(e) {
        return this._chain ? S(e).chain() : e
    };
    S.mixin(S), C(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function(e) {
        var t = n[e];
        S.prototype[e] = function() {
            var i = this._wrapped;
            return t.apply(i, arguments), "shift" != e && "splice" != e || 0 !== i.length || delete i[0], V.call(this, i)
        }
    }), C(["concat", "join", "slice"], function(e) {
        var t = n[e];
        S.prototype[e] = function() {
            return V.call(this, t.apply(this._wrapped, arguments))
        }
    }), S.extend(S.prototype, {
        chain: function() {
            return this._chain = !0, this
        },
        value: function() {
            return this._wrapped
        }
    }), "function" == typeof define && define.amd && define("underscore", [], function() {
        return S
    })
}.call(this);