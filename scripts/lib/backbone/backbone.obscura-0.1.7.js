+function(e, t) {
    "object" == typeof exports ? module.exports = t(require("underscore"), require("backbone")) : "function" == typeof define && define.amd ? define("backbone.obscura", ["underscore", "backbone"], t) : e.Backbone.Obscura = t(e._, e.Backbone)
}(this, function(e, t) {
    function i(i) {
        return {
            underscore: e,
            backbone: t
        }[i]
    }
    var n = function() {
        function e(t) {
            var i = e.cache[t];
            if (!i) {
                var n = {};
                i = e.cache[t] = {
                    id: t,
                    exports: n
                }, e.modules[t].call(n, i, n)
            }
            return i.exports
        }
        return e.cache = [], e.modules = [
            function(t) {
                function n(e, t) {
                    this._superset = e, this._filtered = new r(e, t), this._sorted = new o(this._filtered, t), this._paginated = new l(this._sorted, t), c(this._paginated, this), d.call(this, this._filtered, p), d.call(this, this._sorted, f), d.call(this, this._paginated, v), this.initialize(t)
                }
                var s = i("underscore"),
                    a = i("backbone"),
                    r = e(2),
                    o = e(5),
                    l = e(4),
                    c = e(1),
                    d = e(7),
                    u = {
                        superset: function() {
                            return this._superset
                        },
                        getFilteredLength: function() {
                            return this._filtered.length
                        },
                        removeTransforms: function() {
                            return this._filtered.resetFilters(), this._sorted.removeSort(), this._paginated.removePagination(), this
                        },
                        destroy: function() {
                            this.stopListening(), this._filtered.destroy(), this._sorted.destroy(), this._paginated.destroy(), this.length = 0, this.trigger("obscura:destroy")
                        }
                    }, h = ["filterBy", "removeFilter", "resetFilters", "refilter", "hasFilter", "getFilters"],
                    p = ["filtered:add", "filtered:remove", "filtered:reset"],
                    m = ["setSort", "reverseSort", "removeSort"],
                    f = ["sorted:add", "sorted:remove"],
                    g = ["setPerPage", "setPage", "getPerPage", "getNumPages", "getPage", "hasNextPage", "hasPrevPage", "nextPage", "prevPage", "movePage", "removePagination", "firstPage", "lastPage"],
                    v = ["paginated:change:perPage", "paginated:change:page", "paginated:change:numPages"],
                    y = ["add", "create", "remove", "set", "reset", "sort", "parse", "sync", "fetch", "push", "pop", "shift", "unshift"];
                s.each(h, function(e) {
                    u[e] = function() {
                        var t = r.prototype[e].apply(this._filtered, arguments);
                        return t === this._filtered ? this : t
                    }
                }), s.each(g, function(e) {
                    u[e] = function() {
                        var t = l.prototype[e].apply(this._paginated, arguments);
                        return t === this._paginated ? this : t
                    }
                }), s.each(m, function(e) {
                    u[e] = function() {
                        var t = o.prototype[e].apply(this._sorted, arguments);
                        return t === this._sorted ? this : t
                    }
                }), s.each(y, function(e) {
                    u[e] = function() {
                        throw new Error("Backbone.Obscura: Unsupported method: " + e + "called on read-only proxy")
                    }
                }), s.extend(n.prototype, u, a.Events), n = a.Collection.extend(n.prototype), n.FilteredCollection = r, n.SortedCollection = o, n.PaginatedCollection = l, t.exports = n
            },
            function(e) {
                function t(e, t) {
                    function i() {
                        t.length = e.length
                    }

                    function o(i) {
                        var s = n.toArray(arguments),
                            a = "change" === i || "change:" === i.slice(0, 7);
                        "reset" === i && (t.models = e.models), n.contains(r, i) ? (n.contains(["add", "remove", "destroy"], i) ? s[2] = t : n.contains(["reset", "sort"], i) && (s[1] = t), t.trigger.apply(this, s)) : a && t.contains(s[1]) && t.trigger.apply(this, s)
                    }
                    var l = {};
                    return n.each(n.functions(s.Collection.prototype), function(t) {
                        n.contains(a, t) || (l[t] = function() {
                            return e[t].apply(e, arguments)
                        })
                    }), n.extend(t, s.Events, l), t.listenTo(e, "all", i), t.listenTo(e, "all", o), t.models = e.models, i(), t
                }
                var n = i("underscore"),
                    s = i("backbone"),
                    a = ["_onModelEvent", "_prepareModel", "_removeReference", "_reset", "add", "initialize", "sync", "remove", "reset", "set", "push", "pop", "unshift", "shift", "sort", "parse", "fetch", "create", "model", "off", "on", "listenTo", "listenToOnce", "bind", "trigger", "once", "stopListening"],
                    r = ["add", "remove", "reset", "sort", "destroy", "sync", "request", "error"];
                e.exports = t
            },
            function(t) {
                function n() {
                    this._filterResultCache = {}
                }

                function s(e) {
                    for (var t in this._filterResultCache) this._filterResultCache.hasOwnProperty(t) && delete this._filterResultCache[t][e]
                }

                function a(e, t) {
                    this._filters[e] && s.call(this, e), this._filters[e] = t, this.trigger("filtered:add", e)
                }

                function r(e) {
                    delete this._filters[e], s.call(this, e), this.trigger("filtered:remove", e)
                }

                function o(e) {
                    this._filterResultCache[e.cid] || (this._filterResultCache[e.cid] = {});
                    var t = this._filterResultCache[e.cid];
                    for (var i in this._filters)
                        if (this._filters.hasOwnProperty(i) && (t.hasOwnProperty(i) || (t[i] = this._filters[i].fn(e)), !t[i])) return !1;
                    return !0
                }

                function l(e) {
                    e || (e = {});
                    var t = [];
                    this._superset && (t = this._superset.filter(g.bind(o, this))), e.reset ? this._collection.reset(t) : this._collection.set(t), this.length = this._collection.length
                }

                function c() {
                    l.call(this, {
                        reset: !0
                    })
                }

                function d() {
                    var e = !(!this._superset || !this._superset.comparator);
                    l.call(this, {
                        reset: e
                    })
                }

                function u(e) {
                    if (this._filterResultCache[e.cid] = {}, o.call(this, e)) {
                        if (!this._collection.get(e.cid)) {
                            for (var t = this.superset().indexOf(e), i = null, n = t - 1; n >= 0; n -= 1)
                                if (this.contains(this.superset().at(n))) {
                                    i = this.indexOf(this.superset().at(n)) + 1;
                                    break
                                }
                            i = i || 0, this._collection.add(e, {
                                at: i
                            })
                        }
                    } else this._collection.get(e.cid) && this._collection.remove(e);
                    this.length = this._collection.length
                }

                function h(e) {
                    this._filterResultCache[e.cid] = {}, o.call(this, e) || this._collection.get(e.cid) && this._collection.remove(e)
                }

                function p(e) {
                    "change:" === e.slice(0, 7) && h.call(this, arguments[1])
                }

                function m(e) {
                    this.contains(e) && this._collection.remove(e), this.length = this._collection.length
                }

                function f(e) {
                    this._superset = e, this._collection = new v.Collection(e.toArray()), y(this._collection, this), this.resetFilters(), this.listenTo(this._superset, "reset", c), this.listenTo(this._superset, "sort", d), this.listenTo(this._superset, "add change", u), this.listenTo(this._superset, "remove", m), this.listenTo(this._superset, "all", p)
                }
                var g = i("underscore"),
                    v = i("backbone"),
                    y = e(1),
                    b = e(3),
                    w = {
                        defaultFilterName: "__default",
                        filterBy: function(e, t) {
                            return t || (t = e, e = this.defaultFilterName), a.call(this, e, b(t)), l.call(this), this
                        },
                        removeFilter: function(e) {
                            return e || (e = this.defaultFilterName), r.call(this, e), l.call(this), this
                        },
                        resetFilters: function() {
                            return this._filters = {}, n.call(this), this.trigger("filtered:reset"), l.call(this), this
                        },
                        superset: function() {
                            return this._superset
                        },
                        refilter: function(e) {
                            return "object" == typeof e && e.cid ? u.call(this, e) : (n.call(this), l.call(this)), this
                        },
                        getFilters: function() {
                            return g.keys(this._filters)
                        },
                        hasFilter: function(e) {
                            return g.contains(this.getFilters(), e)
                        },
                        destroy: function() {
                            this.stopListening(), this._collection.reset([]), this._superset = this._collection, this.length = 0, this.trigger("filtered:destroy")
                        }
                    };
                g.extend(f.prototype, w, v.Events), t.exports = f
            },
            function(e) {
                function t(e, t) {
                    return function(i) {
                        return i.get(e) === t
                    }
                }

                function n(e, t) {
                    return function(i) {
                        return t(i.get(e))
                    }
                }

                function s(e, t) {
                    return o.isArray(t) || (t = null), {
                        fn: e,
                        keys: t
                    }
                }

                function a(e) {
                    var i = o.keys(e),
                        a = o.map(i, function(i) {
                            var s = e[i];
                            return o.isFunction(s) ? n(i, s) : t(i, s)
                        }),
                        r = function(e) {
                            for (var t = 0; t < a.length; t++)
                                if (!a[t](e)) return !1;
                            return !0
                        };
                    return s(r, i)
                }

                function r(e, t) {
                    return o.isFunction(e) ? s(e, t) : o.isObject(e) ? a(e) : void 0
                }
                var o = i("underscore");
                e.exports = r
            },
            function(t) {
                function n() {
                    var e = this.getPage() * this.getPerPage(),
                        t = e + this.getPerPage();
                    return [e, t]
                }

                function s() {
                    var e = n.call(this);
                    this._collection.reset(this.superset().slice(e[0], e[1]))
                }

                function a() {
                    var e = (this._totalPages, this.superset().length),
                        t = this.getPerPage(),
                        i = e % t === 0 ? e / t : Math.floor(e / t) + 1,
                        n = this._totalPages !== i;
                    return this._totalPages = i, n && this.trigger("paginated:change:numPages", {
                        numPages: i
                    }), this.getPage() >= i ? (this.setPage(i - 1), !0) : void 0
                }

                function r() {
                    a.call(this) || s.call(this)
                }

                function o(e, t) {
                    for (var i = d.max([e.length, t.length]), n = 0, s = 0; i > n; n += 1, s += 1)
                        if (e[n] !== t[s])
                            if (t[n - 1] === e[n]) s -= 1;
                            else {
                                if (t[n + 1] !== e[n]) return e[n];
                                s += 1
                            }
                }

                function l() {
                    if (!a.call(this)) {
                        var e = n.call(this),
                            t = e[0],
                            i = e[1],
                            s = o(this.superset().slice(t, i), this._collection.toArray()),
                            r = o(this._collection.toArray(), this.superset().slice(t, i));
                        r && this._collection.remove(r), s && this._collection.add(s, {
                            at: this.superset().indexOf(s) - t
                        })
                    }
                }

                function c(e, t) {
                    this._superset = e, this._collection = new u.Collection(e.toArray()), this._page = 0, this.setPerPage(t && t.perPage ? t.perPage : null), h(this._collection, this), this.listenTo(this._superset, "add remove", l), this.listenTo(this._superset, "reset sort", r)
                }
                var d = i("underscore"),
                    u = i("backbone"),
                    h = e(1),
                    p = {
                        removePagination: function() {
                            return this.setPerPage(null), this
                        },
                        setPerPage: function(e) {
                            return this._perPage = e, r.call(this), this.setPage(0), this.trigger("paginated:change:perPage", {
                                perPage: e,
                                numPages: this.getNumPages()
                            }), this
                        },
                        setPage: function(e) {
                            var t = 0,
                                i = this.getNumPages() - 1;
                            return e = e > t ? e : t, e = i > e ? e : i, e = 0 > e ? 0 : e, this._page = e, s.call(this), this.trigger("paginated:change:page", {
                                page: e
                            }), this
                        },
                        getPerPage: function() {
                            return this._perPage || this.superset().length || 1
                        },
                        getNumPages: function() {
                            return this._totalPages
                        },
                        getPage: function() {
                            return this._page
                        },
                        hasNextPage: function() {
                            return this.getPage() < this.getNumPages() - 1
                        },
                        hasPrevPage: function() {
                            return this.getPage() > 0
                        },
                        nextPage: function() {
                            return this.movePage(1), this
                        },
                        prevPage: function() {
                            return this.movePage(-1), this
                        },
                        firstPage: function() {
                            this.setPage(0)
                        },
                        lastPage: function() {
                            this.setPage(this.getNumPages() - 1)
                        },
                        movePage: function(e) {
                            return this.setPage(this.getPage() + e), this
                        },
                        superset: function() {
                            return this._superset
                        },
                        destroy: function() {
                            this.stopListening(), this._collection.reset([]), this._superset = this._collection, this._page = 0, this._totalPages = 0, this.length = 0, this.trigger("paginated:destroy")
                        }
                    };
                d.extend(c.prototype, p, u.Events), t.exports = c
            },
            function(t) {
                function n(e) {
                    return d.isFunction(e) ? e : function(t) {
                        return t.get(e)
                    }
                }

                function s(e) {
                    return this._comparator ? this._reverse ? p(this._collection.toArray(), e, n(this._comparator)) : d.sortedIndex(this._collection.toArray(), e, n(this._comparator)) : this._superset.indexOf(e)
                }

                function a(e) {
                    var t = s.call(this, e);
                    this._collection.add(e, {
                        at: t
                    })
                }

                function r(e) {
                    this.contains(e) && this._collection.remove(e)
                }

                function o(e) {
                    this.contains(e) && this._collection.indexOf(e) !== s.call(this, e) && (this._collection.remove(e), a.call(this, e))
                }

                function l() {
                    if (!this._comparator) return void this._collection.reset(this._superset.toArray());
                    var e = this._superset.sortBy(this._comparator);
                    this._collection.reset(this._reverse ? e.reverse() : e)
                }

                function c(e) {
                    this._superset = e, this._reverse = !1, this._comparator = null, this._collection = new u.Collection(e.toArray()), h(this._collection, this), this.listenTo(this._superset, "add", a), this.listenTo(this._superset, "remove", r), this.listenTo(this._superset, "change", o), this.listenTo(this._superset, "reset", l)
                }
                var d = i("underscore"),
                    u = i("backbone"),
                    h = e(1),
                    p = e(6),
                    m = {
                        setSort: function(e, t) {
                            return this._reverse = "desc" === t ? !0 : !1, this._comparator = e, l.call(this), this.trigger(e ? "sorted:add" : "sorted:remove"), this
                        },
                        reverseSort: function() {
                            return this._reverse = !this._reverse, l.call(this), this
                        },
                        removeSort: function() {
                            return this.setSort(), this
                        },
                        superset: function() {
                            return this._superset
                        },
                        destroy: function() {
                            this.stopListening(), this._collection.reset([]), this._superset = this._collection, this.length = 0, this.trigger("sorted:destroy")
                        }
                    };
                d.extend(c.prototype, m, u.Events), t.exports = c
            },
            function(e) {
                function t(e) {
                    return s.isFunction(e) ? e : function(t) {
                        return t[e]
                    }
                }

                function n(e, i, n, a) {
                    n = null == n ? s.identity : t(n);
                    for (var r = n.call(a, i), o = 0, l = e.length; l > o;) {
                        var c = o + l >>> 1;
                        n.call(a, e[c]) < r ? l = c : o = c + 1
                    }
                    return o
                }
                var s = i("underscore");
                e.exports = n
            },
            function(e) {
                function t(e, t) {
                    n.each(t, function(t) {
                        this.listenTo(e, t, function() {
                            var e = n.toArray(arguments);
                            e.unshift(t), this.trigger.apply(this, e)
                        })
                    }, this)
                }
                var n = i("underscore");
                e.exports = t
            }
        ], e(0)
    }();
    return n
});