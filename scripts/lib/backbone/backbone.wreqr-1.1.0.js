+function(e, t) {
    if ("object" == typeof exports) {
        var i = require("underscore"),
            n = require("backbone");
        module.exports = t(i, n)
    } else "function" == typeof define && define.amd && define("backbone.wreqr", ["underscore", "backbone"], t)
}(this, function(e, t) {
    "use strict";
    return t.Wreqr = function(e, t, i) {
        var n = {};
        return n.Handlers = function(e, t) {
            var i = function(e) {
                this.options = e, this._wreqrHandlers = {}, t.isFunction(this.initialize) && this.initialize(e)
            };
            return i.extend = e.Model.extend, t.extend(i.prototype, e.Events, {
                setHandlers: function(e) {
                    t.each(e, function(e, i) {
                        var n = null;
                        t.isObject(e) && !t.isFunction(e) && (n = e.context, e = e.callback), this.setHandler(i, e, n)
                    }, this)
                },
                setHandler: function(e, t, i) {
                    var n = {
                        callback: t,
                        context: i
                    };
                    this._wreqrHandlers[e] = n, this.trigger("handler:add", e, t, i)
                },
                hasHandler: function(e) {
                    return !!this._wreqrHandlers[e]
                },
                getHandler: function(e) {
                    var t = this._wreqrHandlers[e];
                    if (t) return function() {
                        var e = Array.prototype.slice.apply(arguments);
                        return t.callback.apply(t.context, e)
                    }
                },
                removeHandler: function(e) {
                    delete this._wreqrHandlers[e]
                },
                removeAllHandlers: function() {
                    this._wreqrHandlers = {}
                }
            }), i
        }(e, i), n.CommandStorage = function() {
            var t = function(e) {
                this.options = e, this._commands = {}, i.isFunction(this.initialize) && this.initialize(e)
            };
            return i.extend(t.prototype, e.Events, {
                getCommands: function(e) {
                    var t = this._commands[e];
                    return t || (t = {
                        command: e,
                        instances: []
                    }, this._commands[e] = t), t
                },
                addCommand: function(e, t) {
                    var i = this.getCommands(e);
                    i.instances.push(t)
                },
                clearCommands: function(e) {
                    var t = this.getCommands(e);
                    t.instances = []
                }
            }), t
        }(), n.Commands = function(e) {
            return e.Handlers.extend({
                storageType: e.CommandStorage,
                constructor: function(t) {
                    this.options = t || {}, this._initializeStorage(this.options), this.on("handler:add", this._executeCommands, this);
                    var i = Array.prototype.slice.call(arguments);
                    e.Handlers.prototype.constructor.apply(this, i)
                },
                execute: function(e, t) {
                    e = arguments[0], t = Array.prototype.slice.call(arguments, 1), this.hasHandler(e) ? this.getHandler(e).apply(this, t) : this.storage.addCommand(e, t)
                },
                _executeCommands: function(e, t, n) {
                    var s = this.storage.getCommands(e);
                    i.each(s.instances, function(e) {
                        t.apply(n, e)
                    }), this.storage.clearCommands(e)
                },
                _initializeStorage: function(e) {
                    var t, n = e.storageType || this.storageType;
                    t = i.isFunction(n) ? new n : n, this.storage = t
                }
            })
        }(n), n.RequestResponse = function(e) {
            return e.Handlers.extend({
                request: function() {
                    var e = arguments[0],
                        t = Array.prototype.slice.call(arguments, 1);
                    return this.hasHandler(e) ? this.getHandler(e).apply(this, t) : void 0
                }
            })
        }(n), n.EventAggregator = function(e, t) {
            var i = function() {};
            return i.extend = e.Model.extend, t.extend(i.prototype, e.Events), i
        }(e, i), n
    }(t, t.Marionette, e), t.Wreqr
})