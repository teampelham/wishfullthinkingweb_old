// Place third -libparty dependencies in the lib folder
//
// Configure loading modules from the lib directory,
// except 'app' ones, 
requirejs.config({
    baseUrl: "scripts/lib",
    paths: {
      app: "../",
      base: "../base",
      commands: "../commands",
      desktop: "../desktop",
      templates: "../templates",
      "jquery-lib": "jquery-1.10.2",
      "backbone-lib": "backbone/backbone-1.1.2",
      "marionette-lib": "backbone/backbone.marionette-1.6.4",
      "backbone.wreqr": "backbone/backbone.wreqr-1.1.0",
      "backbone.babysitter": "backbone/backbone.babysitter-0.1.0",
      "backbone.obscura": "backbone/backbone.obscura-0.1.7",
      "handlebars-lib": "backbone/handlebars-1.1.2",
      x2js: "x2js-1.0.11",
       affix: "bootstrap/affix",
        alert: "bootstrap/alert",
        button: "bootstrap/button",
        carousel: "bootstrap/carousel",
        collapse: "bootstrap/collapse",
        dropdown: "bootstrap/dropdown",
        modal: "bootstrap/modal",
        popover: "bootstrap/popover",
        scrollspy: "bootstrap/scrollspy",
        tab: "bootstrap/tab",
        tooltip: "bootstrap/tooltip",
        transition: "bootstrap/transition"
    },
    "shim": {
        x2js: {
            exports: "X2JS"
        },
        hb: ["handlebars"],
        "handlebars-lib": {
            exports: "Handlebars"
        },
        "affix": ["jquery"],
        "alert": ["jquery"],
        "button": ["jquery"],
        "carousel": ["jquery"],
        "collapse": ["jquery"],
        "dropdown": ["jquery"],
        "modal": ["jquery"],
        "scrollspy": ["jquery"],
        "tab": ["jquery"],
        "tooltip": ["jquery"],
        "popover": ["jquery", "tooltip"],
        "transition": ["jquery"],
         "moment": {
            exports: "moment"
        },
    }
});

// Load the main app module to start the app
requirejs(["app/mobileMain"], function(m) {
});