define([
	"base/utils/baseClass", 
	"base/mixins/localizerMixin", 
	"base/adapters/settingsAdapter"
], function(baseClass, localizerMixin, settingsAdapter) {
    "use strict";
	
    var desktopLocalizer = baseClass.extend({
        mixins: localizerMixin,
        
        constructor: function() {
            this.basePath = "translations/";
            this.translations = {};
			
            settingsAdapter.getInstance().on("change:language", function(event, item) {
                this.load(item);
            }, this);
        }
    });
    
    return desktopLocalizer;
});