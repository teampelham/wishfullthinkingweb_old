define([
	"base/utils/browser", 
	"base/models/appModel"
], function(browser, appModel) {
    "use strict";
    
    var desktopDimensions = {
        "X-WF-Product": "WishFull Web",
        "X-WF-Version": appModel.get("version"),
        "X-WF-Client-Identifier": appModel.get("clientID"),
        "X-WF-Platform": browser.name,
        "X-WF-Platform-Version": browser.version,
        "X-WF-Device": browser.platform,
        "X-WF-Device-Name": "WishFull Web (" + browser.name + ")"
    };
    
    appModel.on("change:clientID", function(event, item) {
        desktopDimensions["X-WF-Client-Identifier"] = item;
    });
    
    appModel.on("change:version", function(event, item) {
        desktopDimensions["X-WF-Version"] = item;
    });
    
    return desktopDimensions;
});