define([
	"underscore", 
	//"raven", 
	"base/adapters/analytics/sentryTracker"
], function(_, sentryTracker) {
    "use strict";
	
    var desktopSentryTracker = _.extend({}, sentryTracker, {
        init: function(e, i) {
            /*t.config(e, {
                shouldSendCallback: function(e) {
                    return /plex\.js/.test(e.culprit)
                }
            }).install(), this.set("version", i)*/
        }
    });
    return desktopSentryTracker;
});