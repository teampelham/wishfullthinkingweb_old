define([
	"base/utils/dispatcher", 
	"base/utils/baseClass", 
	"desktop/views/modals/keyboardShortcutsModal"
], function(dispatcher, baseClass, KeyboardShortcutsModal) {
    "use strict";

    var desktopKeyboardShortcuts = baseClass.extend({
        shortcuts: [{
            keys: ["?"],
            fn: "help",
            group: "navigation",
            label: "Open Keyboard Shortcuts"
        }, 
        {
            keys: ["esc"],
            fn: "escape",
            group: "navigation",
            label: "Close Modal or Close Player or Navigate Back"
        }, 
        {
            keys: ["`"],
            fn: "sidebar",
            group: "navigation",
            label: "Toggle Sidebar"
        }, 
        {
            keys: ["g d"],
            fn: "dashboard",
            group: "navigation",
            label: "Navigate to Dashboard"
        }, 
        {
            keys: ["g s"],
            fn: "settings",
            group: "navigation",
            label: "Navigate to Settings"
        }, 
        {
            keys: ["g a"],
            fn: "activity",
            group: "navigation",
            label: "Navigate to Activity"
        }, 
        /*{
            keys: ["g w"],
            fn: "watchLater",
            group: "navigation",
            label: "Navigate to Watch Later"
        }, {
            keys: ["g r"],
            fn: "recommendations",
            group: "navigation",
            label: "Navigate to Recommended"
        }, {
            keys: ["g u"],
            fn: "users",
            group: "navigation",
            label: "Navigate to Users"
        }, {
            keys: ["/"],
            fn: "search",
            group: "actions",
            label: "Focus Search"
        }, {
            keys: ["p"],
            fn: "play",
            group: "actions",
            label: "Open Player"
        }, {
            keys: ["e"],
            fn: "edit",
            group: "actions",
            label: "Edit Library or Edit Item"
        }, {
            keys: ["#", "del"],
            fn: "remove",
            group: "actions",
            label: "Delete Library or Delete Item"
        }, {
            keys: ["shift+s"],
            fn: "save",
            group: "actions",
            label: "Save Changes"
        }, {
            keys: ["r"],
            fn: "refresh",
            group: "actions",
            label: "Update Library or Refresh Item"
        }, {
            keys: ["a"],
            fn: "analyze",
            group: "actions",
            label: "Analyze Library or Analyze Item"
        }, {
            keys: ["w"],
            fn: "markWatched",
            group: "actions",
            label: "Mark as Watched"
        }, {
            keys: ["u"],
            fn: "markUnwatched",
            group: "actions",
            label: "Mark as Unwatched"
        }, {
            keys: ["s"],
            fn: "sync",
            group: "actions",
            label: "Sync"
        }, {
            keys: ["i"],
            fn: "info",
            group: "actions",
            label: "Info"
        }, {
            keys: ["space"],
            fn: "playPause",
            group: "player",
            label: "Play or Pause"
        }, {
            keys: ["down"],
            fn: "decreaseVolume",
            group: "player",
            label: "Decrease Volume"
        }, {
            keys: ["up"],
            fn: "increaseVolume",
            group: "player",
            label: "Increase Volume"
        }, {
            keys: [",", "shift+left"],
            fn: "seekBackward",
            group: "player",
            label: "Seek Backward (15 seconds)"
        }, {
            keys: [".", "shift+right"],
            fn: "seekForward",
            group: "player",
            label: "Seek Forward (30 seconds)"
        }, {
            keys: ["[", "alt+left"],
            fn: "stepBackward",
            group: "player",
            label: "Step Backward (10 minutes)"
        }, {
            keys: ["]", "alt+right"],
            fn: "stepForward",
            group: "player",
            label: "Step Forward (10 minutes)"
        }, {
            keys: ["left"],
            fn: "skipPrevious",
            group: "player",
            label: "Skip to Previous Item"
        }, {
            keys: ["right"],
            fn: "skipNext",
            group: "player",
            label: "Skip to Next Item"
        }*/],
        help: function() {
            dispatcher.trigger("show:modal", new KeyboardShortcutsModal({
                shortcuts: this.shortcuts
            }));
        },
        
        escape: function() {
            dispatcher.trigger(dispatcher.hasListener("shortcut:escape") ? "shortcut:escape" : "navigate:back");
        },
        
        sidebar: function() {
            dispatcher.trigger("shortcut:sidebar");
        },
        
        dashboard: function() {
            dispatcher.trigger("navigate", "dashboard");
        },
        
        settings: function() {
            dispatcher.trigger("navigate", "settings");
        },
        
        activity: function() {
            dispatcher.trigger("navigate", "activity");
        },
        /*watchLater: function() {
            dispatcher.trigger("navigate", "userPlaylist", {
                key: "queue"
            })
        },
        recommendations: function() {
            dispatcher.trigger("navigate", "userPlaylist", {
                key: "recommendations"
            })
        },
        users: function() {
            dispatcher.trigger("navigate", "users")
        },
        search: function() {
            return dispatcher.trigger("shortcut:search"), !1
        },
        edit: function() {
            dispatcher.trigger("shortcut:edit")
        },
        play: function() {
            dispatcher.trigger("shortcut:play")
        },
        remove: function() {
            dispatcher.trigger("shortcut:delete")
        },
        save: function() {
            dispatcher.trigger("shortcut:save")
        },
        refresh: function() {
            dispatcher.trigger("shortcut:refresh")
        },
        analyze: function() {
            dispatcher.trigger("shortcut:analyze")
        },
        markWatched: function() {
            dispatcher.trigger("shortcut:markWatched")
        },
        markUnwatched: function() {
            dispatcher.trigger("shortcut:markUnwatched")
        },
        sync: function() {
            dispatcher.trigger("shortcut:sync")
        },
        info: function() {
            dispatcher.trigger("shortcut:info")
        },
        playPause: function() {
            return dispatcher.hasListener("shortcut:playPause") ? (dispatcher.trigger("shortcut:playPause"), !1) : void 0
        },
        decreaseVolume: function() {
            return dispatcher.hasListener("shortcut:decreaseVolume") ? (dispatcher.trigger("shortcut:decreaseVolume"), !1) : void 0
        },
        increaseVolume: function() {
            return dispatcher.hasListener("shortcut:increaseVolume") ? (dispatcher.trigger("shortcut:increaseVolume"), !1) : void 0
        },
        seekBackward: function() {
            dispatcher.trigger("shortcut:seekBackward")
        },
        seekForward: function() {
            dispatcher.trigger("shortcut:seekForward")
        },
        stepBackward: function() {
            dispatcher.trigger("shortcut:stepBackward")
        },
        stepForward: function() {
            dispatcher.trigger("shortcut:stepForward")
        },
        skipPrevious: function() {
            return dispatcher.hasListener("shortcut:skipPrevious") ? (dispatcher.trigger("shortcut:skipPrevious"), !1) : void 0
        },
        skipNext: function() {
            return dispatcher.hasListener("shortcut:skipNext") ? (dispatcher.trigger("shortcut:skipNext"), !1) : void 0
        }*/
    });
    
    return desktopKeyboardShortcuts;
});