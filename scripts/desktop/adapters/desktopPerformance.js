define([], function() {
    "use strict";
	
    var desktopPerformance = {		// wtf does this do???
        name: "desktop",
        taskThrottle: {
            wait: 10,
            batchSize: 2
        }
    };
	
    return desktopPerformance;
});