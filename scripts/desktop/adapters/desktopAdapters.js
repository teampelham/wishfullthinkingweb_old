define([
	"desktop/adapters/baseDesktopAdapters", 
	"base/adapters/storageAdapter", 
	"base/adapters/webStorage", 
	"base/adapters/dimensionsAdapter", 
	"desktop/adapters/desktopDimensions", 
	"base/adapters/analyticsAdapter", 
	"base/adapters/analytics/googleAnalytics", 
	"base/adapters/keyboardShortcutsAdapter", 
	"desktop/adapters/desktopKeyboardShortcuts", 
	//"base/adapters/playerProfileAdapter", 
	//"desktop/adapters/player/desktopPlayerProfile", 
	//"base/utils/playerUtil", 
	//"base/adapters/playerControllerAdapter", 
	//"desktop/adapters/player/DesktopVideoPlayerController", 
	//"desktop/adapters/player/DesktopMusicPlayerController"*/
], function(baseDesktopAdapters, storageAdapter, webStorage, dimensionsAdapter, desktopDimensions, analyticsAdapter, googleAnalytics, keyboardShortcutsAdapter, DesktopKeyboardShortcuts) {
    "use strict";
	
    var desktopAdapters = {
        registerAll: function() {
            storageAdapter.register(webStorage);
            dimensionsAdapter.register(desktopDimensions);
            analyticsAdapter.register(googleAnalytics);
            keyboardShortcutsAdapter.register(new DesktopKeyboardShortcuts());
            /* c.register(d), h.register({
                protocol: "local",
                type: u.types.VIDEO,
                create: function(e) {
                    return new p({
                        type: e
                    })
                }
            });
			h.register({
                protocol: "local",
                type: u.types.MUSIC,
                create: function(e) {
                    return new m({
                        type: e
                    })
                }
            }); 
			*/
            baseDesktopAdapters.registerAll();
        }
    };
	
    return desktopAdapters;
});