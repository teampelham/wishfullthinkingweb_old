define([
	"base/adapters/settingsAdapter",	
	"desktop/models/desktopSettingsModel",
	"base/adapters/localizerAdapter", 
	"desktop/adapters/desktopLocalizer", 
	"base/adapters/errorTrackerAdapter", 
	"desktop/adapters/analytics/desktopSentryTracker",   //uses some service called 'Raven' that I think needs subscriptions
	"base/adapters/performanceAdapter", 
	"desktop/adapters/desktopPerformance", 
	"base/adapters/loggerAdapter", 
	"base/utils/consoleLogger", 
	"base/adapters/metricsAdapter", 
	"base/adapters/analytics/wfMetrics", 
	//"base/utils/playerUtil", 
	//"base/adapters/playerControllerAdapter", 
	//"desktop/adapters/player/plexPlayerManager", 
	//"desktop/adapters/player/castPlayerManager", 
	//"base/adapters/player/photoPlayerController" 
], function(settingsAdapter, DesktopSettingsModel, localizerAdapter, DesktopLocalizer, errorTrackerAdapter, desktopSentryTracker, performanceAdapter, desktopPerformance, loggerAdapter, ConsoleLogger, metricsAdapter, wfMetrics) {
    "use strict";
    
	var baseDesktopAdapters = {
        registerAll: function() {
            settingsAdapter.register(new DesktopSettingsModel()); 
			localizerAdapter.register(new DesktopLocalizer()); 
			errorTrackerAdapter.register(desktopSentryTracker); 
			performanceAdapter.register(desktopPerformance);
			loggerAdapter.register(new ConsoleLogger());
			metricsAdapter.register(wfMetrics);
            /*
			var playerManager = new PlexPlayerManager();
            var castPlayerManager = new CastPlayerManager();
            
			playerControllerAdapter.register({
                protocol: "plex",
                type: "*",
                create: function(e, t) {
                    return playerManager.createController(e, t)
                }
            }); 
			playerControllerAdapter.register({
                protocol: "cast",
                type: "*",
                create: function(e, t) {
                    return castPlayerManager.createController(e, t)
                }
            }); 
			playerControllerAdapter.register({
                protocol: "local",
                type: playerUtil.types.PHOTO,
                create: function(e) {
                    return new PhotoPlayerController({
                        type: e
                    })
                }
            });*/
        }
    };
	
    return baseDesktopAdapters;
});