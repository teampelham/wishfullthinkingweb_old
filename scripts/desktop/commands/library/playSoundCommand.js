define([
	"underscore", 
	"soundmanager"
], function(_, soundManager) {
    "use strict";

    function destroySound() {
        if (sound) {
			sound.destruct(); 
			sound = null;
		}
    }

    function taperOffSound() {
        var e = setInterval(function() {
            if (sound) {
				sound.setVolume(sound.volume - 5); 
				if (sound.volume <= 0) {
					clearInterval(e); 
					destroySound();
				}
				return;
			} 
			return void clearInterval(e);
        }, 50);
    }

    function initializeSound(soundOptions) {
        soundOptions.onfinish = soundOptions.onfinish ? 
			_.wrap(soundOptions.onfinish, function(finished) {
            	destroySound();
				finished();
        	}) : destroySound; 
			
		soundOptions.id = soundOptions.url; 
		sound = soundManager.createSound(soundOptions); 
		sound.key = soundOptions.key; 
		sound.background = !! soundOptions.background; 
		
		if (soundOptions.oncreate) 
			soundOptions.oncreate(sound);
    }

    function createSound(soundOptions) {
        if (sound && soundOptions.background) {
            if (!sound.background) 
				return;
            if (sound.url === soundOptions.url) 
				return void(sound.key = soundOptions.key);
        }
		
        destroySound(); 
		
		if (soundManager.preferFlash && !soundManager.swfLoaded) {
			soundManager.preferFlash = false; 
			soundManager.onready(function() {
            	initializeSound(soundOptions)
        	}); 
			soundManager.reboot();
		} 
		else
			initializeSound(soundOptions);
    }

    function stopSound(soundOptions) {
		if (!sound)
			return;
			
        if (!soundOptions.key || soundOptions.key === l.key) {
			if (sound.background) 
				taperOffSound(); 
			else if (!soundOptions.background) 
				destroySound();
		}
    }

    function playSoundCommand(soundOptions) {
        soundOptions = soundOptions || {};
		 
		if (soundOptions.stop) 
			stopSound(soundOptions); 
		else
			createSound(soundOptions);
    }
	
    var sound;
	
    return playSoundCommand;
});