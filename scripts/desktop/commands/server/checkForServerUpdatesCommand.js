define([
	"underscore", 
	"jquery"
], function(_, $) {
    "use strict";

    function checkForServerUpdatesCommand(serverUpdater) {
        var server = serverUpdater.server;
        var downloadOptions = {
                download: serverUpdater.download === false ? "0" : "1"
            }; 
		var url = "/web/updatercheck?" + $.param(downloadOptions, true);
        var xhrOptions = _.extend(server.getXHROptions(url), {
                type: "PUT"
            });
        var state = serverUpdater.initialState || "checking";
        var updater = server.get("update");
        updater.set("state", state);
        var xhr = $.ajax(xhrOptions).promise();
        xhr.fail(function() {
            updater.set("state", "error")
        }); 
		
		return xhr;
    }
    return checkForServerUpdatesCommand;
})