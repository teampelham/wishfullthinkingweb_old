define([
	"base/utils/dispatcher", 
	"base/initializers/analyticsInitializer", 
	"base/adapters/analyticsAdapter"
], function(dispatcher, analyticsInitializer, analyticsAdapter) {
    "use strict";
	
    var desktopAnalyticsInitializer = {
        
        init: function() {
            analyticsInitializer.init();
            dispatcher.on("show:modal", function(modal) {
                if (modal.name)
                    analyticsAdapter.trackView(modal.name);
            });
        }
    };
	
    return desktopAnalyticsInitializer;
});