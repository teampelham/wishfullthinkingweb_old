define([
	"underscore", 
	"base/utils/dispatcher", 
	"base/adapters/settingsAdapter", 
	"base/models/appModel", 
	"base/models/server/serverStorageModel", 
	"desktop/views/modals/confirmModal", 
	"base/utils/t", 
	//"desktop/views/modals/user/userSelectModal"
], function(_, dispatcher, settingsAdapter, appModel, serverStorage, ConfirmModal, t) {
    "use strict";

    function onSignedIn(userModel, signedIn) {  // e, t
        if (!signedIn) {
            return;
        }

        /*userModel.get("devices").populate({
            reset: true
        });*/ 
        userModel.get("homeUsers").populate({
            reset: true
        }); 
        userModel.get("homeDetails").populate({
            reset: true
        }); 
        /*userModel.get("friends").populate({
            reset: true
        });
        userModel.get("invites").populate({
            reset: true
        });
        userModel.get("socialNetworks").populate({
            reset: true
        });*/
    }

    function doLogin(appModel, doLogin) {
        if(!doLogin)
            return;

        var modal = new ConfirmModal({
            title: t("Invalid Credentials"),
            message: t("Your credentials have expired. Please sign in again."),
            confirmLabel: t("OK"),
            cancelLabel: t("Ignore")
        });
        
        modal.promise.done(function() {
            dispatcher.trigger("navigate", "login", {
                silent: true
            });
        });
        dispatcher.trigger("show:modal", modal);
    }

    function onLocked(e, t) {
        if (!t)
            return;
        console.log('u');
        /*dispatcher.trigger("show:modal", new UserSelectModal({
            isForced: true
        }));*/
    }

    function onUserSelected(user) {
        if (user.get("signedIn"))
            serverStorage.updateUser(user);
    }

    function p() {
        console.log('p');
        //dispatcher.trigger("navigate:refresh:dashboard")
    }
    
    var userInitializer = {
        
        init: function() {
            var user = appModel.get("user");
            user.on({
                "change:signedIn": onSignedIn,
                "change:hasInvalidToken": doLogin,
                "change:locked": onLocked,
                "change:home": onUserSelected,
                "change:pin": onUserSelected
            }); 
            
            dispatcher.on("home:userswitched", p);
            var currentUser = serverStorage.getCurrentUser();
            
            currentUser.done(function(result) {
                if (result.home && !settingsAdapter.get("autoLogin"))
                    user.set("locked", true);
            });
        }
    };
    
    return userInitializer;
});
