define([
	"backbone", 
	"desktop/routes/desktopAppRouter", 
	"desktop/routes/setupRouter", 
	"desktop/routes/libraryRouter", 
	//"desktop/routes/channelRouter", 
	//"desktop/routes/playlistRouter", 
	//"desktop/routes/settingsRouter", 
	//"desktop/routes/activityRouter", 
	"desktop/routes/userRouter"
], function(bb, DesktopAppRouter, SetupRouter, LibraryRouter, UserRouter) {
    "use strict";
    
	var routerInitializer = {
        init: function() {
			new DesktopAppRouter();
			new SetupRouter();
			new UserRouter();
			new LibraryRouter();
            //new t, new i, new n, new s, new a, new r, new o, new l; 
			console.info('add more routes until complete');
			bb.history.start();	// this seems to kick off routing with the empty route (which ties to Dashboard)
        }
    };
	
    return routerInitializer;
});