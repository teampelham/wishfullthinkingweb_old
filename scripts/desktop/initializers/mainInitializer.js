define([
	"jquery", 
	"base/initializers/appInitializer", 
	"base/initializers/settingsInitializer", 
	"desktop/initializers/desktopAnalyticsInitializer", 
	"base/initializers/cloudServerEventInitializer", 
	"base/initializers/automatedDeviceInitializer", 
	"desktop/initializers/userInitializer", 
	"desktop/initializers/desktopServerInitializer", 
	"desktop/initializers/routerInitializer", 
	//"desktop/initializers/keyboardShortcutsInitializer", 
	"base/models/appModel", 
	"base/utils/sectionViewStateMigrator", 
	"transition", 
	"dropdown", 
	"tooltip", 
	"button", 
	"modal"
], function($, appInitializer, settingsInitializer, desktopAnalyticsInitializer, cloudServerEventInitializer, automatedDeviceInitializer, userInitializer, desktopServerInitializer, routerInitializer, appModel, sectionViewStateMigrator) {
    "use strict";
	
    var mainInitializer = function() {
		console.info('initializers and ui elements');
        var initContainer = [];
        initContainer.push(appInitializer.init(appModel));	// sets the clientId and version, initializes xhrTracking
		initContainer.push(settingsInitializer.init());
		// when the first two initializers resolve, then jQuery will fire off the next batch 
		$.when.apply($, initContainer).done(function() {
            desktopAnalyticsInitializer.init(); 		// Fire up the analytics, which does nothing currently
			cloudServerEventInitializer.init(); 		// Sets up listeners for cloud events via polls and sockets
			automatedDeviceInitializer.init(); 
			userInitializer.init(); 					// Wires up all the user-level event listeners
			desktopServerInitializer.init(); 
			routerInitializer.init(); 
			//keyboardShortcutsInitializer.init();
			sectionViewStateMigrator.migrate();
        });
    };
	
    return mainInitializer;
});