define([
	"base/initializers/serverInitializer", 
	"desktop/managers/serverUpdateManager", 
	"desktop/managers/desktopPrimaryServerManager", 
	"base/models/appModel"
], function(serverInitializer, ServerUpdateManager, DesktopPrimaryServerManager, appModel) {      // e, t, i, n
    "use strict";
    
    var desktopServerInitializer = {
        init: function() {
            var servers = appModel.get("servers");   // s
            var primaryServerManager = (new ServerUpdateManager({
                servers: servers
            }), new DesktopPrimaryServerManager({
                servers: servers
            }));
            
            serverInitializer.init({
                servers: servers,
                primaryServerManager: primaryServerManager
            });
        }
    };
    
    return desktopServerInitializer;
});