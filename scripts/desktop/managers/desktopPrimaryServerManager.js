define([
	"underscore", 
	"base/utils/dispatcher", 
	"base/models/appModel", 
	"base/managers/primaryServerManager"
], function(_, dispatcher, appModel, primaryServerManager) {
    "use strict";
    
    var desktopPrimaryServerManager = primaryServerManager.extend({
        
        fetchRelatedCollections: function(server) {
            primaryServerManager.prototype.fetchRelatedCollections.apply(this, arguments);
            var shared = server.get("shared");
            var home = server.get("home");
            var activeConnection = server.get("activeConnection");
            
            if (!shared) {
                var sessions = server.get("sessions");
                sessions.fetch({
                    reset: true
                });
            }
            
            var localConnection = activeConnection.isLocalAddress || !shared || home;
            if (localConnection) {
                var players = appModel.get("players");
                /*players.server = server; 
                players.fetch({
                    reset: true
                });*/
            }
        },
        
        onPrimaryServerSettingsReset: function(primary) {
            primaryServerManager.prototype.onPrimaryServerSettingsReset.apply(this, arguments);
            var shared = primary.server.get("shared");
            var accepted = primary.get("AcceptedEULA");
            
            if (!shared && accepted && accepted.get("value") === false) 
                dispatcher.trigger("command", "showTermsOfService", {
                    collection: primary
                });
        }
    });
    
    return desktopPrimaryServerManager;
});
