define([
	"underscore", 
	"base/utils/baseClass", 
	"base/utils/dispatcher", 
	"base/adapters/settingsAdapter", 
	"base/mixins/eventMixin", 
	"base/commands/commands", 
	"base/models/appModel", 
	"base/models/config"
], function(_, baseClass, dispatcher, settingsAdapter, eventMixin, commands, appModel, config) {
    "use strict";
    
    var serverUpdateManager = baseClass.extend({
        mixins: eventMixin,
        bundleUpdatesDelay: 36e5,
        
        initialize: function() {
            _.bindAll(this, "checkForBundleUpdates");
            this.listenTo(appModel, {
                "change:primaryServer": this.onPrimaryServerChange,
                "change:updateState": this.onWebUpdateStateChange
            });
        },
        
        isLastHistoryEntry: function() {
            var history = appModel.get("history");
            var lastEntry = _.last(history);
            return !(!lastEntry || !lastEntry.indexOf("fragment"));
        },
        
        isLastDismissedVersion: function(updater) {
            var version = updater.get("version") || "unknown";
            return version === settingsAdapter.get("serverUpdateDismissedVersion")
        },
        
        checkForServerUpdates: function(server) {
            var updater = server.get("update");
            
            if (server.supports("updater")) 
                updater.fetch().then(function() {
                    commands.execute("checkForServerUpdates", {
                        server: server,
                        download: false
                    })
                })
            else if (!this.isLastDismissedVersion(updater)) 
                dispatcher.trigger("show:bar", "alertbar", null);
        },
        
        checkForBundleUpdates: function() {
            console.log('checkForBundleUpdates');
            //a.execute("checkForBundleUpdates"), this.bundleUpdatesTimeout = setTimeout(this.checkForBundleUpdates, this.bundleUpdatesDelay)
        },
        
        onPrimaryServerChange: function(e, server) {
            var previousPrimary = appModel.previous("primaryServer");
            if (previousPrimary) 
                this.stopListening(dispatcher.get("update"));
                
            if(!server || server.get("shared"))
                return void clearTimeout(this.bundleUpdatesTimeout);
            else
                this.listenTo(server.get("update"), "change:state", this.onServerUpdateStateChange);
                this.checkForServerUpdates(server);
                return void(config.get("bundled") && !this.bundleUpdatesTimeout && (this.bundleUpdatesTimeout = setTimeout(this.checkForBundleUpdates, this.bundleUpdatesDelay)));
        },
        
        onWebUpdateStateChange: function(e, t) {
            console.log('onWebUpdateStateChange');
            //this.isLastHistoryEntry("/settings") || /^(available|downloaded)$/.test(t) && i.trigger("show:bar", "alertbar", null); //new c({ state: t }))
        },
        
        onServerUpdateStateChange: function(update, state) {
            if (this.isLastDismissedVersion(update) || this.isLastHistoryEntry("/settings/server") || /^(available|downloaded|notify)$/.test(state)) 
                dispatcher.trigger("show:bar", "alertbar", null); //new l({ model: e }))
        }
    });
    return serverUpdateManager;
});
