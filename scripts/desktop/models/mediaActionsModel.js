define([
	"underscore", 
	"base/utils/t", 
	"base/utils/contentUtil", 
	"base/utils/types/mediaTypes", 
	"base/models/baseModel", 
	"base/models/appModel"
], function(_, t, contentUtil, mediaTypes, baseModel, appModel) {
    "use strict";

    function hasUnviewedLeaves(item) { // r
        return item.get("unviewedLeafCount") > 0;
    }

    function hasOffsetViews(item) { // o
        var viewCount = item.get("viewCount");
        var viewOffset = item.get("viewOffset");
        
        return !(viewCount && !viewOffset);
    }

    function l(e) {
        if (!e || e.get("isFolder")) 
            return false; 
        
        return !mediaTypes.getMediaTypeByString(e.get("type")).child;
    }

    function hasNoUnviewedLeaves(item) {     // c
        return !hasUnviewedLeaves(item);
    }

    function hasNoOffsetViews(item) {     // d
        return !hasOffsetViews(item);
    }
    
    var mediaTypeEnum = {
        movie: {
            showAddToPlaylist: true,
            showAnalyze: true,
            showDelete: true,
            showDownload: true,
            showEdit: true,
            showFixMatch: true,
            showInfo: true,
            showMarkUnwatched: true,
            showMarkWatched: true,
            showMatch: true,
            showPlay: true,
            showPlayTrailer: true,
            showQueue: true,
            showRefresh: true,
            showSplitApart: true,
            showSync: true,
            showUnmatch: true
        },
        show: {
            showAddToPlaylist: true,
            showEdit: true,
            showDelete: true,
            showFixMatch: true,
            showMarkUnwatched: true,
            showMarkWatched: true,
            showMatch: true,
            showPlayAll: true,
            showPlayUnwatched: true,
            showQueue: true,
            showRefresh: true,
            showSelect: true,
            showShuffle: true,
            showSplitApart: true,
            showSync: true,
            showUnmatch: true
        },
        light: {
            showAddToPlaylist: true,
            showEdit: true,
            showDelete: true,
            showFixMatch: true,
            showMarkUnwatched: true,
            showMarkWatched: true,
            showMatch: true,
            showPlayAll: true,
            showPlayUnwatched: true,
            showQueue: true,
            showRefresh: true,
            showSelect: true,
            showShuffle: true,
            showSplitApart: true,
            showSync: true,
            showUnmatch: true
        },
        season: {
            showAddToPlaylist: true,
            showEdit: true,
            showDelete: true,
            showMarkUnwatched: true,
            showMarkWatched: true,
            showPlay: true,
            showQueue: true,
            showSelect: true,
            showShuffle: true,
            showSync: true
        },
        episode: {
            showAddToPlaylist: true,
            showAnalyze: true,
            showDelete: true,
            showDownload: true,
            showEdit: true,
            showInfo: true,
            showMarkUnwatched: true,
            showMarkWatched: true,
            showPlay: true,
            showQueue: true,
            showRefresh: true,
            showSync: true
        },
        artist: {
            showAddToPlaylist: true,
            showEdit: true,
            showDelete: true,
            showFixMatch: true,
            showMatch: true,
            showPlay: true,
            showQueue: true,
            showRefresh: true,
            showSelect: true,
            showShuffle: true,
            showSplitApart: true,
            showSync: true,
            showUnmatch: true
        },
        album: {
            showAddToPlaylist: true,
            showEdit: true,
            showDelete: true,
            showFixMatch: true,
            showMatch: true,
            showPlay: true,
            showQueue: true,
            showRefresh: true,
            showSelect: true,
            showShuffle: true,
            showSplitApart: true,
            showSync: true,
            showUnmatch: true
        },
        track: {
            showAddToPlaylist: true,
            showAnalyze: true,
            showDelete: true,
            showDownload: true,
            showEdit: true,
            showInfo: true,
            showPlayMix: true,
            showPlayMusicVideo: true,
            showQueue: true,
            showSelect: true,
            showSync: true
        },
        photoAlbum: {
            showAddToPlaylist: true,
            showEdit: true,
            showPlay: true,
            showShuffle: true,
            showSync: true
        },
        photo: {
            showAddToPlaylist: true,
            showAnalyze: true,
            showDelete: true,
            showDownload: true,
            showEdit: true,
            showInfo: true,
            showSync: true
        },
        clip: {
            showInfo: true,
            showPlay: true,
            showShare: true,
            showWatchLater: true
        },
        userPlaylistItem: {
            showDeleteFromUserPlaylist: true,
            showInfo: true,
            showMarkUnwatched: true,
            showMarkWatched: true,
            showPlay: true,
            showShare: true,
            showSync: true,
            showWatchLater: true
        },
        playlist: {
            showDelete: true,
            showEdit: true,
            showPlay: true,
            showShuffle: true
        },
        movieSection: {
            showAddToCollection: true,
            showAddToPlaylist: true,
            showAnalyze: true,
            showEdit: true,
            showMarkUnwatched: true,
            showMarkWatched: true,
            showMerge: true,
            showPlay: true,
            showRefresh: true,
            showSelect: true,
            showShuffle: true,
            showSync: true,
            showDelete: true
        },
        showSection: {
            showAddToCollection: true,
            showAddToPlaylist: true,
            showEdit: true,
            showMarkUnwatched: true,
            showMarkWatched: true,
            showMerge: true,
            showPlay: true,
            showRefresh: true,
            showSelect: true,
            showShuffle: true,
            showSync: true,
            showDelete: true
        },
        seasonSection: {
            showAddToPlaylist: true,
            showEdit: true,
            showMarkUnwatched: true,
            showMarkWatched: true,
            showMerge: true,
            showPlay: true,
            showRefresh: true,
            showSelect: true,
            showShuffle: true,
            showSync: true,
            showDelete: true
        },
        episodeSection: {
            showAddToPlaylist: true,
            showAnalyze: true,
            showMarkUnwatched: true,
            showMarkWatched: true,
            showPlay: true,
            showRefresh: true,
            showSelect: true,
            showShuffle: true,
            showSync: true,
            showDelete: true
        },
        artistSection: {
            showAddToCollection: true,
            showAddToPlaylist: true,
            showEdit: true,
            showMerge: true,
            showPlay: true,
            showQueue: true,
            showRefresh: true,
            showSelect: true,
            showShuffle: true,
            showSync: true,
            showDelete: true
        },
        albumSection: {
            showAddToPlaylist: true,
            showEdit: true,
            showMerge: true,
            showPlay: true,
            showQueue: true,
            showRefresh: true,
            showSelect: true,
            showShuffle: true,
            showSync: true,
            showDelete: true
        },
        trackSection: {
            showAddToPlaylist: true,
            showEdit: true,
            showPlay: true,
            showQueue: true,
            showRefresh: true,
            showSelect: true,
            showShuffle: true,
            showSync: true,
            showDelete: true
        },
        photoSection: {
            showAddToPlaylist: true,
            showAnalyze: true,
            showPlay: true,
            showSelect: true,
            showShuffle: true,
            showSync: true,
            showDelete: true
        }
    }; 
    
    var showAttributes = {
        showListDropdown: ["showShuffle", "showQueue", "showRefresh", "showSync", "showDownload", "showWatchLater", "showShare", "showInfo"]
    }; 
    
    var mediaActionsModel = baseModel.extend({
        initialize: function(e, initObj) {
            initObj = initObj || {}; 
            
            this.item = initObj.item; 
            this.parent = this.item || this.section; 
            this.showAdvanced = initObj.showAdvanced; 
            this.user = appModel.get("user"); 
            this.signedIn = this.user.get("signedIn"); 
            this.devices = this.user.get("devices"); 
            this.setDefaults(initObj.type); 
            this.listenToParent(); 
            this.listenTo(appModel, "change:primaryPlayer", this.onPrimaryPlayerChange); 
            this.listenTo(this.user, "change:signedIn", this.onSignedInChange); 
            this.listenTo(this.devices, "reset", this.onDevicesChange); 
            this.listenTo(this, {
                "change:selecting": this.onSelectingChange,
                "change:selectedItems": this.onSelectedItemsChange
            });
        },
        
        setDefaults: function(_type) {
            if (_type && _type !== this.type) {
                this.type = _type; 
                if (this.section) 
                    _type += "Section";
                        
                this.defaults = _.clone(mediaTypeEnum[_type] || {}); 
                this.defaults.selecting = false; 
                this.defaults.selectedItems = []; 
                this.clear().set(this.defaults);
            }
        },
        
        setParent: function(e) {
            if (this.parent) 
                this.stopListening(this.parent); 
            if (this.server) 
                this.stopListening(this.server); 
            if (this.playQueues) 
                this.stopListening(this.playQueues); 
            
            this.item = e.item; 
            this.section = e.section; 
            this.parent = this.item || this.section; 
            this.listenToParent();
        },
        
        listenToParent: function() {
            if (this.section) {
                var currentType = this.parent.get("currentType");
                if (currentType)
                    this.setDefaults(currentType.get("typeString"));
                
                this.listenTo(this.parent, {
                    "change:currentType": this.onCurrentTypeChange,
                    "change:allowSync": this.onAllowSyncChange
                })
            } 
            else if (this.item) {
                this.setDefaults(this.item.get("type")); 
                this.listenTo(this.parent, {
                    "change:primaryExtraKey": this.onPrimaryExtraChange,
                    "change:unviewedLeafCount": this.onViewStateChange,
                    "change:viewOffset": this.onViewStateChange,
                    "change:viewCount": this.onViewStateChange
                }); 
                if (this.showAdvanced) 
                    this.listenTo(this.parent, "change:guid", this.onGuidChange);
            }
            
            if (this.parent) {
                this.server = this.parent.server; 
                this.shared = this.server.get("shared"); 
                this.synced = this.server.get("synced"); 
                this.multiuser = this.server.get("multiuser"); 
                this.playQueues = this.server.get("playQueueManager"); 
                this.listenTo(this.server, "change:shared", this.onServerSharedChange); 
                //this.listenTo(this.playQueues, "change", this.onPlayQueuesChange);
            } 
            this.updateAll();
        },
        
        updateAll: function() {
            this.updateAction("showPlay", this.canPlay); 
            this.updateAction("showPlayAll", this.canPlay); 
            this.updateAction("showPlayUnwatched", this.canPlay); 
            this.updateAction("showPlayMix", this.canPlayMix); 
            this.updateAction("showPlayTrailer", this.canPlayPrimaryExtra); 
            this.updateAction("showPlayMusicVideo", this.canPlayPrimaryExtra); 
            this.updateAction("showShuffle", this.canPlay, {
                shuffle: true
            }); 
            this.updateAction("showQueue", this.isMedia); 
            this.updateAction("showEdit", this.canEdit); 
            this.updateAction("showRefresh", this.canRefresh); 
            this.updateAction("showSync", this.canSync); 
            this.updateAction("showDownload", this.canRefresh); 
            this.updateAction("showAddToPlaylist", this.canAddToPlaylist); 
            this.updateAction("showWatchLater", this.canWatchLater); 
            this.updateAction("showShare", this.canShare); 
            this.updateAction("showDelete", this.canDelete); 
            this.updateAction("showInfo", this.isMedia); 
            this.updateAction("showMarkWatched", this.canMarkWatched); 
            this.updateAction("showMarkUnwatched", this.canMarkUnwatched); 
            
            if (this.showAdvanced) {
                this.updateAction("showSelect", this.canMultiSelect); 
                this.updateAction("showAnalyze", this.canMultiAction); 
                this.updateAction("showMerge", this.canMultiAction); 
                this.updateAction("showAddToCollection", this.canMultiAction); 
                this.updateAction("showMatch", this.canMatch); 
                this.updateAction("showFixMatch", this.canFixMatch); 
                this.updateAction("showUnmatch", this.canUnmatch); 
                this.updateAction("showSplitApart", this.canSplitApart);
            } 
            
            this.updateCanQueue(); 
            this.updateDropdown("showListDropdown");
        },
        
        updateAction: function(property, fn, arg) {
            var propertyValue = this.defaults ? this.defaults[property] : false;
            
            this.set(propertyValue, fn.call(this, propertyValue, arg));
        },
        
        updateCanQueue: function() {
            var hasQueue = this.parent // && this.playQueues.getFor(this.parent);
            var canQueue = true;
            var player = appModel.get("primaryPlayer");
            
            if (player) {
                var capabilities = player.get("protocolCapabilities");
                canQueue = _(capabilities).contains("playqueues")
            }
            
            this.set("canQueue", !(!hasQueue || !canQueue))
        },
        
        updateDropdown: function(showAttr) {
            var attrs = showAttributes[showAttr];
            var existingAttrs = _.map(attrs, function(attr) {
                return this.get(attr);
            }, this);
                
            this.set(showAttr, _.some(existingAttrs));
        },
        
        canPlay: function(showPlay, arg) {
            if (showPlay) {
                if (this.parent && this.playQueues && !this.playQueues.canCreateFor(this.parent, arg)) 
                    return false;
                
                return !this.item || !this.item.get("isFolder") && !this.item.get("hasInvalidKey");
            }
            return false;
        },
        
        canPlayMix: function(mix) {
            if (!mix || !this.item) 
                return false;
            
            var related = this.item.get("related");
            
            return !(!related || !related.findWhere({
                browse: false
            }));
        },
        
        canPlayPrimaryExtra: function(ex) {
            if (ex && this.item) 
                return !! this.item.get("primaryExtraKey");
                 
            return false;
        },
        
        canAddToPlaylist: function(e) {
            if (!e || !this.server) return !1;
            var t, i = this.server.supports("playlists"),
                n = this.parent && this.parent.get("currentSubset");
            if (n) {
                var s = this.get("selectedItems");
                t = "all" === n.id || s && s.length > 0
            } else t = this.item && this.item.isLibraryItem();
            return i && t
        },
        
        canRefresh: function(t) {
            if (!t || this.synced) return !1;
            var i = this.getActionTargets(),
                n = _.every(i, function(e) {
                    var t = this.shared && e.get("isPlaylist"),
                        i = e.get("isSection"),
                        n = !this.shared && (i || e.isLibraryItem() && !e.get("isFolder"));
                    return t || n
                }, this);
            return n
        },
        
        canEdit: function(e) {
            if (!e || !this.canRefresh(e)) return !1;
            var t = this.getActionTargets();
            return t && t.length > 1 && !t[0].server.supports("batchEdit") ? !1 : true;
        },
        
        canMultiSelect: function(e) {
            return e ? !this.shared || this.multiuser : false;
        },
        
        canMultiAction: function(e) {
            return e ? this.item ? this.canEdit(e) : !this.shared && !this.synced && this.section : false;
        },
        
        canMarkWatched: function(e) {
            if (!e) return !1;
            if (!this.item && !this.section) return !1;
            if (!this.canMultiSelect(e) && !l(this.item)) return !1;
            var t = this.getActionTargets();
            if (!t) return !1;
            var i = t[0];
            return i.get("isSection") || i.get("isFolder") ? !1 : this.hasUnwatched();
        },
        
        canMarkUnwatched: function(e) {
            if (!e) return !1;
            if (!this.canMultiSelect(e) && !l(this.item)) return !1;
            var t = this.getActionTargets();
            if (!t) return !1;
            var i = t[0];
            return i.get("isSection") || i.get("isFolder") ? !1 : this.hasWatched();
        },
        
        canMatch: function(e) {
            return this.canEdit(e) && !this.canUnmatch(e);
        },
        
        canFixMatch: function(e) {
            return this.canEdit(e) && this.canUnmatch(e);
        },
        
        canUnmatch: function(e) {
            if (!e || !this.item || this.shared || this.synced) return !1;
            var t = this.item.get("guid");
            return !(!t || /(local|none)\:\/\//.test(t));
        },
        
        canSplitApart: function(e) {
            if (!e || !this.item || this.shared || this.synced) return !1;
            var t;
            return t = this.item.get("show" === this.type || "artist" === this.type || "album" === this.type ? "Location" : "Media"), t.length > 1 && this.server.supports("splitApart");
        },
        
        canSync: function(showSync) {
            if (!showSync || !this.parent || this.synced) 
                return false;
                
            var syncDevices = this.devices.filterSyncDevices(this.item);
            if (!syncDevices.length) 
                return false;
            
            var syncType = this.shared ? "sharedSync" : "sync";
            var supportsSync = this.server.supports(syncType);
            if (!supportsSync || this.parent.get("isFolder")) 
                return false;
            
            var allowSync;
            if (this.shared) {
                var section = this.section || this.item.section;
                allowSync = section && section.get("allowSync");
            } 
            else 
                allowSync = this.parent.get("allowSync") || this.parent.collection && this.parent.collection.allowSync;
            return !!allowSync;
        },
        
        canWatchLater: function(e) {
            if (!e || !this.item) return !1;
            var t = "video" === n.getChildMediaType(this.type).element,
                i = !! this.item.get("url"),
                s = !this.item.get("playlist") || "queue" !== this.item.get("playlist");
            return this.signedIn && this.user.get("queueUid") && t && i && s;
        },
        
        canShare: function(e) {
            return e && this.signedIn && !this.user.get("restricted");
        },
        
        canDelete: function(t) {
            if (!t) return !1;
            if (this.item && !this.item.get("isSection")) return this.canDeleteSingleItem(this.item);
            if (!this.canMultiSelect(t)) return !1;
            var i = this.getActionTargets();
            return i ? _.every(i, this.canDeleteSingleItem, this) : false;
        },
        
        canDeleteSingleItem: function(e) {
            if (!e || this.synced || e.get("isSection")) return !1;
            if (!l(e) && !e.server.supports("compositeDelete")) return !1;
            var t = !(!e.isLibraryItem() || !this.server.get("allowMediaDeletion") || this.shared),
                i = e.get("playlistType");
            return t || i;
        },
        
        isMedia: function(item) {
            if (!item) 
                return false;
            if (!this.item) 
                return true;
                
            var photo = "photo" === this.type && !this.item.isLibraryItem();
            
            return !photo && !this.item.get("isFolder");
        },
        
        shouldStartTrackingSelecting: function(e) {
            var t = this.canMultiSelect(this.get("showSelect")),
                i = e.metaKey || e.ctrlKey || e.shiftKey;
            return t && (i || this.get("selecting"));
        },
        
        selectItems: function(t, i) {
            if (0 !== t.length) {
                var n = this.get("selectedItems"),
                    s = _.bind(this.updateAll, this),
                    a = _.filter(t, function(t) {
                        return !_.contains(n, t)
                    });
                _.each(a, function(e) {
                    e.selected = true, n.push(e), this.listenTo(e, {
                        "change:viewCount": s,
                        "change:viewOffset": s,
                        "change:unviewedLeafCount": s,
                        "change:viewedLeafCount": s
                    })
                }, this);
                var r = this.sortSelectedItems(n, i);
                this.set("selectedItems", r), this.trigger("change:selectedItems", this, r)
            }
        },
        
        sortSelectedItems: function(t, i) {
            var n = i && i.superset;
            return n ? _.sortBy(t, function(e) {
                return n.indexOf(e)
            }) : _.clone(t)
        },
        
        deselectItems: function(t) {
            if (0 !== t.length) {
                var i = this.get("selectedItems");
                _.each(t, function(t) {
                    var n = e.indexOf(i, t); - 1 !== n && (t.selected = !1, i.splice(n, 1), this.stopListening(t))
                }, this), i.length || this.set("selecting", !1), this.trigger("change:selectedItems", this, i)
            }
        },
        
        selectItem: function(e, t) {
            this.selectItems([e], t)
        },
        
        deselectItem: function(e, t) {
            this.deselectItems([e], t)
        },
        
        deselectAll: function() {
            var t = this.get("selectedItems");
            t && 0 !== t.length && (_.each(t, function(e) {
                e.selected = !1
            }), t.length = 0, this.trigger("change:selectedItems", this, t));
        },
        
        getActionTargets: function() {
            var e = this.get("selectedItems");
            return e && e.length > 0 ? e : this.parent ? [this.parent] : void 0;
        },
        
        getActionTargetsTitle: function() {
            console.warn('');
            /*var e = this.getActionTargets();
            if (1 === e.length) {
                var i = e[0];
                return i.get("isSection") ? i.getSyncTitle() : i.get("title")
            }
            return 2 === e.length ? t("{1} and {2}", e[0].get("title"), e[1].get("title")) : 3 === e.length ? t("{1}, {2}, and {3}", e[0].get("title"), e[1].get("title"), e[2].get("title")) : t("{1}, {2}, and {3} others", e[0].get("title"), e[1].get("title"), e.length - 2)*/
        },
        
        contentUri: function(e) {
            console.warn('');
            //return i.uri(this.getActionTargets(), e)
        },
        
        hasWatched: function() {
            var actionTargets = this.getActionTargets();
            var actionType = actionTargets[0].get("type");
            var isTv = "show" === actionType || "season" === actionType;
            
            return _.some(actionTargets, isTv ? hasNoUnviewedLeaves : hasNoOffsetViews);
        },
        
        hasUnwatched: function() {
            var actionTargets = this.getActionTargets();
            var actionType = actionTargets[0].get("type");
            var isTv = "show" === actionType || "season" === actionType;
            
            return _.some(actionTargets, isTv ? hasUnviewedLeaves : hasOffsetViews);
        },
        
        hasSelectedItems: function() {
            console.warn('');
            /*var e = this.get("selectedItems");
            return e && e.length*/
        },
        
        onPrimaryPlayerChange: function() {
            console.warn('');
            //this.updateCanQueue()
        },
        
        onSignedInChange: function(e, t) {
            console.warn('');
            //this.signedIn = t, this.updateAction("showWatchLater", this.canWatchLater), this.updateAction("showShare", this.canShare), this.updateDropdown("showListDropdown")
        },
        
        onCurrentTypeChange: function(e, currentType) {
            this.deselectAll(); 
            
            if (currentType)
                this.setDefaults(currentType.get("typeString")); 
            
            this.updateAll();
        },
        
        onAllowSyncChange: function() {
            this.updateAction("showSync", this.canSync);
        },
        
        onPrimaryExtraChange: function() {
            console.warn('');
            //this.updateAction("showPlayTrailer", this.canPlayPrimaryExtra), this.updateAction("showPlayMusicVideo", this.canPlayPrimaryExtra)
        },
        
        onViewStateChange: function() {
            console.warn('');
            //this.updateAction("showMarkWatched", this.canMarkWatched), this.updateAction("showMarkUnwatched", this.canMarkUnwatched)
        },
        
        onGuidChange: function() {
            console.warn('');
            //this.updateAction("showUnmatch", this.canUnmatch), this.updateAction("showMatch", this.canMatch), this.updateAction("showFixMatch", this.canFixMatch)
        },
        
        onServerSharedChange: function(e, t) {
            console.warn('');
            //this.shared = t, this.updateAll()
        },
        
        onPlayQueuesChange: function() {
            console.warn('');
            //this.updateCanQueue()
        },
        
        onDevicesChange: function() {
            console.warn('');
            //this.updateAction("showSync", this.canSync)
        },
        
        onSelectingChange: function(e, t) {
            console.warn('');
            //t || this.deselectAll()
        },
        
        onSelectedItemsChange: function(e, t) {
            console.warn('');
            //this.set("selecting", !(!t || !t.length)), this.updateAll()
        }
    });
    
    return mediaActionsModel;
});