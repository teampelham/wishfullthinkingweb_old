define([
	"underscore", 
	"base/utils/browser", 
	//"base/utils/constants/subtitlesBurnLevels", 
	//"base/utils/constants/httpFallbackOptions", 
	"base/models/abstractSettingsModel"
], function(_, browser, abstractSettingsModel) {
    "use strict";

    var desktopSettingsModel = abstractSettingsModel.extend({
        storageKey: "settingsv2",
        //plexPassSettings: ["syncVideoQuality", "syncAudioQuality", "syncPictureQuality"],
        defaults: _.extend({}, abstractSettingsModel.prototype.defaults, {
            //playThemeMusic: !1,
            keyboardShortcuts: true,
            dashboardPriorityOnDeck: 1,
            sidebarOpen: true,
            dashboardOnDeckEnabled: true,
            /*showUnwatchedIcons: !1,
            showAdvancedSettings: !1,
            allowHttpFallback: n.NEVER,
            dashboardPriorityRecentlyAdded: 2,
            dashboardPriorityChannels: 3,
            dashboardPriorityQueue: 4,
            dashboardPriorityRecommendations: 5,
            dashboardRecentlyAddedEnabled: !0,
            dashboardChannelsEnabled: !0,
            dashboardQueueEnabled: !0,
            dashboardRecommendationsEnabled: !0,
            sharedSearch: !0,
            channelSearch: !0,
            localQuality: -1,
            remoteQuality: 8,
            channelQuality: -1,
            dash: !0,
            directPlay: !0,
            directStream: !0,
            autoResolution: !0,
            preferAAC: "wiiu" === t.id,
            allowAC3: !1,
            subtitleSize: 100,
            subtitlesBurnLevel: i.ALWAYS,
            audioBoost: 100,
            syncVideoQuality: "medium",
            syncAudioQuality: "medium",
            syncPictureQuality: "medium",
            videoPlayerVolume: 100,
            musicPlayerVolume: 100,
            announcementsLastReadTimestamp: 0,
            serverUpdateDismissedVersion: "",
            hasDismissedPremiumMusicAlert: !1,
            listType: "media-tile-list",
            "listType!track": "media-list"*/
        }),
		
        getListTypeKey: function(key) {
            var listType = "listType";
            if  (key) 
                listType += "!" + key.get("typeString"); 
            
            return listType;
        },
        
        getListType: function(listType) {
            var mediaType, mediaTypeKey;
            listType = listType || {}; 
            
            if (_.has(listType, "mediaType") && _.isObject(listType.mediaType)) 
                mediaTypeKey = this.getListTypeKey(listType.mediaType); 
            
            mediaType = this.get(mediaTypeKey) || this.get(listType.fallbackKey) || this.get("listType");
            return mediaType;
        },
        
        saveListType: function(mediaType, listType) {
            var mediaTypeKey = this.getListTypeKey(mediaType);
            this.save(mediaTypeKey, listType);
            
            if (!_.isEqual(mediaTypeKey, "listType")) 
                this.trigger("change:listType", this, listType);
        },
        
        getSectionListType: function(section, mediaType) {
            var sectionKey;
            if (section) {
                mediaType = mediaType || section.get("currentType"); 
                sectionKey = section.getScopedKey("listType"); 
            }
            
            return this.getListType({
                mediaType: mediaType,
                fallbackKey: sectionKey
            });
        }
    });
	
    return desktopSettingsModel;
});