define([
	"backbone",  
	"base/routes/baseAppRouter", 
	"desktop/routes/desktopAppController", 
	"base/utils/dispatcher"
], function(bb, baseAppRouter, DesktopAppController, dispatcher) {
    "use strict";
	
    var desktopRouter = baseAppRouter.extend({
        
        initialize: function() {
            baseAppRouter.prototype.initialize.apply(this, arguments);
            this.listenTo(dispatcher, "navigate:refresh:dashboard", this.onNavigateRefreshDashboard);
        },
        
        controller: new DesktopAppController(),
        appRoutes: {
            "!/dashboard": "dashboard",
            "": "dashboard",
            "!/login": "login",
            "!/unauthorized": "unauthorized",
            "!/logs": "logs",
            "!/info": "info",
            "*404": "error"
        },
        
        onNavigateRefreshDashboard: function() {
            console.log('finish onNavigateRefreshDashboard');
            //var fragment = bb.history.getFragment();
            
            //if ("" === fragment || "!/dashboard" === fragment)
            //    bb.history.loadUrl("");
            //else
            //    dispatcher.trigger("navigate", "dashboard")
        }
    });
    return desktopRouter;
});