define([
	"base/routes/baseRouter", 
	"desktop/routes/userController"
], function(baseRouter, UserController) {
    "use strict";
    
    var userRouter = baseRouter.extend({
        controller: new UserController(),
        appRoutes: {
            "!/playlist/queue/help": "queueHelp",
            "!/playlist/:key(/:subset)": "userPlaylist",
            "!/playlist/:key/details/:itemID": "userPlaylistDetails",
            "!/announcements": "announcements"
        }
    });
    
    return userRouter;
});