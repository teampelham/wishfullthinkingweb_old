define([
	"base/utils/dispatcher", 
	"base/routes/baseController"
], function(dispatcher, baseController) {
    "use strict";
	
    var setupController = baseController.extend({      
        setupBasic: function(t) {
            console.log('setupBasic');
            /*var n;
            n = t ? {
                serverID: t
            } : {
                primaryServer: !0
            };
            var s = this.populateView(n);
            s.done(function(t) {
                e.trigger("show:view", new i({
                    model: t.server,
                    subset: "setupBasic"
                }))
            })*/
        },
        
        setupLibrary: function() {
            console.log('setupLibrary');
            /*var t = this.populateView({
                primaryServer: !0
            });
            t.done(function(t) {
                e.trigger("show:view", new i({
                    model: t.server,
                    subset: "setupLibrary"
                }))
            })*/
        },
        
        setupChannels: function() {
            console.log('setupChannels');
            /*var t = this.populateView({
                primaryServer: !0
            });
            t.done(function(t) {
                e.trigger("show:view", new i({
                    model: t.server,
                    subset: "setupChannels"
                }))
            })*/
        },
        
        setupComplete: function() {
            console.log('setupComplete');
            /*var t = this.populateView({
                primaryServer: !0
            });
            t.done(function(t) {
                e.trigger("show:view", new i({
                    model: t.server,
                    subset: "setupComplete"
                }))
            })*/
        }
    });
    
    return setupController;
});