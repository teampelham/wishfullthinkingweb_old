define([
	"base/utils/dispatcher", 
	"base/models/appModel", 
	"base/commands/commands", 
	"base/routes/baseAppController", 
	"desktop/views/desktopAppView", 
	"desktop/views/dashboard/dashboardView", 
	"desktop/views/signInView", 
	//"desktop/views/logsView", 
	//"desktop/views/infoView", 
	//"desktop/views/error/channelErrorView", 
	//"desktop/views/error/errorView", 
	"desktop/views/error/notFoundView", 
	//"desktop/views/error/serverRequiredView", 
	//"desktop/views/error/unauthorizedView", 
	"desktop/views/error/unavailableView"
], function(dispatcher, appModel, commands, baseAppController, DesktopAppView, DashboardView, signInView, notFoundView, unavailableView) {
    "use strict";
	
    var desktopAppController = baseAppController.extend({
        viewsByType: {
            signIn: signInView,
            //error: d,
            //channelError: c,
            notFound: notFoundView,
            //serverRequired: h,
            //unauthorized: p,
            unavailable: unavailableView
        },
        
        initialize: function() {
            baseAppController.prototype.initialize.apply(this, arguments);
            this.appView = new DesktopAppView();
            this.appView.render();
        },
        
        dashboard: function() {
			var defer = this.populateView();
            
            defer.done(function() {
                dispatcher.trigger("show:view", new DashboardView())
            });
        },
        
        logout: function() {
            console.log('desktopAppController. - logout');
            /*var t = this.viewFor;
            commands.execute("signOut").done(function() {
                dispatcher.trigger("navigate", "login", {
                    silent: true
                });
            }).fail(function() {
                dispatcher.trigger("show:view", t("logoutError"))
            });*/
        },
        
        logs: function() {
            console.log('desktopAppController. - logs');
            //dispatcher.trigger("show:view", new o({
            //    collection: appModel.get("logs")
            //}));
        },
        
        info: function() {
            console.log('Info view triggered!');
            //dispatcher.trigger("show:view", new infoView());
        }
    });
	
    return desktopAppController;
});