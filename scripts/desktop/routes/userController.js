define([
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/adapters/settingsAdapter", 
	"base/routes/baseController", 
	"base/models/appModel"
], function(t, dispatcher, settingsAdapter, baseController, appModel) {
    "use strict";
    
	var userController = baseController.extend({
        
        userPlaylist: function(i, n) {
            console.log('userPlaylist');
            /*var a = this.populateView({
                serverID: "myPlex"
            });
            a.done(function() {
                var a = s.get("user"),
                    l = new r([], {
                        server: a.server,
                        parent: a,
                        key: i
                    }),
                    c = l.fetch({
                        scope: "page"
                    });
                c.done(function() {
                    t.trigger("show:view", new o({
                        model: s.get("user"),
                        collection: l,
                        subset: n
                    }))
                }), c.fail(function() {
                    t.trigger("show:view", new d({
                        title: e("This playlist is unavailable.")
                    }))
                })
            })*/
        },
        
        userPlaylistDetails: function(i, n) {
            console.log('userPlaylistDetails');
            /*var r = this.populateView({
                serverID: "myPlex"
            });
            r.done(function(r) {
                var o = s.get("user"),
                    l = new a({
                        id: n
                    }, {
                        server: r.server,
                        parent: o,
                        playlist: i
                    }),
                    u = l.fetch({
                        scope: "page"
                    });
                u.done(function() {
                    t.trigger("show:view", new c({
                        model: l
                    }))
                }), u.fail(function() {
                    t.trigger("show:view", new d({
                        title: e("This playlist item is unavailable.")
                    }))
                })
            })*/
        },
        
        queueHelp: function() {
            console.log('queueHelp');
            /*var e = this.populateView({
                serverID: "myPlex"
            });
            e.done(function() {
                var e = s.get("user");
                t.trigger("show:view", new u({
                    queueEmail: e.get("queueEmail")
                }))
            })*/
        },
        
        announcements: function() {
            console.log('announcements');
            /*var n = this.populateView();
            n.done(function() {
                var n = s.get("announcements"),
                    a = n.fetch({
                        scope: "page"
                    });
                a.done(function() {
                    t.trigger("show:view", new l({
                        collection: n
                    })), i.save("announcementsLastReadTimestamp", (new Date).getTime())
                }), a.fail(function() {
                    t.trigger("show:view", new d({
                        title: e("Announcements are not available.")
                    }))
                })
            })*/
        }
    });
    
    return userController;
});