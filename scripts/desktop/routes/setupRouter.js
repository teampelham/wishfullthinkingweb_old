define([
	"base/routes/baseRouter", 
	"desktop/routes/setupController"
], function(baseRouter, SetupController) {
    "use strict";

    var setupRouter = baseRouter.extend({
        controller: new SetupController(),
        
        appRoutes: {
            "!/setup(/:serverID)": "setupBasic",
            "!/setup/library": "setupLibrary",
            "!/setup/channels": "setupChannels",
            "!/setup/complete": "setupComplete"
        }
    });
    
    return setupRouter;
});