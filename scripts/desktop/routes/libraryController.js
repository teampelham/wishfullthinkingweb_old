define([
	"base/utils/dispatcher", 
	"base/routes/baseController", 
	//"desktop/views/searchView", 
	"desktop/views/section/sectionView", 
	"desktop/views/details/detailsView"
], function(dispatcher, baseController, SectionView, DetailsView) {
    "use strict";
	
    var libraryController = baseController.extend({
        
		search: function(t) {
			console.warn('routeMe');
            /*var n = this.populateView({
                primaryServer: !0
            });
            n.done(function() {
                e.trigger("show:view", new i({
                    query: t
                }))
            })*/
        },
		
        section: function(serverID, sectionID, subset) {
            var populatePromise = this.populateView({
                serverID: serverID,
                sectionID: sectionID
            });
            populatePromise.done(function(result) {
                var sectionView = new SectionView({
                    model: result.section,
                    subset: subset
                });
                dispatcher.trigger("show:view", sectionView);
            })
        },
		
        sectionFolder: function(t, i, s) {
			console.warn('routeMe');
            /*var a = this.populateView({
                serverID: t,
                sectionID: i
            });
            a.done(function(t) {
                var i = t.section.get("items");
                i.scrollPosition = 0, e.trigger("show:view", new n({
                    model: t.section,
                    subset: "folder",
                    folderKey: s
                }))
            })*/
        },
		
        sectionFilter: function(t, i, s, a, r, o) {
			console.warn('routeMe');
            /*var l = this.populateView({
                serverID: t,
                sectionID: i
            });
            l.done(function(t) {
                var i = t.section.get("currentFilters");
                i.length = 0, o || (o = r), i.push({
                    key: a,
                    values: r.split(","),
                    titles: o.split(",")
                }), e.trigger("show:view", new n({
                    model: t.section,
                    type: parseInt(s, 10)
                }))
            })*/
        },
		
        sectionType: function(t, i, s, a, r) {
			console.warn('routeMe');
            /*var o = this.populateView({
                serverID: t,
                sectionID: i
            });
            o.done(function(t) {
                e.trigger("show:view", new n({
                    model: t.section,
                    type: s,
                    sortKey: a,
                    sortDirection: r
                }))
            })*/
        },
		
        details: function(serverID, sectionID, key) {
            var populatePromise = this.populateView({
                serverID: serverID,
                sectionID: sectionID,
                key: key
            });
            populatePromise.done(function(result) {
                dispatcher.trigger("show:view", new DetailsView({
                    model: result.item
                }))
            })
        }
    });
	
    return libraryController;
});