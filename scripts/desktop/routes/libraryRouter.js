define([
	"base/routes/baseRouter", 
	"desktop/routes/libraryController"
], function(baseRouter, LibraryController) {
    "use strict";
    
	var libraryRouter = baseRouter.extend({
        controller: new LibraryController(),
        appRoutes: {
            "!/search/:query": "search",
            "!/server/:serverID/section/:sectionID(/:subset)": "section",
            "!/server/:serverID/section/:sectionID/folder/:key": "sectionFolder",
            "!/server/:serverID/section/:sectionID/filter/:type/:key/:value(/:title)": "sectionFilter",
            "!/server/:serverID/section/:sectionID/type/:type/:sortKey/:sortDirection": "sectionType",
            "!/server/:serverID/section/:sectionID/details/:key": "details"
        }
    });
	
    return libraryRouter;
});