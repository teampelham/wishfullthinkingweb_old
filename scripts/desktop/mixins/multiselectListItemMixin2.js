define(["underscore"], function(e) {
    "use strict";
    var t = {
        triggerAutoselect: function(e) {
            this.trigger("autoselect", {
                shiftKey: e.shiftKey
            })
        },
        stopTrackingSelecting: function(e, t) {
            return this.isTrackingSelecting ? (e.preventDefault(), void(this.isTrackingSelecting = !1)) : void(t && t.call(this, e))
        },
        onMouseEnter: function(e) {
            this.parentMediaActions && this.parentMediaActions.get("selecting") && this.triggerAutoselect(e)
        },
        onMouseLeave: function(e) {
            this.stopTrackingSelecting(e)
        },
        onMouseDown: function(t) {
            if (this.parentMediaActions && this.parentMediaActions.shouldStartTrackingSelecting(t)) {
                var i = e.bind(function() {
                    this.ignoreOnClick = !0
                }, this);
                this.listenToOnce(this.parentMediaActions, "change:selectedItems", i), this.isTrackingSelecting = !0, this.trigger("select", {
                    multiSelect: t.shiftKey
                }), this.stopListening(this.parentMediaActions, "change:selectedItems", i)
            }
        },
        onClick: function(e) {
            this.ignoreOnClick && (this.ignoreOnClick = !1, e.preventDefault())
        }
    };
    return t
});