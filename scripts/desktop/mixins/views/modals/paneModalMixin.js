define(["underscore"], function(_) {
    "use strict";

    function validationErrors(paneModel, pane) {
        var validate = pane.constructor.prototype.validate;
        return validate ? validate.call(null, paneModel) : [];
    }

    function isValid(paneKey) {
        var pane = this.panes[paneKey];
        var paneModel = getPaneModel.call(this, pane);
        return 0 === validationErrors.call(this, paneModel, pane).length
    }

    function allDependents(memo, key) {
        return memo.concat(this.panes[key].dependentPanes || []);
    }

    function onValidChanged() {
        var changeBtn = this.$(".change-pane-btn");
        
        changeBtn.addClass("disabled").removeClass("selected"); 
        
        _.each(this.getReachablePanes(), function(pane) {
            changeBtn.filter("." + pane + "-btn").removeClass("disabled");
        }, this); 
        
        changeBtn.filter("." + this.selectedPane + "-btn").addClass("selected")
    }

    function getPaneModel(pane) {
        return this[pane.model] || this[this.panesModel] || this.model;
    }
    
    var paneModalMixin = {
        showPane: function(pane) {
            
            if (pane !== this.selectedPane) {
                var currentView = this.paneRegion.currentView;
                if (currentView) {
                    this.stopListening(currentView, "valid", onValidChanged); 
                    this.stopListening(currentView, "invalid", onValidChanged); 
                    this.$el.removeClass("pane-" + this.selectedPane); 
                    if (this.onPaneHide) 
                        this.onPaneHide(this.selectedPane, currentView);
                } 
                this.selectedPane = pane;
                var paneObj = this.panes[pane];
                var PaneConstructor = paneObj.constructor;
                var arg = this.paneConstructorArg ? this.paneConstructorArg() : {}; 
                var newPane = new PaneConstructor(_.extend(arg, {
                    model: getPaneModel.call(this, paneObj)
                }));
                
                if (newPane.onModalClose) {
                    if (!this._paneOnModalCloseCallbacks) {
                        this.listenToOnce(this, "close", function() {
                            _.each(this._paneOnModalCloseCallbacks, function(cb) {
                                cb[1].call(cb[0]);
                            }, this); 
                            this._paneOnModalCloseCallbacks = null;     
                        });
                    }
                    var closeCallbacks = this._paneOnModalCloseCallbacks || [];
                    closeCallbacks.push([newPane, newPane.onModalClose]); 
                    this._paneOnModalCloseCallbacks = _.uniq(closeCallbacks, function(cb) {
                        return cb[1];
                    });
                }
                
                this.listenTo(newPane, "valid", onValidChanged); 
                this.listenTo(newPane, "invalid", onValidChanged); 
                onValidChanged.call(this); 
                
                if (this.onPaneShow) 
                    this.onPaneShow(pane, newPane); 
                
                this.$el.addClass("pane-" + pane); 
                this.paneRegion.show(newPane); 
                onValidChanged.call(this);
            }
        },
        
        getReachablePanes: function() {
            var validKeys, hasInvalid; 
            var allPanes = _.reduce(this.panes, function(memo, pane) {
                return _.union(memo, pane.dependentPanes || []);
            }, []); 
            var paneKeys = _.filter(_.keys(this.panes), function(pane) {
                return !_.contains(allPanes, pane);
            });
            
            for (var keys = paneKeys; keys.length;) {
                validKeys = _.filter(keys, isValid, this); 
                hasInvalid = hasInvalid || validKeys.length !== keys.length; 
                keys = _.reduce(validKeys, allDependents, [], this); 
                paneKeys = paneKeys.concat(keys);
            }
            
            paneKeys.isAnyInvalid = hasInvalid;
            return paneKeys;
        },
        
        onChangePaneClick: function(e) {
            e.preventDefault();
            
            var target = this.$(e.currentTarget);
            if (!target.isDisabled()) 
                this.showPane(target.data("pane"));
        }
    };
	
    return paneModalMixin;
});