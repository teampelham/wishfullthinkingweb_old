define(["underscore"], function(_) {
    "use strict";
	
    var multiSelectListItemMixin = {
        triggerAutoselect: function(e) {
            this.trigger("autoselect", {
                shiftKey: e.shiftKey
            });
        },
        
		stopTrackingSelecting: function(e, t) {
            if (this.isTrackingSelecting) {
				e.preventDefault();
				return void(this.isTrackingSelecting = false);  
			}
			
			if (t)
				t.call(this, e);
        },
		
        onMouseEnter: function(e) {
            this.parentMediaActions && this.parentMediaActions.get("selecting") && this.triggerAutoselect(e);
        },
		
        onMouseLeave: function(e) {
            this.stopTrackingSelecting(e);
        },
		
        onMouseDown: function(t) {
            if (this.parentMediaActions && this.parentMediaActions.shouldStartTrackingSelecting(t)) {
                var beginIgnore = _.bind(function() {
                    this.ignoreOnClick = true
                }, this);
				
                this.listenToOnce(this.parentMediaActions, "change:selectedItems", beginIgnore);
				this.isTrackingSelecting = true; 
				this.trigger("select", {
                    multiSelect: t.shiftKey
                }); 
				this.stopListening(this.parentMediaActions, "change:selectedItems", beginIgnore);
            }
        },
		
        onClick: function(e) {
            if (this.ignoreOnClick) {
				this.ignoreOnClick = false; 
				e.preventDefault();
			}
        }
    };
	
    return multiSelectListItemMixin;
});