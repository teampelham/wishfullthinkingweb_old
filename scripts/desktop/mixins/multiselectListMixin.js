define([
	"underscore", 
	"marionette", 
	"base/utils/rangeUtil"
], function(_, marionette, rangeUtil) {
    "use strict";
	
    var multiSelectMixin = {
        selectable: true,
        events: {
            mousedown: "onMouseDown",
            mouseup: "onMouseUp",
            mouseleave: "onMouseUp"
        },
        
        itemEvents: {
            select: "onItemSelect",
            autoselect: "onItemAutoselect"
        },
        
        itemViewOptions: function() {
            return {
                mediaActions: this.parentMediaActions || this.mediaActions
            };
        },
        
        initialize: function(section) {
            if (section.mediaActions) {
                this.mediaActions = section.mediaActions;
                this.listenTo(this.mediaActions, {
                "change:selectedItems": this.onMediaActionsSelectedItemsChange,
                "change:selecting": this.onMediaActionsSelectingChange
                }); 
            
                this.shiftRange = {
                    start: -1,
                    end: -1
                };
            }
        },
        
        isViewSelected: function(view) {
            var selectedItems = this.mediaActions.get("selectedItems");
            if (0 === selectedItems.length) 
                return false;
                
            var model = view.model;
            var index = _.indexOf(selectedItems, model);
            if (-1 !== index) 
                return true;
                
            var children = model.get("children");
            var allSelected = children && children.length && children.every(function(child) {
                return child.selected;
            });
            
            return allSelected ? true : false;
        },
        
        singleSelect: function(view) {
            this.shiftRange.end = -1;
            var model = view.model;
            
            if (this.isViewSelected(view)) {
                this.shiftRange.start = -1; 
                this.deselectItem(model); 
                view.deselect();
            } 
            else {
                this.shiftRange.start = this.collection.indexOf(model); 
                this.previousCollectionOffset = this.collection.populatedRange ? this.collection.populatedRange.start : 0; 
                this.selectItem(model); 
                view.select();
            }
        },
        
        multiSelect: function(view) {
            if (this.getViewForModel) {
                var modelIndex = this.collection.indexOf(view.model);
                var rangeStart = this.collection.populatedRange.start;
                var previousStart = this.previousCollectionOffset - rangeStart;
                var selectList = [];
                var deselectList = [];
                this.previousCollectionOffset = rangeStart;
                
                if (this.shiftRange.start < 0) { 
                    this.shiftRange.start = modelIndex; 
                    selectList.push(view.model); 
                    view.select();
                }
                else if (this.shiftRange.end < 0) { 
                    this.shiftRange.start += previousStart; 
                    this.shiftRange.end = modelIndex; 
                    rangeUtil.each(this.shiftRange, function(idx) {
                        var model = this.collection.at(idx);
                        if (model) {
                            selectList.push(model);
                            var view = this.getViewForModel(model);
                            if (view) 
                                view.select();
                        }
                    }, this);
                }
                else {
                    rangeUtil.each(this.shiftRange, function(idx) {
                        var model = this.collection.at(idx);
                        if (model) 
                            deselectList.push(model);
                    }, this); 
                    
                    this.shiftRange.start += previousStart; 
                    this.shiftRange.end = modelIndex; 
                    rangeUtil.each(this.shiftRange, function(idx) {
                        var model = this.collection.at(idx);
                        if (model) {
                            selectList.push(model);
                            var view = this.getViewForModel(model);
                            if (view) 
                                view.select();
                        }
                    }, this);
                    
                    var fullList = [deselectList].concat(selectList);
                    deselectList = _.without.apply(_, fullList); 
                    _.each(deselectList, function(listItem) {
                        var view = this.getViewForModel(listItem);
                        if (view)
                            view.deselect();
                    }, this);
                }
                
                this.mediaActions.selectItems(selectList, {
                    superset: this.collection
                }); 
                
                this.mediaActions.deselectItems(deselectList, {
                    superset: this.collection
                });
            }
        },
        
        selectItem: function(item) {
            this.mediaActions.selectItem(item, {
                superset: this.collection
            });
        },
        
        deselectItem: function(item) {
            this.mediaActions.deselectItem(item, {
                superset: this.collection
            });
        },
        
        forwardItemViewEvents: function(itemView) {
            this.stopListening(itemView, "all"); 
            this.listenTo(itemView, "all", function(n) {
                var args = Array.prototype.slice.call(arguments);
                args[0] = "itemview:" + n; 
                args.splice(1, 0, itemView);
                var normalMethods = marionette.normalizeMethods.call(this, this.itemEvents);
                if ("undefined" != typeof normalMethods && _.isFunction(normalMethods[n])) 
                    normalMethods[n].apply(this, args); 
                    
                marionette.triggerMethod.apply(this, args);
            }, this);
        },
        
        onMediaActionsSelectedItemsChange: function(e, t) {
            if (this.getViewForModel) {
                if (!(t || t.length)) {
                    this.shiftRange.start = -1; 
                    this.shiftRange.end = -1; 
                    this.collection.each(function(e) {
                        var t = this.getViewForModel(e);
                        t && t.deselect && t.deselect()
                    }, this);
                }
            }
        },
        
        onMediaActionsSelectingChange: function(e) {
            this.$el.toggleClass("hide-actions", e);
        },
        
        onClose: function() {
            if (this.mediaActions)
                this.mediaActions.deselectAll();
        },
        
        onMouseDown: function() {
            this.isMouseDown = true;
        },
        
        onMouseUp: function() {
            this.isMouseDown = false;
        },
        
        onItemSelect: function(e, t, i) {
            if (this.selectable) {
                i = i || {}; 
                if (i.multiSelect) 
                    this.multiSelect(t);
                else
                    this.singleSelect(t);
            }
        },
        
        onItemAutoselect: function() {
            if (this.isMouseDown) 
                this.onItemSelect.apply(this, arguments);
        },
        
        onItemRemoved: function(e) {
            if (e.model.selected) {
                this.deselectItem(e.model); 
                e.deselect();
            }
        }
    };
    
    return multiSelectMixin;
})