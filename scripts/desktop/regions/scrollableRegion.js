define([
	"underscore", 
	"jquery", 
	"marionette", 
	"base/utils/dispatcher"
], function(_, $, marionette, dispatcher) {
    "use strict";
	
    var scrollableRegion = marionette.Region.extend({
        resizeSubscribersCallbacks: [],
        scrollSubscribersCallbacks: [],
        sidebarDelay: 200,
        scrollDelay: 100,
        
        initialize: function(initObj) {
            _.bindAll(this, "onResize", "onScroll", "onScrollEvent", "onSidebarChange");
			
            this.$el = $(this.el);
            this.channelName = initObj.channelName;
            
			var eventArray = {};
            eventArray[this.channelName + ".subscribe:resize"] = this.onResizeSubscribe;
            eventArray[this.channelName + ".unsubscribe:resize"] = this.onResizeUnsubscribe;
            eventArray[this.channelName + ".subscribe:scroll"] = this.onScrollSubscribe; 
            eventArray[this.channelName + ".unsubscribe:scroll"] = this.onScrollUnsubscribe;
            
			this.listenTo(dispatcher, eventArray);
            this.updateSize();
        },
        
        updateSize: function() {
            this.size = {
                width: this.$el.width(),
                height: this.$el.height(),
                innerWidth: this.$el.innerWidth(),
                innerHeight: this.$el.innerHeight(),
                outerWidth: this.$el.outerWidth(),
                outerHeight: this.$el.outerHeight(),
                offset: this.$el.offset()
            }
        },
        
        updateScrollPosition: function() {
            this.scrollPosition = {
                scrollHeight: this.$el[0].scrollHeight,
                scrollTop: this.$el[0].scrollTop
            }
        },
        
        onShow: function() {
            this.updateSize();
            this.updateScrollPosition();
        },

        onResizeSubscribe: function(callback) {
            this.resizeSubscribersCallbacks.push(callback); 
            if (1 === this.resizeSubscribersCallbacks.length) {
                this.updateSize(); 
                this.listenTo(dispatcher, {
                    resize: this.onResize,
                    "show:bar hide:bar": this.onSidebarChange
                });
            } 
            callback(this.size);
        },
        
        onResizeUnsubscribe: function(callback) {
            this.resizeSubscribersCallbacks.splice(_.indexOf(this.resizeSubscribersCallbacks, callback), 1); 
            
            if (0 === this.resizeSubscribersCallbacks.length) 
                this.stopListening(dispatcher, "resize show:bar hide:bar");
        },
        
        onScrollSubscribe: function(callback) {
            this.scrollSubscribersCallbacks.push(callback); 
            
            if (1 === this.scrollSubscribersCallbacks.length) {
                this.updateScrollPosition(); 
                this.$el.on("scroll", this.onScroll);
            } 
            callback(this.scrollPosition);
        },
        
        onScrollUnsubscribe: function(t) {
            this.scrollSubscribersCallbacks.splice(_.indexOf(this.scrollSubscribersCallbacks, t), 1); 
            if (0 === this.scrollSubscribersCallbacks.length) 
                this.$el.off("scroll", this.onScroll);
        },
        
        onResize: function() {
            this.updateSize(), 
            _.each(this.resizeSubscribersCallbacks, function(cb) {
                cb(this.size);
            }, this)
        },
        
        onScroll: function() {
            this.scrollTimeout = clearTimeout(this.scrollTimeout); 
            this.scrollTimeout = setTimeout(this.onScrollEvent, this.scrollDelay);
        },
        
        onScrollEvent: function() {
            this.updateScrollPosition(); 
            _.each(this.scrollSubscribersCallbacks, function(e) {
                e(this.scrollPosition)
            }, this);
        },
        
        onSidebarChange: function(view) {
            if ("sidebar" === view) 
                _.delay(this.onResize, this.sidebarDelay);
        }
    });
    return scrollableRegion;
});