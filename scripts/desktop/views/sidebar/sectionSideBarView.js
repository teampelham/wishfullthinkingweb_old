define([
	"templates/desktop/sidebar/sectionSideBarView.tpl", 
	"underscore",
	"marionette", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/adapters/settingsAdapter", 
	"base/models/appModel", 
	"desktop/views/filterbar/sectionFilterBarView", 
	"desktop/views/lists/library/section/currentFilterList", 
	"desktop/views/lists/library/section/filterList", 
	//"desktop/views/lists/library/section/FilterOptionList", 
	"desktop/views/lists/library/section/sortList"
], function(template, _, marionette, t, dispatcher, settingsAdapter, appModel, SectionFilterBarView, CurrentFilterList, FilterList, SortList) {
    "use strict";
	
    var sectionSideBarView = marionette.Layout.extend({
        className: "side-bar",
        template: template,
        
		ui: {
            newFilterBtn: ".new-filter-icon-btn",
            filterOptionsHeader: ".filter-options-header",
            typeContainer: ".section-type-container",
            sortContainer: ".section-sort-container"
        },
        
		regions: {
            currentFilters: ".section-current-filters-list-container",
            filters: ".section-filters-list-container",
            filterOptions: ".section-filter-option-list-container",
            sort: ".section-sort-list-container"
        },
        
		modelEvents: {
            "change:currentFilters": "onCurrentFiltersChange",
            "change:currentType": "onCurrentTypeChange",
            "change:title": "onSectionTitleChange"
        },
        
		events: {
            "click .new-filter-btn": "onNewFilterClick",
            "click .hide-btn": "onHideClick"
        },
        
		initialize: function(initObj) {
            this.hidden = !settingsAdapter.get("sidebarOpen"); 
			this.advancedFilters = !! this.model.get("allowAdvancedFilters"); 
			this.linearFilters = !this.advancedFilters; 
			this.mediaActions = initObj.mediaActions; 
			
			if (this.hidden) 
				this.$el.addClass("transition-out"); 
			else
				dispatcher.trigger("show:bar", "sidebar"); 
			
			appModel.set({
                searchType: "section",
                searchPlaceholder: this.model.get("title"),
                searchValue: this.getSearchValue()
            }); 
			
			this.listenTo(dispatcher, "search:section", this.onSearch);
        },
		
        serializeData: function() {
            return {
                showNewFilterIconBtn: this.advancedFilters && !! this.model.get("currentFilters").length
            };
        },
		
        open: function() {
            this.hidden = false; 
			settingsAdapter.save("sidebarOpen", true); 
			this.$el.removeClass("transition-out"); 
			dispatcher.trigger("show:bar", "sidebar");
        },
		
        hide: function() {
            this.hidden = true; 
			settingsAdapter.save("sidebarOpen", false); 
			this.$el.addClass("transition-out"); 
			dispatcher.trigger("hide:bar", "sidebar");
        },
		
        getSearchValue: function() {
            var currentFilters = this.model.get("currentFilters");
            var titleFilters = _.findWhere(currentFilters, {
                key: "title"
            });
            return titleFilters && titleFilters.values.length ? titleFilters.values[0] : null;
        },
		
        addFilter: function(e, i) {
			console.warn('');
            /*i || (i = {});
            var n = this.model.get("currentFilters");
            this.linearFilters && n.length && (n.length = 0);
            var s = t.findWhere(n, {
                key: e.key
            });
            if (s) {
                if (t.indexOf(s.values, e.value) > -1) return;
                i.replace ? (s.titles = [e.title], s.values = [e.value]) : (s.titles.push(e.title), s.values.push(e.value))
            } else n.push({
                key: e.key,
                titles: [e.title],
                values: [e.value]
            });
            this.model.trigger("change:currentFilters", this.model, n)*/
        },
		
        removeFilter: function(e) {
			console.warn('');
            /*var i = this.model.get("currentFilters"),
                n = t.findWhere(i, {
                    key: e.key
                });
            n && (n.titles = t.reject(n.titles, function(t) {
                return t === e.title
            }), n.values = t.reject(n.values, function(t) {
                return t === e.value
            }), n.values.length || i.splice(t.indexOf(i, n), 1)), this.model.trigger("change:currentFilters", this.model, i)*/
        },
		
        removeAllFilters: function(e) {
			console.warn('');
            /*var i = this.model.get("currentFilters"),
                n = t.findWhere(i, {
                    key: e
                });
            i.splice(t.indexOf(i, n), 1), this.model.trigger("change:currentFilters", this.model, i)*/
        },
		
        onRender: function() {
            var isAll = "all" !== this.model.get("currentSubset").id;
            var currentFilters = this.model.get("currentFilters");
            
            if (isAll) 
                currentFilters.length = 0;
            
            var currentFilterList = new CurrentFilterList({
                model: this.model,
                disabled: isAll
            });
            
            var sortList = new SortList({
                collection: this.model.get("sorts"),
                disabled: isAll
            });
            
            this.listenTo(currentFilterList, "removeAllFilters", this.onRemoveAllFilters); 
            this.listenToOnce(sortList, "collection:rendered", this.onSortListRender); 
            this.currentFilters.show(currentFilterList); 
            this.sort.show(sortList); 
            
            if (!isAll) {
                var filters = this.model.get("filters").populate({
                    reset: true,
                    scope: "page"
                });
                
                filters.done(_.bind(function(result) {
                    if (!this.isClosed) {
                        var collection = this.linearFilters ? result.filterBySecondary() : result;
                        var filterList = new FilterList({
                            collection: collection,
                            disabled: isAll
                        });
                        
                        this.listenTo(filterList, "itemview:addFilter", this.onAddFilter); 
                        this.listenTo(filterList, "itemview:removeFilter", this.onRemoveFilter); 
                        this.listenTo(filterList, "itemview:showOptions", this.onFilterListShowOptions); 
                        this.filters.show(filterList);
                    }
                }, this));
            }
            
            if (currentFilters.length) 
                dispatcher.trigger("show:bar", "filterbar", new SectionFilterBarView({
                    model: this.model
                }));
        },
		
        onClose: function() {
            appModel.set({
                searchType: null,
                searchPlaceholder: null
            });
            
            dispatcher.trigger("hide:bar", "sidebar"); 
            dispatcher.trigger("hide:bar", "filterbar");
        },
		
        onCurrentFiltersChange: function(e, filters) {
            var hasFilters = !! filters.length;
            this.ui.newFilterBtn.toggleClass("hidden", this.linearFilters || !hasFilters); 
            appModel.set("searchValue", this.getSearchValue()); 
            
            if (hasFilters) 
                dispatcher.trigger("show:bar", "filterbar", new SectionFilterBarView({
                    model: this.model
                })); 
            else 
                dispatcher.trigger("hide:bar", "filterbar");
        },
		
        onCurrentTypeChange: function() {
            this.$el.removeClass("show-filters show-filter-options");
			this.removeAllFilters();
        },
		
        onSectionTitleChange: function(e, placeholder) {
            appModel.set("searchPlaceholder", placeholder);
        },
		
        onSearch: function(e) {		
			console.warn('');
            /*return e ? void this.addFilter({
                key: "title",
                value: e,
                title: e
            }, {
                replace: !0
            }) : void this.removeAllFilters("title")*/
        },
		
        onAddFilter: function(e, filter) {
            this.addFilter(filter);
        },
		
        onRemoveFilter: function(e, filter) {
            this.removeFilter(filter);
        },
		
        onRemoveAllFilters: function(scope) {
            this.removeAllFilters(scope);
        },
        
		onFilterListShowOptions: function(e, t) {
			console.warn('');
            /*var i = n(t.get("title"));
            this.$el.addClass("show-filter-options"), this.$el.removeClass("show-filters"), this.ui.filterOptionsHeader.text(i);
            var s = new d({
                collection: t.get("options")
            });
            this.listenTo(s, "addFilter", this.onAddFilter), this.listenTo(s, "removeAllFilters", this.onRemoveAllFilters), this.listenTo(s, "itemview:addFilter", this.onAddFilter), this.listenTo(s, "itemview:removeFilter", this.onRemoveFilter), this.filterOptions.show(s)*/
        },
		
        onSortListRender: function(list) {
			this.ui.sortContainer.toggleClass("hidden", !list.collection.length);
            var listLength = list.collection.length;
            
            if (!listLength) 
                this.listenToOnce(list, "before:item:added", this.onSortListItemAdded);
        },
		
        onSortListItemAdded: function() {
            this.ui.sortContainer.removeClass("hidden");
        },
		
        onNewFilterClick: function(e) {
            e.preventDefault(); 
			this.$el.addClass("show-filters")
        },
		
        onHideClick: function(e) {
            e.preventDefault(); 
			if (this.$el.hasClass("show-filter-options")) {
				this.$el.removeClass("show-filter-options"); 
				this.$el.addClass("show-filters"); 	
			}
			else
				this.$el.removeClass("show-filters");
        }
    });
	
    return sectionSideBarView;
});