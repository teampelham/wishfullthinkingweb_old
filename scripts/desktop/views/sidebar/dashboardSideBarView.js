define([
    "templates/desktop/sidebar/dashboardSideBarView.tpl", 
    "underscore", 
    "marionette", 
    "base/utils/dispatcher", 
    "base/utils/constants/httpFallbackOptions", 
    "base/adapters/settingsAdapter", 
    "base/models/appModel", 
    "base/models/server/serverCollection", 
    "base/commands/commands", 
    "desktop/views/dashboard/dashboardEmptyServersView", 
    "desktop/views/lists/dashboard/dashboardServerList"
], function(template, _, marionette, dispatcher, httpFallbackOptions, settingsAdapter, appModel, ServerCollection, commands, DashboardEmptyServersView, DashboardServerList) {
    "use strict";
	
    var dashboardSideBarView = marionette.Layout.extend({
        className: "side-bar dark-scrollbar",
        template: template,
        regions: {
            empty: ".dashboard-empty-servers-container",
            servers: ".dashboard-servers-container"
        },
        
        initialize: function() {
            var servers = appModel.get("servers");
            var authorizedServers = servers.filter(this.isAuthorized);
            var unavailOwn = servers.filter(this.isUnavailableOwned);
            var sharedUnavail = servers.filter(this.isSharedPresentAndUnavailable);
            var goodBadUgly = authorizedServers.concat(unavailOwn, sharedUnavail);
            
            this.collection = new ServerCollection(goodBadUgly); 
            this.listenTo(appModel, "change:primaryServer", this.onPrimaryServerChange); 
            this.listenTo(servers, "remove", this.onServerRemove); 
            this.listenTo(servers, "change:connected", this.onServerConnectedChange); 
            this.listenTo(servers, "change:presence", this.onServerPresenceChange)
        },
        
        showEmptyView: function() {
            if (!this.collection.length && appModel.get("primaryServer") === false) {
                var servers = appModel.get("servers");
                var sharedServerCount = servers.where({
                    cloud: false,
                    shared: true
                }).length;
                var isPartialUser = ! appModel.get("user").isFullUser();
                
                this.empty.show(new DashboardEmptyServersView({
                    hasSharedServers: sharedServerCount,
                    isManagedUser: isPartialUser
                }));
            }
        },
        
        isAuthorized: function(server) {
            if (server.id && !server.get("cloud")) 
                return server.get("authorized");
            return false;
        },
        
        isUnavailableOwned: function(server) {
            var sharedOrSynced = server.get("shared") || server.get("synced");
            return server.id && !server.get("cloud") && !sharedOrSynced && !server.get("available");
        },
        
        isSharedPresentAndUnavailable: function(server) {
            var fallback = settingsAdapter.get("allowHttpFallback");
            var fallbackSameNetwork = server.get("publicAddressMatches") && fallback === httpFallbackOptions.SAME_NETWORK;
            var shouldFallback = fallback === httpFallbackOptions.ALWAYS || fallbackSameNetwork;
            
            return server.get("shared") && server.get("presence") && !server.get("available") && !shouldFallback;
        },
        
        onRender: function() {
            this.showEmptyView(); 
            this.servers.show(new DashboardServerList({
                collection: this.collection
            }));
        },
        
        onPrimaryServerChange: function() {
            this.showEmptyView();
        },
        
        onServerRemove: function(e) {
            console.warn('init');
            //this.collection.remove(e.cid), this.showEmptyView()
        },
        
        onServerConnectedChange: function(server) {
            if (this.isAuthorized(server) || this.isUnavailableOwned(server) || this.isSharedPresentAndUnavailable(server)) 
                this.collection.add(server); 
            else
                this.collection.remove(server);
        },
        
        onServerPresenceChange: function(server) {
            this.isSharedPresentAndUnavailable(server) 
                this.collection.add(server);
        }
    });
	
    return dashboardSideBarView;
});