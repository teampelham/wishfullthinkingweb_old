define([
	"underscore", 
	"jquery", 
	"marionette", 
	"base/utils/dispatcher"
], function(_, $, marionette, dispatcher) {
    "use strict";
    
	var s = 0;
    var a = [];
    var baseModal = marionette.Layout.extend({
        className: "modal fade",
        focusDelay: 200,
        animationDelay: 500,
        events: {
            "hide.bs.modal": "onHide"
        },
		
        initialize: function() {
            this.once("render", function() {
                s += 1;
                dispatcher.trigger("anyModalOpen");
            });
            
            this.once("close", function() {
                s -= 1;
                if (0 === s)
                    dispatcher.trigger("allModalsClosed");
            })
        },
        
        hide: function() {
            this.isClosed || this.isHidden || this.$el.modal("hide");
        },
        
        onRender: function() {
            if (!this.rerender) {
                if (this.isForced)
                    this.$el.modal({
                        backdrop: "static",
                        keyboard: false
                    });
                else {
                    this.listenTo(dispatcher, "shortcut:escape", _.bind(function() {
                        if (a[0] === this)
                            this.hide();
                    }, this)); 
                    
                    this.listenToOnce(this, "close", function() {
                        var e = a.indexOf(this); 
                        if (-1 !== e)
                            a.splice(e, 1);
                    }); 
                    a.unshift(this); 
                    this.$el.modal();
                } 
            
                $(":focus").blur();
            }
        },
        
        onHide: function() {
            this.isHidden = true;
            _.each(this.panes, function(pane) {
                var modalClose = pane.constructor.onModalClose;
                if (modalClose)
                    modalClose.apply(this);
            }, this);
            
            setTimeout(_.bind(function() {
                dispatcher.trigger("hide:modal", this);
                this.trigger("close");
            }, this), this.animationDelay);
        }
    });
    
    return baseModal;
});