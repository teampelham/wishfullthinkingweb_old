define([
	"templates/desktop/modals/keyboardShortcutsModal.tpl", 
	"underscore", 
	"desktop/views/modals/baseModal"
], function(template, _, baseModal) {
    "use strict";

    /*function n(e) {
        return t.reduce(e, function(e, t, i) {
            var n = s(t);
            if (0 === i) return e.concat(n);
            var a = n.length > 1;
            return e.concat({
                plain: !0,
                split: a,
                s: "or"
            }, n)
        }, [])
    }

    function s(e) {
        var i = e.match(/(.+) (.+)/);
        if (i) return s(i[1]).concat({
            plain: !0,
            s: "then"
        }).concat(s(i[2]));
        var n = e.match(/(.+)\+(\S+)/);
        if (n) return s(n[1]).concat({
            plain: !0,
            s: "+"
        }).concat(s(n[2]));
        var a = t.reduce([
            [/command/, "\u2318"],
            [/up/, "\u2191"],
            [/down/, "\u2193"],
            [/left/, "\u2190"],
            [/right/, "\u2192"]
        ], function(e, t) {
            return e.replace(t[0], t[1])
        }, e);
        return [{
            s: a
        }]
    }*/
    var keyboardShortcutsModal = baseModal.extend({
        name: "keyboardShortcutsModal",
        className: "keyboard-shortcuts-modal modal fade",
        template: template,
        
		/*orderedGroups: [{
            group: "navigation",
            label: "Navigation"
        }, {
            group: "actions",
            label: "Actions"
        }, {
            group: "player",
            label: "Player"
        }],*/
		
        initialize: function(e) {
            baseModal.prototype.initialize.apply(this, arguments), this.shortcuts = e.shortcuts
        },
		
        serializeData: function() {
			console.warn("serialize");
            /*var e = {};
            t.each(this.shortcuts, function(t) {
                var i = t.group;
                e[i] || (e[i] = []), e[i].push(this.humanizeShortcut(t))
            }, this);
            var i = t.map(this.orderedGroups, function(i) {
                var n = t.clone(i);
                return n.shortcuts = e[i.group], n
            }, this);
            return {
                groups: i
            }*/
        },
		
        humanizeShortcut: function(e) {
			console.warn("humanize");
            //return e = t.clone(e), e.keys = n(e.keys), e
        }
    });
	
    return keyboardShortcutsModal;
});