define([
	"templates/desktop/modals/confirmModal.tpl", 
	"underscore", 
	"jquery", 
	"desktop/views/modals/baseModal"
], function(template, _, $, baseModal) {
    "use strict";
	
    var confirmModal = baseModal.extend({
        className: "confirm-modal modal modal-sm fade",
        template: template,
        
		ui: {
            doubleConfirmMessage: ".double-confirm-message",
            confirmBtn: ".confirm-btn"
        },
        
		events: _.extend({
            "click .confirm-btn": "onConfirm"
        }, baseModal.prototype.events),
		
        whitelistedOptions: ["title", "message", "confirmLabel", "cancelLabel", "hasDanger", "hideCancel"],
        
		initialize: function(initObj) {
            baseModal.prototype.initialize.apply(this, arguments); 
			this.async = initObj.async; 
			this.data = _.pick(initObj, this.whitelistedOptions); 
			this.promise = $.Deferred();
        },
		
        serializeData: function() {
            return _.defaults(this.data, {
                confirmLabel: "Yes",
                cancelLabel: "No"
            });
        },
		
        onRender: function() {
            baseModal.prototype.onRender.apply(this, arguments); 
			setTimeout(_.bind(function() {
                this.ui.confirmBtn.focus();
            }, this), this.focusDelay);
        },
		
        onHide: function() {
            baseModal.prototype.onHide.apply(this, arguments); 
			this.promise.reject();
        },
		
        onConfirm: function(e) {
			e.preventDefault();
			
            if (this.options.requireDoubleConfirm && !this.hasConfirmedOnce) {
                this.hasConfirmedOnce = true;
                var doubleMessage = this.options.doubleConfirmMessage;
                var doubleLabel = this.options.doubleConfirmLabel;
                
				if (doubleMessage) 
					this.ui.doubleConfirmMessage.text(doubleMessage);
				
				if (doubleLabel) 
					this.ui.confirmBtn.text(doubleLabel);
					
				return void 0;
            }
			
            this.promise.resolve(); 
			if (this.async) 
				this.ui.confirmBtn.addClass("active");
			else
				this.hide();
        }
    });
	
    return confirmModal;
});