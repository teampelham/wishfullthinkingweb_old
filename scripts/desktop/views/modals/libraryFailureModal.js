define([
	"templates/desktop/modals/libraryFailureModal.tpl", 
	"underscore", 
	"desktop/views/modals/baseModal"
], function(e, t, i) {
    "use strict";
    var n = i.extend({
        name: "libraryFailureModal",
        className: "confirm-modal modal modal-sm fade",
        template: e,
        events: t.extend({
            "click .confirm-btn": "hide"
        }, i.prototype.events),
        serializeData: function() {
            return {
                serverID: this.model.id,
                isSignedIn: "ok" === this.model.get("myPlexSigninState"),
                hasPlexPass: this.model.get("myPlexSubscription")
            }
        }
    });
    return n;
});