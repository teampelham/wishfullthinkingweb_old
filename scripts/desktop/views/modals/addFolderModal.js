define([
	"templates/desktop/modals/addFolderModal.tpl", 
	"underscore", 
	"base/models/server/browseDirectoryCollection", 
	"desktop/views/modals/baseModal", 
	"desktop/views/lists/server/browseDirectoryList"
], function(e, t, i, n, s) {
    "use strict";
    var a = n.extend({
        name: "addFolderModal",
        className: "add-folder-modal modal fade",
        template: e,
        events: t.extend({
            "submit form": "onSubmitForm"
        }, n.prototype.events),
        ui: {
            path: 'input[name="path"]'
        },
        regions: {
            home: ".home-directory-container",
            path: ".path-container"
        },
        initialize: function(e) {
            n.prototype.initialize.apply(this, arguments), this.server = e.server, this.homeCollection = new i([], {
                server: this.server
            }), this.pathCollection = new i([], {
                server: this.server
            }), this.listenTo(this.homeCollection, "reset", this.onHomeReset), this.listenTo(this.pathCollection, "reset", this.onPathReset), this.homeCollection.fetch({
                reset: !0
            })
        },
        onRender: function() {
            n.prototype.onRender.apply(this, arguments);
            var e = new s({
                root: !0,
                collection: this.homeCollection
            });
            this.listenTo(e, "change:path", this.onHomeChange), this.home.show(e);
            var t = new s({
                collection: this.pathCollection
            });
            this.listenTo(t, "change:path", this.onPathChange), this.path.show(t)
        },
        onSubmitForm: function(e) {
            e.preventDefault(), this.trigger("add:folder", this.ui.path.val()), this.hide()
        },
        onHomeReset: function(e) {
            var i = e.where({
                home: !0
            });
            i = i.length ? t.first(i) : e.first(), this.onHomeChange(i)
        },
        onPathReset: function() {
            this.home.currentView.unloadDirectory(this.pathCollection.parent)
        },
        onHomeChange: function(e) {
            this.home.currentView.loadDirectory(e), this.ui.path.val(e.get("path")), this.path.currentView.parentItem = null, this.pathCollection.hasPendingPopulate() && (this.pathCollection.abortPendingPopulate(), this.home.currentView.unselectDirectory(this.pathCollection.parent)), this.pathCollection.parent = e, this.pathCollection.fetch({
                reset: !0
            })
        },
        onPathChange: function(e) {
            this.ui.path.val(e.get("path"))
        }
    });
    return a;
});