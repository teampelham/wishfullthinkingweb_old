define([
	"templates/desktop/modals/localServerTestModal.tpl", 
	"underscore", 
	"jquery", 
	"base/commands/commands", 
	"base/models/server/serverModel", 
	"desktop/views/modals/baseModal"
], function(template, _, $, commands, serverModal, baseModal) {
    "use strict";
	
    var localServerTestModal = baseModal.extend({
        name: "localServerTestModal",
        className: "local-server-test-modal modal modal-sm fade",
        template: template,
        ui: {
            loadingIcon: ".loading",
            serverFound: ".server-found",
            serverNotFound: ".server-not-found"
        },
		
        events: _.extend({
            "click .confirm-btn": "onConfirm"
        }, baseModal.prototype.events),
        
		initialize: function() {
			console.warn('init');
            /*this.bindAllWhenRendered("onConnectionTestSuccess", "onConnectionTestFailure");
            var e = new s,
                t = e.addConnection({
                    address: "127.0.0.1",
                    port: 32400
                }),
                i = n.execute("testServerImage", {
                    server: e,
                    connection: t
                });
            i.done(this.onConnectionTestSuccess), i.fail(this.onConnectionTestFailure), i.always(this.whenRendered(function() {
                this.ui.loadingIcon.addClass("hidden")
            }))*/
        },
		
        onConnectionTestSuccess: function() {
            this.ui.serverFound.removeClass("hidden");
        },
        
		onConnectionTestFailure: function() {
            this.ui.serverNotFound.removeClass("hidden");
        },
        
		onConfirm: function() {
            this.hide();
        }
    });
	
    return localServerTestModal;
});