define([
	"templates/desktop/modals/users/homeWarningModal.tpl", 
	"underscore", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"desktop/views/modals/baseModal"
], function(template, _, t, dispatcher, baseModal) {
    "use strict";
	
    var homeWarningModal = baseModal.extend({
        name: "homeWarningModal",
        template: template,
        events: _.extend({
            "click .server-settings-btn": "onServerSettingsClick"
        }, baseModal.prototype.events),
		
        initialize: function(initType) {
            baseModal.prototype.initialize.apply(this, arguments);
			this.type = initType.type;
        },
		
        serializeData: function() {
            var msg;
            msg = t("pin" === this.type ? "You've added a PIN restriction" : "You've joined a Plex Home");
			return {
                intro: msg
            };
        },
		
        onServerSettingsClick: function(e) {
            e.preventDefault(); 
			this.hide(); 
			dispatcher.trigger("navigate", "serverSettings");
        }
    });
	
    return homeWarningModal;
});