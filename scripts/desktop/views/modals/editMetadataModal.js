define([
	//"templates/desktop/modals/EditMetadataModal.tpl", 
	"underscore", 
	"jquery", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/commands/commands", 
	//"base/models/library/MultiMetadataModel", 
	//"desktop/views/editmetadata/editMetadataGeneralView", 
	//"desktop/views/editmetadata/editMetadataTagsView", 
	//"desktop/views/editmetadata/editMetadataSharingView", 
	//"desktop/views/editmetadata/editMetadataArtworkView", 
	//"desktop/views/editmetadata/editMetadataInfoView", 
	"desktop/views/modals/baseModal", 
	"desktop/mixins/views/modals/paneModalMixin"
], function(e, t, i, n, s, a, r, o, l, c, d, u, h, p) {
    "use strict";
    var m = {
        movie: ["general", "tags", "sharing", "poster", "art", "info"],
        show: ["general", "tags", "sharing", "poster", "banner", "art"],
        season: ["general", "poster", "art", "info"],
        episode: ["general", "tags", "poster", "art", "info"],
        artist: ["general", "tags", "poster", "art"],
        album: ["general", "tags", "sharing", "poster", "art", "info"],
        track: ["general", "tags", "info"],
        photoAlbum: ["general", "poster", "art"],
        photo: ["general", "sharing", "info"],
        playlist: ["general"]
    }, f = ["poster", "art", "banner", "info"],
        g = a.extend({
            mixins: [p],
            name: "editMetadataModal",
            className: "edit-metadata-modal modal modal-lg fade",
            template: e,
            panes: {
                general: {
                    constructor: o
                },
                tags: {
                    constructor: l
                },
                poster: {
                    constructor: d
                },
                art: {
                    constructor: d
                },
                banner: {
                    constructor: d
                },
                sharing: {
                    constructor: c
                },
                info: {
                    constructor: u
                }
            },
            panesModel: "tempModel",
            events: t.extend({
                "click .change-pane-btn": "onChangePaneClick",
                "click .save-btn": "onFormSubmit",
                "submit .edit-metadata-form": "onFormSubmit"
            }, a.prototype.events),
            ui: {
                saveBtn: ".save-btn",
                message: ".form-message",
                navPane: ".modal-nav-pane",
                loadingIndicator: ".modal-body-pane .loading-inline"
            },
            regions: {
                paneRegion: ".pane-region"
            },
            onlyAfterRender: ["saveChanges", "showPane", "showNav"],
            initialize: function(e) {
                a.prototype.initialize.apply(this, arguments);
                var a, o, l, c = e.mediaActions;
                if (c ? (l = c.getActionTargets(), l[0].get("isPlaylist") || !c.server.supports("batchEdit") ? a = l[0] : (a = new r(1 === l.length ? l[0].attributes : {}, {
                    mediaActions: c
                }), o = a.populate())) : (a = e.model, l = [a]), this.model = a, this.tempModel = a.clone(), !o) {
                    var d = i.Deferred();
                    d.resolve(), o = d.promise()
                }
                o = o.done(t.bind(function() {
                    this.tempModel.set(a.attributes), 1 === l.length && this.tempModel.set("key", l[0].get("key")), this.showNav(), this.showPane(e.selectedPane || "general"), this.listenTo(s, "shortcut:save", this.saveChanges)
                }, this)), o.fail(t.bind(function() {
                    this.ui.message.text(n("There was an error editing this item")), this.ui.loadingIndicator.addClass("hidden")
                }, this))
            },
            serializeData: function() {
                var e = m[this.model.get("type")] || m.movie;
                this.model.isSingleMetadataModel && !this.model.isSingleMetadataModel() && (e = t.difference(e, f));
                var i = t.reduce(e, function(e, t) {
                    return e[t] = !0, e
                }, {}),
                    s = t.escape(n("Edit {1}", this.model.get("aggregateTitle") || this.model.get("title"))),
                    a = {
                        item: this.model.toJSON(),
                        panes: i,
                        selectedPane: this.selectedPane,
                        paneTitle: s
                    };
                return a
            },
            paneConstructorArg: function() {
                return {
                    type: this.selectedPane
                }
            },
            invalidChangedAttrs: function(e) {
                var i = [];
                if ("track" === this.tempModel.get("type")) {
                    var n = t(e),
                        s = n.has("artist.id") || n.has("artist.title") || n.has("album.id") || n.has("album.title"),
                        a = e["album.id"] || e["album.title"],
                        r = e["artist.id"] || e["artist.title"],
                        o = !s || r && a;
                    o || (r || i.push("grandparentTitle"), a || i.push("parentTitle"))
                }
                return i
            },
            markInvalid: function(e) {
                this.paneRegion.currentView.markInvalid(e)
            },
            showNav: function() {
                this.ui.navPane.removeClass("hidden")
            },
            saveChanges: function() {
                var e = this.ui.saveBtn,
                    s = this.ui.message;
                if (!e.isDisabled()) {
                    s.text(""), this.paneRegion.currentView.flushChanges();
                    var r = t.reduce(this.model.changedAttributes(this.tempModel.attributes), function(e, t, i) {
                        return /^original\./.test(i) ? e : (e[i] = t, e)
                    }, {}),
                        o = this.invalidChangedAttrs(r);
                    if (o.length) return void this.markInvalid(o);
                    e.addClass("active");
                    var l = [];
                    t.isEmpty(r) || (t.each(["artwork:poster", "artwork:banner", "artwork:art"], function(e) {
                        var t = r[e];
                        t && (l.push(a.execute("changeArtwork", {
                            item: this.model.singleMetadataModel(),
                            artworkType: e.replace("artwork:", ""),
                            artworkRatingKey: t
                        })), delete r[e])
                    }, this), l.push(this.model.save(r)));
                    var c = i.when.apply(i, l);
                    c.done(t.bind(function() {
                        this.hide()
                    }, this)), c.fail(function() {
                        s.text(n("Your changes could not be saved."))
                    }), c.always(function() {
                        e.removeClass("active")
                    })
                }
            },
            onPaneShow: function(e, t) {
                this.listenTo(t, "focusSubmit", function() {
                    this.ui.saveBtn.focus()
                })
            },
            onPaneHide: function(e, t) {
                t.flushChanges(), this.stopListening(t)
            },
            onFormSubmit: function(e) {
                e.preventDefault(), this.saveChanges()
            }
        });
    return g
})