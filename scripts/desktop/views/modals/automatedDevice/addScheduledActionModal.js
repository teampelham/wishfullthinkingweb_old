define([
	"templates/desktop/modals/addScheduledActionModal.tpl", 
	"underscore", 
	"jquery", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/utils/parser", 
	"base/commands/commands", 
	"desktop/views/modals/baseModal", 
    "desktop/views/editschedule/editScheduleDetailsView",
    "desktop/views/editschedule/editScheduleTimeView",
	"desktop/views/modals/libraryFailureModal", 
	"desktop/mixins/views/modals/paneModalMixin",
    "base/models/library/scheduledActionModel"
], function(template, _, $, t, dispatcher, parser, commands, baseModal, editScheduleDetailsView, editScheduleTimeView, LibraryFailureModal, paneModalMixin, ScheduledActionModel) {
    "use strict";
    
    var addScheduledActionModal = baseModal.extend({
        mixins: [paneModalMixin],
        name: "editScheduledActionModal",
        className: "edit-section-modal modal modal-lg fade",
        template: template,
        panes: {
            date: {
                constructor: editScheduleDetailsView,
                nextPane: "time",
                dependentPanes: ["time"]
            },
            time: {
                constructor: editScheduleTimeView,
            }
        },
        
        panesModel: "tempModel",
        ui: {
            sidebar: ".modal-nav-list",
            createBtn: ".create-btn",
            saveBtn: ".save-btn",
            message: ".form-message",
            headerSectionType: ".section-type-title"
        },
        
        regions: {
            paneRegion: ".pane-region"
        },
        
        events: _.extend({
            "click .change-pane-btn": "onChangePaneClick",
            "click .save-btn": "onSaveClick",
            "click .delete-btn": "onDeleteClick",
            "submit .edit-section-type-form": "onGeneralFormSubmit"
        }, baseModal.prototype.events),
        
        onlyAfterRender: ["showPane", "updateMessage", "updateSectionTitle", "updateSectionTitleDestabilized", "updateSaveBtn"],
        
        initialize: function(initObj) {
            baseModal.prototype.initialize.apply(this, arguments); 
            this.server = initObj.server; 
            this.redirect = initObj.redirect; 
            this.tempModel = this.model ? this.model.clone() : new ScheduledActionModel({}, {
                server: this.server,
                collection: this.server.get("sections")
            }); 
            this.showPane(initObj.selectedPane || "date"); 
            
            if (this.tempModel.isNew() && initObj.seedAttrs) 
                this.tempModel.set(initObj.seedAttrs);
        },
        
        serializeData: function() {
            var model = this.tempModel;
            var isNew = model.isNew();
            var data = {
                add: isNew,
                edit: !isNew,
                section: model && model.toJSON(),
                selectedPane: this.selectedPane
            };
            return data;
        },
        
        updateMessage: function(message) {
            this.ui.message.text(message || "");
        },
        
        updateSaveBtn: function() {
            var saveBtn = this.ui.saveBtn;
            var pane = this.panes[this.selectedPane];
            var toggleFn = _.bind(function(isMusic) {
                var nextPane = pane.nextPane;
                var isNew = this.tempModel.isNew();
                var isValid = !pane.constructor.prototype.validate || 0 === pane.constructor.prototype.validate(this.tempModel).length;
                var allValid = !this.getReachablePanes().isAnyInvalid;
                
                this.ui.createBtn.toggleClass("hidden", !isMusic);
                var savePane = nextPane;
                var completelyValid = allValid || isNew && isValid;
                
                if (!isMusic && "create" === nextPane) 
                    savePane = false; 
                
                if (allValid) 
                    savePane = false; 
                
                if (isMusic && "create" === nextPane) 
                    savePane = true; 
                
                if (isMusic && "advanced" === nextPane) 
                    savePane = true; 
                
                saveBtn.toggleClass("save", !savePane).toggleClass("next", !! savePane).toggleClass("disabled", !completelyValid);
            }, this);
            
            var musicModel = "music" === this.tempModel.get("type") && this.tempModel.server.supports("premiumMusic");
            
            if (musicModel) 
                this.paneRegion.currentView.populateAgents().then(function(agents) {
                    var music = agents.findWhere({
                        identifier: "com.plexapp.agents.plexmusic"
                    });
                    toggleFn(music);
                }); 
            else
                toggleFn(false);
        },
        
        updateSectionTitle: function() {
            var e = r.getSectionType(this.tempModel.get("type"));
            if (e) {
                this.updateSectionTitleDestabilized();
                var title = t(e.title, {
                    escape: true
                });
                this.ui.headerSectionType.html(title);
            }
        },
        
        updateSectionTitleDestabilized: function() {
            this.ui.headerSectionType.addClass("section-type-title-unstable"); 
            this.ui.headerSectionType.triggerReflow(); 
            this.ui.headerSectionType.removeClass("section-type-title-unstable");
        },
        
        save: function() {
            var saveBtn = this.ui.saveBtn;
            this.updateMessage("");
            saveBtn.addClass("active");
            
            var modelPromise = this.saveModel();
            modelPromise.always(function() {
               saveBtn.removeClass("active"); 
            });
            
            modelPromise.fail(_.bind(function(result) {
                if(result.status === 503)
                    dispatcher.trigger("show:modal", new LibraryFailureModal({
                        model: this.server
                    }));
                else
                    this.updateMessage(t("Your changes could not be saved."));
            }, this));
            
            modelPromise.then(_.bind(function(result) {
                if (!this.redirect) 
                    return void this.hide();
                var serverInfo = {
                    serverID: result.server.id,
                    sectionID: result.id
                };
                if (result.get("type") ==! "artist") {
                    dispatcher.trigger("navigate", "section", serverInfo);
                    return void this.hide();
                }
                else {
                    serverInfo.sortKey = "addedAt";
                    serverInfo.sortDirection = "desc";
                    this.hide();
                    return void dispatcher.trigger("navigate", "sectionType", serverInfo);
                }
            }, this));
        },
        
        saveModel: function() {
            var model;
            if (this.tempModel.isNew()) {
                model = this.tempModel.save();
                model = model.then(_.bind(function(xml, t, xhr) {
                    var modelStructure = parser.parseXML(xml);
                    if (xhr.status === 201) {
                        if (modelStructure.Directory)
                            modelStructure = modelStructure.Directory;
                             
                        if (!modelStructure.key) {
                            var responseLocation = xhr.getResponseHeader("Location");
                            if (responseLocation)
                                modelStructure.key = responseLocation.substring(responseLocation.lastIndexOf("/") + 1);
                        }
                        
                        var server = this.server;
                        if (server.supports("advancedFilters"))
                            modelStructure.filters = true;
                        
                        var metaModel = new ScheduledActionModel(modelStructure, {
                            parse: false,
                            server: this.server
                        });
                        server.get("sections").add(metaModel);
                        return metaModel;
                    }
                }, this));
            }
            else {
                model = this.model.save(this.model.changedAttributes(this.tempModel.attributes));
                model = model.then(_.bind(this.saveSettings, this));
                model = model.then(_.bind(this.saveAgentSettings, this));
            }
            
            return model;
        },
        
        saveSettings: function(model) {
            var modelPromise = $.Deferred().resolve(model).promise();
            if (!this.server.supports("sectionSettings"))
                return modelPromise;
            
            var settings = this.tempModel.settings;
            if (!settings)
                return modelPromise;
            
            var changes = this.getSettingsChanges(settings);
            if (_.isEmpty(changes))
                return modelPromise;
                
            var firstSetting = settings.first();
            var saveParams = firstSetting.getSaveParams(changes);
            var url = "/library/sections/" + model.id + "/prefs?" + _.param(saveParams, true);
            var xhr = _.extend(this.server.getXHROptions(url), {
                type: "PUT"
            });
            
            return $.ajax(xhr).promise().then(function() {
                return modelPromise;
            });
        },
        
        saveAgentSettings: function(e) {
            var n = $.Deferred().resolve(e).promise();
            var s = this.tempModel.agentSettings;
            
            if (!this.tempModel.agentSettings) 
                return n;
                
            var a = this.getSettingsChanges(s);
            if (_.isEmpty(a)) 
                return n;
            var r = s.first();
            var o = r.getSaveParams(a);
            var l = _.result(s, "url");
            var c = l + "/set?" + $.param(o, !0);
            var d = r.server.getXHROptions(c);
            
            return $.ajax(d).promise().then(function() {
                return n;
            });
        },
        
        getSettingsChanges: function(e) {
            var t = e.filter(function(e) {
                return e.changedAttributes()
            }).reduce(function(e, t) {
                var i = t.get("value");
                return "bool" === t.get("type") && (i = i ? "1" : "0"), e[t.get("id")] = i, e
            }, {});
            return t
        },
        
        onPaneShow: function(e, t) {
            this.listenTo(t, "valid", this.updateSaveBtn), this.listenTo(t, "invalid", this.updateSaveBtn), this.updateSaveBtn()
        },
        
        onPaneHide: function(e, t) {
            this.stopListening(t, "valid", this.updateSaveBtn), this.stopListening(t, "invalid", this.updateSaveBtn)
        },
        
        onChangePaneClick: function(e) {
            e.preventDefault();
            var target = this.$(e.currentTarget);
            if (!target.isDisabled()) 
                this.showPane(target.data("pane"));
        },
        
        onSaveClick: function(e) {
            e.preventDefault();
            var saveBtn = this.ui.saveBtn;
            if (!saveBtn.isDisabled()) {
                if (saveBtn.hasClass("next")) {
                    var nextPane = this.panes[this.selectedPane].nextPane;
                    
                    if(nextPane && !this.ui.sidebar.find("." + nextPane + "-btn").isDisabled()) 
                        this.showPane(nextPane);
                } 
                else 
                    this.save();
            }
        },
		
        onDeleteClick: function(e) {
            e.preventDefault();
            var t = l.execute("deleteSection", {
                section: this.model
            });
            t.done(function() {
                dispatcher.trigger("navigate", "dashboard")
            })
        },
		
        onGeneralFormSubmit: function(e) {
            this.onSaveClick(e)
        }
    });
	
    return addScheduledActionModal;
});