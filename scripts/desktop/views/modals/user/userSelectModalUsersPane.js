define(["templates/desktop/modals/users/userSelectModalUsersPane.tpl", "underscore", "jquery", "marionette", "base/utils/dispatcher", "base/commands/commands", "base/models/appModel", "base/models/server/serverStorageModel", "base/models/user/homeUserCollection", "desktop/views/modals/user/userSelectModalUserList"], function(e, t, i, n, s, a, r, o, l, c) {
    "use strict";
    var d = n.Layout.extend({
        className: "user-select-modal-users-container",
        template: e,
        ui: {
            loading: ".loading"
        },
        regions: {
            body: ".modal-body"
        },
        onlyAfterRender: ["onUserPopulated", "onHomeUsersPopulated", "onError"],
        initialize: function() {
            this.user = r.get("user"), this.homeUsers = this.user.get("homeUsers"), this.user.server.populate().done(t.bind(this.onUserPopulated, this)).fail(t.bind(this.onError, this))
        },
        showList: function(e) {
            this.ui.loading.addClass("hidden"), this.body.show(new c({
                collection: e
            })), this.listenTo(this.body.currentView, "itemview:selectUser", this.onUserSelect)
        },
        selectUser: function(e) {
            if (!this.user.get("signedIn") || e.id === this.user.id) return this.user.set("locked", !1), void this.trigger("resolve");
            this.ui.loading.removeClass("hidden");
            var i = a.execute("switchUser", {
                homeUser: e,
                switchUser: !0
            });
            i.done(t.bind(function() {
                this.trigger("resolve")
            }, this)), i.always(t.bind(function() {
                this.ui.loading.addClass("hidden")
            }, this))
        },
        onUserPopulated: function() {
            this.homeUsers.fetch().done(t.bind(this.onHomeUsersPopulated, this)).fail(t.bind(this.onError, this))
        },
        onHomeUsersPopulated: function(e) {
            var i = a.execute("lockUser");
            i.always(t.bind(function() {
                1 === e.length ? this.trigger("change:pane", "pin", {
                    model: e.first(),
                    showBackButton: !1,
                    focusDelay: 0
                }) : this.showList(e)
            }, this))
        },
        onError: function() {
            var e = o.getCurrentUser();
            e = e.then(t.bind(function(e) {
                if (!e.home) return i.Deferred().reject().promise();
                var t = {
                    id: e.id,
                    title: e.title,
                    "protected": !! e.pin
                };
                this.showList(new l([t], {
                    server: this.user.server,
                    parent: this.user
                }))
            }, this)), e.fail(t.bind(function() {
                this.user.set("locked", !1), this.trigger("resolve")
            }, this))
        },
        onUserSelect: function(e, t) {
            t.get("protected") ? this.trigger("change:pane", "pin", {
                model: t,
                showBackButton: !0,
                focusDelay: 0
            }) : this.selectUser(t)
        }
    });
    return d;
});