define(["templates/desktop/modals/users/userSelectModalPINPane.tpl", "underscore", "jquery", "marionette", "base/utils/t", "base/utils/dispatcher", "base/commands/commands", "base/models/appModel", "desktop/views/modals/user/userSelectModalUserListItem", "desktop/views/modals/homeWarningModal"], function(e, t, i, n, s, a, r, o, l, c) {
    "use strict";
    var d = n.Layout.extend({
        className: "user-select-modal-pin-container",
        template: e,
        ui: {
            inputsContainer: ".pin-inputs-container",
            inputs: "input",
            loading: ".loading",
            removeBtn: ".remove-btn",
            saveBtn: ".save-btn"
        },
        regions: {
            userList: ".user-select-list"
        },
        events: {
            "keydown input": "onKeyDown",
            "animationend .pin-inputs-container": "onInputsAnimationEnd",
            "webkitAnimationEnd .pin-inputs-container": "onInputsAnimationEnd",
            "oAnimationEnd .pin-inputs-container": "onInputsAnimationEnd",
            "click .back-btn": "onBackClick",
            "click .remove-btn": "onRemoveClick",
            "submit #pin-form": "onSubmit"
        },
        onlyAfterRender: ["confirmUser"],
        initialize: function(e) {
            this.isForced = e.isForced, this.isEditing = e.isEditing, this.isEditWarning = e.isEditWarning, this.showBackButton = e.showBackButton, this.focusDelay = e.focusDelay, this.user = o.get("user"), this.isEditConfirmation = this.isEditing && this.model.get("protected") && !this.model.get("restricted")
        },
        serializeData: function() {
            return {
                isEditing: this.isEditing && !this.isEditConfirmation,
                isEditWarning: this.isEditWarning,
                isEditConfirmation: this.isEditConfirmation,
                hasCurrentPIN: this.model.get("protected"),
                showBackButton: this.showBackButton
            }
        },
        focusInput: function(e) {
            var t;
            t = e ? e.next("input") : this.ui.inputs.first(), t.length ? t.focus().select() : !this.isEditing || this.isEditConfirmation ? this.confirmUser(this.getPIN()) : this.ui.saveBtn.focus()
        },
        clearInputs: function() {
            this.ui.inputs.val(""), this.focusInput()
        },
        clearInput: function(e) {
            e.length && (e.val(""), e.focus().select())
        },
        getPIN: function() {
            var e = "";
            return this.ui.inputs.each(function() {
                e += this.value
            }), e
        },
        editPIN: function(e) {
            if (!this.ui.saveBtn.isDisabled()) {
                this.ui.saveBtn.addClass("disabled");
                var i = this.savePIN(e);
                i.fail(t.bind(function() {
                    this.ui.saveBtn.removeClass("disabled"), a.trigger("show:alert", {
                        type: "error",
                        message: s("This PIN could not be saved.")
                    })
                }, this))
            }
        },
        removePIN: function() {
            if (!this.ui.removeBtn.isDisabled()) {
                this.ui.removeBtn.addClass("disabled");
                var e = this.savePIN("");
                e.fail(t.bind(function() {
                    this.ui.removeBtn.removeClass("disabled"), a.trigger("show:alert", {
                        type: "error",
                        message: s("This PIN could not be removed.")
                    })
                }, this))
            }
        },
        savePIN: function(e) {
            this.ui.loading.removeClass("hidden");
            var i = {
                pin: e
            };
            this.currentPin && (i.currentPin = this.currentPin);
            var n = this.model.save(i, {
                wait: !0
            });
            return n.done(t.bind(function() {
                this.model.unset("pin"), this.model.unset("currentPin"), this.model.set("protected", !! e), this.model.id === this.user.id && (e ? this.user.set("pin", this.user.hashPIN(e)) : this.user.unset("pin"), this.user.set("home", this.user.hasHome())), this.trigger("resolve", e), e && this.user.get("homeUsers").length < 2 && a.trigger("show:modal", new c({
                    type: "pin"
                }))
            }, this)), n.always(t.bind(function() {
                this.ui.loading.addClass("hidden")
            }, this)), n
        },
        confirmUser: function(e) {
            this.ui.loading.removeClass("hidden");
            var i = r.execute("switchUser", {
                homeUser: this.model,
                pin: e
            });
            i.done(t.bind(function() {
                this.isEditing ? (this.isEditConfirmation = !1, this.currentPin = e, this.render()) : this.trigger("resolve", e)
            }, this)), i.fail(t.bind(function() {
                this.ui.inputsContainer.addClass("shake"), this.clearInputs()
            }, this)), i.always(t.bind(function() {
                this.ui.loading.addClass("hidden")
            }, this))
        },
        onRender: function() {
            this.userList.show(new l({
                model: this.model,
                disabled: !0
            })), setTimeout(t.bind(this.focusInput, this), this.focusDelay)
        },
        onKeyDown: function(e) {
            var t = e.which;
            if (13 !== t) {
                var n = i(e.currentTarget);
                if (8 !== t && 46 !== t) {
                    e.preventDefault();
                    var s = t >= 48 && 57 >= t,
                        a = t >= 96 && 105 >= t;
                    (s || a) && (a && (t -= 48), n.val(String.fromCharCode(t)), this.focusInput(n))
                } else if (!n.val().length) {
                    e.preventDefault();
                    var r = 8 === t ? n.prev() : n.next();
                    this.clearInput(r)
                }
            }
        },
        onInputsAnimationEnd: function(e) {
            i(e.currentTarget).removeClass("shake")
        },
        onBackClick: function(e) {
            e.preventDefault(), this.trigger("change:pane", "users")
        },
        onRemoveClick: function(e) {
            e.preventDefault(), this.removePIN()
        },
        onSubmit: function(e) {
            e.preventDefault();
            var t = this.getPIN();
            return 4 !== t.length ? void a.trigger("show:alert", {
                type: "error",
                message: s("Please enter a 4 digit PIN.")
            }) : void(this.isEditing && !this.isEditConfirmation ? this.editPIN(t) : this.confirmUser(t))
        }
    });
    return d;
});