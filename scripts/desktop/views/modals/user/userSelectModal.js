define(["templates/desktop/modals/users/userSelectModal.tpl", "underscore", "jquery", "base/utils/t", "base/models/appModel", "desktop/views/modals/baseModal", "desktop/views/modals/user/userSelectModalUsersPane", "desktop/views/modals/user/userSelectModalPINPane"], function(template, _, $, t, appModel, baseModal, userSelectModalUsersPane, userSelectModalPINPane) {
    "use strict";
    
    var userSelectModal = baseModal.extend({
        name: "userSelectModal",
        className: "user-select-modal modal modal-transparent fade",
        template: template,
        transparent: true,
        panes: {
            users: userSelectModalUsersPane,
            pin: userSelectModalPINPane
        },
        
        regions: {
            content: ".modal-content"
        },
        
        initialize: function(options) {
            baseModal.prototype.initialize.apply(this, arguments);
            this.options = options;
            this.isForced = options.isForced;
            this.user = appModel.get("user");
            this.promise = $.Deferred();
        },
        
        showPane: function(paneID, options) {
            console.log('showPane');
            if (this.currentPane)
                this.stopListening(this.currentPane);
            
            var Pane = this.panes[paneID];
            var paneModel = new Pane(_.extend({}, this.options, options));
            
            this.listenTo(paneModel, {
                "change:pane": this.onChangePane,
                resolve: this.onResolve,
                reject: this.onReject
            });
            this.currentPane = paneModel;
            this.content.show(paneModel);
            return paneModel;
        },
        
        onRender: function() {
            baseModal.prototype.onRender.apply(this, arguments);
            
            if (this.model)
                this.showPane("pin", {
                    focusDelay: this.focusDelay
                });
            else
                this.showPane("users");
        },
        
        onHide: function() {
            baseModal.prototype.onHide.apply(this, arguments);
            if ("pending" === this.promise.state())
                this.promise.reject();
        },
        
        onChangePane: function(pane, options) {
            this.showPane(pane, options);
        },
        
        onResolve: function(arg) {
            this.promise.resolve(arg);
            this.hide();
        },
        
        onReject: function() {
            this.promise.reject();
            this.hide();
        }
    });
    
    return userSelectModal;
});