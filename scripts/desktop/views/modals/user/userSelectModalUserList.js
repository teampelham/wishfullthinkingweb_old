define([
	"desktop/views/lists/baseList", 
	"desktop/views/modals/user/userSelectModalUserListItem"
], function(baseList, userSelectModalUserListItem) {
    "use strict";
    
	var userSelectModalUserList = baseList.extend({
        tagName: "ul",
        className: "tile-list user-select-list",
        itemView: userSelectModalUserListItem
    });
	
    return userSelectModalUserList;
});