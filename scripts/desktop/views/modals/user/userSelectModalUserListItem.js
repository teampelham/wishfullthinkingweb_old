define(["templates/desktop/modals/users/userSelectModalUserListItem.tpl", "marionette", "base/models/appModel", "base/views/images/gravatarImage"], function(e, t, i, n) {
    "use strict";
    var s = t.ItemView.extend({
        tagName: "li",
        className: "user-select-list-item",
        template: e,
        ui: {
            container: ".user-select-container",
            poster: ".user-poster"
        },
        events: {
            "click a": "onClick"
        },
        initialize: function(e) {
            this.disabled = e.disabled, this.posterImage = new n({
                model: this.model,
                type: "friend",
                width: 80
            });
            var t = i.get("primaryServer");
            t && t.supports("imageFilters") && (this.backgroundImage = new n({
                model: this.model,
                type: "friend",
                width: 120,
                background: !0,
                filters: {
                    blur: 20,
                    opacity: 50,
                    background: "000000"
                }
            }), this.backgroundImage.placeholderClass = !1)
        },
        serializeData: function() {
            return {
                disabled: this.disabled
            }
        },
        bindings: {
            ".username": {
                modelAttribute: "title"
            },
            ".protected-icon": {
                modelAttribute: "protected",
                autohide: !0,
                manual: !0
            },
            ".admin-icon": {
                modelAttribute: "admin",
                autohide: !0,
                manual: !0
            }
        },
        onRender: function() {
            this.posterImage.setElement(this.ui.poster), this.backgroundImage && this.backgroundImage.setElement(this.ui.container)
        },
        onClose: function() {
            this.posterImage.close(), this.backgroundImage && this.backgroundImage.close()
        },
        onClick: function(e) {
            e.preventDefault(), this.trigger("selectUser", this.model)
        }
    });
    return s;
});