define([
	"templates/desktop/modals/editSectionModal.tpl", 
	"underscore", 
	"jquery", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/utils/parser", 
	"base/utils/types/sectionTypes", 
	"base/utils/types/mediaTypes", 
	"base/commands/commands", 
	"base/models/library/section/sectionModel", 			// c
	//"desktop/views/editsection/EditSectionAdvancedView", // d
	//"desktop/views/editsection/EditSectionFoldersView", 	// u
	//"desktop/views/editsection/EditSectionSelectPremiumMusicView", // h 
	//"desktop/views/editsection/EditSectionTypeView", 			// p
	//"desktop/views/editsection/EditSectionErrorView", 		// m
	"desktop/views/modals/baseModal", 						// f
	//"desktop/views/modals/LibraryFailureModal", 			// g
	"desktop/mixins/views/modals/paneModalMixin"			// v
], function(template, _, $, t, dispatcher, parser, sectionTypes, mediaTypes, commands, SectionModel, baseModal, paneModalMixin) {
    "use strict";
	
    var editSectionModal = baseModal.extend({
        mixins: [paneModalMixin],
        name: "editSectionModal",
        className: "edit-section-modal modal modal-lg fade",
        template: template,
        panes: {
            /*type: {
                constructor: p,
                nextPane: "folders",
                dependentPanes: ["folders"]
            },
            folders: {
                constructor: u,
                nextPane: "create",
                dependentPanes: ["create", "advanced"]
            },
            create: {
                constructor: h,
                nextPane: "advanced"
            },
            advanced: {
                constructor: d
            },
            error: {
                constructor: m
            }*/
        },
        panesModel: "tempModel",
        ui: {
            sidebar: ".modal-nav-list",
            createBtn: ".create-btn",
            saveBtn: ".save-btn",
            message: ".form-message",
            headerSectionType: ".section-type-title"
        },
		
        regions: {
            paneRegion: ".pane-region"
        },
		
        events: _.extend({
            "click .change-pane-btn": "onChangePaneClick",
            "click .save-btn": "onSaveClick",
            "click .delete-btn": "onDeleteClick",
            "submit .edit-section-type-form": "onGeneralFormSubmit"
        }, baseModal.prototype.events),
		
        onlyAfterRender: ["showPane", "updateMessage", "updateSectionTitle", "updateSectionTitleDestabilized", "updateSaveBtn"],
        
		initialize: function(initObj) {
            baseModal.prototype.initialize.apply(this, arguments);
			this.server = initObj.server; 
			this.redirect = initObj.redirect; 
			this.tempModel = this.model ? this.model.clone() : new SectionModel({}, {
                server: this.server,
                collection: this.server.get("sections")
            }); 
			this.listenTo(this.tempModel, "change:type", this.onSectionTypeChange); 
			this.showPane(initObj.selectedPane || "type"); 
			
			if (this.tempModel.isNew() && initObj.seedAttrs) 
				this.tempModel.set(initObj.seedAttrs);
        },
		
        serializeData: function() {
            var tempModel = this.tempModel;
            var isNew = tempModel.isNew();
            var serial = {
                add: isNew,
                edit: !isNew,
                section: tempModel && tempModel.toJSON(),
                selectedPane: this.selectedPane
            };
            return serial;
        },
		
        updateMessage: function(text) {
            this.ui.message.text(text || "");
        },
		
        updateSaveBtn: function() {
			console.warn('');
            /*var e = this.ui.saveBtn,
                i = this.panes[this.selectedPane],
                n = t.bind(function(t) {
                    var n = i.nextPane,
                        s = this.tempModel.isNew(),
                        a = !i.constructor.prototype.validate || 0 === i.constructor.prototype.validate(this.tempModel).length,
                        r = !this.getReachablePanes().isAnyInvalid;
                    this.ui.createBtn.toggleClass("hidden", !t);
                    var o, l = r || s && a;
                    o = n, t || "create" !== n || (o = !1), r && (o = !1), t && "create" === n && (o = !0), t && "advanced" === n && (o = !0), e.toggleClass("save", !o).toggleClass("next", !! o).toggleClass("disabled", !l)
                }, this),
                s = "music" === this.tempModel.get("type") && this.tempModel.server.supports("premiumMusic");
            s ? this.paneRegion.currentView.populateAgents().then(function(e) {
                var t = e.findWhere({
                    identifier: "com.plexapp.agents.plexmusic"
                });
                n(t)
            }) : n(!1)*/
        },
		
        updateSectionTitle: function() {
			console.warn('');
            /*var e = r.getSectionType(this.tempModel.get("type"));
            if (e) {
                this.updateSectionTitleDestabilized();
                var t = n(e.title, {
                    escape: !0
                });
                this.ui.headerSectionType.html(t)
            }*/
        },
		
        updateSectionTitleDestabilized: function() {
            this.ui.headerSectionType.addClass("section-type-title-unstable"); 
			this.ui.headerSectionType.triggerReflow(); 
			this.ui.headerSectionType.removeClass("section-type-title-unstable");
        },
		
        save: function() {
			console.warn('');
            /*var e = this.ui.saveBtn;
            this.updateMessage(""), e.addClass("active");
            var i = this.saveModel();
            i.always(function() {
                e.removeClass("active")
            }), i.fail(t.bind(function(e) {
                503 === e.status ? s.trigger("show:modal", new g({
                    model: this.server
                })) : this.updateMessage(n("Your changes could not be saved."))
            }, this)), i.then(t.bind(function(e) {
                if (!this.redirect) return void this.hide();
                var t = {
                    serverID: e.server.id,
                    sectionID: e.id
                };
                return "artist" !== e.get("type") ? (s.trigger("navigate", "section", t), void this.hide()) : (t.type = o.getMediaTypeByString("album").id, t.sortKey = "addedAt", t.sortDirection = "desc", this.hide(), void s.trigger("navigate", "sectionType", t))
            }, this))*/
        },
		
        saveModel: function() {
			console.warn('');
            /*var e;
            return this.tempModel.isNew() ? (e = this.tempModel.save(), e = e.then(t.bind(function(e, t, i) {
                var n = a.parseXML(e);
                if (201 === i.status) {
                    if (n.Directory && (n = n.Directory), !n.key) {
                        var s = i.getResponseHeader("Location");
                        s && (n.key = s.substring(s.lastIndexOf("/") + 1))
                    }
                    var r = this.server;
                    r.supports("advancedFilters") && (n.filters = !0);
                    var o = new c(n, {
                        parse: !0,
                        server: this.server
                    });
                    return r.get("sections").add(o), o
                }
            }, this))) : e = this.model.save(this.model.changedAttributes(this.tempModel.attributes)), e = e.then(t.bind(this.saveSettings, this)), e = e.then(t.bind(this.saveAgentSettings, this))*/
        },
		
        saveSettings: function(e) {
			console.warn('');
            /*var n = i.Deferred().resolve(e).promise();
            if (!this.server.supports("sectionSettings")) return n;
            var s = this.tempModel.settings;
            if (!s) return n;
            var a = this.getSettingsChanges(s);
            if (t.isEmpty(a)) return n;
            var r = s.first(),
                o = r.getSaveParams(a),
                l = "/library/sections/" + e.id + "/prefs?" + i.param(o, !0),
                c = t.extend(this.server.getXHROptions(l), {
                    type: "PUT"
                });
            return i.ajax(c).promise().then(function() {
                return n
            })*/
        },
		
        saveAgentSettings: function(e) {
			console.warn('');
            /*var n = i.Deferred().resolve(e).promise(),
                s = this.tempModel.agentSettings;
            if (!this.tempModel.agentSettings) return n;
            var a = this.getSettingsChanges(s);
            if (t.isEmpty(a)) return n;
            var r = s.first(),
                o = r.getSaveParams(a),
                l = t.result(s, "url"),
                c = l + "/set?" + i.param(o, !0),
                d = r.server.getXHROptions(c);
            return i.ajax(d).promise().then(function() {
                return n
            })*/
        },
		
        getSettingsChanges: function(e) {
			console.warn('');
            /*var t = e.filter(function(e) {
                return e.changedAttributes()
            }).reduce(function(e, t) {
                var i = t.get("value");
                return "bool" === t.get("type") && (i = i ? "1" : "0"), e[t.get("id")] = i, e
            }, {});
            return t*/
        },
		
        onPaneShow: function(e, t) {
            this.listenTo(t, "valid", this.updateSaveBtn); 
			this.listenTo(t, "invalid", this.updateSaveBtn); 
			this.updateSaveBtn();
        },
		
        onPaneHide: function(e, t) {
			console.warn('');
            //this.stopListening(t, "valid", this.updateSaveBtn), this.stopListening(t, "invalid", this.updateSaveBtn)
        },
		
        onChangePaneClick: function(e) {
			console.warn('');
            /*e.preventDefault();
            var t = this.$(e.currentTarget);
            t.isDisabled() || this.showPane(t.data("pane"))*/
        },
		
        onSaveClick: function(e) {
			console.warn('');
            e.preventDefault();
            /*var t = this.ui.saveBtn;
            if (!t.isDisabled())
                if (t.hasClass("next")) {
                    var i = this.panes[this.selectedPane].nextPane;
                    i && !this.ui.sidebar.find("." + i + "-btn").isDisabled() && this.showPane(i)
                } else this.save()*/
        },
		
        onDeleteClick: function(e) {
			console.warn('');
            /*e.preventDefault();
            var t = l.execute("deleteSection", {
                section: this.model
            });
            t.done(function() {
                s.trigger("navigate", "dashboard")
            })*/
        },
		
        onGeneralFormSubmit: function(e) {
            this.onSaveClick(e);
        },
		
        onSectionTypeChange: function() {
            this.updateSaveBtn();
        }
    });
	
    return editSectionModal;
});