define([
    "templates/desktop/modals/mediaInfoModal.tpl", 
    "underscore", 
    "jquery", 
    "base/utils/formatters/stringUtil", 
    "base/utils/formatters/dateTimeUtil", 
    "base/utils/formatters/metadataUtil", 
    "base/models/appModel", 
    "desktop/views/modals/baseModal", 
    "base/commands/commands", 
    "desktop/models/mediaActionsModel"
], function(e, t, i, n, s, a, r, o, l, c) {
    "use strict";

    function d(e) {
        var i = t.reject(e, function(e) {
            return 0 === e.id
        }),
            n = t.sortBy(i, "streamType");
        return t.map(n, function(e) {
            return g(e, "streamType")
        })
    }

    function u(e) {
        return t.map(e.get("Media"), function(e) {
            return t.extend(g(e, "Part"), {
                Part: t.map(e.Part, function(e) {
                    return t.extend(g(e, "Stream"), {
                        file: e.file,
                        exists: e.exists,
                        accessible: e.accessible,
                        Stream: d(e.Stream)
                    })
                })
            })
        })
    }
    var h = ["id", "key", "index", "videoCodec", "audioCodec", "audioChannels", "default", "selected", "languageCode", "codecID", "accessible", "exists"],
        p = {
            size: a.formatBytes,
            duration: s.formatDuration,
            codec: n.upperCase,
            format: n.upperCase,
            container: n.upperCase,
            videoResolution: a.formatResolution,
            channels: a.formatAudioChannels,
            level: a.formatVideoLevel,
            bitrateMode: n.upperCase,
            accessible: n.formatBoolean,
            exists: n.formatBoolean,
            optimizedForStreaming: n.formatBoolean,
            file: n.getLastSegment
        }, m = {
            optimizedForStreaming: "Web Optimized",
            has64bitOffsets: "Has 64bit Offsets",
            cabac: "CABAC",
            postURL: "URL"
        }, f = {
            frameRate: " fps",
            dialogNorm: " dB",
            bitrate: " kbps",
            samplingRate: " Hz"
        }, g = function(e, i) {
            var s = {
                properties: []
            };
            for (var a in e)
                if (a !== i && -1 === t.indexOf(h, a)) {
                    var r = m[a],
                        o = e[a];
                    p[a] && (o = p[a](o)), o && (f[a] && (o += f[a]), r || (r = n.formatLabel.call(this, a)), s.properties.push({
                        name: r,
                        value: o
                    }))
                }
            return i && e && (s[i] = e[i]), s
        }, v = o.extend({
            name: "mediaInfoModal",
            className: "media-info-modal modal modal-lg fade",
            template: e,
            ui: {
                scroller: ".modal-body-scroll",
                files: ".files > ul",
                stickyProps: ".media > .props, .part > .props"
            },
            events: t.extend({
                "click .delete-btn": "onDeleteClick"
            }, o.prototype.events),
            modelEvents: {
                "change:Media": "onMediaChange"
            },
            initialize: function() {
                o.prototype.initialize.apply(this, arguments), this.bindAllWhenRendered("onMediaChange", "onScroll"), this.loading = !1, this.media = u(this.model), this.mediaActions = new c({}, {
                    item: this.model
                }), this.hasStreams() || (this.loading = !0, this.model.fetch().always(this.whenRendered(function() {
                    this.model.changed.hasOwnProperty("Media") || this.updateMedia()
                })))
            },
            serializeData: function() {
                var e = this.model.get("key"),
                    t = r.get("user"),
                    i = this.model.server;
                return this.model.isLibraryItem() && (e += e.indexOf("?") > 0 ? "&" : "?", e += "checkFiles=1&includeExtras=1"), {
                    key: e,
                    url: this.model.server.resolveUrl(e, {
                        appendToken: !0
                    }),
                    media: this.media,
                    hasFiles: this.hasFiles(),
                    hasStreams: this.hasStreams() || this.loading,
                    showDeleteFile: i.supports("specificMediaDeletion"),
                    showFilepath: !i.get("shared") || t.isNinja(),
                    loading: this.loading
                }
            },
            updateMedia: function() {
                this.loading = !1, this.rerender = !0, this.ui.scroller.off("scroll", this.onScroll), this.media = u(this.model), this.render()
            },
            hasFiles: function() {
                return !!t.chain(this.media).pluck("Part").flatten().pluck("file").compact().size().value()
            },
            hasStreams: function() {
                return !!t.chain(this.media).pluck("Part").flatten().pluck("Stream").flatten().size().value()
            },
            bindingsModel: "mediaActions",
            bindings: {
                ".delete-btn": {
                    modelAttribute: "showDelete",
                    manual: !0,
                    autohide: !0
                }
            },
            onRender: function() {
                o.prototype.onRender.apply(this, arguments), this.ui.scroller.on("scroll", this.onScroll), this.$el.tooltip({
                    selector: ".files .label",
                    placement: "bottom"
                })
            },
            onClose: function() {
                this.ui.scroller.off("scroll", this.onScroll)
            },
            onMediaChange: function() {
                return this.isHidden ? void 0 : this.model.get("Media").length ? void this.updateMedia() : void this.hide()
            },
            onDeleteClick: function(e) {
                e.preventDefault(), l.execute("deleteMediaItem", {
                    item: this.model,
                    index: i(e.currentTarget).data("media-item")
                })
            },
            onScroll: function() {
                var e = this.ui.scroller.scrollTop(),
                    t = this.ui.scroller.offset().top;
                return this.ui.files.length && (t += parseInt(this.ui.files.css("margin-bottom"), 10)), 0 > t ? void setTimeout(this.onScroll, 500) : void this.ui.stickyProps.each(function() {
                    var n = i(this),
                        s = n.parent().height() - n.height(),
                        a = e - this.parentNode.offsetTop,
                        r = Math.min(Math.max(a, 0), s);
                    r && r === a ? (n.addClass("sticky"), n.css("top", t)) : (n.removeClass("sticky"), n.css("top", r))
                })
            }
        });
    return v
});