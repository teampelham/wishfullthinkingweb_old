define([
	"templates/desktop/section/sectionView.tpl", 
	"underscore", 
    "jquery",
	"marionette", 
	"base/utils/dispatcher", 
	"desktop/models/mediaActionsModel", 
	"base/models/library/section/sectionViewStateModel", 
	"base/models/library/section/sectionViewStateStorageModel", 
	"desktop/views/actionbar/sectionActionBarView", 
	"desktop/views/breadcrumbbar/sectionBreadcrumbBarView", 
	//"desktop/views/alphabar/alphaBarView", 
	"desktop/views/section/sectionListView",
    "desktop/views/section/sectionFormView"
], function(template, _, $, marionette, dispatcher, MediaActionsModel, SectionViewStateModel, SectionViewStateStorageModel, SectionActionBarView, SectionBreadcrumbBarView, SectionListView, SectionFormView) {
    "use strict";
	
    var sectionView = marionette.Layout.extend({
        className: "section-container",
        template: template,
        
		regions: {
            list: ".section-list-container"
        },
        
		modelEvents: {
            "change:currentType": "toggleAlphaBar",
            "change:currentSort": "toggleAlphaBar",
            "change:currentSubset": "onCurrentSubsetChange"
        },
        
		initialize: function(initObj) {
			this.mediaActions = new MediaActionsModel({}, {
                section: this.model,
                showAdvanced: true
            }); 
			
			this.initViewState(initObj).then(_.bind(function() {
                this.model.set("currentFolderKey", initObj.folderKey); 
				dispatcher.trigger("show:bar", "actionbar", new SectionActionBarView({
                    model: this.model,
                    mediaActions: this.mediaActions
                })); 
				
				dispatcher.trigger("show:bar", "breadcrumbbar", new SectionBreadcrumbBarView({
                    model: this.model
                }));
				
				this.toggleAlphaBar();
            }, this));
        },
		
        initViewState: function(initObj) {
            var storageModel = _.extend({
                storage: new SectionViewStateStorageModel({}, {
                    section: this.model
                }),
                section: this.model,
                filters: this.model.get("currentFilters")
            }, initObj);
            
            if(this.model.id == "voice") {
                var sectionFormView = new SectionFormView({
                    model: this.model,
                    collection: this.model.get("items"),
                    mediaActions: this.mediaActions
                });
                this.list.append(sectionFormView, this.$el);
                return $.Deferred().resolve().promise();
            }
            
            this.viewState = new SectionViewStateModel({}, storageModel);
			
            var currentSort = this.model.get("currentSort");
            if (currentSort) 
				currentSort.set("selected", false);
			
            var viewStatePromise = this.viewState.fetch();
            
			viewStatePromise.done(_.bind(function() {
                var attrs = this.viewState.getSectionAttrs();
                this.model.set(attrs); 
				this.model.get("currentType").set("selected", true); 
				this.model.get("currentSubset").set("selected", true); 
                if(this.model.get("currentSort"))
				    this.model.get("currentSort").set("selected", true); 
				this.viewState.addListeners();
            }, this)); 
			
			viewStatePromise.always(this.whenRendered(function() {
                var sectionListView = new SectionListView({
                    model: this.model,
                    collection: this.model.get("items"),
                    mediaActions: this.mediaActions
                });
                this.list.append(sectionListView, this.$el);
            }));
			
			return viewStatePromise;
        },
        
        render: function() {
            if(this.$el.has('.section-form-view')) {
                return;
            }
        },
		
        toggleAlphaBar: function() {
            /*if (this.supportsAlphaPagination())
				console.warn('alpha bar me!'); 
				dispatcher.trigger("show:bar", "alphabar", new AlphaBarView({
                	model: this.model,
                	collection: this.model.get("alphanumeric")
            	})); 
			else*/
				dispatcher.trigger("hide:bar", "alphabar");
        },
		
        supportsAlphaPagination: function() {
            if (!this.model.get("allowAdvancedFilters") || "all" !== this.model.get("currentSubset").get("key")) 
				return false;
				
            var currentType = this.model.get("currentType");
            var currentSort = this.model.get("currentSort");
            if (!currentSort) {
                var regex = /^(season|episode|album|track)$/;
                return !regex.test(currentType.get("typeString"));
            }
			
            var sortKey = currentSort.get("key");
            var direction = currentSort.get("direction");
            return "titleSort" === sortKey && "asc" === direction;
        },
        
		onCurrentSubsetChange: function() {
            this.model.get("items").scrollPosition = 0;
        },
        
		onClose: function() {
            dispatcher.trigger("hide:bar", "actionbar"); 
			dispatcher.trigger("hide:bar", "breadcrumbbar"); 
			dispatcher.trigger("hide:bar", "alphabar");
            if (this.viewState) 
			     this.viewState.removeListeners();
        }
    });
	
    return sectionView;
});