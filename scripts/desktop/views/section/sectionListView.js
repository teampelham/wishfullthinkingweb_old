define([
	"templates/desktop/section/sectionListView.tpl", 
	"marionette", 
	"base/utils/constants/timelineEvents", 
	"base/utils/dispatcher", 
	"base/adapters/settingsAdapter", 
	"desktop/views/lists/library/virtualMediaTileList", 
	//"desktop/views/lists/library/virtualMediaDetailsList", 
	"desktop/views/lists/library/virtualMediaList", 
	"desktop/views/modals/editSectionModal",
    "underscore"
], function(template, marionette, timelineEvents, dispatcher, settingsAdapter, virtualMediaTileList, virtualMediaList, EditSectionModal, _) {
    "use strict";
	
    var sectionListView = marionette.Layout.extend({
        className: "section-list-container",
        template: template,
		
        ui: {
            loading: ".section-loading-container",
            emptyMessageContainer: ".empty-message"
        },
        
		regions: {
            list: "ul"
        },
        
		modelEvents: {
            "change:selecting": "onSelectingChange",
            "change:currentType": "onCurrentTypeChange",
            "change:currentFilters": "onListRefresh",
            "change:currentSort": "onListRefresh",
            "change:currentFirstCharacter": "onCurrentFirstCharacterChange",
            "socket:timeline": "onTimeline",
            "change:refreshing": "showEmptyMessage"
        },
		
        collectionEvents: {
            reset: "showEmptyMessage",
            "change:totalSize": "showEmptyMessage"
        },
		
        events: {
            "click .add-to-library-link": "onAddToLibraryLinkClick"
        },
		
        onlyAfterRender: ["showLoading", "hideLoading", "showEmptyMessage"],
        
		initialize: function(initObj) {
            this.listenTo(settingsAdapter.getInstance(), "change:listType", this.onListTypeChange); 
			this.mediaActions = initObj.mediaActions; 
			
			this.collection.fetch({
                reset: true,
                scope: "page"
            });
        },
        
        serializeData: function() {
            return {
                canEdit: !this.model.server.get("shared") && !this.model.server.get("synced")
            };
        },
		
        showLoading: function(loadingOptions) {
            loadingOptions = loadingOptions || {};
            
			var currentView = this.list.currentView;
            var itemsPerPage = currentView.itemsPerPage;
            var currentItem = this.collection[currentView.sizeProperty];
			
            this.ui.loading.toggleClass("has-scrollbar", currentItem > itemsPerPage); 
			this.ui.loading.toggleClass("bottom", !loadingOptions.prepend); 
			this.ui.loading.removeClass("hidden");
        },
		
        hideLoading: function() {
            this.ui.loading.addClass("hidden");
        },
		
        showEmptyMessage: function() {
            this.ui.emptyMessageContainer.removeClass("folder filter refreshing default");
			var collectionSize = this.collection[this.list.currentView.sizeProperty];
            
			if (0 === collectionSize) {
                var currentSubset = this.model.get("currentSubset");
                var currentFilters = this.model.get("currentFilters");
				var containerClass; 
				
				if (currentSubset && "folder" === currentSubset.id) 
					containerClass = "folder";
				else if (currentSubset && "all" !== currentSubset.id) 
					containerClass = "filter"; 
				else if (currentFilters && currentFilters.length) 
					containerClass = "filter"; 
				else if (this.model.get("refreshing")) 
					containerClass = "refreshing"; 
				else 
					containerClass = "default"
					
                this.ui.emptyMessageContainer.addClass(containerClass);
            }
        },
		
        getListConstructor: function() {
            var listType = settingsAdapter.getInstance().getSectionListType(this.model);
            
			switch (listType) {
                case "media-details-list":
                    return console.warn('virtualMediaDetailsList');
                case "media-list":
                    return virtualMediaList;
                default:
                    return virtualMediaTileList;
            }
        },
		
        onRender: function() {
            var ListConstructor = this.getListConstructor();
            if (ListConstructor) {
                var list = new ListConstructor({
                    collection: this.collection,
                    paginated: !this.model.server.get("synced"),
                    mediaActions: this.mediaActions
                });
                this.list.append(list, this.$el); 
				this.listenTo(this.list.currentView, {
                    "change:loading": this.onLoadingChange
                });
            
                this.listenTo(dispatcher, "listview:add", this.onModelAdded);
            }
        },
        
        onModelAdded: function(model) {
            this.list.currentView.addItemToList(model);
        },
        
        onClose: function() {
            this.collection.clear();
        },
		
        onSelectingChange: function(e, change) {
            this.$el.toggleClass("hide-actions", change);
        },
		
        onCurrentTypeChange: function() {
            this.onListRefresh();
            var sectionListType = settingsAdapter.getInstance().getSectionListType(this.model);
            var currentType = settingsAdapter.getInstance().getSectionListType(this.model, this.model.previous("currentType"));
            
			if (sectionListType !== currentType) 
				this.render();
        },
		
        onListRefresh: function() {
            this.collection.scrollPosition = 0; 
			this.collection.clear({
                silent: false
            }); 
			this.collection.fetch({
                reset: false,
                scope: "page"
            }); 
			this.showLoading();
        },
		
        onListTypeChange: function() {
            this.list.currentView.scrollPosition = 0; 
			if (this.collection.populatedRange.start > 0) {
				this.collection.clear(); 
				this.collection.fetch({
	                reset: true,
	                scope: "page"
	            });
			}
			this.render();
        },
		
        onCurrentFirstCharacterChange: function(e, t) {
			console.warn('what?');
            /*for (var i = e.get("alphanumeric"), n = 0, s = 0, a = i.length; a > s; s++) {
                var r = i.at(s);
                if (t.id === r.id) break;
                n += r.get("size")
            }
            this.list.currentView.scrollToIndex(n)*/
        },
		
        onTimeline: function(e) {
			console.warn('timeline');
            /*if (e.state === i.CREATED || e.state === i.DELETED) {
                var t = this.model.get("currentType");
                ("folder" === this.collection.subset || e.type === t.id) && this.list.currentView.merge()
            }*/
        },
		
        onLoadingChange: function(loading, message) {
            if (loading) 
				this.showLoading(message); 
			else
				this.hideLoading();
        },
		
        onAddToLibraryLinkClick: function(e) {
            e.preventDefault(); 

			dispatcher.trigger("show:modal", new EditSectionModal({
                server: this.model.server,
                model: this.model,
                selectedPane: "folders"
            }));
        }
    });
	
    return sectionListView;
});