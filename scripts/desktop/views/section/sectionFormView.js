define([
	"templates/desktop/section/sectionFormView.tpl", 
	"underscore",
	"jquery",
	"marionette"
], function(template, _, $, marionette) {
	"use strict";
	
	var sectionFormView = marionette.Layout.extend({
		className: "section-form-container",
		template: template,
		ui: {
			sayInput: "#sayInput"
		},
		
		events: {
			"click #speak": "onSpeakClick"
		},
		
		initialize: function (initObj) {
		      this.server = this.model.server;
		},
		
		onSpeakClick: function(e) {
			e.preventDefault();
			
            // TODO: Get a voice dd in there too
			var speakText = this.ui.sayInput.val();
			
            // TODO: Check if promise exists and if status is done
			this.promise = this.sendSpeakText(speakText);
            
            this.promise.always(_.bind(function() {
			     this.ui.sayInput.val("");
            }, this));
		},
		
        sendSpeakText: function(speakText) {
            var modelPromise = $.Deferred().resolve(speakText).promise();
            
            // TODO: 
            //if (!this.server.supports("speechOutput"))
            //    return modelPromise;
            
            //var speechParams = { phrase: speakText, voiceID: 0 };
            
            var url = "/voice/speak?phrase=" + encodeURI(speakText) + "&voiceID=0";
            var xhr = _.extend(this.server.getXHROptions(url), {
                type: "GET",
                //data: JSON.stringify(speechParams)
            });
            
            return $.ajax(xhr).promise().then(function() {
                return modelPromise;
            });
        },
	});
	
	return sectionFormView;
});