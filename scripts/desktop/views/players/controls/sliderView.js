define(["templates/desktop/players/controls/sliderView.tpl", "underscore", "jquery", "marionette"], function(e, t, i, n) {
    "use strict";
    var s = n.ItemView.extend({
        className: "player-slider",
        template: e,
        dragging: !1,
        ui: {
            thumb: ".player-slider-thumb",
            track: ".player-slider-track",
            progress: ".player-slider-progress"
        },
        events: {
            mousedown: "onMouseDown",
            touchstart: "onTouchStart"
        },
        onlyAfterRender: ["displayValue"],
        initialize: function(e) {
            t.bindAll(this, "onMouseMove", "onMouseUp", "onTouchMove", "onTouchEnd"), this.$document = i(document), this.currentValue = e.value || 0
        },
        startDragging: function() {
            this.dragging = !0, this.$el.addClass("active")
        },
        stopDragging: function() {
            this.dragging = !1, this.$el.removeClass("active"), this.triggerMethod("dragComplete")
        },
        updateForPosition: function(e, t) {
            var i = this.getValueForPosition(e);
            this.setValue(i), this.displayValue(i, t)
        },
        getValueForPosition: function(e) {
            var t = this.ui.track,
                i = e - t.offset().left,
                n = i / t.width();
            return n = Math.min(Math.max(n, 0), 1), n = Math.round(100 * n)
        },
        setValue: function(e) {
            this.currentValue = e, this.trigger("change:value", e)
        },
        displayValue: function(e) {
            this.ui.thumb.css("left", e + "%"), this.ui.progress.css("width", e + "%"), this.$el.data("value", e)
        },
        onRender: function() {
            this.displayValue(this.currentValue)
        },
        onClose: function() {
            this.$document.off("mousemove", this.onMouseMove), this.$document.off("mouseup", this.onMouseUp), this.$document.off("touchmove", this.onTouchMove), this.$document.off("touchend", this.onTouchEnd)
        },
        onMouseDown: function(e) {
            e.preventDefault(), this.$document.on("mousemove", this.onMouseMove), this.$document.on("mouseup", this.onMouseUp), this.startDragging(), this.setValue(this.getValueForPosition(e.pageX))
        },
        onTouchStart: function(e) {
            this.$document.on("touchmove", this.onTouchMove), this.$document.on("touchend", this.onTouchEnd), this.startDragging();
            var t = e.originalEvent.touches[0];
            t && this.updateForPosition(t.pageX)
        },
        onMouseMove: function(e) {
            e.preventDefault(), this.updateForPosition(e.pageX)
        },
        onTouchMove: function(e) {
            e.preventDefault();
            var t = e.originalEvent.touches[0];
            t && this.updateForPosition(t.pageX)
        },
        onMouseUp: function(e) {
            e.preventDefault(), this.$document.off("mousemove", this.onMouseMove), this.$document.off("mouseup", this.onMouseUp), this.updateForPosition(e.pageX), this.stopDragging()
        },
        onTouchEnd: function(e) {
            this.$document.off("touchmove", this.onTouchMove), this.$document.off("touchend", this.onTouchEnd);
            var t = e.originalEvent.touches[0];
            t && this.updateForPosition(t.pageX), this.stopDragging()
        }
    });
    return s
})