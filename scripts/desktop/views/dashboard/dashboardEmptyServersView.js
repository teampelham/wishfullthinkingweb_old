define([
	"templates/desktop/dashboard/dashboardEmptyServersView.tpl", 
	"marionette", 
	"base/models/config", 
	"base/utils/dispatcher", 
	"desktop/views/modals/localServerTestModal"
], function(template, marionette, config, dispatcher, LocalServerTestModel) {
    "use strict";
    
	var dashboardEmptyServersList = marionette.ItemView.extend({
        className: "dashboard-empty-servers-message",
        template: template,
        events: {
            "click .local-server-test-link": "onLocalServerTestClick"
        },
		
        serializeData: function() {
            return {
                hasSharedServers: this.options.hasSharedServers,
                isManagedUser: this.options.isManagedUser,
                bundled: config.get("bundled")
            }
        },
        
		onLocalServerTestClick: function(e) {
            e.preventDefault(); 
			dispatcher.trigger("show:modal", new LocalServerTestModel());
        }
    });
	
    return dashboardEmptyServersList;
});