define([
    "templates/desktop/dashboard/dashboardMediaView.tpl", 
    "underscore", 
    "jquery", 
    "marionette", 
    "base/utils/t", 
    "base/utils/dispatcher", 
    "base/models/appModel", 
    "base/models/library/metadataCollection", 
    "desktop/views/lists/library/horizontalMediaTileList"
], function(template, _, $, marionette, t, dispatcher, appModel, MetadataCollection, HorizontalMediaTileList) {
    "use strict";
    
    var dashboardMediaView = marionette.Layout.extend({
        className: "dashboard-container hidden",
        template: template,
        
        ui: {
            previousBtn: ".previous-btn",
            nextBtn: ".next-btn",
            list: ".dashboard-list-container"
        },
        
        regions: {
            list: ".dashboard-list-container"
        },
        
        events: {
            "click .previous-btn": "onPreviousClick",
            "click .next-btn": "onNextClick"
        },
        
        onlyAfterRender: ["onResize"],
        
        initialize: function(container) {
            this.key = container.key; 
            this.label = t("onDeck" === container.key ? "On Deck" : "Recently Added"); 
            this.listenTo(dispatcher, "resize", this.onResize); 
            this.listenTo(appModel, "change:primaryServer", this.setup); 
            this.setup();
        },
        
        setup: function() {
            var primaryServer = appModel.get("primaryServer");
            this.$el.addClass("hidden"); 
            this.list.close(); 
            if (primaryServer && (!primaryServer.get("shared") || primaryServer.get("multiuser"))) { 
                this.collection = new MetadataCollection([], {
                    server: primaryServer,
                    url: "/device/" + this.key
                }); 
                this.listenToOnce(this.collection, "reset", this.onReset); 
                this.collection.fetch({
                    reset: true,
                    scope: "page"
                });
            }
        },
        
        serializeData: function() {
            return {
                label: this.label
            };
        },
        
        onResize: function() {
            if (this.list.currentView && this.list.currentView.measureList) 
                this.list.currentView.measureList();
        },
        
        onReset: function(collection) {
            if (collection.length) {
                this.$el.removeClass("hidden");
                var tileList = new HorizontalMediaTileList({
                    collection: collection,
                    $container: this.$el,
                    continuousPlay: true
                });
                this.listenTo(tileList, "scroll", this.onListScroll); 
                this.list.show(tileList);
            }
        },
        
        onListScroll: function(e, t, i) {
            this.ui.previousBtn.toggleClass("disabled", 1 >= t); 
            this.ui.nextBtn.toggleClass("disabled", t >= e.pages); 
            this.ui.list.css("transform", "translate(" + i + "px, 0)");
        },
        
        onPreviousClick: function(e) {
            e.preventDefault(); 
            if (!this.ui.previousBtn.isDisabled()) 
                this.list.currentView.previous();
        },
        
        onNextClick: function(e) {
            e.preventDefault(); 
            if (!this.ui.nextBtn.isDisabled()) 
                this.list.currentView.next();
        }
    });
    
    return dashboardMediaView;
});