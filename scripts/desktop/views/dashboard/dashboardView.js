define([
	"templates/desktop/dashboard/dashboardView.tpl", 
	"marionette", 
	"base/utils/dispatcher", 
	"base/adapters/settingsAdapter", 
	"base/models/appModel", 
	"desktop/views/sidebar/dashboardSideBarView", 
	"desktop/views/dashboard/dashboardMediaView", 
	//"desktop/views/dashboard/dashboardChannelsView", 
	//"desktop/views/dashboard/dashboardUserPlaylistView"
], function(template, marionette, dispatcher, settingsAdapter, appModel, DashboardSideBarView, DashboardMediaView) {
    "use strict";
	
    var dashboardView = marionette.Layout.extend({
        className: "priorities-container",
        template: template,
        
		ui: {
            1: ".priority-1-container",
            2: ".priority-2-container",
            3: ".priority-3-container",
            4: ".priority-4-container",
            5: ".priority-5-container"
        },
		
        regions: {
            onDeck: ".on-deck-container",
            recentlyAdded: ".recently-added-container",
            channels: ".channels-container",
            queue: ".queue-container",
            recommendations: ".recommendations-container"
        },
		
        initialize: function() {
            this.primaryServer = appModel.get("primaryServer");
            this.onDeckPriority = settingsAdapter.get("dashboardPriorityOnDeck");
            //this.recentlyAddedPriority = settingsAdapter.get("dashboardPriorityRecentlyAdded");
            //this.channelsPriority = settingsAdapter.get("dashboardPriorityChannels");
            //this.queuePriority = settingsAdapter.get("dashboardPriorityQueue");
            //this.recommendationsPriority = settingsAdapter.get("dashboardPriorityRecommendations");
            dispatcher.trigger("show:bar", "sidebar", new DashboardSideBarView());
            this.listenTo(appModel, "change:primaryServer", this.onPrimaryServerChange);
            
            if (this.primaryServer)
                this.listenTo(this.primaryServer, "change:shared", this.onPrimaryServerSharedChange);
        },
		
        onRender: function() {
            if (settingsAdapter.get("dashboardOnDeckEnabled")) 
                this.onDeck.append(new DashboardMediaView({
                    key: "onDeck"
                }), this.ui[this.onDeckPriority]); 
            
            if (settingsAdapter.get("dashboardRecentlyAddedEnabled")) 
                this.recentlyAdded.append(new DashboardMediaView({
                    key: "recentlyAdded"
                }), this.ui[this.recentlyAddedPriority]);
                
            if (settingsAdapter.get("dashboardChannelsEnabled")) 
                this.channels.append(new o, this.ui[this.channelsPriority]); 
            
            if (settingsAdapter.get("dashboardQueueEnabled")) 
                this.queue.append(new l({
                    key: "queue"
                }), this.ui[this.queuePriority]); 
                
            if (settingsAdapter.get("dashboardRecommendationsEnabled")) 
                this.recommendations.append(new l({
                    key: "recommendations"
                }), this.ui[this.recommendationsPriority]);
        },
		
        onClose: function() {
            dispatcher.trigger("hide:bar", "sidebar");
        },
		
        onPrimaryServerChange: function(e, server) {
            if (this.primaryServer) 
                this.stopListening(this.primaryServer); 
            
            this.primaryServer = server; 
            if (this.primaryServer) 
                this.listenTo(this.primaryServer, "change:shared", this.onPrimaryServerSharedChange);
        },
		
        onPrimaryServerSharedChange: function() {
			console.warn('');
            //i.trigger("navigate:refresh")
        }
    });
	
    return dashboardView;
});