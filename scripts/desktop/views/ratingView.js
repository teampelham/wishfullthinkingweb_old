define([
	"templates/desktop/ratingView.tpl", 
	"jquery", 
	"underscore", 
	"desktop/views/players/controls/sliderView", 
	"base/commands/commands"
], function(e, t, i, n, s) {
    "use strict";
    var a = n.extend({
        tagName: "span",
        className: "rating",
        template: e,
        disabled: !0,
        modelEvents: {
            "change:rating": "onRatingChange",
            "change:userRating": "onRatingChange"
        },
        events: i.extend({
            mouseenter: "onMouseEnter",
            mouseleave: "onMouseLeave",
            mousemove: "onMouseMove"
        }, n.prototype.events),
        initialize: function(e) {
            n.prototype.initialize.apply(this, arguments), this.disabled = e.disabled || this.model.server.get("shared"), this.passive = e.passive, this.updateRating()
        },
        updateRating: function() {
            this.metadataRating = this.model.get("rating") || 0, this.userRating = this.model.get("userRating"), this.setValue(this.userRating > 0 ? this.userRating : this.metadataRating)
        },
        normalizeRating: function(e) {
            return Math.round(e / 2)
        },
        serializeData: function() {
            var e = this.normalizeRating(this.currentValue);
            return {
                stars: [e > 0, e > 1, e > 2, e > 3, e > 4]
            }
        },
        getValueForPosition: function(e) {
            var t = this.$el,
                i = e - t.offset().left,
                n = i / t.width();
            return n = Math.min(Math.max(n, 0), 1), n = 2 * Math.ceil(5 * n)
        },
        persistValue: function() {
            var e = this.currentValue || -1;
            s.execute("rate", {
                item: this.model,
                rating: e,
                passive: this.passive
            }), this.updateRating(), this.render()
        },
        displayValue: function(e) {
            this.$el.data("value", e);
            var i = this.normalizeRating(e),
                n = this.$(".star-icon");
            n.each(function(e) {
                var n = i > e;
                t(this).toggleClass("star", n).toggleClass("dislikes", !n)
            }), this.$el.toggleClass("unrated", !e)
        },
        onRender: function() {
            n.prototype.onRender.apply(this, arguments), this.$el.toggleClass("unrated", !this.currentValue), this.$el.toggleClass("user-rating", this.userRating > 0), this.$el.toggleClass("disabled", this.disabled)
        },
        onRatingChange: function() {
            this.updateRating(), this.render()
        },
        onMouseEnter: function() {
            this.disabled || (this.$el.addClass("user-rating"), this.$el.removeClass("unrated"))
        },
        onMouseLeave: function() {
            this.disabled || this.render()
        },
        onMouseMove: function(e) {
            if (!this.disabled)
                if (this.dragging) this.updateForPosition(e.pageX);
                else {
                    var t = this.getValueForPosition(e.pageX);
                    this.displayValue(t)
                }
        },
        onDragComplete: function() {
            this.disabled || this.persistValue()
        }
    });
    return a
});