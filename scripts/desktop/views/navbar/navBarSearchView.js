define([
	"templates/desktop/navbar/navBarSearchView.tpl", 
	"underscore", 
	"jquery", 
	"marionette", 
	"base/utils/t", 
	"base/utils/dispatcher"
], function(template, _, $, marionette, t, dispatcher) {
    "use strict";
	
    var navBarSearchView = marionette.ItemView.extend({
        id: "#nav-bar-search-form",
        tagName: "form",
        className: "nav-bar-form nav-bar-left hidden-xs",
        template: template,
        sectionSearchDelay: 500,
        
		ui: {
            searchInput: "#nav-bar-search",
            searchLabel: ".control-label-search",
            clearSearchBtn: ".clear-search-btn"
        },
        
		events: {
            "focus #nav-bar-search": "onSearchFocus",
            "blur #nav-bar-search": "onSearchBlur",
            "keyup #nav-bar-search": "onSearchKeyUp",
            submit: "onSearchSubmit",
            "mousedown .clear-search-btn": "onClearSearchClick"
        },
        
		modelEvents: {
            "change:searchPlaceholder": "onSearchPlaceholderChange",
            "change:searchValue": "onSearchValueChange"
        },
		
        onlyAfterRender: ["focusSearch", "onSearchPlaceholderChange", "onSearchValueChange"],
		
        initialize: function() {
            this.debouncedSearch = _.debounce(this.search, this.sectionSearchDelay); 
            this.listenTo(dispatcher, "shortcut:search", this.focusSearch);
        },
        
        focusSearch: function() {
            this.ui.searchInput.focus();
        },
        
        search: function() {
            console.warn('search');
            /*var e = i.trim(this.ui.searchInput.val()),
                t = this.model.get("searchType");
            return e.length ? void(t ? a.trigger("search:" + t, e) : a.trigger("navigate", "search", {
                query: e
            })) : void this.clearSearch()*/
        },
        
        clearSearch: function() {
            console.warn('testing');
            //var e = this.model.get("searchType");
            //e && a.trigger("search:" + e, "")
        },
        
        onSearchPlaceholderChange: function(e, placeholder) {
            var localPlaceholder = placeholder ? t("Search {1}", placeholder) : "";
            
            this.ui.searchInput.attr("placeholder", localPlaceholder);
        },
        
        onSearchValueChange: function(e, value) {
            if (!this.ui.searchInput.is(document.activeElement)) {
                this.ui.clearSearchBtn.toggleClass("hidden", !value); 
                this.ui.searchInput.val(value);
            }
        },
        
        onSearchFocus: function() {
            this.ui.searchLabel.addClass("selected");
        },
        
        onSearchBlur: function() {
            this.ui.searchLabel.removeClass("selected");
        },
        
        onSearchKeyUp: function(e) {
            console.warn('testing');
            //13 !== e.which && (this.ui.clearSearchBtn.toggleClass("hidden", 0 === this.ui.searchInput.val().length), "section" === this.model.get("searchType") && this.debouncedSearch())
        },
        
        onSearchSubmit: function(e) {
            console.warn('testing');
            //e.preventDefault(), this.sectionSearchTimeout = clearTimeout(this.sectionSearchTimeout), this.search(), this.ui.searchInput.blur()
        },
        
        onClearSearchClick: function(e) {
            console.warn('testing');
            //e.preventDefault(), this.ui.clearSearchBtn.addClass("hidden"), this.ui.searchInput.val(""), this.clearSearch()
        }
    });
	
    return navBarSearchView;
});