define([
	"templates/desktop/navbar/navDropdown.tpl", 
	"underscore", 
	"marionette", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/commands/commands", 
	"base/models/config", 
	"base/views/images/gravatarImage", 
	"desktop/views/modals/user/userSelectModal"
], function(template, _, marionette, t, dispatcher, commands, config, GravatarImage, userSelectModal) {
    "use strict";
	
    var navDropdown = marionette.ItemView.extend({
        id: "nav-dropdown",
        tagName: "li",
        className: "nav-dropdown dropdown",
        template: template,
		
        ui: {
            poster: ".dropdown-poster-toggle .media-poster",
            dropdownMenu: ".dropdown-menu",
            switchUserItem: ".switch-user-item",
            totalBadge: ".total-badge",
            invitesBadge: ".invites-badge",
            announcementsBadge: ".announcements-badge"
        },
		
        events: {
            "click .switch-user-btn": "onSwitchUserClick",
            "click .users-btn": "onUsersClick",
            "click .sign-in-btn": "onSignInClick",
            "click .sign-out-btn": "onSignOutClick"
        },
        
		onlyAfterRender: ["onSignedInChange", "updateBadges"],
        
		bindings: {
            ".username-header": {
                model: "user",
                modelAttribute: "title"
            }
        },
		
        initialize: function() {
            this.user = this.model.get("user"); 
            this.invites = this.user.get("invites"); 
            this.announcements = this.model.get("announcements"); 
            this.image = new GravatarImage({
                model: this.user,
                type: "friend",
                width: 34
            }); 
            this.listenTo(this.user, "change:signedIn", this.onSignedInChange); 
            this.listenTo(this.user, "change:username", this.onUsernameChange); 
            this.listenTo(this.user, "change:home", this.onHomeChange); 
            this.listenTo(this.invites, "reset add remove", this.updateBadges); 
            this.listenTo(this.announcements, "reset add remove change:isUnread", this.updateBadges); 
            
            this.announcements.populate({
                reset: true
            });
        },
		
        serializeData: function() {
			var inviteCount = 0; //this.invites.length;
            var unreadAnnouncementCount = this.getUnreadAnnouncementsCount();
            
            return {
                signedIn: this.user.get("signedIn"),
                home: this.user.get("home"),
                isFullUser: this.user.isFullUser(),
                //invitesCount: inviteCount,
                unreadAnnouncementsCount: unreadAnnouncementCount,
                totalCount: inviteCount + unreadAnnouncementCount,
                accountUrl: this.user.server.get("accountUrl"),
                appsUrl: this.user.server.get("appsUrl"),
                helpUrl: this.user.server.get("helpUrl"),
                defaultTarget: config.get("defaultTarget")
            };
        },
		
        getUnreadAnnouncementsCount: function() {
            var unreadAnnouncements = this.announcements.where({
                isUnread: true
            });
            
            if (this.user.hasPremiumPass()) 
                unreadAnnouncements = _.reject(unreadAnnouncements, function(announcement) {   
                    return "PremiumPass" === announcement.get("promo");
                }); 
            
            return unreadAnnouncements.length;
        },
		
        updateBadges: function() {
            var invitesLength = 0; //this.invites.length;
            var unreadAnnouncementCount = this.getUnreadAnnouncementsCount();
            var badgeCount = invitesLength + unreadAnnouncementCount;
            
            this.ui.totalBadge.toggleClass("hidden", !badgeCount); 
            this.ui.totalBadge.text(badgeCount); 
            this.ui.invitesBadge.toggleClass("hidden", !invitesLength); 
            this.ui.invitesBadge.text(invitesLength); 
            this.ui.announcementsBadge.toggleClass("hidden", !unreadAnnouncementCount); 
            this.ui.announcementsBadge.text(unreadAnnouncementCount);
        },
		
        onRender: function() {
            this.image.setElement(this.ui.poster);
        },
		
        onClose: function() {
            this.image.close()
        },
		
        onSignedInChange: function(e, t) {
			console.warn('testing');
            this.ui.dropdownMenu.toggleClass("signed-in", !! t)
        },
		
        onUsernameChange: function(e, t) {
			console.warn('testing');
            this.ui.dropdownMenu.toggleClass("full-user", !! t)
        },
		
        onHomeChange: function(e) {
			console.warn('testing');
            //this.ui.switchUserItem.toggleClass("hidden", !e.get("home"))
        },
		
        onSwitchUserClick: function(e) {
			console.warn('testing');
            /*e.preventDefault(), s.trigger("show:modal", new l({
                isForced: !0
            }))*/
        },
		
        onUsersClick: function(e) {
			console.warn('testing');
            e.preventDefault(); 
			dispatcher.trigger("navigate", "users");
        },
		
        onSignInClick: function(e) {
			console.warn('testing');
            e.preventDefault(); 
			dispatcher.trigger("navigate", "login");
        },
		
        onSignOutClick: function(e) {
			e.preventDefault();
            commands.execute("signOut").done(_.bind(function() {
                dispatcher.trigger("navigate:refresh:dashboard");
            }, this));
        }
    });
	
    return navDropdown;
});