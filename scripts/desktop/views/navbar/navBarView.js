define([
	"templates/desktop/navbar/navBarView.tpl", 
	//"templates/desktop/tooltips/plexPassTooltip.tpl", 
	"underscore", 
	"marionette", 
	"base/utils/dispatcher", 
	"base/adapters/settingsAdapter", 
	"base/commands/commands", 
	"desktop/views/navbar/navBarSearchView", 
	"desktop/views/navbar/navDropdown", 
	//"desktop/views/dropdowns/primaryPlayerDropdown"
], function(template, _, marionette, dispatcher, settingsAdapter, commands, NavBarSearchView, NavDropdown) {
    "use strict";
	
    var navBarView = marionette.Layout.extend({
        className: "nav-bar",
        template: template,
        sectionSearchDelay: 500,
        
		ui: {
            backBtn: ".back-btn",
            plexPassBtn: ".plex-pass-btn",
            activityBtn: ".activity-btn",
            activityBadge: ".activity-badge"
        },
        
		regions: {
            search: ".nav-bar-search-container",
            navDropdown: ".nav-bar-right",
            playerDropdown: ".nav-bar-right"
        },
        
		events: {
            "click .back-btn": "onBackClick",
            "click .activity-btn": "onActivityClick"
        },
		
        modelEvents: {
            "change:history": "onHistoryChange",
            "change:primaryServer": "onPrimaryServerChange"
        },
		
        onlyAfterRender: ["onSignedInChange", "onHistoryChange", "onPrimaryServerChange", "onNowPlayingChange"],
        
		initialize: function() {
            this.listenTo(this.model.get("user"), "change:signedIn", this.onSignedInChange); 
            this.listenTo(dispatcher, "change:language", this.render); 
            this.setupServerListeners();
        },
		
        setupServerListeners: function() {
			if (this.primaryServer) {
                this.stopListening(this.primaryServer);
                this.stopListening(this.primaryServer.get("sessions"));
            }
            
            this.primaryServer = this.model.get("primaryServer");
            
            if (this.primaryServer) {
                var sessions = this.primaryServer.get("sessions");
                this.listenTo(this.primaryServer, "change:shared", this.onPrimaryServerSharedChange);
                this.listenTo(sessions, {
                    reset: this.onNowPlayingChange,
                    add: this.onNowPlayingChange,
                    remove: this.onNowPlayingChange
                });
            }
        },
		
        serializeData: function() {
			var user = this.model.get("user");
            var sessions = this.primaryServer && this.primaryServer.get("sessions");
            
            return {
                plexPassUrl: user.server.get("plexPassUrl"),
                hasHistory: this.hasHistory(),
                canGoPremium: this.canGoPremium(),
                showActivity: !! this.primaryServer,
                activityCount: sessions && sessions.length
            };
        },
		
        hasHistory: function() {
            var history = this.model.get("history");
            return history.length > 1;
        },
		
        canGoPremium: function() {
            var user = this.model.get("user");
            return user.get("signedIn") && !user.get("restricted") && !user.hasPremiumPass();
        },
		
        showPremiumPassTooltip: function() {
            if (this.canGoPremium() && !settingsAdapter.get("hasSeenHolidayTooltip"))
                _.defer(_.bind(function() {
                    if (!this.isClosed) {
                        settingsAdapter.save("hasSeenHolidayTooltip", true); 
                        this.ui.plexPassBtn.focus();
                    }
                }, this));
        },
		
        onRender: function() {
			this.search.append(new NavBarSearchView({
                model: this.model
            }));
            
            this.navDropdown.append(new NavDropdown({
                model: this.model
            }));
            
            this.$el.tooltip({
                selector: '[data-toggle="tooltip"]',
                container: "body",
                placement: "bottom"
            });
             
            /*this.playerDropdown.append(new c({
                showNowPlaying: !0
            })), 
            this.ui.plexPassBtn.tooltip({
                container: "body",
                placement: "bottom",
                template: '<div class="tooltip tooltip-plex-pass" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
                title: t(),
                html: !0,
                trigger: "hover focus"
            }), */
            this.showPremiumPassTooltip();
        },
		
        onSignedInChange: function() {
            this.ui.plexPassBtn.toggleClass("hidden", !this.canGoPremium()); 
            this.showPremiumPassTooltip();
        },
		
        onHistoryChange: function() {
            this.ui.backBtn.toggleClass("hidden", !this.hasHistory());
        },
		
        onPrimaryServerChange: function(e, server) {
            this.setupServerListeners();
            var serverExists = !! server;
            this.ui.activityBtn.toggleClass("hidden", !serverExists);
        },
		
        onPrimaryServerSharedChange: function(e, t) {
            this.ui.activityBtn.toggleClass("hidden", !! t);
        },
		
        onNowPlayingChange: function() {
            var sessions = this.primaryServer.get("sessions");
            var sessionCount = sessions.length;
            this.ui.activityBtn.parent("li").toggleClass("active", !! sessionCount); 
            this.ui.activityBadge.toggleClass("hidden", !sessionCount); 
            this.ui.activityBadge.text(sessionCount);
        },
		
        onBackClick: function(e) {
            e.preventDefault();
            dispatcher.trigger("navigate:back");
        },
		
        onActivityClick: function(e) {
            e.preventDefault();
            var sessions = this.primaryServer && this.primaryServer.get("sessions");
            
            if (sessions && sessions.length) 
                dispatcher.trigger("navigate", "nowPlaying"); 
            else
                dispatcher.trigger("navigate", "activity");
        }
    });
	
    return navBarView;
});