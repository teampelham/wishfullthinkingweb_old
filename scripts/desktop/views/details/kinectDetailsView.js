define([
    "templates/desktop/details/kinectDetailsView.tpl", 
    "base/mixins/details/showDetailsMixin", 
    "desktop/views/details/baseDetailsView", 
    "desktop/views/ratingView", 
    "desktop/views/details/components/videoControlsView", 
    "desktop/views/details/components/kinectContextView", 
    "base/views/images/mediaFlagImage", 
    "desktop/views/lists/library/seasonTileList",
    "base/utils/dispatcher"
], function(template, showDetailsMixin, baseDetailsView, RatingView, VideoControlsView, KinectContextView, MediaFlagImage, seasonTileList, dispatcher) {
    "use strict";
    
    var kinectDetailsView = baseDetailsView.extend({
        mixins: showDetailsMixin,
        className: "show-details-row details-row",
        template: template,
        allowMirror: false,
        
        initialize: function() {
            baseDetailsView.prototype.initialize.apply(this, arguments); 
            this.ListConstructor = seasonTileList; 
            this.ratingView = new VideoControlsView({
                model: this.model
            }); 
            this.summaryView = new KinectContextView({
                model: this.model
            }); 

            this.contentRatingFlag = new MediaFlagImage({
                model: this.model,
                modelAttribute: "contentRating",
                className: "show-content-rating content-rating-flag",
                width: 70,
                height: 30
            }); 
            this.studioFlag = new MediaFlagImage({
                model: this.model,
                modelAttribute: "studio",
                className: "show-studio studio-flag",
                width: 200,
                height: 40
            });
            //dispatcher.listenTo("pubsub:video", this.onVideoMessage);
            dispatcher.trigger("send:serverMessage", JSON.stringify({ RegisterEventListener: 'Video Action' }));
        },
        
        
    });
    
    return kinectDetailsView;
})