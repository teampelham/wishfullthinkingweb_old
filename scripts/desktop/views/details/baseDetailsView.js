define([
    "underscore", 
    "backbone", 
    "marionette", 
    "base/utils/t", 
    "base/utils/dispatcher", 
    "base/utils/playerUtil", 
    "base/utils/types/mediaTypes", 
    "base/adapters/playerControllerAdapter", 
    "base/mixins/details/baseDetailsMixin", 
    "base/commands/commands", 
    "base/models/appModel", 
    "desktop/views/modals/mediaInfoModal"
], function(_, bb, marionette, t, dispatcher, playerUtil, mediaTypes, playerControllerAdapter, baseDetailsMixin, commands, appModel, MediaInfoModal) {
    "use strict";

    var baseDetailsView = marionette.Layout.extend({
        mixins: baseDetailsMixin,
        posterWidth: 250,
        ui: {
            poster: ".item-poster"
        },
        
        regions: {
            rating: ".rating-container",
            video: ".video-display-container",
            audio: ".item-audio",
            subtitles: ".item-subtitles",
            studio: ".studio-flag-container",
            contentRating: ".content-rating-flag-container",
            videoResolution: ".video-audio-flags-container",
            audioCodec: ".video-audio-flags-container",
            audioChannels: ".video-audio-flags-container",
            poster: ".details-poster-container",
            list: ".details-list-region"
        },
        
        events: {
            "click .details-poster-container > .play-btn": "onPlayClick",
            "click .item-unavailable-btn": "onUnavailableClick"
        },
        
        initialize: function(initObj) {
            _.bindAll(this, "onResize", "onScroll"); 
            this.mediaActions = initObj.mediaActions; 
            this.collection = this.model.get("children"); 
            this.shared = this.model.server.get("shared"); 
            this.synced = this.model.server.get("synced"); 
            this.multiuser = this.model.server.get("multiuser"); 
            this.progress = new bb.Model({
                loading: false,
                failed: false
            }); 
            
            this.listenTo(appModel, "change:primaryPlayer", this.onPrimaryPlayerChange); 
            if (this.pageSize) {
                dispatcher.trigger("content.subscribe:resize", this.onResize); 
                dispatcher.trigger("content.subscribe:scroll", this.onScroll);
            } 
            
            this.mirror();
        },
        
        serializeData: function() {
            var playQueueManager = this.model.server.get("playQueueManager");
            var grandparentKey = this.model.get("grandparentKey");
            var parentKey = this.model.get("parentKey");
            
            return {
                serverID: this.model.server.id,
                grandparentKey: parentKey ? encodeURIComponent(grandparentKey) : null,
                parentKey: parentKey ? encodeURIComponent(parentKey) : null,
                canPlay: playQueueManager.canCreateFor(this.model),
                limited: this.shared || this.synced
            };
        },
        
        mirror: function() {
            if (this.allowMirror) {
                var mediaType = mediaTypes.getChildMediaType(this.model.get("type"));
                var element = mediaType.element;
                var type = playerUtil.getTypeByElement(element);
                var controller = playerControllerAdapter.getInstance(type, {
                    isPassive: true
                });
                if(controller) 
                    controller.mirror({
                        item: this.model
                    });
            }
        },
        
        populatePage: function() {
            this.progress.set("loading", true);
            
            var promise = this.collection.populateRange({
                start: 0,
                end: this.collection.length + this.pageSize,
                scope: "page"
            });
            
            promise.always(_.bind(function() {
                this.progress.set("loading", false)
            }, this));
            
            return promise;
        },
        
        onRender: function() {
            if (this.collection && this.ListConstructor) {
                var promise = this.pageSize ? this.populatePage() : this.collection.fetch({
                    scope: "page"
                }); 
                
                promise.fail(_.bind(function() {
                    dispatcher.trigger("show:alert", {
                        type: "error",
                        message: t("There was a problem loading these items.")
                    }); 
                    
                    this.progress.set("failed", true);
                }, this)); 
                
                this.list.append(new this.ListConstructor({
                    model: this.model,
                    mediaActions: this.mediaActions,
                    collection: this.collection,
                    itemViewOptions: this.listItemViewOptions
                }));
            }
            
            if (this.ratingView) 
                this.rating.append(this.ratingView); 
                
            if (this.audioDropdown) 
                this.audio.append(this.audioDropdown); 
            
            if (this.subtitlesDropdown) 
                this.subtitles.append(this.subtitlesDropdown); 
            
            if (this.posterImage) 
                this.posterImage.setElement(this.ui.poster); 
                
            if (this.studioFlag) 
                this.studio.show(this.studioFlag); 
                
            if (this.contentRatingFlag) 
                this.contentRating.show(this.contentRatingFlag); 
                
            if (this.videoResolutionFlag) 
                this.videoResolution.append(this.videoResolutionFlag); 
                
            if (this.audioCodecFlag) 
                this.audioCodec.append(this.audioCodecFlag); 
                
            if (this.audioChannelsFlag) 
                this.audioChannels.append(this.audioChannelsFlag); 
                
            if (this.summaryView) 
                this.video.append(this.summaryView);
        },
        
        onClose: function() {
            if (this.posterImage) 
                this.posterImage.close(); 
            
            if (this.pageSize) {
                dispatcher.trigger("content.unsubscribe:resize", this.onResize); 
                dispatcher.trigger("content.unsubscribe:scroll", this.onScroll);
            }
        },
        
        onPrimaryPlayerChange: function(e, player) {
            if(player) 
                this.mirror();
        },
        
        onPlayClick: function(e) {
            e.preventDefault(); 
            commands.execute("playMedia", {
                item: this.model
            });
        },
        
        onUnavailableClick: function(e) {
            e.preventDefault(); 
            dispatcher.trigger("show:modal", new MediaInfoModal({
                model: this.model
            }));
        },
        
        onResize: function(size) {
            this.containerSize = size;
        },
        
        onScroll: function(e) {
            if (!this.progress.get("loading")) {
                var children = this.model.get("children");
                var height = e.scrollHeight - this.containerSize.innerHeight;
                var isTop = height - e.scrollTop < 10;
                var notBottom = children.length < children.totalSize;
                
                if (isTop && notBottom) 
                    this.populatePage();
            }
        }
    });
    
    return baseDetailsView;
});