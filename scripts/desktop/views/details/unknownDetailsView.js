define([
    "templates/desktop/details/unknownDetailsView.tpl", 
    "marionette"
], function(e, t) {
    "use strict";
    var i = t.ItemView.extend({
        className: "container",
        template: e
    });
    return i
});