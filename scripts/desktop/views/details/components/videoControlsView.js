define([
    "templates/desktop/details/components/videoControlsView.tpl", 
    "underscore", 
    "jquery", 
    "marionette", 
    "base/utils/dispatcher"
], function(template, _, $, marionette, dispatcher) {
    "use strict";

    var videoControlsView = marionette.ItemView.extend({
        className: "video-buttons",
        template: template,
        ui: {
            summary: ".metadata-summary",
            divider: ".summary-divider",
            dividerBtn: ".summary-divider-btn"
        },
        
        events: {
            "click .summary-divider-btn": "onMoreSummaryClick"
        },
        
        /*bindings: {
            ".item-summary": {
                modelAttribute: "summary",
                autohide: !0,
                isHtml: !0,
                formatter: function(e) {
                    this.$el.toggleClass("hidden", !e), this.updateDividerBtnVisibility();
                    var n = t.map(t.compact(e.split("\n")), function(e) {
                        return i.trim(t.escape(e))
                    });
                    return n.length ? "<p>" + n.join("</p><p>") + "</p>" : void 0
                }
            }
        },*/
        
        initialize: function() {

        },
        
        onMoreSummaryClick: function(e) {
            var command = $(e.currentTarget).prop("id") == "videoOnButton" ? "sendVideo" : "stopVideo";
            var message = { Data: command, Path: "video" };
            dispatcher.trigger("send:serverMessage", message, "video");
        }
    });
    return videoControlsView;
});