define([
    "templates/desktop/details/components/kinectContextView.tpl", 
    "underscore", 
    "jquery", 
    "marionette", 
    "base/utils/dispatcher"
], function(template, _, $, marionette, dispatcher) {
    "use strict";
	
	var kinectContextView = marionette.ItemView.extend({
        className: "summary",
        template: template,
		ui: {
            context: "#kinectImg",
        },
		
		onlyAfterRender: ["onNewFrame"],
		
		initialize: function() {
			
            this.listenTo(dispatcher, "pubsub:VideoFrame", this.onNewFrame);
        },
		
		onRender: function() {
			this.canvas = this.ui.context[0].getContext('2d');
			var data = this.model.get("thumb");
			if(!data)
				return;
			
			var img = new Image();
			img.onload = _.bind(function () {
				this.canvas.drawImage(img, 0, 0);
			}, this);
			img.src = "data:image/bmp;base64," + data.substring(1, data.length - 1);
		},
		
		onNewFrame: function(e, message) {
			if (message.type == "VideoFrame") {
				// Get the data in JSON format.
				var img = new Image();
				img.onload = _.bind(function () {
					this.canvas.drawImage(img, 0, 0);
				}, this);
				img.src = "data:image/bmp;base64," + message.data.substring(1, message.data.length - 1);
			}
		}
	});
	
	return kinectContextView;
});