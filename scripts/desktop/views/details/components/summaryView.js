define([
    "templates/desktop/details/components/summaryView.tpl", 
    "underscore", 
    "jquery", 
    "marionette", 
    "base/utils/t", 
    "base/utils/dispatcher"
], function(e, t, i, n, s, a) {
    "use strict";

    var r = n.ItemView.extend({
        className: "summary",
        template: e,
        ui: {
            summary: ".metadata-summary",
            divider: ".summary-divider",
            dividerBtn: ".summary-divider-btn"
        },
        events: {
            "click .summary-divider-btn": "onMoreSummaryClick"
        },
        onlyAfterRender: ["onDeferredRender", "updateDividerBtnVisibility"],
        bindings: {
            ".item-summary": {
                modelAttribute: "summary",
                autohide: !0,
                isHtml: !0,
                formatter: function(e) {
                    this.$el.toggleClass("hidden", !e), this.updateDividerBtnVisibility();
                    var n = t.map(t.compact(e.split("\n")), function(e) {
                        return i.trim(t.escape(e))
                    });
                    return n.length ? "<p>" + n.join("</p><p>") + "</p>" : void 0
                }
            }
        },
        initialize: function() {
            this.listenTo(a, "resize", this.onResize)
        },
        updateDividerBtn: function(e) {
            var t = s(e ? "More" : "Less");
            this.ui.dividerBtn.html(t)
        },
        updateDividerBtnVisibility: function() {
            if (this.originalHeight) {
                var e = this.ui.summary.prop("scrollHeight");
                e <= this.originalHeight ? this.ui.divider.addClass("hidden") : (this.updateDividerBtn(!this.expanded), this.ui.divider.removeClass("hidden"))
            }
        },
        onRender: function() {
            t.defer(t.bind(this.onDeferredRender, this))
        },
        onDeferredRender: function() {
            this.expanded = !1;
            var e = this.ui.summary.prop("scrollHeight"),
                t = this.ui.summary.prop("clientHeight");
            this.originalHeight = t, e > t && (this.ui.summary.css("max-height", t + "px"), this.ui.divider.removeClass("hidden"))
        },
        onResize: function() {
            this.isClosed || this.updateDividerBtnVisibility()
        },
        onMoreSummaryClick: function() {
            var e = this.ui.summary.prop("scrollHeight") + 100;
            this.expanded ? (this.ui.summary.css("max-height", this.originalHeight + "px"), this.expanded = !1) : (this.ui.summary.css("max-height", e + "px"), this.expanded = !0), this.updateDividerBtn(!this.expanded)
        }
    });
    return r
});