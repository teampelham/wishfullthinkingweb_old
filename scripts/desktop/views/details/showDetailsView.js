define([
    "templates/desktop/details/showDetailsView.tpl", 
    "base/mixins/details/showDetailsMixin", 
    "desktop/views/details/baseDetailsView", 
    "desktop/views/ratingView", 
    "desktop/views/details/components/summaryView", 
    "base/views/images/mediaPosterImage", 
    "base/views/images/mediaFlagImage", 
    "desktop/views/lists/library/seasonTileList"
], function(e, t, i, n, s, a, r, o) {
    "use strict";
    
    var showDetailsView = i.extend({
        mixins: t,
        className: "show-details-row details-row",
        template: e,
        allowMirror: !0,
        initialize: function() {
            i.prototype.initialize.apply(this, arguments); 
            this.ListConstructor = o; 
            this.ratingView = new n({
                model: this.model
            }); 
            this.summaryView = new s({
                model: this.model
            }); 
            this.posterImage = new a({
                model: this.model,
                type: this.model.get("type"),
                width: this.posterWidth
            }); 
            this.contentRatingFlag = new r({
                model: this.model,
                modelAttribute: "contentRating",
                className: "show-content-rating content-rating-flag",
                width: 70,
                height: 30
            }); 
            this.studioFlag = new r({
                model: this.model,
                modelAttribute: "studio",
                className: "show-studio studio-flag",
                width: 200,
                height: 40
            });
        }
    });
    
    return showDetailsView;
});