define([
    "templates/desktop/details/detailsView.tpl", 
    "underscore", 
    "marionette", 
    "base/utils/dispatcher", 
    "base/adapters/settingsAdapter", 
    "base/commands/commands", 
    "desktop/models/mediaActionsModel", 
    "desktop/views/actionbar/actionBarView", 
    "desktop/views/breadcrumbbar/detailsBreadcrumbBarView", 
    //"desktop/views/details/movieDetailsView", 
    "desktop/views/details/showDetailsView", 
    "desktop/views/details/kinectDetailsView",
    //"desktop/views/details/seasonDetailsView", 
    //"desktop/views/details/episodeDetailsView", 
    //"desktop/views/details/artistDetailsView", 
    //"desktop/views/details/albumDetailsView", 
    //"desktop/views/details/photoAlbumDetailsView", 
    //"desktop/views/details/clipDetailsView", 
    //"desktop/views/details/playlistDetailsView", 
    "desktop/views/details/unknownDetailsView"
], function(template, t, i, dispatcher, settingsAdapter, commands, MediaActionsModel, ActionBarView, DetailsBreadcrumbBarView, showDetailsView, kinectDetailsView, UnknownDetailsView) {
    "use strict";
    
    var mediaTypeViews = {  // b
        //movie: c,
        show: showDetailsView,
        kinect: kinectDetailsView
        //season: u,
        //episode: h,
        //artist: p,
        //album: m,
        //photoAlbum: f,
        //clip: g,
        //userPlaylistItem: g,
        //playlist: v
    };
    
    var detailOptions = {
        playlist: {
            blur: 5,
            saturation: 50
        }
    };
     
    var detailsView = i.Layout.extend({
        className: "details-container",
        template: template,
        regions: {
            details: ".row"
        },
        
        events: {
            "click .pivot-link": "onPivotClick"
        },
        
        initialize: function() {
            var theme = this.model.get("grandparentTheme") || this.model.get("parentTheme") || this.model.get("theme");
            var type = this.model.get("type");
            var filters = {};
                
            if(_(detailOptions).has(type)) 
                filters = detailOptions[type];
            
            this.mediaActions = new MediaActionsModel({}, {
                item: this.model,
                type: type,
                showAdvanced: true
            });
             
            this.once("render", _.bind(function() {
                dispatcher.trigger("show:background", {
                    model: this.model,
                    imageFilters: filters
                });
                 
                dispatcher.trigger("show:bar", "actionbar", new ActionBarView({
                    model: this.model,
                    mediaActions: this.mediaActions
                })); 
                
                if (this.model.isLibraryItem()) 
                    dispatcher.trigger("show:bar", "breadcrumbbar", new DetailsBreadcrumbBarView({
                        model: this.model
                    }));
            }, this));
             
            if (theme && settingsAdapter.get("playThemeMusic")) 
                commands.execute("playSound", {
                    key: this.model.get("key"),
                    url: this.model.server.resolveUrl(theme, {
                        appendToken: true
                    }),
                    volume: 50,
                    autoLoad: true,
                    autoPlay: true,
                    background: true
                });
        },
        
        onRender: function() {
            var view; 
            var type = this.model.get("type");
            
            if (_(mediaTypeViews).has(type)) {
                var viewType = mediaTypeViews[type];
                
                view = new viewType({
                    model: this.model,
                    mediaActions: this.mediaActions
                });
            } 
            else 
                view = new UnknownDetailsView({
                    model: this.model
                });
            
            this.details.append(view, this.$el);
        },
        
        onClose: function() {
            dispatcher.trigger("hide:bar", "actionbar"); 
            if (this.model.isLibraryItem()) 
                dispatcher.trigger("hide:bar", "breadcrumbbar");
        },
        
        onPivotClick: function() {
            var section = this.model.section;
            if (section) {
                var items = section.get("items");
                items.scrollPosition = 0;
            }
        }
    });
    
    return detailsView;
});