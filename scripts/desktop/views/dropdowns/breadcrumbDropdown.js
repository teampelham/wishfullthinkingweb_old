define([
	"templates/desktop/dropdowns/breadcrumbDropdown.tpl", 
	"jquery", 
	"marionette", 
	"base/utils/dispatcher"
], function(e, t, i, n) {
    "use strict";
	
    var s = i.ItemView.extend({
        tagName: "span",
        className: "details-breadcrumb dropdown",
        template: e,
        events: {
            "click .breadcrumb-btn": "onClick"
        },
        initialize: function(e) {
            this.listenTo(this.model, "change:title", this.render), this.isLeaf = e && e.isLeaf || !1
        },
        serializeData: function() {
            var e = this.collection && this.collection.length > 1;
            return {
                id: this.id,
                item: this.model.toJSON(),
                items: e ? this.collection.toJSON() : null,
                hasDetails: this.isLeaf,
                showDropdown: e
            }
        },
        onClick: function(e) {
            e.preventDefault();
            var i = t(e.currentTarget).data("key");
            i && n.trigger("navigate", "details", {
                serverID: this.model.server.id,
                key: i
            })
        }
    });
    return s
});