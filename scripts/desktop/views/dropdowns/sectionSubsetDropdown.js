define([
	"templates/desktop/dropdowns/sectionSubsetDropdown.tpl", 
	"marionette"
], function(template, marionette) {
    "use strict";
	
    var sectionSubsetDropdown = marionette.ItemView.extend({
        id: "section-subset-dropdown",
        tagName: "span",
        className: "dropdown",
        template: template,
        collectionEvents: {
            reset: "render"
        },
		
        initialize: function() {
            this.collection.populate({
                reset: true,
                scope: "page"
            })
        },
		
        serializeData: function() {
            var model = this.collection.get(this.model.id) || this.model;
            
            return {
                showDropdown: this.collection.length > 1,
                subsets: this.collection.toJSON(),
                currentSubset: model.toJSON(),
                serverID: this.model.server.id,
                sectionID: this.model.section.id
            }
        }
    });
	
    return sectionSubsetDropdown;
});