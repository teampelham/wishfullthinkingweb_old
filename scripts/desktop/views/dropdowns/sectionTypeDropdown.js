define([
	"templates/desktop/dropdowns/sectionTypeDropdown.tpl", 
	"marionette", 
	"desktop/views/dropdowns/sectionTypeDropdownItem"
], function(template, marionette, sectionTypeDropdownItem) {
    "use strict";
	
    var sectionTypeDropdown = marionette.CompositeView.extend({
        id: "section-type-dropdown",
        tagName: "span",
        className: "dropdown",
        template: template,
        itemView: sectionTypeDropdownItem,
        itemViewContainer: ".dropdown-menu",
        modelEvents: {
            "change:currentType": "render",
            "change:currentSubset": "render"
        },
		
        serializeData: function() {
            return {
                currentType: this.model.get("currentType").toJSON()
            };
        },
		
        onRender: function() {
            var bigCollection = this.collection.length < 2;
            var allowAdvanced = this.model.get("allowAdvancedFilters");
            var notAll = "all" !== this.model.get("currentSubset").id;
			
            this.$el.toggleClass("hidden", bigCollection || !allowAdvanced || notAll);
        }
    });
	
    return sectionTypeDropdown;
});