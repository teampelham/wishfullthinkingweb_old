define([
	"templates/desktop/dropdowns/sectionDropdown.tpl", 
	"marionette"
], function(template, marionette) {
    "use strict";
	
    var sectionDropdown = marionette.ItemView.extend({
        id: "section-dropdown",
        tagName: "span",
        className: "dropdown",
        template: template,
        modelEvents: {
            "change:title": "render"
        },
		
        serializeData: function() {
            var playlists = this.model.server.supports("playlists");
            
			return {
                showDropdown: this.collection.length > 1 || playlists,
                showPlaylists: playlists,
                sections: this.collection.toJSON(),
                serverID: this.model.server.id,
                currentSection: this.model.toJSON()
            }
        }
    });
	
    return sectionDropdown;
});