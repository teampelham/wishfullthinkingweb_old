define([
	"templates/desktop/dropdowns/sectionTypeDropdownItem.tpl", 
	"marionette"
], function(template, marionette) {
    "use strict";
	
    var sectionTypeDropdownItem = marionette.ItemView.extend({
        tagName: "li",
        template: template,
        events: {
            "click a": "onClick"
        },
		
        onClick: function(e) {
            e.preventDefault();
            
			var section = this.model.section;
            var currentType = section.get("currentType");
			
            currentType.set("selected", false); 
			this.model.set("selected", true); 
			this.model.section.set("currentType", this.model)
        }
    });
	
    return sectionTypeDropdownItem;
});