define([
	"templates/desktop/dropdowns/mediaActionsDropdown.tpl", 
	"underscore", 
	"jquery", 
	"marionette", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/utils/formatters/stringUtil", 
	"base/utils/types/mediaTypes", 
	"base/commands/commands", 
	"base/models/library/metadataModel", 
	"desktop/models/mediaActionsModel", 
	//"desktop/views/modals/addToPlaylistModal", 
	//"desktop/views/modals/shareModal", 
	//"desktop/views/modals/mediaInfoModal", 
	//"desktop/views/modals/recommendModal"
], function(e, t, i, n, s, a, r, o, l, c, d) {
    "use strict";
    var f = n.ItemView.extend({
        tagName: "ul",
        className: "dropdown-menu",
        template: e,
        ui: {
            divider: ".divider"
        },
        events: {
            "click .play-all-btn": "onPlayAllClick",
            "click .play-shuffled-btn": "onPlayShuffledClick",
            "click .play-next-btn": "onPlayNextClick",
            "click .add-to-up-next-btn": "onAddToUpNextClick",
            "click .play-mix-btn": "onPlayMixClick",
            "click .play-primary-extra-btn": "onPlayPrimaryExtraClick",
            "click .add-to-playlist-btn": "onAddToPlaylistClick",
            "click .refresh-btn": "onRefreshClick",
            "click .sync-btn": "onSyncClick",
            "click .mark-watched-btn": "onMarkWatchedClick",
            "click .mark-unwatched-btn": "onMarkUnwatchedClick",
            "click .watch-later-btn": "onWatchLaterClick",
            "click .share-btn": "onShareClick",
            "click .recommend-btn": "onRecommendClick",
            "click .delete-btn": "onDeleteClick",
            "click .info-btn": "onInfoClick"
        },
        initialize: function() {
            this.mediaType = o.getMediaTypeByString(this.model.get("type")), this.mediaActions = new d({}, {
                item: this.model
            }), i(".dropdown.open .dropdown-toggle").dropdown("toggle")
        },
        serializeData: function() {
            var e, t;
            if (this.mediaActions.get("showDownload")) {
                e = this.model.getDownloadUrl();
                var i = this.model.getFilepath();
                i && (t = r.getLastSegment(i))
            }
            return {
                downloadUrl: e,
                downloadName: t
            }
        },
        getPosition: function(e, t, i) {
            var n = e.position().left;
            return e.offset().top + this.$el.outerHeight() > t.offset.top + t.outerHeight && (i -= this.$el.height() - e.height()), e.offset().left + this.$el.outerWidth() > t.offset.left + t.width && (n -= this.$el.width() - e.outerWidth()), {
                top: i,
                left: n
            }
        },
        bindingsModel: "mediaActions",
        bindings: {
            ".play-all-btn": {
                modelAttribute: "showPlayAll",
                manual: !0,
                autohide: !0
            },
            ".play-shuffled-btn": {
                modelAttribute: "showShuffle",
                manual: !0,
                autohide: !0
            },
            ".play-next-btn, .add-to-up-next-btn": {
                modelAttribute: "showQueue",
                modelListeners: ["showQueue", "canQueue"],
                manual: !0,
                autohide: !0,
                formatter: function(e, t) {
                    return t.toggleClass("disabled", !this.mediaActions.get("canQueue")), e
                }
            },
            ".play-mix-btn": {
                modelAttribute: "showPlayMix",
                manual: !0,
                autohide: !0,
                formatter: function(e, t) {
                    if (e) {
                        var i = this.model.get("related"),
                            n = i.findWhere({
                                browse: !1
                            }),
                            a = n.get("title");
                        a && t.text(s("Play {1}", a))
                    }
                    return e
                }
            },
            ".play-trailer-btn": {
                modelAttribute: "showPlayTrailer",
                manual: !0,
                autohide: !0
            },
            ".play-music-video-btn": {
                modelAttribute: "showPlayMusicVideo",
                manual: !0,
                autohide: !0
            },
            ".add-to-playlist-btn": {
                modelAttribute: "showAddToPlaylist",
                manual: !0,
                autohide: !0
            },
            ".refresh-btn": {
                modelAttribute: "showRefresh",
                manual: !0,
                autohide: !0
            },
            ".mark-watched-btn": {
                modelAttribute: "showMarkWatched",
                manual: !0,
                autohide: !0
            },
            ".mark-unwatched-btn": {
                modelAttribute: "showMarkUnwatched",
                manual: !0,
                autohide: !0
            },
            ".sync-btn": {
                modelAttribute: "showSync",
                manual: !0,
                autohide: !0
            },
            ".download-btn": {
                modelAttribute: "showDownload",
                manual: !0,
                autohide: !0
            },
            ".watch-later-btn": {
                modelAttribute: "showWatchLater",
                manual: !0,
                autohide: !0
            },
            ".share-btn, .recommend-btn": {
                modelAttribute: "showShare",
                manual: !0,
                autohide: !0
            },
            ".delete-btn": {
                modelAttribute: "showDelete",
                manual: !0,
                autohide: !0
            },
            ".info-btn": {
                modelAttribute: "showInfo",
                manual: !0,
                autohide: !0
            }
        },
        onRender: function() {
            var e = this.ui.divider,
                t = e.prevAll(":not(:has(.hidden))").length,
                i = e.nextAll(":not(:has(.hidden))").length;
            e.toggleClass("hidden", !t || !i)
        },
        onClose: function() {
            this.mediaActions.stopListening()
        },
        onPlayAllClick: function(e) {
            e.preventDefault(), l.execute("playMedia", {
                item: this.model
            })
        },
        onPlayShuffledClick: function(e) {
            e.preventDefault(), l.execute("playMedia", {
                item: this.model,
                shuffle: !0
            })
        },
        onPlayPrimaryExtraClick: function(e) {
            e.preventDefault();
            var i = this.model.server.getSectionForItem(this.model);
            if (!t.isEmpty(i)) {
                var n = new c({
                    key: this.model.get("primaryExtraKey"),
                    type: "clip",
                    librarySectionUUID: i.get("uuid")
                }, {
                    server: this.model.server,
                    section: this.model.section
                });
                l.execute("playMedia", {
                    item: n
                })
            }
        },
        onPlayNextClick: function(e) {
            return e.preventDefault(), i(e.currentTarget).isDisabled() ? void e.stopImmediatePropagation() : void l.execute("queueMedia", {
                item: this.model,
                next: !0
            })
        },
        onAddToUpNextClick: function(e) {
            e.preventDefault(); 
            return i(e.currentTarget).isDisabled() ? void e.stopImmediatePropagation() : void l.execute("queueMedia", {
                item: this.model,
                next: !1
            })
        },
        onPlayMixClick: function(e) {
            e.preventDefault();
            var t = this.model.get("related"),
                i = t.findWhere({
                    browse: !1
                });
            i.set("type", "track"), l.execute("playMedia", {
                item: i
            })
        },
        onAddToPlaylistClick: function(e) {
            return e.preventDefault(), i(e.currentTarget).isDisabled() ? void e.stopImmediatePropagation() : void a.trigger("show:modal", new u({
                model: this.model,
                items: this.mediaActions.getActionTargets(),
                title: this.mediaActions.getActionTargetsTitle()
            }))
        },
        onRefreshClick: function(e) {
            e.preventDefault(), l.execute("refreshMetadata", {
                items: this.mediaActions.getActionTargets()
            })
        },
        onMarkWatchedClick: function(e) {
            e.preventDefault(), l.execute("markWatched", {
                item: this.model
            })
        },
        onMarkUnwatchedClick: function(e) {
            e.preventDefault(), l.execute("markUnwatched", {
                item: this.model
            })
        },
        onSyncClick: function(e) {
            e.preventDefault();
            var t = {
                model: this.model,
                type: this.mediaType,
                isNew: !0,
                isSection: !1
            };
            l.execute("showSyncModal", t)
        },
        onWatchLaterClick: function(e) {
            e.preventDefault(), l.execute("watchLater", {
                item: this.model
            })
        },
        onShareClick: function(e) {
            e.preventDefault(), a.trigger("show:modal", new h({
                model: this.model
            }))
        },
        onRecommendClick: function(e) {
            e.preventDefault(), a.trigger("show:modal", new m({
                model: this.model
            }))
        },
        onDeleteClick: function(e) {
            e.preventDefault(), this.model.get("playlistType") ? l.execute("deletePlaylist", {
                item: this.model
            }) : l.execute("deleteItems", {
                item: this.model,
                skipRedirect: !0
            })
        },
        onInfoClick: function(e) {
            e.preventDefault(), a.trigger("show:modal", new p({
                model: this.model
            }))
        }
    });
    return f
});