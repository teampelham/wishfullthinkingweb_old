define([
	"templates/desktop/breadcrumbbar/sectionBreadcrumbBarView.tpl", 
	"jquery", 
	"underscore", 
	"marionette", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/adapters/settingsAdapter", 
	"base/commands/commands", 
	"desktop/views/dropdowns/sectionDropdown", 
	"desktop/views/dropdowns/sectionSubsetDropdown", 
	"desktop/views/dropdowns/sectionTypeDropdown", 
	//"desktop/views/modals/editSectionModal", 
	//"desktop/views/modals/user/editFriendModal"
    "desktop/views/modals/automatedDevice/addAutomatedDeviceModal",
    "desktop/views/modals/automatedDevice/addScheduledActionModal"
], function(template, $, _, marionette, t, dispatcher, settingsAdapter, commands, SectionDropdown, SectionSubsetDropdown, SectionTypeDropdown, AddAutomatedDeviceModal, AddScheduledActionModal) {
    "use strict";
	
    var sectionBreadcrumbBar = marionette.Layout.extend({
        className: "breadcrumb-bar",
        template: template,
        
		ui: {
            listSelectedIcons: "#section-list-dropdown .dropdown-selected-icon",
            itemsBadge: ".items-badge",
            updateBtn: ".update-btn"
        },
        
		regions: {
            sectionDropdown: ".breadcrumb-dropdown",
            subsetDropdown: ".breadcrumb-subset-dropdown",
            typeDropdown: ".breadcrumb-type-dropdown"
        },
        
		events: {
            "click .list-btn": "onListClick",
            "click .edit-btn": "onEditClick",
            "click .share-btn": "onShareClick",
            "click .update-btn": "onUpdateClick",
            "click .analyze-btn": "onAnalyzeClick",
            "click .force-refresh-btn": "onForceRefreshClick",
            "click .empty-trash-btn": "onEmptyTrashClick",
            "click .delete-btn": "onDeleteClick",
            "click .add-btn": "onAddClick"
        },
		
        onlyAfterRender: ["onTotalSizeChange"],
        
		initialize: function() {
            var items = this.model.get("items");
            this.refreshing = this.model.get("refreshing"); 
			this.updateTitle = t("Update Library"); 
			this.updateTitleAlt = t("Cancel Update"); 
			this.shared = this.model.server.get("shared"); 
			this.synced = this.model.server.get("synced"); 
			this.limited = !(!this.shared && !this.synced); 
			this.listenTo(items, "change:totalSize", this.onTotalSizeChange); 
			this.listenTo(this.model, "change:refreshing", this.onRefreshingChange); 
			this.listenTo(this.model, "change:currentType", this.updateListViewDropdown); 
			this.listenTo(dispatcher, "shortcut:delete", this.onDelete);
        },
		
        serializeData: function() {
            return {
                limited: this.limited,
                totalSize: this.model.get("items").totalSize,
                listType: settingsAdapter.getInstance().getSectionListType(this.model),
                refreshing: this.refreshing,
                updateTitle: this.refreshing ? this.updateTitleAlt : this.updateTitle,
                updateTitleAlt: this.refreshing ? this.updateTitle : this.updateTitleAlt
            }
        },
		
        updateListViewDropdown: function(value) {
            if(!_.isString(value))
                value = settingsAdapter.getInstance().getSectionListType(this.model);
            
            this.ui.listSelectedIcons.each(function(idx, item) {
                var icon = $(item);
                var type = icon.closest(".list-btn").data("type");
                icon.toggleClass("hidden", !_.isEqual(value, type));
            });
        },
		
        onRender: function() {
            this.$el.tooltip({
                selector: ".breadcrumb-actions > a",
                container: "body",
                placement: "bottom"
            }); 
            
            this.sectionDropdown.show(new SectionDropdown({
                model: this.model,
                collection: this.model.server.get("sections")
            })); 
            
            this.subsetDropdown.show(new SectionSubsetDropdown({
                model: this.model.get("currentSubset"),
                collection: this.model.get("subsets")
            })); 
            
            this.typeDropdown.show(new SectionTypeDropdown({
                model: this.model,
                collection: this.model.get("types")
            }));
        },
		
        onTotalSizeChange: function(e, size) {
            var totalSize = null == size ? 0 : size;
            
            this.ui.itemsBadge.toggleClass("hidden", 0 === totalSize); 
            this.ui.itemsBadge.text(totalSize);
        },
		
        onEditClick: function(e) {
			console.warn('');
            /*e.preventDefault(), this.limited || a.trigger("show:modal", new u({
                server: this.model.server,
                model: this.model
            }))*/
        },
        
        onAddClick: function(e) {
            e.preventDefault();
            if (!this.limited) {
                var modal, key;
                key = this.model.get("key");
                if (key == "device")
                    modal = new AddAutomatedDeviceModal({
                        section: this.model,
                        server: this.model.server
                    });
                else if(key == "schedule")
                    modal = new AddScheduledActionModal({
                        section: this.model,
                        server: this.model.server
                    });
                    
                dispatcher.trigger("show:modal", modal);
            }
        },
		
        onUpdateClick: function(e) {
			console.warn('');
            /*e.preventDefault(), this.limited || o.execute("updateLibrary", {
                section: this.model,
                cancel: this.model.get("refreshing")
            })*/
        },
		
        onShareClick: function(e) {
			console.warn('');
            /*e.preventDefault(), this.limited || a.trigger("show:modal", new h({
                defaultPaneType: "ownedServer",
                defaultPaneID: this.model.server.id
            }))*/
        },
		
        onAnalyzeClick: function(e) {
			console.warn('');
            /*e.preventDefault(), this.limited || o.execute("analyzeSection", {
                section: this.model
            })*/
        },
		
        onForceRefreshClick: function(e) {
			console.warn('');
            /*e.preventDefault(), this.limited || o.execute("updateLibrary", {
                section: this.model,
                params: {
                    force: !0
                }
            })*/
        },
		
        onEmptyTrashClick: function(e) {
			console.warn('');
            /*e.preventDefault(), this.limited || o.execute("emptyTrash", {
                section: this.model
            })*/
        },
		
        onDeleteClick: function(e) {
            e.preventDefault(); 
			this.onDelete();
        },
		
        onListClick: function(e) {
            e.preventDefault();
            var currentTarget = $(e.currentTarget);
            var type = currentTarget.data("type");
            var currentType = this.model.get("currentType");
            
            settingsAdapter.getInstance().saveListType(currentType, type);
            this.updateListViewDropdown(type);
        },
		
        onDelete: function() {
			console.warn('');
            /*if (!this.limited) {
                var e = o.execute("deleteSection", {
                    section: this.model
                });
                e.done(function() {
                    a.trigger("navigate", "dashboard")
                })
            }*/
        },
		
        onRefreshingChange: function() {
			console.warn('');
            /*var e = this.ui.updateBtn,
                n = this.ui.updateBtn.attr("data-original-title");
            if (this.ui.updateBtn.toggleClass("active", this.model.get("refreshing")), n) {
                var s = t(".tooltip > .tooltip-inner").html();
                i.contains([this.updateTitle, this.updateTitleAlt], s) && t(".tooltip > .tooltip-inner").html(e.attr("data-title-alt")), e.attr("data-original-title", e.attr("data-title-alt")), e.attr("data-title-alt", n)
            }*/
        }
    });
	
    return sectionBreadcrumbBar;
});