define([
    "templates/desktop/breadcrumbbar/detailsBreadcrumbBarView.tpl", 
    "jquery", 
    "marionette", 
    "base/utils/dispatcher", 
    "base/adapters/settingsAdapter", 
    "base/models/library/metadataModel", 
    "desktop/views/dropdowns/breadcrumbDropdown"
], function(e, t, i, n, s, a, r) {
    "use strict";

    var o = i.Layout.extend({
        className: "details-breadcrumb-bar breadcrumb-bar",
        template: e,
        ui: {
            previousBtn: ".previous-btn",
            nextBtn: ".next-btn",
            listSelectedIcons: "#section-list-dropdown .dropdown-selected-icon"
        },
        regions: {
            currentItem: ".current-item",
            grandparentItem: ".grandparent-item",
            siblingsDropdown: ".siblings-dropdown",
            parentsDropdown: ".parents-dropdown"
        },
        events: {
            "click .playlists-btn": "onPlaylistsClick",
            "click .section-btn": "onSectionClick",
            "click .list-btn": "onListClick",
            "click .previous-btn": "onPreviousClick",
            "click .next-btn": "onNextClick"
        },
        onlyAfterRender: ["onSiblingsPopulate", "onParentsPopulate"],
        initialize: function() {
            var e = this.model.get("parentKey");
            if (!e) return void this.onSiblingsPopulate(!1);
            var t, i = {
                    server: this.model.server,
                    section: this.model.section
                }, n = this.parent = new a({
                    key: e
                }, i),
                s = n.populate({
                    scope: "page"
                });
            s = s.then(function() {
                return t = n.get("children"), t.populate({
                    scope: "page"
                })
            }), s.done(this.onSiblingsPopulate);
            var r = this.model.get("grandparentKey");
            if (r) {
                var o = this.grandparent = new a({
                    key: r
                }, i),
                    l = o.populate({
                        scope: "page"
                    });
                l = l.then(function() {
                    var e = o.get("children");
                    return e.populate({
                        scope: "page"
                    })
                }), l.done(this.onParentsPopulate)
            }
        },
        serializeData: function() {
            var e = this.model.server,
                t = this.model.section,
                i = t || this.model.get("playlistType");
            return {
                serverID: e.id,
                showSectionDropdown: i,
                showPlaylists: this.model.server.supports("playlists"),
                sections: e.get("sections").toJSON(),
                section: t ? t.toJSON() : null,
                item: this.model.toJSON(),
                parent: this.parent ? this.parent.toJSON() : null,
                grandparent: this.grandparent ? this.grandparent.toJSON() : null,
                listType: this.getListType()
            }
        },
        getListTypeSettingKey: function() {
            var e = "episodeListType",
                t = this.model.section || this.model.server;
            return t.getScopedKey(e)
        },
        getListType: function() {
            return "season" === this.model.get("type") ? s.get(this.getListTypeSettingKey()) || "episode-tile-list" : null
        },
        previous: function() {
            this.previousSibling && n.trigger("navigate", "details", {
                serverID: this.model.server.id,
                key: this.previousSibling.get("key")
            })
        },
        next: function() {
            this.nextSibling && n.trigger("navigate", "details", {
                serverID: this.model.server.id,
                key: this.nextSibling.get("key")
            })
        },
        onSiblingsPopulate: function(e) {
            if (!e) return this.currentItem.show(new r({
                model: this.model,
                isLeaf: !0
            })), void this.currentItem.$el.removeClass("hidden");
            var t = e.get(this.model.id),
                i = e.indexOf(t);
            i > 0 && (this.previousSibling = e.at(i - 1)), i < e.length - 1 && (this.nextSibling = e.at(i + 1)), this.ui.previousBtn.toggleClass("disabled", !this.previousSibling), this.ui.nextBtn.toggleClass("disabled", !this.nextSibling), this.ui.previousBtn.removeClass("hidden"), this.ui.nextBtn.removeClass("hidden"), this.grandparent || (this.grandparentItem.show(new r({
                model: this.parent
            })), this.grandparentItem.$el.removeClass("hidden")), this.siblingsDropdown.show(new r({
                id: "siblings-dropdown",
                model: this.model,
                collection: e
            })), this.siblingsDropdown.$el.removeClass("hidden")
        },
        onParentsPopulate: function(e) {
            this.grandparentItem.show(new r({
                model: this.grandparent
            })), this.grandparentItem.$el.removeClass("hidden"), this.parentsDropdown.show(new r({
                id: "parents-dropdown",
                model: this.parent,
                collection: e
            })), this.parentsDropdown.$el.removeClass("hidden")
        },
        onSectionClick: function(e) {
            e.preventDefault();
            var i = t(e.currentTarget),
                s = i.data("key");
            n.trigger("navigate", "section", {
                serverID: this.model.server.id,
                sectionID: s
            })
        },
        onPlaylistsClick: function(e) {
            e.preventDefault(), n.trigger("navigate", "playlists", {
                serverID: this.model.server.id
            })
        },
        onListClick: function(e) {
            e.preventDefault();
            var i = t(e.currentTarget),
                n = i.data("type");
            s.save(this.getListTypeSettingKey(), n), this.ui.listSelectedIcons.addClass("hidden"), i.find(".dropdown-selected-icon").removeClass("hidden")
        },
        onPreviousClick: function(e) {
            e.preventDefault(), this.previous()
        },
        onNextClick: function(e) {
            e.preventDefault(), this.next()
        }
    });
    return o;
});