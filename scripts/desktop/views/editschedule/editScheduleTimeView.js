define([
	"templates/desktop/schedules/editScheduleTimeView.tpl", 
	"underscore", 
	"jquery", 
	"base/utils/languageUtil", 
	"desktop/views/editschedule/editScheduleBaseView"
], function(template, _, $, s, editScheduleBaseView) {
    "use strict";
	
    var editScheduleTimeView = editScheduleBaseView.extend({
        template: template,
        ui: _.extend({
            time: "#edit-time",
            repeaters: "input[type=radio][name=timeBase]",
            sunCycleDiv: "#sunCycleChosen",
            timeDiv: "#timeChosen"
        }, editScheduleBaseView.prototype.ui),
        
        modelEvents: {
            "change:sunrise": "onSunriseChange",
            "change:hour" : "onTimeChange",
            "change:minute": "onTimeChange",
            "change:sunriseOffset": "onSunriseOffsetChange"
        },
        
        events: {
            "click input[type=radio][name=timeBase]": "onEditSunCycleClick",
            "keyup #edit-time": "onEditTimeChange",
            "change #edit-time": "onEditTimeChange",
            "change #repeatChosen input[type=checkbox]": "onEditDaysOfWeekChange"
        },
        
        onlyAfterRender: ["renderSectionLanguage", "updateSectionTypesWizardBlocks"].concat(editScheduleBaseView.prototype.onlyAfterRender),
        
        initialize: function() {
            //this.populateCollections();
        },
        
        serializeData: function() {
            var data = editScheduleBaseView.prototype.serializeData.apply(this, arguments);
            //data.deviceTypes = automatedDeviceTypes.getDeviceTypes();
            //data.protocolTypes = automationProtocolTypes.getSectionTypes();

            data.add = true;       // TODO: Change the model so that model.isNew() actually works
            data.edit = false;
                
            return data;
        },
        
        validate: function(model) {
            var errors = [];
            if (model.get("sunset") == null) 
                if (!(model.get("hour") || model.get("minute")))
                    errors.push("time");    
    			
            return errors;
        },
              
        
        renderSectionLanguage: function() {			// TODO: Use this for Locations/Hardwares?
            
            var e = this.model.agents.get(this.model.get("agent")),
                i = e.get("language"),
                n = this.model.get("language");
            if (!t.contains(i, n)) {
                var r = automationProtocolTypes.getSectionTypeByString(this.model.get("type"));
                var o = r.defaultLanguage;
                this.model.set("language", o, {
                    silent: !0
                }), n = o
            }
            this.updateSectionLanguage(), this.ui.sectionLanguage.html(this.createMenuOptions(t.map(i, function(e) {
                return {
                    label: s.getLanguageName(e),
                    value: e
                }
            }), n))
        },
        
        updateSectionTypesWizardBlocks: function(sunrise) {
            //this.ui.automationTypes.removeClass("selected"); 
            //this.ui.automationTypes.filter('[data-key="' + this.model.get("type") + '"]').addClass("selected"); 
            if(sunrise) {
                this.ui.sunCycleDiv.addClass("section-type-chosen");
                this.ui.timeDiv.removeClass("section-type-chosen");
            }
            else {
                this.ui.timeDiv.addClass("section-type-chosen");
                this.ui.sunCycleDiv.removeClass("section-type-chosen");
            }
        },
        
        
        updateSectionLanguage: function() {
            this.ui.sectionLanguage.val(this.model.get("language"))
        },
        
        updateTime: function() {
            var time = this.ui.time.val().split(':');
            var hour = this.model.get("hour");
            var minute = this.model.get("minute");
            
            if (hour != time[0] || minute != time[1]) 
                this.ui.time.val(hour + ':' + minute);
        },
        
        onEditSunCycleClick: function(e) {
            var radio = $(e.currentTarget);
            var radioID = radio.prop("id");
            var sunrise = radioID === "sunRadio";
            var sunriseProperty = { sunrise: sunrise };
            
            this.model.set(sunriseProperty);         
        },
        
        onEditTimeChange: function(e) {
            var timeInput = $(e.currentTarget);
            var time = timeInput.val();
            var timeComponents = time.indexOf(':') > -1 ? time.split(':') : null;
            
            if(timeComponents !== null) {
                var timeProperties = {
                    hour: timeComponents[0],
                    minute: timeComponents[1]
                };
                this.model.set(timeProperties);
            }
        },
        
        onSunriseOffsetChange: function() {
            this.showValidationResult(this.validate(this.model));
        },
        
        onTimeChange: function () {
            this.updateTime();
            this.showValidationResult(this.validate(this.model));
        },
        
        onSunriseChange: function() {
            this.updateSectionTypesWizardBlocks(this.model.get("sunrise")); 
            //this.showValidationResult(this.validate(this.model));
        }
    });
    
    return editScheduleTimeView;
});