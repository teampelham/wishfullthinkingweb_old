define([
    "underscore", 
    "jquery", 
    "marionette", 
    "base/utils/types/sectionTypes", 
    "base/models/server/serverSettingCollection"
], function(_, $, marionette, n, s) {
    "use strict";
    
    var editScheduleBaseView = marionette.Layout.extend({
        ui: {
            formGroup: ".form-group"
        },
        
        onlyAfterRender: ["showValidationResult"],
        
        serializeData: function() {
            var data = {};
            if (this.model.isNew()) 
                data.add = true; 
            else
                data.edit = true; 
            
            data.section = this.model.toJSON();
            return data;
        },
        
        showValidationResult: function(result) {
            var formGroup = this.ui.formGroup;
            formGroup.removeClass("has-error"); 
			_.each(result, function(item) {
                formGroup.filter("." + item + "-group").addClass("has-error");
            }); 
			this.trigger(result.length ? "invalid" : "valid");
        },
        
        createMenuOptions: function(items, selectedValue) {
            return _.map(items, function(item) {
                return '<option value="' + item.value + '"' + (item.value === selectedValue ? " selected" : "") + ">" + item.label + "</option>";
            }).join("\n");
        },
        
        setAutomationType: function(automationType) {
            this.model.automationType = automationType;
        },
        
        populateSectionSettings: function() {
            if (!this.model.server.supports("sectionSettings")) 
				return $.Deferred().resolve().promise();
            if (!this.model.settings) {
                var e = new l([], {
                    server: this.model.server
                });
                if (this.model.isNew()) {
                    var i = {
                        type: this.getSectionType().typeID,
                        agent: this.model.get("agent")
                    };
                    e.url = "/library/sections/prefs?" + t.param(i, !0)
                } else e.url = "/library/sections/" + this.model.id + "/prefs";
                this.model.settings = e
            }
            var n = this.model.settings.populate();
            return n
        },
        
        onModalClose: function() {
			this.model.settings = null;
        }
    });
    
    return editScheduleBaseView;
});