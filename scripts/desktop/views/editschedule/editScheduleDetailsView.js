define([
	"templates/desktop/schedules/editScheduleDetailsView.tpl", 
	"underscore", 
	"jquery", 
	"base/utils/languageUtil", 
	"desktop/views/editschedule/editScheduleBaseView"
], function(template, _, $, s, editScheduleBaseView) {
    "use strict";
	
    var editScheduleDetailsView = editScheduleBaseView.extend({
        template: template,
        ui: _.extend({
            repeaters: "input[type=radio][name=repeatRadios]",
            deviceTitle: "#edit-device-title",
            repeatCbs: "#repeatChosen input[type=checkbox]",
            repeatDiv: "#repeatChosen",
            dateTimeDiv: "#dateTimeChosen"
        }, editScheduleBaseView.prototype.ui),
        
        modelEvents: {
            "change:repeat": "onRepeatChange",
            "change:daysOfWeek" : "onDaysOfWeekChange",
            "change:title": "onDeviceTitleChange"
        },
        
        events: {
            "click input[type=radio][name=repeatRadios]": "onEditRepeatClick",
            "keyup #edit-device-title": "onEditDeviceTitleChange",
            "change #edit-device-title": "onEditDeviceTitleChange",
            "change #repeatChosen input[type=checkbox]": "onEditDaysOfWeekChange"
        },
        
        onlyAfterRender: ["renderSectionLanguage", "updateSectionTypesWizardBlocks", "updateDeviceTitle"].concat(editScheduleBaseView.prototype.onlyAfterRender),
        
        initialize: function() {
            this.populateCollections();
        },
        
        serializeData: function() {
            var data = editScheduleBaseView.prototype.serializeData.apply(this, arguments);
            //data.deviceTypes = automatedDeviceTypes.getDeviceTypes();
            //data.protocolTypes = automationProtocolTypes.getSectionTypes();

            data.add = true;       // TODO: Change the model so that model.isNew() actually works
            data.edit = false;
                
            return data;
        },
        
        validate: function(model) {
            var errors = [];
            if (model.get("repeat") == null) 
                errors.push("repeat"); 
            else {
                var repeat = model.get("repeat");
                if(repeat && !model.get("daysOfWeek")) 
                    errors.push("daysOfWeek"); 
    			else if (!repeat && !model.get("date")) 
                    errors.push("date");
            }
				
            return errors;
        },
        
        populateCollections: function() {       // TODO: Agent - turns on Register mode? Scanner - scans insteon device states??
            //var e = this.populateAgents();
            //var n = this.populateScanners();
            //var s = this.getSectionType();
            //var a = $.when(s, e, n, this.model.server.get("myPremiumSubscription") && this.model.server.supports("premiumMusic"));
            
            //if (this.model.isNew()) 
            //    a = a.then(t.bind(this.model.setAgentAndScanner, this.model));
            
            //a = a.then(e).then(t.bind(this.onAgentsPopulate, this)).then(t.bind(function() {
            //    this.showValidationResult(this.validate(this.model));
            //}, this));
            
            //return a;
        },
        
        renderSectionLanguage: function() {			// TODO: Use this for Locations/Hardwares?
            
            var e = this.model.agents.get(this.model.get("agent")),
                i = e.get("language"),
                n = this.model.get("language");
            if (!t.contains(i, n)) {
                var r = automationProtocolTypes.getSectionTypeByString(this.model.get("type"));
                var o = r.defaultLanguage;
                this.model.set("language", o, {
                    silent: !0
                }), n = o
            }
            this.updateSectionLanguage(), this.ui.sectionLanguage.html(this.createMenuOptions(t.map(i, function(e) {
                return {
                    label: s.getLanguageName(e),
                    value: e
                }
            }), n))
        },
        
        updateSectionTypesWizardBlocks: function(repeat) {
            //this.ui.automationTypes.removeClass("selected"); 
            //this.ui.automationTypes.filter('[data-key="' + this.model.get("type") + '"]').addClass("selected"); 
            if(repeat) {
                this.ui.repeatDiv.addClass("section-type-chosen");
                this.ui.dateTimeDiv.removeClass("section-type-chosen");
            }
            else {
                this.ui.dateTimeDiv.addClass("section-type-chosen");
                this.ui.repeatDiv.removeClass("section-type-chosen");
            }
        },
        
        
        updateSectionLanguage: function() {
            this.ui.sectionLanguage.val(this.model.get("language"))
        },
        
        updateDeviceTitle: function() {
            var deviceTitle = this.ui.deviceTitle.val();
            var title = this.model.get("title");
            
            if (deviceTitle !== title) 
                this.ui.deviceTitle.val(title);
        },
        
        updateDaysOfWeek: function() {
            console.warn('update the checkboxes');
        },
        
        onEditRepeatClick: function(e) {
            var radio = $(e.currentTarget);
            var radioID = radio.prop("id");
            var repeat = radioID === "repeatRb";
            var repeatProperty = { repeat: repeat };
            
            this.model.set(repeatProperty);         
        },
        
        onEditDaysOfWeekChange: function(e) {
            var t = $(e.currentTarget);
            var daysOfWeek = this.model.get("daysOfWeek");
            var bitMask = parseInt(t.val());
            if(t.prop("checked"))
                daysOfWeek = daysOfWeek | bitMask;
            else
                daysOfWeek = daysOfWeek & ~bitMask;
                
            this.model.set("daysOfWeek", daysOfWeek); 
            this.showValidationResult(this.validate(this.model));
        },
        
        onEditDeviceTypeChange: function(e) {
            var t = $(e.currentTarget);
            this.model.set("deviceType", t.val()); 
            this.showValidationResult(this.validate(this.model))
        },
        
        onRepeatChange: function(e, repeat) {
            this.updateSectionTypesWizardBlocks(repeat);
            if(repeat) {
                var days = this.model.get("daysOfWeek");
                if (days == null)
                    this.model.set("daysOfWeek", 0);
            }
            //var automationProtocol = automationProtocolTypes.getAutomationTypeByID(typeID);

            /*var title = this.model.get("title");
            var previousType = this.model.previous("type");
            var previousProtocol = automationProtocolTypes.getSectionType(previousType);
            var previousProtocolTitle = previousProtocol && previousProtocol.title;
            if (!title || title === previousProtocolTitle) 
                this.model.set("title", automationProtocol.title); 
            
            this.setAutomationType(automationProtocol); 
            this.model.agents = null; 
            this.model.scanners = null; 
            this.model.settings = null; 
            this.populateCollections();*/
        },
        
        onDaysOfWeekChange: function () {
            this.updateDaysOfWeek();  
            this.showValidationResult(this.validate(this.model));
        },
        
        onDeviceTitleChange: function() {
            this.updateDeviceTitle(); 
            this.showValidationResult(this.validate(this.model));
        },
        
        onAgentsPopulate: function() {
            this.renderSectionLanguage();
        }
    });
    
    return editScheduleDetailsView;
});