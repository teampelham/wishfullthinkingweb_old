define([
	"templates/desktop/section/edit/editSectionSettingView.tpl", 
	"jquery", 
	"marionette"
], function(e, t, i) {
    "use strict";
    var n = i.ItemView.extend({
        template: e,
        events: {
            "change [name]": "onEditSectionSettingInputChange"
        },
        serializeData: function() {
            var e = this.model.isNew(),
                t = "music" === this.model.get("type"),
                i = {
                    settings: this.collection.toJSON(),
                    edit: !e,
                    isNewMusicSection: e && t && this.model.server.supports("premiumMusic")
                };
            return i
        },
        onEditSectionSettingInputChange: function(e) {
            var i = t(e.currentTarget),
                n = i.attr("name");
            if ("importFromiTunes" === n) return void this.model.set(n, 1);
            var s = i.is(":checkbox") ? i.prop("checked") : i.val(),
                a = this.collection.findWhere({
                    id: n
                });
            a.set("value", s)
        }
    });
    return n
});