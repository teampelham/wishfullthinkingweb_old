define([
	"marionette", 
	"desktop/views/editsection/editSectionPathItemView"
], function(e, t) {
    "use strict";
    var i = e.CollectionView.extend({
        itemView: t
    });
    return i
});