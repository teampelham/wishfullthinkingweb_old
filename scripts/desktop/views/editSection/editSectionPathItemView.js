define([
	"templates/desktop/section/edit/editSectionPathItemView.tpl", 
	"marionette"
], function(e, t) {
    "use strict";
    var i = t.ItemView.extend({
        template: e,
        className: "row",
        events: {
            "click .remove-path-btn": "onRemovePathClick"
        },
        onRemovePathClick: function(e) {
            e.preventDefault(), this.model.destroy()
        }
    });
    return i
});