define([
	"templates/desktop/section/edit/editSectionFoldersView.tpl", 
	"underscore", 
	"jquery", 
	"backbone", 
	"base/utils/dispatcher", 
	"base/adapters/localizerAdapter", 
	"desktop/views/editsection/editSectionBaseView", 
	"desktop/views/editsection/editSectionPathsListView", 
	"desktop/views/modals/addFolderModal"
], function(e, t, i, n, s, a, r, o, l) {
    "use strict";
    var c = r.extend({
        template: e,
        fieldNames: ["Location"],
        regions: {
            pathsRegion: ".paths-region"
        },
        events: {
            "click .add-folder-btn": "onAddFolderClick",
            "change input[name=path]": "onFolderChange"
        },
        onlyAfterRender: ["renderSectionLocation"].concat(r.prototype.onlyAfterRender),
        initialize: function() {
            if (!this.model.paths) {
                var e = this.model.get("Location") || [],
                    i = t.map(e, function(e) {
                        return {
                            path: e.path
                        }
                    });
                this.model.paths = new n.Collection(i)
            }
            this.listenTo(this.model.paths, {
                add: this.onCollectionChange,
                remove: this.onCollectionChange
            }), this.renderSectionLocation(), this.showValidationResult(this.validate(this.model))
        },
        validate: function(e) {
            var t = [],
                i = e.get("Location") || [];
            return i.length || t.push("Location"), t
        },
        renderSectionLocation: function() {
            var e = new o({
                collection: this.model.paths
            });
            this.pathsRegion.show(e)
        },
        onCollectionChange: function() {
            this.model.set("Location", this.model.paths.toJSON()), this.showValidationResult(this.validate(this.model))
        },
        onAddFolderClick: function(e) {
            e.preventDefault();
            var t = new l({
                server: this.model.server
            });
            this.listenToOnce(t, "add:folder", this.onAddFolder), s.trigger("show:modal", t)
        },
        onAddFolder: function(e) {
            if (e) {
                var t = {
                    path: e
                };
                this.model.paths.findWhere(t) || this.model.paths.add(t)
            }
        },
        onFolderChange: function(e) {
            var t = i(e.currentTarget),
                n = t.prop("defaultValue"),
                s = t.val();
            if (!s) return void t.val(n);
            var a = this.model.paths.findWhere({
                path: n
            });
            a.set("path", s), this.model.set("Location", this.model.paths.toJSON()), t.prop("defaultValue", s)
        }
    });
    return c
});