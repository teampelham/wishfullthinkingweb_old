define([
	"templates/desktop/section/edit/editSectionTypeView.tpl", 
	"underscore", 
	"jquery", 
	"moment", 
	"base/utils/languageUtil", 
	"base/utils/types/sectionTypes", 
	"desktop/views/editsection/editSectionBaseView"
], function(e, t, i, n, s, a, editSectionBaseView) {
    "use strict";
    
    var editSectionTypeView = editSectionBaseView.extend({
        template: e,
        ui: t.extend({
            sectionTypes: ".wizard-block",
            sectionTitle: "#edit-section-title",
            sectionLanguage: "#edit-section-language"
        }, editSectionBaseView.prototype.ui),
        
        modelEvents: {
            "change:type": "onSectionTypeChange",
            "change:title": "onSectionTitleChange",
            "change:language": "onSectionLanguageChange"
        },
        
        events: {
            "click .section-type": "onSectionTypeClick",
            "keyup #edit-section-title": "onEditSectionTitleChange",
            "change #edit-section-title": "onEditSectionTitleChange",
            "change #edit-section-language": "onEditSectionLanguageChange"
        },
        
        onlyAfterRender: ["renderSectionLanguage", "updateSectionTypesWizardBlocks", "updateSectionLanguage", "updateSectionTitle"].concat(r.prototype.onlyAfterRender),
        
        initialize: function() {
            this.populateCollections();
        },
        
        serializeData: function() {
            var e = r.prototype.serializeData.apply(this, arguments);
            e.sectionTypes = a.getSectionTypes();
            var t = this.getSectionType();
            
            if (t) 
                e.currentSectionKey = t.key;
                
            var i = this.model.get("updatedAt");
            if (i) 
                e.updatedAt = n.unix(i).fromNow();
            return e;
        },
        
        validate: function(e) {
            var t = [];
            if (!e.get("type")) 
                t.push("type"); 
            if (!e.get("title")) 
                t.push("title"); 
            if (e.get("language")) 
                t.push("language");
            return t;
        },
        
        populateCollections: function() {
            var e = this.populateAgents();
            var n = this.populateScanners();
            var s = this.getSectionType();
            var a = i.when(s, e, n, this.model.server.get("myPremiumSubscription") && this.model.server.supports("premiumMusic"));
            
            if (this.model.isNew()) 
                a = a.then(t.bind(this.model.setAgentAndScanner, this.model));
            
            a = a.then(e).then(t.bind(this.onAgentsPopulate, this)).then(t.bind(function() {
                this.showValidationResult(this.validate(this.model));
            }, this));
            
            return a;
        },
        
        renderSectionLanguage: function() {
            var e = this.model.agents.get(this.model.get("agent")),
                i = e.get("language"),
                n = this.model.get("language");
            if (!t.contains(i, n)) {
                var r = a.getSectionTypeByString(this.model.get("type")),
                    o = r.defaultLanguage;
                this.model.set("language", o, {
                    silent: !0
                }), n = o
            }
            this.updateSectionLanguage(), this.ui.sectionLanguage.html(this.createMenuOptions(t.map(i, function(e) {
                return {
                    label: s.getLanguageName(e),
                    value: e
                }
            }), n))
        },
        
        updateSectionTypesWizardBlocks: function() {
            this.ui.sectionTypes.removeClass("selected"); 
            this.ui.sectionTypes.filter('[data-key="' + this.model.get("type") + '"]').addClass("selected"); 
            this.$el.addClass("section-type-chosen");
        },
        
        updateSectionLanguage: function() {
            this.ui.sectionLanguage.val(this.model.get("language"))
        },
        
        updateSectionTitle: function() {
            var e = this.ui.sectionTitle.val(),
                t = this.model.get("title");
            e !== t && this.ui.sectionTitle.val(t)
        },
        
        onSectionTypeClick: function(e) {
            e.preventDefault();
            var t = i(e.currentTarget);
            if (t.hasClass("selectable")) {
                var n = t.data("key"),
                    s = {
                        type: n
                    }, r = a.getSectionType(n);
                this.model.get("title") || (s.title = r.title), this.model.set(s)
            }
        },
        
        onEditSectionTitleChange: function(e) {
            var t = i(e.currentTarget);
            this.model.set("title", t.val()), this.showValidationResult(this.validate(this.model))
        },
        
        onEditSectionLanguageChange: function(e) {
            var t = i(e.currentTarget);
            this.model.set("language", t.val()), this.showValidationResult(this.validate(this.model))
        },
        
        onSectionTypeChange: function(e, t) {
            this.updateSectionTypesWizardBlocks();
            var i = a.getSectionType(t);
            if (!this.model.get("language")) {
                var n = i.defaultLanguage;
                this.model.set("language", n)
            }
            var s = this.model.get("title"),
                r = this.model.previous("type"),
                o = a.getSectionType(r),
                l = o && o.title;
            s && s !== l || this.model.set("title", i.title), this.setSectionType(i), this.model.agents = null, this.model.scanners = null, this.model.settings = null, this.populateCollections()
        },
        
        onSectionTitleChange: function() {
            this.updateSectionTitle(); 
            this.showValidationResult(this.validate(this.model));
        },
        
        onSectionLanguageChange: function() {
            this.updateSectionLanguage();
        },
        
        onAgentsPopulate: function() {
            this.renderSectionLanguage();
        }
    });
    
    return editSectionTypeView;
});