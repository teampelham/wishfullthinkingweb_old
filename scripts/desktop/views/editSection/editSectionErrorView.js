define([
	"templates/desktop/section/edit/editSectionErrorView.tpl", 
	"desktop/views/editsection/editSectionBaseView"], function(e, t) {
    "use strict";
    var i = t.extend({
        template: e
    });
    return i
});