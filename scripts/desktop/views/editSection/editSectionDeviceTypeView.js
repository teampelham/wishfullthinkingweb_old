define([
	"templates/desktop/section/edit/editSectionDeviceTypeView.tpl", 
	"underscore", 
	"jquery", 
	"moment", 
	"base/utils/languageUtil", 
	"base/utils/types/automationProtocolTypes", 
    "base/utils/types/automatedDeviceTypes", 
	"desktop/views/editsection/editSectionBaseView"
], function(template, t, $, n, s, automationProtocolTypes, automatedDeviceTypes, editSectionBaseView) {
    "use strict";
    
    var editSectionTypeView = editSectionBaseView.extend({
        template: template,
        ui: t.extend({
            sectionTypes: ".wizard-block",
            sectionTitle: "#edit-section-title",
            sectionLanguage: "#edit-section-language",
            deviceType: "#edit-device-type"
        }, editSectionBaseView.prototype.ui),
        
        modelEvents: {
            "change:type": "onSectionTypeChange",
            "change:title": "onSectionTitleChange",
            "change:language": "onSectionLanguageChange"
        },
        
        events: {
            "click .section-type": "onSectionTypeClick",
            "keyup #edit-section-title": "onEditSectionTitleChange",
            "change #edit-section-title": "onEditSectionTitleChange",
            "change #edit-device-type": "onEditDeviceTypeChange"
        },
        
        onlyAfterRender: ["renderSectionLanguage", "updateSectionTypesWizardBlocks", "updateSectionLanguage", "updateSectionTitle"].concat(editSectionBaseView.prototype.onlyAfterRender),
        
        initialize: function() {
            this.populateCollections();
        },
        
        serializeData: function() {
            var data = editSectionBaseView.prototype.serializeData.apply(this, arguments);
            data.deviceTypes = automatedDeviceTypes.getDeviceTypes();
            data.sectionTypes = automationProtocolTypes.getSectionTypes();
            data.add = true;       // TODO: Change the model so that model.isNew() actually works
            data.edit = false;
            var t = this.getSectionType();
            
            if (t) 
                data.currentSectionKey = t.key;
                
            var i = this.model.get("updatedAt");
            if (i) 
                data.updatedAt = n.unix(i).fromNow();
            return data;
        },
        
        validate: function(e) {
            var t = [];
            if (!e.get("type")) 
                t.push("type"); 
            if (!e.get("title")) 
                t.push("title"); 
            if (!e.get("deviceType")) 
                t.push("deviceType");
            return t;
        },
        
        populateCollections: function() {
            var e = this.populateAgents();
            var n = this.populateScanners();
            var s = this.getSectionType();
            var a = $.when(s, e, n, this.model.server.get("myPremiumSubscription") && this.model.server.supports("premiumMusic"));
            
            //if (this.model.isNew()) 
            //    a = a.then(t.bind(this.model.setAgentAndScanner, this.model));
            
            a = a.then(e).then(t.bind(this.onAgentsPopulate, this)).then(t.bind(function() {
                this.showValidationResult(this.validate(this.model));
            }, this));
            
            return a;
        },
        
        renderSectionLanguage: function() {
            
            var e = this.model.agents.get(this.model.get("agent")),
                i = e.get("language"),
                n = this.model.get("language");
            if (!t.contains(i, n)) {
                var r = automationProtocolTypes.getSectionTypeByString(this.model.get("type"));
                var o = r.defaultLanguage;
                this.model.set("language", o, {
                    silent: !0
                }), n = o
            }
            this.updateSectionLanguage(), this.ui.sectionLanguage.html(this.createMenuOptions(t.map(i, function(e) {
                return {
                    label: s.getLanguageName(e),
                    value: e
                }
            }), n))
        },
        
        updateSectionTypesWizardBlocks: function() {
            this.ui.sectionTypes.removeClass("selected"); 
            this.ui.sectionTypes.filter('[data-key="' + this.model.get("type") + '"]').addClass("selected"); 
            this.$el.addClass("section-type-chosen");
        },
        
        
        updateSectionLanguage: function() {
            this.ui.sectionLanguage.val(this.model.get("language"))
        },
        
        updateSectionTitle: function() {
            var sectionTitle = this.ui.sectionTitle.val();
            var title = this.model.get("title");
            
            if (sectionTitle !== title) 
                this.ui.sectionTitle.val(title);
        },
        
        onSectionTypeClick: function(e) {
            e.preventDefault();
            var sectionType = $(e.currentTarget);
            if (sectionType.hasClass("selectable")) {
                var key = sectionType.data("key");
                var keyType = { type: key  }; 
                var sectionTypeObj = automationProtocolTypes.getAutomationTypeByID(key);
                
                if (!this.model.get("title"))
                    keyType.title = sectionTypeObj.title; 
                this.model.set(keyType);
            }
        },
        
        onEditSectionTitleChange: function(e) {
            var t = $(e.currentTarget);
            this.model.set("title", t.val()); 
            this.showValidationResult(this.validate(this.model));
        },
        
        onEditDeviceTypeChange: function(e) {
            var t = $(e.currentTarget);
            this.model.set("deviceType", t.val()); 
            this.showValidationResult(this.validate(this.model))
        },
        
        onSectionTypeChange: function(e, typeID) {
            this.updateSectionTypesWizardBlocks();
            var automationProtocol = automationProtocolTypes.getAutomationTypeByID(typeID);
            if (!this.model.get("language")) {
                var n = automationProtocol.defaultLanguage;
                this.model.set("language", n)
            }
            var s = this.model.get("title"),
                r = this.model.previous("type"),
                o = automationProtocolTypes.getSectionType(r),
                l = o && o.title;
            s && s !== l || this.model.set("title", automationProtocol.title), this.setSectionType(automationProtocol), this.model.agents = null, this.model.scanners = null, this.model.settings = null, this.populateCollections()
        },
        
        onSectionTitleChange: function() {
            this.updateSectionTitle(); 
            this.showValidationResult(this.validate(this.model));
        },
        
        onSectionLanguageChange: function() {
            this.updateSectionLanguage();
        },
        
        onAgentsPopulate: function() {
            this.renderSectionLanguage();
        }
    });
    
    return editSectionTypeView;
});