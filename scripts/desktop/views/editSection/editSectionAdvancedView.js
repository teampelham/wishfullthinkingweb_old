define([
	"templates/desktop/section/edit/editSectionAdvancedView.tpl", 
	"underscore", 
	"jquery", 
	"base/commands/commands", 
	"base/utils/languageUtil", 
	"base/utils/types/sectionTypes", 
	"base/models/library/agentCollection", 
	"base/models/library/scannerCollection", 
	"desktop/views/editsection/editSectionBaseView", 
	"desktop/views/editsection/editSectionSettingView", 
	"desktop/views/editsection/editSectionAgentSettingsView"
], function(e, t, i, n, s, a, r, o, l, c, d) {
    "use strict";
    var u = l.extend({
        template: e,
        ui: t.extend({
            sectionAgent: "#edit-section-agent",
            sectionScanner: "#edit-section-scanner",
            thumbnailsCheckbox: 'input[name="enableBIFGeneration"]',
            agentGroup: ".agent-group",
            scannerGroup: ".scanner-group"
        }, l.prototype.ui),
        events: {
            "change #edit-section-agent": "onEditSectionAgentChange",
            "change #edit-section-scanner": "onEditSectionScannerChange",
            "click .delete-thumbnails-btn": "onThumbnailsDeleteClick"
        },
        regions: {
            sectionSettingsRegion: ".section-settings-region",
            agentSettingsRegion: ".section-agent-settings-region"
        },
        onlyAfterRender: ["renderSectionAgent", "renderSectionScanner", "renderSectionSettings", "renderSectionAgentSettings"].concat(l.prototype.onlyAfterRender),
        initialize: function() {
            this.populateCollections()
        },
        onRender: function() {
            this.model.agents.populate(), this.model.scanners.populate()
        },
        validate: function(e) {
            var t = [];
            return e.get("agent") || t.push("agent"), e.get("scanner") || t.push("scanner"), t
        },
        renderSectionSettings: function() {
            var e = new c({
                model: this.model,
                collection: this.model.settings
            });
            this.sectionSettingsRegion.show(e)
        },
        renderSectionAgentSettings: function() {
            var e = new d({
                model: this.model,
                collection: this.model.agentSettings
            });
            this.agentSettingsRegion.show(e)
        },
        populateCollections: function() {
            var e = this.populateAgents(),
                n = this.populateScanners(),
                s = this.populateSectionSettings(),
                a = this.populateSectionAgentSettings();
            return this.populateAgentsProxy(), this.populateScannersProxy(), e.then(t.bind(this.onAgentsPopulate, this)), n.then(t.bind(this.onScannersPopulate, this)), s.then(t.bind(this.renderSectionSettings, this)), a.then(t.bind(this.renderSectionAgentSettings, this)), i.when(e, n, s, a).then(t.bind(this.validate, this, this.model))
        },
        renderSectionAgent: function() {
            var e = this.model.agentsProxy,
                t = this.createMenuOptions(e.map(function(e) {
                    return {
                        label: e.get("name"),
                        value: e.get("identifier")
                    }
                }), this.model.get("agent"));
            this.ui.sectionAgent.html(t), e.length > 1 && this.ui.agentGroup.removeClass("hidden")
        },
        renderSectionScanner: function() {
            var e = this.model.scannersProxy,
                t = this.createMenuOptions(e.map(function(e) {
                    return {
                        label: e.get("name"),
                        value: e.get("name")
                    }
                }), this.model.get("scanner"));
            this.ui.sectionScanner.html(t), e.length > 1 && this.ui.scannerGroup.removeClass("hidden")
        },
        onEditSectionAgentChange: function(e) {
            var n = i(e.currentTarget),
                s = n.val();
            this.model.set("agent", s), this.agentSettingsRegion.close();
            var a = this.model.agents.get(s);
            if (a) {
                var r = a.get("language"),
                    o = this.model.get("language"); - 1 === r.indexOf(o) && this.model.set("language", r[0]), this.model.agentSettings = null, this.populateSectionAgentSettings().then(t.bind(this.renderSectionAgentSettings, this))
            }
            this.showValidationResult(this.validate(this.model))
        },
        onEditSectionScannerChange: function(e) {
            var t = i(e.currentTarget);
            this.model.set("scanner", t.val()), this.showValidationResult(this.validate(this.model))
        },
        onAgentsPopulate: function() {
            this.renderSectionAgent()
        },
        onScannersPopulate: function() {
            this.renderSectionScanner()
        },
        onThumbnailsDeleteClick: function(e) {
            e.preventDefault();
            var t = n.execute("deleteSectionThumbnails", {
                section: this.model
            });
            t.done(this.whenRendered(function() {
                this.ui.thumbnailsCheckbox.prop("checked", !1)
            }))
        }
    });
    return u
});