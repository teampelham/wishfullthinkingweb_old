define([
    "underscore", 
    "jquery", 
    "marionette", 
    "base/utils/types/sectionTypes", 
    "base/models/library/agentCollection", 
    "base/models/library/scannerCollection", 
    "base/models/library/exclusiveAgentProxy", 
    "base/models/library/exclusiveScannerProxy", 
    "base/models/server/serverSettingCollection"
], function(e, t, marionette, n, s, a, r, o, l) {
    "use strict";
    
    var editSectionBaseView = marionette.Layout.extend({
        ui: {
            formGroup: ".form-group"
        },
        
        onlyAfterRender: ["showValidationResult"],
        
        serializeData: function() {
            var data = {};
            if (this.model.isNew()) 
                data.add = true; 
            else
                data.edit = true; 
            
            data.section = this.model.toJSON();
            return data;
        },
        
        showValidationResult: function(t) {
            var i = this.ui.formGroup;
            i.removeClass("has-error"), e.each(t, function(e) {
                i.filter("." + e + "-group").addClass("has-error")
            }), this.trigger(t.length ? "invalid" : "valid")
        },
        
        createMenuOptions: function(t, i) {
            return e.map(t, function(e) {
                return '<option value="' + e.value + '"' + (e.value === i ? " selected" : "") + ">" + e.label + "</option>"
            }).join("\n")
        },
        
        getSectionType: function() {
            if (this.model.sectionType) 
                return this.model.sectionType;
                
            var e = this.model.get("type");
            var t = n.getSectionType(e) || n.getSectionTypeByString(e);
            if ("movie" === e && "com.plexapp.agents.none" === this.model.get("agent")) 
                t = n.getSectionType("videos"); 
                
            this.model.sectionType = t;
            return t;
        },
        
        setSectionType: function(e) {
            this.model.sectionType = e;
        },
        
        populateAgents: function() {
            var e = this.getSectionType();
            if (!e) return t.Deferred().reject().promise();
            var i = {
                server: this.model.server,
                sectionType: e
            };
            return this.model.agents || (this.model.agents = new s([], i)), this.model.agents.populate().then(function(e) {
                return e
            })
        },
        
        populateScanners: function() {
            var e = this.getSectionType();
            return e ? (this.model.scanners || (this.model.scanners = new a([], {
                server: this.model.server,
                sectionType: e
            })), this.model.scanners.populate().then(function(e) {
                return e
            })) : t.Deferred().reject().promise()
        },
        
        populateAgentsProxy: function() {
            var e = this.getSectionType().exclusiveAgents,
                i = new r(this.model.agents, e, this.model.get("agent"));
            return this.model.agentsProxy = i, t.Deferred().resolve(i).promise()
        },
        
        populateScannersProxy: function() {
            var e = this.getSectionType().exclusiveScanners,
                i = new o(this.model.scanners, e, this.model.get("scanner"));
            return this.model.scannersProxy = i, t.Deferred().resolve(i).promise()
        },
        
        populateSectionSettings: function() {
            if (!this.model.server.supports("sectionSettings")) return t.Deferred().resolve().promise();
            if (!this.model.settings) {
                var e = new l([], {
                    server: this.model.server
                });
                if (this.model.isNew()) {
                    var i = {
                        type: this.getSectionType().typeID,
                        agent: this.model.get("agent")
                    };
                    e.url = "/library/sections/prefs?" + t.param(i, !0)
                } else e.url = "/library/sections/" + this.model.id + "/prefs";
                this.model.settings = e
            }
            var n = this.model.settings.populate();
            return n
        },
        
        populateSectionAgentSettings: function() {
            if (!this.model.agentSettings) {
                var e = new l([], {
                    server: this.model.server,
                    url: "/:/plugins/" + this.model.get("agent") + "/prefs"
                });
                this.model.agentSettings = e
            }
            var t = this.model.agentSettings.populate();
            return t
        },
        
        onModalClose: function() {
            this.model.scanners = null, this.model.scannersProxy = null, this.model.agents = null, this.model.agentsProxy = null, this.model.settings = null
        }
    });
    
    return editSectionBaseView;
});