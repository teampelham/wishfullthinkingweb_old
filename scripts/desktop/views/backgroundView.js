define([
	"templates/desktop/backgroundView.tpl", 
	"underscore", 
	"marionette", 
	"base/utils/dispatcher", 
	"base/views/images/baseImage",
    "base/adapters/loggerAdapter"
], function(template, _, marionette, dispatcher, BaseImage, logger) {
    "use strict";
    
	var backgroundView = marionette.ItemView.extend({
        className: "background-container",
        template: template,
        
        ui: {
            background: ".background"
        },
        
        onlyAfterRender: ["onTransitionIn", "onTransitionOut", "onImageLoad"],
        
        initialize: function() {
            _.bindAll(this, "onImageLoad"); 
            this.image = new BaseImage({
                width: 1280,
                height: 720
            }); 
            this.listenTo(dispatcher, "show:background", this.onTransitionIn);
            this.listenTo(dispatcher, "hide:background", this.onTransitionOut);
        },
        
        load: function(background) {
            if (!this.model) 
                return void logger.warn("A background cannot be loaded without a model, current key: " + this.currentKey);
                
            if (this.currentBackground !== background || !_.isEmpty(this.imageFilters) || this.image.hasFilters()) {
                this.currentBackground = background;
                if (!background) 
                    return void this.ui.background.addClass("transition-out");
                this.image.server = this.model.server; 
                this.image.setFilters(this.imageFilters); 
                this.image.load(background, true).done(this.onImageLoad);
            }
        },
        
        onClose: function() {
            this.image.close();
        },
        
        onTransitionIn: function(e) {
            if (this.model && this.model.id === e.model.id)
                return void this.references++;
            
            this.references = 1;
            this.model = e.model;
            this.listenTo(this.model, "change:art", this.onArtChange);
            this.imageFilters = e.imageFilters || {};
            this.currentKey = this.model.get("key");
            this.ui.background.toggleClass("dim", !! e.dim);
            
            return void this.load(this.model.get("art"));
        },
        
        onTransitionOut: function(e) {
            if (this.model && this.model.id === e.id) {
                if (this.references > 1) 
                    return void this.references--;
                
                this.image.cancel();
                this.stopListening(this.model);
                this.model = null;
                this.currentBackground = null;
                this.ui.background.addClass("transition-out");
            }
        },
        
        onArtChange: function(e, t) {
            console.log('onArtChange');
            //this.load(t);
        },
        
        onImageLoad: function(e) {
            console.log('onImageLoad');
            //this.currentBackground && this.ui.background.css("background-image", "url(" + e + ")").removeClass("transition-out");
        }
    });
    
    return backgroundView;
});