define([
    "underscore", 
    "backbone", 
    "marionette", 
    "base/utils/dispatcher", 
    "base/utils/rangeUtil", 
    "base/utils/throttledTaskQueue", 
    "desktop/mixins/multiselectListMixin", 
    "base/views/cells/cellContainerView", 
    "jquery.translate"
], function(_, bb, marionette, dispatcher, rangeUtil, throttledTaskQueue, multiselectListMixin, cellContainerView) {
    "use strict";
    
    var virtualList = cellContainerView.extend({
        mixins: multiselectListMixin,
        paginated: true,
        scrollPosition: 0,
        scrollDirection: "down",
        mergeDelay: 5e3,
        viewsByModel: {},
        
        initialize: function(initObj) {
            cellContainerView.prototype.initialize.apply(this, arguments); 
            _.bindAll(this, "onResize", "onScroll"); 
            this.mediaActions = initObj.mediaActions; 
            multiselectListMixin.initialize.apply(this, arguments);
            
            if (null != initObj.paginated)
                this.paginated = initObj.paginated; 
            
            this.channelName = initObj.channelName || "content"; 
            this.sizeProperty = this.paginated ? "totalSize" : "length"; 
            this.initializingScrollPosition = true; 
            this.merge = _.throttle(_.bind(this.merge, this), this.mergeDelay); 
            
            if(this.collection.scrollPosition) {
                this.restoringScrollPosition = true; 
                this.scrollPosition = this.collection.scrollPosition;
            } 
            
            this.listenTo(this.collection, "reset", this.onCollectionReset); 
            this.listenTo(this.collection, "add", this.onCollectionAdd); 
            this.listenTo(this.collection, "remove", this.onCollectionRemove); 
            dispatcher.trigger(this.channelName + ".subscribe:resize", this.onResize); 
            dispatcher.trigger(this.channelName + ".subscribe:scroll", this.onScroll); 
            dispatcher.trigger("start:scrollListener");
        },
        
        getLockIndexes: function() {
            var lockIndexes;
            var visibleBounds = this.getVisibleBounds();
            var lockBounds = this.getLockBounds();
            var indexBounds = this.getIndexBounds();
            var allVisibleBounds = _.range(visibleBounds.from, visibleBounds.to + 1);
            var visibleLockTo = _.range(visibleBounds.to + 1, lockBounds.to + 1);
            var visibleLockFrom = _.range(visibleBounds.from - 1, lockBounds.from - 1, -1);
            
            if ("down" === this.scrollDirection) {
                visibleLockFrom.reverse(); 
                lockIndexes = allVisibleBounds.concat(visibleLockTo, visibleLockFrom); 
            }
            else {
                allVisibleBounds.reverse(); 
                visibleLockFrom.reverse(); 
                lockIndexes = allVisibleBounds.concat(visibleLockFrom, visibleLockTo);
            } 
            
            return _.filter(lockIndexes, function(lockIndex) {
                return lockIndex >= indexBounds.from && lockIndex <= indexBounds.to;
            }, this);
        },
        
        getVisibleBounds: function() {
            if (this.itemHeight && this.itemsPerRow && this.itemsPerPage) {
                var rowHeight = Math.floor(this.scrollPosition / this.itemHeight) * this.itemsPerRow;
                if (rowHeight > this.getTotalSize()) {
                    this.scrollPosition = 0; 
                    rowHeight = 0;
                }
                 
                return {
                    from: rowHeight,
                    to: rowHeight + this.itemsPerPage - 1 + 2 * this.itemsPerRow
                }
            }
            
            return {
                from: 0,
                to: 0
            };
        },
        
        getLockBounds: function() {
            var pageSize = this.getPageSize();
            var visibleBounds = this.getVisibleBounds();
            
            return {
                from: visibleBounds.from - pageSize,
                to: visibleBounds.to + pageSize
            };
        },
        
        prepareCells: function(cells) {
            var indexBounds = this.getIndexBounds();
            this.pool.filter(function(poolView) {
                if (indexBounds.from <= poolView.id && indexBounds.to >= poolView.id) 
                    return true; 
                
                poolView.view.close(); 
                poolView.view.$el.remove();
                return false;
            }, this);
            
            var newCells = _.reject(cells, this.getModelForId, this);
            
            if (newCells.length) {
                if (this.collection.populatePromise && "pending" === this.collection.populatePromise.state()) 
                    return void this.collection.populatePromise.done(_.bind(this.prepareCells, this, cells));
                
                var range = rangeUtil.roundByPage({
                    start: _.min(newCells),
                    last: _.max(newCells)
                }, 2 * this.getPageSize());
                
                range.reloadThreshold = range.size;
                
                var populatePromise = this.collection.populateRange(range);
                
                this.trigger("change:loading", true, {
                    prepend: "up" === this.scrollDirection
                });
                
                populatePromise.always(_.bind(function() {
                    this.trigger("change:loading", false);
                }, this));
            }
        },
        
        moveCell: function(cell) {
            if (!cell.updatedCSS) {
                cell.view.$el.css({
                    position: "absolute"
                }); 
                cell.updatedCSS = true;
            }
            
            if (this.itemWidth && this.itemsPerRow) {
                var position = cell.id % this.itemsPerRow;
                var row = Math.floor(cell.id / this.itemsPerRow);
                
                cell.view.$el.translate(position * this.itemWidth, row * this.itemHeight); 
                cell.movedToIndex = cell.id;
            }
        },
        
        moveElement: function() {},
        
        createCellView: function(cell) {
            var itemViewOptions = marionette.getOption(this, "itemViewOptions");
            
            if (_.isFunction(itemViewOptions)) 
                itemViewOptions = itemViewOptions.call(this, cell); 
                
            itemViewOptions = _.clone(itemViewOptions); 
            itemViewOptions.model = this.getModelForId(cell.id); 
            itemViewOptions.mediaActions = this.mediaActions; 
            
            if (cell.el) 
                itemViewOptions.el = cell.el;
                
            var itemView = marionette.getOption(this, "itemView");
            if (!itemView._ensureElementExtension) 
                itemView._ensureElementExtension = itemView.extend({
                    _ensureElement: function() {
                        var element = this.el ? _.result(this, "el") : bb.$("<" + _.result(this, "tagName") + ">");
                        
                        this.setElement(element, false);
                        
                        var attributes = _.extend({}, _.result(this, "attributes"));
                        
                        if (this.id) 
                            attributes.id = _.result(this, "id");
                        
                        if (this.className) 
                            attributes["class"] = _.result(this, "className"); 
                        
                        this.$el.attr(attributes);
                    }
                });
            
            var cellView = new itemView._ensureElementExtension(itemViewOptions);
            
            if (!cell.el) {
                this.$el.append(cellView.$el); 
                cell.el = cellView.el;
            } 
            
            this.forwardItemViewEvents(cellView); 
            cellView.remove = function() {
                this.stopListening(); 
                this.$el.unbind(); 
                this.$el.empty();
                return this;
            }; 
            return cellView;
        },
        
        getPageSize: function() {
            return this.itemsPerPage || 1;
        },
        
        clearAllReusedCells: function() {
            this.pool.each(function(poolView) {
                var filledWithSame = poolView.filledWithIndex === poolView.id;
                var filledWithNone = -1 === poolView.filledWithIndex;
                
                if (!(filledWithSame || filledWithNone)) {
                    this.clearCell(poolView); 
                    poolView.filledWithIndex = -1;
                }
            }, this);
        },
        
        getTotalSize: function() {
            return this.collection[this.sizeProperty] || 0;
        },
        
        fillCell: function(cell) {
            var model = this.getModelForId(cell.id);
            
            if (model && model === cell.view.model && !cell.view.isClosed) 
                return true; 
            
            this.clearCell(cell); 
            
            if (model) {
                cell.view = this.createCellView(cell); 
                cell.view.render(); 
                this.indexViewByModel(cell.view);
                return true;
            }
            
            return false;
        },
        
        clearCell: function(cell) {
            var view = cell.view;
            if (view.model) 
                delete this.viewsByModel[view.model.cid];
            
            if (!view.isClosed) 
                view.close();
        },
        
        getIndexBounds: function() {
            return {
                from: 0,
                to: this.getTotalSize() - 1
            };
        },
        
        getModelForId: function(id) {
            return this.collection.at(id - this.collection.populatedRange.start);
        },
        
        getViewForModel: function(model) {
            return this.viewsByModel[model.cid];
        },
        
        indexViewByModel: function(model) {
            this.viewsByModel[model.model.cid] = model;
        },
        
        render: function() {
            this.updateHeight(); 
            
            if (!this.restoringScrollPosition) {
                cellContainerView.prototype.render.apply(this, arguments); 
                this.clearAllReusedCells();
            }
        },
        
        updateHeight: function() {
            if (this.recalculateHeight) {
                this.recalculateHeight = false; 
                
                var indexBounds = this.getIndexBounds().to || 1;
                this.listHeight = Math.ceil(indexBounds / this.itemsPerRow) * this.itemHeight - this.itemMarginBottom; 
                this.$el.css("height", this.listHeight); 
                this.initScrolling();
            }
        },
        
        itemViewOptions: function() {
            var itemType, sort;
            var section = this.collection.section;
            
            if (section) {
                var currentType = section.get("currentType");
                var currentSort = this.collection.getSort();
                itemType = currentType && currentType.get("typeString");
                
                if (currentSort) {
                    var sorts = currentSort.split(":");
                    if (sorts.length)
                        sort = _.first(sorts);
                }
            } 
            else 
                itemType = "clip";
            
            return {
                type: itemType,
                sort: sort
            };
        },
        
        measureItem: function() {
            var cellView = this.createCellView({
                id: this.collection.populatedRange.start
            });
            
            if (!cellView.model) 
                this.remeasureItem = true; 
            
            cellView.render(); 
            cellView.$el.css("visibility", "hidden");
            var renderedView = cellView.$el.appendTo(this.$el);
            
            _.defer(_.bind(function() {
                this.itemWidth = renderedView.outerWidth(true); 
                this.itemHeight = renderedView.outerHeight(true); 
                this.itemMarginRight = parseInt(renderedView.css("margin-right"), 10); 
                this.itemMarginBottom = parseInt(renderedView.css("margin-bottom"), 10); 
                cellView.close(); 
                cellView.$el.remove(); 
                this.measureContainer(); 
                this.render();
            }, this));
        },
        
        measureContainer: function() {
            var marginTop = parseInt(this.$el.css("margin-top"), 10);
            var marginBottom = parseInt(this.$el.css("margin-bottom"), 10);
            this.containerWidth = this.$el.width(); 
            this.containerHeight = this.containerSize.height - marginTop - marginBottom; 
            this.itemsPerRow = Math.floor(this.containerWidth / this.itemWidth); 
            this.itemsPerColumn = Math.floor((this.containerHeight + this.itemMarginBottom) / this.itemHeight); 
            this.itemsPerPage = this.itemsPerRow * this.itemsPerColumn; 
            this.itemsPadding = this.itemsPerRow * Math.max(this.itemsPerColumn, 2); 
            this.itemsPerPageWithPadding = 2 * this.itemsPadding + this.itemsPerPage; 
            
            if (this.itemHeight) 
                this.recalculateHeight = true;
        },
        
        scrollToIndex: function(idx) {
            var rowHeight = Math.floor(idx / this.itemsPerRow);
            var height = rowHeight * this.itemHeight;
            
            this.restrictScrollDirection = true; 
            dispatcher.trigger("update:scrollPosition", height);
        },
        
        merge: function() {
            if (!this.isClosed) {
                var populatedRange = this.collection.populatedRange;
                var start = populatedRange.start;
                var end = populatedRange.end > this.itemsPerPageWithPadding ? populatedRange.end : this.itemsPerPageWithPadding;
                
                if (end) {
                    if (start > 0) {
                        this.scrollDirection = "up";
                        return void this.collection.populateRange({
                            start: 0,
                            size: start
                        });
                    }
                    
                    this.collection.pageStart = start; 
                    this.collection.pageSize = end;
                    var s = this.collection.fetch();
                    
                    s.done(_.bind(function() {
                        if (this.collection.length < end) 
                            end = this.collection.length; 
                        
                        this.collection.populatedRange = {
                            start: populatedRange.start,
                            end: populatedRange.start + end,
                            size: end
                        }; 
                        this.collection.scrollPosition = this.scrollPosition; 
                        this.onCollectionReset();
                    }, this));
                }
            }
        },
        
        initScrolling: function() {
            if (this.initializingScrollPosition) {
                this.restrictScrollDirection = true; 
                this.initializingScrollPosition = false; 
                this.restoringScrollPosition = false; 
                
                if (this.collection.scrollPosition) 
                    dispatcher.trigger("update:scrollPosition", this.collection.scrollPosition); 
                else if (this.containerScrollPosition.scrollTop > 0) 
                    dispatcher.trigger("update:scrollPosition", 0);
            }
        },
        
        onDeselectAll: function(subset) {
            _.each(subset, function(model) {
                this.pool.each(function(poolView) {
                    if (poolView.view.model === model) 
                        poolView.view.deselect();
                }, this);
            }, this);
        },
        
        onClose: function() {
            cellContainerView.prototype.onClose.apply(this, arguments); 
            multiselectListMixin.onClose.apply(this, arguments); 
            dispatcher.trigger("stop:scrollListener"); 
            dispatcher.trigger(this.channelName + ".unsubscribe:resize", this.onResize); 
            dispatcher.trigger(this.channelName + ".unsubscribe:scroll", this.onScroll); 
            this.collection.scrollPosition = this.scrollPosition;
        },
        
        onCollectionReset: function(e, oldQueue) {
            throttledTaskQueue.cancel(this.cid); 
            
            this.pool.each(function(poolView) {
                poolView.id = poolView.filledWithIndex = this.collection.indexOf(poolView.view.model);
            }, this); 
            
            if (oldQueue && oldQueue.previousModels) 
                _.each(oldQueue.previousModels, this.onCollectionRemove, this); 
            
            this.initializingScrollPosition = true; 
            this.measureItem(); 
            this.render(); 
            
            if (!this.getTotalSize()) 
                this.trigger("change:loading", false);
        },
        
        addItemToList: function(item) {
            this.remeasureItem = true;
            this.collection.addItem(item);
            this.collection.reset(this.collection.models);
        },
        
        onCollectionAdd: function(e) {
            if (this.remeasureItem) {
                this.remeasureItem = false; 
                this.measureItem(e);
            }
        },
        
        onCollectionRemove: function(model) {
            var poolView = this.pool.findByModel(model);
            var view = poolView && poolView.view || {
                model: model,
                deselect: function() {}
            };
            this.triggerMethod("item:removed", view);
        },
        
        onResize: function(size) {
            this.containerSize = size; 
            this.measureItem(); 
            this.scrollDirection = this.containerWidth > this.$el.width() ? "up" : "down"; 
            this.render();
        },
        
        onScroll: function(scrollPosition) {
            this.containerScrollPosition = scrollPosition; 
            
            if (!this.restoringScrollPosition) {
                if (!this.itemWidth) 
                    this.measureItem(); 
                
                this.scrollDirection = 0 !== this.containerScrollPosition.scrollTop && this.containerScrollPosition.scrollTop < this.scrollPosition ? "up" : "down"; 
                if (this.restrictScrollDirection) {
                    this.scrollDirection = "down"; 
                    this.restrictScrollDirection = false;
                } 
                this.scrollPosition = this.containerScrollPosition.scrollTop; 
                this.render();
            }
        }
    });
    
    return virtualList;
});