define([
	"marionette", 
	"jquery", 
	"desktop/views/lists/server/browseDirectoryListItem"
], function(e, t, i) {
    "use strict";
    var n = e.CollectionView.extend({
        tagName: "ul",
        className: "list browse-directory-list",
        itemView: i,
        events: {
            "click a": "onDirectoryClick"
        },
        collectionEvents: {
            reset: "onReset"
        },
        initialize: function(e) {
            this.root = e.root
        },
        loadDirectory: function(e) {
            this.$("a").removeClass("selected");
            var t = this.children.findByModel(e);
            t && t.markAsSelected(!0)
        },
        unloadDirectory: function(e) {
            var t = this.children.findByModel(e);
            t && t.markAsSelected(!1)
        },
        unselectDirectory: function(e) {
            var t = this.children.findByModel(e);
            t && t.removeSelection()
        },
        onReset: function() {
            this.parentItem && this.collection.add(this.parentItem, {
                at: 0,
                silent: !0
            })
        },
        onDirectoryClick: function(e) {
            e.preventDefault(), this.collection.hasPendingPopulate() && (this.collection.abortPendingPopulate(), this.unselectDirectory(this.collection.parent));
            var i = t(e.currentTarget),
                n = i.data("key"),
                s = this.collection.get(n) || this.parentItem;
            this.trigger("change:path", s), this.root || (this.collection.parent = s, s.get("parent") ? (this.parentItem = s.get("parent").clone(), this.parentItem.set("back", !0), this.parentItem.set("title", ".. [Back]")) : this.parentItem = null, this.collection.fetch({
                reset: !0
            }))
        }
    });
    return n
});