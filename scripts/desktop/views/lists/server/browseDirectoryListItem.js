define([
	"templates/desktop/lists/server/browseDirectoryListItem.tpl", 
	"marionette"
], function(e, t) {
    "use strict";
    var i = t.ItemView.extend({
        tagName: "li",
        className: "striped-list-item browse-directory-list-item",
        template: e,
        ui: {
            link: "a",
            spinner: ".loading"
        },
        events: {
            "click a": "onDirectoryClick"
        },
        markAsSelected: function(e) {
            this.ui.link.addClass("selected"), this.ui.spinner.toggleClass("hidden", !e)
        },
        removeSelection: function() {
            this.ui.link.removeClass("selected"), this.ui.spinner.addClass("hidden")
        },
        onDirectoryClick: function(e) {
            e.preventDefault(), this.markAsSelected(!0)
        }
    });
    return i
});