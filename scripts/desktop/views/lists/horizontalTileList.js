define(["desktop/views/lists/baseList"], function(baseList) {
    "use strict";
	
    var horizontalTileList = baseList.extend({
        measure: true,
        page: 1,
        pagesPadding: 1,
        itemViewOptions: {
            skipFirstRender: true
        },
		
        initialize: function(init) {
            this.$container = init.$container;
        },
		
        scroll: function() {
            var pagedWidth = (this.page - 1) * this.pageWidth;
            var skip = (this.page - 1) * this.itemsPerPage;
            var take = skip + this.itemsPerPage;
			
            skip -= this.itemsPadding; 
			take += this.itemsPadding;
			
            for (var idx = 0, childrenCount = this.children.length; childrenCount > idx; idx++) {
                var child = this.children.findByIndex(idx);
                if (idx >= skip && take > idx)
					child.show(); 
				else
					child.hide();
            }
            this.trigger("scroll", this, this.page, -1 * pagedWidth);
        },
		
        previous: function() {
            this.page = Math.max(this.page - 1, 1); 
			this.scroll();
        },
		
        next: function() {
            this.page = Math.min(this.page + 1, this.pages); 
			this.scroll();
        },
		
        onMeasure: function() {
            this.containerWidth = this.$container.width(); 
			this.listWidth = this.itemWidth * this.children.length; 
			this.itemsPerPage = Math.floor((this.containerWidth + this.itemMarginRight) / this.itemWidth); 
			this.itemsPadding = this.pagesPadding * this.itemsPerPage; 
			this.pageWidth = this.itemWidth * this.itemsPerPage; 
			this.pages = Math.ceil(this.listWidth / this.pageWidth); 
			this.$el.width(this.listWidth); 
			this.$el.removeClass("hidden"); 
			this.page = 1; 
			this.scroll();
        },
		
        onCollectionBeforeClose: function() {
            this.isClosing = true;
        },
		
        onCollectionClosed: function() {
            this.isClosing = false;
        },
		
        onItemRemoved: function() {
            if (this.isClosing) 
				this.measureList();
        }
    });
	
    return horizontalTileList;
});