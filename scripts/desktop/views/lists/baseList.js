define([
	"underscore", 
	"marionette"
], function(_, marionette) {
    "use strict";
	
    var baseList = marionette.CollectionView.extend({
		
        appendHtml: function() {
            if (this.sorted) 
				this.sortedAppendHtml.apply(this, arguments) 
			else if (this.reversed) 
				this.reversedAppendHtml.apply(this, arguments) 
			else
				marionette.CollectionView.prototype.appendHtml.apply(this, arguments);
        },
		
        sortedAppendHtml: function(first, view, count) {
            if (first.isBuffering) {
                var childNodes = first.elBuffer.childNodes;
                
                if (childNodes.length <= count)  
                    first.elBuffer.appendChild(view.el);
                else 
                    first.elBuffer.insertBefore(view.el, childNodes[count]); 
                first._bufferedChildren.push(view);
            } 
            else {
                var el = first.itemViewContainer ? first.$(e.itemViewContainer) : first.$el;
                var elChildren = el.children();
                
                if (elChildren.size() <= count) 
                    el.append(view.el); 
                else
                    elChildren.eq(count).before(view.el);
            }
        },
		
        reversedAppendHtml: function(e, t) {
			console.warn('reverse');
            /*if (e.isBuffering) e.elBuffer.insertBefore(t.el, e.elBuffer.firstChild), e._bufferedChildren.push(t);
            else {
                var i = e.itemViewContainer ? e.$(e.itemViewContainer) : e.$el;
                i.prepend(t.el)
            }*/
        },
		
        measureList: function() {
            if (this.children.length) {
                var childViews = this.children._views;
                var firstView = childViews[_.first(_.keys(childViews))];
                
                _.defer(_.bind(function() {
                    this.itemWidth = firstView.$el.outerWidth(true); 
                    this.itemHeight = firstView.$el.outerHeight(true); 
                    this.itemMarginRight = parseInt(firstView.$el.css("margin-right"), 10); 
                    this.itemMarginBottom = parseInt(firstView.$el.css("margin-bottom"), 10); 
                    this.triggerMethod("measure", this);
                }, this));
            }
        },
		
        getViewForModel: function(model) {
            var viewIdx = this.children._indexByModel[model.cid];
            return this.children._views[viewIdx];
        },
		
        onCollectionRendered: function() {
            if (this.measure) 
				this.measureList();
        }
    });
	
    return baseList;
});