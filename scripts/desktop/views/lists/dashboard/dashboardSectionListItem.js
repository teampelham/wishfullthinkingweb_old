define([
	"templates/desktop/lists/dashboard/dashboardSectionListItem.tpl", 
	"marionette", 
	"base/utils/dispatcher", 
	//"desktop/views/modals/editSectionModal"
], function(template, marionette, dispatcher) {
    "use strict";
    var s = {
        artist: "music",
        movie: "film",
        photo: "picture",
        show: "display"
    }; 
	var dashboardSectionListItem = marionette.ItemView.extend({
		tagName: "li",
		className: "dashboard-section-list-item",
		template: template,
		
		ui: {
			loadingBtn: ".btn-loading"
		},
		
		events: {
			"click .edit-section-btn": "onEditClick"
		},
		
		initialize: function(initObj) {
			this.showActions = initObj.showActions; 
			
			if (this.showActions) { 
				this.listenTo(this.model, "change:refreshing", this.onRefreshingChange); 
				this.refreshing = this.model.get("refreshing");
			}
		},
		
		serializeData: function() {
			var server = this.model.server;
			var model = this.model.toJSON();
			return {
				section: model,
				serverID: server.id,
				icon: s[model.type],
				showActions: this.showActions,
				refreshing: this.refreshing
			}
		},
		
		bindings: {
			".section-title": {
				modelAttribute: "title"
			}
		},
		
		onEditClick: function(e) {
			e.preventDefault();
			console.warn('edit');
			/*dispatcher.trigger("show:modal", new n({
				server: this.model.server,
				model: this.model
			}))*/
		},
		
		onRefreshingChange: function(e, isRefreshing) {
			this.ui.loadingBtn.toggleClass("active", isRefreshing);
		}
	});
		
    return dashboardSectionListItem;
});