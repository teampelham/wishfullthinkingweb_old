define([
	"desktop/views/lists/baseList", 
	"desktop/views/lists/dashboard/dashboardServerListItem"
], function(baseList, dashboardServerListItem) {
    "use strict";
    
	var dashboardServerList = baseList.extend({
        tagName: "ul",
        className: "list dashboard-server-list",
        itemView: dashboardServerListItem,
        sorted: true
    });
	
    return dashboardServerList;
});