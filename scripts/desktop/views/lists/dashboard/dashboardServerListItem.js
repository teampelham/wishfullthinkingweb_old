define([
	"templates/desktop/lists/dashboard/dashboardServerListItem.tpl", 
	"jquery", 
	"underscore", 
	"marionette", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/adapters/settingsAdapter", 
	"base/commands/commands", 
	"base/models/appModel", 
	"base/models/library/agentCollection", 
	"base/models/server/serverStorageModel", 
	"desktop/views/lists/dashboard/dashboardSectionListItem", 
	//"desktop/views/modals/editSectionModal", 
	//"desktop/views/modals/user/editFriendModal", 
	//"desktop/views/alerts/premiumMusicAlertBarView"
], function(template, $, _, marionette, t, dispatcher, settingsAdapter, commands, appModel, AgentCollection, serverStorageModel, dashboardSectionListItem) {
    "use strict";
	
    var dashboardServerListItem = marionette.CompositeView.extend({
        tagName: "li",
        className: "dashboard-server-list-item",
        template: template,
        itemView: dashboardSectionListItem,
        itemViewContainer: ".dashboard-section-list",
        itemViewOptions: function() {
            return {
                showActions: this.showActions
            }
        },
        ui: {
            actions: ".side-bar-actions",
            selectServerBtn: ".select-server-btn",
            updateBtnLabel: ".update-btn .dropdown-truncated-label",
            secureConnectionIcon: ".secure-connection-icon"
        },
        modelEvents: {
            "change:connected": "onConnectedChange",
            "change:shared": "onSharedChange",
            "change:activeConnection": "onActiveConnectionChange"
        },
        events: {
            "click .select-server-btn": "onSelectServerClick",
            "click .add-library-btn": "onAddLibraryClick",
            "click .settings-btn": "onSettingsClick",
            "click .share-btn": "onShareClick",
            "click .update-btn": "onUpdateClick",
            "click .optimize-btn": "onOptimizeClick",
            "click .clean-bundles-btn": "onCleanBundlesClick",
            "click .empty-trash-btn": "onEmptyTrashClick",
            "click .mixed-content-btn": "onMixedContentClick",
            "click .http-fallback-btn": "onHttpFallbackClick",
            "click .remove-btn": "onRemoveClick"
        },
		
        onlyAfterRender: ["onPrimaryServerChange", "onSectionsChange", "onSectionRefreshingChange"],
        
        initialize: function() {
		    this.collection = this.model.get("sections"); 
            this.user = appModel.get("user"); 
            this.listenTo(appModel, "change:primaryServer", this.onPrimaryServerChange); 
            this.listenTo(this.user, "change:signedIn", this.onSignedInChange); 
            this.setup();
        },
		
        setup: function() {
            var isOutThere = this.model.get("shared") || this.model.get("synced");
            this.allowCancel = this.model.supports("cancelAllUpdates") && !isOutThere; 
            this.showActions = !isOutThere;
             
            if (this.model.get("connected")) {
                if (this.allowCancel && this.collection.length) 
                    this.updateRefreshingStatus();
                
                var noPromise = !this.collection.populatePromise;
                var verb = noPromise ? "reset" : "add remove";
                var listItems = this.collection.fetch({
                    reset: noPromise
                });
                
                this.agents = new AgentCollection([], {
                    server: this.model
                });
                
                var isPrimary = this.model === appModel.get("primaryServer");
                var hasDismissedMusic = settingsAdapter.get("hasDismissedPremiumMusicAlert");
                
                if (!isOutThere && isPrimary && !hasDismissedMusic) {
                    var agentPromise = this.agents.populate();
                    var agentCallback = $.when(listItems, agentPromise);
                    
                    agentCallback.done(_.bind(this.checkForPremiumMusic, this));
                }
                
                if (this.allowCancel) {
                    this.listenTo(this.collection, verb, this.onSectionsChange); 
                    this.listenTo(this.collection, "change:refreshing", this.onSectionRefreshingChange);
                }
            }
        },
        
        serializeData: function() {
            var primaryServer = appModel.get("primaryServer");
            var modelJson = this.model.toJSON();
            var hostedAvailable = this.model.useHostedHttps && modelJson.presence && !modelJson.available;
            var unAvailable = !this.model.useHostedHttps && modelJson.presence && !modelJson.available;
            
            return {
                server: modelJson,
                selected: primaryServer && this.model.id === primaryServer.id,
                signedIn: this.user.get("signedIn"),
                canBePrimary: this.model.canBePrimary(),
                showActions: this.showActions,
                showChannels: this.showActions && settingsAdapter.get("dashboardChannelsEnabled"),
                showPlaylists: this.model.supports("playlists") && !this.model.get("synced"),
                showUnavailableMessage: !hostedAvailable && !unAvailable && !modelJson.available,
                showMixedContentMessage: hostedAvailable,
                showHttpFallbackMessgae: unAvailable,
                hasRefreshingSection: this.hasRefreshingSection,
                hasSecureConnection: this.model.hasSecureConnection()
            };
        },
		
        updateRefreshingStatus: function() {
            this.hasRefreshingSection = this.collection.some(function(e) {
                return e.get("refreshing");
            });
        },
        
        getUpdateLabel: function() {
			console.warn('listItem');
            //return s(this.hasRefreshingSection ? "Cancel Update" : "Update Libraries")
        },
		
        checkForPremiumMusic: function() {
            /*if (!this.user.get("signedIn")) return void this.listenToOnce(this.user, "change:signedIn", this.checkForPremiumMusic);
            if (this.user.hasPlexPass() && this.model.supports("premiumMusic")) {
                var e = this.collection.findWhere({
                    agent: "com.plexapp.agents.lastfm"
                }),
                    t = this.collection.findWhere({
                        agent: "com.plexapp.agents.plexmusic"
                    }),
                    i = this.agents.get("com.plexapp.agents.plexmusic");
                e && !t && i && a.trigger("show:bar", "alertbar", new m({
                    model: this.model,
                    seed: e
                }))
            }*/
        },
        
        onPrimaryServerChange: function(e, server) {
            this.ui.selectServerBtn.toggleClass("selected", server && server.id === this.model.id);
        },
		
        onSignedInChange: function(e, t) {
			console.warn('listItem');
            //this.ui.actions.toggleClass("signed-in", !! t)
        },
		
        onSectionsChange: function() {
			console.warn('listItem');
            //this.updateRefreshingStatus(), this.ui.updateBtnLabel.text(this.getUpdateLabel())
        },
		
        onSectionRefreshingChange: function(e, t) {
			console.warn('listItem');
            //this.hasRefreshingSection = t, this.ui.updateBtnLabel.text(this.getUpdateLabel())
        },
		
        onConnectedChange: function() {
			console.warn('listItem');
            //this.setup(), this.render()
        },
		
        onSharedChange: function() {
            this.render();
        },
		
        onActiveConnectionChange: function(connection) {
            this.ui.secureConnectionIcon.toggleClass("hidden", !connection.hasSecureConnection());
        },
		
        onSelectServerClick: function(e) {
            e.preventDefault();
            
            var btn = this.ui.selectServerBtn;
            if (!btn.hasClass("disabled") && !btn.hasClass("selected")) {
                appModel.set("primaryServer", this.model); 
                serverStorageModel.save("lastPrimaryServerID", this.model.id);
            }
        },
		
        onAddLibraryClick: function(e) {
			console.warn('listItem');
            /*e.preventDefault(), a.trigger("show:modal", new h({
                server: this.model,
                redirect: !0
            }))*/
        },
		
        onSettingsClick: function(e) {
			console.warn('listItem');
            /*e.preventDefault(), a.trigger("navigate", "serverSettings", {
                serverID: this.model.id
            })*/
        },
		
        onShareClick: function(e) {
			console.warn('listItem');
            /*e.preventDefault(), a.trigger("show:modal", new p({
                defaultPaneType: "ownedServer",
                defaultPaneID: this.model.id
            }))*/
        },
		
        onUpdateClick: function(e) {
			console.warn('listItem');
            /*e.preventDefault();
            var t = this.allowCancel ? this.hasRefreshingSection : !1;
            o.execute("updateLibrary", {
                server: this.model,
                cancel: t
            })*/
        },
		
        onOptimizeClick: function(e) {
			console.warn('listItem');
            /*e.preventDefault(), o.execute("optimize", {
                server: this.model
            })*/
        },
		
        onCleanBundlesClick: function(e) {
			console.warn('listItem');
            /*e.preventDefault(), o.execute("cleanBundles", {
                server: this.model
            })*/
        },
		
        onEmptyTrashClick: function(e) {
			console.warn('listItem');
            /*e.preventDefault(), o.execute("emptyTrash", {
                server: this.model
            })*/
        },
		
        onMixedContentClick: function(e) {
			console.warn('listItem');
            //e.preventDefault(), o.execute("redirectToHttp")
        },
		
        onHttpFallbackClick: function(e) {
			console.warn('listItem');
            //e.preventDefault(), o.execute("allowHttpFallback", this.model)
        },
		
        onRemoveClick: function(e) {
			console.warn('listItem');
            /*e.preventDefault(), o.execute("removeServer", {
                server: this.model
            })*/
        }
    });
	
    return dashboardServerListItem;
});