define([
    "desktop/mixins/multiselectListMixin", 
    "desktop/views/lists/baseList", 
    "desktop/views/lists/library/seasonTileListItem"
], function(e, t, i) {
    "use strict";
    var n = t.extend({
        mixins: [e],
        tagName: "ul",
        className: "tile-list media-tile-list season-tile-list",
        itemView: i
    });
    return n
});