define([
	"templates/desktop/lists/library/mediaListItem.tpl", 
	"jquery", 
	"underscore", 
	"moment", 
	"marionette", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/utils/mediaUtil", 
	"base/utils/formatters/dateTimeUtil", 
	"base/utils/formatters/metadataUtil", 
	"base/adapters/settingsAdapter", 
	"base/commands/commands", 
	"desktop/mixins/multiselectListItemMixin", 
	"desktop/models/mediaActionsModel", 
	"desktop/views/lists/library/mediaProgressView", 
	"desktop/views/ratingView", 
	"desktop/views/dropdowns/mediaActionsDropdown", 
	"desktop/views/modals/editMetadataModal"
], function(template, $, _, n, marionette, a, dispatcher, o, l, c, d, commands, multiselectListItemMixin, MediaActionsModel, MediaProgressView, RatingView, MediaActionsDropdown, EditMetadataModal) {
    "use strict";
    
    var mediaListItem = marionette.Layout.extend({
        mixins: [multiselectListItemMixin],
        tagName: "li",
        className: "striped-list-item media-list-item",
        template: template,
        visible: false,
        
        ui: {
            container: ".media-list-inner-item",
            actions: ".media-actions",
            dropdown: ".media-actions-dropdown"
        },
        
        regions: {
            rating: ".media-rating",
            progress: ".media-progress"
        },
        
        events: {
            "mouseenter > a": "onMouseEnter",
            "mouseleave > a": "onMouseLeave",
            "mousedown > a": "onMouseDown",
            "click > a": "onClick",
            "click .play-btn": "onPlayClick",
            "click .play-unwatched-btn": "onPlayUnwatchedClick",
            "click .edit-btn": "onEditClick",
            "click .more-btn": "onMoreClick",
            "click .media-title-link": "onTitleClick",
            "hide.bs.dropdown .media-actions-dropdown": "onDropdownHide"
        },
        
        onlyAfterRender: ["select", "deselect"],
        
        initialize: function(initObj) {
            _.bindAll(this, "onResize"); 
            this.skipFirstRender = initObj.skipFirstRender; 
            //this.continuousPlay = initObj.continuousPlay;
            var type;
            
            if (this.model) {
                type = this.model.get("type");
                this.listenTo(this.model, "change:onLevel", this.onLightLevelChange);
            }
            if (!type) 
                type = initObj.type;
                 
            this.type = type; 
            this.sort = initObj.sort; 
            this.$el.addClass(type); 
            
            if (this.sort && /^(?:addedAt|originallyAvailableAt|rating|mediaHeight|duration)$/.test(this.sort)) 
                this.$el.addClass("show-sort"); 
                
            if (type === "light" && this.model && this.model.get("OnLevel") > 0) 
                this.$el.addClass("on");
            
            this.parentMediaActions = initObj.mediaActions;
            var server = this.model && this.model.server || this.parentMediaActions && this.parentMediaActions.server;
            
            this.mediaActions = new MediaActionsModel({}, {
                item: this.model,
                type: type,
                server: server
            }), 
            
            this.listenTo(this.mediaActions, {
                "change:showPlay": this.onActionsChange,
                "change:showEdit": this.onActionsChange,
                "change:showListDropdown": this.onActionsChange
            }), 
            
            dispatcher.trigger("content.subscribe:resize", this.onResize);
            
            if(type === "light")
                this.listenTo(this.model, "change:onLevel", this.onLightLevelChange);
        },
        
        serializeData: function() {
            this.shared = this.model.server.get("shared"); 
            this.synced = this.model.server.get("synced"); 
            this.multiuser = this.model.server.get("multiuser");
            var model = this.model.toJSON();
            var url = "#!/server/" + encodeURIComponent(this.model.server.id);
            
            if (model.isFolder) 
                url += "/section/" + encodeURIComponent(this.model.section.id) + "/folder/" + encodeURIComponent(model.key);
            else if (model.isChannel) {
                var i = this.model.channel;
                var n = i.id ? i.id : i.get("identifier");
                url += "/channel/" + encodeURIComponent(n); 
                url += "clip" === model.type ? "/details/" + encodeURIComponent(model.key) : "/" + encodeURIComponent(model.key);
            } 
            else if ("userPlaylistItem" === model.type) 
                url = "#!/playlist/" + model.playlist + "/details/" + model.id;
            else if (this.model.section) 
                url += "/section/" + encodeURIComponent(this.model.section.id) + "/details/" + encodeURIComponent(model.key);
            else
                url += "/details/" + encodeURIComponent(model.key);
            
            return {
                item: model,
                url: url,
                selected: this.model.selected,
                showActions: this.showActions(),
                sort: this.sort
            }
        },
        
        render: function() {
            if (!this.model)
                return this;

            if (this.skipFirstRender) {
                this.skipFirstRender = false;
                return this;
            }

            this.visible = true;
            return void marionette.Layout.prototype.render.apply(this, arguments);
        },
        
        show: function() {
            if (this.model && !this.visible) {
                if (this.image)
                    this.image.setModel(this.model);
                
                if (this.mediaActions)
                    this.mediaActions.setParent({
                        item: this.model
                    });
                this.visible = false;
                this.render();
            }
        },
        
        hide: function() {
            if (this.model && this.visible) {
                if (this.image) 
                    this.image.cancel(); 
                this.visible = false; 
                this.stopListening(this.model); 
                this.$el.empty();
            }
        },
        
        select: function() {
            this.ui.container.addClass("selected");
        },
        
        deselect: function() {
            this.ui.container.removeClass("selected");
        },
        
        getPosterUrl: function(item) {
            if (this.type === "show")
                this.model.get("grandparentThumb") || this.model.get("parentThumb") || item;
                
            return item || this.model.get("parentThumb") || this.model.get("grandparentThumb");
        },
        
        showActions: function() {
            return this.mediaActions && (this.mediaActions.get("showPlay") || this.mediaActions.get("showEdit") || this.mediaActions.get("showListDropdown"));
        },
        
        canPlaySiblings: function() {
            return this.model.get("isExtra") ? false : /^(track|photo|clip|userPlaylistItem)$/.test(this.type);
        },
        
        bindings: {
            ".play-btn": {
                model: "mediaActions",
                modelAttribute: "showPlay",
                manual: true,
                autohide: true
            },
            ".play-unwatched-btn": {
                model: "mediaActions",
                modelAttribute: "showPlayUnwatched",
                manual: true,
                autohide: true
            },
            ".edit-btn": {
                model: "mediaActions",
                modelAttribute: "showEdit",
                manual: true,
                autohide: true
            },
            ".more-btn": {
                model: "mediaActions",
                modelAttribute: "showListDropdown",
                manual: true,
                autohide: true
            },
            ".unwatched-icon": {
                modelAttribute: "viewCount",
                modelListeners: ["viewCount", "viewOffset"],
                manual: !0,
                autohide: function() {
                    if (!this.bypassShowUnwatchedIconsSetting && !d.get("showUnwatchedIcons")) return !1;
                    if (!/^(movie|episode|userPlaylistItem)$/.test(this.model.get("type"))) return !1;
                    if (this.model.get("isFolder")) return !1;
                    if (this.shared && !this.multiuser) return !1;
                    var e = this.model.get("viewCount"),
                        t = this.model.get("viewOffset");
                    return e ? t : !0
                },
                formatter: function(e, t) {
                    var i = this.model.get("viewOffset");
                    i && t.toggleClass("unwatched-icon-partial", !0)
                }
            },
            ".media-title": {
                modelAttribute: "title",
                modelListeners: ["title", "parentTitle", "grandparentTitle"],
                isHtml: !0,
                formatter: function(e) {
                    var t = this.model.get("grandparentTitle") || this.model.get("parentTitle") || e,
                        i = this.model.get("type");
                    return /^(season|episode|album|track)$/.test(i) && !this.model.get("isChannel") ? '<span class="btn-metadata-link media-title-link">' + t + "</span>" : t
                }
            },
            ".media-subtitle-1": {
                modelAttribute: "year",
                modelListeners: ["title", "parentTitle", "grandparentTitle", "year"],
                autohide: !0,
                formatter: function(e) {
                    var t;
                    if ("userPlaylistItem" === this.model.get("type")) this.model.get("recommender") && (t = a("from {1}", this.model.get("recommender")));
                    else {
                        var i = this.model.get("grandparentTitle") || this.model.get("parentTitle");
                        t = i ? this.model.get("title") : e
                    }
                    return this.$el.toggleClass("show-subtitle", !! t), t
                }
            },
            ".media-subtitle-2": {
                modelAttribute: "index",
                modelListeners: ["index", "message"],
                autohide: !0,
                formatter: function(e) {
                    var t = this.model.get("type");
                    return "userPlaylistItem" === t ? this.model.get("message") : "episode" === t ? o.getSeasonEpisodeIndex({
                        seasonIndex: this.model.get("parentIndex"),
                        episodePrefix: " \xb7 E{1}",
                        episodeIndex: e,
                        date: this.model.get("originallyAvailableAt"),
                        noLeadingZero: !0
                    }) : void 0
                }
            },
            ".media-context": {
                modelAttribute: "sourceTitle",
                manual: !0,
                formatter: function(e, t) {
                    var i = !! this.model.get("librarySectionID"),
                        n = i ? this.model.server.get("friendlyName") : e;
                    t.toggleClass("multiserver-only", i), t.text(n)
                }
            },
            ".media-added-at": {
                modelAttribute: "addedAt",
                autohide: !0,
                formatter: function(e) {
                    var t = Math.min(e, n().unix());
                    return n.unix(t).fromNow()
                }
            },
            ".media-available-at": {
                modelAttribute: "originallyAvailableAt",
                autohide: !0,
                formatter: function(e) {
                    return l.formatDate(e)
                }
            },
            ".media-resolution": {
                modelAttribute: "Media",
                autohide: !0,
                formatter: function(e) {
                    return e && e.length ? c.formatResolution(e[0].videoResolution) : ""
                }
            },
            ".media-duration": {
                modelAttribute: "duration",
                autohide: !0,
                formatter: function(e) {
                    return l.formatHoursMinutes(e)
                }
            }
        },
        
        onLightLevelChange: function(e) {
            if (e > 0) 
                this.$el.addClass('on');
            else
                this.$el.removeClass('on');
                
            this.image.$el.css("opacity", "1");
        },
        
        onRender: function() {
            if (this.image) 
                this.image.setElement(this.ui.poster); 
            
            if ("rating" === this.sort || this.showRating) 
                this.rating.show(new RatingView({
                    model: this.model,
                    disabled: true
                }));
                 
            if (this.progress) 
                this.progress.show(new MediaProgressView({
                    model: this.model
                }));
        },
        
        onClose: function() {
            if (this.image) 
                this.image.close(); 
            
            if (this.dropdown) 
                this.dropdown.close(); 
                
            dispatcher.trigger("content.unsubscribe:resize", this.onResize)
        },
        
        onResize: function(size) {
            this.containerSize = size; 
            
            if (this.mediaActions) 
                this.mediaActions.stopListening();
        },
        
        onActionsChange: function() {
            if (this.isRendered) {
                var hideActions = !this.showActions();
                this.ui.container.toggleClass("hide-actions", hideActions); 
                this.ui.actions.toggleClass("hidden", hideActions);
            }
        },
        
        onClick: function(e) {
            this.stopTrackingSelecting(e, function() {
                if(this.model.get("type") == "light") {      // TODO: Create new model types for each section, then call their "Click" action
                    e.preventDefault(); 

                    commands.execute("onOff", {
                        item: this
                    });
                }
            });
        },
        
        onPlayClick: function(e) {
            e.preventDefault(); 
            e.stopImmediatePropagation(); 
            commands.execute("onOff", {
                item: this
            });
        },
        
        onPlayUnwatchedClick: function(e) {
            e.preventDefault(); 
            e.stopImmediatePropagation(); 
            commands.execute("onOff", {
                item: this
            });
        },
        
        onEditClick: function(e) {
            e.preventDefault(), 
            e.stopImmediatePropagation();
            var target = $(e.currentTarget);
            var element = target.hasClass("btn-loading") ? target : this.$el;
            
            if (!element.hasClass("active")) {
                element.addClass("active");
                var promise = this.model.fetch();
                
                promise.done(_.bind(function() {
                    dispatcher.trigger("show:modal", new EditMetadataModal({
                        mediaActions: this.mediaActions
                    }));
                }, this)); 
                
                promise.always(_.bind(function() {
                    element.removeClass("active")
                }, this));
            }
        },
        
        onMoreClick: function(e) {
            e.preventDefault(); 
            e.stopImmediatePropagation(); 
            
            $(".media-actions-dropdown").trigger("hide.bs.dropdown"); 
            this.dropdown = new MediaActionsDropdown({
                model: this.model,
                mediaActions: this.mediaActions
            }); 
            
            this.ui.dropdown.append(this.dropdown.render().el); 
            this.ui.dropdown.addClass("open"); 
            this.$el.addClass("force-hover on-top");
            
            var target = $(e.currentTarget);
            var actionsTop = this.ui.actions.position().top;
            var ddPosition = this.dropdown.getPosition(target, this.containerSize, actionsTop);
            
            this.ui.dropdown.css(ddPosition);
        },
        
        onTitleClick: function(e) {
            e.preventDefault(); 
            e.stopImmediatePropagation(); 
            dispatcher.trigger("navigate", "details", {
                serverID: this.model.server.id,
                key: this.model.get("grandparentKey") || this.model.get("parentKey")
            });
        },
        
        onDropdownHide: function() {
            if (this.dropdown) {
                this.dropdown.close(); 
                this.dropdown = null; 
                this.$el.removeClass("force-hover on-top");
            }
        }
    });
    
    return mediaListItem;
});