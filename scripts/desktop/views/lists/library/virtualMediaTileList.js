define([
	"desktop/views/lists/virtualList", 
	"desktop/views/lists/library/mediaTileListItem"
], function(virtualList, mediaTileListItem) {
    "use strict";
    
	var virtualMediaTileList = virtualList.extend({
        tagName: "ul",
        className: "tile-list virtual-list media-tile-list",
        itemView: mediaTileListItem
    });
	
    return virtualMediaTileList;
});
