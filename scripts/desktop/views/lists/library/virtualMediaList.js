define([
	"desktop/views/lists/virtualList", 
	"desktop/views/lists/library/mediaListItem"
], function(virtualList, mediaListItem) {
    "use strict";
	
    var virtualMediaList = virtualList.extend({
        tagName: "ul",
        className: "list virtual-list media-list",
        itemView: mediaListItem
    });
	
    return virtualMediaList;
});