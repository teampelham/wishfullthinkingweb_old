define([
    "desktop/views/lists/horizontalTileList", 
    "desktop/views/lists/library/mediaTileListItem"
], function(horizontalTileList, mediaTileListItem) {
    "use strict";
    
    var horizontalMediaTileList = horizontalTileList.extend({
        tagName: "ul",
        className: "tile-list media-tile-list horizontal-media-tile-list hidden",
        itemView: mediaTileListItem,
        
        initialize: function(t) {
            horizontalTileList.prototype.initialize.apply(this, arguments); 
            this.itemViewOptions.continuousPlay = mediaTileListItem.continuousPlay === true;
        }
    });
    
    return horizontalMediaTileList;
});