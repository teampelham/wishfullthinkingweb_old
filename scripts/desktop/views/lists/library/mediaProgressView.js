define([
	"templates/desktop/lists/library/mediaProgressView.tpl", 
	"marionette"
], function(template, marionette) {
    "use strict";
    var mediaProgressView = marionette.ItemView.extend({
        className: "hidden",
        template: template,
        bindings: {
            ".media-progress-pct": {
                modelAttribute: "viewOffset",
                manual: true,
                formatter: function(progress, element) {
                    var track = !! progress && "track" !== this.model.get("type");
                    
					if (track) {
                        var complete = Math.round(progress / this.model.get("duration") * 100);
                        element.css("width", complete + "%")
                    }
					
                    this.$el.toggleClass("hidden", !track);
                }
            }
        }
    });
	
    return mediaProgressView;
});