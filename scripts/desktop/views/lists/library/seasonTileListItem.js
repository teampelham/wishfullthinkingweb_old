define([
    "templates/desktop/lists/library/seasonTileListItem.tpl", 
    "underscore", 
    "base/utils/t", 
    "desktop/views/lists/library/mediaListItem", 
    "desktop/views/lists/library/mediaTileListItem", 
    "base/views/images/mediaPosterImage"
], function(e, t, i, n, s, a) {
    "use strict";

    var r = n.extend({
        className: "poster-item media-tile-list-item",
        template: e,
        ui: s.prototype.ui,
        initialize: function() {
            n.prototype.initialize.apply(this, arguments), this.image = new a({
                model: this.model,
                type: "season",
                width: 150
            })
        },
        bindings: t.extend({}, n.prototype.bindings, {
            ".media-title": {
                modelAttribute: "title"
            },
            ".media-subtitle-1": {
                modelAttribute: "leafCount",
                formatter: function(e) {
                    return null == e ? "" : 1 === e ? i("{1} episode", e) : i("{1} episodes", e)
                }
            },
            ".unwatched-count-badge": {
                modelAttribute: "unviewedLeafCount",
                autohide: function(e) {
                    var t = this.model.server.get("shared"),
                        i = this.model.server.get("multiuser");
                    return t && !i ? !1 : "album" === this.type ? !1 : !! e
                }
            }
        }),
        onMoreClick: function() {
            s.prototype.onMoreClick.apply(this, arguments)
        }
    });
    
    return r;
});