define([
	"desktop/views/lists/baseList", 
	"desktop/views/lists/library/section/alphaListItem"
], function(e, t) {
    "use strict";
    var i = e.extend({
        tagName: "ul",
        className: "list alpha-bar-list",
        itemView: t
    });
    return i
});