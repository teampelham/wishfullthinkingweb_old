define([
	"templates/desktop/lists/library/section/alphaListItem.tpl", 
	"marionette"
], function(e, t) {
    "use strict";
    var i = t.ItemView.extend({
        tagName: "li",
        className: "alpha-bar-list-item",
        template: e,
        events: {
            "click a": "onClick"
        },
        onClick: function(e) {
            e.preventDefault(), this.model.section.set("currentFirstCharacter", this.model)
        }
    });
    return i
})