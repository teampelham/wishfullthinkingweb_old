define([
	"desktop/views/lists/baseList", 
	"desktop/views/lists/library/section/filterListItem"
], function(baseList, filterListItem) {
    "use strict";
	
    var filterList = baseList.extend({
        tagName: "ul",
        className: "list side-bar-list",
        itemView: filterListItem,
		
        initialize: function(initObj) {
            var section = this.collection.section;
            if (initObj.disabled) 
				this.$el.addClass("disabled") 
			else
				this.listenTo(section, "change:currentType", this.onCurrentTypeChange);
        },
		
        onCurrentTypeChange: function() {
            this.collection.fetch({
                reset: true,
                scope: "page"
            });
        }
    });
	
    return filterList;
});