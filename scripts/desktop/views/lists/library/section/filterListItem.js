define([
	"templates/desktop/lists/library/section/filterListItem.tpl", 
	"underscore", 
	"marionette"
], function(template, _, marionette) {
    "use strict";
	
    var filterListItem = marionette.ItemView.extend({
        tagName: "li",
        template: template,
        ui: {
            btn: "a",
            selectedIcon: ".selected-icon"
        },
		
        events: {
            "click .boolean-btn": "onBooleanFilterClick",
            "click .filter-btn": "onFilterClick"
        },
		
        onlyAfterRender: ["onCurrentFiltersChange"],
        
		initialize: function() {
            this.listenTo(this.model.section, "change:currentFilters", this.onCurrentFiltersChange);
        },
        
		serializeData: function() {
            return {
                filter: this.model.toJSON(),
                selected: this.isCurrentFilter()
            };
        },
        
		isCurrentFilter: function() {
            var currentFilters = this.model.section.get("currentFilters");
            return !!_.findWhere(currentFilters, {
                key: this.model.id
            });
        },
        
		onBooleanFilterClick: function(e) {
            e.preventDefault();
			
            var filterObj = {
                key: this.model.id,
                title: this.model.get("title"),
                value: 1
            };
            
			if (this.ui.btn.hasClass("selected")) 
				this.trigger("removeFilter", filterObj);
			else
				this.trigger("addFilter", filterObj);
        },
        
		onFilterClick: function(e) {
            e.preventDefault(); 
			this.trigger("showOptions", this.model);
        },
        
		onCurrentFiltersChange: function() {
            var isCurrentFilter = this.isCurrentFilter();
            this.ui.btn.toggleClass("selected", isCurrentFilter); 
			this.ui.selectedIcon.toggleClass("hidden", !isCurrentFilter);
        }
    });
	
    return filterListItem;
});