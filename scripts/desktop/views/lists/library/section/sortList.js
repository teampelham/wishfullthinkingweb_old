define([
	"desktop/views/lists/baseList", 
	"desktop/views/lists/library/section/sortListItem"
], function(baseList, sortListItem) {
    "use strict";
	
    var sortList = baseList.extend({
        tagName: "ul",
        className: "list side-bar-list",
        itemView: sortListItem,
        
		initialize: function(initObj) {
            var section = this.collection.section;
            if (initObj.disabled) 
				this.$el.addClass("disabled"); 
			else if (section) {
				this.listenTo(section, "change:currentType", this.onCurrentTypeChange); 
				if (!section.get("pseudo")) 
					this.collection.populate({
                		reset: true,
                		scope: "page"
            		});
			}
        },
        
		onCurrentTypeChange: function(currentType) {
            this.collection.section.set("currentSort", null, {
                silent: true
            }); 
			
			if (!currentType.get("pseudo")) 
				this.collection.fetch({
                	reset: true,
                	scope: "page"
            	});
        }
    });
    return sortList;
});