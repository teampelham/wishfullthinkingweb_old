define([
	"templates/desktop/lists/library/section/currentFilterList.tpl", 
	"jquery", 
	"marionette"
], function(template, $, marionette) {
    "use strict";
	
    var currentFilterList = marionette.ItemView.extend({
        className: "label-list section-current-filters-list",
        template: template,
        modelEvents: {
            "change:currentFilters": "render"
        },
		
        events: {
            "click .remove-filter-btn": "onRemoveFilterClick"
        },
		
        initialize: function(initObj) {
            if (initObj.disabled) 
				this.$el.addClass("disabled");
        },
		
        serializeData: function() {
            return {
                currentFilters: this.model.get("currentFilters")
            };
        },
		
        onRemoveFilterClick: function(e) {
            e.preventDefault();
            
			var key = $(e.currentTarget).data("key");
            this.trigger("removeAllFilters", key);
        }
    });
	
    return currentFilterList;
});