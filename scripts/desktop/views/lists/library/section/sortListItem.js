define([
	"templates/desktop/lists/library/section/sortListItem.tpl", 
	"marionette"
], function(template, marionette) {
    "use strict";
	
    var sortListItem = marionette.ItemView.extend({
        tagName: "li",
        template: template,
        events: {
            "click a": "onClick"
        },
		
        initialize: function() {
            var currentSort = this.model.section.get("currentSort");
            if (!currentSort) {
                var defaultDirection = this.model.get("defaultDirection");
                if (defaultDirection) {
					this.model.set({
                    	selected: true,
                    	direction: defaultDirection
                	}); 
					this.model.section.set("currentSort", this.model, {
                    	silent: true
                	});
				}
            }
            this.listenTo(this.model.section, "change:currentSort", this.onCurrentSortChange);
        },
		
        bindings: {
            ".sort-btn": {
                modelAttribute: "selected",
                manual: !0,
                formatter: function(e, t) {
                    t.toggleClass("selected", e)
                }
            },
            ".desc-icon": {
                modelAttribute: "direction",
                manual: !0,
                formatter: function(e, t) {
                    t.toggleClass("hidden", "desc" !== e)
                }
            },
            ".asc-icon": {
                modelAttribute: "direction",
                manual: !0,
                formatter: function(e, t) {
                    t.toggleClass("hidden", "asc" !== e)
                }
            }
        },
		
        onCurrentSortChange: function(e, t) {
			console.warn('');
            /*t && this.model.id !== t.id && this.model.set({
                selected: !1
            })*/
        },
		
        onClick: function(e) {
            e.preventDefault();
			console.warn('');
            /*var t = this.model.section.get("currentSort");
            t && t.id === this.model.id ? t.reverseSortDirection() : this.model.set({
                selected: !0
            }), this.model.section.set("currentSort", this.model, {
                silent: !0
            }), this.model.section.trigger("change:currentSort", this.model.section, this.model)*/
        }
    });
	
    return sortListItem;
})