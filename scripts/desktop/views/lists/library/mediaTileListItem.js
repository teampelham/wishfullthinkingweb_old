define([
	"templates/desktop/lists/library/mediaTileListItem.tpl", 
	"underscore", 
	"jquery", 
	"desktop/views/lists/library/mediaListItem", 
	"base/views/images/mediaPosterImage"
], function(template, _, $, mediaListItem, MediaPosterImage) {
    "use strict";
	
    var metadataTileListItem = mediaListItem.extend({
        className: "poster-item media-tile-list-item",
        template: template,
        
		ui: {
            container: ".media-poster-container",
            poster: ".media-poster",
            actions: ".media-poster-actions",
            dropdown: ".media-actions-dropdown",
            subtitle1Fill: ".media-subtitle-1-fill",
            subtitle2Fill: ".media-subtitle-2-fill"
        },
		
        initialize: function() {
            mediaListItem.prototype.initialize.apply(this, arguments); 
			if (this.model) 
				this.createImageView();           
        },
		
        createImageView: function() {
            var metaType = this.type;
            var width = 150;
            
			if (metaType === "episode") 
				metaType = "show";
			else if ("clip" === metaType || "userPlaylistItem" === metaType) 
				width = 250; 
			
			this.image = new MediaPosterImage({
                model: this.model,
                type: metaType,
                getUrl: this.getPosterUrl,
                width: width
            });
        },
		
        updateDim: function() {
            var mediaState = this.model.get("mediaState");
            var metaState = this.model.get("metadataState");
            var notCreated = metaState && "created" !== metaState || mediaState;
            var notDeleted = !! this.model.get("deletedAt");
            
            if (notCreated || notDeleted) 
                this.$el.addClass("dim"); 
            else
                this.$el.removeClass("dim")
        },
        
        /************** MY CODE  ***************/
        
        setTransitionaryState: function() {
            this.image.$el.css("opacity", "0.1");
        },
        
        /************** MY CODE  ***************/
		
        bindings: _.extend({}, mediaListItem.prototype.bindings, {
            ".media-subtitle-1": {
                modelAttribute: "year",
                modelListeners: ["title", "year"],
                autohide: !0,
                formatter: function() {
                    var e = mediaListItem.prototype.bindings[".media-subtitle-1"].formatter.apply(this, arguments);
                    return "userPlaylistItem" !== this.model.get("type") && this.ui.subtitle1Fill.toggleClass("hidden", !! e), e
                }
            },
            ".media-subtitle-2": {
                modelAttribute: "index",
                modelListeners: ["index", "message"],
                autohide: !0,
                formatter: function() {
                    var e = mediaListItem.prototype.bindings[".media-subtitle-2"].formatter.apply(this, arguments);
                    return "episode" === this.model.get("type") && this.ui.subtitle2Fill.toggleClass("hidden", !! e), e
                }
            },
            ".media-count-badge": {
                modelAttribute: "Media",
                formatter: function(media) {
                    return media ? media.length : "0";
                },
                autohide: function(e) {
                    return /^(clip|userPlaylistItem)$/.test(this.model.get("type")) ? !1 : e > 1
                }
            },
            ".unwatched-count-badge": {
                modelAttribute: "unviewedLeafCount",
                autohide: function(e) {
                    return this.shared && !this.multiuser ? !1 : /^(artist|album)$/.test(this.model.get("type")) ? !1 : !! e
                }
            },
            ".analyzing-icon": {
                modelAttribute: "mediaState",
                manual: !0,
                autohide: !0,
                formatter: function(e) {
                    return this.updateDim(), e
                }
            },
            ".matching-icon": {
                modelAttribute: "metadataState",
                manual: !0,
                autohide: !0,
                formatter: function(e) {
                    return this.updateDim(), "matching" === e
                }
            },
            ".metadata-icon": {
                modelAttribute: "metadataState",
                manual: !0,
                autohide: !0,
                formatter: function(e) {
                    return this.updateDim(), "processing" === e || "loading" === e
                }
            },
            ".trash-icon": {
                modelAttribute: "deletedAt",
                manual: !0,
                autohide: !0,
                formatter: function(e) {
                    return this.updateDim(), e
                }
            }
        })
    });
	
    return metadataTileListItem;
})