define([
	"templates/desktop/actionbar/actionBarView.tpl", 
	"underscore", 
	"jquery", 
	"marionette", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/utils/types/mediaTypes", 
	"base/utils/formatters/stringUtil", 
	"base/commands/commands", 
	"desktop/models/mediaActionsModel", 
	//"desktop/views/modals/addToPlaylistModal", 
	"desktop/views/modals/editMetadataModal", 
	//"desktop/views/modals/fixIncorrectMatchModal", 
	//"desktop/views/modals/shareModal", 
	//"desktop/views/modals/recommendModal", 
	"desktop/views/modals/mediaInfoModal"
], function(e, t, i, n, s, a, r, o, l, c, d, u, h, p, m, f) {
    "use strict";
	
    var g = ["episode", "movie", "clip", "userPlaylistItem"],
        v = {
            preventDefault: !0,
            stopPropagation: !1
        }, y = n.ItemView.extend({
            className: "action-bar",
            template: e,
            ui: {
                primaryActions: ".action-bar-nav",
                secondaryActions: ".dropdown-menu",
                playBtn: ".play-btn",
                playAllBtn: ".play-all-btn",
                playUnwatchedBtn: ".play-unwatched-btn",
                playShuffledBtn: ".play-shuffled-btn",
                playTrailerBtn: ".play-trailer-btn",
                playNextBtn: ".play-next-btn",
                addToUpNextBtn: ".add-to-up-next-btn",
                addToPlaylistBtn: ".add-to-playlist-btn",
                secondaryDivider: ".secondary-divider",
                editBtn: ".edit-btn",
                refreshBtn: ".refresh-btn",
                analyzeBtn: ".analyze-btn",
                markWatchedBtn: ".mark-watched-btn",
                markUnwatchedBtn: ".mark-unwatched-btn",
                syncBtn: ".sync-btn",
                watchLaterBtn: ".watch-later-btn",
                shareBtn: ".share-btn",
                recommendBtn: ".recommend-btn",
                deleteBtn: ".delete-btn",
                deleteFromUserPlaylistBtn: ".delete-from-user-playlist-btn",
                infoBtn: ".info-btn"
            },
            triggers: {
                "click .play-btn": "play",
                "click .play-all-btn": t.extend({
                    event: "play"
                }, v),
                "click .play-unwatched-btn": "playUnwatched",
                "click .play-shuffled-btn": "playShuffled",
                "click .play-trailer-btn": "playTrailer",
                "click .edit-btn": "edit",
                "click .refresh-btn": "refresh",
                "click .analyze-btn": t.extend({
                    event: "analyze"
                }, v),
                "click .mark-watched-btn": "markWatched",
                "click .mark-unwatched-btn": "markUnwatched",
                "click .match-btn": t.extend({
                    event: "fixMatch"
                }, v),
                "click .select-btn": "select",
                "click .deselect-btn": "deselect",
                "click .fix-match-btn": t.extend({
                    event: "fixMatch"
                }, v),
                "click .unmatch-btn": t.extend({
                    event: "unmatch"
                }, v),
                "click .split-apart-btn": t.extend({
                    event: "splitApart"
                }, v),
                "click .sync-btn": "sync",
                "click .watch-later-btn": t.extend({
                    event: "watchLater"
                }, v),
                "click .share-btn": t.extend({
                    event: "share"
                }, v),
                "click .recommend-btn": t.extend({
                    event: "recommend"
                }, v),
                "click .delete-btn": t.extend({
                    event: "delete"
                }, v),
                "click .delete-from-user-playlist-btn": t.extend({
                    event: "deleteFromUserPlaylist"
                }, v),
                "click .info-btn": t.extend({
                    event: "info"
                }, v),
                "click .play-next-btn": t.extend({
                    event: "playNext"
                }, v),
                "click .add-to-up-next-btn": t.extend({
                    event: "addToUpNext"
                }, v),
                "click .add-to-playlist-btn": "addToPlaylist"
            },
            onlyAfterRender: ["onPlay", "onPlayUnwatched", "onPlayShuffled", "onPlayTrailer", "onEdit", "onRefresh", "onAnalyze", "onMarkWatched", "onMarkUnwatched", "onSync", "onWatchLater", "onShare", "onRecommend", "onDelete", "onDeleteFromUserPlaylist", "onInfo", "onPlayNext", "onAddToUpNext", "onAddToPlaylist", "updateDropdown"],
            initialize: function(e) {
                this.type = this.model.get("type"), this.mediaActions = e.mediaActions, this.listenTo(this.mediaActions, "change", t.throttle(this.updateDropdown, 50)), this.listenTo(this.mediaActions, "change:selectedItems", this.onSelectedItemsChange), this.listenTo(a, {
                    "shortcut:play": this.onPlay,
                    "shortcut:edit": this.onEdit,
                    "shortcut:refresh": this.onRefresh,
                    "shortcut:analyze": this.onAnalyze,
                    "shortcut:markWatched": this.onMarkWatched,
                    "shortcut:markUnwatched": this.onMarkUnwatched,
                    "shortcut:sync": this.onSync,
                    "shortcut:delete": this.onDelete,
                    "shortcut:info": this.onInfo
                }), this.listenTo(a, "shortcut:play", this.onPlayUnwatched), this.listenTo(a, "shortcut:delete", this.onDeleteFromUserPlaylist)
            },
            serializeData: function() {
                var e = t.extend({}, this.mediaActions.attributes);
                if (e.isSingleItem = t.contains(g, this.type), this.mediaActions.get("showDownload")) {
                    e.downloadUrl = this.model.getDownloadUrl();
                    var i = this.model.getFilepath();
                    i && (e.downloadName = o.getLastSegment(i))
                }
                return e
            },
            updateDropdown: function() {
                var e = this.ui.secondaryActions.find("a:not(.hidden)"),
                    t = e.hasClass("secondary-only"),
                    i = e.length > 2 || t;
                if (this.$el.toggleClass("show-secondary-actions-dropdown", i), i) {
                    var n = this.ui.secondaryDivider,
                        s = n.prevAll(":not(:has(.hidden))").length,
                        a = n.nextAll(":not(:has(.hidden))").length;
                    n.toggleClass("hidden", !s || !a)
                }
            },
            bindingsModel: "mediaActions",
            bindings: {
                ".play-btn": {
                    modelAttribute: "showPlay",
                    manual: !0,
                    autohide: !0
                },
                ".play-all-btn": {
                    modelAttribute: "showPlayAll",
                    manual: !0,
                    autohide: !0
                },
                ".play-unwatched-btn": {
                    modelAttribute: "showPlayUnwatched",
                    manual: !0,
                    autohide: !0
                },
                ".play-shuffled-btn": {
                    modelAttribute: "showShuffle",
                    manual: !0,
                    autohide: !0
                },
                ".play-trailer-btn": {
                    modelAttribute: "showPlayTrailer",
                    manual: !0,
                    autohide: !0
                },
                ".add-to-playlist-btn": {
                    modelAttribute: "showAddToPlaylist",
                    manual: !0,
                    autohide: !0
                },
                ".edit-btn": {
                    modelAttribute: "showEdit",
                    manual: !0,
                    autohide: !0
                },
                ".refresh-btn": {
                    modelAttribute: "showRefresh",
                    manual: !0,
                    autohide: !0
                },
                ".analyze-btn": {
                    modelAttribute: "showAnalyze",
                    manual: !0,
                    autohide: !0
                },
                ".mark-watched-btn": {
                    modelAttribute: "showMarkWatched",
                    manual: !0,
                    autohide: !0
                },
                ".mark-unwatched-btn": {
                    modelAttribute: "showMarkUnwatched",
                    manual: !0,
                    autohide: !0
                },
                ".match-btn": {
                    modelAttribute: "showMatch",
                    manual: !0,
                    autohide: !0
                },
                ".select-btn": {
                    modelAttribute: "selecting",
                    manual: !0,
                    autohide: function(e) {
                        return !e && this.mediaActions.get("showSelect")
                    }
                },
                ".deselect-btn": {
                    modelAttribute: "selecting",
                    manual: !0,
                    autohide: !0
                },
                ".fix-match-btn": {
                    modelAttribute: "showFixMatch",
                    manual: !0,
                    autohide: !0
                },
                ".unmatch-btn": {
                    modelAttribute: "showUnmatch",
                    manual: !0,
                    autohide: !0
                },
                ".split-apart-btn": {
                    modelAttribute: "showSplitApart",
                    manual: !0,
                    autohide: !0
                },
                ".sync-btn": {
                    modelAttribute: "showSync",
                    manual: !0,
                    autohide: !0
                },
                ".download-btn": {
                    modelAttribute: "showDownload",
                    manual: !0,
                    autohide: !0
                },
                ".watch-later-btn": {
                    modelAttribute: "showWatchLater",
                    manual: !0,
                    autohide: !0
                },
                ".share-btn, .recommend-btn": {
                    modelAttribute: "showShare",
                    manual: !0,
                    autohide: !0
                },
                ".delete-btn": {
                    modelAttribute: "showDelete",
                    manual: !0,
                    autohide: !0
                },
                ".delete-from-user-playlist-btn": {
                    modelAttribute: "showDeleteFromUserPlaylist",
                    manual: !0,
                    autohide: !0
                },
                ".info-btn": {
                    modelAttribute: "showInfo",
                    manual: !0,
                    autohide: !0
                }
            },
            onRender: function() {
                this.$el.tooltip({
                    selector: ".nav > li > a",
                    container: "body",
                    placement: "right"
                }), this.updateDropdown()
            },
            onPlay: function() {
                this.ui.playBtn.isDisabled() && this.ui.playAllBtn.isDisabled() || l.execute("playMedia", {
                    items: this.mediaActions.getActionTargets()
                })
            },
            onPlayUnwatched: function() {
                this.ui.playUnwatchedBtn.isDisabled() || l.execute("playMedia", {
                    items: this.mediaActions.getActionTargets(),
                    unwatched: !0
                })
            },
            onPlayShuffled: function() {
                this.ui.playShuffledBtn.isDisabled() || l.execute("playMedia", {
                    items: this.mediaActions.getActionTargets(),
                    shuffle: !0
                })
            },
            onPlayTrailer: function() {
                if (!this.ui.playTrailerBtn.isDisabled()) {
                    var e = this.model.get("extras"),
                        t = e.findWhere({
                            key: this.model.get("primaryExtraKey")
                        });
                    l.execute("playMedia", {
                        items: [t]
                    })
                }
            },
            onEdit: function() {
                this.ui.editBtn.isDisabled() || a.trigger("show:modal", new u({
                    mediaActions: this.mediaActions
                }))
            },
            onRefresh: function() {
                this.ui.refreshBtn.isDisabled() || l.execute("refreshMetadata", {
                    items: this.mediaActions.getActionTargets()
                })
            },
            onAnalyze: function() {
                this.ui.analyzeBtn.isDisabled() || l.execute("analyzeItems", {
                    items: this.mediaActions.getActionTargets()
                })
            },
            onMarkWatched: function() {
                this.ui.markWatchedBtn.isDisabled() || l.execute("markWatched", {
                    items: this.mediaActions.getActionTargets()
                })
            },
            onMarkUnwatched: function() {
                this.ui.markUnwatchedBtn.isDisabled() || l.execute("markUnwatched", {
                    items: this.mediaActions.getActionTargets()
                })
            },
            onSelect: function() {
                this.mediaActions.set("selecting", !0)
            },
            onDeselect: function() {
                this.mediaActions.set("selecting", !1)
            },
            onFixMatch: function() {
                a.trigger("show:modal", new h({
                    model: this.model
                }))
            },
            onUnmatch: function() {
                l.execute("unmatch", {
                    item: this.model
                })
            },
            onSplitApart: function() {
                l.execute("splitApart", {
                    item: this.model
                })
            },
            onSync: function() {
                if (!this.ui.syncBtn.isDisabled()) {
                    var e = r.getMediaTypeByString(this.model.get("type")),
                        t = this.mediaActions.getActionTargets(),
                        i = t.length > 1,
                        n = {
                            model: this.model,
                            type: e,
                            isNew: !0,
                            isSection: i,
                            section: this.model.server.getSectionForItem(this.model),
                            selectedItems: t
                        };
                    l.execute("showSyncModal", n)
                }
            },
            onWatchLater: function() {
                this.ui.watchLaterBtn.isDisabled() || l.execute("watchLater", {
                    items: this.mediaActions.getActionTargets()
                })
            },
            onShare: function() {
                /*this.ui.shareBtn.isDisabled() || a.trigger("show:modal", new p({
                    model: this.model
                }))*/
            },
            onRecommend: function() {
				console.warn('recommend');
                /*this.ui.recommendBtn.isDisabled() || a.trigger("show:modal", new m({
                    model: this.model
                }))*/
            },
            onDelete: function() {
                this.ui.deleteBtn.isDisabled() || (this.model.get("playlistType") ? l.execute("deletePlaylist", {
                    item: this.model
                }) : l.execute("deleteItems", {
                    items: this.mediaActions.getActionTargets()
                }))
            },
            onDeleteFromUserPlaylist: function() {
                if (!this.ui.deleteFromUserPlaylistBtn.isDisabled()) {
                    var e = this.model.destroy();
                    e.done(t.bind(function() {
                        i(".tooltip").remove(), a.trigger("navigate", "userPlaylist", {
                            key: this.model.get("playlist")
                        })
                    }, this))
                }
            },
            onInfo: function() {
                /*this.ui.infoBtn.isDisabled() || a.trigger("show:modal", new f({
                    model: this.model
                }))*/
            },
            onPlayNext: function() {
                this.ui.playNextBtn.isDisabled() || l.execute("queueMedia", {
                    item: this.model,
                    next: !0
                })
            },
            onAddToUpNext: function() {
                this.ui.addToUpNextBtn.isDisabled() || l.execute("queueMedia", {
                    item: this.model,
                    next: !1
                })
            },
            onAddToPlaylist: function() {
                /*this.ui.addToPlaylistBtn.isDisabled() || a.trigger("show:modal", new d({
                    model: this.model,
                    items: this.mediaActions.getActionTargets(),
                    title: this.mediaActions.getActionTargetsTitle()
                }))*/
            },
            onSelectedItemsChange: function(e, t) {
                var i = s(t && t.length ? "Play selected" : "Play All");
                this.ui.playBtn.attr("data-original-title", i)
            }
        });
    return y
});