define([
	"templates/desktop/actionbar/sectionActionBarView.tpl", 
	"underscore", 
	"jquery", 
	"marionette", 
	"base/utils/t", 
	"base/utils/dispatcher", 
	"base/utils/types/mediaTypes", 
	"base/commands/commands", 
	"base/models/appModel", 
	"desktop/views/sidebar/sectionSideBarView", 
	//"desktop/views/modals/AddToCollectionModal", 
	//"desktop/views/modals/AddToPlaylistModal", 
	//"desktop/views/modals/EditMetadataModal", 
	//"desktop/views/modals/EditSectionModal"
], function(template, _, $, marionette, t, dispatcher, mediaTypes, commands, appModel, SectionSideBarView) {
    "use strict";
    
	var sectionActionBarView = marionette.Layout.extend({
        className: "section-side-bar-container",
        template: template,
        ui: {
            playBtn: ".play-btn",
            playShuffledBtn: ".play-shuffled-btn",
            refreshBtn: ".refresh-btn",
            analyzeBtn: ".analyze-btn",
            markWatchedBtn: ".mark-watched-btn",
            markUnwatchedBtn: ".mark-unwatched-btn",
            addToCollectionBtn: ".add-to-collection-btn",
            addToPlaylistBtn: ".add-to-playlist-btn",
            mergeBtn: ".merge-btn",
            syncBtn: ".sync-btn",
            deleteBtn: ".delete-btn"
        },
        regions: {
            sidebar: ".side-bar"
        },
        triggers: {
            "click .options-btn": "toggleSidebar",
            "click .play-btn": "play",
            "click .play-shuffled-btn": "playShuffled",
            "click .select-btn": "select",
            "click .deselect-btn": "deselect",
            "click .edit-btn": "edit",
            "click .refresh-btn": "refresh",
            "click .analyze-btn": "analyze",
            "click .mark-watched-btn": "markWatched",
            "click .mark-unwatched-btn": "markUnwatched",
            "click .add-to-collection-btn": "addToCollection",
            "click .add-to-playlist-btn": "addToPlaylist",
            "click .merge-btn": "merge",
            "click .sync-btn": "sync",
            "click .delete-btn": "delete"
        },
		
        initialize: function(initObj) {
            this.mediaActions = initObj.mediaActions; 
			this.type = this.model.get("type"); 
			this.multiuser = this.model.server.get("multiuser"); 
			this.signedIn = appModel.get("user").get("signedIn"); 
			this.listenTo(this.mediaActions, "change:selectedItems", this.onSelectedItemsChange); 
			this.listenTo(dispatcher, {
                "shortcut:sidebar": this.onToggleSidebar,
                "shortcut:play": this.onPlay,
                "shortcut:refresh": this.onRefresh,
                "shortcut:analyze": this.onAnalyze,
                "shortcut:markWatched": this.onMarkWatched,
                "shortcut:markUnwatched": this.onMarkUnwatched,
                "shortcut:sync": this.onSync,
                "shortcut:edit": this.onEdit,
                "shortcut:delete": this.onDelete
            });
        },
        
		bindingsModel: "mediaActions",
        
		bindings: {
            ".play-btn": {
                modelAttribute: "showPlay",
                manual: true,
                autohide: true
            },
            ".play-shuffled-btn": {
                modelAttribute: "showShuffle",
                manual: true,
                autohide: true
            },
            ".select-btn": {
                modelAttribute: "selecting",
                modelListeners: ["selecting", "showSelect"],
                manual: true,
                autohide: function(e) {
                    return this.mediaActions.get("showSelect") ? !e : false
                }
            },
            ".deselect-btn": {
                modelAttribute: "selecting",
                manual: true,
                autohide: true
            },
            ".edit-btn": {
                modelAttribute: "selectedItems",
                modelListeners: ["selectedItems", "showEdit"],
                manual: true,
                autohide: function(e) {
                    return this.mediaActions.get("showEdit") ? e && e.length ? true : this.model.get("currentFilters").length ? true : void 0 : false
                }
            },
            ".refresh-btn": {
                modelAttribute: "selectedItems",
                modelListeners: ["selectedItems", "showRefresh"],
                manual: true,
                autohide: function(e) {
                    return this.mediaActions.get("showRefresh") ? e && e.length && !this.hasSelectedFolder : false
                }
            },
            ".analyze-btn": {
                modelAttribute: "selectedItems",
                modelListeners: ["selectedItems", "showAnalyze"],
                manual: true,
                autohide: function(e) {
                    return this.mediaActions.get("showAnalyze") ? e && e.length && !this.hasSelectedFolder : false
                }
            },
            ".mark-watched-btn": {
                modelAttribute: "showMarkWatched",
                manual: true,
                autohide: true
            },
            ".mark-unwatched-btn": {
                modelAttribute: "showMarkUnwatched",
                manual: true,
                autohide: true
            },
            ".add-to-collection-btn": {
                modelAttribute: "selectedItems",
                modelListeners: ["selectedItems", "showAddToCollection"],
                manual: true,
                autohide: function(e) {
                    return this.mediaActions.get("showAddToCollection") ? e && e.length && !this.hasSelectedFolder : false
                }
            },
            ".add-to-playlist-btn": {
                modelAttribute: "showAddToPlaylist",
                manual: true,
                autohide: true
            },
            ".merge-btn": {
                modelAttribute: "selectedItems",
                modelListeners: ["selectedItems", "showMerge"],
                manual: true,
                autohide: function(item) {
                    if (!this.mediaActions.get("showMerge")) 
						return false;
                    var supportsMerge = this.model.server.supports("merge");
                    var hasItems = item && item.length > 1;
                    return supportsMerge && hasItems && !this.hasSelectedFolder;
                }
            },
            ".sync-btn": {
                modelAttribute: "showSync",
                manual: true,
                autohide: true
            },
            ".delete-btn": {
                modelAttribute: "selectedItems",
                modelListeners: ["selectedItems", "showDelete"],
                manual: true,
                autohide: function(e) {
                    return this.mediaActions.get("showDelete") ? e && e.length && !this.hasSelectedFolder : false
                }
            }
        },
        
        onRender: function() {
			var sectionSideBarView = new SectionSideBarView({
                model: this.model,
                mediaActions: this.mediaActions
            });
            
            this.sidebar.append(sectionSideBarView, this.$el); 
            
            this.$el.tooltip({
                selector: ".nav > li > a",
                container: "body",
                placement: "right"
            })
        },
		
        onClose: function() {
            this.mediaActions.stopListening(); 
			this.mediaActions.deselectAll();
        },
		
        onSelectedItemsChange: function(e, i) {
			console.warn('');
            /*if (i) {
                this.hasSelectedFolder = t.some(i, function(e) {
                    return e.get("isFolder")
                });
                var n = s(i && i.length ? "Play selected" : "Play All");
                this.ui.playBtn.attr("data-original-title", n)
            }*/
        },
		
        onToggleSidebar: function() {
			$(".tooltip").remove();
            var currentView = this.sidebar.currentView;
            if(currentView.hidden)
                currentView.open();
            else 
                currentView.hide();
        },
		
        onPlay: function() {
			console.warn('');
            /*this.ui.playBtn.isDisabled() || o.execute("playMedia", {
                items: this.mediaActions.getActionTargets()
            })*/
        },
		
        onPlayShuffled: function() {
			console.warn('');
            /*this.ui.playShuffledBtn.isDisabled() || o.execute("playMedia", {
                items: this.mediaActions.getActionTargets(),
                shuffle: true
            })*/
        },
		
        onSelect: function() {
            this.mediaActions.set("selecting", true);
        },
        
		onDeselect: function() {
            this.mediaActions.set("selecting", false);
        },
        
		onEdit: function() {
			console.warn('');
            /*if (!this.model.server.get("shared") && !this.model.server.get("synced")) {
                var e = this.mediaActions.get("selectedItems");
                if (e.length) {
                    if (e.length > 1 && !e[0].server.supports("batchEdit")) return;
                    a.trigger("show:modal", new h({
                        mediaActions: this.mediaActions
                    }))
                } else a.trigger("show:modal", new p({
                    server: this.model.server,
                    model: this.model
                }))
            }*/
        },
		
        onRefresh: function() {
			console.warn('');
            /*return this.ui.refreshBtn.isDisabled() ? void o.execute("updateLibrary", {
                section: this.model
            }) : void o.execute("refreshMetadata", {
                items: this.mediaActions.getActionTargets()
            })*/
        },
		
        onAnalyze: function() {
			console.warn('');
            /*return this.ui.analyzeBtn.isDisabled() ? void o.execute("analyzeSection", {
                section: this.model
            }) : void o.execute("analyzeItems", {
                items: this.mediaActions.getActionTargets()
            })*/
        },
		
        onMarkWatched: function() {
			console.warn('');
            /*this.ui.markWatchedBtn.isDisabled() || o.execute("markWatched", {
                items: this.mediaActions.getActionTargets()
            }).then(t.bind(function() {
                this.mediaActions.updateAll()
            }, this))*/
        },
		
        onMarkUnwatched: function() {
			console.warn('');
            /*this.ui.markUnwatchedBtn.isDisabled() || o.execute("markUnwatched", {
                items: this.mediaActions.getActionTargets()
            }).then(t.bind(function() {
                this.mediaActions.updateAll()
            }, this))*/
        },
		
        onAddToCollection: function() {
			console.warn('');
            /*this.ui.addToCollectionBtn.isDisabled() || a.trigger("show:modal", new d({
                items: this.mediaActions.getActionTargets()
            }))*/
        },
		
        onAddToPlaylist: function() {
			console.warn('');
            /*this.ui.addToPlaylistBtn.isDisabled() || a.trigger("show:modal", new u({
                model: this.model,
                items: this.mediaActions.getActionTargets(),
                title: this.mediaActions.getActionTargetsTitle()
            }))*/
        },
		
        onMerge: function() {
			console.warn('');
            /*if (!this.ui.mergeBtn.isDisabled()) {
                var e = this.mediaActions.getActionTargets(),
                    i = t.first(e),
                    n = e.slice(1);
                o.execute("merge", {
                    item: i,
                    items: n
                })
            }*/
        },
		
        onSync: function() {
			console.warn('');
            /*if (!this.ui.syncBtn.isDisabled()) {
                var e = this.mediaActions.get("selectedItems"),
                    t = r.getMediaTypeByString(this.model.get("type")),
                    i = {
                        model: this.model,
                        type: t,
                        isNew: true,
                        isSection: true,
                        isDirectory: true
                    };
                e.length && !this.hasSelectedFolder && (i.selectedItems = e), o.execute("showSyncModal", i)
            }*/
        },
		
        onDelete: function() {
			console.warn('');
            /*o.execute("deleteItems", {
                items: this.mediaActions.getActionTargets(),
                skipRedirect: true
            })*/
        }
    });
	
    return sectionActionBarView;
});