define([
	"templates/desktop/signInFormView.tpl", 
	"underscore", 
	"marionette", 
	"moment", 
	"base/adapters/metricsAdapter", 
	"base/models/appModel"
], function(template, _, marionette, moment, metricsAdapter, appModel) {
    "use strict";
	
    var signInFormView = marionette.ItemView.extend({
        className: "sign-in-form",
        template: template,
        validationFields: ["username", "email", "password", "confirm", "age", "terms"],
        
		ui: {
            submitBtn: "[type=submit]",
            signInBtn: ".sign-in-btn",
            signUpBtn: ".sign-up-btn",
            username: ".username-group",
            email: ".email-group",
            password: ".password-group",
            confirm: ".confirm-group",
            age: ".age-group",
            terms: ".terms-group"
        },
        
		events: {
            "click .sign-in-btn": "onSignInClick",
            "click .sign-up-btn": "onSignUpClick",
            "blur input": "onInputBlur",
            "submit form": "onSubmit"
        },
        
		initialize: function(initObj) {
            this.showRememberMe = initObj.showRememberMe; 
			metricsAdapter.trackEvent("view", "signin");
        },
        
		serializeData: function() {
            // called after initialize by marionette
            return {
                showRememberMe: this.showRememberMe
            };
        },
		
        validateAll: function(form, validationFields, values) {
			var isValid = true;
            
            if (!validationFields)
                validationFields = this.validationFields;
            _.each(validationFields, _.bind(function(validateField) {
                var field = form[validateField];
                if (!this.validate(field, values[validateField], values))
                    isValid = false;
            }, this));
            
            return isValid;
        },
		
        validate: function(target, value, form) {   // e, i, s
            var isValid;            // a 
            var id = target.id;     // r
            
            if (_.indexOf(this.validationFields, id) < 0) 
                return true;        // not a field to validate
            
            var sel = this.ui[id];    // jquery selector, o
            
            if ("confirm" === target.id) 
                isValid = value === target.form.password.value;
            else if ("age" === target.id) {
                var day = parseInt(target.form.day.value, 10);                // l    
                var month = parseInt(target.form.month.value, 10);       // c
                var year = parseInt(value, 10);                          // d
                var isDate = !! (day && month && year);                 // u
                var isValidDay = 32 > day;                              // h
                var isValidMonth = 13 > month;                    // p
                var isValidYear = year > 1800;                          // m
                    
                if (!isValidMonth && 32 > month && 13 > day) {      // backwardass Euros
                    var monthHolder = month;
                    month = day; 
                    day = monthHolder; 
                    isValidMonth = true;
                }
                
                if (isDate && isValidDay && isValidMonth && isValidYear) {
                    var bday = moment({                 // g
                        day: day,
                        month: month - 1,
                        year: year
                    });
                    var yearDiff = moment().diff(bday, "years");   // v 
                    
                    if (form) {
                        form.birthday = bday;
                    } 
                    
                    isValid = yearDiff > 12; 
                    sel.removeClass("is-invalid"); 
                    sel.addClass("is-underage");
                } 
                else {
                    isValid = false;
                    sel.addClass("is-invalid"); 
                    sel.removeClass("is-underage");
                }
            } 
            else 
                isValid = !! value;
                
            sel.toggleClass("has-error", !isValid); 
            return isValid;
        },
		
        onSignInClick: function(e) {
            e.preventDefault(); 
            this.ui.signInBtn.addClass("selected"); 
            this.ui.signUpBtn.removeClass("selected"); 
            this.$el.addClass("sign-in-form"); 
            this.$el.removeClass("sign-up-form");
            metricsAdapter.trackEvent("view", "signin");
        },
		
        onSignUpClick: function(e) {
            e.preventDefault(); 
            this.ui.signInBtn.removeClass("selected"); 
            this.ui.signUpBtn.addClass("selected"); 
            this.$el.removeClass("sign-in-form");
            this.$el.addClass("sign-up-form");
            metricsAdapter.trackEvent("view", "signup");
        },
		
        onInputBlur: function(e) {
            var target = e.target;
            var value = "checkbox" === target.type ? target.checked : target.value;
            this.validate(target, value);
        }
    });
	
    return signInFormView;
});