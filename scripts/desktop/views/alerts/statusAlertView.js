define([
	"templates/desktop/alerts/statusAlertView.tpl", 
	"underscore", 
	"marionette", 
	"base/utils/dispatcher"
], function(template, _, marionette, dispatcher) {
    "use strict";
    
	var statusAlertView = marionette.ItemView.extend({
        className: "alert alert-status transition-out",
        template: template,
        coolDown: false,
        alertDuration: 5e3,
        coolDownDuration: 5e3,
        
        ui: {
            status: ".status"
        },
        
        events: {
            mouseover: "onMouseOver"
        },
        
        onlyAfterRender: ["onShowStatus"],
        
        initialize: function() {
            _.bindAll(this, "hide"); 
            this.listenTo(dispatcher, "show:status", this.onShowStatus);
        },
        
        hide: function() {
            this.$el.addClass("transition-out");
        },
        
        onShowStatus: function(server, statusText) {
            if (this.coolDown)
                return;
            
            this.server = server;
            this.ui.status.text(statusText); 
            this.$el.removeClass("transition-out"); 
            this.inactivityTimeout = clearTimeout(this.inactivityTimeout);
            this.inactivityTimeout = setTimeout(this.hide, this.alertDuration);
        },
        
        onMouseOver: function() {
            this.inactivityTimeout = clearTimeout(this.inactivityTimeout);
            this.hide();
            this.coolDown = true;
             
            setTimeout(_.bind(function() {
                this.coolDown = false;
            }, this), this.coolDownDuration);
        }
    });
    
    return statusAlertView;
});