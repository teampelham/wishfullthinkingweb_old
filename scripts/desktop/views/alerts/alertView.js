define([
	"templates/desktop/alerts/alertView.tpl", 
	"underscore", 
	"marionette"
], function(template, _, marionette) {
    "use strict";
    
	var alertView = marionette.ItemView.extend({
        className: "alert transition-in",
        template: template,
        alertDuration: 2e3,
        transitionDuration: 500,
        
		events: {
            mouseenter: "onMouseEnter",
            mouseleave: "onMouseLeave",
            "click .close-btn": "onCloseClick"
        },
        
        initialState: "transitionIn",
		
        states: {
            transitionIn: {
                toState: "visible",
                onEnter: function() {
                    this.$el.triggerReflow();
                    this.$el.removeClass("transition-in");
                    this.startTransition();
                }
            },
            
            visible: {
                toState: "transitionOut",
                onEnter: function() {
                    this.transitionOutTimeout = setTimeout(this.enterNextState, this.alertDuration);
                }
            },
            transitionOut: {
                onEnter: function() {
                    this.$el.addClass("transition-out");
                    this.startTransition();
                }
            }
        },
        
        initialize: function(alertObj) {
            this.bindAllWhenRendered(["enterNextState"]);   // should fire the enterNextState function when element is rendered
            
            if (_.isString(alertObj))
                alertObj = {
                    message: alertObj
                };
            this.type = alertObj.type;
            this.message = alertObj.message;
            
            if (this.type)
                this.$el.addClass("has-" + this.type);
        },
        
        serializeData: function() {
            return {
                type: this.type,
                message: this.message
            }
        },
        
        enterNextState: function() {
            this.currentState = this.currentState ? this.states[this.currentState].toState : this.initialState;
            if (this.currentState) 
                this.states[this.currentState].onEnter.call(this);
            else
                this.close();
        },
        
        startTransition: function() {
            setTimeout(this.enterNextState, this.transitionDuration);
        },
        
        onRender: function() {
            _.defer(this.enterNextState);
        },
        
        onMouseEnter: function() {
            clearTimeout(this.transitionOutTimeout);
        },
        
        onMouseLeave: function() {
            if ("visible" === this.currentState) {
                clearTimeout(this.transitionOutTimeout);
                this.transitionOutTimeout = setTimeout(this.enterNextState, this.alertDuration / 2);
            }
        },
        
        onCloseClick: function(e) {
            e.preventDefault(); 
            if ("visible" === this.currentState) {
                clearTimeout(this.transitionOutTimeout); 
                this.enterNextState();
            }
        }
    });
    return alertView;
});