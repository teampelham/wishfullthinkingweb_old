define([
	"templates/desktop/alerts/alertBarView.tpl", 
	"marionette", 
	"base/utils/dispatcher"
], function(template, marionette, dispatcher) {
    "use strict";
    var alertBarView = marionette.ItemView.extend({
        className: "alert-bar",
        template: template,
        
		events: {
            "click .close-btn": "onCloseClick"
        },
		
        initialize: function(e) {
			console.warn("init");
            //this.alert = e.alert, this.alert && this.$el.addClass(this.alert.type)
        },
		
        hide: function() {
            dispatcher.trigger("hide:bar", "alertbar");
        },
		
        serializeData: function() {
            return this.alert;
        },
		
        onCloseClick: function(e) {
            e.preventDefault();
			this.hide();
        }
    });
	
    return alertBarView;
});