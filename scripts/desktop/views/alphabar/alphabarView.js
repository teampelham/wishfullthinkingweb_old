define([
    "templates/desktop/alphabar/alphaBarView.tpl", 
    "underscore", 
    "marionette", 
    "base/utils/dispatcher", 
    "base/utils/constants/timelineEvents", 
    "desktop/views/lists/library/section/alphaList"
], function(e, t, i, n, s, a) {
    "use strict";
    var r = i.Layout.extend({
        className: "action-bar alpha-bar",
        template: e,
        regions: {
            list: ".alpha-bar-list"
        },
        modelEvents: {
            "change:currentFilters": "onListRefresh",
            "socket:timeline": "onTimeline"
        },
        collectionEvents: {
            reset: "onReset"
        },
        initialize: function() {
            this.listenTo(n, "resize", this.onResize), this.collection.clear(), this.updateCollection(), this.debouncedUpdateCollection = t.debounce(this.updateCollection, 2e3)
        },
        measure: function() {
            this.updateContainerHeight(), this.updateLineHeight()
        },
        updateContainerHeight: function() {
            this.containerHeight = this.$el.height()
        },
        updateLineHeight: function() {
            var e = this.collection.length;
            if (e) {
                var t = this.containerHeight / e;
                this.$el.css("line-height", t + "px")
            }
        },
        updateCollection: function() {
            this.collection.fetch({
                reset: !0,
                scope: "page"
            })
        },
        onRender: function() {
            this.list.append(new a({
                collection: this.collection
            }), this.$el), t.defer(t.bind(this.measure, this))
        },
        onListRefresh: function() {
            this.list.currentView.$el.addClass("hidden"), this.updateCollection()
        },
        onReset: function(e) {
            return e.length < 10 ? (this.$el.addClass("hidden"), void n.trigger("hide:bar", "alphabar", {
                preserve: !0
            })) : (this.containerHeight && this.updateLineHeight(), this.$el.removeClass("hidden"), this.list.currentView.$el.removeClass("hidden"), void n.trigger("show:bar", "alphabar"))
        },
        onResize: function() {
            this.updateContainerHeight(), this.updateLineHeight()
        },
        onTimeline: function(e) {
            (e.state === s.CREATED || e.state === s.DELETED) && this.debouncedUpdateCollection()
        }
    });
    return r
})