define([
	"templates/desktop/signInView.tpl", 
	"underscore", 
	"jquery", 
	"marionette", 
	"base/utils/dispatcher", 
	"base/adapters/metricsAdapter", 
	"base/commands/commands", 
	"base/models/appModel", 
	"desktop/views/signInFormView"
], function(template, _, $, marionette, dispatcher, metricsAdapter, commands, appModel, SignInFormView) {
    "use strict";
    
	var signInView = marionette.Layout.extend({
        className: "sign-in-container container",
        template: template,
        
        ui: {
            form: "#user-account-form"
        },
        
        regions: {
            form: "#user-account-form"
        },
        
        events: {
            "submit form": "onSubmit"
        },
        
        signIn: function(e) {
            if (this.form.currentView.validateAll(this.ui.form[0], ["username", "password"], e))
                return commands.execute("signIn", e);
            
            return $.Deferred().reject().promise();
        },
        
        signUp: function(e) {
            if (this.form.currentView.validateAll(this.ui.form[0], null, e))
                return commands.execute("signUp", e);
            
            return $.Deferred().reject().promise();
        },
        
        onRender: function() {
            this.form.show(new SignInFormView({
                showRememberMe: true
            }));
        },
        
        onSubmit: function(e) {
            e.preventDefault();
            
            var target = e.currentTarget;
            var submitBtn = this.form.currentView.ui.submitBtn;
            
            if (submitBtn.hasClass("active")) 
                return;
                
            submitBtn.addClass("active");
            
            var commandResult;
            var resultEvent;
            var userParams = {
                username: target.username.value,
                email: target.email.value,
                password: target.password.value,
                confirm: target.confirm.value,
                age: target.age.value,
                terms: target.terms.checked,
                remember: target.remember.checked
            };
            
            if (this.form.currentView.$el.hasClass("sign-in-form")) {
                commandResult = this.signIn(userParams);
                resultEvent = "signin_success";
            }
            else {
                commandResult = this.signUp(userParams);
                resultEvent = "signup_success";
            }
            
            commandResult.done(function() {
                dispatcher.trigger("navigate:refresh");
                metricsAdapter.trackEvent("view", resultEvent, {
                    userID: appModel.get("user").id
                });
            });
            
            commandResult.fail(_.bind(function() {
                submitBtn.removeClass("active");
            }, this));
        }
    });
    
    return signInView;
});