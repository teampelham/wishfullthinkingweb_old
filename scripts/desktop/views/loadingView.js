define([
	"templates/desktop/loadingView.tpl", 
	"marionette"
], function(template, marionette) {
    "use strict";
	
    var loadingView = marionette.ItemView.extend({
        className: "loading",
        template: template
    });
	
    return loadingView;
});