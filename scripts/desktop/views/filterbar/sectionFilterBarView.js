define([
	"templates/desktop/filterbar/sectionFilterBarView.tpl", 
	"underscore", 
	"marionette", 
	"base/utils/dispatcher"
], function(template, _, marionette, dispatcher) {
    "use strict";
	
    var sectionFilterBarView = marionette.ItemView.extend({
        className: "filter-bar filter-bar-fixed",
        template: template,
        events: {
            "click .clear-filters-btn": "onClearFiltersClick"
        },
		
        serializeData: function() {
            var currentFilters = this.model.get("currentFilters");
            var filerTitles = [];
            
			_.each(currentFilters, function(filter) {
                filerTitles.push(filter.titles.join(", "));
            });
			
			return {
                titles: filerTitles.join(", ")
            };
        },
		
        onClearFiltersClick: function(e) {
            e.preventDefault();
            var t = this.model.get("currentFilters");
            t.length = 0, this.model.trigger("change:currentFilters", this.model, t), n.trigger("navigate", "section", {
                serverID: this.model.server.id,
                sectionID: this.model.id,
                subset: this.model.get("subset"),
                trigger: !1
            })
        }
    });
	
    return sectionFilterBarView;
});