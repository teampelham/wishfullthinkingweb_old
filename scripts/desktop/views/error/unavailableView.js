define([
	"templates/desktop/error/errorView.tpl", 
	"base/views/error/baseUnavailableView"
], function(template, baseUnavailableView) {
    "use strict";
    
	var unavailableView = baseUnavailableView.extend({
        className: "container",
        template: template
    });
    
	return unavailableView;
});