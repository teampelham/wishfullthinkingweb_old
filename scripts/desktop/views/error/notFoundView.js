define([
	"templates/desktop/error/errorView.tpl", 
	"base/views/error/baseNotFoundView"
], function(template, baseNotFoundView) {
    "use strict";
	
    var notFoundView = baseNotFoundView.extend({
        className: "container",
        template: template
    });
	
    return notFoundView;
});