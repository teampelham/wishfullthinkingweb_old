define([
	"templates/desktop/desktopAppView.tpl", // e
	"underscore",                           // t
	"jquery",                               // i
	"marionette",                           // n
	"base/utils/dispatcher",               // s       
    "base/commands/commands",               // r
    "base/models/config",                   // l
	"desktop/views/backgroundView",        // d
	"desktop/regions/scrollableRegion",    // w
	"base/models/appModel",                // o
	"desktop/views/alerts/statusAlertView", // h
	"desktop/views/loadingView",           // c
	"desktop/views/alerts/alertView",      // u
    "desktop/views/navbar/navBarView",      // y
    "desktop/views/alerts/alertBarView"     // b
], function(template, _, $, marionette, dispatcher, commands, config, BackgroundView, ScrollableRegion, appModel, StatusAlertView, LoadingView, AlertView, NavBarView, AlertBarView) {
    "use strict";
    
    var desktopAppView = marionette.Layout.extend({
        el: "#perplexed",
        template: template,
        resizeDelay: 200,
        
        ui: {
            content: "#content",
            shadow: ".scroll-shadow"
        },
        
        // define all the custom regions of the desktop layout
        customRegions: {
            content: { selector: "#content", global: true, scrollable: true, channelName: "content" },
            background: { prepend: true, global: true },
            status: { prepend: true, global: true },
            alertbar: { modifier: "show-alert-bar", append: true, global: true },
            /*
            player: { append: true, global: true },
            miniPlayer: { append: true, global: true },*/
            navbar: {
                modifier: "show-nav-bar",
                prepend: true,
                global: true
            },
            sidebar: {
                modifier: "show-side-bar",
                append: true
            },
            actionbar: {
                modifier: "show-action-bar",
                append: true
            },
            breadcrumbbar: {
                modifier: "show-breadcrumb-bar",
                append: true
            },
            filterbar: {
                modifier: "show-filter-bar",
                append: true
            },
            alphabar: {
                modifier: "show-alpha-bar",
                append: true
            },
            
        },
        
        openAlerts: {},
        
        initialize: function() {
            _.bindAll(this, "triggerResizeEvent", "onResize", "onScroll"); 
            
            this.modals = [];
            
            _.each(_.keys(this.customRegions), function(cr) {
                var region = this.customRegions[cr];
                var selector = region.selector || _.uniqueId("r");      // make sure each region has a selector
                
                // add each region to the layout's region collection
                if (region.scrollable) 
                    this.addRegion(cr, new ScrollableRegion({
                        el: selector,
                        channelName: region.channelName
                    }));
                else
                    this.addRegion(cr, selector);
            }, this); 
            
            // hook up the event listeners for this view
            this.user = appModel.get("user"); 
            this.listenTo(appModel, {
                "change:authorized": this.onAuthorizedChange,
                "change:multiserver": this.onMultiserverChange
            });
            this.listenTo(this.user, {
                "change:locked": this.onUserLockedChange
            });
            this.listenTo(dispatcher, {
                "show:loading": this.showLoading,
                "show:view": this.showView,
                "show:alert": this.showAlert,
                "show:bar": this.showBar,
                "hide:bar": this.hideBar,
                "show:modal": this.showModal,
                "hide:modal": this.hideModal,
                "show:player": this.showPlayer,
                "hide:player": this.hidePlayer,
                "start:scrollListener": this.onStartScrollListener,
                "stop:scrollListener": this.onStopScrollListener,
                "update:scrollPosition": this.onUpdateScrollPosition
            });
            $(window).on("resize", this.onResize);
        },
        
        showCustomRegion: function(regionName, view) {
			var region = this.customRegions[regionName];
            var viewContainer = this[regionName];
            
            if (this.user.get("locked")) {      // regions are locked when the user isn't logged in
                viewContainer.lockedView = view;
                return void view.close();
            }
            
            // Does the region want to be appended, prepended, or just shown?
            if (region.append) 
                viewContainer.append(view, this.$el);
            else if (region.prepend) 
                viewContainer.prepend(view, this.$el);
            else 
                viewContainer.show(view);
        },
        
        showBar: function(bar, showOptions) {
            var barRegion = this.customRegions[bar];
            var barModifier = barRegion.modifier;
            
            if (barModifier) 
                this.$el.addClass(this.customRegions[bar].modifier);
            
            if (showOptions) 
                this.showCustomRegion(bar, showOptions);
        },
        
        hideBar: function(regionName, view) {
            if (!view)
                view = {};
                
            var region = this.customRegions[regionName];
            var modifier = region.modifier;
            
            if (modifier)
                this.$el.removeClass(modifier);
            
            if (!view.preserve) { 
                this[regionName].lockedView = null;
                this[regionName].close();
            }
        },
        
        showLoading: function() {
            this.hideBar("alertbar");
            
            if (this.user.get("locked"))
                this.resetContentLockedRegions();
                
            this.showCustomRegion("content", new LoadingView());
        },
        
        showView: function(view) {
            $(".tooltip").remove();                 // kill the tooltips
            this.hideBackground();                  // hide the background stuff
            dispatcher.trigger("cycle:cache");      // cycle the cache to free up some stuff
            this.currentView = view; 
            this.showCustomRegion("content", view); // show the view!
            this.ui.content.scrollTop(0);
        },
        
        showAlert: function(args) {
			var message = args.message;
            
            if (!this.openAlerts[message]) {
                var alertView = new AlertView(args);
                
                this.openAlerts[message] = true;
                this.listenToOnce(alertView, "close", function() {
                    delete this.openAlerts[message];
                }), 
                this.$el.append(alertView.render().el);
            }
        },
        
        showModal: function(modal) {
            this.modals.push(modal);
            var body = $("body");
            
            if (modal.transparent) 
                body.addClass("show-modal-transparent");
            
            body.append(modal.render().el);
        },
        
        hideModal: function(modal) {
			console.warn('view');
            var idx = _.indexOf(this.modals, modal);
            
            if (idx > -1)
                this.modals.splice(idx, 1)[0].close();
            
            if (modal.transparent && !_.findWhere(this.modals, { transparent: true }))
                $("body").removeClass("show-modal-transparent");
        },
        
        hideBackground: function() {
            if (this.currentView && this.currentView.model) {
                var model = this.currentView.model;
                dispatcher.trigger("hide:background", model);
                if (model.get("key")) 
                    commands.execute("playSound", {     // play sound??
                        key: model.get("key"),
                        stop: true,
                        background: true
                    });
            }
        },
        
        resetContentLockedRegions: function() {
            console.log('resetContentLockedRegions');
            /*_.each(_.keys(this.customRegions), function(regionName) {
                var customRegion = this.customRegions[regionName];
                if (!customRegion.global) {
                    var region = this[regionName];
                    if (region.lockedView)  
                        region.lockedView = null;
                }
            }, this);*/
        },
        
        triggerResizeEvent: function() {
            dispatcher.trigger("resize");
        },
        
        onRender: function() {
            this.showCustomRegion("background", new BackgroundView());
            this.showCustomRegion("status", new StatusAlertView());
        },
        
        onAuthorizedChange: function(e, authorized) {
            if (authorized) {
                this.showBar("navbar", new NavBarView({
                    model: appModel
                }), {
                    prepend: true
                });
                
                var alerts = config.get("alerts");
                if (alerts && alerts.length) { 
                    config.set("alerts", null); 
                    this.showBar("alertbar", new AlertBarView({
                        alert: _.first(alerts)
                    }));
                }
            } 
            else 
                this.hideBar("navbar"); 
            //this.hidePlayer();
        },
        
        onMultiserverChange: function(e, t) {
            console.log('onMultiserverChange');
            //this.$el.toggleClass("show-multiserver", t)
        },
        
        onUserLockedChange: function(appModel, locked) {
            if (locked) {
                console.log('t');
                this.lockAll();
            }
            else if (locked === false || locked === null)
                this.restoreAll();
        },
        
        onResize: function() {
            this.resizeTimeout = clearTimeout(this.resizeTimeout);
            this.resizeTimeout = setTimeout(this.triggerResizeEvent, this.resizeDelay);
        },
        
        onStartScrollListener: function() {
            this.ui.content.on("scroll", this.onScroll);
        },
        
        onStopScrollListener: function() {
            this.ui.content.off("scroll", this.onScroll); 
            this.ui.shadow.addClass("transition-out");
        },
        
        onScroll: function() {
            console.log('onScroll');
            //var e = this.ui.content.scrollTop();
            //this.ui.shadow.toggleClass("transition-out", 10 > e)
        },
        
        onUpdateScrollPosition: function(e) {
            console.log('onUpdateScrollPosition');
            //this.ui.content[0].scrollTop = e
        },
        
        lockAll: function() {
			console.warn('view');
            var regions = _.map(_.keys(this.customRegions), function(region) {
                return this[region];
            }, this);
            
            _.each(regions, function(region) {
                region.lockedView = region.currentView;
                region.close();
            }, this);
        },
        
        restoreAll: function() {
            _.each(_.keys(this.customRegions), function(regionKey) {
                var region = this[regionKey];
                var lockedView = region.lockedView;
                
                if (lockedView) {
                    this.showCustomRegion(regionKey, new lockedView.constructor(lockedView.options));
                    region.lockedView = null;
                }
            }, this);
        }
    });
	
    return desktopAppView;
});