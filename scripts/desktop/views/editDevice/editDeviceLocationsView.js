define([
    "templates/desktop/automatedDevice/editDeviceAddressLocationView.tpl", 
    "underscore", 
    "jquery",
    "backbone", 
    "desktop/views/editDevice/editDeviceBaseView", 
    "desktop/views/editDevice/editDeviceLocationsListView"
], function(template, _, $, bb, editDeviceBaseView, EditDeviceLocationsListView) {
	"use_strict";
	
	var editDeviceAddressLocationView = editDeviceBaseView.extend({
		template: template,
		fieldNames: ["Location"],
        
        ui: _.extend({
            locations: "#edit-location"
        }, editDeviceBaseView.prototype.ui),
        
        regions: {
            locationsRegion: ".paths-region"
        },
        
        events: {
            "click .add-location-btn": "onAddLocationClick",
            "change #edit-location": "onLocationChange"
        },
        
        onlyAfterRender: ["renderSectionLocation"].concat(editDeviceBaseView.prototype.onlyAfterRender),
		
		initialize: function() {
            if (!this.model.location) {
                var location = this.model.get("Location") || {};
                this.model.location = location;
            }
			
            this.populateCollections();
        },
        
        populateCollections: function() {
            var locationsPromise = this.populateLocations();
            
            locationsPromise = locationsPromise.then(_.bind(this.onLocationsPopulate, this)).then(_.bind(function() {
                if (this.model.get("location"))
                    this.showValidationResult(this.validate(this.model));
            }, this));
            
            return locationsPromise;
        },
        
        serializeData: function() {
            var data = editDeviceBaseView.prototype.serializeData.apply(this, arguments);
            if (this.locationsCollection && this.locationsCollection.any())
                data.locations = this.locationsCollection.toJSON();
            data.add = true;       // TODO: Change the model so that model.isNew() actually works
            data.edit = false;
            
            return data;
        },
		
		validate: function(model) {
            var issues = [];
            if (!model.get("location")) 
				issues.push("location");
                 
			return issues;
        },
		
		renderSectionLocation: function() {            
            var locationsView = new EditDeviceLocationsListView({
                collection: this.locationsCollection
            });
			
            this.listenTo(locationsView, "location:selected", this.onLocationSelected);
            this.locationsRegion.show(locationsView);
            /*var location = this.model.get("location") || 0;
            this.locationsCollection.each(_.bind(function(location) {
                var option = '<option ';
                if(location == location.get("key"))
                    option += 'selected="true" ';
                option += 'value="' + location.get('key') + '">' + location.get('title') + '</option>';
                this.ui.locations.append(option);
            }, this));*/
        },
        
        onLocationSelected: function (locationID) {
            var location, self = this; 
            this.locationsCollection.each(function(l) {
                if (!location) {
                    if(l.get("key") == locationID) {
                        location = l.attributes;
                    } else {
                        location = self.retrieveLocation(locationID, l.get("childLocations"));
                    }
                }
            });
            this.model.set("location", locationID);
            this.model.set("locationTitle", location.title);
            this.showValidationResult(this.validate(this.model));  
        },
        
        onLocationChange: function(e) {
            var locationDDL = $(e.currentTarget);
            this.model.set("location", locationDDL.val());  
            this.showValidationResult(this.validate(this.model));
        },
        
        onLocationsPopulate: function() {
            this.locationsCollection = this.model.locations;
            this.renderSectionLocation();
        },
        
        retrieveLocation: function (locationID, locations) {
            var location = _.findWhere(locations, function (l) {
               return l.key == locationID; 
            });
            
            if(location)
                return location;
              
            for(var i = 0; i < locations.length; i++) {
                location = this.retrieveLocation(locationID, locations[i].childLocations);
                if(location)
                    return location;
            }
        }
    });
	
    return editDeviceAddressLocationView;
});