define([
	"templates/desktop/automatedDevice/editDeviceTypeView.tpl", 
	"underscore", 
	"jquery", 
	"base/utils/languageUtil", 
	"base/utils/types/automationProtocolTypes", 
    "base/utils/types/automatedDeviceTypes", 
	"desktop/views/editDevice/editDeviceBaseView"
], function(template, _, $, s, automationProtocolTypes, automatedDeviceTypes, editDeviceBaseView) {
    "use strict";
	
    var editSectionTypeView = editDeviceBaseView.extend({
        template: template,
        ui: _.extend({
            automationTypes: ".wizard-block",
            deviceTitle: "#edit-device-title",
            deviceType: "#edit-device-type"
        }, editDeviceBaseView.prototype.ui),
        
        modelEvents: {
            "change:protocolType": "onAutomationTypeChange",
            "change:title": "onDeviceTitleChange",
            //"change:type": "onDeviceTypeChange"
        },
        
        events: {
            "click .section-type": "onAutomationTypeClick",
            "keyup #edit-device-title": "onEditDeviceTitleChange",
            "change #edit-device-title": "onEditDeviceTitleChange",
            "change #edit-device-type": "onEditDeviceTypeChange"
        },
        
        onlyAfterRender: ["renderSectionLanguage", "updateSectionTypesWizardBlocks", "updateDeviceTitle"].concat(editDeviceBaseView.prototype.onlyAfterRender),
        
        initialize: function() {
            this.populateCollections();
        },
        
        serializeData: function() {
            var currentType = this.model.get("type");
            var currentTitle = this.model.get("title");
            var currentDeviceType = this.model.get("deviceType");
            var data = editDeviceBaseView.prototype.serializeData.apply(this, arguments);
            data.deviceTypes = automatedDeviceTypes.getDeviceTypes();
            data.protocolTypes = automationProtocolTypes.getSectionTypes();

            data.add = true;       // TODO: Change the model so that model.isNew() actually works
            data.edit = false;
            
            if (currentType)
                data.currentType = currentType;
            if (currentTitle) 
                data.currentTitle = currentTitle;
            if (currentDeviceType)
                data.currentDeviceType = currentDeviceType;
                
            return data;
        },
        
        onRender: function() {
            this.ui.deviceTitle.val(this.model.get("locationTitle"));
        },
        
        validate: function(model) {
            var errors = [];
            if (!model.get("protocolType")) 
                errors.push("protocolType"); 
            
			if (!model.get("title")) 
                errors.push("title"); 
            
			if (!model.get("deviceType")) 
                errors.push("deviceType");
				
            return errors;
        },
        
        populateCollections: function() {       // TODO: Agent - turns on Register mode? Scanner - scans insteon device states??
            //var e = this.populateAgents();
            //var n = this.populateScanners();
            //var s = this.getSectionType();
            //var a = $.when(s, e, n, this.model.server.get("myPremiumSubscription") && this.model.server.supports("premiumMusic"));
            
            //if (this.model.isNew()) 
            //    a = a.then(t.bind(this.model.setAgentAndScanner, this.model));
            
            //a = a.then(e).then(t.bind(this.onAgentsPopulate, this)).then(t.bind(function() {
            //    this.showValidationResult(this.validate(this.model));
            //}, this));
            
            //return a;
        },
        
        renderSectionLanguage: function() {			// TODO: Use this for Locations/Hardwares?
            
            var e = this.model.agents.get(this.model.get("agent")),
                i = e.get("language"),
                n = this.model.get("language");
            if (!t.contains(i, n)) {
                var r = automationProtocolTypes.getSectionTypeByString(this.model.get("protocolType"));
                var o = r.defaultLanguage;
                this.model.set("language", o, {
                    silent: !0
                }), n = o
            }
            this.updateSectionLanguage(), this.ui.sectionLanguage.html(this.createMenuOptions(t.map(i, function(e) {
                return {
                    label: s.getLanguageName(e),
                    value: e
                }
            }), n))
        },
        
        updateSectionTypesWizardBlocks: function() {
            this.ui.automationTypes.removeClass("selected"); 
            this.ui.automationTypes.filter('[data-key="' + this.model.get("protocolType") + '"]').addClass("selected"); 
            this.$el.addClass("section-type-chosen");
        },
        
        
        updateSectionLanguage: function() {
            this.ui.sectionLanguage.val(this.model.get("language"))
        },
        
        updateDeviceTitle: function() {
            var deviceTitle = this.ui.deviceTitle.val();
            var title = this.model.get("title");
            
            if (deviceTitle !== title) 
                this.ui.deviceTitle.val(title);
        },
        
        onAutomationTypeClick: function(e) {
            e.preventDefault();
            var sectionType = $(e.currentTarget);
            if (sectionType.hasClass("selectable")) {
                var key = sectionType.data("key");
                var keyType = { protocolType: key  }; 
                var sectionTypeObj = automationProtocolTypes.getAutomationTypeByID(key);
                
                if (!this.model.get("title"))
                    keyType.title = sectionTypeObj.title; 
                this.model.set(keyType);
            }
        },
        
        onDeviceTypeChange: function(e, typeID) {
            var type = this.ui.deviceType.text();
            this.ui.deviceTitle.val(this.model.get("locationTitle") + " " + type);
        },
        
        onEditDeviceTitleChange: function(e) {
            var t = $(e.currentTarget);
            this.model.set("title", t.val()); 
            this.showValidationResult(this.validate(this.model));
        },
        
        onEditDeviceTypeChange: function(e) {
            var t = $(e.currentTarget);
            this.model.set("deviceType", t.val()); 
            this.model.set("type", automatedDeviceTypes.getDeviceTypeByID(t.val()).key);
            this.showValidationResult(this.validate(this.model))
        },
        
        onAutomationTypeChange: function(e, typeID) {
            this.updateSectionTypesWizardBlocks();
            var automationProtocol = automationProtocolTypes.getAutomationTypeByID(typeID);

            var title = this.model.get("title");
            var previousType = this.model.previous("protocolType");
            var previousProtocol = automationProtocolTypes.getSectionType(previousType);
            var previousProtocolTitle = previousProtocol && previousProtocol.title;
            if (!title || title === previousProtocolTitle) 
                this.model.set("title", automationProtocol.title); 
            
            this.setAutomationType(automationProtocol); 
            this.model.agents = null; 
            this.model.scanners = null; 
            this.model.settings = null; 
            this.populateCollections();
        },
        
        onDeviceTitleChange: function() {
            this.updateDeviceTitle(); 
            this.showValidationResult(this.validate(this.model));
        },
        
        onAgentsPopulate: function() {
            this.renderSectionLanguage();
        }
    });
    
    return editSectionTypeView;
});