define([
    "templates/desktop/automatedDevice/editDeviceLocationsListViewItem.tpl",
    "underscore",
	"marionette", 
    "jstree"
], function(template, _, marionette, jstree) {
    
    "use strict";
    var editDeviceLocationsListView = marionette.ItemView.extend({
        template: template,
        initialize: function() {

        },
        
        serializeData: function() {
            var locations = [];
            this.collection.each(function (location) {
                locations.push(location.attributes);
            });
            return locations;
        },
        
        onRender: function() {
            var self = this;
            
            this.$el.find('.dd').jstree({
                "core": {
                    "themes": {
                        "name": "default-dark"
                    }   
                }
            }).on("select_node.jstree", function(e, data) {
                var locationID = data.node.id.replace("location-", "");
                self.trigger("location:selected", locationID);
            });
        }
    });
    
    return editDeviceLocationsListView;
});