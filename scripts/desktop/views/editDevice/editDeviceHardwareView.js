define([
    "templates/desktop/automatedDevice/editDeviceHardwareView.tpl", 
    "underscore", 
    "jquery",
    "backbone", 
    "desktop/views/editDevice/editDeviceBaseView",
    "desktop/views/editDevice/editInsteonSetupView"
], function(template, _, $, bb, editDeviceBaseView, EditInsteonSetupView) {
	"use_strict";
	
	var editDeviceHardwareView = editDeviceBaseView.extend({
		template: template,
        
        ui: _.extend({
            deviceAddress: "#edit-address-title",
            hardware: "#edit-hardware",
            dimmable: "#edit-dimmable"
        }, editDeviceBaseView.prototype.ui),
        
        regions: {
            protocolRegion: ".protocol-region"
        },

        modelEvents: {
            "change:address": "onDeviceAddressChange",
            "change:dimmable": "onDeviceDimmableChange",
            "change:hardware": "onDeviceHardwareChange"
        },
        
        
        
        onlyAfterRender: ["renderSectionHardware", "updateDeviceAddress"].concat(editDeviceBaseView.prototype.onlyAfterRender),
		
		initialize: function() {
            this.hardwarePromise = this.populateCollections();     
        },
        
        serializeData: function() {
            var data = editDeviceBaseView.prototype.serializeData.apply(this, arguments);
            return data;
        },
        
        onRender: function() {
            var type = this.model.get("protocolType");
            //var protocolHardwares;
            
            if (type == "1") {
                //protocolHardwares = this.hardwareCollection.where({ automationProtocolID: 1 });
                this.protocolRegion.show(new EditInsteonSetupView({
                    model: this.model,
                    hardwares: this.hardwarePromise
                }));
            }
        },
		
		validate: function(model) {
            var issues = [];
            
            if (!model.get("address"))
                issues.push("address");
            if (!model.get("hardware")) 
				issues.push("hardware");
                 
			return issues;
        },
        
        populateCollections: function() {
          var hardwarePromise = this.populateHardwares();
            
            hardwarePromise = hardwarePromise.then(_.bind(this.onHardwarePopulate, this)).then(_.bind(function() {
                this.showValidationResult(this.validate(this.model));
            }, this));
            
            return hardwarePromise;  
        },
		
		renderSectionHardware: function() {            
            /*var locationsView = new EditDeviceLocationsListView({
                collection: this.locationsCollection
            });
			
            this.locationsRegion.show(locationsView);*/
            this.hardwareCollection.each(_.bind(function(hardware) {
                var option = '<option value="' + hardware.get('key') + '">' + hardware.get('title') + '</option>';
                this.ui.hardware.append(option);
            }, this));
        },

        onHardwarePopulate: function() {
            this.hardwareCollection = this.model.hardwares;
            this.renderSectionHardware();
        },
                
        updateDeviceAddress: function() {
            var deviceAddress = this.ui.deviceAddress.val();
            var address = this.model.get("address");
            
            if (deviceAddress !== address) 
                this.ui.deviceAddress.val(address);
        },
        
        updateDeviceDimmable: function() {
            this.ui.dimmable.prop("checked", !!this.model.get("dimmable"))
        },
        
        updateDeviceHardware: function() {
            var deviceHardware = this.ui.hardware.val();
            var hardware = this.model.get("hardware");
            
            if (deviceHardware !== hardware) 
                this.ui.hardware.val(hardware);
                // TODO: Enable dimmable status
        },
        
        onDeviceAddressChange: function() { 
            this.updateDeviceAddress();
            this.showValidationResult(this.validate(this.model));
        },
        
        onDeviceDimmableChange: function() { 
            this.updateDeviceDimmable();
            this.showValidationResult(this.validate(this.model));
        },
        
        onDeviceHardwareChange: function() { 
            this.updateDeviceHardware();
            this.showValidationResult(this.validate(this.model));
        },
        
        
    });
	
    return editDeviceHardwareView;
});