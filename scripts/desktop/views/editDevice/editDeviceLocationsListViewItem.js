define([
	"templates/desktop/automatedDevice/editDeviceLocationsListViewItem.tpl",
    "underscore", 
    "jquery",
	"marionette"
], function(template, _, $, marionette) {
	"use_strict";
	
	var editDeviceLocationsListViewItem = marionette.ItemView.extend({
		tagName: "li",
		className: "dd-item",
		template: template,
		
		initialize: function(itemViewOptions) {
			this.listViewType = itemViewOptions.listViewType;
		},
		
        serializeData: function() {
            return this.model.attributes;
        },
		
		onRender: function() {
			var childLocations = this.model.get("childLocations");
			if(childLocations && childLocations.length) {
				this.$el.find(".dd-handle").next(new this.listViewType(childLocations));
			}
		}
	});
	
	return editDeviceLocationsListViewItem;
});