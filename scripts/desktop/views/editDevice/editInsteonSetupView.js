define([
    "templates/desktop/automatedDevice/editInsteonSetupView.tpl", 
    "underscore", 
    "jquery",
    "backbone", 
    "desktop/views/editDevice/editDeviceBaseView",
    "base/models/library/automateddevice/insteonAgentModel",
    "base/utils/dispatcher"
], function(template, _, $, bb, editDeviceBaseView, InsteonAgentModel, dispatcher) {
	"use_strict";
	
	var editInsteonSetupView = editDeviceBaseView.extend({
		template: template,
        
        ui: _.extend({
            registerBtn: "#btnRegister",
            deviceAddress: "#edit-address-title",
            deviceHardware: "#edit-hardware",
            deviceDimmable: "#edit-dimmable"
        }, editDeviceBaseView.prototype.ui),
        
        events: {
            "click #btnRegister": "onClickRegisterBtn",
            "keyup #edit-address-title": "onEditAddressChange",
            "change #edit-address-title": "onEditAddressChange",
            "change #edit-hardware": "onEditHardwareChange",
            "change #edit-dimmable": "onEditDimmableChange",
        },
        
        initialize: function(initObj) {
            this.autoRegister = true;
            this.isRegistering = false;
            this.agent = new InsteonAgentModel({
                model: this.model,
                server: this.model.server
            });
            
            if(initObj.hardwares)
                initObj.hardwares.then(_.bind(this.onHardwarePopulate, this));
            
            this.listenTo(dispatcher, "pubsub:Device_Action", this.onMessageReceived);
        }, 
        
        serializeData: function() {
            var currentAddress = this.model.get("address");
            var currentHardware = this.model.get("hardware");
            var currentDimmable = this.model.get("dimmable");
            var data = editDeviceBaseView.prototype.serializeData.apply(this, arguments);
            if (this.hardwareCollection)
                data.hardware = this.hardwareCollection.toJSON();
            
            if (currentAddress)
                data.currentAddress = currentAddress;
            if (currentHardware) 
                data.currentHardware = currentHardware;
            if (currentDimmable)
                data.currentDimmable = currentDimmable;
                
            return data;
        },
        
        validate: function(model) {
            var issues = [];
            
            if (!model.get("address"))
                issues.push("address");
            if (!model.get("hardware")) 
				issues.push("hardware");
                 
			return issues;
        },
        
        renderHardwares: function() {            
            _.each(this.hardwareCollection, _.bind(function(hardware) {
                var option = '<option value="' + hardware.get('key') + '">' + hardware.get('title') + '</option>';
                this.ui.deviceHardware.append(option);
            }, this));
        },
        
        onHardwarePopulate: function() {
            this.hardwareCollection = this.model.hardwares.filter(function(hw) {
                return hw.get('automationProtocolID') == 1;
            });
            this.renderHardwares();
        },
        
        onClickRegisterBtn: function(e) {
            e.preventDefault();
            if(this.autoRegister) {
                if (this.isRegistering) {
                    this.ui.registerBtn.text('Cancelling Register').addClass("disabled");
                    this.agent.cancelRegisterMode();
                }
                else {
                    this.ui.registerBtn.text('Starting Register').addClass("disabled");
                    this.agent.beginRegisterMode();
                }
            }
        },
        
        onEditHardwareChange: function(e) {
            var hardwareDDL = $(e.currentTarget);
            this.model.set("hardware", hardwareDDL.val());  
            this.showValidationResult(this.validate(this.model));
        }, 
        
        onEditDimmableChange: function(e) {
            var addressInput = $(e.currentTarget);
            this.model.set("dimmable", addressInput.prop("checked"));  
            this.showValidationResult(this.validate(this.model));
        },
        
        onEditAddressChange: function(e) {
            var addressInput = $(e.currentTarget);
            this.model.set("address", addressInput.val());  
            this.showValidationResult(this.validate(this.model));
        },
        
        onMessageReceived: function(server, data) {
            data = data || {};
            
            if (data.type == "Register") {
                this.isRegistering = true;
                this.ui.registerBtn.text('Cancel Register').removeClass("disabled");
            }
            else if (data.type == "RegisterComplete") {
                this.isRegistering = false;
                if(data.item) {
                    this.ui.deviceAddress.val(data.item.DeviceAddress);
                    this.ui.deviceHardware.val(data.item.HardwareID);
                    this.showValidationResult(this.validate(this.model));
                }
                this.ui.registerBtn.text('Auto-Register');
            }
            else if (data.type == "RegisterCancel") {
                this.isRegistering = false;
                this.ui.registerBtn.text('Auto-Register').removeClass("disabled");
            }
            
            console.info(data);
        }
    });
    
    return editInsteonSetupView;
});