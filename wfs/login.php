<?php
include ('wfs/database_connection.php');

header('Content-Type: text/xml');
echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";

$error = array(); //Declare An Array to store any error message

// Validate username
if (empty($_POST['username'])) { //if no name has been supplied
    $error[] = 'Please Enter a username '; //add to array "error"
} else {
    $name = $_POST['username']; //else assign it a variable
}

if (empty($_POST['email'])) {
    $error[] = 'Please Enter your Email ';
} else {

    if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",
        $_POST['email'])) {
        //regular expression for email validation
        $Email = $_POST['email'];
    } else {
        $error[] = 'Your Email Address is invalid  ';
    }

}

if (empty($_POST['password'])) {
    $error[] = 'Please Enter Your Password ';
} else if(strlen($_POST['password']) < 8) {
    $error[] = 'Password must be at least 8 characters '; 
} else {
    $Password = $_POST['password'];
}

if (empty($error)) //send to Database if there's no error '
{ 

    // Make sure the email address is available:
    $query_verify_email = "SELECT * FROM members  WHERE Email ='$Email'";
    $result_verify_email = mysqli_query($dbc, $query_verify_email);
    if (!$result_verify_email) { //if the Query Failed ,similar to if($result_verify_email==false)
        echo ' Database Error Occured ';
    }

    if (mysqli_num_rows($result_verify_email) > 0) { 
        $error[] = 'That email
        address has already been registered.';
    } 
    
    // Make sure the username  is available:
    $query_verify_username = "SELECT * FROM members  WHERE Username ='$name'";
    $result_verify_username = mysqli_query($dbc, $query_verify_username);
    if (!$result_verify_username) { //if the Query Failed ,similar to if($result_verify_email==false)
        echo ' Database Error Occured ';
    }
    
    if (mysqli_num_rows($result_verify_username) > 0) { 
        $error[] = 'The Username is already in use.';
    }
}
    
if (empty($error)) //send to Database if there's no error '

{ // If everything's OK...
    // Create a unique  activation code:
    $activation = md5(uniqid(rand(), true));
    $authToken = md5(uniqid(rand(), true));

    $query_insert_user =
        "INSERT INTO `members` ( `Username`, `Email`, `Password`, `AuthToken`, `Activation`) VALUES ( '$name', '$Email', '$Password', '$authToken', '$activation')";

    $result_insert_user = mysqli_query($dbc, $query_insert_user);
    if (!$result_insert_user) {
        echo 'Query Failed ';
    }

    if (mysqli_affected_rows($dbc) == 1) { //If the Insert Query was successfull.

        // Send the email:
        $message = " To activate your account, please click on this link:\n\n";
        $message .= WEBSITE_URL . '/activate.php?email=' . urlencode($Email) . "&key=$activation";
        mail($Email, 'Registration Confirmation', $message, 'From:'.EMAIL);

        // Flush the buffered output.
        echo '<user email="' . $Email . '" username="' . $name . '" authenticationToken="' . $authToken . '">' . "\n";
        echo '    <entitlements all="0"></entitlements>' . "\n";
        echo "    <username>" . $name . "</username>\n";
        echo "    <email>" . $Email . "</email>\n";
        echo "    <authentication-token>" . $authToken . "</authentication-token>\n";
        echo "</user>\n";
/*<user email="peanutpelham@gmail.com" id="6153720" thumb="https://secure.gravatar.com/avatar/0d52cc6ec6d8588202307ddc856fe9d5?d=https%3A%2F%2Fplex.tv%2Favatars%2FEB9F9F%2F50%2F1" username="peanutp" title="peanutp" cloudSyncDevice="" locale="" authenticationToken="QRQyGPfFFZCQrqVgYQx7" scrobbleTypes="" restricted="0" home="0" guest="0" queueEmail="queue+isFUFNizMiTKM7PSKaCp@save.plex.tv" queueUid="ace23a482e5258cd" homeSize="1" secure="1" certificateVersion="2">
  <entitlements all="0">
  </entitlements>
  <profile_settings auto_select_audio="1" auto_select_subtitle="1" default_audio_language="" default_subtitle_language=""/>
  <username>peanutp</username>
  <email>peanutpelham@gmail.com</email>
  <joined-at type="datetime">2015-09-09 16:36:28 UTC</joined-at>
  <authentication-token>QRQyGPfFFZCQrqVgYQx7</authentication-token>
</user>*/


    } else { // If it did not run OK.
        $error[] = 'You could not be registered due to a system error. We apologize for any inconvenience.';
    }

} 

if (!empty($error)) //send to Database if there's no error '
{ //If the "error" array contains error msg , display them
    echo "<errors>\n";
    
    foreach ($error as $key => $values) {
        echo "    <error>".$values."</error>\n";
    }
    
    echo "</errors>\n";
}

mysqli_close($dbc); //Close the DB Connection

?>