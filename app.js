var express = require('express');
var fs = require('fs');
var app = express();


app.get('/*', function(req, res) {
	var html = fs.readFileSync(req.url.substr(1));
	if(req.url.indexOf('.html') > -1)
		res.writeHeader(200, {"Content-Type": "text/html"});
	else if(req.url.indexOf('.css') > -1)
		res.writeHeader(200, {"Content-Type": "text/css"});
	else if(req.url.indexOf('.js') > -1)
		res.writeHeader(200, {"Content-Type": "text/Javascript"});
	else {
		res.end('dang');
		return;
	}
	res.write(html);
	res.end();
});

var server = app.listen(8081, function() { 
	console.log('ready');
});