<?php
include ('../wfs/database_connection.php');

function parseTokenHeader() {
    $headers = array();
    foreach($_SERVER as $key => $value) {
        if (substr($key, 0, 15) <> 'HTTP_X_WF_TOKEN') {
            continue;
        }
        $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
        return $value;
    }
    return NULL;
}

header('Content-Type: text/xml');

$authHeader = parseTokenHeader();
$error = array(); //Declare An Array to store any error message

if(empty($authHeader) || $authHeader == "undefined") {
    $error[] = 'Invalid Token ';
}

if (empty($error)) //send to Database if there's no error '
{ 
    $query_verify = "SELECT D.* 
		FROM  `Device` AS D
		INNER JOIN  `Users_Devices` AS UD ON D.DeviceID = UD.DeviceID
		INNER JOIN  `Users` AS U ON UD.UserID = U.UserID
		WHERE AuthenticationToken = '$authHeader'";
    $result_verify = mysqli_query($dbc, $query_verify);
    
    if (!$result_verify) { //if the Query Failed ,similar to if($result_verify_email==false)
        $error[] = ' Database Error Occurred ';
    } 
	
	if(empty($error))
    {
        echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
		echo '<MediaContainer size="' . mysqli_num_rows($result_verify) . '">' . "\n";
        
        $resource_query = "SELECT Protocol, Address, Port, Uri, Local FROM `Resources` AS R 
            INNER JOIN `Device_Resources` AS DR ON R.ResourceID = DR.ResourceID
            WHERE DR.DeviceID = ?";
        $prepped_query = $dbc->prepare($resource_query);
        
        while($row = $result_verify->fetch_object()) {

            echo '    <Device name="' . $row->Name . '" product="' . $row->Product . '" productVersion="' . $row->ProductVersion . '" platform="' . $row->Platform . '" platformVersion="' . $row->PlatformVersion . '" device="' . $row->Device . '" clientIdentifier="' .                 $row->ClientIdentifier . '" createdAt="' . $row->CreatedAt . '" lastSeenAt="' . $row->LastSeenAt . '" provides="' . $row->Provides . '" owned="' . $row->Owned . '" accessToken="' . $row->AccessToken . '" publicAddressMatches="' . $row->PublicAddressMatches . '"  presence="' . $row->Presence . '">' . "\n";
            
            $prepped_query->bind_param('i', $row->DeviceID);
            $prepped_query->execute();
            $prepped_query->bind_result($protocol, $address, $port, $uri, $local);
            
            while($prepped_query->fetch()) {  // fetch_object instead?
                echo '        <Connection protocol="' . $protocol . '" address="' . $address . '" port="' . $port . '" uri="' . $uri . '" local="' . $local . '"/>' . "\n";
            } 
            
            echo "    </Device>\n";
        }
           
        echo "</MediaContainer>\n";
    }
    mysqli_close($dbc); //Close the DB Connection
}

if (!empty($error)) //send to Database if there's no error '
{ //If the "error" array contains error msg , display them
    http_response_code(422);
   //header("HTTP/1.1 401 DEAD WRONG");
    echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
    echo "<errors>\n";
    
    foreach ($error as $key => $values) {
        echo "    <error>".$values."</error>\n";
    }
    
    echo "</errors>\n";
}
?>